"""Fit the signal Bc2D0MuNu MC to fix the tail pars of the signal-charm and the mass-swap shapes.
Thereafter, execute fit to the data-driven samples employed in the fit (DCS, CF, MisID, WS) and 
extract sWeights. Correlation with MCORR monitored via the Kendall Tau coeffs.
"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint

import tensorflow as tf
import zfit
from zfit import z  # math backend of zfit
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from typing import Tuple
import sys
import numpy as np
import mplhep as hep
from collections import namedtuple
import numpy.typing as npt
from particle import Particle
import vector
from typing import Callable, TypeVar, Tuple, List
from functools import partial
from hepstats.splot import compute_sweights
from sweights import kendall_tau
from iminuit.pdg_format import pdg_format
from pathlib import Path
import pandas as pd
import pickle

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import batched_load_ROOT_file, oneshot_load_ROOT_file, read_feats

plt.style.use("science")


# sanity check to verify TF on GPU
import tensorflow as tf

if tf.test.gpu_device_name():
    print("Default GPU Device:{}".format(tf.test.gpu_device_name()))
else:
    raise Exception("Please install GPU version of TF")


# # NOTE: must adopt a python version < 3.10; test this.
# if sys.version_info[0] >= 3.10:
#     raise ValueError("zfit requires python<3.10")

# load the from-terminal wildcards
parser = ArgumentParser(description="eval configuration")
parser.add_argument(
    "-y",
    "--year",
    choices=["2016", "2017", "2018", "5.x_invfb", "9.x_invfb"],
    required=True,
    help="str: year of data taking; choices:['2016', '2017','2018, '5.x_invfb', '9.x_invfb']",
)
parser.add_argument(
    "-m",
    "--mode",
    choices=["D0MuNu", "D0pi0MuNu", "D0gMuNu"],
    default="D0MuNu",
    help="str: signal mode; choices: ['D0MuNu', 'D0gMuNu','D0pi0MuNu']; default: 'D0MuNu",
)
opts = parser.parse_args()

# XXX: implement relative paths
_mc_storage = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/MC/combined"
_data_storage = (
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
)

# init section
# ------------
# define function type for the zfit minimiser inint
# RFE: introduce a bound method to typecheck that this is a minimizer in zfit
MINIMIZER = TypeVar("MINIMIZER")

# init tuple container for fit results
FitObj = namedtuple("FitObj", ["model", "data", "result"])

# def section
# -----------
def __init_minimizer__(isgrad: bool) -> MINIMIZER:
    """book gradient in minimizer.
    For very large stats we might want to turn this off.
    """
    return zfit.minimize.Minuit(gradient=isgrad)


# instantiate immediately the two minimisers that we will use in this script
grad_minimizer = partial(__init_minimizer__, True)
nograd_minimizer = partial(__init_minimizer__, False)


def plot_fit(
    model,
    data,
    year,
    _lower=1810,
    _upper=1930,
    plot_path="./plots",
    plot_name=None,
) -> None:
    """Auxiliary function to plot the fit"""

    fig, (ax, ax_p) = plt.subplots(
        2,
        1,
        gridspec_kw={"height_ratios": [5, 1]},
        sharex=True,
    )
    nbins = 100
    lower, upper = _lower, _upper
    counts, bin_edges = np.histogram(data.unstack_x(), bins=nbins)
    # plot obs
    hep.histplot(
        counts,
        bins=bin_edges,
        histtype="errorbar",
        yerr=True,
        label="Data",
        ax=ax,
        color="black",
        markersize=3.5,
    )
    # plot total model
    x = np.linspace(lower, upper, num=1000)  # or np.linspace

    # Line plots of the total pdf and the sub-pdfs.
    y = model.pdf(x) * len(data.unstack_x()) / nbins * data.data_range.area()
    ax.plot(x, y, label="Fit", color="#7fcdbb")

    ax.set_ylabel(r"Candidates / ({:2g} MeV$/c^2$)".format((upper - lower) / nbins))
    ax.set_title(f"LHCb Unofficial {year} (13 TeV)")
    ax.legend()

    # pulls
    centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    ypreds = model.pdf(centres) * len(data.unstack_x()) / nbins * data.data_range.area()
    pulls = (counts - ypreds) / np.sqrt(counts)

    ax_p.set_ylim(-5.5, 5.5)
    ax_p.axhline(0, color="forestgreen", lw=0.5, ls="--")
    ax_p.axhline(5, color="red", lw=0.5, ls=":")
    ax_p.axhline(-5, color="red", lw=0.5, ls=":")
    ax_p.axhspan(-3, 3, facecolor="forestgreen", alpha=0.1)
    ax_p.bar(
        x=centres, height=pulls, color="black", width=(bin_edges[1] - bin_edges[0])
    )
    ax.set_ylim(bottom=0)
    ax_p.set_xlabel(r"Charm mass [MeV/$c^2$]")
    ax_p.set_ylabel(r"Pulls $[\sigma]$")

    Path(f"{plot_path}/{year}").mkdir(parents=True, exist_ok=True)
    [
        plt.savefig(f"{plot_path}/{year}/{plot_name}.{ext}")
        for ext in ("pdf", "png", "eps")
    ]


# RFE: refactor plotting to implement inheritance of functions
def plot_composite_model_fit(
    fitres: namedtuple,  # fit results container
    ktau: tuple,
    year: str,
    _lower=1810,
    _upper=1925,
    plot_path="./plots",
    plot_name=None,
) -> None:
    """Auxiliary function to plot the fit"""

    fig, (ax, ax_p) = plt.subplots(
        2,
        1,
        gridspec_kw={"height_ratios": [5, 1]},
        sharex=True,
    )
    nbins = 100
    lower, upper = _lower, _upper
    counts, bin_edges = np.histogram(fitres.data.unstack_x(), bins=nbins)
    # plot obs
    hep.histplot(
        counts,
        bins=bin_edges,
        histtype="errorbar",
        yerr=True,
        label="Data",
        ax=ax,
        color="black",
        markersize=3.5,
    )

    # plot total model
    binwidth = np.diff(bin_edges)[0]
    x = np.linspace(lower, upper, num=1000)  # or np.linspace

    # Line plots of the total pdf and the sub-pdfs.
    def model_y(model: Callable) -> npt.ArrayLike:
        """wrapper to return predicted y"""
        return model.ext_pdf(x) * binwidth

    y = model_y(fitres.model)
    ax.plot(x, y, label="Total fit model", color="#7fcdbb")

    # comb exp
    ycomb = model_y(fitres.model.pdfs[1])
    ax.fill_between(x, y1=ycomb, label="Combinatorial", facecolor="#fdae61", alpha=1)

    # k mass swap
    yks = model_y(fitres.model.pdfs[2])
    ax.fill_between(
        x, ycomb, yks + ycomb, label="Lower-mass swap", facecolor="#f46d43", alpha=1
    )

    # k mass swap
    yps = model_y(fitres.model.pdfs[3])
    ax.fill_between(
        x, ycomb, yps + ycomb, label="Higher-mass swap", facecolor="#d53e4f", alpha=1
    )

    # signal DCB
    ysig = model_y(fitres.model.pdfs[0])
    ax.plot(x, ysig, label="Signal", color="#3288bd", ls="--")

    ax.set_ylabel(r"Candidates / ({:2g} MeV$/c^2$)".format((upper - lower) / nbins))
    ax.set_title(f"LHCb Unofficial {year} (13 TeV)")
    ax.legend(loc="center", bbox_to_anchor=(0.5, -0.9), ncol=2)

    # annotate the kendall_tau
    ax.text(
        0.78,
        0.85,
        r"$\tau$ = {:.3f} $\pm$ {:.3f}".format(ktau[0], ktau[1]),
        horizontalalignment="center",
        verticalalignment="center",
        transform=ax.transAxes,
    )

    # pulls
    centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    ypreds = (
        fitres.model.pdf(centres)
        * len(fitres.data.unstack_x())
        / nbins
        * fitres.data.data_range.area()
    )
    pulls = (counts - ypreds) / np.sqrt(counts)

    ax_p.set_ylim(-5.5, 5.5)
    ax_p.axhline(0, color="forestgreen", lw=0.5, ls="--")
    ax_p.axhline(5, color="red", lw=0.5, ls=":")
    ax_p.axhline(-5, color="red", lw=0.5, ls=":")
    ax_p.axhspan(-3, 3, facecolor="forestgreen", alpha=0.1)
    ax_p.bar(
        x=centres, height=pulls, color="black", width=(bin_edges[1] - bin_edges[0])
    )
    ax.set_ylim(bottom=0)
    ax_p.set_xlabel(r"Charm mass [MeV/$c^2$]")
    ax_p.set_ylabel(r"Pulls $[\sigma]$")

    # save plot
    Path(f"{plot_path}/{year}").mkdir(parents=True, exist_ok=True)
    [
        plt.savefig(f"{plot_path}/{year}/{plot_name}.{ext}")
        for ext in ("pdf", "png", "eps")
    ]


# @nb.njit # https://vector.readthedocs.io/en/develop/usage/intro.html#Compiling-your-Python-with-Numba
def build_4vec(
    pX: npt.ArrayLike,
    pY: npt.ArrayLike,
    pZ: npt.ArrayLike,
    mass_hypo: npt.ArrayLike,
) -> vector.Lorentz:
    """build and check 4-vec with a given mass hypothesis

    Parameters
    ----------
    {pX, pY, pZ, mass_hypo}: npt.ArrayLike
        Three-momenta (spatial) and mass hypothesis, from PDG

    Returns
    --------
    vector.Lorentz:
        mass-swapped four-vector
    """
    fourvec = vector.array({"x": pX, "y": pY, "z": pZ, "M": mass_hypo})
    assert isinstance(fourvec, vector.Lorentz)

    return fourvec


def build_KPi_swaps(mc: npt.ArrayLike) -> Tuple[npt.ArrayLike, npt.ArrayLike]:
    """Perform the kaon-pion mass-hypo swaps

    Parameters
    ----------
    mc_df: npt.ArrayLike
        container (default: pandas df) holding the MC branches

    Returns
    -------
    ArrayLike:
        object holding the mass swaps
    """
    # perform the swaps
    # ------------------
    # kaon swap
    ks_4vec = build_4vec(
        mc.K_minus_PX,
        mc.K_minus_PY,
        mc.K_minus_PZ,
        np.repeat(Particle.from_pdgid(211).mass, len(mc)),
    )
    # proper pion
    pic_4vec = build_4vec(
        mc.Pi_1_PX,
        mc.Pi_1_PY,
        mc.Pi_1_PZ,
        np.repeat(Particle.from_pdgid(211).mass, len(mc)),
    )
    k_swp_charm_mass = (ks_4vec + pic_4vec).mass

    # pion swap
    ps_4vec = build_4vec(
        mc.Pi_1_PX,
        mc.Pi_1_PY,
        mc.Pi_1_PZ,
        np.repeat(Particle.from_pdgid(321).mass, len(mc)),
    )
    # proper kaon
    kc_4vec = build_4vec(
        mc.K_minus_PX,
        mc.K_minus_PY,
        mc.K_minus_PZ,
        np.repeat(Particle.from_pdgid(321).mass, len(mc)),
    )
    pi_swp_charm_mass = (ps_4vec + kc_4vec).mass

    return k_swp_charm_mass, pi_swp_charm_mass


def run_MC_fit(mc: npt.ArrayLike, prefix: str, verbose: bool = False) -> FitObj:
    """Execut the ML fit to extract the DCB tail parameters from a fit to signal MC

    Parameters
    ----------
    mc_df: npt.ArrayLike
        container (default: pandas df) holding the MC branches

    verbose: bool, default = False
        if True, print result and NLL sanity metrics

    prefix: str
        prefix for the pdf parameters, to identify which mass swap is being fitted

    Returns
    -------
    FitObj(data, model, result): namedtuple
        container of the fit results
    """

    # obs space
    mass_obs = zfit.Space("D0_M", limits=(1810, 1930))

    # generate dataset, subjected to boundaries inherited from obs
    data = zfit.Data.from_pandas(
        df=mc,
        obs=mass_obs,
        name="Charm mass",
    )  # discriminant variable

    # Signal component
    mc_mu_sig = zfit.Parameter(f"{prefix}_mc_mu_sig", 1865, 1865 - 30, 1865 + 30)
    mc_sigma_sig = zfit.Parameter(f"{prefix}_mc_sigma_sig", 8, 1, 100)
    mc_alpha_l = zfit.Parameter(f"{prefix}_mc_alpha_l", 10, 0, 20, floating=True)
    mc_alpha_r = zfit.Parameter(f"{prefix}_mc_alpha_r", 5, 0, 20, floating=True)
    mc_n_l = zfit.Parameter(f"{prefix}_mc_n_l", 2, 0.1, 10, floating=True)
    mc_n_r = zfit.Parameter(f"{prefix}_mc_n_r", 2, 0.1, 10, floating=True)
    MCmodel = zfit.pdf.DoubleCB(
        obs=mass_obs,
        mu=mc_mu_sig,
        sigma=mc_sigma_sig,
        alphal=mc_alpha_l,
        nl=mc_n_l,
        alphar=mc_alpha_r,
        nr=mc_n_r,
    )

    # build loss
    # NOTE not extended NLL, no yield needed
    nll = zfit.loss.UnbinnedNLL(model=MCmodel, data=data)

    # load and instantiate a minimizer; use minuit
    minimizer = zfit.minimize.Minuit(gradient=True)
    result = minimizer.minimize(loss=nll)

    # hesse error calculation if grad available
    if minimizer.minuit_grad:
        result.hesse()

    # print the fitted parameters and fit status
    if verbose:
        print(result)

    return FitObj(model=MCmodel, data=data, result=result.params)


# XXX: mostly duplicated function
def run_ks_fit(
    ms_array: npt.ArrayLike, limits: tuple, prefix: str, verbose: bool = False
) -> Tuple[FitObj, tuple]:
    """Execut the ML fit to extract the CB tail parameters from a fit to the mass swaps in signal MC

    Parameters
    ----------
    mc_array: npt.arraylike
        container (default: pandas df) holding the inv mass of the kaon/pion mass swaps

    limits: tuple
        per-swap mass range limits

    prefix: str
        prefix for the pdf parameters, to identify which mass swap is being fitted

    verbose: bool, default = False
        if True, print the results of the fit and NLL sanity checks

    Returns
    -------
    FitObj(data, model, result): namedtuple
        container of the fit results
    """

    # obs space
    mass_obs = zfit.Space("D0_M", limits=limits)

    # generate dataset, subjected to boundaries inherited from obs
    data = zfit.Data.from_numpy(
        array=ms_array,
        obs=mass_obs,
        name="Charm mass",
    )  # discriminant variable

    # Signal component
    ks_mu_sig = zfit.Parameter(f"{prefix}_mu_sig", 1700, 1500, 2500)
    ks_sigma_sig = zfit.Parameter(f"{prefix}_sigma_sig", 10, 1, 20)
    ks_alpha = zfit.Parameter(f"{prefix}_alpha", 3, 0, 20, floating=True)
    ks_n = zfit.Parameter(f"{prefix}_n", 3, 0.1, 10, floating=True)
    KSmodel = zfit.pdf.CrystalBall(
        obs=mass_obs,
        mu=ks_mu_sig,
        sigma=ks_sigma_sig,
        alpha=ks_alpha,
        n=ks_n,
    )

    # build loss
    # NOTE not extended NLL, no yield needed
    nll = zfit.loss.UnbinnedNLL(model=KSmodel, data=data)

    # load and instantiate a minimizer; use minuit
    minimizer = zfit.minimize.Minuit(gradient=True)
    result = minimizer.minimize(loss=nll)

    # hesse error calculation
    result.hesse()

    # print the fitted parameters and fit status
    if verbose:
        print(result)

    return FitObj(model=KSmodel, data=data, result=result.params)


# XXX: mostlu duplicated function
def run_ps_fit(
    ms_array: npt.ArrayLike, limits: tuple, prefix: str, verbose: bool = False
) -> FitObj:
    """Execut the ML fit to extract the CB tail parameters from a fit to the mass swaps in signal MC

    Parameters
    ----------
    mc_array: npt.arraylike
        container (default: pandas df) holding the inv mass of the kaon/pion mass swaps

    limits: tuple
        per-swap mass range limits

    prefix: str
        prefix for the pdf parameters, to identify which mass swap is being fitted

    verbose: bool, default = False
        if True, print the results of the fit and NLL sanity checks

    Returns
    -------
    FitObj(data, model, result): namedtuple
        container of the fit results
    """

    # obs space
    mass_obs = zfit.Space("D0_M", limits=limits)

    # generate dataset, subjected to boundaries inherited from obs
    data = zfit.Data.from_numpy(
        array=ms_array,
        obs=mass_obs,
        name="Charm mass",
    )  # discriminant variable

    # Signal component
    ps_mu_sig = zfit.Parameter(f"{prefix}_mu_sig", 1950, 1850, 2100)
    ps_sigma_sig = zfit.Parameter(f"{prefix}_sigma_sig", 10, 1, 20)
    ps_alpha = zfit.Parameter(f"{prefix}_alpha", -1, -3, 0, floating=True)
    ps_n = zfit.Parameter(f"{prefix}_n", 3, 0.1, 10, floating=True)
    PSmodel = zfit.pdf.CrystalBall(
        obs=mass_obs,
        mu=ps_mu_sig,
        sigma=ps_sigma_sig,
        alpha=ps_alpha,
        n=ps_n,
    )

    # build loss
    # NOTE not extended NLL, no yield needed
    nll = zfit.loss.UnbinnedNLL(model=PSmodel, data=data)

    # load and instantiate a minimizer; use minuit
    minimizer = zfit.minimize.Minuit(gradient=True)
    result = minimizer.minimize(loss=nll)

    # hesse error calculation
    result.hesse()

    # print the fitted parameters and fit status
    if verbose:
        print(result)

    return FitObj(model=PSmodel, data=data, result=result.params)


def build_data_config(
    year: str, storage: str = _data_storage, mode: str = opts.mode
) -> Callable:
    """Closure to protect the 'cut', 'prefix' and 'minimizer' parameters from the outer config"""

    def build_conf_dict() -> dict:
        """Return a config dict for all the samples"""

        data_samples = {
            "dcs": {
                "data_path": f"{storage}/D0MuNu/{year}/Bc2{mode}X_sig.root",
                "data_path": f"{storage}/D0MuNu/{year}/Bc2{mode}X_sig.root",
                "cut": "(K_minus_ID*Mu_plus_ID)>0",
                "prefix": f"dcs_{year}",
                "_minimizer": grad_minimizer(),
            },
            # "misid": {
            #     "data_path": f"{storage}/D0MuNu/2018/Bc2{mode}X_misid_hadron_enriched.root",
            #     "cut": "(K_minus_ID*Mu_plus_ID)>0",
            #     "prefix": f"misid_{year}",
            #     "_minimizer": grad_minimizer(),
            # },
            # "ws": {
            #     "data_path": f"{storage}/DpMuNu/{year}/Bc2{mode}X_sig.root".replace(
            #         "Bc2D0MuNuX_sig", "Bc2DpMuNuX_sig"
            #     ),
            #     "cut": "(D0_ID*Mu_plus_ID)<0",
            #     "prefix": f"ws_{year}",
            #     "_minimizer": grad_minimizer(),
            # },
            # "cf": {
            #     "data_path": f"{storage}/D0MuNu/{year}/Bc2{mode}X_sig.root",
            #     "cut": "(K_minus_ID*Mu_plus_ID)<0",
            #     "prefix": f"cf_{year}",
            #     "_minimizer": grad_minimizer(),
            # },
        }

        # # HACK: if '5.x_invfb' kwd, as in the case of the aggregated and resampled WS data, book only the 'kde' sub-entry
        # if year == "5.x_invfb":
        #     data_samples = {
        #         key: value for key, value in data_samples.items() if key == "kde"
        #     }

        return data_samples

    return build_conf_dict


def run_data_fit(
    MCfitres: FitObj,
    KSfitres: FitObj,
    PSfitres: FitObj,
    data_path: str,
    cut: str,
    prefix: str,
    _minimizer: MINIMIZER,
    OBS_LIMITS: tuple = (1810.0, 1925.0),
    # load the features selected for the two XGBs
    XGB_dict_path: str = "/home/blaised/private/Bc2D0MuNuX/XGB/yml/features.yml",
    verbose: bool = False,
) -> Tuple[FitObj, float, pd.DataFrame]:
    """Inherit fix parameters from fit to MC and perform EML fit data

    Parameters
    ----------
    {MC,KS,PS}fitres: FitObj(model, data, result)
        container of the MC/KS/PS fit result

    data_path: str,
        path to the data samples

    cut: str
        cut to apply to the data

    prefix: str
        necessary to instantiate different model parameters in the namespace

    minimizer: MINIMIZER
        minimizer, with or without grad enabled

    OBS_LIMITS: tuple
        mass range for the D0_M discriminant variables

    XGB_dict_path: str
        path to yml file containing variables used in the XGB classifier deployed downstream in the pipeline

    Returns
    -------
    FitObj(model, data, result): namedtuple
        container of the data fit result

    kt: tuple
        Kendall Tau (n,s) between sW and MCORR

    _data: pd.DataFrame
        persist the update dataframe
    """

    # obs space

    mass_obs = zfit.Space("D0_M", limits=OBS_LIMITS)

    # book the branches required for XGBs and fit
    if "kde" in prefix:
        # load only the variables resampled by the KDE
        _branches = [
            # "D0_M",
            # "B_plus_FIT_LTIME",
            "XGB_antiComb",
            # "XGB_antiVcb",
            # "B_plus_M",
            "B_plus_MCORR",
        ]
        print(f"KDE standalone load initialised: branches = {_branches}")
    else:
        # load all the variables necessary train and evaluate the XGB classifiers downstream
        _branches = list(
            list(read_feats(XGB_dict_path, "antiComb").keys())
            + list(read_feats(XGB_dict_path, "antiVcb").keys())
            + [
                "D0_M",
                "B_plus_M",
                "B_plus_MCORR",
                "K_minus_ID",
                "Mu_plus_ID",
                "CosXY_Mu_plus_D0",
                "B_plus_P",
            ]
        )

    # ad-hoc additions for FS signature and weights
    add_br = {
        "ws": ["D0_ID"],
        "misid": ["misid_w"],
    }
    for k in add_br.keys():
        if k in prefix:
            _branches = _branches + add_br[k]
            print(f"Added {add_br[k]} branch to expressions list in {k} dataframe")

    # load the data into np.array to implement FS cut
    _fdata = batched_load_ROOT_file(
        file=f"{data_path}:DecayTree",
        branches=_branches,
        cut=f"{cut}",
    )
    # HACK: unclear why, but incorporating the cut below in the df loader fails
    _data = _fdata.query(f"D0_M > {OBS_LIMITS[0]} and D0_M < {OBS_LIMITS[1]}")
    del _fdata  # memory cleanup

    # load the signal MC
    data = zfit.Data.from_pandas(
        df=_data,
        obs=mass_obs,
        name="Charm mass",
    )  # discriminant variable

    # load and instantiate a minimizer; use minuit
    minimizer = _minimizer

    # book the combinatorial exp model
    lam = zfit.Parameter(f"{prefix}_lambda", -0.0001, -0.05, -0.00001)
    comb_bkg = zfit.pdf.Exponential(lam, obs=mass_obs)

    # # prefit the upper-mass combinatorial
    # values = z.unstack_x(data)
    # obs_right_tail = zfit.Space("D0_M", (1900, 1925))
    # data_tail = zfit.Data.from_tensor(obs=obs_right_tail, tensor=values)
    # with comb_bkg.set_norm_range(obs_right_tail):
    #     nll_tail = zfit.loss.UnbinnedNLL(comb_bkg, data_tail)
    #     minimizer.minimize(nll_tail)

    # # NOTE: fix the exponent
    # lam.floating = False
    # assert (
    #     lam.floating == False
    # ), "ValueError: combinatorial exponent not fixed post-fit"

    # now proceed with inheriting the DCB tail parameters
    alpha_r = MCfitres.model.params["alphar"]
    alpha_l = MCfitres.model.params["alphal"]
    n_r = MCfitres.model.params["nr"]
    n_l = MCfitres.model.params["nl"]
    for par in (alpha_l, alpha_r, n_l, n_r):
        par.floating = False

    # book the signal component
    mu_sig = zfit.Parameter(f"{prefix}_mu_sig", 1865, 1865 - 30, 1865 + 30)
    sigma_sig = zfit.Parameter(f"{prefix}_sigma_sig", 3, 1, 10)
    signal = zfit.pdf.DoubleCB(
        obs=mass_obs,
        mu=mu_sig,
        sigma=sigma_sig,
        alphal=alpha_l,
        nl=n_l,
        alphar=alpha_r,
        nr=n_r,
    )

    # book the lower mass swap pdf & inherit fix parameters
    ks_alpha = KSfitres.model.params["alpha"]
    ks_n = KSfitres.model.params["n"]
    ks_mu = KSfitres.model.params["mu"]
    ks_sigma = KSfitres.model.params["sigma"]
    for par in (ks_mu, ks_sigma):
        par.floating = False

    ks = zfit.pdf.CrystalBall(
        obs=mass_obs,
        mu=ks_mu,
        sigma=ks_sigma,
        alpha=ks_alpha,
        n=ks_n,
    )

    # book the higher mass swap pdf & inherit fix parameters
    ps_alpha = PSfitres.model.params["alpha"]
    ps_n = PSfitres.model.params["n"]
    ps_mu = PSfitres.model.params["mu"]
    ps_sigma = PSfitres.model.params["sigma"]
    for par in (ps_mu, ps_sigma):
        par.floating = False

    ps = zfit.pdf.CrystalBall(
        obs=mass_obs,
        mu=ps_mu,
        sigma=ps_sigma,
        alpha=ps_alpha,
        n=ps_n,
    )

    # book yeilds and compose the model for the extended maximum likelihood minimisation
    sig_yield = zfit.Parameter(
        f"{prefix}_sig_yield", 100_000, 0, len(data.unstack_x()), step_size=1
    )
    sig_ext = signal.create_extended(sig_yield)

    comb_bkg_yield = zfit.Parameter(
        f"{prefix}_comb_bkg_yield", 10_000, 0, len(data.unstack_x()), step_size=1
    )
    comb_bkg_ext = comb_bkg.create_extended(comb_bkg_yield)

    ks_yield = zfit.Parameter(
        f"{prefix}_ks_yield", 1_000, 0, len(data.unstack_x()), step_size=1
    )
    ks_ext = ks.create_extended(ks_yield)

    ps_yield = zfit.Parameter(
        f"{prefix}_ps_yield", 1_000, 0, len(data.unstack_x()), step_size=1
    )
    ps_ext = ps.create_extended(ps_yield)

    # mixture of pdfs
    DATAmodel = zfit.pdf.SumPDF([sig_ext, comb_bkg_ext, ks_ext, ps_ext])

    # build loss
    # NOTE not extended NLL, no yield needed
    nll = zfit.loss.ExtendedUnbinnedNLL(model=DATAmodel, data=data)

    # minimise
    result = minimizer.minimize(loss=nll)

    # hesse error calculation
    result.hesse()

    # print the fitted parameters and fit status if verbose booked in closure
    if verbose:
        print(result)

    # sWeights segment
    # ----------------
    # must compute within the scope of the zfit.Par initialisation

    # 2-class sweights post-fit
    BkgPDF = zfit.pdf.SumPDF(DATAmodel.pdfs[1:])
    SigPDF = DATAmodel.pdfs[0]
    TwoClassPDF = zfit.pdf.SumPDF([SigPDF, BkgPDF])
    _sweights = compute_sweights(TwoClassPDF, data)

    # check sweights sum to unity within a tolerance of 1e-4
    assert (
        np.abs(sum(_sweights.values()) - 1.0).max() < 1e-4
    ), "sWeights do not add to unity within chosen tolerance"
    print("SUCCESS: sWeights extracted")

    # assign sweights
    _data["sw"] = _sweights[sig_yield]
    kt = kendall_tau(_data.sw, _data.B_plus_MCORR)

    # write to file
    # -------------
    out_path = data_path.replace(".root", f"_{prefix.split('_')[0]}_sw.pkl")
    _data.to_pickle(out_path)
    _data.name = prefix
    print(f"SUCCESS: sWeighted file written to {out_path}")

    # persit the fit result
    res_path = Path(f"./zfit_res/{prefix}")
    res_path.mkdir(parents=True, exist_ok=True)

    result.freeze()
    pickle.dump(
        result,
        open(f"{res_path}/model.pkl", "wb"),
    )

    # RFE: it would be nice to optionally return the data for memory consumption
    return FitObj(model=DATAmodel, data=data, result=result.params), kt, _data


def run_worflow(persist_data: bool) -> Callable:
    """Closure to enforce, when required, returning a dict of the sFit'd dataframe
    In this sense, it serves as constructor.
    """

    def fit_sw_data(year: str):
        """Fit the MC and then the data.
        Perform the sFit and add branch to the dataframe.
        """
        # HACK: we may need to book the '5'x_invfb' sfits for the standalone kde data
        # in that case, use the 2018 MC, as they emulate the largest proportion of the expected dataset conditions
        if year == "5.x_invfb" or year == "9x_invfb":
            _year = "2018"
        else:
            _year = year

        # load the Bc->D0MuNu signal MC df
        mc = oneshot_load_ROOT_file(
            file=f"{_mc_storage}/D0MuNu/{_year}/Bc2{opts.mode}.root:DecayTree",
            branches=[
                "D0_M",
                "K_minus_PX",
                "K_minus_PY",
                "K_minus_PZ",
                "Pi_1_PX",
                "Pi_1_PY",
                "Pi_1_PZ",
            ],
        )

        # study the relevant mass swaps
        k_swp_charm_mass, pi_swp_charm_mass = build_KPi_swaps(mc=mc)
        KSfitres = run_ks_fit(
            k_swp_charm_mass, limits=(1500, 1850), prefix=f"k_swp_{_year}"
        )
        plot_fit(
            model=KSfitres.model,
            data=KSfitres.data,
            plot_name="KSfit",
            _lower=1500,
            _upper=1850,
            year=_year,
        )

        PSfitres = run_ps_fit(
            pi_swp_charm_mass, limits=(1850, 2400), prefix=f"pi_swp_{_year}"
        )
        plot_fit(
            model=PSfitres.model,
            data=PSfitres.data,
            plot_name="PSfit",
            _lower=1850,
            _upper=2400,
            year=_year,
        )

        # fit the MC to get the tail parameters
        MCfitres = run_MC_fit(mc=mc, prefix=f"dzmunu_{year}")
        plot_fit(
            model=MCfitres.model, data=MCfitres.data, plot_name="MCfit", year=_year
        )

        # data samples config
        # -------------------
        data_samples = build_data_config(
            year
        )()  # HACK to hide the internal FS sign config, and the associate minimiser machinery

        # init  dict
        y_container = {}

        # execute the fit, plot and extract sWeights for each data sample
        for k in data_samples.keys():
            print(f"Fitting sample: {k} - year: {year}")
            # fit the DCS data
            DATAfitres, ktau, y_df = run_data_fit(
                MCfitres=MCfitres,
                KSfitres=KSfitres,
                PSfitres=PSfitres,
                **data_samples[k],
            )
            y_container[y_df.name.split(".")[0]] = y_df

            # plot the fit result
            plot_composite_model_fit(
                fitres=DATAfitres,
                plot_name=k,
                ktau=ktau,
                year=year,
            )
        print("SUCCESS: workflow complete\n")

        if persist_data:
            return y_container  # persist for access only when appropriate (aggregation)

    return fit_sw_data


# construct the function using the closure above
run_sfit_y = run_worflow(persist_data=False)
persisted_sfit_y = run_worflow(persist_data=True)


def main(year: str) -> None:
    """Depending on the year config, run a year-specific fit,
    extract the sWeights, or loop and aggregate the resulting

    Parameters
    ----------
    year: str, options: ['2016', '2017', '2018', '5.x_invfb']
        Year ID or full dataset considered in the analysis

    Returns
    -------
    Execute the fits. If '5.x_invfb', aggregate all years post-sWeights.
    """
    # XXX: this is a natural application for py3.10's `match`. However,
    # zfit requires a py version < 3.10
    dty = ["2016", "2017", "2018"]

    if year in dty:
        print(f"Booked fitter and sWeighter for dataset: {year}")
        run_sfit_y(year)

    if year == "5.x_invfb":
        print(f"Booked fitter and sWeighter for dataset: {year}")

        # init container for loop over the per-year samples
        y_container = {}
        print("Initialising the loop...")
        for y in ("2016", "2017", "2018"):
            y_container[y] = persisted_sfit_y(
                y
            )  # allocate a dict entry for the per-year sFit'd data samples

            # sanity check: make sure that the samples are non-empty
            for s in y_container.values():
                assert len(s) > 0, "ValueError: empty dataframe"

        print("SUCCESS. Aggregating and writing to file...")

        # aggregate
        conf = build_data_config(year)()

        # loop through the modes and write to file
        op = f"{_data_storage}/D0MuNu/{year}"
        Path(op).mkdir(parents=True, exist_ok=True)
        for k in conf.keys():
            aggr_df = pd.concat(
                # XXX: is there a nicer syntax? eg y_container[*years][k]
                [y_container[x][f"{k}_{x}"] for x in dty]
            )
            aggr_df.name = f"{k}_{year}"
            assert len(aggr_df) == len(y_container["2016"][f"{k}_2016"]) + len(
                y_container["2017"][f"{k}_2017"]
            ) + len(
                y_container["2018"][f"{k}_2018"]
            ), "ValueError: total df size does not match the sum of events in per-year dfs"

            # write to file
            aggr_df.to_pickle(f"{op}/Bc2D0MuNu_{k}_sw.pkl")
        print("===== SUCCESS. End of workflow. =====")

    if year == "9.x_invfb":
        print("Adding Run 1 data-taking years...")
        dty += ["2011", "2012"]
        print(f"Booked fitter and sWeighter for dataset: {year}")

        # init container for loop over the per-year samples
        y_container = {}
        print("Initialising the loop...")
        for y in ("2011", "2012", "2016", "2017", "2018"):
            y_container[y] = persisted_sfit_y(
                y
            )  # allocate a dict entry for the per-year sFit'd data samples

            # sanity check: make sure that the samples are non-empty
            for s in y_container.values():
                assert len(s) > 0, "ValueError: empty dataframe"

        print("SUCCESS. Aggregating and writing to file...")

        # aggregate
        conf = build_data_config(year)()

        # loop through the modes and write to file
        op = f"{_data_storage}/D0MuNu/{year}"
        Path(op).mkdir(parents=True, exist_ok=True)
        for k in conf.keys():
            aggr_df = pd.concat(
                # XXX: is there a nicer syntax? eg y_container[*years][k]
                [y_container[x][f"{k}_{x}"] for x in dty]
            )
            aggr_df.name = f"{k}_{year}"
            assert len(aggr_df) == len(y_container["2011"][f"{k}_2011"]) + len(
                y_container["2012"][f"{k}_2012"]
            ) + len(y_container["2016"][f"{k}_2016"]) + len(
                y_container["2017"][f"{k}_2017"]
            ) + len(
                y_container["2018"][f"{k}_2018"]
            ), "ValueError: total df size does not match the sum of events in per-year dfs"

            # write to file
            aggr_df.to_pickle(f"{op}/Bc2D0MuNu_{k}_sw.pkl")
        print("===== SUCCESS. End of workflow. =====")

    if year not in ["2016", "2017", "2018", "5.x_invfb", "9.x_invfb"]:
        print(f"Unknown year identifier: {year!r}.")


if __name__ == "__main__":
    main(opts.year)
