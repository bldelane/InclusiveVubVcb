"""Template builders for data and MC samples. The objects take a set of tuples and combined them
by executing a (efficiency-, for MC) luminosity-weighted sum of the tuples.

3 types of builders are defined:
- DataCombiner: for data samples
- MCCombiner: for MC samples
- NaiveCombiner: simple concat of tuples. It riases an exception the distributions of interest do not pass a compatibility test.
"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

from typing import Protocol, Union, List
from pandas import DataFrame
import pandas as pd
from numpy.typing import ArrayLike
from Pathlib import Path
import matplotlib.pyplot as plt

plt.style.use("science")


class Combiner(Protocol):
    """Template builders for data and MC samples. The objects take a set of tuples and combined them"""

    def combine(
        self, tuples: List, branches: Union[List[str, ...], None], **kwargs
    ) -> Union[DataFrame, ArrayLike]:
        ...


class NaiveCombiner:
    """Simple combiner that concatenates the tuples. It raises an exception if the distributions of interest do not pass a compatibility test."""

    @staticmethod
    def plot_evaluate_compatibility(
        tuples: List, branches: List[str, ...], **kwargs
    ) -> None:
        """Plot the distributions of interest and evaluate their compatibility in a simple way."""

        # book plots
        p = Path("/home/blaised/private/Bc2D0MuNuX/src/tmp/templates/plots")
        p.mkdir(parents=True, exist_ok=True)

        for var in branches:
            fig, ax = plt.subplots()
            for t in tuples:
                ax.hist(t[var], bins=25, label=var, density=True)
            ax.legend()
            [fig.savefig(f"{p}/{var}.{ext}") for ext in ["pdf", "png"]]

    def combine(
        self, tuples: List, branches: Union[List[str, ...], None], **kwargs
    ) -> DataFrame:
        """Concatenate the tuples. It raises an exception if the distributions of interest do not pass a compatibility test."""

        # include the plotting routine
        if branches is not None:
            
            
        print("Executing naive cobination (aka concatenation) of tuples: ", tuples)
        df = pd.concat(tuples)
        return df
