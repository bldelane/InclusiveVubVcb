"""Script to extrapolate the weighting from upper-mass misid combinatorial.
Sebsequently, the extrapolated weights are applied to PC WS data."""

from re import I
import numpy as np
import boost_histogram as bh
import pandas as pd
import os, sys
from numpy.typing import ArrayLike
from typing import Union, Any, Tuple
import hist
from hist import Hist
from uncertainties import unumpy
from numba_stats import expon
from iminuit.cost import LeastSquares
from iminuit import Minuit

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")  # XXX: temporary
from utils import plot_data

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit")  # XXX: temporary
from bmlfit import sig_hi_base_sel

import matplotlib.pyplot as plt

plt.style.use(["science", "high-contrast"])


def fill_wh(
    data: ArrayLike,
    weights: ArrayLike,
    range: Union[Tuple, list],
    label: Union[None, str] = None,
    bins: int = 30,
    normed: bool = True,
) -> Any:
    """Fill (weighted) histogram and return hist object"""
    h = hist.Hist(
        hist.axis.Regular(bins, *range, name=label),
        storage=hist.storage.Weight(),
    )
    h.fill(data, weight=weights)

    if normed:
        h /= h.sum().value

    return h


if __name__ == "__main__":

    # build the nominal hi-lev selection
    HI_LEV_SEL = sig_hi_base_sel(
        _visM_inf=5_500,  # assume no Bc in WS and MisID
        _visM_sup=10_000,
        _k_antiComb=0.66,  # NOTE: for simplicity, we relax the antiComb selection cut
        _k_antiVcb=0.375,
    ).build()  # hi-level sel

    _range = (7_000, 12_000)

    # load the WS PC data
    # NOTE: for simplicity, we do not reweight the WS data in PT
    pc_ws = pd.read_pickle(
        "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_ws_sw_xgb.pkl"
    ).query(f"D0_IPCHI2_OWNPV < 3 and {HI_LEV_SEL}")

    # read in the misid
    misid = pd.read_pickle(
        "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_misid_hadron_enriched_misid_sw_xgb.pkl"
    ).query(f"{HI_LEV_SEL}")

    # fill the histogram
    WSh = fill_wh(
        data=pc_ws["B_plus_MCORR"],
        weights=pc_ws["sw"],
        range=_range,  # upper mass, nonzero bins
        label="PC WS data",
    )

    # fill the histogram
    MisIDh = fill_wh(
        data=misid["B_plus_MCORR"],
        weights=misid["sw"],
        range=_range,  # upper mass, nonzero bins
        label="MisID data",
    )

    # compute the ratio of yields -> that is what we must fit
    # NOTE: this is a scale factor taking into account the template normalisations
    WS_bs = unumpy.uarray(WSh.values(), WSh.variances() ** 0.5)
    MisID_bs = unumpy.uarray(MisIDh.values(), MisIDh.variances() ** 0.5)

    # what must we fit
    hRATIO = MisID_bs / WS_bs  # RATIO x WS = MisID combinatorial component

    # execute an exp CHI2 fit to the high-mass region, from 6 GeV onwards
    x = WSh.axes[0].centers
    assert (
        x.all() == MisIDh.axes[0].centers.all()
    ), "Histogram binning must be identical"
    y = unumpy.nominal_values(hRATIO)
    yerr = unumpy.std_devs(hRATIO)

    pol1 = lambda x, p0, p1: p0 + p1 * x
    pol2 = lambda x, p0, p1, p2: p0 + p1 * x + p2 * x**2
    pol3 = lambda x, p0, p1, p2, p3: p0 + p1 * x + p2 * x**2 + p3 * x**3

    cost1 = LeastSquares(x, y, yerr, pol1)
    cost2 = LeastSquares(x, y, yerr, pol2)
    cost3 = LeastSquares(x, y, yerr, pol3)

    mi1 = Minuit(cost1, p0=0, p1=-1)
    mi2 = Minuit(cost2, p0=0, p1=-1, p2=0.01)
    mi3 = Minuit(cost3, p0=0, p1=-1, p2=0.01, p3=0.01)

    # minimise
    mi1.migrad()
    mi1.hesse()
    print(mi1)

    mi2.migrad()
    mi2.hesse()
    print(mi2)

    mi3.migrad()
    mi3.hesse()
    print(mi3)

    fig, ax = plt.subplots()
    ax.errorbar(
        x,
        y,
        yerr=yerr,
        xerr=WSh.axes[0].widths / 2,
        fmt=".",
        color="black",
        label="Data",
    )

    # ax.plot(x, pol1(x, *mi1.values), label="Order-1 Poly", lw=1.5)
    # ax.plot(x, pol2(x, *mi2.values), label="Order-2 Poly", lw=1.5)
    # ax.plot(x, pol3(x, *mi3.values), label="Order-3 Poly", lw=1.5)
    ax.set_xlabel(r"$m_{\rm{corr}}(D^0\mu^+)$ [MeV/$c^2$]")
    ax.set_ylabel("Per-bin MisID/WS ratio")
    ax.set_ylim(bottom=-3, top=3)
    ax.legend()
    [fig.savefig(f"ws/plots/misid_extrap.{ext}") for ext in ["png", "pdf"]]
