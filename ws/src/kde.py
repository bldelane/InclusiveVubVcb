"""KDE studies using WS PC GBR data."""

__author__ = ["Blaise Delaney"]
__email__ = ["blaise.delaney at cern.ch"]

# read in the PC WS data - NOTE: no sWeights applied; that is the point of this study!
import pandas as pd
import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")  # XXX: temporary
from utils import plot_super
from utils.plot_super import plot_hist_err, plot_data
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from numpy.typing import ArrayLike
from typing import AnyStr, Callable, Union, Any, Tuple
import kalepy
import hist 

plt.style.use(["science"])
from mpl_toolkits.axes_grid1 import make_axes_locatable

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit")  # XXX: temporary
from bmlfit import sig_hi_base_sel


class KDEFactory(object):
    """Reweighting class & sampler factory.
    The Kalepy KDE object posses the functionalities of interest.
    """

    def __init__(
        self,
        data: ArrayLike,
        weights: Union[ArrayLike, None],
    ) -> None:
        self._data = data
        self._weights = weights

    @property
    def data(self) -> ArrayLike:
        return self._data

    @property
    def weights(self) -> ArrayLike:
        return self._weights

    def get_kernel(self, kernel: str, **kwargs) -> Any:
        """Get the kernel function."""

        # configure the data
        _dataset = self.data.to_numpy()
        if _dataset.shape[-1] < _dataset.shape[0]:
            _dataset = _dataset.transpose()

        # weights, can be None
        if self.weights is not None:
            _weights = self.weights.to_numpy()
            if _weights.shape[-1] < _weights.shape[0]:
                _weights = _weights.transpose()
        else:
            _weights = self.weights

        # RFE: UNIT TEST

        # factory section
        if kernel == "gaussian":
            return GaussianKDE(data=_dataset, weights=_weights, **kwargs)
        else:
            raise ValueError(f"Unknown kernel: {kernel}")


class GaussianKDE(KDEFactory):
    def __init__(
        self,
        data: ArrayLike,
        weights: Union[ArrayLike, None],
        bandwidth: Union[float, None] = None,  # let kalepy choose
    ) -> None:
        super().__init__(data, weights)
        self.kde = kalepy.KDE(
            dataset=data,
            weights=weights,
            kernel="gaussian",
            bandwidth=bandwidth,
        )

    def resample(self, n: int) -> np.ndarray:
        """Resample the data."""
        return self.kde.resample(n)


def plot_truth_resampled(
    truth_sb: ArrayLike,
    truth_sr: ArrayLike,
    resampled_sb: ArrayLike,
    resampled_sr: ArrayLike,
    bins: int,
    range: Union[Tuple, list],
    xlabel: str,
    filename: str,
    title: str = "LHCb Unofficial (13 TeV)",
    ylabel: str = "Candidates, normalised",
    wts: Union[ArrayLike, None] = None,
) -> None:
    """Plot the truth and resampled data."""

    fig, ax = plt.subplots()

    # plot the truth
    plot_data(
        data=truth_sb,
        ax=ax,
        bins=bins,
        label="Truth SB",
        col="firebrick",
        isnorm=True,
        range=range,
    )
    plot_data(
        data=truth_sr,
        ax=ax,
        bins=bins,
        label="Truth SR",
        col="navy",
        isnorm=True,
        range=range,
    )

    # plot the resampled
    plot_hist_err(
        ax=ax,
        data=resampled_sb,
        bins=bins,
        label="Resampled SB",
        color="coral",
        _isnorm=True,
        range=range,
    )
    plot_hist_err(
        ax=ax,
        data=resampled_sr,
        bins=bins,
        label="Resampled SR",
        color="cornflowerblue",
        _isnorm=True,
        range=range,
    )

    # plot settings
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.legend()

    # save
    [
        plt.savefig(f"/home/blaised/private/Bc2D0MuNuX/ws/plots/kde/{filename}.{ext}")
        for ext in ("png", "pdf")
    ]

def fill_wh(
    data: ArrayLike,
    weights: ArrayLike,
    range: Union[Tuple, list],
    label: Union[None, str] = None,
    bins: int = 30,
) -> Any:
    """Fill (weighted) histogram and return hist object"""
    h = hist.Hist(hist.axis.Regular(bins, **range, name=label), storage=hist.storage.Double())
    h.fill(data, weight=weights)

if __name__ == "__main__":
    # impose the high-level selectoin
    # ...

    # formulate hi-lev sel
    HI_LEV_SEL = sig_hi_base_sel(
        _visM_inf=3_500,
        _visM_sup=6285,
        _k_antiComb=0.0,
        _k_antiVcb=0.375,
    ).build()  # hi-level sel

    # load the prompt-charm WS data
    pc_ws = pd.read_pickle(
        "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_GBR_PC_ws_sw_xgb.pkl"
    ).query(HI_LEV_SEL)

    # pseudo-fit results
    MEAN = 1869
    SIGMA = 10
    LOW_SIDEBAND_M = MEAN - 3 * SIGMA
    HIGH_SIDEBAND_M = MEAN + 3 * SIGMA

    # kde-fit the sidebands
    pc_ws_sidebands = pc_ws.query(
        f"D0_M < {LOW_SIDEBAND_M} or D0_M > {HIGH_SIDEBAND_M}"
    )
    pc_ws_sigregion = pc_ws.query(
        f"D0_M >= {LOW_SIDEBAND_M} or D0_M <= {HIGH_SIDEBAND_M}"
    )

    # resampled sideband
    gKDE_sb = (
        KDEFactory(
            data=pc_ws_sidebands[["B_plus_MCORR", "XGB_antiComb"]],
            weights=None,
        )
        .get_kernel("gaussian", bandwidth=0.01)
        .resample(1_00_000)
    )
    gKDE_sb_df = pd.DataFrame(
        {"B_plus_MCORR": gKDE_sb[0], "XGB_antiComb": gKDE_sb[1]},
    )

    # resampled signal region
    gKDE_sr = (
        KDEFactory(
            data=pc_ws_sigregion[["B_plus_MCORR", "XGB_antiComb"]],
            weights=None,
        )
        .get_kernel("gaussian", bandwidth=0.01)
        .resample(1_00_000)
    )
    gKDE_sr_df = pd.DataFrame(
        {"B_plus_MCORR": gKDE_sr[0], "XGB_antiComb": gKDE_sr[1]},
    )

    # visualise the resampling
    plot_truth_resampled(
        truth_sb=pc_ws_sidebands["XGB_antiComb"],
        truth_sr=pc_ws_sigregion["XGB_antiComb"],
        resampled_sb=gKDE_sb_df["XGB_antiComb"],
        resampled_sr=gKDE_sr_df["XGB_antiComb"],
        bins=30,
        range=(0, 1),
        xlabel=r"$\chi_{comb}$ [Arbitrary Units]",
        filename="resampled_XGBcomb",
    )

    # visualise the resampling
    plot_truth_resampled(
        truth_sb=pc_ws_sidebands.query("XGB_antiComb>0.2")["B_plus_MCORR"],
        truth_sr=pc_ws_sigregion.query("XGB_antiComb>0.2")["B_plus_MCORR"],
        resampled_sb=gKDE_sb_df.query("XGB_antiComb>0.2")["B_plus_MCORR"],
        resampled_sr=gKDE_sr_df.query("XGB_antiComb>0.2")["B_plus_MCORR"],
        bins=30,
        range=(4_200, 7_200),
        xlabel=r"$m_{corr}(D^+\mu^+)$ [MeV$/c^2$]",
        filename="mcorr_templ",
    )
