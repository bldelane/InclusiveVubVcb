"""Define the reweighter protocol that handles the reweighting of the events."""

__author__ = ["Blaise Delaney"]
__email__ = ["blaise.delaney at cern.ch"]

from os import rmdir
from typing import Protocol, Union, Callable, List, Dict, Any, Tuple
from numpy.typing import ArrayLike
from pandas import DataFrame, read_pickle
import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")  # XXX: temporary
from utils import plot_super
from utils.plot_super import plot_hist_err, plot_data
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

plt.style.use(["science", "high-vis"])
from mpl_toolkits.axes_grid1 import make_axes_locatable

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit")  # XXX: temporary
from bmlfit import sig_hi_base_sel
from hep_ml.reweight import GBReweighter, FoldingReweighter


class ReweighterFactory(Protocol):
    """Protocol for the reweighter."""

    def reweight(
        self,
        control: Union[DataFrame, ArrayLike],
        data: Union[DataFrame, ArrayLike],
    ) -> Union[DataFrame, ArrayLike]:
        """Reweight the data to match the control."""
        ...


class BinaryReweighter(ReweighterFactory):
    """Reweighter class to compare and reweight the data to to the control."""

    def __init__(
        self,
        control: DataFrame,
        data: DataFrame,
        probe_vars: List[str],  # list of variables to use for the reweighting
        spectator_vars: Union[
            List[str], None
        ],  # variables to assess compatibility and not use in reweighting
        training_vars: Union[List[str], str],  # variables to use for the reweighting
        training_window: str = "B_plus_M>6_400",  # upper-mass DCS
    ) -> None:
        """Initialize the reweighter."""
        self._control = control  # target
        self._data = data  # what I want to reweight
        self.probe_vars = probe_vars
        self.spectator_vars = spectator_vars
        self._training_vars = training_vars
        self._training_window = training_window

    @property
    def control(self) -> Union[DataFrame, ArrayLike]:
        """Return the control."""
        return self._control

    @property
    def data(self) -> Union[DataFrame, ArrayLike]:
        """Return the control."""
        return self._data

    @property
    def training_vars(self) -> Union[List[str], str]:
        """Return the training variables."""
        return self._training_vars

    @property
    def training_window(self) -> str:
        """Return the training window."""
        return self._training_window

    def reweight(self) -> Union[DataFrame, ArrayLike]:
        """Reweight (with folding) the data to match the control"""
        reweighter_base = GBReweighter(max_depth=5, gb_args={"subsample": 0.33})
        reweighter = FoldingReweighter(reweighter_base, n_folds=22)

        # train the reweighter
        print("Training the reweighter on window: ", self.training_window)
        reweighter.fit(
            original=self._data.query(self._training_window)[self._training_vars],
            target=self._control.query(self._training_window)[self._training_vars],
            original_weight=self._data.query(self._training_window)["sw"],
            target_weight=self._control.query(self._training_window)["sw"],
        )

        # inference. NOTE: result is computed as original_weight * reweighter_multipliers.
        print("Reweighting the data across the full region of interest")
        WS_weights = reweighter.predict_weights(
            original=self._data[self._training_vars], original_weight=self._data["sw"]
        )

        # generate a copt of the WS dataset and allocate column for the weights
        GBR_WS_df = self._data.copy()
        GBR_WS_df["gbr_sw"] = WS_weights
        print("Done!")

        return GBR_WS_df

    # RFE: change of design: self.reweight generates the self.ws_gbr attribute
    @property
    def ws_gbr(self, **kwargs) -> Union[DataFrame, ArrayLike]:
        """Update the data sample with GBR weights."""
        return self.reweight()

    def viz_compare(
        self,
        vars: Dict[Any, Any],  # variable alias : range
        plot_path: str = "/home/blaised/private/Bc2D0MuNuX/ws/plots/validation",
        cut_str: Union[str, None] = None,
        bins: int = 30,
        xlabel: Union[str, None] = None,
        weights_var: str = "sw",
        title: str = "LHCb Unofficial (13 TeV)",
        ylabel: str = "Candidates, normalised",
        data_label: str = "WS",
        control_label: str = "Control",
    ) -> None:
        """Visualise the comparison of the data to the control."""

        # book the plots directory
        Path(f"{plot_path}").mkdir(parents=True, exist_ok=True)

        for var in vars.keys():
            fig, ax = plt.subplots()

            # legend labels
            self._data.name = data_label
            self._control.name = control_label

            # HACK: reduce bloating of plt function - could replace with functools.partial
            def plot(
                spl: DataFrame,
                color: str,
                datapoint: bool = False,
            ) -> Any:
                """Ad hoc closure to plot the data."""
                if not datapoint:
                    return plot_hist_err(
                        data=spl[var],
                        bins=bins,
                        range=vars[var],  # value provides the appropriate range
                        ax=ax,
                        wts=spl[weights_var],
                        label=spl.name,
                        _isnorm=True,
                        color=color,
                    )
                if datapoint:
                    return plot_data(
                        data=spl[var],
                        bins=bins,
                        range=vars[var],  # value provides the appropriate range
                        ax=ax,
                        wts=spl[weights_var],
                        label=spl.name,
                        col=color,
                        isnorm=True,
                    )

            samples = (self._data, self._control)
            colours = ("darkorange", "black")

            # select control region
            for spl in samples:
                if cut_str is not None:
                    spl = spl[spl[cut_str]]

            # plot the data
            [
                plot(spl, colour, pts)
                for spl, colour, pts in zip(samples, colours, (False, True))
            ]

            # set the labels and save
            ax.legend()
            ax.set_ylim(bottom=0)
            ax.set_title(title)
            if xlabel is not None:
                ax.set_xlabel(xlabel)
            else:
                ax.set_xlabel(var.replace("_", "-"))
            ax.set_ylabel(ylabel)

            Path(f"{plot_path}").mkdir(parents=True, exist_ok=True)  # book the dir
            [fig.savefig(f"{plot_path}/{var}.{ext}") for ext in ("png", "pdf")]
            plt.close(fig)


def allocate_axs(tot: int, cols: int) -> Tuple[Any, ...]:
    """Allocate the axes for the plots.

    Parameters
    ----------
    tot: int
        Total number of plots.

    cols: int
        Number of columns in the plot grid.

    Returns
    -------
    axs: object
    """
    return plt.subplots(
        nrows=tot // cols,
        ncols=cols,
        sharex=False,
        sharey=True,
        squeeze=False,
        figsize=(cols * 5, tot // cols * 4.5),
    )


def inspect_mcorr_reaction(
    sel_conds: ArrayLike,
    probe_var: str,
    df: DataFrame,
    cut_var: Union[str, List[str]] = "B_plus_M",
    probe_range: Union[Tuple[float, float], None] = (3_000, 12_000),
    plot_path: str = "/home/blaised/private/Bc2D0MuNuX/ws/plots/inspect_mcorr_reaction",
    hi_lev_sel: Union[str, None] = "B_plus_M>3_500",
    force_low_nbins: bool = False,
    control: DataFrame = DataFrame(),
    common_sel: str = "B_plus_M>0",
    fig_rows: int = 2,
    cuttype: str = ">",
    plot_mc: bool = False,
    logy: bool = False,
    corr_w: Union[str, None] = None,
    control_label: Union[str, None] = "DCS",
    ws_label: Union[str, None] = "WS",
    suffix: Union[str, None] = "",
    leg_cols: int = 2,
) -> None:
    """Inspect how a probe observable distribution reacts a result of a given cut(s) in the data;
    Generates plot(s) of the distribution of the probe observable in the data after the cut(s).

    Parameters
    ----------
    sel_conds: Union[str, List[str]]
        Selection condition(s) to use for the cut [requires `cut_var>sel_conds`]

    probe_var: str
        Alias for the probe variable whose distribution is to be inspected

    df: DataFrame
        Data/MC sample

    cut_var: Union[str, List[str]]
        Alias for the cut variable(s) to use for the cut

    probe_range: Union[Tuple[float, float], None]
        Range of the probe variable to use for the plot

    plot_path: str
        Path to the directory to save the plots to

    hi_lev_sel: Union[str, None]
        Selection condition to use for the high-level selection stage

    force_low_nbins: bool
        Force the number of bins to be 50 [default: False]

    control: Union[None, DataFrame]
        If None, include DCS sW'd hist

    common_sel: str
        If None, include common selection cuts for all hists [default: "B_plus_M>0"]

    fig_rows: int
        Number of rows in the figure grid [default: 2]

    cuttype: str
        Type of cut to use [default: ">"]

    plot_mc : bool
        Plot the MC [default: False]

    logy: bool
        Plot the y-axis on a log scale [default: False]

    corr_w: Union[None, str]
        If not None, incomporare the GBR weights

    control_label: Union[str, None]
        If not None, assign label to the control sample (DCS, CF)

    ws_label: Union[str, None]
        If not None, assign label to the data sample [default: 'WS']

    suffix: Union[str, None]
        If not None, append the suffix to the plot name

    leg_cols: int
        Number of columns in the legend [default: 2]
    """

    # apply the common selection
    df = df.query(common_sel)
    if not control.empty:
        control = control.query(common_sel)
    if plot_mc:
        mc = read_pickle(
            "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/MC/combined/D0MuNu/5.x_invfb/Bc2D0MuNuX_xgb.pkl"
        ).query(common_sel)

    # book the selection cut(s) in the form of a list
    if isinstance(sel_conds, float):
        sel_conds = [sel_conds]

    # book the number of axes, based on the number of cuts
    fig, axs = allocate_axs(len(sel_conds), fig_rows)
    # bookeeping: match the shapes of the axes to the number of cuts
    sel_conds = np.array(sel_conds)
    sel_conds = sel_conds.reshape(axs.shape)

    print("Booked hi-lev selection:", hi_lev_sel)
    for iy, ix in np.ndindex(axs.shape):

        # select the data after the cut
        assert cuttype in (">", "leq"), "Cut type must be either '>' or 'leq'"
        cutval = sel_conds[iy, ix]  # sanity check

        if cuttype == "leq":
            df_sel = df[df[cut_var] <= cutval]
            if not control.empty:
                control_sel = control[control[cut_var] <= cutval]
            if plot_mc:
                mc_sel = mc[mc[cut_var] <= cutval]
        if cuttype == ">":
            df_sel = df[df[cut_var] > cutval]
            if not control.empty:
                control_sel = control[control[cut_var] > cutval]
            if plot_mc:
                mc_sel = mc[mc[cut_var] > cutval]

        # hacky handling of bins
        if probe_range[1] > 5000:
            bs = int((probe_range[1] - probe_range[0]) // 100)
        else:
            bs = 30

        if force_low_nbins == True:
            bs = 25

        # split the ax to add pull plot below
        axi = axs[iy, ix]
        divider = make_axes_locatable(axi)
        axi1 = divider.new_vertical(size="400%", pad=0.1)
        fig = axi.get_figure()
        fig.add_axes(axi1)

        # plot the distribution of the probe variable in the data after the cut
        data_z_order = 3 if plot_mc else 2
        d_nh, d_cx, d_err = plot_data(
            data=df_sel[probe_var],
            bins=bs,
            range=probe_range,
            ax=axi1,
            isnorm=True,
            label=ws_label,
            wts=df_sel["sw"],
            zorder=data_z_order,
        )

        # plot the control data
        h_nh, h_cx, h_err = plot_hist_err(
            data=control_sel[probe_var],
            bins=bs,
            range=probe_range,
            ax=axi1,
            _isnorm=True,
            color="lightgrey",
            histtype="fill",
            alpha=1,
            zorder=0,
            label=control_label,
            wts=control_sel["sw"],
        )

        # HACK to overwrite the pulls
        _h_nh = h_nh
        _h_cx = h_cx
        _h_err = h_err

        # apply the prompt-only selection; overwrite the values employed to compute the pulls
        if hi_lev_sel is not None:
            _df = df_sel.query(hi_lev_sel)
            h_nh, h_cx, h_err = plot_hist_err(
                data=_df[probe_var],
                bins=bs,
                range=probe_range,
                ax=axi1,
                _isnorm=True,
                label=r"WS log$(D \text{IP} \chi^2 )< 3$",
                color="tab:red",
                wts=_df["sw"],
                zorder=1,
            )

        if corr_w is not None:
            print("===== INCORPORATING GBR WEIGHTS IN THE PLOT =====")
            assert (
                df_sel.D0_IPCHI2_OWNPV.max() < 3
            ), "ConsistencyError: check that D0IPCHI2 cut is applied"
            _df = df_sel
            h_nh, h_cx, h_err = plot_hist_err(
                data=_df[probe_var],
                bins=bs,
                range=probe_range,
                ax=axi1,
                _isnorm=True,
                label=r"WS log$(D~\text{IP} \chi^2)< 3$",
                color="tab:blue",
                wts=_df[corr_w],
            )

            # HACK: overwrite pull calculation to compute between the GBR and the DCS
            d_nh = _h_nh
            d_cx = _h_cx
            d_err = _h_err

        if plot_mc:
            _ = plot_hist_err(
                data=mc_sel[probe_var],
                bins=bs,
                range=probe_range,
                ax=axi1,
                _isnorm=True,
                color="#e66101",
                histtype="step",
                lw=1.1,
                ls="--",
                label=r"Signal",
            )

        # generate the pulls
        pulls = np.nan_to_num((d_nh - h_nh) / np.sqrt(d_err**2 + h_err**2))
        axi.bar(
            x=d_cx,
            height=pulls,
            width=d_cx[1] - d_cx[0],
            color="black",
        )
        axi.axhline(0, color="black", linestyle="-", lw=0.5)
        axi.axhline(3, color="black", linestyle=":", lw=1)
        axi.axhline(-3, color="black", linestyle=":", lw=1)
        axi.set_ylim(-5, 5)
        if logy == True:
            axi1.set_yscale("log")
        else:
            axi1.set_ylim(bottom=0)
        axi1.get_shared_x_axes().join(axi, axi1)
        axi1.set_xticklabels([])

        tex_common_sel = (
            "$"
            + common_sel.replace("0.002 and", "0.002$ \n $")
            .replace("XGB_antiComb", "\chi_{comb}")
            .replace("and", ",")
            .replace("XGB_antiVcb", "\chi_{V_{cb}}")
            .replace("B_plus_M", "m(D\mu^+)")
            .replace("<=", "\\leq")
            .replace("\t", "")
            .replace("B_plus_FIT_LTIME", "\\tau(X_b)")
            + "$"
        )
        # add the cut value
        axi1.text(
            0.0,
            1.25,
            tex_common_sel,
            horizontalalignment="left",
            verticalalignment="center",
            transform=axi1.transAxes,
        )

        if cuttype == "leq":
            cut_leg = "\\leq"
        else:
            cut_leg = cuttype
        axi1.text(
            0.0,
            1.15,
            rf"\textbf{{{cut_var.replace('_','-')}}}  ${{{cut_leg}}}$ \textbf{{{cutval}}}",
            horizontalalignment="left",
            verticalalignment="center",
            transform=axi1.transAxes,
            color="black",
        )

        if iy == fig_rows - 1:
            axi1.legend(
                loc="center", bbox_to_anchor=(0.5, -0.55), ncol=leg_cols, frameon=True
            ).get_frame().set_edgecolor("white")

        axi1.set_title(r"LHCb Unofficial 5.4 fb$^{-1}$ (13 TeV)")
        axi.set_xlabel(probe_var.replace("_", "-"))
        axi1.set_ylabel(f"Candidates / {d_cx[1]-d_cx[0]:.2f}")
        axi.set_ylabel(r"Pulls $[\sigma]$")

    # make space for the cuts
    if fig_rows > 1:
        fig.subplots_adjust(hspace=0.5)

    # save the plots & close the figure
    Path(f"{plot_path}").mkdir(parents=True, exist_ok=True)  # book the dir
    [
        plt.savefig(
            f"{plot_path}/{probe_var}_distribution_after_{cut_var}{suffix}.{ext}"
        )
        for ext in ("png", "pdf")
    ]
    plt.close(fig)


if __name__ == "__main__":

    __storage_path = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch"

    # formulate hi-lev sel
    HI_LEV_SEL = sig_hi_base_sel(
        _visM_inf=0.0,
        _visM_sup=10_000,  # stripping
        _k_antiComb=0.0,
        _k_antiVcb=0.0,
    ).build()  # hi-level sel

    # load the relevant samples
    ws = read_pickle(
        f"{__storage_path}/nominal/DATA/combined/D0MuNu/9.x_invfb/Bc2D0MuNu_ws_sw_xgb.pkl"
    )
    dcs = read_pickle(
        f"{__storage_path}/nominal/DATA/combined/D0MuNu/9.x_invfb/Bc2D0MuNu_dcs_sw_xgb.pkl"
    )
    # cf = read_pickle(
    #     f"{__storage_path}/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_cf_sw_xgb.pkl"
    # )
    misid = read_pickle(
        "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_misid_hadron_enriched_misid_sw_xgb.pkl",
    )
    # # =================== CONTROL REGIONS ===================
    # # visM
    # inspect_mcorr_reaction(
    #     sel_conds=[6_400],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(6_000, 12_000),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     fig_rows=1,
    #     ws_label="WS",
    #     suffix="_ctrl",
    #     plot_mc=True,
    #     leg_cols=2,
    #     logy=False,
    # )

    # # CosXY
    # inspect_mcorr_reaction(
    #     sel_conds=[-0.8],
    #     cut_var="CosXY_Mu_plus_D0",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(0, 12_000),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype="leq",
    #     fig_rows=1,
    #     ws_label="WS",
    #     suffix="_ctrl",
    #     plot_mc=True,
    #     leg_cols=2,
    #     logy=False,
    # )

    # # for completeness, show the high-mass D0 IPCHI2
    # inspect_mcorr_reaction(
    #     sel_conds=[6_400],
    #     cut_var="B_plus_M",
    #     probe_var="D0_IPCHI2_OWNPV",
    #     probe_range=(0, 10),
    #     df=ws,
    #     hi_lev_sel=None,
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     fig_rows=1,
    #     ws_label="WS",
    #     suffix="_ctrl",
    #     plot_mc=False,
    #     leg_cols=2,
    #     logy=False,
    # )

    # # for completeness, show the high-mass D0 IPCHI2
    # inspect_mcorr_reaction(
    #     sel_conds=[6_400],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_PT",
    #     probe_range=(0, 10),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     fig_rows=1,
    #     ws_label="WS",
    #     suffix="_highVisM",
    #     plot_mc=False,
    #     leg_cols=3,
    #     logy=False,
    # )

    # # for completeness, show the high-mass D0 IPCHI2
    # inspect_mcorr_reaction(
    #     sel_conds=[6_400],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_P",
    #     probe_range=(0, 300000),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=True,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     fig_rows=1,
    #     ws_label="WS",
    #     suffix="_highVisM",
    #     plot_mc=False,
    #     leg_cols=3,
    #     logy=False,
    # )

    # # =================== PLOT THE VISM INTERVALS ===================
    # inspect_mcorr_reaction(
    #     sel_conds=[0, 3_500, 5_200, 6_400],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(3_000, 18_000),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     fig_rows=2,
    #     ws_label="WS",
    #     suffix="_cycle",
    #     plot_mc=False,
    #     leg_cols=3,
    #     logy=False,
    # )

    # # =================== PLOT THE CosXY INTERVALS ==================
    # inspect_mcorr_reaction(
    #     sel_conds=[-0.8, -0.4, 0.0, 1.0],
    #     cut_var="CosXY_Mu_plus_D0",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(3_000, 18_000),
    #     df=ws,
    #     hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #     force_low_nbins=False,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype="leq",
    #     fig_rows=2,
    #     ws_label="WS",
    #     suffix="_cycle",
    #     plot_mc=False,
    #     leg_cols=3,
    #     logy=False,
    # )

    # # plot the high-mass WS pops
    # ws_obs = (
    #     {
    #         "probe_var": "B_plus_MCORR",
    #         "probe_range": (3_000, 18_000),
    #         "suffix": "_hiVisM",
    #     },
    #     {
    #         "probe_var": "B_plus_MCORR",
    #         "probe_range": (4_200, 7_200),
    #         "suffix": "_hiVisM",
    #     },
    #     {
    #         "probe_var": "B_plus_MCORRERR",
    #         "probe_range": (0, 2_000),
    #         "suffix": "_hiVisM",
    #     },
    #     {"probe_var": "B_plus_PT", "probe_range": (0, 10), "suffix": "_hiVisM"},
    #     {
    #         "probe_var": "B_plus_P",
    #         "probe_range": (0, 300_000),
    #         "force_low_nbins": True,
    #         "suffix": "_hiVisM",
    #     },
    #     {
    #         "probe_var": "B_plus_FIT_LTIME",
    #         "probe_range": (0, 0.002),
    #         "suffix": "_hiVisM",
    #     },
    #     {
    #         "probe_var": "B_plus_FDCHI2_OWNPV",
    #         "probe_range": (0, 10),
    #         "suffix": "_hiVisM",
    #     },
    #     {
    #         "probe_var": "B_plus_IPCHI2_OWNPV",
    #         "probe_range": (0, 10),
    #         "suffix": "_hiVisM",
    #     },
    # )
    # for obs in ws_obs:
    #     inspect_mcorr_reaction(
    #         sel_conds=[0],
    #         cut_var="B_plus_M",
    #         df=ws,
    #         hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #         control=dcs,
    #         common_sel=HI_LEV_SEL,
    #         cuttype=">",
    #         fig_rows=1,
    #         ws_label="WS",
    #         plot_mc=False,
    #         leg_cols=3,
    #         logy=False,
    #         **obs,
    #     )
    # breakpoint()
    # for obs in ws_obs:
    #     inspect_mcorr_reaction(
    #         sel_conds=[-0.8],
    #         cut_var="CosXY_Mu_plus_D0",
    #         df=ws,
    #         hi_lev_sel="D0_IPCHI2_OWNPV < 3",
    #         control=dcs,
    #         common_sel=HI_LEV_SEL,
    #         cuttype="leq",
    #         fig_rows=1,
    #         ws_label="WS",
    #         plot_mc=False,
    #         leg_cols=3,
    #         logy=False,
    #         **obs,
    #     )

    # breakpoint()

    # initialise the reweighter
    gbr = BinaryReweighter(
        control=dcs.query(f"{HI_LEV_SEL}"),
        data=ws.query(f"{HI_LEV_SEL} and D0_IPCHI2_OWNPV<3"),
        probe_vars=["B_plus_P", "B_plus_PT"],
        spectator_vars=["B_plus_MCORR", "B_plus_PT", "B_plus_M"],
        training_vars=[
            "B_plus_P",
        ],
        training_window="B_plus_M>6400",  # upper DCS mass
    )

    # execute reweighting
    gbr_ws = gbr.ws_gbr

    # write to scratch
    gbr_ws.to_pickle(
        f"{__storage_path}/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_GBR_PC_ws_sw_xgb.pkl"
    )

    # formulate hi-lev sel
    test_sel = sig_hi_base_sel(
        _visM_inf=3_500,
        _visM_sup=6285,  # stripping
        _k_antiComb=0.5,
        _k_antiVcb=0.375,
    ).build()  # hi-level sel

    fig, ax = plt.subplots()
    ax.hist(
        gbr_ws.query(f"{test_sel}")["B_plus_MCORR"],
        weights=gbr_ws.query(f"{test_sel}")["gbr_sw"],
        bins=30,
        range=[4_200, 7_200],
        density=True,
        histtype="step",
        label=rf"WS weighted",
    )
    ax.legend()

    ax.hist(
        gbr_ws.query(f"{test_sel}")["B_plus_MCORR"],
        weights=gbr_ws.query(f"{test_sel}")["sw"],
        bins=30,
        range=[4_200, 7_200],
        density=True,
        histtype="step",
        label=rf"WS sW",
    )
    ax.legend()

    ax.hist(
        gbr_ws.query(f"{test_sel}")["B_plus_MCORR"],
        bins=30,
        range=[4_200, 7_200],
        density=True,
        histtype="step",
        label=rf"WS un-weighted",
    )
    ax.legend()
    plt.savefig("/home/blaised/private/Bc2D0MuNuX/ws/test_invWS.png")

    # # post high-level selection
    # nominal_hi_lev_sel = sig_hi_base_sel(
    #     _visM_inf=3_500,
    #     _visM_sup=8_000,  # stripping
    #     _k_antiComb=0.2,
    #     _k_antiVcb=0.375,
    # ).build()  # hi-level sel

    # inspect_mcorr_reaction(
    #     sel_conds=[3_500],
    #     cut_var="B_plus_M",
    #     df=gbr_ws,  # train and only then cut
    #     hi_lev_sel=None,
    #     control=ws,
    #     common_sel=nominal_hi_lev_sel,
    #     cuttype=">",
    #     fig_rows=1,
    #     ws_label=r"GBR WS $\log(D ~\textrm{IP} \chi^2)<3$",
    #     control_label=r"WS",
    #     corr_w="gbr_sw",  # include the GBR correction
    #     plot_mc=False,
    #     leg_cols=2,
    #     logy=False,
    #     probe_var="B_plus_MCORR",
    #     probe_range=(4_200, 7_200),
    #     force_low_nbins=False,
    # )

    # inspect changes in shape with comb MVA cut

    # # plot results
    # postgbr_obs = (
    #     {"probe_var": "B_plus_MCORR", "probe_range": (3_000, 18_000), "suffix": "_gbr"},
    #     {
    #         "probe_var": "B_plus_MCORR",
    #         "probe_range": (4_200, 7_200),
    #         "suffix": "_fitrange_gbr",
    #     },
    #     {"probe_var": "B_plus_MCORRERR", "probe_range": (0, 2_000), "suffix": "_gbr"},
    #     {"probe_var": "B_plus_PT", "probe_range": (0, 10), "suffix": "_gbr"},
    #     {
    #         "probe_var": "B_plus_P",
    #         "probe_range": (0, 300_000),
    #         "force_low_nbins": True,
    #         "suffix": "_gbr",
    #     },
    #     {"probe_var": "B_plus_FIT_LTIME", "probe_range": (0, 0.002), "suffix": "_gbr"},
    #     {"probe_var": "B_plus_FDCHI2_OWNPV", "probe_range": (0, 10), "suffix": "_gbr"},
    #     {"probe_var": "B_plus_IPCHI2_OWNPV", "probe_range": (0, 10), "suffix": "_gbr"},
    # )
    # for obs in postgbr_obs:
    #     inspect_mcorr_reaction(
    #         sel_conds=[0],
    #         cut_var="B_plus_M",
    #         df=gbr_ws,
    #         hi_lev_sel=None,
    #         control=dcs,
    #         common_sel=HI_LEV_SEL,
    #         cuttype=">",
    #         fig_rows=1,
    #         ws_label=r"WS $\log(D ~\textrm{IP} \chi^2)<3$",
    #         corr_w="gbr_sw",  # include the GBR correction
    #         plot_mc=False,
    #         leg_cols=3,
    #         logy=False,
    #         **obs,
    #     )
    # breakpoint()

    # # for obs in postgbr_obs:
    # #     inspect_mcorr_reaction(
    # #         sel_conds=[-0.8],
    # #         cut_var="CosXY_Mu_plus_D0",
    # #         df=gbr_ws,
    # #         hi_lev_sel=None,
    # #         control=dcs,
    # #         common_sel=HI_LEV_SEL,
    # #         cuttype="leq",
    # #         fig_rows=1,
    # #         ws_label=r"WS $\log(D ~\textrm{IP} \chi^2)<3$",
    # #         corr_w="gbr_sw",  # include the GBR correction
    # #         plot_mc=False,
    # #         leg_cols=3,
    # #         logy=False,
    # #         **obs,
    # #     )
    # # breakpoint()

    # # =================== PLOT THE HIGH-MASS MCORR ===================
    # inspect_mcorr_reaction(
    #     sel_conds=[-0.8],
    #     cut_var="CosXY_Mu_plus_D0",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(3_000, 18_000),
    #     df=gbr_ws,
    #     hi_lev_sel=f"D0_IPCHI2_OWNPV<3",
    #     force_low_nbins=True,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype="leq",
    #     plot_mc=False,
    #     fig_rows=1,
    #     corr_w="gbr_sw",  # include the GBR correction
    #     suffix="_gbr",
    # )

    # # =================== PLOT THE HIGH-MASS MCORR ===================
    # inspect_mcorr_reaction(
    #     sel_conds=[0],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(3_000, 10_000),
    #     df=gbr_ws,
    #     hi_lev_sel=f"D0_IPCHI2_OWNPV<3",
    #     force_low_nbins=True,
    #     control=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     plot_mc=False,
    #     fig_rows=1,
    #     corr_w="gbr_sw",  # include the GBR correction
    #     suffix="_gbr_check",
    # )

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # # what happens when you run the full selection
    # post_sel = sig_hi_base_sel(
    #     _visM_inf=3_500,
    #     _visM_sup=8_000,  # stripping
    #     _k_antiComb=0.2,
    #     _k_antiVcb=0.375,
    # ).build()  # hi-level sel

    # # =================== PLOT THE HIGH-MASS MCORR ===================
    # inspect_mcorr_reaction(
    #     sel_conds=[0],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(4_200, 7_200),
    #     df=gbr_ws.query(post_sel),
    #     hi_lev_sel=f"D0_IPCHI2_OWNPV<3",
    #     force_low_nbins=True,
    #     control=dcs.query(post_sel),
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     plot_mc=False,
    #     fig_rows=1,
    #     corr_w="gbr_sw",  # include the GBR correction
    #     suffix="_gbr_check_postsel",
    # )

    # # =================== PLOT THE HIGH-MASS PT ===================
    # inspect_mcorr_reaction(
    #     sel_conds=[6_400],
    #     cut_var="B_plus_M",
    #     probe_var="B_plus_MCORR",
    #     probe_range=(3_000, 12_000),
    #     df=gbr_ws,
    #     hi_lev_sel=f"D0_IPCHI2_OWNPV<3",
    #     force_low_nbins=False,
    #     dcs=dcs,
    #     common_sel=HI_LEV_SEL,
    #     cuttype=">",
    #     plot_mc=False,
    #     fig_rows=1,
    #     corr_w="gbr_sw",  # include the GBR correction
    # )

    # # # # perform comparison
    # # # gbr.viz_compare(
    # # #     vars = {
    # # #         "B_plus_PT": (0, 10), # scaled to GeV
    # # #         "B_plus_MCORR": (0, 12_000),
    # # #         "B_plus_IPCHI2_OWNPV": (0, 10),
    # # #         "D0_IPCHI2_OWNPV": (0, 10),
    # # #         "B_plus_M": (0, 8_000),
    # # #     },

    # # # )
