"""Derive the combonbinatorial fit template.

This study relies on three sequential steps:
- fit the GBR PC WS data, to learn 
"""
import pandas as pd
import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")  # XXX: temporary
from utils import plot_super
from utils.plot_super import plot_hist_err, plot_data
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

plt.style.use(["science", "high-vis"])
from mpl_toolkits.axes_grid1 import make_axes_locatable

__author__ = ["Blaise Delaney"]
__email__ = ["blaise.delaney at cern.ch"]

# load the GBR PC WS data
gbr_pc_ws = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_GBR_PC_ws_sw_xgb.pkl"
)

# plot
fig, ax = plt.subplots()
plot_data(
    ax=ax,
    data=gbr_pc_ws.D0_M,
    wts=gbr_pc_ws.gbr_sw,
    bins=30,
    range=[1790, 1940],
)
ax.set_xlabel(r"WS PC $m_{corr}(D^0 \mu^+)$ [MeV$/c^2$]")
ax.set_ylabel("")

plt.savefig(f"/home/blaised/private/Bc2D0MuNuX/ws/plots/misid/gbr_pc_ws_mass.png")
