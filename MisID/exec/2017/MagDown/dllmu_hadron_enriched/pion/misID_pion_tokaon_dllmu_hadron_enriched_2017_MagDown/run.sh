export PATH=$(getconf PATH)
export HOME=/usera/delaney
export X509_USER_PROXY=/usera/delaney/.grid.proxy
source /lhcb/scripts/lhcb-setup.sh
lb_set_platform x86_64-centos7-gcc9-opt
/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/run python /usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py "Turbo17" "MagDown" "Pi" "MC15TuneV1_ProbNNghost<0.2 && DLLK>0.0 && (DLLK-DLLp)>0.0 && (DLLK-DLLe)>0.0 && DLLmu<0 && IsMuon==0" "Brunel_P" "Brunel_ETA" "nTracks_Brunel" --binSchemeFile="/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/python/PIDPerfScripts/custom_binning.py" --schemeName="hadron_enriched_run2_!2016" --outputDir="/usera/delaney/private/Bc2D0MuNuX/MisID/exec/2017/MagDown/dllmu_hadron_enriched/pion/misID_pion_tokaon_dllmu_hadron_enriched_2017_MagDown" -c "InMuonAcc==1.0 && nShared==0 && Brunel_P>10000 && Brunel_P<100000 && Brunel_PT>1500"  && 
touch exec/2017/MagDown/dllmu_hadron_enriched/pion/misID_pion_tokaon_dllmu_hadron_enriched_2017_MagDown/pidcalib.done && 
for f in /usera/delaney/private/Bc2D0MuNuX/MisID/exec/2017/MagDown/dllmu_hadron_enriched/pion/misID_pion_tokaon_dllmu_hadron_enriched_2017_MagDown/*.root; do
mv "$f" /usera/delaney/private/Bc2D0MuNuX/MisID/exec/2017/MagDown/dllmu_hadron_enriched/pion/misID_pion_tokaon_dllmu_hadron_enriched_2017_MagDown/perfHist.root
done
                    
source /usera/delaney/miniconda3/bin/activate /usera/delaney/miniconda3/envs/FITENV