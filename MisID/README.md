# Pipeline for the retrival of PIDCalib-derived efficiency values

This workflow is controlled by a `Snakefile`. This pipeline may be run through the conda environment adopted throughout the entire analysis. The conda env activated using the following spec: `/usera/delaney/private/Bc2D0MuNuX/Conda/Requirements.yml`. The user may run the snake routine on Condor using the usual command `snakemake --cluster /usera/delaney/private/Bc2D0MuNuX/Conda/snakemake/wrappers/condor_wrapper.py`.

## Requirments
1. A Urania build within this directory on your local machine. The Cambridge build uses Urania `v8r0` with a minor hack sugested by Vitalii to address changes implemented centrally to port Urinia to `v9rXYZ`. The entire `Urania` directory has been added to the `.gitignore`.

2. Please note that you may find the current version of `custom_binning.py` in the `python/` directory. Should you wish to amend this or add a new scheme, you will need to copy it over to `Urania_vxry/PIDCalib/PIDPerfScripts/python/PIDPerfScripts/`. After a re-build of Urania is necessary through `make purge`, `make configure` and `make install`. See PICalib twiki for further details. _I may try to implement an automation of this step in the pipeline at some stage, likely closer to when we come evaluating systematics_. 

## Workflow:

To be filled in detail.