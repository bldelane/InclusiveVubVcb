'''Executable to write to hadron-enriched DCS data the information needed to fully unfold the 
hadron, ghost and electron abundances in data.

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from root_pandas import to_root
import time
start = time.time()

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"]
    )
parser.add_argument(
    '-t',
    '--test',
    help='Test with only 1K events',
    action="store_true"
    )
parser.add_argument(
    '-x',
    '--magcheck',
    help='Test with only 10 events',
    action="store_true"
    )
parser.add_argument(
    '-s',
    '--smallrun',
    help='Test ~50K events',
    action="store_true"
    )
opts = parser.parse_args()


def compute_misID_w(
    PID_hist,
    muonID_hist,
    L0GlobTIS_hist,
    data_P, data_ETA, data_nTracks, data_PT, data_fakemuon_w, B_P, B_PT,
    year,
    magpol,
    # gtomu_eff_hist, GHOST REMOVAL, as assumed that only kaon, pion, proton contribute
    tofit_pid_path=None
    ):
    '''function to calculate the misID weights exploiting the UHI+ syntax of Hist
    params:
    -------
    PID_hist : histgram holding the #events in hadron-enriched data, PID template pdf integrals and extracted per-species abundances
    muonID_hist : eff(isMuon && L0MuonTOS) histogram
    L0GlobTIS_hist : eff(B_L0GlobalTIS) histogram, for unfolding
    tofit_pid_dir: where to look up the PID eff of DLLmu>3 when extrapolating to signal window
    data: hadron-enriched dataframe
    gtomu_eff_hist : g->mu PID eff hist from inclusive MC
    year & magpol'''

    w_pid_dict = {}
    if opts.test is True:
        print(c("\n----------------------------------------------").magenta.bold)
        print(c(f"Kinematics and multiplicity coordinates - {magpol}").magenta.bold)
        print(c("-----------------------------------------------").magenta.bold)
        print(c(f"P : {data_P}").magenta)
        print(c(f"PT : {data_PT}").magenta)
        print(c(f"ETA : {data_ETA}").magenta)
        print(c(f"nTracks : {data_nTracks}").magenta)
        print(c(f"fakemuon_w : {data_fakemuon_w}").magenta)
        print(c("\nStart by unfolding the PID categories").magenta.underline)
    
    # NOTE: this is a function of the number of events to which the weights are applied!
    NORM = PID_NORM_HIST[
        hist.loc(magpol),
        hist.loc(data_P), 
        hist.loc(data_ETA), 
        hist.loc(data_nTracks), 
        ].value  

    # # ======== WRONG ===========
    # NORM = PID_hist[
    #     hist.loc(data_P), 
    #     hist.loc(data_ETA), 
    #     hist.loc(data_nTracks), 
    #     "kaon",
    #     "reco_nevents"
    #     ].value  
    # #============================

    #print(c(f"Event coordinates: P={data['Mu_plus_P']}, ETA={data['Mu_plus_ETA']}, nTr={data['nTracks']}"))
    #for ps in ["kaon", "pion", "proton", "electron", "ghost"]: # get these from pidcalib
    for ps in ["kaon", "pion", "proton"]: # get these from pidcalib
        
        if opts.test is True:
            print(c(f"\nspecies: {ps}").magenta.bold)

        if opts.test is True:
            print(c(f"NORM events: {NORM}").magenta)

        fit_yield = PID_hist[
            hist.loc(data_P), 
            hist.loc(data_ETA), 
            hist.loc(data_nTracks), 
            ps,
            "unfolded_yield"
            ].value  
        if opts.test is True:
            print(c(f"fit y : {fit_yield}").magenta)

        pdf_integral = PID_hist[
            hist.loc(data_P), 
            hist.loc(data_ETA), 
            hist.loc(data_nTracks), 
            ps,
            "integr_PID_template"
            ].value  
        if opts.test is True:
            print(c(f"pdf integral : {pdf_integral}").magenta)
        
        # PIDmu>3 eff take from PIDCalib for non-ghosts and inclusive MC for ghosts
        if ps != "ghost":
            with open(tofit_pid_path+"/"+year+"/"+magpol+"/muonID_fit/"+ps+"/misID_"+ps+"_tomuon_muonID_fit_"+year+"_"+magpol+"/eff_th3w.pkl", "rb") as ffit: 
                hist_tofit = pickle.load(ffit)
            tomu_eff = hist_tofit[
                hist.loc(data_P), 
                hist.loc(data_ETA), 
                hist.loc(data_nTracks), 
                ].value  
            if opts.test is True:
                print(c(f"DLLmu>3 & isMuon & ProbNNghost <.2 eff : {tomu_eff}").magenta)
        
        # if ps == "ghost":
        #     if opts.test is True:
        #         print(c("Note: sourcing eff(g->mu) from PID calibration samples").red)
            
        #     tomu_eff = gtomu_eff_hist[
        #         hist.loc(data_P), 
        #         hist.loc(data_ETA), 
        #         hist.loc(data_nTracks), 
        #         ]
            # if opts.test is True:
            #     print(c(f"DLLmu>3 & isMuon eff : {tomu_eff}").magenta)

        # derive the w_PID term by performing a weighted sum over all species
        if pdf_integral==0.0: 
            w_pid_dict[ps] = 0.0 # HACK: something is wrong with the PDF integral
        else:
            w_pid_dict[ps] = (fit_yield)*(1./pdf_integral)*tomu_eff
    if NORM != 0.0:
        w_PID = 1./NORM * np.sum(list(w_pid_dict.values()))
    if NORM == 0.0:
        w_PID = 0.0
        print(c("==== !!! WARNING: degenerate event !!! ===").red)

    if opts.test is True or opts.magcheck:
        print(c(f"\nExtracted PID per-event eff: \n{w_pid_dict}").red)
        print(c(f"Extracted PID weight = {w_PID}"))

    # now source the L0GlobTIS and muonID effs
    if opts.test is True:
        print(c("Now focus on L0GlobalTIS & eff(Mu_L0MuonTOS)").yellow.underline)
    
    eff_TIS = L0GlobTIS_hist[
            hist.loc(B_P), # parameterised as a fn of B momentum 
            hist.loc(B_PT), # parameterised as a fn of B transverse momentum 
            ].value  
    if opts.test is True:
        print(c(f"\neff TIS hist structure: {L0GlobTIS_hist}").yellow)
        print(c(f"\nMomentum lookup: B_P={B_P}, B_PT={B_PT}").yellow)
        print(c(f"\nL0 GlobTIS eff : {eff_TIS}").blue)        
    
    eff_muonID = muonID_hist[
            hist.loc(data_PT), 
            ].value  
    if opts.test is True:
        print(c(f"\neff muon ID hist structure: {muonID_hist}").yellow)
        print(c(f"\nMomentum lookup: Mu_PT={data_PT}").yellow)
        print(c(f"eff(L0MuonTOS) : {eff_muonID}").blue)

    #  putting it all together
    w_misID = w_PID * eff_muonID * (1./eff_TIS)

    if opts.test is True:
        print(c(f"\nmisID weight w/o prescale: {w_misID}").cyan)    
        print(c(f"misID weight * 1/prescale: {w_misID * data_fakemuon_w}").cyan)    
    return w_misID * data_fakemuon_w # absorb anti-prescale term here


def calc_gtomu_eff(
    binning,
    incl_MC
    ):
    '''function to compile a eff hist for g passing muonID selection to fit window
    from inclusive MC
    
    returns an efficiency histogram'''
    gtomu_hist = Hist(
        hist.axis.Variable(binning["p"],         name="p",         label=r"$p$   [MeV$/c$]"),
        hist.axis.Variable(binning["eta"],       name="eta",       label=r"$\eta$"),
        hist.axis.Variable(binning["ntracks"],   name="ntracks",   label="nTracks"),
        storage=hist.storage.Double()
    )

    p_bins   = gtomu_hist.project("p").axes[0].edges    
    eta_bins = gtomu_hist.project("eta").axes[0].edges    
    nTr_bins = gtomu_hist.project("ntracks").axes[0].edges 

    for p_idx in range(len(p_bins)-1):
        pb_lo = p_bins[p_idx]
        pb_hi = p_bins[p_idx+1]
        for eta_idx in range(len(eta_bins)-1):
            eb_lo = eta_bins[eta_idx]
            eb_hi = eta_bins[eta_idx+1]
            for nTr_idx in range(len(nTr_bins)-1):
                nb_lo = nTr_bins[nTr_idx]
                nb_hi = nTr_bins[nTr_idx+1]
    
                ghost_pop = incl_MC.query(f"(Mu_plus_P>={pb_lo} and Mu_plus_P<{pb_hi}) and \
                            (Mu_plus_ETA>={eb_lo} and Mu_plus_ETA<{eb_hi}) and \
                            (nTracks>={nb_lo} and nTracks<{nb_hi}) and Mu_plus_InMuonAcc==1.0 and Mu_plus_NShared==0")
                
                g_to_mu = len(ghost_pop.query("Mu_plus_PIDmu>3 and Mu_plus_isMuon==1.0"))
                gtomu_eff = g_to_mu/len(ghost_pop)
                
                # assign to hist & return
                gtomu_hist[p_idx, eta_idx, nTr_idx] = gtomu_eff

                return gtomu_hist


#=======================================================================================================
# bookkeping
YEAR   = opts.year
CONFIG_PATH = "/home/blaised/private/Bc2D0MuNuX/MisID/config.yml"
MAGPOLS = ["MagUp", "MagDown"]
CHANNEL = "D0MuNu"
if CHANNEL=="D0MuNu": charm_hadron="D0"
if CHANNEL=="JpsiMuNu": charm_hadron="Jpsi"
NAMESPACE = f"Bc2{CHANNEL}X"
istest = opts.test
smallrun = opts.smallrun
if istest: 
    MAXEVTS=5000 # debugging
    OUTFILE = f"Bc2{CHANNEL}X_misid_hadron_enriched.root"
if smallrun: 
    MAXEVTS=200 # local tuple to get a feel of distributions
    OUTFILE = f"Bc2{CHANNEL}X_misid_hadron_enriched.root"
else: 
    OUTFILE = f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/{CHANNEL}/{YEAR}/Bc2{CHANNEL}X_misid_hadron_enriched.root"
misid_data_prefix = f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/hadron_enriched/DATA/merged_misid/{CHANNEL}/{YEAR}"
L0GlobTIS_path = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/pkl/{YEAR}/L0GlobTIS_eff.pkl"
hadPID_prefix = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}" # split by polarity
muonID_prefix = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/muonID/{YEAR}" # split by polarity 
tofit_PID_path = f"/home/blaised/private/Bc2D0MuNuX/MisID/exec"

with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)
#=======================================================================================================

# ------------------------------------------------------------------------------------------------------------------
# load eff hists
# pid fits to hadron-enriched data
PID_eff_dict = {}
for m in MAGPOLS:
    with open(f"{hadPID_prefix}/{m}/pkl/hPID_eff_N_y.pkl", "rb") as hpid: PID_eff_dict[m] = pickle.load(hpid)

# L0MuonTOS
muonID_eff_dict = {}
for m in MAGPOLS:
    # with tag and probe for the moment we don't distinguish between magpols
    with open(f"{muonID_prefix}/pkl/MuonID_eff.pkl", "rb") as hmuid: muonID_eff_dict[m] = pickle.load(hmuid) #b2jpsik
    #with open(f"{muonID_prefix}/{m}/pkl/MuonID_eff.pkl", "rb") as hmuid: muonID_eff_dict[m] = pickle.load(hmuid) #L0muonTOS & isMuon, from MC
    #print(c(f"Read in the isMuon && L0MuonTOS eff histogram: {muonID_prefix}/{m}/pkl/MuonID_eff.pkl").cyan)

# L0GlobalTIS
with open(f"{L0GlobTIS_path}", "rb") as hl0: hGlobTIS = pickle.load(hl0)

# No longer consider ghosts
# # NOTE: uses the inclusive MC produced by Alison, of which we have only 2016 sample sat time of development + **these need PID correction** [an issue has been opened]
# inclMC_dict = {}
# for m in MAGPOLS:
#     inclMC_RDF = RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_{m}_5M.root").Filter(f"Mu_plus_TRUEID==0 && Mu_plus_MC_MOTHER_ID==0")
#     inclMC_dict[m] = pandas.DataFrame( inclMC_RDF.AsNumpy(columns=["Mu_plus_PIDe", "Mu_plus_PIDmu", "Mu_plus_PIDK", "Mu_plus_PIDp", "Mu_plus_MC15TuneV1_ProbNNghost", "Mu_plus_P", "Mu_plus_ETA", "nTracks", "Mu_plus_isMuon", "Mu_plus_InMuonAcc", "Mu_plus_NShared"]) ) 
# ------------------------------------------------------------------------------------------------------------------

in_branches = [
    "B_plus_M", 
    "B_plus_MCORR", 
    "B_plus_MCORRERR", 
    f"{charm_hadron}_M", 
    "B_plus_FIT_LTIME", 
    "fakemuon_w", 
    "Mu_plus_P", 
    "Mu_plus_PT", 
    "Mu_plus_ETA", 
    "D0_PT", 
    "D0_ETA", 
    "nTracks", 
    "K_minus_ID", 
    "Mu_plus_ID",
    "B_plus_P", 
    "B_plus_PT",
    "FitVar_Mmiss2",
    "FitVar_El",
    "B_plus_DOCACHI2_D0_Mu_plus",  
    "B_plus_DOCA_D0_Mu_plus",  
    "B_plus_ENDVERTEX_CHI2",  
    "B_plus_ETA",  
    "B_plus_FDCHI2_OWNPV",  
    "B_plus_IPCHI2_OWNPV",  
    "B_plus_ISOLATION_BDT",  
    "B_plus_OWNPV_CHI2",  
    "D0_DIRA_OWNPV",  
    "D0_FDCHI2_OWNPV",  
    "D0_IPCHI2_OWNPV",  
    "D0_ORIVX_CHI2",  
    "D0_OWNPV_CHI2",  
    "K_minus_IPCHI2_OWNPV",  
    "K_minus_PT",  
    "Mu_plus_IPCHI2_OWNPV",  
    "Mu_plus_ORIVX_CHI2",  
    "Pi_1_IPCHI2_OWNPV",  
    "Pi_1_PT",  
    "CosXY_Mu_plus_D0",  
    "CosXYZ_Mu_plus_Pi_1",  
    "CosXYZ_Mu_plus_K_minus", 
    "Mu_plus_ProbNNmu", 
    ]
if CHANNEL=="D0MuNu": in_branches+["K_minus_ID", "Mu_plus_ID"]

# blanket boundaries
B_MIN_P  = config["b2jpsik"]["B_MIN_P"]
B_MAX_P  = config["b2jpsik"]["B_MAX_P"]
B_MIN_PT = config["b2jpsik"]["B_MIN_PT"]
B_MAX_PT = config["b2jpsik"]["B_MAX_PT"]

# =============================================================================
# the high-level selection has been applied upstream, no need for cuts in loco
# -----------------------------------------------------------------------------
# massang cuts from pipeline
# angcuts =  "(CosXY_Mu_plus_D0>-0.4) & (CosXYZ_Mu_plus_Pi_1<0.9997) & (CosXYZ_Mu_plus_K_minus<0.9997) & (B_plus_DIRA_OWNPV>0.999)"
# D0cuts = "(D0_M >= 1810) & (D0_M <= 1920)"
# masscuts = "(B_plus_M>1864.83+105.6584) & (B_plus_M<6275.6) & (FitVar_Mmiss2>-5e6) & (B_plus_MCORRERR<650)"
# ltimecut = "(B_plus_FIT_LTIME>0)"
# SELECTION = f"{angcuts} & {D0cuts} & {masscuts} & {ltimecut} & (B_plus_P>{B_MIN_P}) & (B_plus_P<{B_MAX_P}) & (B_plus_PT>{B_MIN_PT}) & (B_plus_PT<{B_MAX_PT})"
# =============================================================================

SELECTION = f"(B_plus_P>{B_MIN_P}) & (B_plus_P<{B_MAX_P}) & (B_plus_PT>{B_MIN_PT}) & (B_plus_PT<{B_MAX_PT})"

# store per-polarity dataframes; require DCS sign K-mu sign combination
data_dict = {}

for m in MAGPOLS:
    if m == "MagUp":
        events = uproot.open(f"{misid_data_prefix}/MU/{NAMESPACE}.root:DecayTree")
    if m == "MagDown":
        events = uproot.open(f"{misid_data_prefix}/MD/{NAMESPACE}.root:DecayTree")
    data_dict[f"{m}"] = events.arrays(in_branches, cut=f"{SELECTION}", library="pd")
    print(c(f"# events read in: {m} : {len(data_dict[m])}").yellow)

if smallrun:
    print(c(f"\nWarning: running in SMALLRUN mode with {MAXEVTS} events").green)
    for m in MAGPOLS:
        if m == "MagUp":
            events = uproot.open(f"{misid_data_prefix}/MU/{NAMESPACE}.root:DecayTree")
        if m == "MagDown":
            events = uproot.open(f"{misid_data_prefix}/MD/{NAMESPACE}.root:DecayTree")
        data_dict[f"{m}"] = events.arrays(in_branches, cut=f"{SELECTION}", entry_stop=MAXEVTS, library="pd")
        print(c(f"# events read in: {m} : {len(data_dict[m])}").yellow)

if istest:
    print(c(f"\nWarning: running in TEST mode with {MAXEVTS} events").red)
    for m in MAGPOLS:
        if m == "MagUp":
            events = uproot.open(f"{misid_data_prefix}/MU/{NAMESPACE}.root:DecayTree")
        if m == "MagDown":
            events = uproot.open(f"{misid_data_prefix}/MD/{NAMESPACE}.root:DecayTree")
        data_dict[f"{m}"] = events.arrays(in_branches, cut=f"{SELECTION}", entry_stop=MAXEVTS, library="pd")
        print(c(f"# events read in: {m} : {len(data_dict[m])}").yellow)
        
if opts.magcheck:
    print(c(f"\nWarning: running in MAGCHECK mode with 5 events per polarity").red)
    for m in MAGPOLS:
        if m == "MagUp":
            events = uproot.open(f"{misid_data_prefix}/MU/{NAMESPACE}.root:DecayTree")
        if m == "MagDown":
            events = uproot.open(f"{misid_data_prefix}/MD/{NAMESPACE}.root:DecayTree")
        data_dict[f"{m}"] = events.arrays(in_branches, cut=f"{SELECTION}", entry_stop=100, library="pd")
        print(c(f"# events read in: {m} : {len(data_dict[m])}").yellow)

print(c("\nHadron-enriched data read in\n").bold)    
# sanity checks
for m in MAGPOLS:
    assert((data_dict[m]["K_minus_ID"]*data_dict[m]["Mu_plus_ID"]).all()>0) # only DCS
    print(c(f"{m}").underline)
    print("Expect only fake muons with anti-prescale w=50:\n", data_dict[m]["fakemuon_w"].value_counts()) # anti-prescale set to 1/0.02, expect only fakes to pass !isMuon due to StdNoPIDMuons 
    print(f"Range compatibility checks for {m}:")
    print(f"[min(charm_M)={np.min(data_dict[m]['D0_M'])} : max(charm_M)={np.max(data_dict[m]['D0_M'])}]")
    print(f"[min(B_M)={np.min(data_dict[m]['B_plus_M'])} : max(B_M)={np.max(data_dict[m]['B_plus_M'])}]")
    print(f"[min(P)={np.min(data_dict[m]['Mu_plus_P'])} : max(P)={np.max(data_dict[m]['Mu_plus_P'])}]")
    print(f"[min(PT)={np.min(data_dict[m]['Mu_plus_PT'])} : max(PT)={np.max(data_dict[m]['Mu_plus_PT'])}]")
    print(f"[min(ETA)={np.min(data_dict[m]['Mu_plus_ETA'])} : max(ETA)={np.max(data_dict[m]['Mu_plus_ETA'])}]")
    print(f"[min(nTr)={np.min(data_dict[m]['nTracks'])} : max(nTr)={np.max(data_dict[m]['nTracks'])}]")
    print() 
print(c("Success: passed all sanity checks\n").green)



# ===================================================================================================================
# need to establish the PID normalisation based on the number of events read in post-selection
with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)
binning = config["pid"]["binning"]

PID_NORM_HIST = Hist(
    hist.axis.StrCategory(["MagUp", "MagDown"], name="magpols"), 
    hist.axis.Variable(binning["p"],            name="p",         ),
    hist.axis.Variable(binning["eta"],          name="eta",       ),
    hist.axis.Variable(binning["ntracks"],      name="ntracks",   ),
    storage=hist.storage.Weight() # making life easier: store only central vals
)
print(c(f"Normalisation histogram intialised").yellow)

# now fill, based on slices of data in bins of kins and occpcy
for m in MAGPOLS:
    for u in range(len(binning["p"])-1):
        low_momentum_bin = binning["p"][u]
        high_momentum_bin = binning["p"][u+1]
        
        for v in range(len(binning["eta"])-1):
            low_eta_bin = binning["eta"][v]
            high_eta_bin = binning["eta"][v+1]
        
            for w in range(len(binning["ntracks"])-1):
                low_ntracks_bin = binning["ntracks"][w]
                high_ntracks_bin = binning["ntracks"][w+1]
                
                # now partition the data that we read in 
                per_bin_sel = f"Mu_plus_P>={low_momentum_bin} and Mu_plus_P<={high_momentum_bin} and Mu_plus_ETA>={low_eta_bin} and Mu_plus_ETA<={high_eta_bin} and nTracks>={low_ntracks_bin} and nTracks<={high_ntracks_bin}" 
                PID_NORM_HIST[m, u, v, w] = [ len( data_dict[m].query(per_bin_sel) ), np.sqrt( len(data_dict[m].query(per_bin_sel)) ) ] #poisson error

print(c(f"\nChecking normalisation histogram").red.bold)
print(c(f"[0,0,0] MagUp bin : {PID_NORM_HIST['MagUp', 0,0,0]}").red)
print(c(f"[0,0,0] MagDown bin : {PID_NORM_HIST['MagDown', 0,0,0]}").red)
print(c(f"[1,0,0] MagUp bin : {PID_NORM_HIST['MagUp', 1,0,0]}").red)
print(c(f"[1,0,0] MagDown bin : {PID_NORM_HIST['MagDown', 1,0,0]}").red)
print(c(f"[0,0,1] MagUp bin : {PID_NORM_HIST['MagUp', 0,0,1]}").red)
print(c(f"[0,0,1] MagDown bin : {PID_NORM_HIST['MagDown', 0,0,1]}").red)
print(c(f"[1,1,1] MagUp bin : {PID_NORM_HIST['MagUp', 1,1,1]}").red)
print(c(f"[1,1,1] MagDown bin : {PID_NORM_HIST['MagDown', 1,1,1]}").red)
print()
# ===================================================================================================================


for m in MAGPOLS:
    data_dict[m]["misid_w"] = np.nan

for m in MAGPOLS:
    print(c(f"Processing {m}").cyan.bold)
    data_dict[m]["misid_w"]=data_dict[m].apply(lambda row: compute_misID_w(
        PID_hist = PID_eff_dict[m],
        muonID_hist = muonID_eff_dict[m],
        L0GlobTIS_hist = hGlobTIS,
        data_P=row["Mu_plus_P"], 
        data_ETA=row["Mu_plus_ETA"], 
        data_nTracks=row["nTracks"], 
        data_PT=row["Mu_plus_PT"], 
        data_fakemuon_w=row["fakemuon_w"],
        B_P=row["B_plus_P"],
        B_PT=row["B_plus_PT"],
        year="2018",
        magpol=m,
        tofit_pid_path=tofit_PID_path
        #gtomu_eff_hist = calc_gtomu_eff(binning=config["pid"]["binning"], incl_MC=inclMC_dict[m])
    ), axis=1)
    print(c(f"\nNew column 'misid_w' added to {m} hadron-enriched dataframe").yellow)
    print(c(f"N entries in {m} : {len(data_dict[m])}").yellow.dark)

if opts.magcheck:
    for m in MAGPOLS:
        print(c(f"Magnet polarity : {m}").bold)
        print(data_dict[m][['B_plus_P', 'B_plus_PT', 'Mu_plus_P', 'Mu_plus_PT', 'Mu_plus_ETA', 'nTracks', 'misid_w']][:20])
        print()

# concatenate dataframes
out_df = pandas.concat( list(data_dict.values()) )
print(c(f"\nMerged polarities: {len(out_df)} events").green)

# sanity checks
assert(len(data_dict["MagUp"]) + len(data_dict["MagDown"]) == len(out_df) )
assert(out_df["misid_w"].all()!=np.nan)
assert(out_df["misid_w"].all()>=0)

# write to r01
print(c(f"Writing to ROOT format @ {OUTFILE}").green)
out_df.to_root(f"{OUTFILE}", key="DecayTree")

end = time.time()
print(c(f"\nExecution time: ").bold, end-start, " seconds")