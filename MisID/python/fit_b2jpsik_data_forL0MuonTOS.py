'''Executable to perform the fit to B2JpsiK to both TOSTIS and TOS data and derive the LoGlobalTIS eff and compile TeX docs 
__author__: Blaise Delaney
__email__: blaise.delaney@cern.ch
'''
from header import *
from termcolor2 import c
# import custom fit models
import sys
sys.path.insert(0, "/usera/delaney/private/Bc2D0MuNuX/Fitter/jpsik")
from models import dcb, dcb_cdf, dcbwg, dcbwg_cdf
from scipy.stats import expon
import uncertainties as u

# read in YEAR and MAGPOL 
parser = ArgumentParser()
parser.add_argument('-y',  '--year',    required=True , help='Year of data-taking or MC')
opts = parser.parse_args()

# ---------------------------------------------------------------------------------------------
# === bookkeeping ===
YEAR = opts.year
ntuple_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/ntuples/{YEAR}"
mc_pars_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}"
_prefix  = "/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/muonID/plots" 
pkl_prefix  = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/muonID/{YEAR}/pkl" 
mc_sig_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}/sig"
mc_misid_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}/misid"
# ---------------------------------------------------------------------------------------------

# read in data
data_b2jpsik_infile = uproot.open(f"{ntuple_path}/test_b2jpsik_data_forL0MuonTOS.root:DecayTree")
b2jpsik_data = data_b2jpsik_infile.arrays(library="pd")

# read in sig shape params
with open (f"{mc_pars_path}/sig/dcbwg_cdf_b2jpsik_sig_spec_forL0MuonTOS.yml") as sig_spec_file:
    mcpars = yaml.safe_load(sig_spec_file)

# read in misid shape params
with open (f"{mc_pars_path}/misid/dcb_cdf_b2jpsik_misid_spec_forL0MuonTOS.yml") as misid_spec_file:
    midpars = yaml.safe_load(misid_spec_file)

# set the mass range 
with open ("/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml") as in_cf:
    config = yaml.safe_load(in_cf)
MIN_M  = config["b2jpsik"]["MIN_M"];  MAX_M  = config["b2jpsik"]["MAX_M"]; 

# vis mass range and binning
mrange = (MIN_M, MAX_M)
mbins = 400 # nbins for fitting
pbins = 100 # nbins for plotting

def pdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb, comps=None):
  if comps is None:
    comps = ['sig','bkg','mid']

  tot = 0

  sig = dcbwg(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  if 'sig' in comps:
    tot += ns * sig

  mid = dcb(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)
  if 'mid' in comps:
    tot += nm * mid

  exp = expon(mrange[0],lb)
  bkg  = exp.pdf(x) / np.diff( exp.cdf(mrange) )
  if 'bkg' in comps:
    tot += nb * bkg

  return tot

def cdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb):

  sig = dcbwg_cdf(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  mid = dcb_cdf(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)

  exp = expon(mrange[0],lb)
  bkg = exp.cdf(x) / np.diff( exp.cdf(mrange) )

  return ns*sig + nb*bkg + nm*mid

def fit(data, cdf):
  print(c(f"Fit initialised").yellow)
  nh, xe = np.histogram(data, range=mrange, bins=mbins)

  mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf ),
               ns = 0.98*len(data),
               nb = 0.02*len(data),
               nm = 0.01*len(data),
               mug = 5280,
               sgg = 7,
               sgl = 10,
               sgr = 10,
               mum = 5330,
               lb  = 200
             )

  mi.limits['ns'] = (0, len(data))
  mi.limits['nb'] = (0, len(data))
  mi.limits['nm'] = (0, len(data))
  mi.limits['mug'] = (5240,5320)
  mi.limits['sgg'] = (0,20)
  mi.limits['sgl'] = (0,20)
  mi.limits['sgr'] = (0,20)
  mi.limits['mum'] = (5300,5350)
  mi.limits['lb']  = (10,500)

  mi.migrad()
  mi.hesse()
  print(mi)

  # # sanity fit checks
  # assert(mi.fmin.is_valid)
  # assert(mi.fmin.has_covariance)
  # assert(mi.fmin.has_posdef_covar)
  # assert(mi.fmin.has_accurate_covar)
  # print(c(f"SUCCESS! Fit converged & sanity checks passed.\n").green)
  
  return mi


def plot(axt, axb, data, pdf, _label=None,  _pt_lo=None, _pt_hi=None):
  nh, xe = np.histogram(data, range=mrange, bins=pbins)
  cx = 0.5 * (xe[1:]+xe[:-1])

  if _label == "mum_tos":
    data_label=rf"LHCb {YEAR} $\mu^-$TOS"
  if _label == "tos":
    data_label=rf"LHCb {YEAR} $\mu^-$TOS \& $\mu^+$TOS"
  if _label==None:
    data_label=rf"LHCb {YEAR}"

  axt.errorbar(
    x=cx, 
    y=nh, 
    yerr=np.sqrt(nh),
    xerr= 0.5 * (xe[1:] - xe[:-1]),
    fmt='.', 
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1, 
    markeredgewidth=1,
    label=data_label
    )
  x = np.linspace(*mrange,400)
  N = (mrange[1]-mrange[0])/pbins

  axt.plot([], [], color="white", label=rf"$p_T(\mu^+) \in [{_pt_lo:.0f}, {_pt_hi:.0f})$ MeV/c")
  axt.fill_between(x, 0, N*pdf(x, comps=["bkg"]), facecolor="darkorange", alpha=.7, label='Combinatorial')
  axt.fill_between(x, N*pdf(x, comps=["bkg"]), N*pdf(x, comps=["mid"])+N*pdf(x, comps=["bkg"]), facecolor="forestgreen", alpha=.7, label='MisID')
  axt.plot(x, N*pdf(x,  comps=['sig']), 'firebrick' , label=r"$B^+\rightarrow J/\psi\,K^+$", lw=2, ls="--")
  axt.plot(x, N*pdf(x), color="tab:blue", lw=3,label='Total Fit Model')

  handles,labels = axt.get_legend_handles_labels()
  handles = [handles[0], handles[1], handles[3], handles[4], handles[2]]
  labels =  [labels[0], labels[1], labels[3], labels[4], labels[2]]

  pull = (nh-N*pdf(cx))/(nh**0.5)

  # plot pull
  axb.errorbar( 
    x=cx, 
    y=pull, 
    xerr= 0.5 * (xe[1:] - xe[:-1]),
    yerr=np.ones_like(cx), 
    fmt='.', 
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1, 
    markeredgewidth=1,
  )

  axb.set_ylim(bottom=-5.5, top=5.5)
  axb.axhline(y=5, color="red", ls="--", lw=1)
  axb.axhline(y=0, color="green", ls="--", lw=1, alpha=1.)
  axb.axhspan(-3, 3, alpha=0.1, facecolor='green')
  axb.axhline(y=-5, color="red", ls="--", lw=1)
  
  # labels
  axb.set_ylabel(r"Pulls $[\sigma]$", loc="center")
  axb.set_xlabel(r"$m_{\text{DTF}}(J/\psi K^{+})$   [MeV/c$^2$]", loc="center")
  bin_width = 0.5 * (xe[1] - xe[0])
  axt.set_ylabel(rf"Events / {bin_width} MeV$/c^2$", loc="center")
  axt.legend(handles, labels, fontsize=13, loc="upper right")

def run(
  dframe, 
  _name,
  _prefix,
  _pt_lo,
  _pt_hi,
  ):

  # sloppy: changed the cuts but kept naming
  tos = dframe["Bplus_DTF_M"] # mu-TOS
  tis = dframe.query("muplus_L0MuonDecision_TOS==True")["Bplus_DTF_M"] # mu+TOS & mu-TOS
  kin_coords = (_pt_lo, _pt_hi)
  
  # fit tos sample
  tos_mi = fit( tos, cdf )
  tos_pdf = lambda x, comps=None: pdf(x,*tos_mi.values, comps)

  # fit tis sample
  tis_mi = fit( tis, cdf )
  tis_pdf = lambda x, comps=None: pdf(x,*tis_mi.values, comps)

  print(c("Fitted yields:").blue.underline)
  print(c('nTOS = {:f} +/- {:f}'.format(tos_mi.values['ns'], tos_mi.errors['ns'])).blue)
  print(c('nTIS = {:f} +/- {:f}'.format(tis_mi.values['ns'], tis_mi.errors['ns'])).blue)
  ntos = u.ufloat( tos_mi.values['ns'], tos_mi.errors['ns'] )
  ntis = u.ufloat( tis_mi.values['ns'], tis_mi.errors['ns'] )
  eff = ntis / ntos
  print(c("-"*40).red)
  print(c('Efficiency = {:f} +/- {:f}'.format(eff.n,eff.s)).red)
  print(c("-"*40).red)

  # plots
  fig, ax = plt.subplots(2,2, figsize=(18,8), sharex=True, gridspec_kw={'height_ratios': [5, 1]})

  print(c("\nbookig a TOS plot").yellow)
  plot(ax[0,0], ax[1,0], tos, tos_pdf, "mum_tos", *kin_coords )
  print(c("TOS plot complete").green)
  print(c("\nbookig a TIS plot").yellow)
  plot(ax[0,1], ax[1,1], tis, tis_pdf, "tos", *kin_coords )
  print(c("TIS plot complete").green)

  fig.tight_layout()
  fig.savefig(f"{_prefix}/{YEAR}/linscale/data_b2jpsik_fit_{_name}_forL0MuonTOS.pdf")
  fig.savefig(f"{_prefix}/{YEAR}/linscale/data_b2jpsik_fit_{_name}_forL0MuonTOS.png")

  ax[0,0].set_yscale('log')
  ax[0,1].set_yscale('log')
  fig.tight_layout()
  fig.savefig(f"{_prefix}/{YEAR}/logscale/data_b2jpsik_fit_{_name}_forL0MuonTOS.pdf")
  fig.savefig(f"{_prefix}/{YEAR}/logscale/data_b2jpsik_fit_{_name}_forL0MuonTOS.png")

  return eff.n, eff.s

# === move onto the fit & count stage ===
  # keep dir tree up to date
if os.path.isdir(f"{_prefix}/{YEAR}"): os.system(f"rm -rf {_prefix}/{YEAR}")
os.system(f"mkdir -p {_prefix}/{YEAR}/linscale")
os.system(f"mkdir -p {_prefix}/{YEAR}/logscale")

MIN_PT  = config["b2jpsik"]["MIN_PT"];  MAX_PT  = config["b2jpsik"]["MAX_PT"]; 

pt_edges = np.append(np.linspace(start=MIN_PT, stop=10_000, num=config["L0MuonTOS"]["PT_bins"]), [MAX_PT])# variable edges for pt
h_L0 = Hist( 
        hist.axis.Variable(pt_edges, label=r"$p_T(\mu^{+})$   [MeV/c]", name="muon_pt", underflow=False, overflow=False),  
        storage=hist.storage.Weight() # store eff cval & stdev
    )

# compile the summary table, then write to md and tex formats
headers = [r"P low", r"P high", r"PT low", r"PT high", r"eff", r"err"]
tab_body = {}
# now loop over the b2jpsik dataframe and split accordingly
for ptb in range(len(h_L0.project("muon_pt").axes[0].edges[:-1])):
  ptb_lo = h_L0.project("muon_pt").axes[0].edges[ptb]
  ptb_hi = h_L0.project("muon_pt").axes[0].edges[ptb+1]

  # convention: lower edge incuded, upper excluded
  _dataframe = b2jpsik_data.query(f"muplus_PT>={ptb_lo} and muplus_PT<{ptb_hi}")
  df_name = f"ptb_{ptb_lo:.0f}_{ptb_hi:.0f}"
  
  # # fit & count
  print(c(f"\nFit & count in bin of kinematics: p: [pt: [{ptb_lo}, {ptb_hi})").cyan.underline)
  kin_coords = (ptb_lo, ptb_hi)
  cval, stdev = run(_dataframe, df_name, _prefix, *kin_coords)
  tab_body[f"ptb_{ptb}"] = [f"{ptb_lo:.0f}", f"{ptb_hi:.0f}", f"{cval:.6f}", f"{stdev:.6f}"]

  # fill eff hist
  h_L0[ptb] = [cval, stdev**2]

table = list(tab_body.values())
md_table  = tabulate(table, headers, tablefmt="github")
tex_table = tabulate(table, headers, tablefmt="latex")
print(md_table, file=open(f"{_prefix}/{YEAR}/L0GlobTIS_eff_table_forL0MuonTOS.md", "w") )
print(tex_table, file=open(f"{_prefix}/{YEAR}/L0GlobTIS_eff_table_forL0MuonTOS.tex", "w") )
  
# plot
f, axs = plt.subplots(1, 1, sharex=False, sharey=False, figsize=(10,8))
axs.errorbar(
  x=h_L0.axes[0].centers, 
  y=h_L0.view().value, 
  xerr=h_L0.axes[0].widths/2, 
  yerr=h_L0.view().variance**.5,
  fmt="x", 
  markersize=5, 
  color="tab:blue",
  elinewidth=1.5,
  capsize=3, 
  markeredgewidth=2,
  )

axs.set_xlabel(h_L0.axes[0].label, loc="center")
axs.set_ylabel(rf"$\varepsilon(\mu^+\,$L0$\mu$TOS) {YEAR}", loc="center")
plt.savefig(f"{_prefix}/{YEAR}/L0GlobTIS_eff_forL0MuonTOS.pdf")
plt.savefig(f"{_prefix}/{YEAR}/L0GlobTIS_eff_forL0MuonTOS.png")

# save 
os.system(f"mkdir -p {pkl_prefix}")
with open(f"{pkl_prefix}/MuonID_eff.pkl", "wb") as f:
    pickle.dump(h_L0, f)


# === compile a tex/pdf summary of plots ===
# tried pylatex but somehow does not compile; should be easy enough to write by hand
to_tex = open(f"{_prefix}/{YEAR}/b2jpsik_fits_{YEAR}_forL0MuonTOS.tex", "w")
preamble=r'''\documentclass[11pt]{article}
\usepackage{graphicx}

\begin{document}
\section{\texttt{L0MuonTOS} efficiency from $B^+\rightarrow J/\psi K^+$ %s data}
'''%YEAR
to_tex.write(preamble)

eff_img = f"{_prefix}/{YEAR}/L0GlobTIS_eff_forL0MuonTOS.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.5]{%s}"%eff_img+"\n")
to_tex.write(r"\caption{Efficiency for the trigger requirement $\mu^{+}$ \texttt{L0MuonTOS} computed in bins of $p_T(\mu^+)$ using $B^+\rightarrow J/\psi K^+$ data from both magnet polarities combined.}")
to_tex.write("\end{figure}\n")

# add actual values and errors in dedicated table
to_tex.write(r"\newpage")
to_tex.write(tex_table)

# sig mc fit 
to_tex.write(r"\newpage")
to_tex.write(r"\section{Fits to MC %s}"%YEAR)
to_tex.write(r"\subsection{Signal $B^{+} \rightarrow J/\psi K^+$}")
sig_lin_img = f"{mc_sig_path}/linscale/sig_mc_fit_forL0MuonTOS.pdf"
sig_log_img = f"{mc_sig_path}/logscale/sig_mc_fit_forL0MuonTOS.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%sig_lin_img+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%sig_log_img+"\n")
to_tex.write(r"\caption{Fit to $B^+\rightarrow J/\psi K^+$ simulation. This is used to fix the signal shape parameters. The fit is shown on a linear (top) and a logarithmic (bottom) scale.}")
to_tex.write("\end{figure}\n")

# misid mc fit 
to_tex.write(r"\newpage")
to_tex.write(r"\subsection{MisID $B^{+} \rightarrow J/\psi \pi^+$}")
misid_lin_img = f"{mc_misid_path}/linscale/misid_mc_fit_forL0MuonTOS.pdf"
misid_log_img = f"{mc_misid_path}/logscale/misid_mc_fit_forL0MuonTOS.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%misid_lin_img+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%misid_log_img+"\n")
to_tex.write(r"\caption{Fit to $B^+\rightarrow J/\psi \pi^+$ simlation. This is used to fix the misID shape parameters. The fit is shown on a linear (top) and a logarithmic (bottom) scale.}")
to_tex.write("\end{figure}\n")
to_tex.write(r"\clearpage")

to_tex.write(r"\newpage")
to_tex.write(r"\section{Fits to $B^{+}\rightarrow J/\psi K^{+}$ %s Data}"%YEAR)
for file in os.listdir(f"{_prefix}/{YEAR}/linscale"):
    if file.endswith(".pdf"):
        lin_img = f"{_prefix}/{YEAR}/linscale/{file}"
        log_img = f"{_prefix}/{YEAR}/logscale/{file}"
        to_tex.write(r"\begin{figure}[h]"+"\n")
        to_tex.write(r"\centering"+"\n")
        to_tex.write(r"\includegraphics[scale=.3]{%s}"%lin_img+"\n")
        to_tex.write(r"\includegraphics[scale=.3]{%s}"%log_img+"\n")
        to_tex.write(r"\caption{Fit shown on linear scale (top) and logarithmic scale (bottom). The mometum range used to select the data is displayed in the fit legend. Each sample is obtained by merging the datasets from both magnet polarities.}")
        to_tex.write("\end{figure}\n")
        to_tex.write(r"\clearpage")

to_tex.write(r"\end{document}")
to_tex.close()

os.system(f"pdflatex -output-directory={_prefix}/{YEAR} {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}_forL0MuonTOS.tex")
os.system(f"rm {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}_forL0MuonTOS.log {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}_forL0MuonTOS.aux")