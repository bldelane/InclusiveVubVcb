'''Executable to perform the fit to B2JpsiK to both TOSTIS and TOS data and derive the LoGlobalTIS eff and compile TeX docs 
__author__: Blaise Delaney
__email__: blaise.delaney@cern.ch
'''
from header import *
from termcolor2 import c
# import custom fit models
import sys
sys.path.insert(0, "/usera/delaney/private/Bc2D0MuNuX/Fitter/jpsik")
from models import dcb, dcb_cdf, dcbwg, dcbwg_cdf
from scipy.stats import expon
import uncertainties as u

#destination dir
os.system('mkdir -p scratch/b2jpsik/plots')

# read in YEAR and MAGPOL 
parser = ArgumentParser()
parser.add_argument('-y',  '--year',    required=True , help='Year of data-taking or MC')
opts = parser.parse_args()

# ---------------------------------------------------------------------------------------------
# === bookkeeping ===
YEAR = opts.year
ntuple_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/ntuples/{YEAR}"
mc_pars_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}"
_prefix  = "/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/plots" 
pkl_prefix  = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/pkl/{YEAR}" 
mc_sig_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}/sig"
mc_misid_path = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits/{YEAR}/misid"
# ---------------------------------------------------------------------------------------------

# read in data
data_b2jpsik_infile = uproot.open(f"{ntuple_path}/test_b2jpsik_data.root:DecayTree")
b2jpsik_data = data_b2jpsik_infile.arrays(library="pd")

# read in sig shape params
with open (f"{mc_pars_path}/sig/dcbwg_cdf_b2jpsik_sig_spec.yml") as sig_spec_file:
    mcpars = yaml.safe_load(sig_spec_file)

# read in misid shape params
with open (f"{mc_pars_path}/misid/dcb_cdf_b2jpsik_misid_spec.yml") as misid_spec_file:
    midpars = yaml.safe_load(misid_spec_file)

# set the mass range 
with open ("/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml") as in_cf:
    config = yaml.safe_load(in_cf)
MIN_M  = config["b2jpsik"]["MIN_M"];  MAX_M  = config["b2jpsik"]["MAX_M"]; 

# vis mass range and binning
mrange = (MIN_M, MAX_M)
mbins = 400 # nbins for fitting
pbins = 100 # nbins for plotting

def pdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb, comps=None):
  if comps is None:
    comps = ['sig','bkg','mid']

  tot = 0

  sig = dcbwg(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  if 'sig' in comps:
    tot += ns * sig

  mid = dcb(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)
  if 'mid' in comps:
    tot += nm * mid

  exp = expon(mrange[0],lb)
  bkg  = exp.pdf(x) / np.diff( exp.cdf(mrange) )
  if 'bkg' in comps:
    tot += nb * bkg

  return tot

def cdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb):

  sig = dcbwg_cdf(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  mid = dcb_cdf(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)

  exp = expon(mrange[0],lb)
  bkg = exp.cdf(x) / np.diff( exp.cdf(mrange) )

  return ns*sig + nb*bkg + nm*mid

def fit(data, cdf):
  print(c(f"Fit initialised").yellow)
  nh, xe = np.histogram(data, range=mrange, bins=mbins)

  mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf ),
               ns = 0.98*len(data),
               nb = 0.02*len(data),
               nm = 0.01*len(data),
               mug = 5280,
               sgg = 7,
               sgl = 10,
               sgr = 10,
               mum = 5330,
               lb  = 200
             )

  mi.limits['ns'] = (0, len(data))
  mi.limits['nb'] = (0, len(data))
  mi.limits['nm'] = (0, len(data))
  mi.limits['mug'] = (5240,5320)
  mi.limits['sgg'] = (0,20)
  mi.limits['sgl'] = (0,20)
  mi.limits['sgr'] = (0,20)
  mi.limits['mum'] = (5300,5500)
  mi.limits['lb']  = (10,500)

  mi.migrad()
  mi.hesse()
  print(mi)

  # sanity fit checks
  # assert(mi.fmin.is_valid)
  # assert(mi.fmin.has_covariance)
  # assert(mi.fmin.has_posdef_covar)
  # assert(mi.fmin.has_accurate_covar)
  # print(c(f"SUCCESS! Fit converged & sanity checks passed.\n").green)
  
  return mi


def plot(axt, axb, data, pdf, _label=None,  _p_lo=None, _p_hi=None, _pt_lo=None, _pt_hi=None):
  nh, xe = np.histogram(data, range=mrange, bins=pbins)
  cx = 0.5 * (xe[1:]+xe[:-1])

  if _label == "tos":
    data_label=rf"LHCb {YEAR} $\mu$TOS"
  if _label == "tis":
    data_label=rf"LHCb {YEAR} $\mu$TOS \& $B$\,GlobalTIS"
  if _label==None:
    data_label=rf"LHCb {YEAR}"

  axt.errorbar(
    x=cx, 
    y=nh, 
    yerr=np.sqrt(nh),
    xerr= 0.5 * (xe[1:] - xe[:-1]),
    fmt='.', 
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1, 
    markeredgewidth=1,
    label=data_label
    )
  x = np.linspace(*mrange,400)
  N = (mrange[1]-mrange[0])/pbins

  axt.plot([], [], color="white", label=rf"$p(B^+)   \in [{_p_lo:.0f}, {_p_hi:.0f})$ MeV/c")
  axt.plot([], [], color="white", label=rf"$p_T(B^+) \in [{_pt_lo:.0f}, {_pt_hi:.0f})$ MeV/c")
  axt.fill_between(x, 0, N*pdf(x, comps=["bkg"]), facecolor="darkorange", alpha=.7, label='Combinatorial')
  axt.fill_between(x, N*pdf(x, comps=["bkg"]), N*pdf(x, comps=["mid"])+N*pdf(x, comps=["bkg"]), facecolor="forestgreen", alpha=.7, label='MisID')
  axt.plot(x, N*pdf(x,  comps=['sig']), 'firebrick' , label=r"$B^+\rightarrow J/\psi\,K^+$", lw=2, ls="--")
  axt.plot(x, N*pdf(x), color="tab:blue", lw=3,label='Total Fit Model')

  handles,labels = axt.get_legend_handles_labels()
  handles = [handles[0], handles[1], handles[6], handles[3], handles[5], handles[4], handles[2]]
  labels =  [labels[0], labels[1], labels[6], labels[3], labels[5], labels[4], labels[2]]

  pull = (nh-N*pdf(cx))/(nh**0.5)

  # plot pull
  axb.errorbar( 
    x=cx, 
    y=pull, 
    xerr= 0.5 * (xe[1:] - xe[:-1]),
    yerr=np.ones_like(cx), 
    fmt='.', 
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1, 
    markeredgewidth=1,
  )

  axb.set_ylim(bottom=-5.5, top=5.5)
  axb.axhline(y=5, color="red", ls="--", lw=1)
  axb.axhline(y=0, color="green", ls="--", lw=1, alpha=1.)
  axb.axhspan(-3, 3, alpha=0.1, facecolor='green')
  axb.axhline(y=-5, color="red", ls="--", lw=1)
  
  # labels
  axb.set_ylabel(r"Pulls $[\sigma]$", loc="center")
  axb.set_xlabel(r"$m_{\text{DTF}}(J/\psi K^+)$   [MeV/c$^2$]", loc="center")
  bin_width = 0.5 * (xe[1] - xe[0])
  axt.set_ylabel(rf"Events / {bin_width} MeV$/c^2$", loc="center")
  axt.legend(handles, labels, fontsize=13, loc="upper right")

def run(
  dframe, 
  _name,
  _prefix,
  _p_lo,
  _p_hi,
  _pt_lo,
  _pt_hi,
  ):

  tos = dframe["Bplus_DTF_M"]
  tis = dframe.query("Bplus_L0Global_TIS==True")["Bplus_DTF_M"]
  kin_coords = (_p_lo, _p_hi, _pt_lo, _pt_hi)
  
  # fit tos sample
  tos_mi = fit( tos, cdf )
  tos_pdf = lambda x, comps=None: pdf(x,*tos_mi.values, comps)

  # fit tis sample
  tis_mi = fit( tis, cdf )
  tis_pdf = lambda x, comps=None: pdf(x,*tis_mi.values, comps)

  print(c("Fitted yields:").blue.underline)
  print(c('nTOS = {:f} +/- {:f}'.format(tos_mi.values['ns'], tos_mi.errors['ns'])).blue)
  print(c('nTIS = {:f} +/- {:f}'.format(tis_mi.values['ns'], tis_mi.errors['ns'])).blue)
  ntos = u.ufloat( tos_mi.values['ns'], tos_mi.errors['ns'] )
  ntis = u.ufloat( tis_mi.values['ns'], tis_mi.errors['ns'] )
  eff = ntis / ntos
  print(c("-"*40).red)
  print(c('Efficiency = {:f} +/- {:f}'.format(eff.n,eff.s)).red)
  print(c("-"*40).red)


  # plots
  fig, ax = plt.subplots(2,2, figsize=(18,8), sharex=True, gridspec_kw={'height_ratios': [5, 1]})

  print(c("\nbookig a TOS plot").yellow)
  plot(ax[0,0], ax[1,0], tos, tos_pdf, "tos", *kin_coords )
  print(c("TOS plot complete").green)
  print(c("\nbookig a TIS plot").yellow)
  plot(ax[0,1], ax[1,1], tis, tis_pdf, "tis", *kin_coords )
  print(c("TIS plot complete").green)

  fig.tight_layout()
  fig.savefig(f"{_prefix}/{YEAR}/linscale/data_b2jpsik_fit_{_name}.pdf")
  fig.savefig(f"{_prefix}/{YEAR}/linscale/data_b2jpsik_fit_{_name}.png")

  ax[0,0].set_yscale('log')
  ax[0,1].set_yscale('log')
  fig.tight_layout()
  fig.savefig(f"{_prefix}/{YEAR}/logscale/data_b2jpsik_fit_{_name}.pdf")
  fig.savefig(f"{_prefix}/{YEAR}/logscale/data_b2jpsik_fit_{_name}.png")

  return eff.n, eff.s

# === move onto the fit & count stage ===
  # keep dir tree up to date

if os.path.isdir(f"{_prefix}/{YEAR}"): os.system(f"rm -rf {_prefix}/{YEAR}")
os.system(f"mkdir -p {_prefix}/{YEAR}/linscale")
os.system(f"mkdir -p {_prefix}/{YEAR}/logscale")

B_MIN_P   = config["b2jpsik"]["B_MIN_P"];   B_MAX_P  = config["b2jpsik"]["B_MAX_P"]; 
B_MIN_PT  = config["b2jpsik"]["B_MIN_PT"];  B_MAX_PT  = config["b2jpsik"]["B_MAX_PT"]; 

pt_edges = np.append(np.linspace(start=B_MIN_PT, stop=15_000, num=config["L0"]["PT_bins"]), [B_MAX_PT])# variable edges for pt
p_edges  = np.append(np.linspace(start=B_MIN_P,  stop=200_000, num=config["L0"]["P_bins"]), [B_MAX_P])# variable edges for nSPD
h_L0 = Hist( 
        hist.axis.Variable(p_edges,  label=rf"$p(B)$   [MeV/c]", name="B_p", underflow=False, overflow=False), 
        hist.axis.Variable(pt_edges, label=rf"$p_T(B)$   [MeV/c]", name="B_pt", underflow=False, overflow=False),  
        storage=hist.storage.Weight() # store eff cval & stdev
    )

# compile the summary table, then write to md and tex formats
headers = [r"P low", r"P high", r"PT low", r"PT high", r"eff", r"err"]
tab_body = {}
# now loop over the b2jpsik dataframe and split accordingly
for pb in range(len(h_L0.project("B_p").axes[0].edges[:-1])):
  pb_lo = h_L0.project("B_p").axes[0].edges[pb]
  pb_hi = h_L0.project("B_p").axes[0].edges[pb+1]
  for ptb in range(len(h_L0.project("B_pt").axes[0].edges[:-1])):
    ptb_lo = h_L0.project("B_pt").axes[0].edges[ptb]
    ptb_hi = h_L0.project("B_pt").axes[0].edges[ptb+1]

    # convention: lower edge incuded, upper excluded
    _dataframe = b2jpsik_data.query(f"Bplus_P>={pb_lo} and Bplus_P<{pb_hi} and Bplus_PT>={ptb_lo} and Bplus_PT<{ptb_hi}")
    df_name = f"pb_{pb_lo:.0f}_{pb_hi:.0f}_ptb_{ptb_lo:.0f}_{ptb_hi:.0f}"
    
    # # fit & count
    print(c(f"\nFit & count in bin of kinematics: p: [{pb_lo}, {pb_hi}), pt: [{ptb_lo}, {ptb_hi})").cyan.underline)
    kin_coords = (pb_lo, pb_hi, ptb_lo, ptb_hi)
    cval, stdev = run(_dataframe, df_name, _prefix, *kin_coords)
    tab_body[f"pb_{pb}_ptb_{ptb}"] = [f"{pb_lo:.0f}", f"{pb_hi:.0f}", f"{ptb_lo:.0f}", f"{ptb_hi:.0f}", f"{cval:.6f}", f"{stdev:.6f}"]

    # fill eff hist
    h_L0[pb, ptb] = [cval, stdev**2]

table = list(tab_body.values())
md_table  = tabulate(table, headers, tablefmt="github")
tex_table = tabulate(table, headers, tablefmt="latex")
print(md_table, file=open(f"{_prefix}/{YEAR}/L0GlobTIS_eff_table.md", "w") )
print(tex_table, file=open(f"{_prefix}/{YEAR}/L0GlobTIS_eff_table.tex", "w") )


# plot
f, axs = plt.subplots(1, 1, sharex=False, sharey=False, figsize=(8,8))
hep.hist2dplot(np.round(h_L0.view().value,2), h_L0.axes[0].edges, 
          h_L0.axes[1].edges, labels=True, ax=axs,);
axs.set_xlabel(h_L0.axes[0].label, loc="center")
axs.set_ylabel(h_L0.axes[1].label, loc="center")
axs.set_title(rf"$\varepsilon(B\,$L0 Global TIS) {YEAR}", loc="right")
axs.ticklabel_format(style='sci', axis='x', scilimits=(3,3))
axs.ticklabel_format(style='sci', axis='y', scilimits=(3,3))
plt.xticks(rotation=45)
plt.savefig(f"{_prefix}/{YEAR}/L0GlobTIS_eff.pdf")
plt.savefig(f"{_prefix}/{YEAR}/L0GlobTIS_eff.png")

# save 
os.system(f"mkdir -p {pkl_prefix}")
with open(f"{pkl_prefix}/L0GlobTIS_eff.pkl", "wb") as f:
    pickle.dump(h_L0, f)


# === compile a tex/pdf summary of plots ===
# tried pylatex but somehow does not compile; should be easy enough to write by hand
to_tex = open(f"{_prefix}/{YEAR}/b2jpsik_fits_{YEAR}.tex", "w")
preamble=r'''\documentclass[11pt]{article}
\usepackage{graphicx}

\begin{document}
\section{\texttt{L0GlobalTIS} efficiency from $B^+\rightarrow J/\psi K^+$ %s data}
'''%YEAR
to_tex.write(preamble)


eff_img = f"{_prefix}/{YEAR}/L0GlobTIS_eff.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.5]{%s}"%eff_img+"\n")
to_tex.write(r"\caption{Efficiency for the trigger requirement $B$ \texttt{L0GlobalTIS} computed in bins of $p(B^+)$ and $p_T(B^+)$ using $B^+\rightarrow J/\psi K^+$ data from both magnet polarities combined.}")
to_tex.write("\end{figure}\n")

# add actual values and errors in dedicated table
to_tex.write(r"\newpage")
to_tex.write(tex_table)

# sig mc fit 
to_tex.write(r"\newpage")
to_tex.write(r"\section{Fits to MC %s}"%YEAR)
to_tex.write(r"\subsection{Signal $B^{+} \rightarrow J/\psi K^+$}")
sig_lin_img = f"{mc_sig_path}/linscale/sig_mc_fit.pdf"
sig_log_img = f"{mc_sig_path}/logscale/sig_mc_fit.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%sig_lin_img+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%sig_log_img+"\n")
to_tex.write(r"\caption{Fit to $B^+\rightarrow J/\psi K^+$ simulation. This is used to fix the signal shape parameters. The fit is shown on a linear (top) and a logarithmic (bottom) scale.}")
to_tex.write("\end{figure}\n")

# misid mc fit 
to_tex.write(r"\newpage")
to_tex.write(r"\subsection{MisID $B^{+} \rightarrow J/\psi \pi^+$}")
misid_lin_img = f"{mc_misid_path}/linscale/misid_mc_fit.pdf"
misid_log_img = f"{mc_misid_path}/logscale/misid_mc_fit.pdf"
to_tex.write(r"\begin{figure}[h]"+"\n")
to_tex.write(r"\centering"+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%misid_lin_img+"\n")
to_tex.write(r"\includegraphics[scale=.3]{%s}"%misid_log_img+"\n")
to_tex.write(r"\caption{Fit to $B^+\rightarrow J/\psi \pi^+$ simlation. This is used to fix the misID shape parameters. The fit is shown on a linear (top) and a logarithmic (bottom) scale.}")
to_tex.write("\end{figure}\n")
to_tex.write(r"\clearpage")

to_tex.write(r"\newpage")
to_tex.write(r"\section{Fits to $B^{+}\rightarrow J/\psi K^{+}$ %s Data}"%YEAR)
for file in os.listdir(f"{_prefix}/{YEAR}/linscale"):
    if file.endswith(".pdf"):
        lin_img = f"{_prefix}/{YEAR}/linscale/{file}"
        log_img = f"{_prefix}/{YEAR}/logscale/{file}"
        to_tex.write(r"\begin{figure}[h]"+"\n")
        to_tex.write(r"\centering"+"\n")
        to_tex.write(r"\includegraphics[scale=.3]{%s}"%lin_img+"\n")
        to_tex.write(r"\includegraphics[scale=.3]{%s}"%log_img+"\n")
        to_tex.write(r"\caption{Fit shown on linear scale (top) and logarithmic scale (bottom). The mometum range used to select the data is displayed in the fit legend. Each sample is obtained by merging the datasets from both magnet polarities.}")
        to_tex.write("\end{figure}\n")
        to_tex.write(r"\clearpage")

to_tex.write(r"\end{document}")
to_tex.close()

os.system(f"pdflatex -output-directory={_prefix}/{YEAR} {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}.tex")
os.system(f"rm {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}.log {_prefix}/{YEAR}/b2jpsik_fits_{YEAR}.aux")