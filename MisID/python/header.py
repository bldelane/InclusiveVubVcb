# common imports for the PID/misID branch of the analysis
import boost_histogram as bh
import uproot
import hist
from hist import Hist
import yaml
import ROOT as r
from argparse import ArgumentParser
from tabulate import tabulate
import os
import pickle
import numpy as np
import ROOT
RDF = ROOT.ROOT.RDataFrame
import pandas
import json, requests, jsonschema
from matplotlib import gridspec
from termcolor2 import c

# stats
import iminuit
from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL, UnbinnedNLL, BinnedNLL
from uncertainties import *
import pyhf
pyhf.set_backend("numpy", "minuit") # get uncertainties and covariance

#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
hep.style.use(["LHCbTex", "LHCb2"])
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble":r"\usepackage{amsmath}",
    })
