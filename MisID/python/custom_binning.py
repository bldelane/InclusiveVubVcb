"""This file is an example of a user-defined binning scheme file, which """ \
"""can be passed as an argument to the multi-track calibration scripts.
The methods for constructing binning schema are defined in """ \
"""$PIDPERFSCRIPTSROOT/python/PIDPerfScripts/binning.py."""

from PIDPerfScripts.Binning import *
from PIDPerfScripts.Definitions import *

# === custom binning to unfold makeup of misID reference sample in _loose_ method ===
# run 1 
for trType in ('K', 'Pi', 'P', 'e', 'Mu'):
    # P - by eye
    AddBinScheme(trType,   'P', 'hadron_enriched_run1', 10000, 100000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 15000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 20000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 25000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 30000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 35000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 40000)
    AddBinBoundary(trType, 'P', 'hadron_enriched_run1', 100000)
    
    # ETA - Lucia
    AddBinScheme(trType,   'ETA', 'hadron_enriched_run1', 1.5, 5.0)
    AddBinBoundary(trType, 'ETA', 'hadron_enriched_run1', 3.0)
    
    # nTracks - R(D(st))
    AddBinScheme(trType,   'nTracks', 'hadron_enriched_run1', 0, 1000)
    AddBinBoundary(trType, 'nTracks', 'hadron_enriched_run1', 250)


# run 2 w/o 2016: electrons are problematic and produced via stripping28
for trType in ('K', 'Pi', 'P', 'e_B_Jpsi', 'Mu'):
    # P - by eye 
    AddBinScheme(trType,   'Brunel_P', 'hadron_enriched_run2_!2016', 10000, 100000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 15000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 20000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 25000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 30000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 35000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 40000)
    AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_!2016', 100000)
    
    # ETA - Lucia
    AddBinScheme(trType,   'Brunel_ETA', 'hadron_enriched_run2_!2016', 1.5, 5.0)
    AddBinBoundary(trType, 'Brunel_ETA', 'hadron_enriched_run2_!2016', 3.0)
    
    # nTracks - R(D(st))
    AddBinScheme(trType,   'nTracks_Brunel', 'hadron_enriched_run2_!2016', 0, 1000)
    AddBinBoundary(trType, 'nTracks_Brunel', 'hadron_enriched_run2_!2016', 250)

    
# run 2 - 2016: address difference binning vars due to electron sample via s28
for trType in ('K', 'Pi', 'P', 'e_B_Jpsi', 'Mu'):
    if trType=='e_B_Jpsi':
        # P - by eye
        AddBinScheme(trType,   'P', 'hadron_enriched_run2_2016', 10000, 100000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 15000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 20000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 25000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 30000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 35000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 40000)
        AddBinBoundary(trType, 'P', 'hadron_enriched_run2_2016', 100000)

        # ETA - Lucia
        AddBinScheme(trType,   'ETA', 'hadron_enriched_run2_2016', 1.5, 5.0)
        AddBinBoundary(trType, 'ETA', 'hadron_enriched_run2_2016', 3.0)

        # nTracks - R(D(st))
        AddBinScheme(trType,   'nTracks', 'hadron_enriched_run2_2016', 0, 1000)
        AddBinBoundary(trType, 'nTracks', 'hadron_enriched_run2_2016', 250)

    if trType!='e_B_Jpsi':
        # P - by eye
        AddBinScheme(trType,   'Brunel_P', 'hadron_enriched_run2_2016', 10000, 100000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 15000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 20000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 25000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 30000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 35000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 40000)
        AddBinBoundary(trType, 'Brunel_P', 'hadron_enriched_run2_2016', 100000)
    
        # ETA - Lucia
        AddBinScheme(trType,   'Brunel_ETA', 'hadron_enriched_run2_2016', 1.5, 5.0)
        AddBinBoundary(trType, 'Brunel_ETA', 'hadron_enriched_run2_2016', 3.0)
    
        # nTracks - R(D(st))
        AddBinScheme(trType,   'nTracks_Brunel', 'hadron_enriched_run2_2016', 0, 1000)
        AddBinBoundary(trType, 'nTracks_Brunel', 'hadron_enriched_run2_2016', 250)

