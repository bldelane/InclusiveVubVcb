from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-y','--year', default='2018')
parser.add_argument('-p','--pol' , default='MagDown')
parser.add_argument('-m','--dll' , default='low')
opts = parser.parse_args()

import uproot
import numpy as np

year = opts.year
magpol = opts.pol
dllbin = 'dllmu_'+opts.dll
ibins = 3
jbins = 2
kbins = 2

dir = f'/usera/delaney/private/Bc2D0MuNuX/MisID_JpsiMuNu/exec_nominal/{year}/{magpol}/{dllbin}'

particle = { 'kaon'  : 'K',
             'pion'  : 'Pi',
             'proton': 'P',
             'muon'  : 'Mu',
             'electron': 'e_B_Jpsi'
           }

reco_cuts = { 'kaon'    : 'MC15TuneV1_ProbNNghost<0.2 && DLLK>0.0 && (DLLK-DLLp)>0.0 && (DLLK-DLLe)>0.0 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All' ,
              'pion'    : 'MC15TuneV1_ProbNNghost<0.2 && DLLK<0.0 && DLLp<0.0 && DLLe<0.0 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All',
              'proton'  : 'MC15TuneV1_ProbNNghost<0.2 && DLLp>0.0 && (DLLp-DLLK)>0.0 && (DLLp-DLLe)>0.0 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All',
              'muon'    : 'MC15TuneV1_ProbNNghost<0.2 && (DLLmu-DLLp)>0.0 && (DLLmu-DLLe)>0.0 && (DLLmu-DLLK)>0 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All',
              'electron': 'MC15TuneV1_ProbNNghost<0.2 && DLLe>0.0 && (DLLe-DLLK)>0.0 && (DLLe-DLLp)>0.0 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All',
              'ghost'   : 'MC15TuneV1_ProbNNghost>=0.2 && IsMuon==0.0 && InMuonAcc==1.0 && nShared==0 && DLLmu<0 && P>10000 && P<100000 && PT>1500_All'
            }

if dllbin == 'dllmu_high':
  for part, cut in reco_cuts.items():
    reco_cuts[part] = reco_cuts[part].replace('DLLmu<0','DLLmu>0 && DLLmu<=3')

def read_hist( true, reco ):

  fname = f'{dir}/{true}/misID_{true}_to{reco}_{dllbin}_{year}_{magpol}/perfHist.root'

  hname = f'{particle[true]}_{reco_cuts[reco]}'

  with uproot.open(fname) as tfile:
    #print(tfile.classnames())
    #print(hname)
    bh = tfile[hname].to_boost()
    return bh

def get_shape( hists, i, j, k, norm=True ):
  vals = np.array( [ hist[i,j,k].value for hist in hists ] )
  errs = np.array( [ hist[i,j,k].variance for hist in hists] )
  vsum = np.sum(vals)
  if norm and vsum>0:
    vals /= vsum
    errs /= vsum
  return vals, errs**0.5

truths = ['kaon','pion','proton','muon','electron']
recos  = ['kaon','pion','proton','muon','electron','ghost']

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

x = np.array( range(len(recos)) )

fig, ax  = plt.subplots(ibins,jbins*kbins,figsize=(ibins*6, jbins*kbins*4))

for i in range(ibins):
  for j in range(jbins):
    for k in range(kbins):

      h = {}
      e = {}
      for truth in truths:
        h[truth], e[truth] = get_shape( [read_hist(truth, reco) for reco in recos  ], i, j, k )

      #ax[i,j+k*kbins].bar
      print(h)
      print(e)
      #print(i,j,k, i, j+k*kbins )

      ax[i,j+k*kbins].bar( x-0.4, h['kaon']    , width=0.2, label='kaon' )
      ax[i,j+k*kbins].bar( x-0.2, h['pion']    , width=0.2, label='pion' )
      ax[i,j+k*kbins].bar( x    , h['proton']  , width=0.2, label='proton' )
      ax[i,j+k*kbins].bar( x+0.2, h['muon']    , width=0.2, label='muon' )
      ax[i,j+k*kbins].bar( x+0.4, h['electron'], width=0.2, label='electron' )

      #ax[i,j+k*kbins].errorbar( x-0.4, h['kaon']    , e['kaon']    , 0.2, ls='', color='C0', label='kaon' )
      #ax[i,j+k*kbins].errorbar( x-0.2, h['pion']    , e['pion']    , 0.2, ls='', color='C1', label='pion' )
      #ax[i,j+k*kbins].errorbar( x    , h['proton']  , e['proton']  , 0.2, ls='', color='C2', label='proton' )
      #ax[i,j+k*kbins].errorbar( x+0.2, h['muon']    , e['muon']    , 0.2, ls='', color='C3', label='muon' )
      #ax[i,j+k*kbins].errorbar( x+0.4, h['electron'], e['electron'], 0.2, ls='', color='C4', label='electron' )

      ax[i,j+k*kbins].legend()
      ax[i,j+k*kbins].set_xticks(x)
      ax[i,j+k*kbins].set_xticklabels(recos)
      ax[i,j+k*kbins].set_ylabel('Fraction')
      ax[i,j+k*kbins].set_title(f'$P_b$={i}, $\eta_b$={j}, $N_b$={k}')

fig.tight_layout()
print("\n\n>>>>>> SAVING FILE")
#fig.savefig(f'/usera/delaney/private/Bc2D0MuNuX/PIDCalib/{year}_{magpol}_{dllbin}.pdf')
fig.savefig(f'{year}_{magpol}_{dllbin}.pdf')
print("saved!")
#plt.show()