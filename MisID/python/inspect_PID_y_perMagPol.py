'''Executable to compare the fractional yeild of each PID template,,
per bin of P, ETA, nTracks, per magnet polarity

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from pyhf import Model
import json, requests, jsonschema

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
opts = parser.parse_args()

#==========================================================================
# bookkeping
CONFIG_PATH = "/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml"
YEAR   = opts.year
pkl_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}/"

# where does the report live
_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}"
#==========================================================================

hPID_hists = {}

with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)
os.system(f"mkdir -p {_prefix}/y_checks")    

SPECIES = config["reco_cuts"].keys()

for s in SPECIES:
    print(s)
    plt.figure()
    print(c(f"Species: {s}").magenta.underline)
    plt.plot([], [], "", label=f"{s} {YEAR}", color="white")

    cvals = {}
    stds  = {}
    for m in ["MagUp", "MagDown"]:
        with open(f"{pkl_prefix}/{m}/pkl/hPID_eff_N_y.pkl", "rb") as f_in: 
            PID_y = pickle.load(f_in)
            
        if m == "MagUp": _color="black" 
        if m == "MagDown": _color="darkorange" 
        plt.errorbar(
            x = np.arange(1, len(PID_y[..., s, "unfolded_yield"].view().value.flatten())+1),
            xerr = 0.5,
            y = PID_y[..., s, "unfolded_yield"].view().value.flatten()/np.sum(PID_y[..., s, "unfolded_yield"].view().value.flatten()),
            yerr = (PID_y[..., s, "unfolded_yield"].view().variance**.5).flatten()/np.sum(PID_y[..., s, "unfolded_yield"].view().value.flatten()),
            color=_color,
            fmt=".",
            markersize=10,
            elinewidth=2,
            capsize=2,
            markeredgewidth=2,
            label=f"{m}"
        )
        cvals[m] = PID_y[..., s, "unfolded_yield"].view().value.flatten()/np.sum(PID_y[..., s, "unfolded_yield"].view().value.flatten())
        stds [m] = (PID_y[..., s, "unfolded_yield"].view().variance**.5).flatten()/np.sum(PID_y[..., s, "unfolded_yield"].view().value.flatten())
        print(c(f"{m} plot done").blue)

    chisq = np.sum(
        ( (cvals["MagUp"]-cvals["MagDown"])**2 ) / np.nan_to_num( (stds["MagUp"]**2 + stds["MagDown"]**2), nan=1e-5)
    )

    # check with matts code
    M_err = np.sqrt(np.nan_to_num( (stds["MagUp"]**2 + stds["MagDown"]**2), nan=1e-5))
    M_diff = (cvals["MagUp"]-cvals["MagDown"])
    M_chi2 = np.sum( M_diff**2 / M_err**2 )
    print(c(f"Blaise chis1: {chisq}").blue)
    print(c(f"Matts sanity check value: {M_chi2}").cyan)


    ndof = len(PID_y[..., s, "unfolded_yield"].view().value.flatten())
    print(c(f"NDF: {ndof}").green)
    chisq_ndof = chisq / ndof
    print(c(f"species: {s}; chi2/ndof = {chisq_ndof}").yellow)

    plt.plot([], [], "", color="white", label=r"$\chi^2$/ndf = %.2f $/$ %s = %.2f"%(chisq, ndof, chisq_ndof))

    plt.ylabel("Fractional yield", loc="center")
    plt.xlabel(r"Bins in $p, \eta$, nTracks", loc="center")
    
    #get handles and labels
    handles, labels = plt.gca().get_legend_handles_labels()

    #specify order of items in legend
    order = [0,2,3,1]
    plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order])

    plt.savefig(f"{_prefix}/y_checks/{s}.png")
    plt.savefig(f"{_prefix}/y_checks/{s}.pdf")