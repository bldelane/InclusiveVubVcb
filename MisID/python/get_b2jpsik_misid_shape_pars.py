'''Executable to fit the B2JpsiK MC to retrieve misid shape parameters, based on work by Matt Kenzie
__author__: blaise.delaney
__email__: blaise.delaney@cern.ch
'''
from header import *
# import custom fit models
import sys
sys.path.insert(0, "/usera/delaney/private/Bc2D0MuNuX/Fitter/jpsik")
from models import dcb, dcb_cdf, dcbwg, dcbwg_cdf
from termcolor2 import c
#destination dir
os.system('mkdir -p /usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/plots')


# read in YEAR; both magpols have already been concatenated
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking or MC',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
parser.add_argument(
    '-t',
    '--trigger',
    required=True,
    help='L0 trigger line, choose from [`L0MuonTOS`,`L0GlobalTIS`]',
    choices=[
        "L0MuonTOS",
        "L0GlobalTIS"])
opts = parser.parse_args()

#------------------------------------------------------------------------------------------
# bookeeping
YEAR = opts.year
TRIGGER = opts.trigger
plotdir_path = "/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/mcfits"
os.system(f"mkdir -p {plotdir_path}/{YEAR}/misid/linscale")
os.system(f"mkdir -p {plotdir_path}/{YEAR}/misid/logscale")

source = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/ntuples/{YEAR}"
PREFIX = "test_b2jpsik"
#------------------------------------------------------------------------------------------

# read in truthmatched MC
if TRIGGER=="L0GlobalTIS":
    print(c(f"fitting MC for {TRIGGER} shape - root file: {PREFIX}_misid.root").blue)
    MC_b2jpsik_infile = uproot.open(f"{source}/{PREFIX}_misid.root:DecayTree")
    MC_b2jpsik_df = MC_b2jpsik_infile.arrays(library="pd")
if TRIGGER=="L0MuonTOS":
    print(c(f"fitting MC for {TRIGGER} shape - root file: {PREFIX}_misid_forL0MuonTOS.root").blue)
    MC_b2jpsik_infile = uproot.open(f"{source}/{PREFIX}_misid_forL0MuonTOS.root:DecayTree")
    MC_b2jpsik_df = MC_b2jpsik_infile.arrays(library="pd")

with open("/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml") as in_cf:
    config = yaml.safe_load(in_cf)
MIN_M     = config["b2jpsik"]["MIN_M"]
MAX_M     = config["b2jpsik"]["MAX_M"]

# fit misid MC
mrange = (MIN_M, MAX_M)
fit_bins  = 400 # bins for fitting
plot_bins = 100 # bins for plotting

# book models
pdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)
cdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb_cdf(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)

# bin contents and edges
nh, xe = np.histogram(MC_b2jpsik_df["Bplus_DTF_M"], range=mrange, bins=fit_bins)

# now performed a binned likelihood fit
# mi = Minuit( UnbinnedNLL( mc, pdf ),
mi = Minuit( BinnedNLL( nh, xe, cdf),
             f   = .5,
             mul = 5330,
             mur = 5330,
             sgl = 15,
             sgr = 15,
             al  = .5,
             ar  = 1,
             nl  = 5,
             nr  = 3
           )
mi.limits['f'] = (0,1)
mi.limits['mul'] = (5300,5500)
mi.limits['mur'] = (5300,5500)
mi.limits['sgl'] = (0,30)
mi.limits['sgr'] = (0,30)
mi.limits['al'] = (0,3)
mi.limits['ar'] = (0,3)
mi.limits['nl'] = (1,5)
mi.limits['nr'] = (1,5)


# fit
mi.migrad()
mi.hesse()
print(mi)

# sanity fit checks
print(c(f"Sanity checks: please inspect function minimum properties").blue.underline)
print(c(f"{repr(mi.fmin)}").blue)
assert(mi.fmin.is_valid)
assert(mi.fmin.has_covariance)
assert(mi.fmin.has_posdef_covar)
assert(mi.fmin.has_accurate_covar)

# preserve the cdf pars for fit to data
misid_MC_spec = {}
for par in mi.parameters:
    misid_MC_spec[f"{par}"] = round(mi.values[par], 6) # 6 decimal places

# write to file
if TRIGGER=="L0GlobalTIS":
    print(c(f"Writing misID {TRIGGER} shape parameters - spec file: dcb_cdf_b2jpsik_misid_spec.yml").blue)
    with open(f'{plotdir_path}/{YEAR}/misid/dcb_cdf_b2jpsik_misid_spec.yml', 'w') as outfile:
        yaml.dump(misid_MC_spec, outfile, default_flow_style=False)
if TRIGGER=="L0MuonTOS":
    print(c(f"Writing misID {TRIGGER} shape parameters - spec file: dcb_cdf_b2jpsik_misid_spec_forL0MuonTOS.yml").blue)
    with open(f'{plotdir_path}/{YEAR}/misid/dcb_cdf_b2jpsik_misid_spec_forL0MuonTOS.yml', 'w') as outfile:
        yaml.dump(misid_MC_spec, outfile, default_flow_style=False)

# plot
fig, ax = plt.subplots(
    2, 1, figsize=(
        9, 8), sharex=True, gridspec_kw={
            'height_ratios': [
                5, 1]})

# naming convention as boost
values, edges = np.histogram(
    MC_b2jpsik_df["Bplus_DTF_M"], bins=plot_bins, range=mrange)
centers = 0.5 * (edges[1:] + edges[:-1])
ax[0].errorbar(
    x=centers,
    y=values,
    yerr=np.sqrt(values),
    xerr=0.5 * (edges[1:] - edges[:-1]),
    label=rf"LHCb MC {{{YEAR}}}",
    fmt='.',
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1,
    markeredgewidth=1,
)

# run the pdf
x = np.linspace(*mrange, 400)
intgr_factor = np.sum(values) * (mrange[1] - mrange[0]) / plot_bins

ax[0].plot(
    x, intgr_factor * pdf(x, *mi.values),
    label=r"Nominal $B^+\rightarrow J/\psi \,\pi^+$ Fit",
    color="tab:blue",
    lw=3
)
pull = (values - intgr_factor * pdf(centers, *mi.values)) / (values**0.5)

ax[0].set_ylim(bottom=0)
ax[0].legend(fontsize=15)
bin_width = 0.5 * (edges[1] - edges[0])
ax[0].set_ylabel(rf"Events / {bin_width} MeV$/c^2$", loc="center")

# pull plot
ax[1].errorbar(
    x=centers,
    y=pull,
    yerr=np.ones_like(centers),
    xerr=0.5 * (edges[1:] - edges[:-1]),
    fmt='.',
    color="black",
    markersize=5,
    elinewidth=1.5,
    capsize=1,
    markeredgewidth=1,
)

ax[1].set_ylim(bottom=-5.5, top=5.5)
ax[1].axhline(y=5, color="red", ls="--", lw=1)
ax[1].axhline(y=0, color="green", ls="--", lw=1, alpha=1.)
ax[1].axhspan(-3, 3, alpha=0.1, facecolor='green')
ax[1].axhline(y=-5, color="red", ls="--", lw=1)
ax[1].set_ylabel(r"Pulls $[\sigma]$", loc="center")
ax[1].set_xlabel(r"$m_{\text{DTF}}(J/\psi \,\pi^+)$   [MeV/c$^2$]", loc="center")

plt.tight_layout()

if TRIGGER=="L0GlobalTIS":
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/linscale/misid_mc_fit.png")
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/linscale/misid_mc_fit.pdf")

    # plot also on a log scale for convenience
    ax[0].set_yscale("log")
    ax[0].set_ylim(bottom=None)
    plt.tight_layout()
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/logscale/misid_mc_fit.png")
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/logscale/misid_mc_fit.pdf")

if TRIGGER=="L0MuonTOS":
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/linscale/misid_mc_fit_forL0MuonTOS.png")
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/linscale/misid_mc_fit_forL0MuonTOS.pdf")

    # plot also on a log scale for convenience
    ax[0].set_yscale("log")
    ax[0].set_ylim(bottom=None)
    plt.tight_layout()
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/logscale/misid_mc_fit_forL0MuonTOS.png")
    plt.savefig(f"{plotdir_path}/{YEAR}/misid/logscale/misid_mc_fit_forL0MuonTOS.pdf")
