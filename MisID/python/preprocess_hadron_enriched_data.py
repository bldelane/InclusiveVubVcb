'''Script to apply full selection to hadron-enriched data,
while keeping magnet polarities separate

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from pyhf import Model
import json, requests, jsonschema
ROOT.EnableImplicitMT()
from termcolor2 import c
import pprint 
import yaml
import os

# read in YEAR and MAGPOL 
parser = ArgumentParser()
parser.add_argument('-y',  '--year',    required=True , help='Year of data-taking')
opts = parser.parse_args()

mass_ang_cuts_config_file = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/MassCuts_hadron_enriched.yml"

with open(mass_ang_cuts_config_file,'r') as f:
    fit_sel = yaml.safe_load(f)
fit_sel.pop("name")
sel_str = "( " + " && ".join(fit_sel.values()) + " )"

print(c(f"In the case of signal, it's probably worth imposing the DCS and B_plus_M>3500 cuts before unfolding the PID").bold)
print(c(f"Full selection: should be mass cuts + DCS + visM cuts:\n{sel_str}").red)

# rename the raw hadron-enriched data straight out of the pipeline
for m in ["MU", "MD"]:
    #try:
        os.rename(f"/r01/lhcb/Bc2D0MuNuX/pipeline/hadron_enriched/DATA/merged_misid/D0MuNu/{opts.year}/{m}/Bc2D0MuNuX.root", f"/r01/lhcb/Bc2D0MuNuX/pipeline/hadron_enriched/DATA/merged_misid/D0MuNu/{opts.year}/{m}/unfiltered_Bc2D0MuNuX.root")

# apply sel string
root_path = f"/r01/lhcb/Bc2D0MuNuX/pipeline/hadron_enriched/DATA/merged_misid/D0MuNu/{opts.year}"
for m in ["MU", "MD"]:
    events = RDF("DecayTree", f"{root_path}/{m}/unfiltered_Bc2D0MuNuX.root").Filter(sel_str)
    outfile = events.Snapshot("DecayTree", f"{root_path}/{m}/Bc2D0MuNuX.root")

# REVERT 
# for m in ["MU", "MD"]:
#         os.rename(f"/r01/lhcb/Bc2D0MuNuX/pipeline/hadron_enriched/DATA/merged_misid/D0MuNu/{opts.year}/{m}/unfiltered_Bc2D0MuNuX.root", f"/r01/lhcb/Bc2D0MuNuX/pipeline/hadron_enriched/DATA/merged_misid/D0MuNu/{opts.year}/{m}/Bc2D0MuNuX.root")