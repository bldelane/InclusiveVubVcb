'''Executable to filer the Bc/Bu MC as a step preceding the extractin of eff(L0MuonTOS && Mu_isMuon);
The simualted samples are filtered as follow:

- Truthmatching: particle ID && mother ID 
- (Eventually corrected) PID conditions on the hadrons and muons in agreement with signal selection
- Mass/Ang cuts
- Main selection w/o isMuon (this is what we want to fold in)
- Trigger w/0 L0MuonTOS (this is what we want to fold in)

The above is to fold in the effect of applying L0MuonTOS and isMuon to a hadron-enriched sample extrapolated to the 
fit window, hence passing PIDmu>3, mass cuts

__author__: Blaise.Delaney
__email__ : blaise.delaney@cern.ch
'''

from header import *
#ROOT.EnableImplicitMT()

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
parser.add_argument(
    '-m',
    '--magpol',
    required=True,
    help='Magnet polarity',
    choices=[
        "MagUp",
        "MagDown"])
opts = parser.parse_args()

def ParseSelection(yamlselfile):
    sel = {}
    
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
            if "name" in sel.keys():
                del sel["name"]
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string

#==========================================================================
# bookkeping
CONFIG_PATH = "/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml"
YEAR   = opts.year
MAGPOL = opts.magpol
DOMAIN = "muonID"
MODES = ["Bc2D0MuNu", "Bu2D0MuNu"]
IS_PID_CORRECTED=str(False) # emulate what we might be reading in, a string and not a bool

# where to save the filtered MC files
_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/{DOMAIN}/ntuples/{YEAR}/{MAGPOL}"
#==========================================================================

# sel stages
trigger = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/TriggerSelection_muonID_eff_studies.yml")
main_selection = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/MainSelection_muonID_eff_studies.yml")
truthmatching = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/Truthmatching.yml")

if IS_PID_CORRECTED=="False": # same naming as data, lacking _gen suffix
    hadron_PID = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/PIDSelection.yml")
    muon_PID = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/RealMuon.yml")
    PID_SEL = f"{hadron_PID} && {muon_PID}"
    
if IS_PID_CORRECTED=="True": # _gen suffix
    hadron_PID = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/PIDSelectionMC.yml")
    muon_PID = ParseSelection("/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/RealMuonMC.yml")
    PID_SEL = f"{hadron_PID} && {muon_PID}"
    

MC_SEL = f"{trigger} && {main_selection} && {truthmatching} && {PID_SEL}"
print("\nGenerating MC files needed to study the eff(L0MuonTOS && Mu_isMuon)...")
print(c("\nSelection implemented:\n").underline)
print(c("Truthmatching: ").bold, truthmatching, "\n")
print(c("Trigger: ").bold, trigger, "\n")
print(c("PID: ").bold, PID_SEL, "\n")
print(c("Kinematics: ").bold, main_selection, "\n")

out_branches = ("Mu_plus_L0MuonDecision_TOS", "Mu_plus_isMuon", "Mu_plus_P", "Mu_plus_PT")
branchList = ROOT.vector('string')()
for branchName in out_branches: # fill in momentum
    branchList.push_back(branchName)

print(c("Writing filtered MC samples for L0MuonTOS && isMuon studies").yellow)
print(c(f"Branches written to file : {out_branches}").green)
os.system(f"mkdir -p {_prefix}")
if MAGPOL == "MagUp"   : mag_label = "MU"
if MAGPOL == "MagDown" : mag_label = "MD"
for m in MODES:
    sigMC_in = RDF(f"B2DMuNuX_D02KPiTuple/DecayTree", f"root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC/D0MuNu/{YEAR}/{mag_label}/MC_{m}_{YEAR}_{mag_label}.root").Filter(MC_SEL)
    sigMC_out = sigMC_in.Snapshot("DecayTree", f"{_prefix}/MC_{m}_{YEAR}.root", branchList)
    print(c(f"SUCCESS: {_prefix}/MC_{m}_{YEAR}.root written to file").green)