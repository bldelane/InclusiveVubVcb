'''Initialise the misID pipeline by setting up the subdiretory tree. This loops over years,
magpols, specied and muon ID bins (hadron_enriched and fit) to set up the correct bash files for 
PIDCalib and the overall subdirectory needed for the steps downstram of the DAG.

Reads in config from dedicated file. Needs a Urania build with an appropriate binning placed in the 
correct Urania directory.

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
# === Config dictionaries for PIDCalib-derived effs jobs ===
from argparse import ArgumentParser
import os, glob
from header import *

# directory where to dump all the sh files
parser = ArgumentParser()
parser.add_argument('-o',  '--output', default="exec",  required=False , help='please specify directory where to dump sh commands for PIDCalib [default: exec]')
parser.add_argument('-t',  '--test',   default="False",  required=False , help='is this a test run? If True, working on run1 only [default: False]')
opts = parser.parse_args()

# setup directory tree for output
ISTEST = opts.test
TOP_OUTPUT_DIR  = opts.output
localpath = "/usera/delaney/private/Bc2D0MuNuX/MisID"
os.system(f"mkdir -p {localpath}/{TOP_OUTPUT_DIR}")

preamble="""export PATH=$(getconf PATH)
export HOME=/usera/delaney
export X509_USER_PROXY=/usera/delaney/.grid.proxy
source /lhcb/scripts/lhcb-setup.sh
lb_set_platform x86_64-centos7-gcc9-opt
"""

# load config main file
CONFIG_PATH = "/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml"
with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)

#==================================================================
# pipe to custom_binning within Urania later;
# keep as a dict to read in other executables downstream in the DAG
binning = config["pid"]["binning"]
#==================================================================

# true/reco categories
species = config["pid"]["species"]
years   = config["pid"]["years"]
magpols = config["pid"]["magpols"]

# bins in DLLmu
#NOTE: only fake muons will make up the hadron-enriched sample as built with StdNoPIDMuon, hence don't have isMuon applied by default in stripping
DLLmu_bins = config["pid"]["DLLmu_bins"]

# === dicts for PIDCalib configuration ===
# reco categories
reco_cuts = config["reco_cuts"]

# how to partition the hadron-enriched data, imported later in the DAG
# note that we want to study abundance of hadrons in a _hadron-enriched_ sample, hence veto the isMuon flag
# this is also to bring the PIDCalib and hadron-enriched data in line
data_cuts = config["data_cuts"]

# === PIDCalib tunings ===
tunings = {}
tunings["2018"] = "MC15TuneV1"
tunings["2017"] = "MC15TuneV1"
tunings["2016"] = "MC15TuneV1"
tunings["2015"] = "MC15TuneV1"
tunings["2012"] = "MC12TuneV2"
tunings["2011"] = "MC12TuneV2"

# run years
calib_samples = {}
calib_samples["2018_!electrons"] = "Turbo18"
calib_samples["2017_!electrons"] = "Turbo17"
calib_samples["2016_!electrons"] = "Turbo16"
calib_samples["2015_!electrons"] = "Turbo15"
# electrons have their own calibration data
calib_samples["2018_electrons"] = "Electron18"
calib_samples["2017_electrons"] = "Electron17"
calib_samples["2016_electrons"] = "Electron16"
calib_samples["2015_electrons"] = "Electron15"
# run 1
calib_samples["2012_!electrons"] = "21" 
calib_samples["2011_!electrons"] = "21r1"
calib_samples["2012_electrons"] = "21" 
calib_samples["2011_electrons"] = "21r1"

binning_vars = {}
# run2 stripping analyses, not turbo
binning_vars["2018"]  = "\"Brunel_P\" \"Brunel_ETA\" \"nTracks_Brunel\"" # escape quotes
binning_vars["2017"]  = "\"Brunel_P\" \"Brunel_ETA\" \"nTracks_Brunel\"" # escape quotes
binning_vars["2016"]  = "\"Brunel_P\" \"Brunel_ETA\" \"nTracks_Brunel\"" # escape quotes
binning_vars["2015"]  = "\"Brunel_P\" \"Brunel_ETA\" \"nTracks_Brunel\"" # escape quotes
binning_vars["2012"]  = "\"P\" \"ETA\" \"nTracks\"" # escape quotes
binning_vars["2011"]  = "\"P\" \"ETA\" \"nTracks\"" # escape quotes

# cuts applied to calibration samples (numerator and denominator)
common_cuts = {}
common_cuts["2011"] = "InMuonAcc==1.0 && nShared==0 && P>10000 && P<100000 && PT>1500" # from event selection
common_cuts["2012"] = "InMuonAcc==1.0 && nShared==0 && P>10000 && P<100000 && PT>1500" # from event selection
common_cuts["2015"] = "InMuonAcc==1.0 && nShared==0 && Brunel_P>10000 && Brunel_P<100000 && Brunel_PT>1500" # from event selection
common_cuts["2016"] = "InMuonAcc==1.0 && nShared==0 && Brunel_P>10000 && Brunel_P<100000 && Brunel_PT>1500" # from event selection
common_cuts["2017"] = "InMuonAcc==1.0 && nShared==0 && Brunel_P>10000 && Brunel_P<100000 && Brunel_PT>1500" # from event selection
common_cuts["2018"] = "InMuonAcc==1.0 && nShared==0 && Brunel_P>10000 && Brunel_P<100000 && Brunel_PT>1500" # from event selection

# custom_binning.py has two schemes to account for nTracks_{Brunel} in run1 vs run2
binning_scheme_names = {}
for run in ["2015", "2017", "2018"]:
    binning_scheme_names[run] = "hadron_enriched_run2_!2016"
for run in ["2016"]:
    binning_scheme_names[run] = "hadron_enriched_run2_2016"
for run in ["2011", "2012"]:
    binning_scheme_names[run] = "hadron_enriched_run1"


# remove the previous iteration of the setup loop, per year
for y in years:
    os.system(f"rm -rf {TOP_OUTPUT_DIR}/{y}")

# === HADRON-ENRICHED LOOP ===
# compile files
for y in years:
    #if y=="2015" or y=="2016" or y=="2017" or y=="2018": species["electron"] = "e_B_Jpsi" # ugly hack
    common_cuts_string = common_cuts[y]
    for b in DLLmu_bins.keys():
        for s_true in species.keys(): # eg a true kaon
            if species[s_true]=="e_B_Jpsi" and y=="2016": binning_vars[y]="\"P\" \"ETA\" \"nTracks\"" # ugly hack
            for s_reco in reco_cuts.keys(): # eg what is the eff  of true kaon to be reconstructed as a {K, pi, p, mu, e, g} in a bin of (p, eta, nTracks), per-year, per-magpol
                for m in magpols:
                    if s_true != "electron":
                        calib_ID = f"{y}_!electrons"
                    if s_true == "electron":
                        calib_ID = f"{y}_electrons"
                    
                    #=====================================================
                    # parameters fed to PIDCalib
                    CALIB_SAMPLE = f"{calib_samples[calib_ID]}"
                    MAGPOL = f"{m}"
                    TRUE_SPECIES  = f"{species[s_true]}"
                    RECO_CATEGORY = f"{tunings[y]}{reco_cuts[s_reco]} && {DLLmu_bins[b]}"
                    BINNING_VARS = f"{binning_vars[y]}"
                    BINNING_NAME = f"{binning_scheme_names[y]}"
                    BINNING_FILE = "/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/python/PIDPerfScripts/custom_binning.py"
                    #=====================================================
                    
                    # impose a more organised subdirectory structure
                    OUTPUT_DIR = f"{TOP_OUTPUT_DIR}/{y}/{MAGPOL}/{b}/{s_true}" # eg 2018/MagUp/dllmu_low/kaon
                    sh_file_namespace = f"misID_{s_true}_to{s_reco}_{b}_{y}_{m}"
                    os.system(f"mkdir -p {localpath}/{OUTPUT_DIR}/{sh_file_namespace}")
                    
                    # write to temp sh file
                    config_string = f"\"{CALIB_SAMPLE}\" \"{MAGPOL}\" \"{TRUE_SPECIES}\" \"{RECO_CATEGORY}\" {BINNING_VARS} --binSchemeFile=\"{BINNING_FILE}\" --schemeName=\"{BINNING_NAME}\" --outputDir=\"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}\" -c \"{common_cuts_string}\" "
                    if ISTEST=="True":
                        # notice && at the end: bash command to execute epilogue only if PIDCalib finished the job successfully
                        calib_string = f"/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/run python /usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py --minRun=114205 --maxRun=114206 {config_string} &&"
                    if ISTEST=="False":
                        # notice && at the end: bash command to execute epilogue only if PIDCalib finished the job successfully
                        calib_string = f"/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/run python /usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py {config_string} &&"

                    # need to preserve the selection info to read in the eff histograms from root fike 
                    preserv_file = open(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/preserv.yaml", "w")
                    preserv_file.write(f"real     : \"{s_true}\"\n")
                    preserv_file.write(f"reco     : \"{s_reco}\"\n")
                    preserv_file.write(f"dllmu    : \"{b}\"\n")
                    preserv_file.write(f"eff_hist : \"{TRUE_SPECIES}_{RECO_CATEGORY}_All\"")
                    preserv_file.close()

                    # bash file to run PIDCalib
                    sh_file = open(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/run.sh", "w")
                    # pipe command to bashfile
                    sh_file.write(preamble) # need to invoke the correct env to run PIDCalib
                    sh_file.write(f"{calib_string}")
                    sh_file.write(f" \ntouch {OUTPUT_DIR}/{sh_file_namespace}/pidcalib.done && \n") # placeholder filer to signal complete execution of PIDCalib

                    # HACK: rename so that all root tuples are called the same - helps with the snake pipeline
                    epilogue=f"""for f in {localpath}/{OUTPUT_DIR}/{sh_file_namespace}/*.root; do\nmv \"$f\" {localpath}/{OUTPUT_DIR}/{sh_file_namespace}/perfHist.root\ndone
                    """
                    sh_file.write(epilogue+"\n")
                    sh_file.write("source /usera/delaney/miniconda3/bin/activate /usera/delaney/miniconda3/envs/FITENV") 
                    sh_file.close()

                    os.chmod(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/run.sh", 0o0777)


# === LOOP TO EXTRAPOLATE TO SIGNAL WINDOW
# compile files
for y in years:
    common_cuts_string = common_cuts[y]
    #if y=="2015" or y=="2016" or y=="2017" or y=="2018": species["electron"] = "e_B_Jpsi" # ugly hack
    b="muonID_fit"
    for s_true in species.keys(): # eg a true kaon
            if species[s_true]=="e_B_Jpsi" and y=="2016": binning_vars[y]="\"P\" \"ETA\" \"nTracks\"" # ugly hack
            for m in magpols: # no reco cats: every hadron extrapolated to muonID criteria for the fit
                    if s_true != "electron":
                        calib_ID = f"{y}_!electrons"
                    if s_true == "electron":
                        calib_ID = f"{y}_electrons"
                    
                    #=====================================================
                    # parameters fed to PIDCalib
                    CALIB_SAMPLE = f"{calib_samples[calib_ID]}"
                    MAGPOL = f"{m}"
                    TRUE_SPECIES  = f"{species[s_true]}"
                    RECO_CATEGORY = f"{tunings[y]}{config['pid'][b]}"
                    BINNING_VARS = f"{binning_vars[y]}"
                    BINNING_NAME = f"{binning_scheme_names[y]}"
                    BINNING_FILE = "/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/python/PIDPerfScripts/custom_binning.py"
                    #=====================================================
                    
                    # impose a more organised subdirectory structure
                    OUTPUT_DIR = f"{TOP_OUTPUT_DIR}/{y}/{MAGPOL}/{b}/{s_true}" # eg 2018/MagUp/dllmu_low/kaon
                    sh_file_namespace = f"misID_{s_true}_tomuon_{b}_{y}_{m}"
                    os.system(f"mkdir -p {localpath}/{OUTPUT_DIR}/{sh_file_namespace}")
                    
                    # write to temp sh file
                    config_string = f"\"{CALIB_SAMPLE}\" \"{MAGPOL}\" \"{TRUE_SPECIES}\" \"{RECO_CATEGORY}\" {BINNING_VARS} --binSchemeFile=\"{BINNING_FILE}\" --schemeName=\"{BINNING_NAME}\" --outputDir=\"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}\" -c \"{common_cuts_string}\" "
                    if ISTEST=="True":
                        # notice && at the end: bash command to execute epilogue only if PIDCalib finished the job successfully
                        calib_string = f"/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/run python /usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py --minRun=114205 --maxRun=114206 {config_string} &&"
                    if ISTEST=="False":
                        # notice && at the end: bash command to execute epilogue only if PIDCalib finished the job successfully
                        calib_string = f"/usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/run python /usera/delaney/private/Bc2D0MuNuX/MisID/UraniaDev_v10r0/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py {config_string} &&"

                    # need to preserve the selection info to read in the eff histograms from root fike 
                    preserv_file = open(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/preserv.yaml", "w")
                    preserv_file.write(f"real     : \"{s_true}\"\n")
                    preserv_file.write(f"reco     : \"{s_reco}\"\n")
                    preserv_file.write(f"dllmu    : \"{b}\"\n")
                    preserv_file.write(f"eff_hist : \"{TRUE_SPECIES}_{RECO_CATEGORY}_All\"")
                    preserv_file.close()

                    # bash file to run PIDCalib
                    sh_file = open(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/run.sh", "w")
                    # pipe command to bashfile
                    sh_file.write(preamble) # need to invoke the correct env to run PIDCalib
                    sh_file.write(f"{calib_string}")
                    sh_file.write(f" \ntouch {OUTPUT_DIR}/{sh_file_namespace}/pidcalib.done && \n") # placeholder filer to signal complete execution of PIDCalib

                    # HACK: rename so that all root tuples are called the same - helps with the snake pipeline
                    epilogue=f"""for f in {localpath}/{OUTPUT_DIR}/{sh_file_namespace}/*.root; do\nmv \"$f\" {localpath}/{OUTPUT_DIR}/{sh_file_namespace}/perfHist.root\ndone
                    """
                    sh_file.write(epilogue+"\n")
                    sh_file.write("source /usera/delaney/miniconda3/bin/activate /usera/delaney/miniconda3/envs/FITENV") 
                    sh_file.close()

                    os.chmod(f"{localpath}/{OUTPUT_DIR}/{sh_file_namespace}/run.sh", 0o0777)
