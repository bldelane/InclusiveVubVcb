'''Executable to source the B2JpsiK tuples from eos and write temp local files to perform fits
and extract efficiency values of L0GlobalTIS via TOSTIS
__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from termcolor2 import c
from header import *
ROOT.EnableImplicitMT()

# read in YEAR and MAGPOL
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking or MC')

opts = parser.parse_args()
YEAR = opts.year

# --------------------------------------------------------------------------------------
# bookeeping - ensure consistency of locations when building pipeline
destination_dir = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/b2jpsik/ntuples/{YEAR}"
os.system(f"mkdir -p {destination_dir}")
PREFIX = "test_b2jpsik"
# --------------------------------------------------------------------------------------

# since studying the effcy wrt B, no kinemati cuts on children to avoid sculpting
with open("/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml") as in_cf:
    config = yaml.safe_load(in_cf)
MIN_M = config["b2jpsik"]["MIN_M"]
MAX_M = config["b2jpsik"]["MAX_M"]

# ===== DATA file on /eos =====
branchList = ROOT.vector('string')()
for branchName in [
    "Bplus_L0Global_TIS",
    "Bplus_P",
    "Bplus_PT",
    "Bplus_DTF_M"]:
    branchList.push_back(branchName)

data_fileList = ROOT.vector('string')()
for d_fin in [
    f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/data/{YEAR}/MagUp/all.root",
    f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/data/{YEAR}/MagDown/all.root"
    ]:
    data_fileList.push_back(d_fin)

sel_string = f"Kplus_PIDK>{config['b2jpsik']['Kplus_PIDK_cut']} && Bplus_DTF_M>{MIN_M} && Bplus_DTF_M<{MAX_M} && (muplus_L0MuonDecision_TOS || muminus_L0MuonDecision_TOS)"
print(c(f"\nDebug layer: data_sel_string = {sel_string}\n").magenta)

in_data_RDF = RDF(
    f"B2JpsimmKTuple/DecayTree",
    data_fileList,
    branchList).Filter(f"{sel_string}")

# write data file
out_data = in_data_RDF.Snapshot(
    "DecayTree",
    f"{destination_dir}/{PREFIX}_data.root",
    branchList)
print(c(f"Data skimming done. Output tree: {destination_dir}/{PREFIX}_data.root:DecayTree").green)

# ===== now focus on MC ======
# *ASSUMPTION*: MC fixes the signal shape, and this remains the same regardless of K_L0Muon or B_L0GlobalTIS
MC_branchList = ROOT.vector('string')()
for MC_branchName in ["Bplus_DTF_M"]:
    MC_branchList.push_back(MC_branchName)

mc_fileList = ROOT.vector('string')()
for mc_fin in [
    f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/MagUp/B2JpsimumuK.root",
    f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/MagDown/B2JpsimumuK.root",
    ]:
    mc_fileList.push_back(mc_fin)

# notice a PIDK>0 on uncorrected PID; assume shape tail parameters are
# sufficiently uncorrelated - not interested in efficiencies but only in
# shape
mc_sel_string = f"Bplus_DTF_M>{MIN_M} && Bplus_DTF_M<{MAX_M} && {config['b2jpsik']['truthmatching']} && Kplus_PIDK>{config['b2jpsik']['Kplus_PIDK_cut']}"
print(c(f"\nDebug layer: mc_sel_string = {mc_sel_string}\n").magenta)

mc_in_RDF = RDF(
    f"B2JpsimmKTuple/DecayTree",
    mc_fileList
    ).Filter(f"{mc_sel_string}")

# write MC file
out_sim = mc_in_RDF.Snapshot(
    "DecayTree",
    f"{destination_dir}/{PREFIX}_sig.root",
    MC_branchList)

print(c(f"Signal MC skimming done. Output tree: {destination_dir}/{PREFIX}_sig.root:DecayTree").green)


# ===== move onto the misID: look at B2JpsiK MC with BKGCAT=30 =====
# notice a PIDK>0 on uncorrected PID; assume shape tail parameters are
# sufficiently uncorrelated - not interested in efficiencies but only in
# shape
misid_sel_string = f"Bplus_DTF_M>{MIN_M} && Bplus_DTF_M<{MAX_M} && Kplus_PIDK>{config['b2jpsik']['Kplus_PIDK_cut']} && Bplus_BKGCAT==30"
print(c(f"\nDebug layer: misid_sel_string = {misid_sel_string}\n").magenta)

misidmc_fileList = ROOT.vector('string')()
# hack: Alison seems not to have 2016 MC, so read in 2018 instead
# ultimately need only shape info
if YEAR!="2016":
    for misidmc_fin in [
        f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/MagUp/B2Jpsimumupi.root",
        f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/MagDown/B2Jpsimumupi.root",
        ]:
        misidmc_fileList.push_back(misidmc_fin)
if YEAR=="2016":
    print(c("Hack: 2016 MC for Bu2JpsiPi absent, read 2018 instead").blue)
    for misidmc_fin in [
        f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagUp/B2Jpsimumupi.root",
        f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagDown/B2Jpsimumupi.root",
        ]:
        misidmc_fileList.push_back(misidmc_fin)

# save both polarities
misid_mc_in_RDF = RDF(
    f"B2JpsimmKTuple/DecayTree",
    misidmc_fileList
    ).Filter(f"{misid_sel_string}")
    
# write MC file
out_misid_sim = misid_mc_in_RDF.Snapshot(
    "DecayTree",
    f"{destination_dir}/{PREFIX}_misid.root",
    MC_branchList)

print(c(f"MisID MC skimming done. Output tree: {destination_dir}/{PREFIX}_misid.root:DecayTree").green)
