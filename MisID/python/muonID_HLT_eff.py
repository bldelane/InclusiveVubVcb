'''Extract from simulation the eff(L0MuonTOS && Mu_isMuon);
The effs are extracted in bins of pT, as both ID criteria are a function of the muon track
transverse momentum.

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from pyhf import Model
import json, requests, jsonschema
from uncertainties import ufloat
ROOT.EnableImplicitMT()
import yaml

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
opts = parser.parse_args()

#==========================================================================
# bookkeping
#TRUTHMATCHING = "( (abs(Mu_plus_TRUEID)==321) || (abs(Mu_plus_TRUEID)==211) || (abs(Mu_plus_TRUEID)==0) || (abs(Mu_plus_TRUEID)==11) || (abs(Mu_plus_TRUEID)==2212) )"
# assume this is made of kaons, pions, protons; neglect probNNghost as the idea is that is factorises and calcels in the ratios
TRUTHMATCHING = "( (abs(Mu_plus_TRUEID)==321) || (abs(Mu_plus_TRUEID)==211) || (abs(Mu_plus_TRUEID)==2212) )"
MOMENTUM_CUTS = "(Mu_plus_PT>1500) && (Mu_plus_P>10e3) && (Mu_plus_P<100e3)"
EXTRA_CUTS = "(Mu_plus_hasMuon) && (Mu_plus_NShared==0)"

HLT_config_file = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection/D0MuNu/TriggerSelection.yml"
with open(HLT_config_file,'r') as f:
    trigger_sel = yaml.safe_load(f)

HLT1_CUTS = trigger_sel["HLT1"] 
HLT2_CUTS = trigger_sel["HLT2"] 

CONFIG_PATH = "/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml"
YEAR   = opts.year
MAGPOLS = ["MagUp","MagDown"]
DOMAIN = "muonID_HLT"
plots_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/{DOMAIN}/{YEAR}/plots"
os.system(f"mkdir -p {plots_prefix}")
ADD_B2JPSIK = False
#==========================================================================

def tohist(cvals, nbins=10, _MIN_PT=1_500, _MAX_PT=40_000, _MID_PT=15_000, _MID_HIGH=None, _name="muonID_pt"):
    '''utility fn to book hist of IDd events ad a fucntion of pt'''
    
    if _MID_HIGH is None: pt_edges = np.append(np.linspace(start=_MIN_PT, stop=_MID_PT, num=nbins-1), [_MAX_PT])# variable edges for pt
    else: pt_edges = np.append(np.linspace(start=_MIN_PT, stop=_MID_PT, num=nbins-2), [_MID_HIGH, _MAX_PT])# variable edges for pt
    h = Hist( 
        hist.axis.Variable(pt_edges, 
        label=r"$p_T(\mu^{+})$   [MeV/c]", 
        name=_name, 
        underflow=False, 
        overflow=False),  
        storage=hist.storage.Double()
    )
    h.fill( cvals )
    return h

def eff_std(N, k):
    '''Utility function to compute the eff+/- err
    the error estimate is performed via binomial errors
    this is valid unless k=N or k=0
    source: Paterno, FERMILAB-TM-2286-CD
    '''
    std = (1/N)*np.sqrt(k*(1-(k/N)))
    cval = k/N
    return std

with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)
 
PAR = "Mu_plus_PT"
MIN_PT=config["b2jpsik"]["MIN_PT"] # from selection
MID_PT=8_000
MAX_PT=config["b2jpsik"]["MAX_PT"] # from selection
NBINS=7
MID_HIGH=None
pt_edges = np.append(np.linspace(start=MIN_PT, stop=MID_PT, num=NBINS-1), [MAX_PT])

#TRUTHMATCHING = "( (abs(Mu_plus_TRUEID)==321) | (abs(Mu_plus_TRUEID)==211) | (abs(Mu_plus_TRUEID)==0) | (abs(Mu_plus_TRUEID)==11) | (abs(Mu_plus_TRUEID)==2212) )"
TRUTHMATCHING = "( (abs(Mu_plus_TRUEID)==321) | (abs(Mu_plus_TRUEID)==211) | (abs(Mu_plus_TRUEID)==2212) )"
MOMENTUM_CUTS = "(Mu_plus_PT>1500) & (Mu_plus_P>10e3) & (Mu_plus_P<100e3)"
EXTRA_CUTS = "(Mu_plus_hasMuon) & (Mu_plus_NShared==0)"
#MAGPOL="MagUp"
#REMOTE_PATH = f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_{MAGPOL}_5M.root"

def to_eff(denominator_criteria, numerator_criteria, magup_color, magdown_color, plot_name, _ylabel):
    _ = plt.figure(figsize=(9, 7));
    plt.plot([], [], "", label=f"LHCb 2016 Simulation, Unofficial", color="white")

    out_hists = {}
    for MAGPOL in MAGPOLS:
        REMOTE_PATH = f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_{MAGPOL}_5M.root"
        
        denominator_df = pandas.DataFrame(RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"{REMOTE_PATH}").Filter(f"{denominator_criteria}").AsNumpy(), columns=["Mu_plus_PT", "Mu_plus_TRUEID"])
        denominator_hist = tohist(cvals=denominator_df[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)

        numerator_df = pandas.DataFrame(RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"{REMOTE_PATH}").Filter(f"{numerator_criteria}").AsNumpy(), columns=["Mu_plus_PT", "Mu_plus_TRUEID"])
        numerator_hist = tohist(cvals=numerator_df[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)

        print(c(f"\nComputing eff({plot_name})").cyan)

        print(c(f"\nDebug layer: instances of muon track ID").underline)
        print("DEN : \n", denominator_df["Mu_plus_TRUEID"].value_counts())
        print("NUM : \n", numerator_df["Mu_plus_TRUEID"].value_counts())

        print(c(f"\nDenominator : \n{denominator_hist.view()}").yellow)
        print(c(f"Numerator : \n{numerator_hist.view()}").yellow)
        
        # cvals of eff
        HLT_had_enr_eff = numerator_hist.view()/denominator_hist.view()
        print(c(f"Efficiency : \n{HLT_had_enr_eff}").red)
        
        # eff hist 
        eff_HLT_had_enr_hist = Hist( 
                hist.axis.Variable(pt_edges, 
                label=r"$p_T(\mu^{+})$   [MeV/c]", 
                underflow=False, 
                overflow=False),  
                storage=hist.storage.Weight()
            )
        
        for i in range(len(HLT_had_enr_eff)):
            eff_HLT_had_enr_hist[i] = [HLT_had_enr_eff[i], eff_std(N=denominator_hist.view(), k=numerator_hist.view())[i]**2]

        print(c(f"Debug: efficiency hist: \n{eff_HLT_had_enr_hist.view().value}").magenta)
        print(c(f"Debug: efficiency errs: \n{eff_HLT_had_enr_hist.view().variance**.5}").magenta)
        # sanity checks
        assert(eff_HLT_had_enr_hist.view().value.all() == HLT_had_enr_eff.all())
        assert((eff_HLT_had_enr_hist.view().variance**.5).all() == eff_std(N=denominator_hist.view(), k=numerator_hist.view()).all())
        print(c(f"\nExecution complete for {MAGPOL} polarity").blue)
    
        # plots
        if MAGPOL=="MagUp"   : color=magup_color
        if MAGPOL=="MagDown" : color=magdown_color
        plt.errorbar(x=eff_HLT_had_enr_hist.axes[0].centers, y=eff_HLT_had_enr_hist.view().value, 
                        xerr=eff_HLT_had_enr_hist.axes[0].widths/2, 
                        yerr=eff_HLT_had_enr_hist.view().variance**.5,
                        fmt="x", markersize=5, elinewidth=1., color=color,
                        label=MAGPOL)
        
        out_hists[MAGPOL] = eff_HLT_had_enr_hist

    plt.xlabel(rf"$p_T(X')$   [MeV/c]", loc="center")
    plt.ylabel(_ylabel, loc="center", fontsize=20)
    plt.legend(loc="best", fontsize=13)

    plt.savefig(f"{plots_prefix}/{plot_name}.pdf")
    plt.savefig(f"{plots_prefix}/{plot_name}.png")
    print(c(f"Plots saved to {plots_prefix}/{plot_name}.pdf/png").blue)

    return out_hists

# eff(HLT | GlobTIS & !isMuon)
eff_HLT_had_enr = to_eff(
    denominator_criteria=f"{TRUTHMATCHING} && {MOMENTUM_CUTS} && {EXTRA_CUTS} && B_plus_L0Global_TIS && !Mu_plus_isMuon",
    numerator_criteria=f"{TRUTHMATCHING} && {MOMENTUM_CUTS} && {EXTRA_CUTS} && B_plus_L0Global_TIS && !Mu_plus_isMuon && {HLT1_CUTS} && {HLT2_CUTS}",
    magdown_color="pink", 
    magup_color="firebrick",
    plot_name="HLT_L0GlobTIS_notIsMuon",
    _ylabel=r"$\varepsilon(\text{HLT} | B\, \text{L0GlobalTIS}~\&~\text{!isMuon})$"
)

# eff(HLT | L0Muon) - isMuon is already inlcuded
eff_full_muon_id = to_eff(
    denominator_criteria=f"{TRUTHMATCHING} && {MOMENTUM_CUTS} && {EXTRA_CUTS} && Mu_plus_L0MuonDecision_TOS",
    numerator_criteria=f"{TRUTHMATCHING} && {MOMENTUM_CUTS} && {EXTRA_CUTS} && Mu_plus_L0MuonDecision_TOS && {HLT1_CUTS} && {HLT2_CUTS}",
    magdown_color="tab:blue", 
    magup_color="navy",
    plot_name="full_muonID_HLT",
    _ylabel=r"$\varepsilon(\text{HLT} | \mu \, \text{L0Muon})$"
)
    
    
_ = plt.figure(figsize=(9, 7));
plt.plot([], [], "", label=f"LHCb 2016 Simulation, Unofficial", color="white")

for MAGPOL in MAGPOLS:

    if MAGPOL=="MagUp" : color = "mediumpurple"
    if MAGPOL=="MagDown" : color = "darkslateblue"
    
    CVALS = []
    ERRS  = []


    for i in range(len(eff_full_muon_id[MAGPOL].view().value)):
        HLT_he   = ufloat(eff_HLT_had_enr[MAGPOL].view().value[i], (eff_HLT_had_enr[MAGPOL].view().variance**.5)[i])    
        HLT_full = ufloat(eff_full_muon_id[MAGPOL].view().value[i], (eff_full_muon_id[MAGPOL].view().variance**.5)[i])    

        CVALS.append((HLT_full/HLT_he).n)  
        ERRS.append((HLT_full/HLT_he).s)  
    
    print(c(f"Reading in {MAGPOL}").green)
    print(c(f"Hadron-enriched eff:\n{eff_HLT_had_enr[MAGPOL]}").yellow)
    print(c(f"Full muonID eff:\n{eff_full_muon_id[MAGPOL]}").yellow)
    print(c(f"Efficiency central vals: {CVALS}").blue)
    print(c(f"Efficiency erros: {ERRS}").blue)

    plt.errorbar(x=eff_full_muon_id[MAGPOL].axes[0].centers, y=CVALS, 
                xerr=eff_full_muon_id[MAGPOL].axes[0].widths/2, 
                yerr=ERRS,
                fmt="x", markersize=5, elinewidth=1., color=color,
                label=MAGPOL)

plt.xlabel(rf"$p_T(X')$   [MeV/c]", loc="center")
plt.ylabel(r"$\varepsilon(\text{HLT} | \mu \, \text{L0MuonTOS}) / \varepsilon( \text{HLT} | B\, \text{L0GlobalTIS}\, \& \, \text{!isMuon})$", loc="center", fontsize=20)
plt.legend(loc="best", fontsize=13)

plot_name = f"eff_HLT_ratio"
plt.savefig(f"{plots_prefix}/{plot_name}.pdf")
plt.savefig(f"{plots_prefix}/{plot_name}.png")
print(c(f"Plots saved to {plots_prefix}/{plot_name}.pdf/png").blue)
