import ROOT as r
from argparse import ArgumentParser
from tabulate import tabulate
import os
import yaml 
import boost_histogram 
import uproot
import pyhf
import hist
import pickle
import numpy as np

#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif"
    })

# directory where to dump all the sh files
parser = ArgumentParser()
parser.add_argument('-i',  '--input', required=True , help='Input perHist.root path+file')
parser.add_argument('-p',  '--preserv', required=True , help='Input preserv.py file to retrieve the TH3F')
opts = parser.parse_args()

INPUT_ROOTFILE = opts.input
PRESERV_FILE   = opts.preserv
NAMESPACE, HEAD = os.path.split(PRESERV_FILE) # remove the yaml file

with open(PRESERV_FILE) as f_in:
  preserv_dict = yaml.load(f_in)


tf = r.TFile(INPUT_ROOTFILE)

# get the _All histogram
eff_hist = tf.Get(preserv_dict["eff_hist"])

# quickly draw the efficiencies
c = r.TCanvas('eff','eff',2400,600)
c.Divide(3,1)
c.cd(1)
eff_hist.ProjectionX().Draw("LEP") # should be P
c.cd(2)
eff_hist.ProjectionY().Draw("LEP") # should be ETA
c.cd(3)
eff_hist.ProjectionZ().Draw("LEP") # should be nTracks
c.Update()
c.Modified()
c.Print(f"{NAMESPACE}/projs1.pdf")
c.Close()

P_bins   = eff_hist.GetNbinsX()
ETA_bins = eff_hist.GetNbinsY()
nTr_bins = eff_hist.GetNbinsZ()

print(P_bins)
print(ETA_bins)
print(nTr_bins)
print_rows = []

# loop the bins
for xbin in range(1, P_bins+1):
  for ybin in range(1, ETA_bins+1):
    for zbin in range(1, nTr_bins+1):
      xcent = eff_hist.GetXaxis().GetBinCenter(xbin)
      xlow  = eff_hist.GetXaxis().GetBinLowEdge(xbin)
      xhgh  = eff_hist.GetXaxis().GetBinLowEdge(xbin+1)
      ycent = eff_hist.GetYaxis().GetBinCenter(ybin)
      ylow  = eff_hist.GetYaxis().GetBinLowEdge(ybin)
      yhgh  = eff_hist.GetYaxis().GetBinLowEdge(ybin+1)
      zcent = eff_hist.GetZaxis().GetBinCenter(zbin)
      zlow  = eff_hist.GetZaxis().GetBinLowEdge(zbin)
      zhgh  = eff_hist.GetZaxis().GetBinLowEdge(zbin+1)

      # get the bin content and error
      eff = eff_hist.GetBinContent(xbin,ybin,zbin)
      err = eff_hist.GetBinError(xbin,ybin,zbin)

      # check the pass and total (which indeed gives the efficiency back)

      print_rows.append( [ xbin, xcent, xlow, xhgh, ybin, ycent, ylow, yhgh, zbin, zcent, zlow, zhgh, eff, err ] )


eff_table = tabulate(print_rows, headers=['P bin', 'P cent','P low','P high', 'ETA bin', 'ETA cent', 'ETA low', 'ETA high', 'nTrk bin', 'nTrk cent', 'nTrk low', 'nTrk high', 'eff', 'err' ], tablefmt="latex" ) 
raw_eff_table = tabulate(print_rows, headers=['P bin', 'P cent','P low','P high', 'ETA bin', 'ETA cent', 'ETA low', 'ETA high', 'nTrk bin', 'nTrk cent', 'nTrk low', 'nTrk high', 'eff', 'err' ], tablefmt="github" ) 
print(eff_table, file=open(f"{NAMESPACE}/eff.tex", "w") )
print(raw_eff_table, file=open(f"{NAMESPACE}/eff.log", "w") )


# draw the Lucia-grams
# she does one per ETA bin so need to loop in y first
c2 = r.TCanvas("projs","projs",1600,1200)
c2.DivideSquare(ETA_bins)
projs = []
for ybin in range(1, ETA_bins+1):
  ylow  = eff_hist.GetYaxis().GetBinLowEdge(ybin)
  yhgh  = eff_hist.GetYaxis().GetBinLowEdge(ybin+1)
  # 2D projection I think should be (can check with numbers printed above)
  c2.cd(ybin)
  eff_hist.GetYaxis().SetRangeUser(ylow, yhgh)
  proj = eff_hist.Project3D("zx")
  proj.SetTitle(" {:.2f} < #eta < {:.2f}".format(ylow,yhgh) )
  proj.Draw("colztext")
  # keep object alive for the plot
  projs.append(proj)
  c2.Update()
  c2.Modified()


c2.Update()
c2.Modified()
c2.Print(f"{NAMESPACE}/projs2d.pdf")
c2.Close()


# === boost-based workflow ===
# useful for plotting, standardise the labels
labels = {}
labels["kaon"]       = "K"
labels["pion"]       = "\pi"
labels["proton"]     = "p"
labels["electron"]   = "e"
labels["muon"]       = "\mu"
labels["ghost"]       = "g"
labels["dllmu_high"] = "high DLL$\mu$ bin"
labels["dllmu_low"]  = "low DLL$\mu$ bin"

# read in file via uproot4 and then convert to boost histogram
eff_file = uproot.open(INPUT_ROOTFILE)
eff_hist = eff_file[preserv_dict["eff_hist"]]
bh = eff_hist.to_boost() # weighted bh with values = effs, weights = errs; THIS WAS CHECKED!

# assign labels to axes through metadata
bh.axes[0].metadata="$p$   [MeV]"
bh.axes[1].metadata="$\eta$"
bh.axes[2].metadata="nTracks"

# slice in eta
low_eta_cvals = bh[:,0,:]
high_eta_cvals = bh[:,1,:]

# pretty plots 
'''
# >>> low eta
f, axs = plt.subplots(1, 1, sharex=False, sharey=False, figsize=(20,20))
axs.set_title(rf"${labels[preserv_dict['real']]} \rightarrow {labels[preserv_dict['reco']]}$, $1.5<\eta<3.0$", fontsize=40)
hep.hist2dplot(low_eta_cvals.view().value, low_eta_cvals.axes[0].edges, 
              low_eta_cvals.axes[1].edges, labels=True, ax=axs, cmin=0.0);
axs.set_xlabel(low_eta_cvals.axes[0].metadata, fontsize=40)
axs.set_ylabel(low_eta_cvals.axes[1].metadata, fontsize=40)
plt.savefig(f"{NAMESPACE}/annotated_eff_table_low_eta.pdf")
plt.savefig(f"{NAMESPACE}/annotated_eff_table_low_eta.png")

# >>>  high eta
f, axs = plt.subplots(1, 1, sharex=False, sharey=False, figsize=(20,20))
axs.set_title(rf"${labels[preserv_dict['real']]} \rightarrow {labels[preserv_dict['reco']]}$, $3.0<\eta<5.0$", fontsize=40)
hep.hist2dplot(high_eta_cvals.view().value, high_eta_cvals.axes[0].edges, 
              high_eta_cvals.axes[1].edges, labels=True, ax=axs, cmin=0.0);
axs.set_xlabel(high_eta_cvals.axes[0].metadata, fontsize=40)
axs.set_ylabel(high_eta_cvals.axes[1].metadata, fontsize=40)
plt.savefig(f"{NAMESPACE}/annotated_eff_table_high_eta.pdf")
plt.savefig(f"{NAMESPACE}/annotated_eff_table_high_eta.png")

# >>> together
fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=[28, 10])

# low eta
im_low = ax0.pcolormesh(low_eta_cvals.axes[0].edges, low_eta_cvals.axes[1].edges, low_eta_cvals.view().value.T, cmap="YlGnBu")
fig.colorbar(im_low, ax=ax0).set_label("$\epsilon_{PID}$", fontsize=40)
ax0.set_xlabel(low_eta_cvals.axes[0].metadata, fontsize=40)
ax0.set_ylabel(low_eta_cvals.axes[1].metadata, fontsize=40)
ax0.set_title(rf"${labels[preserv_dict['real']]} \rightarrow {labels[preserv_dict['reco']]}$, $1.5<\eta<3.0$", fontsize=40)

# high eta
im_low = ax1.pcolormesh(high_eta_cvals.axes[0].edges, high_eta_cvals.axes[1].edges, high_eta_cvals.view().value.T, cmap="YlGnBu")
fig.colorbar(im_low, ax=ax1).set_label("$\epsilon_{PID}$", fontsize=40)
ax1.set_xlabel(high_eta_cvals.axes[0].metadata, fontsize=40)
ax1.set_ylabel(high_eta_cvals.axes[1].metadata, fontsize=40)
ax1.set_title(rf"${labels[preserv_dict['real']]} \rightarrow {labels[preserv_dict['reco']]}$, $3.0<\eta<5.0$", fontsize=40)
fig.subplots_adjust(wspace=10)

plt.savefig(f"{NAMESPACE}/eff_table.png")
plt.savefig(f"{NAMESPACE}/eff_table.pdf")
'''


# worth preserving the info of the boost-histogram
with open(f"{NAMESPACE}/eff_th3w.pkl", "wb") as f:
    pickle.dump(bh, f)