'''Executable to unfold the number of true hadrons, electrons and ghosts in the hadron-enriched sample.
Produces a unique Hist container holding the unfolded yields, in bins of kinematics and occupancy, from 
ML binned fits to the reco categories.

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from pyhf import Model
import json, requests, jsonschema

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
parser.add_argument(
    '-m',
    '--magpol',
    required=True,
    help='Magnet polarity',
    choices=[
        "MagUp",
        "MagDown"])
parser.add_argument(
    '-d',
    '--debug',
    action="store_true",
    help="Enable prints for debugging purposes")
opts = parser.parse_args()

#==========================================================================
# bookkeping
CONFIG_PATH = "/home/blaised/private/Bc2D0MuNuX/MisID/config.yml"
YEAR   = opts.year
MAGPOL = opts.magpol
DLLMU_BIN = "dllmu_hadron_enriched"
TOP_DIR = "exec"
MISID_DATA_PATH = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/hadron_enriched/DATA/merged_misid"
MODE = "D0MuNu"
PLOTS_PATH = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}/{MAGPOL}/fitplots"
FIT_SPEC_PATH = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}/{MAGPOL}/fitspecs"
IS_DEBUG = opts.debug
# where does the TeX report live and the output hist
_prefix = f"/home/blaised/private/Bc2D0MuNuX/MisID/scratch/hPID/{YEAR}/{MAGPOL}"
#==========================================================================


def build_template(alleffs, true_species, p_bin, eta_bin, ntr_bin, isnorm=True):
    """Ugly-ass function to project out the efficiencies for a given true species in bins
    of reco category, for a given bin in {p, eta, ntracks}
    
    returns: 
        1. np.array of central values of PID eff in bins of reco categories
        2. np.array of errors of PID eff in bins of reco categories
    
    parameters:
    -----------
    alleffs: 5D histogram containing with Weight storage containing all eff+/-err vals
    
    true_species: ['kaon', 'pion', 'proton', 'muon', 'electon', 'ghost'], the true particle/ghost 
    for which to query the effs in the various reco-categories
    
    p_bin, eta_bin, ntr_bin: indices of each bin of kinematics and occupancy
    
    isnorm: normalise to unity? [default: True]
    """
    true_species_proj_cvals = []
    true_species_proj_uncty = []
    
    for reco_cat in reco_species:
        true_species_proj_cvals.append(alleffs[f"{true_species}", reco_cat, p_bin, eta_bin, ntr_bin].value)
        true_species_proj_uncty.append(alleffs[f"{true_species}", reco_cat, p_bin, eta_bin, ntr_bin].variance**.5)
    
    true_species_proj_cvals = np.array(true_species_proj_cvals)
    true_species_proj_uncty = np.array(true_species_proj_uncty)
    
    # need to catch negative efficiencies!
    true_species_proj_cvals[true_species_proj_cvals<0] = 1e-9
    assert(true_species_proj_cvals.all()>=0) # efficiencies should all be >~0
    # stop if template is practically empty (at least one bin)
    #assert(true_species_proj_cvals.all()>2e-9), "ERROR: templates statistics below threshold 2e-9"
    
    
    if isnorm==True:
        true_species_proj_cvals/=np.sum(true_species_proj_cvals)
        true_species_proj_uncty/=np.sum(true_species_proj_cvals) #ratio of error to central value preserved
    
    # return the binned PID eff template, w/o nonzero values and then normalised to unity
    return true_species_proj_cvals, true_species_proj_uncty

def compute_cut_eff(_inclB2D0X, _per_bin_sel):
    '''
    the error estimate is performed via binomial errors
    this is valid unless k=N or k=0
    source: Paterno, FERMILAB-TM-2286-CD
    '''
    print(c(f"The numerator has this cut instead: {_per_bin_sel}").green)
    N = len(_inclB2D0X)
    k = len(_inclB2D0X.query(_per_bin_sel))
    eff = float(k/N)
    err = float( (1/N)*np.sqrt( k*(1-(k/N)) ) )

    return eff, err

def generate_templates(
    kaon_cvals, 
    pion_cvals,
    proton_cvals,
    # electron_cvals,
    # ghost_cvals
    ):
    """build the necessary dicts to generate the templates in the pyhf schema
    
    parameters:
    -----------
    per-species bin central values; let the overall template normalisation float and 
    not implement uncorrelated uncties in the binned templates
    """
    kaon_template = {
        "name": "kaon_calib_template",
        "data": (kaon_cvals).tolist(),
        "modifiers": [
            {"name": "K_y", "type": "normfactor", "data": None }, # floating normalisation
            ]
    }
    pion_template = {
        "name": "pion_calib_template",
        "data": (pion_cvals).tolist(),
        "modifiers": [
            {"name": "pi_y", "type": "normfactor", "data": None }, # floating normalisation
            ]
        }
    proton_template = {
        "name": "proton_calib_template",
        "data": (proton_cvals).tolist(),
        "modifiers": [
            {"name": "p_y", "type": "normfactor", "data": None }, # floating normalisation
            ]
        }
    # electron_template = {
    #     "name": "electron_calib_template",
    #     "data": (electron_cvals).tolist(),
    #     "modifiers": [
    #         {"name": "e_y", "type": "normfactor", "data": None }, # floating normalisation
    #         ]
    # }
    # ghost_template = {
    #     "name": "ghost_calib_template",
    #     "data": (ghost_cvals).tolist(),
    #     "modifiers": [
    #         {"name": "g_y", "type": "normfactor", "data": None }, # floating normalisation
    #         ]
    # }
    #return [kaon_template, pion_template, proton_template, electron_template, ghost_template]
    return [kaon_template, pion_template, proton_template]


# used later, cosmetics
labels = {}
labels["dllmu_bin"]  = "DLL$\mu<0$"
labels["kaon"]       = "K"
labels["proton"]     = "p"
labels["pion"]       = "\pi"
#labels["electron"]   = "e"
#labels["ghost"]      = "g"

with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)

# partitions of the data into reco categories
binning       = config["pid"]["binning"]
reco_species  = config["reco_cuts"].keys() # proton, kaon, pion, electron, ghost
calib_species = config["pid"]["species"].keys() # pion, kaon, proton, electron
data_cuts     = config["data_cuts"] # partition of hadron-enriched data into high-purity PID reco categories

# loop over the reco cats and read in the eff.pkl ("what is the eff of a true kaon to be reconstructed as p,k,pi,e,g")
pid_eff_dict = {} # dict of PIDCalib efficiencies, these will make the binned templates
for real in calib_species:
    pid_eff_dict[real] = {}
    for reco in reco_species:
        pkl =  f"{TOP_DIR}/{YEAR}/{MAGPOL}/{DLLMU_BIN}/{real}/misID_{real}_to{reco}_{DLLMU_BIN}_{YEAR}_{MAGPOL}/eff_th3w.pkl"
        with open(pkl, "rb") as f_in: pid_eff_dict[real][reco] = pickle.load(f_in)

COLUMNS=["Mu_plus_P", "nTracks", "Mu_plus_LK_ETA", "Mu_plus_PIDmu", "Mu_plus_ProbNNghost", "Mu_plus_PIDK", "Mu_plus_PIDe", "Mu_plus_PIDp", "fakemuon_w", "Mu_plus_ID", "K_minus_ID"]

# read in misID-hadron-enriched data
if MAGPOL=="MagUp": polarity="MU"
if MAGPOL=="MagDown": polarity="MD"
misid_f_in = uproot.open(f"{MISID_DATA_PATH}/{MODE}/{YEAR}/{polarity}/Bc2D0MuNuX.root:DecayTree")
misid_df = misid_f_in.arrays(COLUMNS, "K_minus_ID*Mu_plus_ID>0", library="pd")

# check that only fakemuons contribute to the hadron-enriched DCS sample 
assert(len(misid_df.query("fakemuon_w==50")) == len(misid_df))
assert((misid_df["K_minus_ID"]*misid_df["Mu_plus_ID"]).all()>0)

# intialise the Hist container
full_eff_hist = Hist(
    hist.axis.StrCategory(calib_species,     name="calib"),
    hist.axis.StrCategory(reco_species,      name="reco",      label=f"reco {reco_species}"),
    hist.axis.Variable(binning["p"],         name="p",         label=r"$p$   [MeV$/c$]"),
    hist.axis.Variable(binning["eta"],       name="eta",       label=r"$\eta$"),
    hist.axis.Variable(binning["ntracks"],   name="ntracks",   label="nTracks"),
    storage=hist.storage.Weight()
)

# now fill the efficiency dict 
for c in pid_eff_dict.keys(): # loop the calibration samples
    for r in pid_eff_dict[c]: # loop over reco categories for a given calibration sample
        full_eff_hist[f"{c}", f"{r}", ...]  = pid_eff_dict[f"{c}"][f"{r}"].view()

# # now we need to build the ghost-specific template;load the MC and fill, per reco category, efficiencies
# # NOTE: this uses the inclusive MC produced by Alison, of which we have only 2016 sample sat time of development + **these need PID correction** [an issue has been opened]
# in_RDF = RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_{MAGPOL}_5M.root").Filter(f"Mu_plus_TRUEID==0 && Mu_plus_MC_MOTHER_ID==0")
# inclB2D0X = pandas.DataFrame( in_RDF.AsNumpy(columns=["Mu_plus_PIDe", "Mu_plus_PIDmu", "Mu_plus_PIDK", "Mu_plus_PIDp", "Mu_plus_MC15TuneV1_ProbNNghost", "Mu_plus_P", "Mu_plus_LK_ETA", "nTracks", "Mu_plus_isMuon", "Mu_plus_InMuonAcc", "Mu_plus_NShared"]) ) 

# === book dedicated histograms for ghosts and hadron-enriched data ===
# ghost_hist = Hist(
#     hist.axis.StrCategory(["ghost"],         name="ghost_from_sim"),
#     hist.axis.StrCategory(reco_species,      name="reco",      label=f"reco {reco_species}"),
#     hist.axis.Variable(binning["p"],         name="p",         label=r"$p$   [MeV$/c$]"),
#     hist.axis.Variable(binning["eta"],       name="eta",       label=r"$\eta$"),
#     hist.axis.Variable(binning["ntracks"],   name="ntracks",   label="nTracks"),
#     storage=hist.storage.Weight()
# )
data_hist = Hist(
    hist.axis.StrCategory(reco_species,      name="reco",      label=f"reco {reco_species}"),
    hist.axis.Variable(binning["p"],         name="p",         label="$p$   [MeV$/c$]"),
    hist.axis.Variable(binning["eta"],       name="eta",       label="$\eta$"),
    hist.axis.Variable(binning["ntracks"],   name="ntracks",   label="nTracks"),
)

# separate hist for the normalisation of the PID weights: #event in a bin of p, eta, ntracks
# this is to account for the fact the reco categories might not capture the full sample size
# and this must be the normalisation used to extract w_PID
normalisation_hist = Hist(
    hist.axis.StrCategory(reco_species,      name="reco",      label=f"reco {reco_species}"),
    hist.axis.Variable(binning["p"],         name="p",         label="$p$   [MeV$/c$]"),
    hist.axis.Variable(binning["eta"],       name="eta",       label="$\eta$"),
    hist.axis.Variable(binning["ntracks"],   name="ntracks",   label="nTracks"),
)

from termcolor2 import c
# fill: loop through all the partitions
data_pop_counter = 0
for reco_category in reco_species:
    reco_string = data_cuts[reco_category].replace("&&", "and")    

    for u in range(len(binning["p"])-1):
        low_momentum_bin = binning["p"][u]
        high_momentum_bin = binning["p"][u+1]
        
        for v in range(len(binning["eta"])-1):
            low_eta_bin = binning["eta"][v]
            high_eta_bin = binning["eta"][v+1]
        
            for w in range(len(binning["ntracks"])-1):
                low_ntracks_bin = binning["ntracks"][w]
                high_ntracks_bin = binning["ntracks"][w+1]
                
                # now partition the misID data
                # my understanding is that somehow PIDCalib implements closed intervals - odd, but should not make a massive difference
                per_bin_sel = f"{reco_string} and Mu_plus_P>={low_momentum_bin} and Mu_plus_P<={high_momentum_bin} and Mu_plus_LK_ETA>={low_eta_bin} and Mu_plus_LK_ETA<={high_eta_bin} and nTracks>={low_ntracks_bin} and nTracks<={high_ntracks_bin}" #tuples don't have isMuon saved as a branch, but the data has been double-checked to have cuts consistent with hadron-enriched

                print(c(f"\n\n{reco_category}: bin [{u}, {v}, {w}]").cyan.underline)
                if IS_DEBUG is True:
                    print(c("\nEntered data/ghost hist builder loop").magenta.bold)
                    print(c(f"Studying the {reco_category} reco category in a kin/occpcy bin").magenta.bold)
                    print(c(f"Reco catgeory string: {per_bin_sel} for data in that bin (+ !isMuon & hasMuon & DLLmu<0 & NShared==0\n").magenta)
                
                data_pop = len(misid_df.query(f"{per_bin_sel}"))
                data_hist[reco_category, u, v, w ] = data_pop
                print(c(f"Population from adding the reco samples size: {data_pop}\n").bold)           
                data_pop_counter+=data_pop  
                   
                # assign the normalistaion
                pop_kin_occpcy_bin = len(misid_df.query(f"Mu_plus_P>={low_momentum_bin} and Mu_plus_P<={high_momentum_bin} and Mu_plus_LK_ETA>={low_eta_bin} and Mu_plus_LK_ETA<={high_eta_bin} and nTracks>={low_ntracks_bin} and nTracks<={high_ntracks_bin}"))
                normalisation_hist[reco_category, u, v, w ] = pop_kin_occpcy_bin 
                print(c(f"Data population in the chosen bin of kinms and occupcy: {pop_kin_occpcy_bin}\n").blue)           

                # # ghost 
                # ghost_per_bin_sel = f"{per_bin_sel} and Mu_plus_isMuon==0 and Mu_plus_InMuonAcc==1.0 and Mu_plus_NShared==0"
                # ghost_per_bin_sel = ghost_per_bin_sel.replace("Mu_plus_ProbNNghost", "Mu_plus_MC15TuneV1_ProbNNghost")
                # # print(c(f"Ghost PID string: {ghost_per_bin_sel}\n").yellow)

                # raw_ghost_per_bin_sel = f"Mu_plus_P>={low_momentum_bin} and Mu_plus_P<={high_momentum_bin} and Mu_plus_LK_ETA>={low_eta_bin} and Mu_plus_LK_ETA<={high_eta_bin} and nTracks>={low_ntracks_bin} and nTracks<={high_ntracks_bin} and Mu_plus_InMuonAcc==1.0 and Mu_plus_NShared==0"

                # if IS_DEBUG is True: 
                #     print(c(f"Note: this blanket cut is applied to the denominator of the ghost->reco effs: {raw_ghost_per_bin_sel}").red)

                # eff, err = compute_cut_eff(_inclB2D0X=inclB2D0X.query(raw_ghost_per_bin_sel), _per_bin_sel=ghost_per_bin_sel)
                # ghost_hist["ghost", reco_category, u, v, w] = [eff, err**2]
    print(c("--------------------------------------------------------------\n").yellow)

print(c("\nSuccess: all histograms have been filled").cyan.underline)

print(c("\nTemplates consistency check:").underline)
print(c("PIDCalib").bold)
print(full_eff_hist)
# print(c("Ghost from inclusive MC").bold)
# print(ghost_hist)
print(c("Hadron-enriched data split in reco cats").bold)
print(data_hist)
print(c(f"Data population counter by hand: {data_pop_counter}").magenta)
print(c("Now check the number of events in data, in each bin").bold)
print(normalisation_hist)


# intialise the final output hist
"""In last axis, store what is needed for the final hadron-enriched PID (hPID) weights:
per p, eta, ntr and one of {K,pi,p,e,g}, store:

params:
--------
PID_template: the integral of the *unnormalised* binned pdf obtained from PIDCalib+incl MC - e.g. the eff for a kaon to be reconstructed in each reco category

reco_events: the number of events in a given observed reco category - e.g. the number of events in the kaon-like reco partition of hadron-enriched data

unfolded_yield: the overall abundance of a species obtained from the binned ML fit, i.e. the extracted yield - e.g. how many true kaons make up a sample of hadron-enriched data in a bin of p, eta, ntracks
"""
out_hist = Hist(
    hist.axis.Variable(binning["p"],            name="p",         ),
    hist.axis.Variable(binning["eta"],          name="eta",       ),
    hist.axis.Variable(binning["ntracks"],      name="ntracks",   ),
    hist.axis.StrCategory(reco_species,         name="species"), 
    hist.axis.StrCategory(["integr_PID_template", "reco_nevents", "unfolded_yield"],  name="wPID"), 
    storage=hist.storage.Weight() # making life easier: store only central vals
)

# where do the outputs live
os.system(f"mkdir -p {FIT_SPEC_PATH}")
os.system(f"mkdir -p {PLOTS_PATH}")

# === compile a tex/pdf summary of plots ===
headers =["Species", "Yield", "Error"]
to_tex = open(f"{_prefix}/hPID_fits_report.tex", "w")
preamble=r'''\documentclass[11pt]{article}
\usepackage{graphicx}

\begin{document}
\section{PID fits to hadron-enriched %s %s data}
'''%(YEAR, MAGPOL)
to_tex.write(preamble)

# === Fit, plot, TeX, fill loop ===
NORM=True # fit using normalised templates  
for _p_bin in range(len(binning["p"])-1):
    for _eta_bin in range(len(binning["eta"])-1):
        for _ntr_bin in range(len(binning["ntracks"])-1):
            print(c(f"\nFitting data in bin: [P = {_p_bin} : ETA = {_eta_bin} : nTracks = {_ntr_bin}]").red.underline)

            # addendum: before normalising templates, store info in out_hist
            # from PIDCalib
            #for s in ["kaon", "pion", "proton", "electron"]:
            for s in ["kaon", "pion", "proton"]:
                # store the unnormalised PID template integrals in out_hist
                out_hist[_p_bin, _eta_bin, _ntr_bin, s, "integr_PID_template"] = [np.sum(full_eff_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value), 1e-9] # don't need to ge the err on the integral
                print(c(f"{s} PDF integral: {np.sum(full_eff_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value)}").red.dark)
                # store the observed events in each reco bin of hadron-enriched data
                out_hist[_p_bin, _eta_bin, _ntr_bin, s, "reco_nevents"] = [ normalisation_hist[s, _p_bin, _eta_bin, _ntr_bin], np.sqrt(normalisation_hist[s, _p_bin, _eta_bin, _ntr_bin]) ] # store poisson variance
            
            # # derived from incl MC and stored in ghost_hist
            # for s in ["ghost"]: 
            #     # store the unnormalised PID template integrals in out_hist
            #     out_hist[_p_bin, _eta_bin, _ntr_bin, s, "integr_PID_template"] = [np.sum(ghost_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value), 1e-9] # don't need to ge the err on the integral
            #     print(c(f"{s} PDF integral: {np.sum(ghost_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value)}").red.dark)
            #     # store the observed events in each reco bin of hadron-enriched data
            #     out_hist[_p_bin, _eta_bin, _ntr_bin, s, "reco_nevents"] = [ data_hist[s, _p_bin, _eta_bin, _ntr_bin], data_hist[s, _p_bin, _eta_bin, _ntr_bin] ] # store poisson variance
            
            if IS_DEBUG is True:
                print(c(f"Random entry in data population hist: kaon: {out_hist[0,0,0,'kaon','reco_nevents'].value}").green)
                print(c(f"Random entry in data population hist: pion: {out_hist[0,0,0,'pion','reco_nevents'].value}").green)
                print()
                #for s in ["kaon", "pion", "proton", "electron"]:
                for s in ["kaon", "pion", "proton"]:
                    print(c(f"Debugging layer: inspecting the PIDCalib {s} template").magenta.bold)
                    print(c(f"{full_eff_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value}").magenta)
                    print(c(f"Unnormalised integral = {np.sum(full_eff_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value)}").magenta)
                
                # for s in ["ghost"]:
                #     print(c(f"Debugging layer: inspecting the MC-derived {s} template").magenta.bold)
                #     print(c(f"{ghost_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value}").magenta)
                #     print(c(f"Unnormalised integral = {np.sum(ghost_hist[s, :, _p_bin, _eta_bin, _ntr_bin].view().value)}").magenta)

            # PIDCalib-derived templates
            kaon_template_cvals, kaon_template_uncties  = build_template(full_eff_hist, "kaon", p_bin=_p_bin, eta_bin=_eta_bin, ntr_bin=_ntr_bin, isnorm=NORM)
            pion_template_cvals, pion_template_uncties  = build_template(full_eff_hist, "pion", p_bin=_p_bin, eta_bin=_eta_bin, ntr_bin=_ntr_bin, isnorm=NORM)
            proton_template_cvals, proton_template_uncties  = build_template(full_eff_hist, "proton", p_bin=_p_bin, eta_bin=_eta_bin, ntr_bin=_ntr_bin, isnorm=NORM)
            # electron_template_cvals, electron_template_uncties  = build_template(full_eff_hist, "electron", p_bin=_p_bin, eta_bin=_eta_bin, ntr_bin=_ntr_bin, isnorm=NORM)

            # # MC-derived ghost template
            # ghost_template_cvals, ghost_template_uncties  = build_template(ghost_hist, "ghost", p_bin=_p_bin, eta_bin=_eta_bin, ntr_bin=_ntr_bin, isnorm=NORM)
            
            if IS_DEBUG is True:
                print(c("Now verify that the binned templates in the fit are normalised:").magenta.bold)
                #for t in [kaon_template_cvals, pion_template_cvals, proton_template_cvals, electron_template_cvals, ghost_template_cvals]:
                for t in [kaon_template_cvals, pion_template_cvals, proton_template_cvals]:
                    print(c(f"{np.sum(t)}").magenta)

            print(c("All binned templates have been built, normalised to unity").cyan)
            print(c("Start building the pyhf schema ").yellow)

            print(c("\nTemplates:").underline)
            print(c("Kaon").bold)
            print(kaon_template_cvals)
            print(c("Pion").bold)
            print(pion_template_cvals)
            print(c("Proton").bold)
            print(proton_template_cvals)
            # print(c("Electron").bold)
            # print(electron_template_cvals)
            # print(c("Ghost").bold)
            # print(ghost_template_cvals)
            obs=data_hist[..., _p_bin, _eta_bin, _ntr_bin]
            print(c("\nObservation").bold)
            print(obs)
            
            # buid the pyhf schema
            spec = {
                "channels": [
                    {
                        "name": "singlechannel",
                        "samples":  generate_templates(
                            kaon_template_cvals,
                            pion_template_cvals,
                            proton_template_cvals,
                            # electron_template_cvals,
                            # ghost_template_cvals
                        )
                    },
                ],
                "observations": [
                    {
                        "name": "singlechannel",
                        "data": obs.view().tolist(),
                    },
                ],
                "measurements": [
                    { 
                        "name": "true_species_y_extraction",
                        "config": {
                            "poi": "pi_y",
                            "parameters": [
                                # bounds on floatig normalisations 
                                {"name":"K_y",   "bounds": [[ 0, obs.sum() ]], "inits":[obs["kaon", ...]] },
                                {"name":"pi_y",  "bounds": [[ 0, obs.sum() ]], "inits":[obs["pion", ...]] },
                                {"name":"p_y",   "bounds": [[ 0, obs.sum() ]], "inits":[obs["proton", ...]] },
                                # {"name":"e_y",   "bounds": [[ 0, obs.sum() ]], "inits":[obs["electron", ...]] },
                                # {"name":"g_y",   "bounds": [[ 0, obs.sum() ]], "inits":[obs["ghost", ...]] },
                            ]
                        }
                    }
                ],
                "version": "1.0.0"
            }
            #write schema and verify correctness of syntax
            spec = str(json.dumps(spec, indent=4)).replace("None", "null")
            with open(f"{FIT_SPEC_PATH}/model_spec_{_p_bin}_{_eta_bin}_{_ntr_bin}.json","w") as outfile:
                outfile.write(spec)
            # validate schema
            workspace = json.load(open(f"{FIT_SPEC_PATH}/model_spec_{_p_bin}_{_eta_bin}_{_ntr_bin}.json"))
            schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
            # If no exception is raised by validate(), the instance is valid.
            jsonschema.validate(instance=workspace, schema=schema)
            print(c("\nSUCCESS: validation of workspace complete").yellow)
            workspace = pyhf.Workspace(workspace)
            print(c("SUCCESS: workspace read in").yellow)

            # FIT!
            pdf = workspace.model(measurement_name = "true_species_y_extraction")
            data = workspace.data(pdf)
            fit_result, likelihood= pyhf.infer.mle.fit(data, pdf, return_fitted_val=True,return_uncertainties=True)
            best_fit = pyhf.infer.mle.fit(data, pdf)
            bestfit_pars, par_uncerts = fit_result.T
            print(c("Fit complete").green)

            # === FIT RESULTS ===
            par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}

            print(c("\nResults:").blue.underline)
            PRESCALED_FIT_RESULTS = {}
            tab_body = {}
            for k, v in par_name_dict.items():
                PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
                tab_body[k] = [k, bestfit_pars[v], par_uncerts[v] ]
                print(c(f"Extracted normalisation of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}").blue)

            # store in out hist the extracted central values of the yields; store variance;
            out_hist[_p_bin, _eta_bin, _ntr_bin, "kaon", "unfolded_yield"]     =  [bestfit_pars[par_name_dict["K_y"]], par_uncerts[par_name_dict["K_y"]]**2 ]
            out_hist[_p_bin, _eta_bin, _ntr_bin, "pion", "unfolded_yield"]     =  [bestfit_pars[par_name_dict["pi_y"]],par_uncerts[par_name_dict["pi_y"]]**2]
            out_hist[_p_bin, _eta_bin, _ntr_bin, "proton", "unfolded_yield"]   =  [bestfit_pars[par_name_dict["p_y"]], par_uncerts[par_name_dict["p_y"]]**2 ]
            # out_hist[_p_bin, _eta_bin, _ntr_bin, "electron", "unfolded_yield"] =  [bestfit_pars[par_name_dict["e_y"]], par_uncerts[par_name_dict["e_y"]]**2 ]
            # out_hist[_p_bin, _eta_bin, _ntr_bin, "ghost", "unfolded_yield"]    =  [bestfit_pars[par_name_dict["g_y"]], par_uncerts[par_name_dict["g_y"]]**2 ]
            
            # write extracted yields to TeX
            table = list(tab_body.values())
            tex_table = tabulate(table, headers, tablefmt="latex")

            # === PLOTS ===
            import warnings
            warnings.filterwarnings("ignore")

            plt.rc("axes", labelweight="bold")
            # now proceed to plot
            indxs = (_p_bin, _eta_bin, _ntr_bin)

            # label the bin
            P_EDGE_LO = data_hist.axes[1].edges[indxs[0]]
            P_EDGE_HI = data_hist.axes[1].edges[indxs[0]+1]

            ETA_EDGE_LO = data_hist.axes[2].edges[indxs[1]]
            ETA_EDGE_HI = data_hist.axes[2].edges[indxs[1]+1]

            NTR_EDGE_LO = data_hist.axes[3].edges[indxs[2]]
            NTR_EDGE_HI = data_hist.axes[3].edges[indxs[2]+1]

            # scale templates to the yields
            fit_k   = kaon_template_cvals * bestfit_pars[0]
            fit_pi  = pion_template_cvals * bestfit_pars[1]
            fit_p   = proton_template_cvals * bestfit_pars[2]
            # fit_e   = electron_template_cvals * bestfit_pars[3]
            # fit_g   = ghost_template_cvals * bestfit_pars[4]

            # check that the extracted yields add up to the observed event count
            print(c(f"Difference between fitted yields and obs: {np.abs( np.sum([fit_k, fit_pi, fit_p]) - np.sum(obs.view()) )}").red.bold)


            try:
                #fig = plt.figure(figsize=[14, 8])
                fig = plt.figure(figsize=[10, 8])
                gs = gridspec.GridSpec(ncols=1, nrows=2, height_ratios=[5, 1])

                # plot the data first
                ax = plt.subplot(gs[0])
                plt.minorticks_on()
                plt.tick_params(axis='both', which='major', direction="in")
                plt.tick_params(axis='both', which='minor', direction="in")
                xvals = np.arange(len(obs.view()))
                ax.errorbar(x=xvals, y=obs.view(), yerr=np.sqrt(obs.view()), xerr=.4, fmt=".", color="black", 
                            elinewidth=1.5, markersize=10, label=f"LHCb DCS {YEAR} {MAGPOL}")

                # templates
                ax.bar(x=xvals,height=fit_k, label="Kaon", bottom=[0], 
                        color="#1f78b4", width=.8, alpha=1.)
                ax.bar(x=xvals,height=fit_pi, label="Pion", bottom=fit_k, 
                        color="#a6cee3", width=.8, alpha=1.)
                ax.bar(x=xvals,height=fit_p, label="Proton", bottom=fit_k+fit_pi, 
                        color="#ff7f00", width=.8, alpha=1.)
                # ax.bar(x=xvals,height=fit_e, label="Electron", bottom=fit_k+fit_pi+fit_p, 
                #         color="#3690c0", width=.8, alpha=.7)
                # ax.bar(x=xvals,height=fit_g, label="Ghost", bottom=fit_k+fit_pi+fit_p+fit_e, 
                #         color="#a1d99b", width=.8, alpha=.7)

                # label bin of kins and occpy
                ax.plot([], [], "", label=f"{labels['dllmu_bin']}", color="white")
                ax.plot([], [], "", label=rf"{int(P_EDGE_LO)} MeV $\leq p \leq $ {int(P_EDGE_HI)} MeV", color="white")
                ax.plot([], [], "", label=rf"{ETA_EDGE_LO} $\leq \eta \leq $ {ETA_EDGE_HI} ", color="white")
                ax.plot([], [], "", label=rf"{int(NTR_EDGE_LO)} $\leq$ nTracks $\leq$ {int(NTR_EDGE_HI)}", color="white")
                ax.plot([], [], "", label="\n", color="white")
                #plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', fontsize=20)
                ax.legend(fontsize=18, ncol=2, loc="upper center")

                ax.set_ylabel("Events", loc="center")
                ax.set_ylim(0, 1100)
                # pull plot (missing errors on pulls)
                ax1 = plt.subplot(gs[1])

                PULLS = (obs.view() / pdf.expected_data(bestfit_pars, include_auxdata=False))
                plt.errorbar(x=xvals, y=PULLS, alpha=1.,  color="black", fmt=".", xerr=.4, elinewidth=2, markersize=10, 
                            yerr=np.sqrt(obs.view())/pdf.expected_data(bestfit_pars, include_auxdata=False))

                plt.ylim(0.99, 1.01)
                plt.minorticks_on()
                plt.axhline(y = 1.0 , color = 'red', linestyle = '--', linewidth=1, alpha=1.) 

                ll = ["", r"$K$", r"$\pi$", r"$p$",  r"$e$", "$g$"]
                ax1.set_xticklabels(ll)
                ax.set_xticklabels(ll)
                ax.get_shared_x_axes().join(ax, ax1)
                ax.set_xticklabels([])
                plt.ylabel(r"$\frac{\text{Data}}{\text{Fit}}$", loc="center", fontsize=45)
                plt.subplots_adjust(hspace=.0)

                plt.savefig(f"{PLOTS_PATH}/PID_eff_fit_{_p_bin}_{_eta_bin}_{_ntr_bin}.pdf")
                plt.savefig(f"{PLOTS_PATH}/PID_eff_fit_{_p_bin}_{_eta_bin}_{_ntr_bin}.png")
                print(c(f"Plots written to file: {PLOTS_PATH}/PID_eff_fit_{_p_bin}_{_eta_bin}_{_ntr_bin}.pdf/png").blue.dark)

                # tex 
                to_tex.write(r"\textbf{Kinematics and occupancy bin coordinates:}\\"+"\n")
                to_tex.write(rf"{int(P_EDGE_LO)} MeV $\leq p \leq $ {int(P_EDGE_HI)} MeV\\"+"\n")
                to_tex.write(rf"{ETA_EDGE_LO} $\leq \eta \leq $ {ETA_EDGE_HI}\\"+"\n")
                to_tex.write(rf"{int(NTR_EDGE_LO)} $\leq$ nTracks $\leq$ {int(NTR_EDGE_HI)}\\"+"\n")
                to_tex.write(r"\begin{figure}[h]"+"\n")
                #to_tex.write(r"\centering"+"\n")
                fit_img = f"{PLOTS_PATH}/PID_eff_fit_{_p_bin}_{_eta_bin}_{_ntr_bin}.pdf"
                to_tex.write(r"\includegraphics[scale=.4]{%s}"%fit_img+"\n")
                to_tex.write(r"\caption{Binned fits to \textit{reco} partitions of hadron-enriched data. The binned $K, \pi, p, e$ templates are derived from PIDCalib, while the binned $g$ template is derived from inclusive $B^+ \rightarrow D^0 X$ simulation. The bin of $p, \eta$ and nTracks used in the fit is listed in the legend.}")
                to_tex.write("\end{figure}\n")
                tex_table = tabulate(table, headers, tablefmt="latex") 
                to_tex.write(r"\begin{center}"+"\n")
                to_tex.write(rf"{{{tex_table}}}")
                to_tex.write(r"\end{center}"+"\n")
                to_tex.write(r"\clearpage")
                to_tex.write(r"\newpage")
            except:
                pass


to_tex.write(r"\end{document}")
to_tex.close()

os.system(f"pdflatex -output-directory={_prefix} {_prefix}/hPID_fits_report.tex")
os.system(f"rm {_prefix}/hPID_fits_report.log {_prefix}/hPID_fits_report.aux")

# save out_hist to file
os.system(f"mkdir -p {_prefix}/pkl")
with open(f"{_prefix}/pkl/hPID_eff_N_y.pkl", "wb") as f:
    pickle.dump(out_hist, f)