'''Extract from simulation the eff(L0MuonTOS && Mu_isMuon);
The effs are extracted in bins of pT, as both ID criteria are a function of the muon track
transverse momentum.

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''
from header import *
from pyhf import Model
import json, requests, jsonschema
ROOT.EnableImplicitMT()

# wildcards
parser = ArgumentParser()
parser.add_argument(
    '-y',
    '--year',
    required=True,
    help='Year of data-taking',
    choices=[
        "2011",
        "2012",
        "2015",
        "2016",
        "2017",
        "2018"])
opts = parser.parse_args()

#==========================================================================
# bookkeping
#TRUTHMATCHING = "( abs(Bplus_TRUEID)==521 && J_psi_1S_TRUEID==443 && abs(J_psi_1S_MC_MOTHER_ID)==521 && abs(muplus_TRUEID)==13 && muplus_MC_MOTHER_ID==443 && abs(muminus_TRUEID)==13 && muminus_MC_MOTHER_ID==443 && ( abs(Kplus_TRUEID)==321 || abs(Kplus_TRUEID)==211 || abs(Kplus_TRUEID)==0 || abs(Kplus_TRUEID)==11 || abs(Kplus_TRUEID)==2212 ) && abs(Kplus_MC_MOTHER_ID)==521 && Bplus_BKGCAT==0 )"
TRUTHMATCHING = "( (abs(Mu_plus_TRUEID)==321) || (abs(Mu_plus_TRUEID)==211) || (abs(Mu_plus_TRUEID)==0) || (abs(Mu_plus_TRUEID)==11) || (abs(Mu_plus_TRUEID)==2212) )"
MOMENTUM_CUTS = "(Mu_plus_PT>1500) && (Mu_plus_P>10e3) && (Mu_plus_P<100e3)"
EXTRA_CUTS = "(Mu_plus_hasMuon) && (Mu_plus_NShared==0) && (Mu_plus_PIDmu>3)"

CONFIG_PATH = "/usera/delaney/private/Bc2D0MuNuX/MisID/config.yml"
YEAR   = opts.year
MAGPOLS = ["MagUp","MagDown"]
DOMAIN = "muonID"
plots_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/{DOMAIN}/{YEAR}/plots"
ADD_B2JPSIK = False
#==========================================================================

def tohist(cvals, nbins=10, _MIN_PT=1_500, _MAX_PT=40_000, _MID_PT=15_000, _MID_HIGH=None, _name="muonID_pt"):
    '''utility fn to book hist of IDd events ad a fucntion of pt'''
    
    if _MID_HIGH is None: pt_edges = np.append(np.linspace(start=_MIN_PT, stop=_MID_PT, num=nbins-1), [_MAX_PT])# variable edges for pt
    else: pt_edges = np.append(np.linspace(start=_MIN_PT, stop=_MID_PT, num=nbins-2), [_MID_HIGH, _MAX_PT])# variable edges for pt
    h = Hist( 
        hist.axis.Variable(pt_edges, 
        label=r"$p_T(\mu^{+})$   [MeV/c]", 
        name=_name, 
        underflow=False, 
        overflow=False),  
        storage=hist.storage.Double()
    )
    h.fill( cvals )
    return h

def eff_std(N, k):
    '''Utility function to compute the eff+/- err
    the error estimate is performed via binomial errors
    this is valid unless k=N or k=0
    source: Paterno, FERMILAB-TM-2286-CD
    '''
    std = (1/N)*np.sqrt(k*(1-(k/N)))
    cval = k/N
    return std

with open(f"{CONFIG_PATH}") as in_cf:
    config = yaml.safe_load(in_cf)

_ = plt.figure(figsize=(9, 7));
plt.plot([], [], "", label=f"LHCb 2016 Simulation, Unofficial", color="white")

for MAGPOL in MAGPOLS:
    # if MAGPOL=="MagUp": mag="MU"
    # if MAGPOL=="MagDown": mag="MD"
    #NTUPLE_PATH = f"/r01/lhcb/Bc2D0MuNuX/pipeline/muonID_eff_studies/MC/sel/D0MuNu/2018/{mag}"
    
    REMOTE_PATH = f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_{MAGPOL}_5M.root"
    #REMOTE_PATH = f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/{MAGPOL}/B2JpsimumuK.root"
    pkl_prefix = f"/usera/delaney/private/Bc2D0MuNuX/MisID/scratch/{DOMAIN}/{YEAR}/{MAGPOL}/pkl"
    
    # read in the MC 
    # events = uproot.open(f"{REMOTE_PATH}:B2DMuNuX_D02KPiTuple/DecayTree")
    # BcMC = events.arrays(["Mu_plus_PT", "Mu_plus_isMuon", "Mu_plus_L0MuonDecision_TOS", "Mu_plus_TRUEID"],
    #                         cut = f"{TRUTHMATCHING} & {MOMENTUM_CUTS} & {EXTRA_CUTS} & {HLT1_CUTS} & {HLT2_CUTS}", 
    #                         library="pd")
    BcMC = pandas.DataFrame(RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"{REMOTE_PATH}").Filter(f"{TRUTHMATCHING} && {MOMENTUM_CUTS} && {EXTRA_CUTS}").AsNumpy(), columns=["Mu_plus_PT", "Mu_plus_isMuon", "Mu_plus_L0MuonDecision_TOS", "Mu_plus_TRUEID"])
    
    print(c(f"\nDebug layer: instances of muon track ID").cyan.underline)
    print(BcMC["Mu_plus_TRUEID"].value_counts())


    PAR = "Mu_plus_PT"
    MIN_PT=config["b2jpsik"]["MIN_PT"] # from selection
    MID_PT=8_000
    MAX_PT=config["b2jpsik"]["MAX_PT"] # from selection
    NBINS=7
    MID_HIGH=None
    
    pt_edges = np.append(np.linspace(start=MIN_PT, stop=MID_PT, num=NBINS-1), [MAX_PT])
    nocuts_hist = tohist(cvals=BcMC[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)
    #muon_id_hist= tohist(cvals=BcMC.query("Mu_plus_L0MuonDecision_TOS==True and Mu_plus_isMuon==True")[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)
    
    muon_id_hist= tohist(cvals=BcMC.query("Mu_plus_isMuon==True & Mu_plus_L0MuonDecision_TOS==True")[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)

    print(c(f"\nRaw simulated events with kin/occpcy + truthmatching # events : \n{nocuts_hist.view()}").yellow)
    print(c(f"isMuon & L0MuonTOS simulated events with kin/occpcy + truthmatching # events : \n{muon_id_hist.view()}").yellow)
    # cvals of eff
    muon_id_eff = muon_id_hist.view()/nocuts_hist.view()
    print(c(f"Efficiency : \n{muon_id_eff}").yellow)
    # output hist 
    out_hist = Hist( 
            hist.axis.Variable(pt_edges, 
            label=r"$p_T(\mu^{+})$   [MeV/c]", 
            name="muonID_eff", 
            underflow=False, 
            overflow=False),  
            storage=hist.storage.Weight()
        )
    
    
    # fill
    for i in range(len(muon_id_eff)):
        out_hist[i] = [muon_id_eff[i], eff_std(N=nocuts_hist.view(), k=muon_id_hist.view())[i]**2]
    print(c(f"Debug: efficiency hist: \n{out_hist.view().value}").magenta)
    print(c(f"Debug: efficiency errs: \n{out_hist.view().variance**.5}").magenta)

    # save
    os.system(f"mkdir -p {pkl_prefix}") 
    with open(f"{pkl_prefix}/MuonID_eff.pkl", "wb") as f:
        pickle.dump(out_hist, f)
    
    # sanity checks
    assert(out_hist.view().value.all() == muon_id_eff.all())
    assert((out_hist.view().variance**.5).all() == eff_std(N=nocuts_hist.view(), k=muon_id_eff).all())
    print(c(f"\nExecution complete for {MAGPOL} polarity").blue.underline)
    print(c(f"{pkl_prefix}/MuonID_eff.pkl written to file:").blue)
    print(c(f"{out_hist}").blue)

    # plots
    if MAGPOL=="MagUp"   : color="black"
    if MAGPOL=="MagDown" : color="darkorange"
    plt.errorbar(x=nocuts_hist.axes[0].centers, y=out_hist.view().value, 
                    xerr=nocuts_hist.axes[0].widths/2, 
                    yerr=out_hist.view().variance**.5,
                    fmt="x", markersize=5, elinewidth=1., color=color,
                    #label=r"$B_c^{+}\rightarrow D^{0}\mu\nu$, \texttt{isMuon \& L0MuonTOS} "+MAGPOL)
                    label=r"$X_b \rightarrow D^0 h X$, $\texttt{isMuon} \& \texttt{L0MuonTOS}$ "+MAGPOL)





# # B2JpsiK section
# print(c("\nB2JpsiK efficiency points being added as requested").cyan.bold)
# if ADD_B2JPSIK is True:
#     for MAGPOL in MAGPOLS:
        
#         REMOTE_PATH = f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/{YEAR}/{MAGPOL}/B2JpsimumuK.root"

#         K_TRUTHMATCHING = "( (abs(Kplus_TRUEID)==321) || (abs(Kplus_TRUEID)==211) || (abs(Kplus_TRUEID)==0) || (abs(Kplus_TRUEID)==11) || (abs(Kplus_TRUEID)==2212) )"
#         K_MOMENTUM_CUTS = "(Kplus_PT>1500) && (Kplus_P>10e3) && (Kplus_P<100e3)"
#         K_EXTRA_CUTS = "(Kplus_hasMuon) && (Kplus_NShared==0) && (Kplus_PIDmu>3)"
#         # read in the MC 
#         BcMC = pandas.DataFrame(RDF("B2JpsimmKTuple/DecayTree", REMOTE_PATH).Filter(f"{K_TRUTHMATCHING} && {K_MOMENTUM_CUTS} && {K_EXTRA_CUTS}").AsNumpy(), 
#                                 columns = ["Kplus_PT", "Kplus_isMuon", "Kplus_L0MuonDecision_TOS", "Kplus_TRUEID"])
        
#         print(c(f"\nDebug layer: instances of muon track ID").cyan.underline)
#         print(BcMC["Kplus_TRUEID"].value_counts())


#         PAR = "Kplus_PT"
#         MIN_PT=config["b2jpsik"]["MIN_PT"] # from selection
#         MID_PT=8_000
#         MAX_PT=config["b2jpsik"]["MAX_PT"] # from selection
#         NBINS=7
#         MID_HIGH=None
        
#         pt_edges = np.append(np.linspace(start=MIN_PT, stop=MID_PT, num=NBINS-1), [MAX_PT])
#         nocuts_hist = tohist(cvals=BcMC[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)
        
#         muon_id_hist= tohist(cvals=BcMC.query("Kplus_isMuon==True & Kplus_L0MuonDecision_TOS==True")[f"{PAR}"], nbins=NBINS, _MIN_PT=MIN_PT, _MAX_PT=MAX_PT, _MID_PT=MID_PT, _MID_HIGH=MID_HIGH)

#         print(c(f"\nRaw simulated events with kin/occpcy + truthmatching # events : \n{nocuts_hist.view()}").yellow)
#         print(c(f"isMuon & L0MuonTOS simulated events with kin/occpcy + truthmatching # events : \n{muon_id_hist.view()}").yellow)
#         # cvals of eff
#         muon_id_eff = muon_id_hist.view()/nocuts_hist.view()
#         print(c(f"Efficiency : \n{muon_id_eff}").yellow)
#         # output hist 
#         out_hist = Hist( 
#                 hist.axis.Variable(pt_edges, 
#                 label=r"$p_T(\mu^{+})$   [MeV/c]", 
#                 name="muonID_eff", 
#                 underflow=False, 
#                 overflow=False),  
#                 storage=hist.storage.Weight()
#             )
        
        
#         # fill
#         for i in range(len(muon_id_eff)):
#             out_hist[i] = [muon_id_eff[i], eff_std(N=nocuts_hist.view(), k=muon_id_hist.view())[i]**2]
#         print(c(f"Debug: efficiency hist: \n{out_hist.view().value}").magenta)
#         print(c(f"Debug: efficiency errs: \n{out_hist.view().variance**.5}").magenta)

        
#         # sanity checks
#         assert(out_hist.view().value.all() == muon_id_eff.all())
#         assert((out_hist.view().variance**.5).all() == eff_std(N=nocuts_hist.view(), k=muon_id_eff).all())
#         print(c(f"\nExecution complete for {MAGPOL} polarity").blue.underline)

#         # plots
#         if MAGPOL=="MagUp"   : color="firebrick"
#         if MAGPOL=="MagDown" : color="tab:blue"
#         plt.errorbar(x=nocuts_hist.axes[0].centers, y=out_hist.view().value, 
#                         xerr=nocuts_hist.axes[0].widths/2, 
#                         yerr=out_hist.view().variance**.5,
#                         fmt="x", markersize=5, elinewidth=1., color=color,
#                         #label=r"$B_c^{+}\rightarrow D^{0}\mu\nu$, \texttt{isMuon \& L0MuonTOS} "+MAGPOL)
#                         label=r"$B^+ \rightarrow J/\psi K^+$, $\texttt{isMuon} \& \texttt{L0MuonTOS}$ "+MAGPOL)

#plt.axhline(y = 1.0 , color = 'grey', linestyle = '--', linewidth=1.5, alpha=1.) 
#plt.xlim([8e3, 102e3])
#plt.ylim([.0, 1.1])
plt.xlabel(rf"$p_T(h)$   [MeV/c]", loc="center")
plt.ylabel(r"$\varepsilon(\text{is}\mu \,\, \& \,\, \text{L0} \mu \text{TOS} |$ DLL$_{\mu\pi}>3$ \& nShared=0 \& has$\mu$)", loc="center", fontsize=20)
plt.legend(loc="best", fontsize=13)

os.system(f"mkdir -p {plots_prefix}")
plt.savefig(f"{plots_prefix}/muonID_eff.pdf")
plt.savefig(f"{plots_prefix}/muonID_eff.png")
print(c(f"Plots saved to {plots_prefix}/muonID_eff.pdf/png").magenta)


