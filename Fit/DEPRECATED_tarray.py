"""Base for the data used for the signal templates"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import numpy.typing as npt
from dataclasses import dataclass

@dataclass
class sig_hi_base_sel(frozen=True, kw_only=True, slots=True):
	"""NOTE: make hi-level sel private class properties to protect against unwanted changes"""
	_k_antiComb: float = 0.948 # punzi
	_k_antiVcb: float  = 0.3 # punzi
	_LTIME_sup: float  = 0.002 
	_visM_inf: float   = 3_500 # anti b->c
	_visM_sup: float   = 6275.6 # PDG m(Bc) 

	def build(self) -> string:
		sel_str = f"XGB_antiComb>{self._k_antiComb} and \
  			XGB_antiVcb>{self._k_antiComb} and \
  			B_plus_LTIME<{self._LTIME_sup} and \
			B_plus_M>{self._visM_inf} and \
  			B_plus_M<={self._visM_sup}"
		print("SELECTION: ", sel_str)
		return sel_str

base_sel = sig_hi_base_sel().build()

def source_sel(tname: str) -> str: #>=py3.10
	"""Impose FS relative-sign selection based on template tname"""
	match tname:
		case ["dcs" | "misid" ]:
			_sel = "{basel_sel} and (K_minus_ID*Mu_plus_ID)>0"
		case "cf":
			_sel = "{basel_sel} and (K_minus_ID*Mu_plus_ID)<0"
		case "ws":
			_sel = "{basel_sel} and (D0_ID*Mu_plus_ID)<0"
		case ["D0MuNu" | "DstMuNu" | "inclSig"]:
			_sel = "{basel_sel}"
		case other:
			print(f"Unknown tname: {other!r}.")
	print(f"Booked template identifier: {tname}; returning sourced hi-lev sel: {_sel}")
	return _sel


@dataclass
class tarray(frozen=True, kw_only=True, slots=True): #>=py3.10
	"""template array to store the candidates' value in fit var, and the relevant weights"""
	name: str,
	year: str,	
	data: npt.ArrayLike,
	weights: npt.ArrayLike = field(default_factory=None)
	_id: str = field(init=False)
	_sel: str = field(init=False, repr=False) # bookkeping: FS relative-sign sel

	def __post_init__(self) -> None:
		self._id = f"{self.name}_{self.year}"
		self._sel = source_sel(self.name)