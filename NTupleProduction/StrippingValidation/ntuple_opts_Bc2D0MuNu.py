#Options file for Ganga submission for Bc2D0MuNu_D02KMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch


#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

#configure SL Isolation Tools, SL mass correction and momentum scaling
#from SemileptonicCommonTools import * 
from Configurables import TrackScaleState as SCALER
scaler = SCALER('StateScale')


IOHelper().inputFiles([
        '/usera/delaney/private/DV4Ganga/DaVinciDev_v41r4p5/00092449_00000073_1.semileptonic.dst'
        ], clear=True)


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, years
run_year = '2012' #2011, 2012, 2015, 2016, 2017

stream = 'Semileptonic' 
line = 'B2DMuNuX_D0' #run I (available for run II too)
#line = 'B2DMuNuX_D0_FakeMuon' #run I (available for run II too)
#line = 'B2DMuNuX_D0_K3Pi' #run I (available for run II too)

isMC = False

#set to -1 for full run at submission
evts=-1
#----------------------------------------------------


#look at events with relevant stripping line
#fltrs = LoKi_Filters (
#            STRIP_Code = "HLT_PASS_RE('Stripping%sDecision')"%line #check
#            )
#DaVinci().EventPreFilters = fltrs.filters('Filters')


#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('TupleBc2D0MuNU_D02K3Pi') #name of tuple withing the root file

if not isMC:
    dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)] 

#branches and naming for daughter article(s)
#dtt.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+ ^pi- ^pi+]CC) ^mu+]CC'
dtt.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC'
                                    
## branches
dtt.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC',
                 'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC',
                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})

#dtt.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
#                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
#                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+ pi- pi+]CC) mu+]CC',
#                 'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+ pi- pi+]CC) mu+]CC',
#                 'Pi_2'      :  '[Xb -> ([[D0]cc -> K- pi+ ^pi- pi+]CC) mu+]CC',
#                 'Pi_3'      :  '[Xb -> ([[D0]cc -> K- pi+ pi- ^pi+]CC) mu+]CC',
#                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC'})


DaVinci().UserAlgorithms += [scaler]
DaVinci().UserAlgorithms += [dtt]

DaVinci().TupleFile = 'Bc2D0MuNu_D02K3Pi.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = run_year


#run on data: s28 & s29 are leptonic and mDST
if not isMC:
    DaVinci().Simulation = False
    DaVinci().Lumi = not DaVinci().Simulation #Lumi True
    DaVinci().InputType = 'DST'

# Only ask for luminosity information when not using simulated data
DaVinci().EvtMax = evts
