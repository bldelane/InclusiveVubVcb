from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

from Configurables import PrintDecayTree, PrintDecayTreeTool

mode = 'B2DMuNuX_D0'
stream = "Semileptonic"
line = mode
tesLoc = "/Event/{0}/Phys/{1}/Particles".format(stream,line)

# Make DTT
from Configurables import FilterDesktop
from Configurables import TisTosParticleTagger
from PhysConf.Selections import Selection
from PhysSelPython.Wrappers import DataOnDemand
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Selections import PrintSelection

trigFilt = TisTosParticleTagger( mode+'TrigFilter' )
trigFilt.TisTosSpecs = { "L0.*Mu.*Decision%TOS":0 , "Hlt1.*Mu.*Decision%TOS":0 , "Hlt1.*Track.*Decision%TOS":0 ,
                   "Hlt2.*Topo.*Decision%TOS":0, "Hlt2.*Mu.*Decision%TOS":0 }
trigSel = Selection( mode+'TrigSel', Algorithm=trigFilt, RequiredSelections = [ DataOnDemand(tesLoc) ] )

filt = FilterDesktop(mode+'Filt')
filt.Code = """
            INTREE( (ABSID=='K+')  & (PIDK>0) & (P<100*GeV) & (MIPCHI2DV(PRIMARY)>16) ) &
            INTREE( (ABSID=='pi+') & (PIDK<0) & (P<100*GeV) & (MIPCHI2DV(PRIMARY)>16) ) &
            INTREE( (ABSID=='mu+') & (P<100*GeV) & (MIPCHI2DV(PRIMARY)>16) )
            """

sel = Selection(mode+'Sel', Algorithm=filt, RequiredSelections=[trigSel] )
sel = PrintSelection( sel )
selSeq = SelectionSequence( mode+'_SubSelSeq', TopSelection = sel )
dtt = DecayTreeTuple( 'Tuple'+mode )
dtt.Inputs = [ selSeq.outputLocation() ]
#dtt.Inputs = [ tesLoc ]
dtt.Decay = '([B- -> [(D0 -> K- pi+)]CC mu-]CC)'

pt = PrintDecayTree( 'Print'+mode )
pt.addTool( PrintDecayTreeTool, name = 'PrintDecay' )
pt.PrintDecay.Information = "Name M P Px Py Pz Pt chi2"
pt.Inputs = [ tesLoc ]

#from PhysConf.Filters import LoKi_Filters
#fltrs = LoKi_Filters (
    #STRIP_Code = "HLT_PASS_RE('Stripping"+line+"Decision')"
#)
from Configurables import LoKi__HDRFilter
strip_filter = LoKi__HDRFilter(mode+'StripFilter'+line, Code="HLT_PASS('Stripping"+line+"Decision')")

from Configurables import DaVinci
#DaVinci().EventPreFilters = fltrs.filters('Filters')
#DaVinci().RootInTES = tesLoc
DaVinci().UserAlgorithms += [strip_filter]
DaVinci().UserAlgorithms += [selSeq.sequence()]
DaVinci().UserAlgorithms += [dtt]
#DaVinci().UserAlgorithms += [pt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVNTuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2016'
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = 2000

from GaudiConf import IOHelper
IOHelper().inputFiles([
  #'/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/00070452_00008532_1.semileptonic.dst'
  '/r02/lhcb/mkenzie/Bc2D0MuNuX/raw_files/00069601_00005082_1.semileptonic.dst'
  ], clear=True)
