# NTuple Production

Scripts in here will produce the various ntuples for the analysis by running DaVinci.

The DV selection is run by the ``ClassPrototype_Bc2D0MuNuX.py`` class.

Submission of jobs to the grid is handled by ``GangaSubmit.py``

## Getting DaVinci setup

- We make use of some specific TupleTools (from SemileptonicCommonTools) which require a bespoke local installation of DaVinci
- For the data (2011-2018) we use DaVinci v45r2
- Because we want to restrip the MC we have to use the appropriate DaVinci version for each MC year
- Consequently you will need to install several different DV versions
- The whole installation procedure is handled by running

  ```bash
  python SetupDaVinci.py
  ```

- This will produce bash scripts for each of the required versions which
  - setup the right platform / environment
  - checkout the appropriate version of DaVinci
  - checkout the appropriate version of `Phys/DecayTreeTuple` and `Phys/SemileptonicCommonTools`
  - compile the code

## Running some local tests

- You can get hold of some of the approriate test files by running

  ```bash
  ganga FetchTestDatasets.py
  ```

- You can then run a local test using

  ```bash
  python RunDVTest.py -m D0MuNu -y 2016 -d DATA -e 10000
  ```

- Run with `-h` to see a list of options but the above example will run a test of 10K events with the D0MuNu mode on 2016 data

## Running over the full dataset and simulation

### Submitting the DATA

- All of the submission is handled through the `GangaSubmit.py` file
- To checkout the options run it with python: `python GangaSubmit.py -h` to actually execute it properly then run it with ganga: `ganga GangaSubmiy.py [opts]`
- You can (if you are a bit devil-may-care) execute all of the processing in one line:

  ```bash
  ganga -i GangaSubmit.py -m D0MuNu -m JpsiMuNu -y 2011 -y 2012 -y 2015 -y 2016 -y 2017 -y 2018 -p MD -p MU -o /usera/mkenzie/lhcb/Bc2D0MuNuX/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /usera/mkenzie/lhcb/Bc2D0MuNuX/DaVinci_DevX -x -S
  ```

- This will be produce a ganga job for each mode (D0MuNu, JpsiMuNu), each year and each magnet polarity.
- Note you really should use the `-x` option as it will produce slimmed down ntuples (otherwise your files are going to be unmanageably big)
- Using the `-S` option will actually submit the jobs
- By default this will try and split up each job into subjobs
  - For data it will split into 20 files per job up to a maximum of about 400 jobs
  - For MC it will split into 5 files per jobs up to a maximum of about 25 jobs
- The output files will be saved in the grid as:
    1. ``Bc2D0MuNuSlim.root``   (contains trees for D0KPi, D0KPiFakeMuon, D0K3Pi, D0K3PiFakeMuon)
    2. ``Bc2JpsiMuNuSlim.root`` (contains one tree for the Jpsi mode with all muons [fake and real])

- In practise you will probably run out of grid space when the jobs start completing if you do it like this and its very difficult to debug so I would recommend submitting in blocks then downloading the output before submitting the next block

  ```bash
  # submit the Run 1 D0 modes
  ganga GangaSubmit.py -m D0MuNu -y 2011 -y 2012 -p MU -p MD -o /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/DaVinci_DevX -x -S
  # wait a few days (perhaps even sort through the output - see "Post-processing the DATA" below - and then do the next batch)
  # submit the Run 1 Jpsi modes
  ganga GangaSubmit.py -m JpsiMuNu -y 2011 -y 2012 -p MU -p MD -o /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/DaVinci_DevX -x -S
  # wait a bit
  # submit all of 2015 and 2016
  ganga GangaSubmit.py -m D0MuNu -m JpsiMuNu -y 2015 -y 2016 -p MU -p MD -o /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/DaVinci_DevX -x -S
  # wait a bit
  # submit all of 2017 and 2018
  ganga GangaSubmit.py -m D0MuNu -m JpsiMuNu -y 2017 -y 2018 -p MU -p MD -o /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/DaVinci_DevX -x -S
  ```

- The output files for this are large so will be saved as `DiracFiles` i.e. they will get saved in your grid user space somewhere
- For post-processing see below

### Post-processing the DATA

- The output files for the data are very large (e.g. the D0MuNu mode Data in 2016 MD alone is 88G) so in total we can expect about ~1.5T
- In principle there is enough space in your grid quota (2T) to store this if it is almost empty
- There is a script (procedure) outlined below to help clean this up as you go

1. Create a log of each jobs status

  - The first thing required is that for each ganga job you have a log of which jobs have completed and what their output filename is.
  - You can do this with a script provided (`ganga_file_tracker.py`) but it's quite slow because it has to fire up ganga and so if you're doing it multiple times it takes a while and you can't have another ganga sessions running.
  - I do this using a function in my `.ganga.py` which looks like this:

  ```python
  def file_log(jobid):

    log = {}
    for sj in jobs(jobid).subjobs:
      inputfile_lfn_base = sj.inputdata.files[0].lfn_prefix
      inputfile_names    = sj.inputdata.files[0].suffixes
      inputfile_lfns     = []
      outputfile_lfns    = []

      for f in sj.outputfiles:
        if f.lfn!='': outputfile_lfns.append( f.lfn )

      for f in inputfile_names:
        inputfile_lfns.append( os.path.join(inputfile_lfn_base, f) )

    log[sj.id] = { 'output' : outputfile_lfns, 'input' : inputfile_lfns }

    # this obviously needs to change to your LocalXML path
    # should probably update this snippet to get this automaticall as
    # ganga will know it
    basepath = '/home/epp/phskbk/gangadir/workspace/phskbk/LocalXML/'
    os.system('mkdir -p %s/%d/output'%(basepath,jobid))
    path = os.path.join(basepath,str(jobid),'output/file_tracker.json')
    logf = open(path,'w')
    json.dump(log,logf,indent=2)
    logf.close()
  ```
  - Put this function snippet in your `.ganga.py` and then run it from within inside ganga using `file_log(jobid)`. This will then save a file in the output for that jobid called `file_tracker.json`. Do not worry about if it prints some warnings like:
    ```bash
    WARNING  Do NOT have an LFN, for file: *.root
    WARNING  If file exists locally try first using the method put()
    ```
  - This file keeps a log for each subjob of which input files were (or should have been) run over and provides lfns for the output (if available)
  - In this way we can simply check if the output is available and then download it somewhere and if we run into any problems (e.g. a job keeps failing) we know which files we still have left to run over

2. Download the job output to `/eos/`

  - We want to clear out our grid space and also make the output tuples available in the SL working group `/eos/` area
  - So now we can download the output to `/eos/` using the `GangaDownloadOutput.py` script - note you must run this on lxplus (this should be updated to use `xrdcp` from other sites)
  - I use a script to synchronise log files which track which files have been run on. This will essentially copy over the `.json` files created above to lxplus
    - To run this just update the file `log/job_map.json` to provide a mapping from each ganga jobid to some sensible name.
    - Then execute `python synch_data_file_logs.py`
    - Now in the `log` directory there are `.json` files for each job I want to download the output for
  - I then run something like this for each `.json` file to download it all (note that this is really slow the first time you run it and probably could be sped up)

    ```bash
    python GangaDownloadOutput.py -l Data_Bc2D0MuNu_2016_MD.json -o /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/vnew/DATA/D0MuNu/2016/MD/
    ```

  - Note this will check if the output file exists on the grid and if it does it will download it, move it over to `/eos/` and then delete the original
  - This means that you can repeat this process whilst some jobs are still finishing or if some jobs are being resubmitted
  - This is quite painfully slow but only the **first time**  
    - I think could be sped up by just downloading a list of the LFNs all at once but this relies on them having completely unique basenames and I don't know if 
      that is always the case). Anyway I've found it best just to run this in a few screen sessions on lxplus. Becuase it takes a while you will want a screen 
      session that doesn't expire so follow the recipe here to make sure you access token remains valid for long enough https://hsf-training.github.io/analysis-essentials/shell-extras/persistent-screen.html

  - **Note:** to update when more jobs have completed you just need to repeat this process (and it won't overwrite anything but just update what's missing):
    - update the `file_tracker.json` files
    - `scp` them over to lxplus (if not already there)
    - run `python GangaDownloadOutput.py` to synch missing files to `/eos/lhcb/wg/semileptonic` and clear out your grid space
    
  - Finally there is another script (which you don't have to run) that will check the output called `check_synched_files.py`
    - it makes sure that the expected files exists (and there are the right number)
    - it makes sure that those files are not truncated (they got downloaded in full)
    - it makes sure that those files contain the expected ntuples
    - 
  - You can run this on each directory if you want to
    
    ```bash
    lb-run DaVinci/latest python check_synched_files.py -l log/Data_Bc2D0MuNu_2016_MD.json -o /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v2/DATA/D0MuNu/2016/MD
    ```
  - **THERE IS ONE SCRIPT WHICH WILL EXECUTE ALL OF THE ABOVE FOR ALL MODES AND SAMPLES**
    - The script `synch_ganga_output.sh` will take of everything above for you providing you fill in the `log/job_map.json` file appropriately
    - You might need to make a few changes to make sure the files get copied from and to the right locations and it has your usename etc.
    - This will loop all modes, years and polarities to do everything you need
    ```bash
    ./synch_ganga_output.sh
    ```

3. Running the lumi calculation

  - For each path you have downloaded to you will want to run the lumi summary and save the output. An example would be (note this should be on lxplus)

    ```bash
    lb-run DaVinci/latest python RunLumiSummary.py /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/vnew/DATA/D0MuNu/2016/MD/*
    ```

** You should now have all your data ntuples and you're ready to execute the `Pipeline` for the next part of the analysis chain **

### Submitting the MC

- As with data this is handled by the `GangaSubmit.py` script
- In this case you need to pass `-s` for simulation
- You will also probably want to omit the `-x` so you do not apply any additional selection and you get more branches in your tuples
  - This also helps to calculate efficiencies later
- You will also need to pass a `-d` option(s) with an argument to signify which MC decays you want to run on
- An example to get the bare minimum of MC you need is:

  ```bash
  # Get hold of D0MuNu D->Kpi mode MC in 2016
  ganga -i GangaSubmit.py -m D0MuNu -y 2016 -p MD -p MU -d Bc2D0MuNu -d Bc2D0gMuNu -d Bc2D0pi0MuNu -d Bu2D0MuNu -o /usera/mkenzie/lhcb/Bc2D0MuNuX/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /usera/mkenzie/lhcb/Bc2D0MuNuX/DaVinci_DevX -s -S
  # Get hold of JpsiMuNu mode MC in 2016
  ganga -i GangaSubmit.py -m JpsiMuNu -y 2016 -p MD -p MU -d Bc2JpsiMuNu -o /usera/mkenzie/lhcb/Bc2D0MuNuX/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /usera/mkenzie/lhcb/Bc2D0MuNuX/DaVinci_DevX -s -S
  ```

- The output files for this are much more manageable so are saved as `LocalFiles` i.e. they get put in your local ganga `workspace` directory
- For post-processing see below

### Post-processing the MC

- This is fairly straightforward and you just need to `hadd` up the local output files and calculate the efficiency.
- If you have submitted the jobs using the `GangaSubmit.py` script then it will create a log file for you which tracks which job ids were assigned to each job. This gets stored in a dictionary and save to a file called `log/sub_mc_<unique_int>.py` where `unique_int` is a time stamp from the submission.
- So if you want to `hadd` a lot of different jobs at the same time you can call the merging script below on a dictionary of jobs

1. Merge the output files

  - For each jobid that you want to add up the files for just run
	```bash
	python ganga_hadder.py -j <jobid>
	```
  - There is an optional `-f` argument to force overwriting
  - If you have many jobs at once then you can pass a dictionary instead with
  ```bash
  python ganga_hadder.py -d <dict_file>
  ```
  - You can then upload these back into the WG `eos` folder using the `synch_mc.py` script

2. Calculate the efficiency

  - For each jobid that you want to calculate the efficiency for run
	```bash
	python ganga_eff.py <jobid>
	```

  - The output for both of these will go into your local ganga workspace in the `<jobid>/output` directory

  - You should then `scp` it over to the appropriate location in the SL wg `/eos/` area

3. PID calibration
  
  - You will then need to run the pid calibration on the output of the MC
  - This is handled by a straightforward and simple to run set of scripts in the `InclusiveVub/PidResampling` direcory
  - Instructions for that are in [PidResampling/README.md](../PidResampling/README.md)

## OUTPUT
- All of the output should end up in ``/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples``
- You can then run the analysis `Pipeline` for the next part of the analysis chain

