from __future__ import print_function
from argparse import ArgumentParser
parser = ArgumentParser(usage="python GangaDownloadOutput.py")
parser.add_argument('-l','--logfile', default=None, help='File checker log.json file')
parser.add_argument('-o','--outdir' , default=None, help='Output location')
parser.add_argument('-u','--user',   default='mkenzie', help='lxplus username for grid file truncation')
opts = parser.parse_args()

import json
import os
import sys
import ROOT as r
r.gErrorIgnoreLevel = r.kFatal

check_tuples = { 'Bc2D0MuNuXSlim'   : [ "B2DMuNuX_D02KPiTuple", "B2DMuNuX_D02K3PiTuple", "B2DMuNuX_D02K3Pi_FakeMuonTuple", "B2DMuNuX_D02KPi_FakeMuonTuple", "GetIntegratedLuminosity" ] ,
                 'Bc2JpsiMuNuXSlim' : [ "B2JpsiMuNuXTuple", "GetIntegratedLuminosity" ]
               }

assert( os.path.exists( opts.logfile ) )
with open( opts.logfile ) as f:
  log = json.load( f )

missing    = []
incomplete = []
wrongtuples = []
ok = []
path = sys.argv[1]
for i, jid in enumerate(log.keys()):
  outfiles = log[jid]['output']
  if len(outfiles)==0: missing.append( jid )
  for outf in outfiles:
    if 'DVHistos' in outf: continue
    endpath = os.path.join( opts.outdir, '%s_%s'%(jid,outf.split(opts.user+'/')[-1].replace('/','_')) )

    if not os.path.exists: 
      missing.append( (jid,endpath) )
      continue

    print('Cheking %30s: '%opts.outdir,'%4d / %-4d'%(i+1,len(log.keys())), end='\r')
    if i==len(log.keys())-1: 
      print('')
    else:
      sys.stdout.flush()

    tf = r.TFile.Open(endpath)
    if not tf:
      incomplete.append( (jid,endpath) )
      continue
    if tf.IsZombie():
      incomplete.append( (jid,endpath) )
      continue
    
    miss_tuples = []
    for keyname, tupledirs in check_tuples.items():
      if keyname in os.path.basename(endpath):
        for tupledir in tupledirs:
          if not tf.Get(tupledir):
            miss_tuples.append(tupledir)
    if len(miss_tuples)>0:
      wrongtuples.append( (jid, endpath, miss_tuples) )

    tf.Close()
    ok.append( (jid, endpath) )

if len(missing)==0 and len(incomplete)==0 and len(wrongtuples)==0:
  print('--> ALL OK %4s/%-4s'%(len(ok),len(log.keys())))
else:
  print('--> Found %4s/%-4s files are OK'%(len(ok),len(log.keys())))

if len(missing)>0:
  print('--> Missing', len(missing), 'files:')
  for jid in missing:
    print('   sj: %-4s'%jid )

if len(incomplete)>0:
  if len(missing)==0:
    print('--> Missing', len(missing), 'files:')
  print('--> Incomplete', len(incomplete), 'files:')
  for jid, fname in incomplete:
    print('   sj: %-4s'%jid, 'file:', fname )

if len(wrongtuples)>0:
  if len(missing)==0:
    print('--> Missing', len(missing), 'files:')
  if len(incomplete)==0:
    print('--> Incomplete', len(incomplete), 'files:')
  print('--> Missing tuple(s) in', len(wrongtuples), 'files:')
  for jid, fname, miss_tuples in wrongtuples:
    print('   sj: %-4s'%jid, 'file:', fname, 'tuples:', miss_tuples )

