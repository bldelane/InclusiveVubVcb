# Just an example of how to extract efficiencies of the stripping
# from ganga output logs

# In these examples (and in this MC) no additional selection has been
# applied on top of the stripping (this is not the case for data for which
# the preselection is applied at the DV level. In principle this is fine for
# calculating efficiencies as long as that preselection is applied on MC by the
# pipeline anyway (which I believe it is)

# Note the jpsi is slightly different as the stripping line only creates detached Jpsi
# we then add our own muons to this

def getEff(nIn, nOut):
  eff = float(nOut)/float(nIn)
  err = ((float(nOut)*(1-eff))**0.5)/float(nIn)
  return (eff*100.,err*100.)

import os

fpath = '/r01/lhcb/mkenzie/gangadir/workspace/mkenzie/LocalXML/'

eff_map = {}

print('| %-16s | %-4s | %-3s | %-19s | %-24s | '%('Decay','Year','Pol','nOut / nIn','Efficiency'))
print '-'*82

logf = open('log/MCTypeToID.log')
for line in logf.readlines():
  if line.startswith('Decay') or line.startswith('---'): continue
  if line.startswith('#'): continue
  decay = line.split()[0]
  year = line.split()[2]
  pol  = line.split()[4]
  jid  = line.split()[6]
  # look for subdirs (i.e. output of subjobs) in relevant path
  fold = os.path.join(fpath,jid)
  assert( os.path.exists( fold  ) )
  totIn = 0
  totOut = 0
  for root, dirs, fils in os.walk( fold ):
    if root==fold: # only go one level deep
      for di in dirs:
        if di=='input' or di=='output': continue
        fname = os.path.join(root,di,'output/Script1_Ganga_GaudiExec.log')
        #assert( os.path.exists(fname) ) # actually don't need this as job may have failed
        if not ( os.path.exists(fname) ): continue
        gang_out_f = open(fname)
        glines = gang_out_f.readlines()
        for i, gl in enumerate(glines):
          if 'Jpsi' in decay:
            if gl.startswith('StripPassFilter'):
              relline = glines[i+2]
              stripIn = int( relline.split('|')[2] )
              stripOut = int( relline.split('|')[3] )
            if gl.startswith(' | "# Phys/B2JpsiMuNuX_TrigSel"'):
              relline = glines[i+2]
              selOut = int( relline.split('|')[3] )
              totIn += stripIn
              totOut += selOut
          else:
            if gl.startswith('StripPassFilter'):
              relline = glines[i+2]
              evIn = int( relline.split('|')[2] )
              evOut = int( relline.split('|')[3] )
              totIn += evIn
              totOut += evOut
        gang_out_f.close()

  eff, err = getEff(totIn, totOut)
  print('| %-16s | %4s | %3s | %8d / %-8d | (%8.5f +/- %8.5f)%% |'%(decay,year,pol,totOut,totIn,eff,err))
logf.close()

