from __future__ import print_function
from GangaSubmit import input_data_bk_paths as data_paths
from GangaSubmit import input_mc_bk_paths as mc_paths


download_loc = '../../TestFiles'

pols = ['MD']

import json
import os

lfns = { 'DATA': {}, 'MC': {} }

if not os.path.exists('DataTestFiles.json'):
	for mode in data_paths.keys():
		for year in data_paths[mode].keys():
			for pol in data_paths[mode][year].keys():
				path = data_paths[mode][year][pol]
				if pol not in pols: continue
				print(path)
				bkq = BKQuery(path)
				dset = bkq.getDataset()
				lfn = dset[0].lfn
				print(lfn)
				lfns['DATA']['%s_%s'%(mode,year)] = lfn

	f = open('DataTestFiles.json','w')
	json.dump(lfns['DATA'],f)
	f.close()

if not os.path.exists('MCTestFiles.json'):
	for mode in mc_paths.keys():
		for decay in mc_paths[mode].keys():
			if decay != 'Bc2D0MuNu' and decay != 'Bu2D0MuNu' and decay != 'Bc2JpsiMuNu' and decay != 'Bc2D0MuNu_K3Pi': continue
			print(decay,':')
			for year in mc_paths[mode][decay].keys():
				for pol in mc_paths[mode][decay][year].keys():
					path = mc_paths[mode][decay][year][pol]
					if pol not in pols: continue
					if path=='': continue
					print('  ', path)
					bkq = BKQuery(path)
					dset = bkq.getDataset()
					lfn = dset[0].lfn
					print('  ', lfn)
					lfns['MC']['%s_%s'%(decay,year)] = lfn

	f = open('MCTestFiles.json','w')
	json.dump(lfns['MC'],f, indent=2)
	f.close()

import sys
sys.exit()

#f = open('DataTestFiles.json')
#lfns['DATA'] = json.load(f)
#f.close()

f = open('MCTestFiles.json')
lfns['MC'] = json.load(f)
f.close()

os.chdir(download_loc)
f = open('test_files.lfn','w')
for dtype, cfg in lfns.items():
	print(dtype)
	for key, lfn in cfg.items():
		print( '\t', key, lfn )
		if not os.path.exists(os.path.basename(lfn)) and not os.path.exists('%s_%s_%s'%(dtype,key,os.path.basename(lfn))):
			f.write(lfn+'\n')
f.close()

f = open('download.sh','w')
f.write('#/bin/bash\n')
f.write('source /cvmfs/lhcb.cern.ch/lib/LbEnv\n')
f.write('lhcb-proxy-init\n')
f.write('lb-dirac dirac-dms-get-file --File=test_files.lfn\n')
f.close()

os.system('chmod +x %s'%f.name)
os.system('bash %s'%f.name)

for dtype, cfg in lfns.items():
	for key, lfn in cfg.items():
		cmd = 'mv %s %s_%s_%s'%(os.path.basename(lfn),dtype,key,os.path.basename(lfn))
		os.system(cmd)
