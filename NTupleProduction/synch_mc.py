from __future__ import print_function

import json
import os

uname      = 'mkenzie'
remote_loc = '/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/vL0Eff/MC'
local_loc  = '/storage/epp2/phskbk/gangadir/workspace/phskbk/LocalXML/'
#job_map_fi = 'log/mc_job_map.py'
job_map_fi = 'log/sub_mc_1656191802.py'
#job_map_fi = 'log/sub_mc_1656195908.py'
clean = True

import subprocess
subprocess.run('kinit -f {0}@CERN.CH'.format(uname),shell=True)

import importlib
import pprint

jmap = None
if os.path.exists(job_map_fi):
	spec = importlib.util.spec_from_file_location('module',job_map_fi)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	if hasattr(module, 'job_map'):
		jmap = module.job_map
		pp = pprint.PrettyPrinter(indent=2)
		pp.pprint(jmap)

if not jmap: sys.exit('No job map found')

for dec, ditems in jmap.items():
	for year, yitems in ditems.items():
		for pol, jid in yitems.items():
			if jid is None: continue
			print( dec, year, pol, jid )
			mode = 'D0MuNu' if '2D0' in dec else 'JpsiMuNu'

			infname = 'Bc2'+mode+'X.root'
			outfname = '_'.join(['MC',dec,year,pol]) + '.root'

			local_path = os.path.join(local_loc,str(jid),'output',infname)
			remote_path = os.path.join(remote_loc,mode,year,pol,outfname)

			if not os.path.exists(local_path):
				print('Cannot find local file', local_path)

			print('Uploading', local_path)
			print('  to     ', remote_path)
			subprocess.run( 'scp {0} {1}@lxplus.cern.ch:{2}'.format(local_path,uname,remote_path), shell=True, check=True, capture_output=False )

			if clean:
				subprocess.run( 'rm -f {0}'.format(local_path), shell=True, check=True, capture_output=False )



#for jid, name in jm.items():
  #if 'MC' not in name: continue
  #remote_path = os.path.join(remote_loc,jid,'output')

  #magpol = name.split('_')[-1]
  #year = name.split('_')[-2]
  #channel = 'D0MuNu' if ('D0MuNu' in name or 'D0gMuNu' in name or 'D0pi0MuNu' in name) else 'JpsiMuNu'

  #remote_file = os.path.join( remote_path, 'Bc2%sX.root'%channel )
  #remote_eff  = os.path.join( remote_path, 'eff.log' )

  #local_path = os.path.join(local_loc,channel,year,magpol)
  #loc_file = os.path.join(local_path,name+".root")
  #loc_eff  = os.path.join(local_path,name+"_eff.log")

  #if not os.path.exists(os.path.dirname(loc_file)):
    #os.system('mkdir -p %s'%os.path.dirname(loc_file))

  #if not os.path.exists(loc_file):
    #print('Downloading',remote_file,'to',loc_file)
    #os.system('scp %s %s'%(remote_file,loc_file))

  #if not os.path.exists(loc_eff):
    #print('Downloading',remote_eff,'to',loc_eff)
    #os.system('scp %s %s'%(remote_eff,loc_eff))


