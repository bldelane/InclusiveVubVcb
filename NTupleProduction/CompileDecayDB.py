from tinydb import TinyDB, Query
import os
import datetime
timestamp=datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

db = TinyDB("DecayDB_DATAONLY.json", sort_keys=False, indent=4, separators=(',', ': '))


def Insert(decay_type='', decay_mode='', decay_year='', decay_polarity='', decay_localpath='', decay_BKKpath='',
        decay_dbtags='', decay_DVversion='', decay_DVplatform=''):
    db.insert(
                {
                  "type": f"{decay_type}",
                  "mode": f"{decay_mode}",
                  "year": f"{decay_year}",
                  "polarity": f"{decay_polarity}",
                  "localpath": f"{decay_localpath}",
                  "BKKpath": f"{decay_BKKpath}",
                  "DVversion": f"{decay_DVversion}",
                  "DVplatform": f"{decay_localpath}"
                })


# -------- INPUT DATA DICTIONARY -------- #
input_data_files = { # keys are : [mode][year][polarity]
    'D0MuNu' :
        { '2011' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py' },
          '2012' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py'},
          '2015' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1_SEMILEPTONIC.DST.py'},
          '2016' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py'},
          '2017' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py'},
          '2018' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/18/LHCb_Collision18_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco18_Stripping34_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/18/LHCb_Collision18_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco18_Stripping34_SEMILEPTONIC.DST.py'},
        },
    'JpsiMuNu' :
        { '2011' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_RealData_Reco14_Stripping21r1_DIMUON.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1_DIMUON.DST.py'},
          '2012' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21_DIMUON.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21_DIMUON.DST.py'},
          '2015' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py'},
          '2016' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py'},
          '2017' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py'},
          '2018' :  { 'MD' : '',
                      'MU' : ''},
        }
    }

input_data_bk_paths = { # keys are : [mode][year][polarity]
    'D0MuNu' :
        { '2011' :  { 'MD' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST' },
          '2012' :  { 'MD' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST'},
          '2015' :  { 'MD' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/SEMILEPTONIC.DST'},
          '2016' :  { 'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/SEMILEPTONIC.DST'},
          '2017' :  { 'MD' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/SEMILEPTONIC.DST'},
          '2018' :  { 'MD' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/SEMILEPTONIC.DST'},
        },
    'JpsiMuNu' :
        { '2011' :  { 'MD' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p1a/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p1a/90000000/DIMUON.DST' },
          '2012' :  { 'MD' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p1a/90000000/DIMUON.DST'},
          '2015' :  { 'MD' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/DIMUON.DST'},
          '2016' :  { 'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/DIMUON.DST'},
          '2017' :  { 'MD' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST'},
          '2018' :  { 'MD' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/DIMUON.DST'},
        },
    }


# -------- JOB CONFIG DICTIONARIES ------ #
dv_version = {
    'D0MuNu' :
        { '2011' : 'DaVinciDev_v39r1p1',
          '2012' : 'DaVinciDev_v39r1p1',
          '2015' : 'DaVinciDev_v38r1p7',
          '2016' : 'DaVinciDev_v41r4p5',
          '2017' : 'DaVinciDev_v42r7p3',
          '2018' : 'DaVinciDev_v44r7',
        },
    'JpsiMuNu' :
        { '2011' : 'DaVinciDev_v39r1p1',
          '2012' : 'DaVinciDev_v39r1p1',
          '2015' : 'DaVinciDev_v38r1p7',
          '2016' : 'DaVinciDev_v41r4p5',
          '2017' : 'DaVinciDev_v42r7p3',
          '2018' : 'DaVinciDev_v44r7',
        }
    }

dv_platform = {
    'D0MuNu' :
        { '2011' : 'x86_64-slc6-gcc49-opt',
          '2012' : 'x86_64-slc6-gcc49-opt',
          '2015' : 'x86_64-slc6-gcc49-opt',
          '2016' : 'x86_64-slc6-gcc49-opt',
          '2017' : 'x86_64-slc6-gcc49-opt',
          '2018' : 'x86_64-slc6-gcc62-opt',
        },
    'JpsiMuNu' :
        { '2011' : 'x86_64-slc6-gcc49-opt',
          '2012' : 'x86_64-slc6-gcc49-opt',
          '2015' : 'x86_64-slc6-gcc49-opt',
          '2016' : 'x86_64-slc6-gcc49-opt',
          '2017' : 'x86_64-slc6-gcc49-opt',
          '2018' : 'x86_64-slc6-gcc62-opt',
        }
    }


for mode in input_data_files.keys():
    for year in input_data_files[mode].keys():
        for pol in input_data_files[mode][year].keys():
            localpath=input_data_files[mode][year][pol]

            bkpath=input_data_bk_paths[mode][year][pol]
            version=dv_version[mode][year]
            platform=dv_platform[mode][year]
            
            try:
                os.system(f"cp DecayDB_DATAONLY.json DBhistory/DecayDB_DATAONLY_{timestamp}.json")

                Insert(decay_type='Data', decay_mode=mode, decay_year=year, decay_polarity=pol, decay_localpath=localpath)
                decay=Query()
                db.update({'BKKpath':bkpath}, (decay.mode==mode) & (decay.year==year) & (decay.polarity==pol))
                db.update({'DVversion':version}, (decay.mode==mode) & (decay.year==year) & (decay.polarity==pol))
                db.update({'DVplatform':platform}, (decay.mode==mode) & (decay.year==year) &(decay.polarity==pol))
            except:
                print("Database Error: please consult logs.")
            finally:
                print(f"Logs updates for entry: {mode} {year} {pol} @ {timestamp}")
