# GANGA SUBMISSION SCRIPT
import os
os.system('mkdir -p log')
import sys
import time

# --- OPTION DEFAULTS ---
mode_choices = [ 'D0MuNu', 'JpsiMuNu']
year_choices = ['2011','2012','2015','2016','2017','2018']
pol_choices = [ 'MU', 'MD' ]
mcdec_choices = ['Bc2D0MuNuOld','Bc2D0MuNu','Bc2D0gMuNu','Bc2D0pi0MuNu','Bc2D0MuNu_Kis','Bc2D0gMuNu_Kis','Bc2D0pi0MuNu_Kis','Bu2D0MuNu','Bu2D0XMuNu','Bu2D0Pi','Bu2KPiPi','Bd2KPi','Bd2JpsiKPi','Bd2JpsiKK','Bd2JpsiPiPi','Bd2KstRho','Bd2KstPhi','Bc2D0MuNu_K3Pi','Bc2D0gMuNu_K3Pi','Bc2D0pi0MuNu_K3Pi','Bu2D0MuNu_K3Pi','Bc2JpsiMuNu','Bc2JpsiTauNu','Bc2Psi2SMuNu','Bc2ChiCMuNu','Bc2ChiC2MuNu','Bc2JpsiDx','Bc2JpsiDxKx','Bc2JpsiDsx','Bu2JpsiX','Bd2JpsiX','Bs2JpsiX']
#optsfile_def  = '/usera/delaney/private/Bc2D0MuNuX/NTupleProudction/ClassPrototype_Bc2D0MuNuX.py'
#userarea_def  = '/usera/delaney/private/DV4Ganga'
optsfile_def  = '/home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py'
userarea_def  = '/home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/DaVinci_DevX'
# --- OPTION PARSING ---
from argparse import ArgumentParser
parser = ArgumentParser(usage="ganga GangaSubmit.py [options]")
parser.add_argument('-m','--mode'       , dest='mode'       , metavar='', action='append', choices = mode_choices, default=[], help='Mode(s) to run. Can pass multiple times. Allowed values: {'+', '.join(mode_choices)+'}' )
parser.add_argument('-y','--year'       , dest='year'       , metavar='', action='append', choices = year_choices, default=[], help='Year(s) to run. Can pass multiple times. Allowed values: {'+', '.join(year_choices)+'}' )
parser.add_argument('-p','--polarity'   , dest='polarity'   , metavar='', action='append', choices = pol_choices , default=[], help='Magnet polarity(ies) to run. Can pass multiple times. Allowed values: {'+', '.join(pol_choices)+'}' )
parser.add_argument('-d','--mcdecays'   , dest='mcdecays'   , metavar='', action='append', choices = mcdec_choices,default=[], help='Which MC decays to run. Default will do all of them. Allowed values: {'+', '.join(mcdec_choices)+'}' )
parser.add_argument('-o','--optsfile'   , dest='optsfile'   , metavar='', default=optsfile_def   , help='Full path to arguments files ( %(default)s ).')
parser.add_argument('-u','--user_area'  , dest='user_area'  , metavar='', default=userarea_def   , help='Path to directory containg DV builds ( %(default)s ).')
parser.add_argument('-e','--events'     , dest='events'     , metavar='', default=-1, type=int   , help='Max Events To Run')
parser.add_argument('-f','--filesPerJob', dest='filesPerJob', metavar='', default=-1, type=int   , help='Request number of files per job (default will do some auto-splitting)')
parser.add_argument('-s','--simulation' , dest='simulation' , action='store_true', default=False , help='Is MC? Default is false')
parser.add_argument('-x','--slim'       , dest='slim'       , action='store_true', default=False , help='Slim Tuples? Default is false')
parser.add_argument('-t','--test'       , dest='test'       , action='store_true', default=False , help='Run a test (will submit to local backend only)')
parser.add_argument('-S','--submit'     , dest='submit'     , action='store_true', default=False , help='Actually submit the jobs')
parser.add_argument('-c','--check'      , dest='check'      , action='store_true', default=False , help='Just check the BK paths are ok and contain files')
parser.add_argument('-B','--bannedSites', dest='bannedSites', metavar='', action='append', default=[], help='Sites to ban')
parser.add_argument('-D','--dryRun'     , dest='dryRun'     , action='store_true', default=False , help='Do everything but don\'t create the jobs')
opts = parser.parse_args()

#if opts.test: opts.events = 1000 # just run user specified number
if len(opts.mcdecays)==0: opts.mcdecays = mcdec_choices
if not opts.simulation: opts.mcdecays = []
print('------------------------ OPTIONS ------------------------')
print('Modes:      ', opts.mode)
print('Years:      ', opts.year)
print('Polarties:  ', opts.polarity)
print('Simulation: ', opts.simulation)
print('Slim Tuples:', opts.slim)
print('MC Decays:  ', opts.mcdecays)
print('OptsFile:   ', opts.optsfile)
print('UserArea:   ', opts.user_area)
print('MaxEvents:  ', opts.events)
print('FilesPerJob ', opts.filesPerJob)
print('Test?:      ', opts.test)
print('Submit?:    ', opts.submit)
print('BannedSites:', opts.bannedSites)
print('Dry Run?:   ', opts.dryRun)
print('---------------------------------------------------------')

# -------- INPUT DATA DICTIONARY -------- #
input_data_files = { # keys are : [mode][year][polarity]
    'D0MuNu' :
        { '2011' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py' },
          '2012' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py'},
          '2015' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1_SEMILEPTONIC.DST.py'},
          '2016' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py'},
          '2017' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py'},
          '2018' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/18/LHCb_Collision18_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco18_Stripping34_SEMILEPTONIC.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/18/LHCb_Collision18_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco18_Stripping34_SEMILEPTONIC.DST.py'},
        },
    'JpsiMuNu' :
        { '2011' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_RealData_Reco14_Stripping21r1_DIMUON.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1_DIMUON.DST.py'},
          '2012' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21_DIMUON.DST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21_DIMUON.DST.py'},
          '2015' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py'},
          '2016' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py'},
          '2017' :  { 'MD' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py',
                      'MU' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py'},
          '2018' :  { 'MD' : '',
                      'MU' : ''},
        }
    }

input_data_bk_paths = { # keys are : [mode][year][polarity]
    'D0MuNu' :
        { '2011' :  { 'MD' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p2/90000000/SEMILEPTONIC.DST' },
          '2012' :  { 'MD' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p2/90000000/SEMILEPTONIC.DST'},
          '2015' :  { 'MD' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/SEMILEPTONIC.DST'},
          '2016' :  { 'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/SEMILEPTONIC.DST'},
          '2017' :  { 'MD' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/SEMILEPTONIC.DST'},
          '2018' :  { 'MD' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/SEMILEPTONIC.DST',
                      'MU' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/SEMILEPTONIC.DST'},
        },
    'JpsiMuNu' :
        { '2011' :  { 'MD' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST' },
          '2012' :  { 'MD' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/DIMUON.DST'},
          '2015' :  { 'MD' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST'},
          '2016' :  { 'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST'},
          '2017' :  { 'MD' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST'},
          '2018' :  { 'MD' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST',
                      'MU' : '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/DIMUON.DST'},
        },
    }

# -------- INPUT MC DICTIONARY -------- #
input_mc_files = {}

input_mc_bk_paths = { # keys are : [mode][decay][year][polarity]
    'D0MuNu' :
    {
      # Config (relate reconstruction mode to simulated decay)
      'Config' : { 'Bc2D0MuNuX_D02KPi'  : [ 'Bc2D0MuNuOld', 'Bc2D0MuNu', 'Bc2D0gMuNu', 'Bc2D0pi0MuNu', 'Bc2D0MuNu_Kis', 'Bc2D0gMuNu_Kis', 'Bc2D0pi0MuNu_Kis', 'Bu2D0MuNu', 'Bu2D0XMuNu', 'Bu2D0Pi', 'Bu2KPiPi', 'Bd2KPi','Bd2JpsiKPi','Bd2JpsiKK','Bd2JpsiPiPi','Bd2KstRho','Bd2KstPhi' ] ,
                   'Bc2D0MuNuX_D02K3Pi' : [ 'Bc2D0MuNu_K3Pi', 'Bc2D0gMuNu_K3Pi', 'Bc2D0pi0MuNu_K3Pi', 'Bu2D0MuNu_K3Pi' ]
                 },

      # 14573002
      'Bc2D0MuNuOld'     : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14573002/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14573002/ALLSTREAMS.DST'},
                           },
      # 14573023
			'Bc2D0MuNu'        : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573023/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573023/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573023/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573023/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573023/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573023/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573023/BC2D0MUNU.STRIP.DST'},
                           },
      # 14573013
      'Bc2D0MuNu_Kis'    : { '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573013/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573013/BC2D0MUNU.STRIP.DST'},
                           },
      # 14573223
      'Bc2D0gMuNu'       : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573223/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573223/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573223/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573223/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573223/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573223/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573223/BC2D0MUNU.STRIP.DST'},
                           },
      # 14573213
      'Bc2D0gMuNu_Kis'   : { '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573213/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573213/BC2D0MUNU.STRIP.DST'},
                           },
      # 14573423
      'Bc2D0pi0MuNu'     : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14573423/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14573423/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14573423/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14573423/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14573423/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573423/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573423/BC2D0MUNU.STRIP.DST'},
                           },
      # 14573413
      'Bc2D0pi0MuNu_Kis' : { '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573413/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14573413/BC2D0MUNU.STRIP.DST'},
                           },
      # 14675024
      'Bc2D0MuNu_K3Pi'   : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675024/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675024/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675024/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675024/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675024/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675024/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675024/BC2D0MUNU.STRIP.DST'},
                           },
      # 14675224
      'Bc2D0gMuNu_K3Pi'  : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675224/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675224/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675224/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675224/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675224/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675224/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675224/BC2D0MUNU.STRIP.DST'},
                           },
      # 14675424
      'Bc2D0pi0MuNu_K3Pi': { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09j-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/14675424/BC2D0MUNU.STRIP.DST'},
                             '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/14675424/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/14675424/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered/14675424/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/14675424/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675424/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/14675424/BC2D0MUNU.STRIP.DST'},
                           },
      # 12873002
      'Bu2D0MuNu'        : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09j-ReDecay01/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/12873002/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09j-ReDecay01/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/12873002/BC2D0MUNU.STRIP.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09j-ReDecay01/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/12873002/BC2D0MUNU.STRIP.DST',
																        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09j-ReDecay01/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/12873002/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/12873002/BC2D0MUNU.STRIP.DST',
																        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/12873002/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2Filtered/12873002/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2Filtered/12873002/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/12873002/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/12873002/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/12873002/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/12873002/BC2D0MUNU.STRIP.DST'},
                           },
      # 12875407
			'Bu2D0MuNu_K3Pi'   : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09j-ReDecay01/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/12875407/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09j-ReDecay01/Trig0x40760037/Reco14c/Stripping21r1p2Filtered/12875407/BC2D0MUNU.STRIP.DST'} ,
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09j-ReDecay01/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/12875407/BC2D0MUNU.STRIP.DST',
																        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09j-ReDecay01/Trig0x409f0045/Reco14c/Stripping21r0p2Filtered/12875407/BC2D0MUNU.STRIP.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/12875407/BC2D0MUNU.STRIP.DST',
																        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered/12875407/BC2D0MUNU.STRIP.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2Filtered/12875407/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x6138160F/Reco16/Turbo03a/Stripping28r2Filtered/12875407/BC2D0MUNU.STRIP.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/12875407/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/12875407/BC2D0MUNU.STRIP.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/12875407/BC2D0MUNU.STRIP.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09i-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/12875407/BC2D0MUNU.STRIP.DST'},
                           },
      # 12875420
      'Bu2D0XMuNu'       : { '2012' : { 'MD' : '',
                                        'MU' : ''},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/12875420/DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/12875420/DST'},
                           },
      # 12163001
      'Bu2D0Pi'          : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09b/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/12163001/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09b/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/12163001/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09e-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/12163001/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09e-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/12163001/ALLSTREAMS.DST'},
                           },
      # 12103024
      'Bu2KPiPi'         : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12103024/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12103024/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '',
                                        'MU' : ''},
                           },
      # 11102001 / 11102005
      'Bd2KPi'           : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11102001/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11102001/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11102003/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11102003/ALLSTREAMS.DST'},
                           },
      # 11144050
      'Bd2JpsiKPi'       : { '2012' : { 'MD' : '',
                                        'MU' : ''},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11144050/ALLSTREAMS.MDST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11144050/ALLSTREAMS.MDST'},
                           },
      # 11144071
      'Bd2JpsiKK'       : { '2012' : { 'MD' : '',
                                       'MU' : ''},
                            '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11144071/ALLSTREAMS.DST',
                                       'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11144071/ALLSTREAMS.DST'},
                           },
      # 11144061
      'Bd2JpsiPiPi'     : { '2012' : { 'MD' : '',
                                       'MU' : ''},
                            '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/11144061/ALLSTREAMS.MDST',
                                       'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/11144061/ALLSTREAMS.DST'},
                           },
      # 11104041
      'Bd2KstRho'       : { '2012' : { 'MD' : '',
                                       'MU' : ''},
                            '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11104041/ALLSTREAMS.MDST',
                                       'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11104041/ALLSTREAMS.MDST'},
                           },
      # 11104020
      'Bd2KstPhi'       : { '2012' : { 'MD' : '',
                                       'MU' : ''},
                            '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11104020/ALLSTREAMS.MDST',
                                       'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/11104020/ALLSTREAMS.MDST'},
                           },

      },
  'JpsiMuNu' : # to be filled in
      {
      # Config (relate reconstruction mode to simulated decay)
      'Config' : { 'Bc2JpsiMuNuX'  : [ 'Bc2JpsiMuNu', 'Bc2JpsiTauNu', 'Bc2Psi2SMuNu', 'Bc2ChiC2MuNu', 'Bc2ChiCMuNu', 'Bc2JpsiDx', 'Bc2JpsiDxKx', 'Bc2JpsiDsx', 'Bu2JpsiX', 'Bd2JpsiX', 'Bs2JpsiX' ] ,
                 },

      # 14543010
			'Bc2JpsiMuNu'      : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
														 '2015' : { 'MD' : '/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14543010/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09i-ReDecay02/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14543010/ALLSTREAMS.DST'},
                           },
      # 14643048
			'Bc2JpsiTauNu'     : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14643048/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14643048/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14643048/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14643048/ALLSTREAMS.DST'},
                             '2015' : { 'MD' : '',
                                        'MU' : ''},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/14643048/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/14643048/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14643048/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14643048/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14643048/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14643048/ALLSTREAMS.DST'},
                           },
      # 14845008
			'Bc2Psi2SMuNu'     : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14845008/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14845008/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14845008/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14845008/ALLSTREAMS.DST'},
                             '2015' : { 'MD' : '',
                                        'MU' : ''},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14845008/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14845008/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14845008/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14845008/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14845008/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14845008/ALLSTREAMS.DST'},
                           },
      # 14543201
			'Bc2ChiCMuNu'      : { '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h-ReDecay02/Trig0x6139160F/Reco16/Turbo03/Stripping28r1p1NoPrescalingFlagged/14543201/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h-ReDecay02/Trig0x6139160F/Reco16/Turbo03/Stripping28r1p1NoPrescalingFlagged/14543201/ALLSTREAMS.DST'},
                           },
      # 14743201
			'Bc2ChiC2MuNu'     : { '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09h-ReDecay02/Trig0x6139160F/Reco16/Turbo03/Stripping28r1p1NoPrescalingFlagged/14743201/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09h-ReDecay02/Trig0x6139160F/Reco16/Turbo03/Stripping28r1p1NoPrescalingFlagged/14743201/ALLSTREAMS.DST'},
                           },
      # 14873607
			'Bc2JpsiDx'        : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14873607/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14873607/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14873607/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14873607/ALLSTREAMS.DST'},
                             '2015' : { 'MD' : '',
                                        'MU' : ''},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873607/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873607/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873607/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873607/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873607/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873607/ALLSTREAMS.DST'},
                           },
      # 14873430
			'Bc2JpsiDxKx'      : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14873430/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14873430/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14873430/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14873430/ALLSTREAMS.DST'},
                             '2015' : { 'MD' : '',
                                        'MU' : ''},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873430/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873430/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873430/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873430/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873430/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873430/ALLSTREAMS.DST'},
                           },
      # 14873620
			'Bc2JpsiDsx'       : { '2011' : { 'MD' : '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14873620/ALLSTREAMS.DST',
                                        'MU' : '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/14873620/ALLSTREAMS.DST'},
														 '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14873620/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14873620/ALLSTREAMS.DST'},
                             '2015' : { 'MD' : '',
                                        'MU' : ''},
														 '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873620/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/14873620/ALLSTREAMS.DST'},
														 '2017' : { 'MD' : '/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873620/ALLSTREAMS.DST',
                                        'MU' : '/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14873620/ALLSTREAMS.DST'},
														 '2018' : { 'MD' : '/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873620/ALLSTREAMS.DST',
                                        'MU' : '/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/14873620/ALLSTREAMS.DST'},
                           },
      # 11442012
      'Bd2JpsiX'         : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/11442012/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/11442012/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/11442012/DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/11442012/DST'},
                           },
      # 12442012 / 12442001
      'Bu2JpsiX'         : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/12442012/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/12442012/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/12442001/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/12442001/ALLSTREAMS.DST'},
                           },
      # 13442012 / 13442001
      'Bs2JpsiX'         : { '2012' : { 'MD' : '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13442012/ALLSTREAMS.DST',
                                        'MU' : '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13442012/ALLSTREAMS.DST'},
                             '2016' : { 'MD' : '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/13442001/ALLSTREAMS.DST',
                                        'MU' : '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/13442001/ALLSTREAMS.DST'},
                           },
      # Example
      'Name'             : { '2012' : { 'MD' : 'path',
                                        'MU' : 'path'},
                             '2016' : { 'MD' : 'path',
                                        'MU' : 'path'},
                           },
      },
}

input_mc_tags = {
    'D0MuNu' :
    {
      'Bu2D0XMuNu'       : { '2012' : { 'MD' : None,
                                        'MU' : None},
                             '2016' : { 'MD' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-mu100' }},
                           },
      'Bu2KPiPi'         : { '2012' : { 'MD' : {'DDDBtag' : 'dddb-20120831' , 'CondDBtag' : 'sim-20121025-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20120831' , 'CondDBtag' : 'sim-20121025-vc-mu100' }},
                             '2016' : { 'MD' : None,
                                        'MU' : None},
                           },
      'Bd2KPi'           : { '2012' : { 'MD' : {'DDDBtag' : 'dddb-20120831' , 'CondDBtag' : 'sim-20121025-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20120831' , 'CondDBtag' : 'sim-20121025-vc-mu100' }},
                             '2016' : { 'MD' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-mu100' }},
                           },
    },
    'JpsiMuNu' :
    {
      'Bd2JpsiX'         : { '2012' : { 'MD' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-mu100' }},
                             '2016' : { 'MD' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-mu100' }},
                           },
      'Bu2JpsiX'         : { '2012' : { 'MD' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-mu100' }},
                             '2016' : { 'MD' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-mu100' }},
                           },
      'Bs2JpsiX'         : { '2012' : { 'MD' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20130929' , 'CondDBtag' : 'sim-20130522-vc-mu100' }},
                             '2016' : { 'MD' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-md100' },
                                        'MU' : {'DDDBtag' : 'dddb-20150724' , 'CondDBtag' : 'sim-20161124-vc-mu100' }},
                           },
    }
}

# -------- JOB CONFIG DICTIONARIES ------ #
# MC
if opts.simulation:
	dv_version = {
			'D0MuNu' :
					{ '2011' : 'DaVinciDev_v39r1p6',
						'2012' : 'DaVinciDev_v39r1p6',
						'2015' : 'DaVinciDev_v44r10p5',
						'2016' : 'DaVinciDev_v44r10p5',
						'2017' : 'DaVinciDev_v42r7p2',
						'2018' : 'DaVinciDev_v44r4',
					},
			'JpsiMuNu' :
					{ '2011' : 'DaVinciDev_v36r1p1',
						'2012' : 'DaVinciDev_v36r1p1',
						'2015' : 'DaVinciDev_v44r10p5',
						'2016' : 'DaVinciDev_v44r10p5',
						'2017' : 'DaVinciDev_v42r7p2',
						'2018' : 'DaVinciDev_v44r4',
					}
			}

	dv_platform = {
			'D0MuNu' :
					{ '2011' : 'x86_64-slc6-gcc49-opt',
						'2012' : 'x86_64-slc6-gcc49-opt',
						'2015' : 'x86_64-centos7-gcc7-opt',
						'2016' : 'x86_64-centos7-gcc7-opt',
						'2017' : 'x86_64-slc6-gcc62-opt',
						'2018' : 'x86_64-centos7-gcc7-opt',
					},
			'JpsiMuNu' :
					{ '2011' : 'x86_64-slc6-gcc48-opt',
						'2012' : 'x86_64-slc6-gcc48-opt',
						'2015' : 'x86_64-centos7-gcc7-opt',
						'2016' : 'x86_64-centos7-gcc7-opt',
						'2017' : 'x86_64-slc6-gcc62-opt',
						'2018' : 'x86_64-centos7-gcc7-opt',
					}
			}
# Data
else:
	dv_version = {
			'D0MuNu' :
					{ '2011' : 'DaVinciDev_v45r2',
						'2012' : 'DaVinciDev_v45r2',
						'2015' : 'DaVinciDev_v45r2',
						'2016' : 'DaVinciDev_v45r2',
						'2017' : 'DaVinciDev_v45r2',
						'2018' : 'DaVinciDev_v45r2',
					},
			'JpsiMuNu' :
					{ '2011' : 'DaVinciDev_v45r2',
						'2012' : 'DaVinciDev_v45r2',
						'2015' : 'DaVinciDev_v45r2',
						'2016' : 'DaVinciDev_v45r2',
						'2017' : 'DaVinciDev_v45r2',
						'2018' : 'DaVinciDev_v45r2',
					}
			}

	dv_platform = {
			'D0MuNu' :
					{ '2011' : 'x86_64-centos7-gcc8-opt',
						'2012' : 'x86_64-centos7-gcc8-opt',
						'2015' : 'x86_64-centos7-gcc8-opt',
						'2016' : 'x86_64-centos7-gcc8-opt',
						'2017' : 'x86_64-centos7-gcc8-opt',
						'2018' : 'x86_64-centos7-gcc8-opt',
					},
			'JpsiMuNu' :
					{ '2011' : 'x86_64-centos7-gcc8-opt',
						'2012' : 'x86_64-centos7-gcc8-opt',
						'2015' : 'x86_64-centos7-gcc8-opt',
						'2016' : 'x86_64-centos7-gcc8-opt',
						'2017' : 'x86_64-centos7-gcc8-opt',
						'2018' : 'x86_64-centos7-gcc8-opt',
					}
			}

# FUNCTION TO GET LFNS IN RIGHT FORMAT FOR GANGA
def GetListOfLFNsFromPyFile( pyfile ):
  lfn_list = []
  f = open(pyfile)
  for line in f.readlines():
    if line.startswith('#'): continue
    if not 'LFN' in line: continue
    line = line.strip('IOHelper(\'ROOT\').inputFiles([')
    line = line.replace(',','')
    line = line.replace('\n','')
    line = line.replace('\'','')
    lfn_list.append(line)
  f.close()
  return lfn_list

# FUNCTION TO GET REC DEC FROM MC DEC
def LookUpRecDecayFromMCDec( mc_dec, cfg ):
  for recdec, mcdecs in cfg.items():
    if mc_dec in mcdecs: return recdec
  sys.exit( 'Could not find appropriate rec decay for mc decay \''+mc_dec+'\' in MC Config' )

# FUNCTION TO GET APPROPRIATE TAGS WHEN NEEDED
def FindTags( input_mc_tags, mode, mcdec, year, polarity ):
  if mode in list(input_mc_tags.keys()):
    if mcdec in list(input_mc_tags[mode].keys()):
      if year in list(input_mc_tags[mode][mcdec].keys()):
        if polarity in list(input_mc_tags[mode][mcdec][year].keys()):
          return input_mc_tags[mode][mcdec][year][polarity]
  return None

# FUNCTION TO MAKE AND SUBMIT THE JOB IN GANGA
def CreateGangaJob( dv_version, dv_platform, year, job_name, recdec, data_path, input_data, fpj, tags=None ):

  print('Creating Job \'%s\''%job_name)
  print('\t Input:       ', data_path, ' (%d files)'%len(input_data.getLFNs()))
  print('\t DV ver:      ', dv_version)
  print('\t DV plat:     ', dv_platform)
  print('\t FilesPerJob: ',fpj)

  myApp = GaudiExec()
  myApp.directory = os.path.join(opts.user_area, dv_version)
  myApp.platform = dv_platform
  #myApp.options = [ opts.optsfile ]
  os.system('cp %s %s/%s/Phys/DecayTreeTuple/python/'%(opts.optsfile,opts.user_area,dv_version))

  inputtype = 'MDST' if '.MDST' in data_path else 'DST'
  myApp.extraOpts = (
      'from ClassPrototype_Bc2D0MuNuX import DaVinciConfig ; '+
      'DV = DaVinciConfig( \'%s\', \'%s\', %s, \'%s\', %d, %s) ;'%(recdec,inputtype,str(opts.simulation),year,opts.events,opts.slim) +
      'DV.apply_configuration() ;'
    )
  if opts.simulation:
    if tags:
      myApp.extraOpts += (
          'from Configurables import DaVinci ; '+
          'DaVinci().DDDBtag = \'%s\' ; '%tags['DDDBtag'] +
          'DaVinci().CondDBtag = \'%s\' ;'%tags['CondDBtag']
        )
    else:
      myApp.autoDBtags = True

  job = Job( name = job_name )
  job.comment = job_name
  job.application = myApp
  job.inputfiles = [ LocalFile(os.path.join(opts.user_area, dv_version, 'Phys/SemileptonicCommonTools/src/weights.xml') ) ]
  job.inputdata = input_data
  if opts.test:
    job.outputfiles = [ LocalFile('*.root') ]
    job.backend = Local()
  else:
    if opts.simulation:
      job.outputfiles = [ LocalFile('*.root') ]
    else:
      job.outputfiles = [ DiracFile('*.root') ]
    job.splitter = SplitByFiles( filesPerJob = fpj, ignoremissing = True )
    job.backend = Dirac()
    job.backend.settings['CPUTime'] = 100000000
    if len( opts.bannedSites ) > 0:
      job.backend.settings['BannedSites'] = opts.bannedSites
    #job.postprocessors.append(RootMerger(files = [ "Bc2D0MuNuX.root" ], ignorefailed = True, overwrite = True) )

  if opts.submit:
    job.submit()

  return job.id

# ACTUALLY EXECUTE JOB CREATION HERE
logdict = {}
for mode in opts.mode:
	for year in opts.year:
		for polarity in opts.polarity:
			# get input location
			## --- WILL LOOK UP ALL PATHS IN BK --- ##
			## ----- RUN ON MC ----- ##
			if opts.simulation:
				for mcdec in opts.mcdecays:
					recdec = LookUpRecDecayFromMCDec( mcdec, input_mc_bk_paths[mode]['Config'] )
					if mode not in input_mc_bk_paths.keys(): continue
					if mcdec not in input_mc_bk_paths[mode].keys(): continue
					if year not in input_mc_bk_paths[mode][mcdec].keys(): continue
					if polarity not in input_mc_bk_paths[mode][mcdec][year].keys(): continue
					data_path = input_mc_bk_paths[mode][mcdec][year][polarity]
					if data_path == '': continue
					bkq = BKQuery( data_path )
					dset = bkq.getDataset()
					print('Data at path ', data_path, ' contains %d files'%len(dset.getLFNs()))
					if len(dset.getLFNs())==0:
						print('No files found at this path so not submitting this job')
						continue
					if opts.check:
						continue
					fpj = opts.filesPerJob
					if opts.filesPerJob == -1:
						maxjobs = 25
						minjobs = 5
						fpj = max(minjobs, int(round((len(dset.getLFNs())/float(maxjobs))+0.5)))
					print('Will run %d files per job'%fpj)

					job_name = 'MC_%s_%s_%s_%s'%(mcdec,mode,year,polarity)
					if opts.test:
						dset.files[0].suffixes = dset.files[0].suffixes[0:opts.filesPerJob]

					tags = FindTags( input_mc_tags, mode, mcdec, year, polarity )
					print(tags)

					if not opts.dryRun:
						jid = CreateGangaJob( dv_version[mode][year], dv_platform[mode][year], year, job_name, recdec, data_path, dset, fpj, tags )
						# fill log dictionary
						if mcdec not in logdict.keys(): logdict[mcdec] = {}
						if year not in logdict[mcdec].keys(): logdict[mcdec][year] = {}
						if polarity not in logdict[mcdec][year].keys(): logdict[mcdec][year][polarity] = jid

			## ---- RUN ON DATA ---- ##
			else:
				data_path = input_data_bk_paths[mode][year][polarity]
				bkq = BKQuery( data_path )
				dset = bkq.getDataset()
				print('Data at path ', data_path, ' contains %d files'%len(dset.getLFNs()))
				if len(dset.getLFNs())==0:
					print('No files found at this path so not submitting this job')
					continue
				if opts.check:
					continue
				fpj = opts.filesPerJob
				if opts.filesPerJob == -1:
					fpj = max(20, int(round((len(dset.getLFNs())/400.)+0.5)))

				job_name = 'Data_%s_%s_%s'%(mode,year,polarity)
				if opts.test:
					dset.files[0].suffixes = dset.files[0].suffixes[0:opts.filesPerJob]
				recdec = 'Bc2D0MuNuX_allModes' if mode == 'D0MuNu' else 'Bc2JpsiMuNuX'

				if not opts.dryRun:
					jid = CreateGangaJob( dv_version[mode][year], dv_platform[mode][year], year, job_name, recdec, data_path, dset, fpj )
					# fill log dictionary
					if mode not in logdict.keys(): logdict[mode] = {}
					if year not in logdict[mode].keys(): logdict[mode][year] = {}
					if polarity not in logdict[mode][year].keys(): logdict[mode][year][polarity] = jid

# file for tracking submission
typstr = 'mc' if opts.simulation else 'data'
logf = open('log/sub_%s_%d.py'%(typstr,int(time.time())),'w')
logf.write('job_map = {\n')
for mode, mitems in logdict.items():
	logf.write('  {:20s} : {{\n'.format('\''+mode+'\'') )
	for year, yitems in mitems.items():
		itstr = ' '*26
		itstr += '\''+year+'\': {'
		if 'MU' in yitems.keys(): itstr += '\'MU\': ' + str(yitems['MU']) + ','
		if 'MD' in yitems.keys(): itstr += '\'MD\': ' + str(yitems['MD']) + '},\n'
		logf.write(itstr)
	logf.write('  },\n')
logf.write('}\n')


