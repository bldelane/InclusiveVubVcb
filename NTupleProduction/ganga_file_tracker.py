import json
import os

def file_log(jobid):

	log = {}
	for sj in jobs(jobid).subjobs:
		inputfile_lfn_base = sj.inputdata.files[0].lfn_prefix
		inputfile_names    = sj.inputdata.files[0].suffixes
		inputfile_lfns     = []
		outputfile_lfns    = []

		for f in sj.outputfiles:
			outputfile_lfns.append( f.lfn )

		for f in inputfile_names:
			inputfile_lfns.append( os.path.join(inputfile_lfn_base, f) )

		log[sj.id] = { 'output' : outputfile_lfns, 'input' : inputfile_lfns }


	basepath = '/home/epp/phskbk/gangadir/workspace/phskbk/LocalXML/'
	os.system('mkdir -p %s/%d/output'%(basepath,jobid))
	path = os.path.join(basepath,str(job_id),'output/file_tracker.json')
	logf = open(path,'w')
	json.dump(log,logf,indent=2)
	logf.close()

import sys
if len(sys.argv)!=2: sys.exit('<usage> ganga ganga_file_tracker.py <jid>')

jid = int(sys.argv[1])

file_log(jid)
