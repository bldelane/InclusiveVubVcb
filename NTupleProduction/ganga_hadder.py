from __future__ import print_function
import os
import sys
import importlib
import pprint
import subprocess
from argparse import ArgumentParser
parser = ArgumentParser(usage='python ganga_hadder.py [options]')
parser.add_argument('-j','--jobid', dest='jobid', type=int, default=None, help='Ganga Job ID')
parser.add_argument('-d','--dict' , dest='dict' , default=None , help='Path to python file containing job dictionary')
parser.add_argument('-f','--force', dest='force', default=False, action='store_true', help='Force overwrite of output file')
parser.add_argument('-p','--path' , dest='path' , default='/storage/epp2/phskbk/gangadir/workspace/phskbk/LocalXML/', help='Basepath for ganga output')
parser.add_argument('-n','--name' , dest='name' , default=None ,                      help='Output file name')
parser.add_argument('-D','--dryrun', dest='dryrun', default=False, action='store_true', help='Don\'t actually execute the add')
parser.add_argument('-c','--noclean', dest='noclean', default=False, action='store_true', help='Turn off cleaning up of old files if all jobs are done')
opts = parser.parse_args()

def hadd(jobid):

	path = os.path.join(opts.path,str(jobid))

	if not os.path.exists(path):
		sys.exit('Could not find path to job %d'%jobid)

	# go and find the subjob directories and files
	nsubjobs = None
	for root, dirs, files in os.walk( path ):
		if root!=path: continue
		if 'input' in dirs: dirs.remove('input')
		if 'output' in dirs: dirs.remove('output')
		dir_list_as_ints = sorted([ int(d) for d in dirs ])
		nsubjobs = len(dir_list_as_ints)
		unique_files = set()
		for d in dir_list_as_ints:
			subpath = os.path.join(path,'%d'%d,'output')
			for sroot, sdirs, sfiles in os.walk( subpath ):
				if sroot!=subpath: continue
				for f in sfiles:
					if f.endswith('.root'):
						unique_files.add(f)

	# now we need to check that each file exists in each subdirectory and
	# if not then remove the subdirectory from the list
	for ufil in unique_files:
		copy_list = dir_list_as_ints[:]
		for d in copy_list:
			if not os.path.exists( os.path.join( opts.path, str(jobid), str(d), 'output', ufil ) ):
				dir_list_as_ints.remove(d)

	print( 'Subdirectories found', dir_list_as_ints )
	print( 'Unique files found', unique_files )
	print( 'Figure that', len(dir_list_as_ints), '/', nsubjobs, 'jobs have completed' )
	output_loc = os.path.join(path,'output')
	print( 'Output location', output_loc )

	# now we should check if an hadd has already been done if there
	# are missing files
	hadd_log_path = os.path.join(output_loc,'hadd.log')
	if os.path.exists(hadd_log_path):
		haddf = open(hadd_log_path)
		found_sj_line=False
		prev_unique_files = set()
		prev_dir_list_as_ints = []
		for line in haddf.readlines():
			if line.startswith('Hadded files'):
				continue
			if line.startswith('Used subjob'):
				found_sj_line = True
				continue
			if found_sj_line:
				prev_dir_list_as_ints.append( int(line.split()[0].strip('\n')) )
			else:
				prev_unique_files.add( line.split()[0].strip('\n') )

		something_new = False

		#print(unique_files)
		#print(prev_unique_files)
		#print(dir_list_as_ints)
		#print(prev_dir_list_as_ints)

		for ufil in unique_files:
			if ufil not in prev_unique_files:
				something_new = True
			if not os.path.exists( os.path.join(output_loc,ufil) ):
				print('It looks like an old hadded file', os.path.join(output_loc,ufil),'has been deleted so will now recreate it')
		for d in dir_list_as_ints:
			if d not in prev_dir_list_as_ints: something_new = True

		if not something_new:
			if not opts.force:
				print('It looks like the hadd has already taken place so going to just bail out now')
				return
			else:
				print('There are already hadded files in place but you have forced an overwrite so going to now rerun the hadd')
		else:
			print('There are already hadded files in place but from the logs it looks like there is new subjob file so going to now rerun the hadd')

	# Now perform the hadd
	os.system('mkdir -p %s'%output_loc)

	for ufil in unique_files:
		add_str = 'lb-run DaVinci/latest hadd -ff'
		add_str += ' %s'%(os.path.join(output_loc,ufil))

		for d in dir_list_as_ints:
			add_str += ' %s'%(os.path.join(path,str(d),'output',ufil))

		print( 'Hadding file', os.path.join(output_loc,ufil) )
		if not opts.dryrun:
			subprocess.run( add_str, shell=True, capture_output=False, check=True )

	if not opts.dryrun:
		outf = open(hadd_log_path,'w')
		outf.write('Hadded files:\n')
		for ufil in unique_files:
			outf.write('  %s\n'%ufil)
		outf.write('Used subjobs:\n')
		for d in dir_list_as_ints:
			outf.write('  %d\n'%d)
		outf.close()

	if not opts.noclean and len(dir_list_as_ints)==nsubjobs:
		print('As all jobs have completed will now clean up.')
		print('Removing files from', nsubjobs, 'subjobs.')
		for ufil in unique_files:
			if os.path.exists( os.path.join(path, 'output', ufil) ):
				for d in dir_list_as_ints:
					if not opts.dryrun:
						subprocess.run( 'rm -f %s'%os.path.join(path,str(d),'output',ufil), shell=True )

def process_dict(job_map):

	for mode, iitems in job_map.items():
		for year, jitems in iitems.items():
			for pol, jid in jitems.items():
				#print(mode, year, pol, jid)
				if jid is not None:
					hadd(jid)

## __main__ ##
if opts.jobid and not opts.dict:
	hadd(opts.jobid)
else:
	if os.path.exists(opts.dict):
		spec = importlib.util.spec_from_file_location('module',opts.dict)
		module = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(module)
		if hasattr(module, 'job_map'):
			jmap = module.job_map
			pp = pprint.PrettyPrinter(indent=2)
			pp.pprint(jmap)
			process_dict(jmap)

		#process_dict()


