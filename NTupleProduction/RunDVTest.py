from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-m','--mode', required=True, help='Which mode to test', choices=['D0MuNu','JpsiMuNu','D0MuNu_K3Pi'] )
parser.add_argument('-y','--year', required=True, help='Which year to test', choices=['2011','2012','2015','2016','2017','2018'] )
parser.add_argument('-d','--dtype', required=True, help='Which data type', choices=['DATA','MC'])
parser.add_argument('-l','--runloc', default='../../TestRuns', help='Location to run tests in' )
parser.add_argument('-D','--dvloc' , default='../DaVinci_DevX', help='Location of DV builds (must be relative to run location)' )
parser.add_argument('-e','--events', default=-1, help='Number of events to run')
opts = parser.parse_args()

import os
os.system('mkdir -p %s'%opts.runloc)
os.system('cp ClassPrototype_Bc2D0MuNuX.py %s/TestRun_ClassPrototype.py'%opts.runloc)
os.system('cp weights.xml %s/'%opts.runloc)
os.chdir( opts.runloc )

lfpath = '/home/epp/phskbk/Scratch/lhcb/Bc2D0MuNu/TestFiles/'

local_lfns = { 'DATA' : { 'D0MuNu'  : { '2011' : [ lfpath+'DATA_D0MuNu_2011_00093829_00017637_1.semileptonic.dst' ] ,
                                        '2012' : [ lfpath+'DATA_D0MuNu_2012_00095378_00013240_1.semileptonic.dst' ],
                                        '2015' : [ lfpath+'DATA_D0MuNu_2015_00102452_00010700_1.semileptonic.dst' ],
                                        '2016' : [ lfpath+'DATA_D0MuNu_2016_00103804_00026595_1.semileptonic.dst' ],
                                        '2017' : [ lfpath+'DATA_D0MuNu_2017_00071907_00003880_1.semileptonic.dst' ],
                                        '2018' : [ lfpath+'DATA_D0MuNu_2018_00075559_00034472_1.semileptonic.dst' ],
                                      },
                          'JpsiMuNu': { '2011' : [ lfpath+'DATA_JpsiMuNu_2011_00041840_00001436_1.dimuon.dst' ] ,
                                        '2012' : [ lfpath+'DATA_JpsiMuNu_2012_00041836_00070137_1.dimuon.dst' ],
                                        '2015' : [ lfpath+'DATA_JpsiMuNu_2015_00102452_00011313_1.dimuon.dst' ],
                                        '2016' : [ lfpath+'DATA_JpsiMuNu_2016_00103400_00005491_1.dimuon.dst' ],
                                        '2017' : [ lfpath+'DATA_JpsiMuNu_2017_00071700_00004493_1.dimuon.dst' ],
                                        '2018' : [ lfpath+'DATA_JpsiMuNu_2018_00075559_00039636_1.dimuon.dst' ],
                                      },
                        },
               'MC'   : { 'D0MuNu'  : { '2011' : [ lfpath+'MC_Bc2D0MuNu_2011_00122559_00000004_1.bc2d0munu.strip.dst'] ,
                                        '2012' : [ lfpath+'MC_Bc2D0MuNu_2012_00115077_00000004_1.bc2d0munu.strip.dst'],
                                        '2015' : [ lfpath+'MC_Bc2D0MuNu_2015_00115367_00000002_1.bc2d0munu.strip.dst'],
                                        '2016' : [ lfpath+'MC_Bc2D0MuNu_2016_00115724_00000005_1.bc2d0munu.strip.dst'],
                                        '2017' : [ lfpath+'MC_Bc2D0MuNu_2017_00115860_00000006_1.bc2d0munu.strip.dst'],
                                        '2018' : [ lfpath+'MC_Bc2D0MuNu_2018_00116149_00000007_1.bc2d0munu.strip.dst'],
                                      },
                          'D0MuNu_K3Pi': { '2011' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2011_00122568_00000013_1.bc2d0munu.strip.dst'] ,
                                           '2012' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2012_00115065_00000012_1.bc2d0munu.strip.dst'],
                                           '2015' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2015_00115373_00000003_1.bc2d0munu.strip.dst'],
                                           '2016' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2016_00115712_00000011_1.bc2d0munu.strip.dst'],
                                           '2017' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2017_00115863_00000007_1.bc2d0munu.strip.dst'],
                                           '2018' : [ lfpath+'MC_Bc2D0MuNu_K3Pi_2018_00116131_00000001_1.bc2d0munu.strip.dst'],
                                      },
                          'JpsiMuNu': { '2011' : [ lfpath+'MC_Bc2JpsiMuNu_2011_00114442_00000241_5.AllStreams.dst'] ,
                                        '2012' : [ lfpath+'MC_Bc2JpsiMuNu_2012_00114526_00000115_5.AllStreams.dst'],
                                        '2015' : [ lfpath+'MC_Bc2JpsiMuNu_2015_00114530_00000037_6.AllStreams.dst'],
                                        '2016' : [ lfpath+'MC_Bc2JpsiMuNu_2016_00114534_00000135_7.AllStreams.dst'],
                                        '2017' : [ lfpath+'MC_Bc2JpsiMuNu_2017_00114606_00000132_7.AllStreams.dst'],
                                        '2018' : [ lfpath+'MC_Bc2JpsiMuNu_2018_00114666_00000008_7.AllStreams.dst'],
                                      },
                         },
             }

dv_version = {
        'MC':
            { 'D0MuNu' :
                    { '2011' : 'DaVinciDev_v39r1p6',
                        '2012' : 'DaVinciDev_v39r1p6',
                        '2015' : 'DaVinciDev_v44r10p5',
                        '2016' : 'DaVinciDev_v44r10p5',
                        '2017' : 'DaVinciDev_v42r7p2',
                        '2018' : 'DaVinciDev_v44r4',
                    },
              'D0MuNu_K3Pi' :
                    { '2011' : 'DaVinciDev_v39r1p6',
                        '2012' : 'DaVinciDev_v39r1p6',
                        '2015' : 'DaVinciDev_v44r10p5',
                        '2016' : 'DaVinciDev_v44r10p5',
                        '2017' : 'DaVinciDev_v42r7p2',
                        '2018' : 'DaVinciDev_v44r4',
                    },
                'JpsiMuNu' :
                    { '2011' : 'DaVinciDev_v36r1p1',
                        '2012' : 'DaVinciDev_v36r1p1',
                        '2015' : 'DaVinciDev_v44r10p5',
                        '2016' : 'DaVinciDev_v44r10p5',
                        '2017' : 'DaVinciDev_v42r7p2',
                        '2018' : 'DaVinciDev_v44r4',
                    },
            },
        'DATA':
            { 'D0MuNu' :
                    { '2011' : 'DaVinciDev_v45r2',
                        '2012' : 'DaVinciDev_v45r2',
                        '2015' : 'DaVinciDev_v45r2',
                        '2016' : 'DaVinciDev_v45r2',
                        '2017' : 'DaVinciDev_v45r2',
                        '2018' : 'DaVinciDev_v45r2',
                    },
                'D0MuNu_K3Pi' :
                    { '2011' : 'DaVinciDev_v45r2',
                        '2012' : 'DaVinciDev_v45r2',
                        '2015' : 'DaVinciDev_v45r2',
                      '2016' : 'DaVinciDev_v45r2',
                        '2017' : 'DaVinciDev_v45r2',
                        '2018' : 'DaVinciDev_v45r2',
                    },
                'JpsiMuNu' :
                    { '2011' : 'DaVinciDev_v45r2',
                        '2012' : 'DaVinciDev_v45r2',
                        '2015' : 'DaVinciDev_v45r2',
                      '2016' : 'DaVinciDev_v45r2',
                        '2017' : 'DaVinciDev_v45r2',
                        '2018' : 'DaVinciDev_v45r2',
                    }
            },
}

dv_platform = {
        'MC':
            { 'D0MuNu' :
                    { '2011' : 'x86_64-slc6-gcc49-opt',
                        '2012' : 'x86_64-slc6-gcc49-opt',
                        '2015' : 'x86_64-centos7-gcc7-opt',
                        '2016' : 'x86_64-centos7-gcc7-opt',
                        '2017' : 'x86_64-slc6-gcc62-opt',
                        '2018' : 'x86_64-centos7-gcc7-opt',
                    },
              'D0MuNu_K3Pi' :
                    { '2011' : 'x86_64-slc6-gcc49-opt',
                        '2012' : 'x86_64-slc6-gcc49-opt',
                        '2015' : 'x86_64-centos7-gcc7-opt',
                        '2016' : 'x86_64-centos7-gcc7-opt',
                        '2017' : 'x86_64-slc6-gcc62-opt',
                        '2018' : 'x86_64-centos7-gcc7-opt',
                    },
                'JpsiMuNu' :
                    { '2011' : 'x86_64-slc6-gcc48-opt',
                        '2012' : 'x86_64-slc6-gcc48-opt',
                        '2015' : 'x86_64-centos7-gcc7-opt',
                        '2016' : 'x86_64-centos7-gcc7-opt',
                        '2017' : 'x86_64-slc6-gcc62-opt',
                        '2018' : 'x86_64-centos7-gcc7-opt',
                    }
            },
        'DATA':
            { 'D0MuNu' :
                    { '2011' : 'x86_64-centos7-gcc8-opt',
                        '2012' : 'x86_64-centos7-gcc8-opt',
                        '2015' : 'x86_64-centos7-gcc8-opt',
                        '2016' : 'x86_64-centos7-gcc8-opt',
                        '2017' : 'x86_64-centos7-gcc8-opt',
                        '2018' : 'x86_64-centos7-gcc8-opt',
                    },
                'D0MuNu_K3Pi' :
                    { '2011' : 'x86_64-centos7-gcc8-opt',
                        '2012' : 'x86_64-centos7-gcc8-opt',
                        '2015' : 'x86_64-centos7-gcc8-opt',
                        '2016' : 'x86_64-centos7-gcc8-opt',
                        '2017' : 'x86_64-centos7-gcc8-opt',
                        '2018' : 'x86_64-centos7-gcc8-opt',
                    },
                'JpsiMuNu' :
                    { '2011' : 'x86_64-centos7-gcc8-opt',
                        '2012' : 'x86_64-centos7-gcc8-opt',
                        '2015' : 'x86_64-centos7-gcc8-opt',
                        '2016' : 'x86_64-centos7-gcc8-opt',
                        '2017' : 'x86_64-centos7-gcc8-opt',
                        '2018' : 'x86_64-centos7-gcc8-opt',
                    }
            }
}

mode = { 'DATA' : { 'D0MuNu' : 'Bc2D0MuNuX_allModes', 'JpsiMuNu': 'Bc2JpsiMuNuX', 'D0MuNu_K3Pi' : 'Bc2D0MuNuX_allModes' },
                 'MC'   : { 'D0MuNu' : 'Bc2D0MuNuX_D02KPi'  , 'JpsiMuNu': 'Bc2JpsiMuNuX', 'D0MuNu_K3Pi' : 'Bc2D0MuNuX_D02K3Pi' }
             }

f = open('test_run_%s_%s_%s.sh'%(opts.dtype,opts.mode,opts.year),'w')
f.write('#/bin/bash\n')

#f.write('source /cvmfs/lhcb.cern.ch/group_login.sh\n')
#f.write('source `which LbLogin.sh` -c %s\n\n'%dv_platform[opts.dtype][opts.mode][opts.year])
f.write('source /cvmfs/lhcb.cern.ch/lib/LbEnv\n')
f.write('lb_set_platform %s\n\n'%dv_platform[opts.dtype][opts.mode][opts.year])

sim  = 'True'  if opts.dtype=='MC' else 'False'
slim = 'False' if opts.dtype=='MC' else 'True'

file_str = 'files = [ ' + ','.join('\'{0}\''.format(f) for f in local_lfns[opts.dtype][opts.mode][opts.year]) + ' ]'
cfg_line = 'DV = DaVinciConfig( \'{mode}\', \'DST\', {sim}, \'{year}\', {evs}, {slim}, False, files )'.format( mode=mode[opts.dtype][opts.mode], sim=sim, year=opts.year, evs=opts.events, slim=slim)

f.write( 'echo "%s" >> TestRun_ClassPrototype.py\n'%file_str )
f.write( 'echo "%s" >> TestRun_ClassPrototype.py\n'%cfg_line )
f.write( 'echo "DV.apply_configuration()" >> TestRun_ClassPrototype.py\n\n')

f.write( '%s/%s/run gaudirun.py TestRun_ClassPrototype.py 2>&1 | tee out_test_run_%s_%s_%s.log\n'%(opts.dvloc,dv_version[opts.dtype][opts.mode][opts.year],opts.dtype,opts.mode,opts.year) )

if opts.mode=='D0MuNu' or opts.mode=='D0MuNu_K3Pi':
    if opts.dtype=='MC':
        f.write(' mv Bc2D0MuNuX_DVHistos.root TestRun_%s_%s_%s_Bc2D0MuNuX_DVHistos.root \n'%(opts.dtype,opts.mode,opts.year) )
        f.write(' mv Bc2D0MuNuX.root TestRun_%s_%s_%s_Bc2D0MuNuX.root \n'%(opts.dtype,opts.mode,opts.year) )
    else:
        f.write(' mv Bc2D0MuNuXSlim_DVHistos.root TestRun_%s_%s_%s_Bc2D0MuNuXSlim_DVHistos.root \n'%(opts.dtype,opts.mode,opts.year) )
        f.write(' mv Bc2D0MuNuXSlim.root TestRun_%s_%s_%s_Bc2D0MuNuXSlim.root \n'%(opts.dtype,opts.mode,opts.year) )
else:
    if opts.dtype=='MC':
        f.write(' mv Bc2JpsiMuNuX_DVHistos.root TestRun_%s_%s_%s_Bc2JpsiMuNuX_DVHistos.root \n'%(opts.dtype,opts.mode,opts.year) )
        f.write(' mv Bc2JpsiMuNuX.root TestRun_%s_%s_%s_Bc2JpsiMuNuX.root \n'%(opts.dtype,opts.mode,opts.year) )
    else:
        f.write(' mv Bc2JpsiMuNuXSlim_DVHistos.root TestRun_%s_%s_%s_Bc2JpsiMuNuXSlim_DVHistos.root \n'%(opts.dtype,opts.mode,opts.year) )
        f.write(' mv Bc2JpsiMuNuXSlim.root TestRun_%s_%s_%s_Bc2JpsiMuNuXSlim.root \n'%(opts.dtype,opts.mode,opts.year) )

f.close()

os.system('chmod +x %s'%f.name)

os.system('/bin/bash %s'%f.name)

print('Ran %s in %s'%(f.name,os.getcwd()))

