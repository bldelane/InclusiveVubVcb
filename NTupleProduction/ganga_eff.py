from __future__ import print_function
import os
import sys
import importlib
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-b','--basepath', default='/storage/epp2/phskbk/gangadir/workspace/phskbk/LocalXML/', help='Base path to look in.')
parser.add_argument('-j','--jobid'   , default=[], action="append", help='Job IDs')
parser.add_argument('-r','--read'    , default=None, help='Read job ids from job map')
parser.add_argument('-f','--force'   , default=False, action="store_true", help='Do not run checks and force overwrite')
parser.add_argument('-s','--singlefile', default=None, help='Run on a single .log file (e.g. for ISGW2 private production)')
parser.add_argument('-o','--outputfile', default=None, help='If singlefile then put the output here')
parser.add_argument('-v','--verbose' , default=False, action="store_true", help='More output')
opts = parser.parse_args()

def write_eff_file(jobid=-1, singlefile=None):

  ganga_log_files = []
  if singlefile:
    if os.path.exists(singlefile):
      ganga_log_files.append(singlefile)
      path = ''

  else:
    path = os.path.join(opts.basepath,jobid)

    if not os.path.exists(path):
      sys.exit('Could not find path to job %s'%jobid)

    # go and find the subjob directories and files
    for root, dirs, files in os.walk( path ):
      if root!=path: continue
      if 'input' in dirs: dirs.remove('input')
      if 'output' in dirs: dirs.remove('output')
      dir_list_as_ints = sorted([ int(d) for d in dirs ])
      unique_files = set()
      for d in dir_list_as_ints:
        log_file_path = os.path.join(path,'%d'%d,'output','Script1_Ganga_GaudiExec.log')
        if os.path.exists(log_file_path):
          ganga_log_files.append( log_file_path )

  outfname = os.path.join(path,'output','eff.log')
  if opts.outputfile:
    os.system('mkdir -p %s'%os.path.dirname(opts.outputfile))
    outfname = opts.outputfile

  # write to file and print
  if not opts.force and os.path.exists( outfname ):
    raise RuntimeError('File already exists')

  outf = open(outfname,'w')

  tot_events_proc   = 0
  tot_events_passed = 0
  for glog in ganga_log_files:
    proc    = None
    passed  = None
    tproc   = None
    tpassed = None
    if opts.verbose: print('Reading file', glog)
    f = open(glog)
    for line in f.readlines():
      if line.startswith('DaVinciInitAlg') and 'events processed' in line:
        proc = int(line.split()[2])
      if 'accept' in line: ## we should simply pick up the last occurence
        if '*' not in line: continue
        passed = int(float(line.split()[4]))
      # and to test
      #if line.startswith('TimingAuditor') and 'EVENT LOOP' in line:
        #if len(line.split())==16: tproc = int(float(line.split()[12]))
        #else: tproc = int(float(line.split()[13]))
      #if line.startswith('TimingAuditor') and 'Tuple' in line:
        #if len(line.split())==15: tpassed = int(float(line.split()[11]))
        #else: tpassed = int(float(line.split()[12]))
    f.close()
    if opts.verbose:
      print(f'  Found {proc} events processed')
      print(f'  Found {passed} events passed')
      #print(f'  Found {tproc} events processed from TimingAuditor')
      #print(f'  Found {tpassed} events passed from TimingAuditor')

    # the output file exists but job hasn't completed successfully
    if not proc or not passed: # or not tproc or not tpassed:
      continue

    #if not opts.force:
      #if proc!=tproc:
        #print('Process check failed: ', proc, '!=', tproc)
        #assert(0)
      #if passed!=tpassed:
        #print('Pass check failed: ', passed, '!=', tpassed)
        #assert(0)

    if singlefile:
      sjid = 0
    else:
      sjid = int(glog.split(path)[1].split('output')[0].strip('/'))
    outl = 'File %2d: %8d / %-8d = %6.4f%%'%(sjid, passed,proc,100*(float(passed)/float(proc)))
    print( outl )
    outf.write( outl+'\n' )

    tot_events_proc += proc
    tot_events_passed += passed

  eff     = float(tot_events_passed)/float(tot_events_proc)

	## This is the proper way to get the error:
	#from ROOT import TEfficiency as TE
	#from ROOT import TMath as TM
	#eff_err_up = TE.Wilson(tot_events_proc, tot_events_passed, 1.-TM.Prob(1,1), True)  - eff
	#eff_err_dn = eff - TE.Wilson(tot_events_proc, tot_events_passed, 1.-TM.Prob(1,1), False)
	#eff_err = (eff_err_up+eff_err_dn)/2.

  eff_err = ( (eff*(1.-eff))/float(tot_events_proc) )**0.5

  outl = 'TOTAL  : %8d / %-8d = (%5.3f +/- %5.3f)%%'%(tot_events_passed,tot_events_proc,100*eff, 100*eff_err)
  print( outl )
  outf.write( outl+'\n' )
  outf.close()
  if opts.verbose: print('Output written to', outf.name)

## main bit ##
if opts.read is not None:

	if not os.path.exists(opts.read):
		raise RuntimeError('Cannot find file', opts.read)

	spec = importlib.util.spec_from_file_location('module',  opts.read)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	if not hasattr(module,'job_map'):
		raise RuntimeError('No dic job_map in file')

	for dec, cfg in module.job_map.items():
		for year, ycfg in cfg.items():
			for pol, jid in ycfg.items():
				if jid is None: continue
				#print(dec,year,pol,jid)

				opts.jobid.append(jid)

for jid in opts.jobid:
	write_eff_file(str(jid))

if opts.singlefile:
  write_eff_file(singlefile=opts.singlefile)
