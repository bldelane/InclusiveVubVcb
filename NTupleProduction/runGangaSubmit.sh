# submit a local test to run one job, on 5 files for both D0 and Jpsi modes
# can use to roughly benchmark timing and output size of jobs

ganga -i GangaSubmit.py -m D0MuNu -m JpsiMuNu -y 2016 -p MD -o /usera/mkenzie/lhcb/Bc2D0MuNuX/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /usera/mkenzie/lhcb/Bc2D0MuNuX/DaVinci_DevX -e -1 -t -x -f 5 # -S can also then submit to different backend if wanted

### BENCHMARKS ###
## 1 file Local D0MuNu  : 16 mins, 9.6M, 52618 evs, 2231 (2360) sel D0 evs,
##                                                    34 (  37) sel D0Fake evs,
##                                                  2049 (2662) sel D0K3Pi evs,
##                                                     7 (   7) sel D0K3PiFake evs,
## 1 file Local JpsiMuNu: 12 mins, 4.1M, 40271 evs, 1496 (1985) sel evs,

## SUBMIT E.G. 2016 DATA

ganga -i GangaSubmit.py -m D0MuNu -m JpsiMuNu -y 2016 -p MD -p MU -o /usera/mkenzie/lhcb/Bc2D0MuNuX/InclusiveVubVcb/NTupleProduction/ClassPrototype_Bc2D0MuNuX.py -u /usera/mkenzie/lhcb/Bc2D0MuNuX/DaVinci_DevX -e -1 -t -x -f 100 # -S
