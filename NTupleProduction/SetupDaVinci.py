## Should set up DV for you ##
## needs on argument that is the path you would like this to happen in ##
## default is ../../DaVinci_DevX

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-l','--loc', help='Path for DV installations', default='../../DaVinci_DevX')
parser.add_argument('-u','--user', help='Your lxplus username if not running on lxplus', default='mkenzie')
parser.add_argument('-f','--force',help='Force overwrite', default=False, action="store_true")
parser.add_argument('-q','--quiet',help='Write setup script but don\'t run it', default=False, action="store_true")
opts = parser.parse_args()

import sys
import os
path = os.path.join(os.getcwd(),opts.loc)

import socket
if 'lxplus' not in socket.gethostname():
	os.system('kinit -f %s@CERN.CH'%opts.user)

DV_cfg = {
		       'DaVinci_v36r1p1'  : { 'platform': 'x86_64-slc6-gcc48-opt'  , 'analysis': 'v13r1'  , 'slbranch': 'sltools-mkenzie-useAncientDV', 'strippingbranch':'tully-Bc2D0munu-S21-noPID'},
		       'DaVinci_v39r1p6'  : { 'platform': 'x86_64-slc6-gcc49-opt'  , 'analysis': 'v15r1p3', 'slbranch': 'sltools-mkenzie-usewOldDV',    'strippingbranch':'tully-Bc2D0munu-S21r0p2-noPID'},
		       'DaVinci_v42r7p2'  : { 'platform': 'x86_64-slc6-gcc62-opt'  , 'analysis': 'v18r6p1', 'slbranch': 'sltools-mkenzie-usewOldDV',    'strippingbranch':'tully-Bc2D0munu-S29r2-noPID'},
		       'DaVinci_v44r4'    : { 'platform': 'x86_64-centos7-gcc7-opt', 'analysis': 'v20r4'  , 'slbranch': 'sltools-mkenzie-usewDVv45r2',  'strippingbranch':'tully-Bc2D0munu-S34-noPID'},
		       'DaVinci_v44r10p5' : { 'platform': 'x86_64-centos7-gcc7-opt', 'analysis': 'v20r9p3', 'slbranch': 'sltools-mkenzie-usewDVv45r2',  'strippingbranch':'tully-Bc2D0munu-S28r2-noPID'},
		       'DaVinci_v45r2'    : { 'platform': 'x86_64-centos7-gcc8-opt', 'analysis': 'v21r2'  , 'slbranch': 'sltools-mkenzie-usewDVv45r2',  'strippingbranch': None},
					}

from time import sleep # slow us down a bit but easier to see what's happening

for dv_version, dv_cfg in list(DV_cfg.items()):

    os.chdir(path)

    print('Running Setup for', dv_version, 'in', os.getcwd())
    if not opts.force and os.path.exists( os.path.join(os.getcwd(), dv_version.replace('DaVinci','DaVinciDev') ) ):

    	print('Already found a path for', dv_version, 'so moving on')
    	sleep(1)
    	continue

    # Write the bash file
    f = open('Setup%s'%dv_version,'w')

    f.write('#/bin/bash\n\n')

    #f.write('source /cvmfs/lhcb.cern.ch/group_login.sh\n')
    #f.write('source `which LbLogin.sh` -c %s\n\n'%dv_cfg['platform'])
    f.write('source /cvmfs/lhcb.cern.ch/lib/LbEnv\n')
    f.write('lb_set_platform %s\n\n'%dv_cfg['platform'])

    f.write('lb-dev DaVinci/%s\n'%dv_version.split('_')[1])
    f.write('cd %s\n\n'%dv_version.replace('DaVinci','DaVinciDev'))

    f.write( 'git lb-use DaVinci; git lb-use Analysis\n' )
    #f.write( 'git lb-checkout DaVinci/%s CMakeLists.txt\n'%dv_version.split('_')[1] )
    f.write( 'git lb-checkout Analysis/%s Phys/DecayTreeTuple\n'%dv_cfg['analysis'] )
    f.write( 'git lb-use SemileptonicCommonTools ssh://git@gitlab.cern.ch:7999/lhcb-slb/SemileptonicCommonTools.git\n')
    #https://:@gitlab.cern.ch:8443/lhcb-slb/SemileptonicCommonTools.git\n')
    f.write( 'git lb-checkout SemileptonicCommonTools/%s Phys/SemileptonicCommonTools\n'%dv_cfg['slbranch'] )
    if not dv_cfg['strippingbranch']==None:
        f.write( 'git lb-use Stripping ssh://git@gitlab.cern.ch:7999/atully/Stripping.git\n')
        f.write( 'git lb-checkout Stripping/%s Phys/StrippingSelections\n'%dv_cfg['strippingbranch'] )
    f.write( '\n' )
    f.write( 'make configure\n' )
    f.write( 'make -j8 install\n' )

    f.close()

    if not opts.quiet:
        print('Will now setup and compile', dv_version)
        sleep(1)

        # Print the bash file
        print('####################################################')
        os.system('cat %s'%f.name)
        print('####################################################')

        # Run it
        os.system('chmod +x %s'%f.name)

        os.system('/bin/bash %s'%f.name)

