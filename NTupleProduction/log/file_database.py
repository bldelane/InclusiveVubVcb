# this script keeps hold of a map between ganga jobs and
# a sensible naming scheme to identify jobs

job_map = { 12 : 'MC_Bc2JpsiMuNu_2016_MU',
					  13 : 'MC_Bc2JpsiMuNu_2016_MD',
						20 : 'MC_Bc2D0MuNu_2016_MD',
						21 : 'MC_Bc2D0gMuNu_2016_MD',
						22 : 'MC_Bc2D0pi0MuNu_2016_MD',
						23 : 'MC_Bu2D0MuNu_2016_MD',
						24 : 'MC_Bc2D0MuNu_2016_MU',
						25 : 'MC_Bc2D0gMuNu_2016_MU',
						26 : 'MC_Bc2D0pi0MuNu_2016_MU',
						27 : 'MC_Bu2D0MuNu_2016_MU',
						28 : 'Data_Bc2D0MuNu_2016_MD',
						29 : 'Data_Bc2D0MuNu_2016_MU',
						30 : 'Data_Bc2JpsiMuNu_2016_MD',
						31 : 'Data_Bc2JpsiMuNu_2016_MU'
					}

gangadir = '/home/epp/phskbk/gangadir/workspace/phskbk/LocalXML'
link_path = 'files'

import os

for jobid, outfname in job_map.items():
	inf = 'Bc2D0MuNuX.root'
	if 'Bc2JpsiMuNu' in outfname: inf = 'Bc2JpsiMuNuX.root'
	if 'Data' in outfname: inf.replace('.root','Slim.root')

	path_to_file = os.path.join( gangadir, str(jobid), 'output', inf )
	if not os.path.exists(path_to_file):
		print('No file found for %s.root at %s'%(outfname,path_to_file))
		continue

	out_path = os.path.join(link_path,'%s.root'%outfname)
	if os.path.exists( out_path ):
		print('Removing old link %s'%out_path)
		os.system('rm -f %s'%out_path)

	os.system('ln -s %s %s'%(path_to_file,out_path))

	path_to_eff_file = os.path.join( gangadir, str(jobid), 'output', 'eff.log' )
	if os.path.exists( path_to_eff_file ):
		out_eff_path = os.path.join(link_path,'%s_eff.log'%outfname)

		if os.path.exists( out_eff_path ):
			print('Removing old link %s'%out_eff_path)
			os.system('rm -f %s'%out_eff_path)

		os.system('ln -s %s %s'%(path_to_eff_file,out_eff_path) )
