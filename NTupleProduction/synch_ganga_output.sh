#!/bin/bash

# get grid proxy
lhcb-proxy-init -v 72:00

# synch file_track.json log files
python synch_data_file_logs.py

# download output
echo "################################"
echo "####   DOWNLOADING OUTPUT   ####"
echo "################################"

for mode in D0MuNu JpsiMuNu; 
  do for year in 2011 2012 2015 2016 2017 2018; 
    do for pol in MU MD; 
      do python GangaDownloadOutput.py -l log/Data_Bc2${mode}_${year}_${pol}.json -o /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA/${mode}/${year}/${pol} -p; 
    done; 
  done; 
done 2>&1 | tee download.log

# check output
echo "################################"
echo "####     CHECKING OUTPUT    ####"
echo "################################"

for mode in D0MuNu JpsiMuNu; 
  do for year in 2011 2012 2015 2016 2017 2018; 
    do for pol in MU MD; 
      do lb-run -c x86_64-centos7-gcc8-opt DaVinci/v45r1 python check_synched_files.py -l log/Data_Bc2${mode}_${year}_${pol}.json -o /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA/${mode}/${year}/${pol}; 
    done; 
  done; 
done 2>&1 | tee check.log
