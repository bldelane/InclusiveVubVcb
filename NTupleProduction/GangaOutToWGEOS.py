### THIS MUST RUN AT CERN ####

import json
import sys
import os

verbose = False
sub_dir = 'v1/DATA' if len(sys.argv)==1 else sys.argv[1]
wg_direc = '/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples'
if not verbose: os.system('lhcb-proxy-init')

f = open('log/ganga_lfns.json')
dic = json.load(f)
f.close()

for mode in sorted(dic.keys()):
  for year in sorted(dic[mode].keys()):
    for polarity in sorted(dic[mode][year].keys()):
      lfns = dic[mode][year][polarity]
      if not lfns: continue
      sjkeys = sorted( [ int(x) for x in lfns.keys() ] )
      print mode, year, polarity, '-', len(sjkeys), 'files'
      for sj in sjkeys:
        sjid = str(sj)
        lfn = lfns[sjid]
        target_dir = os.path.join(wg_direc,sub_dir,mode,year,polarity,'dv_tuples',sjid)
        download_loc = os.path.join(target_dir, os.path.basename(lfn))
        final_name = os.path.join(wg_direc,sub_dir,mode,year,polarity,'dv_tuples', str(sjid)+'_'+lfn.split('mkenzie/')[-1].replace('/','_') )
        print '\t {:4} {:30}'.format( sjid, lfn )
        #print sjid, lfn, target_dir
        #print download_loc, final_name
        
        dirac_cmd = 'lb-run LHCbDirac dirac-dms-get-file {0} -D {1}'.format(lfn,target_dir)
        bash_cmd = 'mv {0} {1}'.format(download_loc,final_name)
        clean_cmd = 'rm -rf {0}'.format(target_dir)
        dirac_clean_cmd = 'lb-run LHCbDirac dirac-dms-remove-files {0}'.format(lfn)
        
        if not os.path.exists( final_name ):
          if not os.path.exists( download_loc):
            if verbose: print dirac_cmd
            else: os.system(dirac_cmd)
          if verbose: print bash_cmd
          else: os.system(bash_cmd)
          if verbose: print clean_cmd
          else: os.system(clean_cmd)
          #if verbose: print dirac_clean_cmd
          #os.system(dirac_clean_cmd)


