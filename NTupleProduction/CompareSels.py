import ROOT as r

import sys

f1 = sys.argv[1]
f2 = sys.argv[2]

print( '{:>10} : {:>20} {:>20}'.format('File',f1,f2) )

tf1 = r.TFile(f1)
tf2 = r.TFile(f2)

t1 = tf1.Get('B2DMuNuX_D02KPiTuple/DecayTree')
t2 = tf2.Get('B2DMuNuX_D02KPiTuple/DecayTree')

ent1 = t1.GetEntries()
ent2 = t2.GetEntries()

trig1 = t1.GetEntries("( (Mu_plus_L0MuonDecision_TOS==1) && (B_plus_Hlt1TwoTrackMVADecision_TOS==1 || B_plus_Hlt1TrackMuonMVADecision_TOS==1 || B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TrackMuonDecision_TOS ) && (B_plus_Hlt2Topo2BodyDecision_TOS==1 || B_plus_Hlt2TopoMu3BodyDecision_TOS==1 || B_plus_Hlt2SingleMuonDecision_TOS==1 || B_plus_Hlt2TopoMu2BodyDecision_TOS==1 || B_plus_Hlt2Topo3BodyDecision_TOS==1) ) ")
trig2 = t2.GetEntries("( (Mu_plus_L0MuonDecision_TOS==1) && (B_plus_Hlt1TwoTrackMVADecision_TOS==1 || B_plus_Hlt1TrackMuonMVADecision_TOS==1 || B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TrackMuonDecision_TOS ) && (B_plus_Hlt2Topo2BodyDecision_TOS==1 || B_plus_Hlt2TopoMu3BodyDecision_TOS==1 || B_plus_Hlt2SingleMuonDecision_TOS==1 || B_plus_Hlt2TopoMu2BodyDecision_TOS==1 || B_plus_Hlt2Topo3BodyDecision_TOS==1) ) ")

sel1 = t1.GetEntries("( K_minus_PIDK>0 && Pi_1_PIDK<0 ) && ( Mu_plus_P < 100e3  && K_minus_P < 100e3 && Pi_1_P < 100e3 ) && (Mu_plus_IPCHI2_OWNPV>16 && K_minus_IPCHI2_OWNPV>16 && Pi_1_IPCHI2_OWNPV>16)" )
sel2 = t2.GetEntries("( K_minus_PIDK>0 && Pi_1_PIDK<0 ) && ( Mu_plus_P < 100e3  && K_minus_P < 100e3 && Pi_1_P < 100e3 ) && (Mu_plus_IPCHI2_OWNPV>16 && K_minus_IPCHI2_OWNPV>16 && Pi_1_IPCHI2_OWNPV>16)" )

both1 = t1.GetEntries("( (Mu_plus_L0MuonDecision_TOS==1) && (B_plus_Hlt1TwoTrackMVADecision_TOS==1 || B_plus_Hlt1TrackMuonMVADecision_TOS==1 || B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TrackMuonDecision_TOS ) && (B_plus_Hlt2Topo2BodyDecision_TOS==1 || B_plus_Hlt2TopoMu3BodyDecision_TOS==1 || B_plus_Hlt2SingleMuonDecision_TOS==1 || B_plus_Hlt2TopoMu2BodyDecision_TOS==1 || B_plus_Hlt2Topo3BodyDecision_TOS==1) ) && ( K_minus_PIDK>0 && Pi_1_PIDK<0 ) && ( Mu_plus_P < 100e3  && K_minus_P < 100e3 && Pi_1_P < 100e3 ) && (Mu_plus_IPCHI2_OWNPV>16 && K_minus_IPCHI2_OWNPV>16 && Pi_1_IPCHI2_OWNPV>16)")
both2 = t2.GetEntries("( (Mu_plus_L0MuonDecision_TOS==1) && (B_plus_Hlt1TwoTrackMVADecision_TOS==1 || B_plus_Hlt1TrackMuonMVADecision_TOS==1 || B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TrackMuonDecision_TOS ) && (B_plus_Hlt2Topo2BodyDecision_TOS==1 || B_plus_Hlt2TopoMu3BodyDecision_TOS==1 || B_plus_Hlt2SingleMuonDecision_TOS==1 || B_plus_Hlt2TopoMu2BodyDecision_TOS==1 || B_plus_Hlt2Topo3BodyDecision_TOS==1) ) && ( K_minus_PIDK>0 && Pi_1_PIDK<0 ) && ( Mu_plus_P < 100e3  && K_minus_P < 100e3 && Pi_1_P < 100e3 ) && (Mu_plus_IPCHI2_OWNPV>16 && K_minus_IPCHI2_OWNPV>16 && Pi_1_IPCHI2_OWNPV>16)")

print( '{:>10} : {:>20} {:>20}'.format('Total',ent1,ent2) )
print( '{:>10} : {:>20} {:>20}'.format('Trig',trig1,trig2) )
print( '{:>10} : {:>20} {:>20}'.format('Sel',sel1,sel2) )
print( '{:>10} : {:>20} {:>20}'.format('Both',both1,both2) )

# percentage difference
checkVars = ['B_plus_M', 'D0_M' ]
hists = {}

for var in checkVars:
  hists[var] = r.TH1F(var,'',10,-0.01,0.01)

for ev1 in range(ent1):
  t1.GetEntry(ev1)
  if not t1.Mu_plus_L0MuonDecision_TOS: continue
  if not t1.B_plus_Hlt1TrackMVADecision_TOS and not t1.B_plus_Hlt1TwoTrackMVADecision_TOS and not t1.B_plus_Hlt1TrackMuonDecision_TOS and not t1.B_plus_Hlt1TrackMuonMVADecision_TOS: continue
  if not t1.B_plus_Hlt2Topo2BodyDecision_TOS and not t1.B_plus_Hlt2Topo3BodyDecision_TOS and not t1.B_plus_Hlt2TopoMu2BodyDecision_TOS and not t1.B_plus_Hlt2TopoMu3BodyDecision_TOS and not t1.B_plus_Hlt2SingleMuonDecision_TOS: continue
  if t1.Mu_plus_P > 100e3 or t1.K_minus_P > 100e3 or t1.Pi_1_P > 100e3: continue
  if t1.Mu_plus_IPCHI2_OWNPV < 16 or t1.K_minus_IPCHI2_OWNPV < 16 or t1.Pi_1_IPCHI2_OWNPV < 16: continue
  if t1.K_minus_PIDK < 0 or t1.Pi_1_PIDK > 0: continue
  for ev2 in range(ent2):
    t2.GetEntry(ev2)
    if t1.eventNumber == t2.eventNumber:
      for var in checkVars:
        if ev2%100==0: print '{:10} : {:10} {:20} {:20} {:10}'.format(t1.eventNumber, var, getattr(t1,var), getattr(t2,var), getattr(t1,var) - getattr(t2,var))
        hists[var].Fill( getattr(t1,var) - getattr(t2,var) )


for var in checkVars:
  print( var, hists[var].GetMean() )
tf1.Close()
tf2.Close()
