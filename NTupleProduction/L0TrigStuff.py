# trawl files to get the L0 TCKs in data for each year
import glob
import uproot
import json
from tqdm import tqdm

basepath = '/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA'

modes = ['D0MuNu','JpsiMuNu']
years = ['2011','2012','2015','2016','2017','2018']
pols = ['MD','MU']

tck_dict = {}

for mode in modes:
  tck_dict[mode] = {}
  for year in years:
    tck_dict[mode][year] = {}
    files = []
    for pol in pols:
      files += glob.glob( f'{basepath}/{mode}/{year}/{pol}/*.root' )

    tck_set = set()
    for fil in tqdm(files, desc=f'{mode} {year}'):
      folder = 'B2DMuNuX_D02KPiTuple' if mode=='D0MuNu' else 'B2JpsiMuNuXTuple' 
      tree = uproot.open(fil+f':{folder}/DecayTree')
      tcks = tree.arrays(['L0DUTCK','HLT1TCK','HLT2TCK'], library='np' )
      l0_tcks = set(tcks['L0DUTCK'])
      l0_tcks = [ hex(tck) for tck in l0_tcks ]
      
      tck_set = tck_set.union(l0_tcks)

    tck_dict[mode][year] = list(tck_set)

with open('l0_tcks.json','w') as f:
  json.dump(tck_dict,f, indent=2)

print(tck_dict)
