#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
# Ganga submission file for Ganga submission for DATA for channels:
#  - Bc -> D0(->K pi)  Mu Nu X
#  - Bc -> D0(->K 3pi) Mu Nu X
#  - Bc -> Jpsi(-> mu mu) Mu Nu X
#
# Note on stripping lines:
#   - B2DMuNuX_D0(_FakeMuon)Line is used for both Run I and Run II for D0->KPi
#   - B2DMuNuX_D0_K3Pi(_FakeMuon)Line is used for Run I and Run II for D0->K3Pi
#   - FullDSTDiMuonJpsi2MuMuDetachedLine is used for Run 1 and Run 11 Jpsi modes
#
# DaVinci Versions:
#
#    Runs I,II Stream: Semileptonic (DST) and Dimuon (DST)
#
#     Stream: Semileptonic (DST) for Bc -> D0MuNu
#      -stripping 21r1p2 (Stripping for 2011 incremental restripping): DV v39r1p6
#      -stripping 21r0p2 (Stripping for 2012 incremental restripping): DV v39r1p6
#      -stripping 24r2   (Full restripping of 2015 data)             : DV v44r10p5
#      -stripping 28r2   (Stripping for 2016 full restripping)       : DV v44r10p5
#      -stripping 29r2   (Full restripping of 2017 pp data)          : DV v42r7p2
#      -stripping 34     (Concurrent stripping for 2018 pp data)     : DV v44r4
#
#     Stream: Dimuon (DST) for Bc -> JpsiMuNu
#      -stripping 21r1   (Legacy stripping of 2011 data)             : DV v36r1p1
#      -stripping 21     (Legacy stripping of 2012 data)             : DV v36r1p1
#      -stripping 24r2   (Full restripping of 2015 data)             : DV v44r10p5
#      -stripping 28r2   (Stripping for 2016 full restripping)       : DV v44r10p5
#      -stripping 29r2   (Full restripping of 2017 pp data)          : DV v42r7p2
#      -stripping 34     (Concurrent stripping for 2018 pp data)     : DV v44r4
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst
#=======================================================================================================================

#=======================================================================================================================
# TODO Look into SLTRUTH Implementation
# TODO possible error: k3pi-specific tools if loop condition
# TODO AutoDB() Tags in Ganga
# TODO Double-check stripping lines
# TODO test and print IgnoreFilter=True
# TODO check function to implement restrip
# TODO check if mDST: need to use ROOTINTES
# TODO look into restripping extras for s21
# TODO implement doctrings for member functions
# TODO add mother ID tools
# TODO implement RunI-specific configurablesi
# TODO implement MC: {EventNodeKiller as a member function, TES with 'MyStream', ROOTINTES IN  DV}
# TODO finish documenting the class with doctring. Add all members.
# TODO Implement EVent node killer in apply_config function
# TODO check that default DTT tools implemeny booked opys, as technically not part of list
# TODO DTF CTAU doesnt seem to do much; test with a bunch of dst and if substantial improvement,
# remove  for CPU time
#=======================================================================================================================

#=======================================================================================================================
#                                        Class Prototype for Bc2D0MuNuX
#=======================================================================================================================


# import tools and LoKis
from Configurables import DaVinci
from Configurables import DecayTreeTuple, MCDecayTreeTuple, TupleToolApplyIsolation, MCMatchObjP2MCRelator
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

# imports for restripping, scale, smear, prefilters
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
from StrippingSelections import buildersConf
from Configurables import EventNodeKiller
from Configurables import TrackSmearState as SMEAR
from Configurables import TrackScaleState as SCALER
from Configurables import LoKi__HDRFilter
from Configurables import LoKi__VoidFilter
from Configurables import TupleToolSLTruth
from Configurables import TupleToolSLTools
from Configurables import TupleToolTauMuDiscrVars
from Configurables import MCTupleToolHierarchy
from Configurables import MCTupleToolKinematic

# imports need to apply our slimmed selection
from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import DataOnDemand
from Configurables import FilterDesktop
from Configurables import TisTosParticleTagger
from Configurables import CombineParticles
#from PhysConf.Selections import PrintSelection

class DaVinciConfig():
    '''
    Class to configure the NTuples per model

    *Key idea*: label the tuple-specific tools by the corresponding key in the various dicts

    Structure:
        - Top Level Gaudisequence: member of DV UserAlgorithms; per-event run the DTTs; needs IgnoreFilterPass;
        - Decay-specific Gaudisequences: ensemble of tools, each member of the Top Level Sequence.
    '''

    def __init__(self, name, inputFormat, sim, year, maxEvts, slim_tuples=False, verbose=False, lfn=None):
        self.name = name
        self.inputFormat = inputFormat
        self.sim = sim
        self.year = year
        self.maxEvts = maxEvts
        self.lfn = lfn
        self.slim = slim_tuples
        self.verbose = verbose
        if self.slim: self.InitializeSlim()

    __properties__ = {
        """
        Dictionaries of configurables for DaVinci and DecayTreeTuple, DecayTreeFitter and PrintDecayTree

        Arguments:
            - DataType: run year; default None
            - Simulation: by default set to False
            - InputType: by default set to 'DST'
            - DecayModes: selection sequences, decay-specific; each will be assigned to a GaudiSequence
            - Streams: labelled by keys of DecayModes
            - StrippingLines: labelled by keys of DecayModes
            - DecayDescriptors : labelled by keys of DecayModes
            - TupleBranches : labelled by keys of DecayModes
            - TTools: TupleTools; included:{default DV tools, common tools, SL tools, MC tools}
            - LoKi_Variables: functors applied at particle level
            - TrigLInes : L0, HLT1, HLT2 triggerr lines
        """

        # DV Config

        #--------------------------------------------------------------------------------
        # should replace this with the __init__ members -> done, should be safe to delete
        'DataType'     : None,
        'Simulation'   : None,
        'InputType'    : None,
        'nEvents'      : None,
        #--------------------------------------------------------------------------------

        # instantiate the keys for the decay modes and corresponding configurables
        # use self.name to indicate which modes to run on
        'DecayModes'       : {
                                'Bc2D0MuNuX_D02KPi'            : ['B2DMuNuX_D02KPi'],
                                'Bc2D0MuNuX_D02KPi_FakeMuon'   : ['B2DMuNuX_D02KPi_FakeMuon'],
                                'Bc2D0MuNuX_D02K3Pi'           : ['B2DMuNuX_D02K3Pi'],
                                'Bc2D0MuNuX_D02K3Pi_FakeMuon'  : ['B2DMuNuX_D02K3Pi_FakeMuon'],
                                'Bc2D0MuNuX_allModes'          : ['B2DMuNuX_D02KPi', 'B2DMuNuX_D02K3Pi',
                                                                  'B2DMuNuX_D02KPi_FakeMuon','B2DMuNuX_D02K3Pi_FakeMuon'],
                                'Bc2JpsiMuNuX'                 : ['B2JpsiMuNuX']
                             },

        'Streams'               : {
                                    'B2DMuNuX_D02KPi'             : 'Semileptonic',
                                    'B2DMuNuX_D02K3Pi'            : 'Semileptonic',
                                    'B2DMuNuX_D02KPi_FakeMuon'    : 'Semileptonic',
                                    'B2DMuNuX_D02K3Pi_FakeMuon'   : 'Semileptonic',
                                    'B2JpsiMuNuX'                 : 'Dimuon',
                                  },

        'StrippingLines'        : { 'B2DMuNuX_D02KPi'              : 'B2DMuNuX_D0',
                                    'B2DMuNuX_D02KPi_FakeMuon'     : 'B2DMuNuX_D0_FakeMuon',
                                    'B2DMuNuX_D02K3Pi'             : 'B2DMuNuX_D0_K3Pi',
                                    'B2DMuNuX_D02K3Pi_FakeMuon'    : 'B2DMuNuX_D0_K3Pi_FakeMuon',
                                    'B2JpsiMuNuX'                  : 'FullDSTDiMuonJpsi2MuMuDetachedLine'
                                  },
#
		'StrippingVersions'     : { 'Bc2D0MuNuX_D02KPi'             : { '2011': 'stripping21r1p2', '2012': 'stripping21r0p2', '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' } ,
                                    'Bc2D0MuNuX_D02KPi_FakeMuon'    : { '2011': 'stripping21r1p2', '2012': 'stripping21r0p2', '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' } ,
                                    'Bc2D0MuNuX_D02K3Pi'            : { '2011': 'stripping21r1p2', '2012': 'stripping21r0p2', '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' } ,
                                    'Bc2D0MuNuX_D02K3Pi_FakeMuon'   : { '2011': 'stripping21r1p2', '2012': 'stripping21r0p2', '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' } ,
                                    'Bc2D0MuNuX_allModes'          : { '2011': 'stripping21r1p2', '2012': 'stripping21r0p2', '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' } ,
                                    'Bc2JpsiMuNuX'                 : { '2011': 'stripping21r1'  , '2012': 'stripping21'    , '2015': 'stripping24r2', '2016': 'stripping28r2', '2017': 'stripping29r2', '2018': 'stripping34' }
                                  },
        'L0TCKs' : {'Bc2D0MuNuX_D02KPi': { '2011': ['0x35', '0x38', '0x34', '0x33', '0x36', '0x37', '0x32'],
                                           '2012': ['0x44', '0x3d', '0x46', '0x43', '0x42', '0x3a', '0x45', '0x40'],
                                           '2015': ['0xa8', '0xa2', '0xa6', '0xa1', '0xa7', '0xa3'],
                                           '2016': ['0x160f', '0x1612', '0x1603', '0x160e', '0x1605', '0x1609', '0x1604', '0x1611'],
                                           '2017': ['0x1705', '0x1704', '0x1702', '0x1709', '0x1706', '0x1703', '0x1707', '0x17a7', '0x1708'],
                                           '2018': ['0x18a1', '0x18a2', '0x18a4', '0x1801']
                                         },
                     'Bc2JpsiMuNuX'    : { '2011': ['0x38', '0x35', '0x34', '0x33', '0x36', '0x37', '0x32'],
                                           '2012': ['0x44', '0x3d', '0x46', '0x43', '0x42', '0x3a', '0x45', '0x40'],
                                           '2015': ['0xa8', '0xa2', '0xa6', '0xa1', '0xa7', '0xa3'],
                                           '2016': ['0x160f', '0x1612', '0x1603', '0x160e', '0x1605', '0x1609', '0x1604', '0x1611'],
                                           '2017': ['0x1705', '0x1704', '0x1702', '0x1709', '0x1706', '0x1708', '0x1707', '0x17a7', '0x1703'],
                                           '2018': ['0x18a1', '0x18a2', '0x18a4', '0x1801']
                                         }
                    },

        ### With this DecayDescriptor we still get all combinations of three daughters:
        ###     1) B- -> (K- pi+) mu-  (tuple will always call these D0s  with mu- [CF  B-, DCS Bc-] ) K/Mu have SS
        ###     2) B- -> (K+ pi-) mu-  (tuple will always call these D~0s with mu- [DCS B-, CF  Bc-] ) K/Mu have OS
        ###     3) B+ -> (K- pi+) mu+  (tuple will always call these D~0s with mu+ [DCS B+, CF  Bc+] ) K/Mu have OS
        ###     4) B+ -> (K+ pi-) mu+  (tuple will always call these D0s  will mu+ [CF  B+, DCS Bc+] ) K/Mu have SS
        ### Later we never care what the tuple calls D0 or D0bar (i.e. ABSID==421) we differentiate on the
        ###   requirement of:
        ###     i)   OS K/Mu - our signal search for the Bc
        ###     ii)  SS K/Mu - the dominate source from B+
        'DecayDescriptors'       : {
                                    'B2DMuNuX_D02KPi'           : '[B- -> ^[(D0 -> ^K- ^pi+)]CC ^mu-]CC',
                                    'B2DMuNuX_D02KPi_FakeMuon'  : '[B- -> ^[(D0 -> ^K- ^pi+)]CC ^mu-]CC',
                                    'B2DMuNuX_D02K3Pi'          : '[B- -> ^[(D0 -> ^K- ^pi+ ^pi- ^pi+)]CC ^mu-]CC',
                                    'B2DMuNuX_D02K3Pi_FakeMuon' : '[B- -> ^[(D0 -> ^K- ^pi+ ^pi- ^pi+)]CC ^mu-]CC',
                                    'B2JpsiMuNuX'               : '[B_c- -> ^(J/psi(1S) -> ^mu+ ^mu-) ^mu-]CC',
                                    },

        # convention: label branches following the signal mode/DCS B+ decay
        'TupleBranches'         : {
                                    'B2DMuNuX_D02KPi'          : {'B_plus'    :  '[B- -> [(D0 -> K- pi+)]CC mu-]CC',
                                                                  'D0'        :  '[B- -> ^[(D0 -> K- pi+)]CC mu-]CC',
                                                                  'K_minus'   :  '[B- -> [(D0 -> ^K- pi+)]CC mu-]CC',
                                                                  'Pi_1'      :  '[B- -> [(D0 -> K- ^pi+)]CC mu-]CC',
                                                                  'Mu_plus'   :  '[B- -> [(D0 -> K- pi+)]CC ^mu-]CC'},

                                    'B2DMuNuX_D02KPi_FakeMuon' : {'B_plus'    :  '[B- -> [(D0 -> K- pi+)]CC mu-]CC',
                                                                  'D0'        :  '[B- -> ^[(D0 -> K- pi+)]CC mu-]CC',
                                                                  'K_minus'   :  '[B- -> [(D0 -> ^K- pi+)]CC mu-]CC',
                                                                  'Pi_1'      :  '[B- -> [(D0 -> K- ^pi+)]CC mu-]CC',
                                                                  'Mu_plus'   :  '[B- -> [(D0 -> K- pi+)]CC ^mu-]CC'},

                                    'B2DMuNuX_D02K3Pi'         : {'B_plus'    :  '[B- -> [(D0 -> K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'D0'        :  '[B- -> ^[(D0 -> K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'K_minus'   :  '[B- -> [(D0 -> ^K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'Pi_1'      :  '[B- -> [(D0 -> K- ^pi+ pi- pi+)]CC mu-]CC',
                                                                  'Pi_2'      :  '[B- -> [(D0 -> K- pi+ ^pi- pi+)]CC mu-]CC',
                                                                  'Pi_3'      :  '[B- -> [(D0 -> K- pi+ pi- ^pi+)]CC mu-]CC',
                                                                  'Mu_plus'   :  '[B- -> [(D0 -> K- pi+ pi- pi+)]CC ^mu-]CC'},

                                    'B2DMuNuX_D02K3Pi_FakeMuon': {'B_plus'    :  '[B- -> [(D0 -> K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'D0'        :  '[B- -> ^[(D0 -> K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'K_minus'   :  '[B- -> [(D0 -> ^K- pi+ pi- pi+)]CC mu-]CC',
                                                                  'Pi_1'      :  '[B- -> [(D0 -> K- ^pi+ pi- pi+)]CC mu-]CC',
                                                                  'Pi_2'      :  '[B- -> [(D0 -> K- pi+ ^pi- pi+)]CC mu-]CC',
                                                                  'Pi_3'      :  '[B- -> [(D0 -> K- pi+ pi- ^pi+)]CC mu-]CC',
                                                                  'Mu_plus'   :  '[B- -> [(D0 -> K- pi+ pi- pi+)]CC ^mu-]CC'},

                                    'B2JpsiMuNuX'              : {'B_plus'    :  '[B_c- -> (J/psi(1S) -> mu+ mu-) mu-]CC',
                                                                  'Jpsi'      :  '[B_c- -> ^(J/psi(1S) -> mu+ mu-) mu-]CC',
                                                                  'Mu_1'      :  '[B_c- -> (J/psi(1S) -> ^mu+ mu-) mu-]CC',
                                                                  'Mu_2'      :  '[B_c- -> (J/psi(1S) -> mu+ ^mu-) mu-]CC',
                                                                  'Mu_plus'   :  '[B_c- -> (J/psi(1S) -> mu+ mu-) ^mu-]CC'}
                                    },

        'TrigLines': {
            'trigListL0': ["L0HadronDecision",
                           "L0MuonDecision",
                           "L0DiMuonDecision"],

            'trigListHlt1': ["Hlt1TrackAllL0Decision",
                             "Hlt1TrackMVADecision",
                             "Hlt1TrackMuonDecision",
                             "Hlt1TrackMuonMVADecision",
                             "Hlt1TwoTrackMVADecision",
                             "Hlt1DiMuonLowMassDecision",
                             "Hlt1DiMuonHighMassDecision",
                             "Hlt1SingleMuonHighPTDecision",
                             "Hlt1SingleMuonLowPTDecision",
                             "Hlt1SingleMuonDecision"
                             ],

            'trigListHlt2': [
                            "Hlt2Topo2BodyDecision",
                            "Hlt2Topo3BodyDecision",
                            "Hlt2Topo4BodyDecision",
                            "Hlt2Topo2BodyBBDTDecision",
                            "Hlt2Topo3BodyBBDTDecision",
                            "Hlt2Topo4BodyBBDTDecision",
                            "Hlt2TopoMu2BodyDecision",
                            "Hlt2TopoMu3BodyDecision",
                            "Hlt2TopoMu4BodyDecision",
                            "Hlt2TopoMu2BodyBBDTDecision",
                            "Hlt2TopoMu3BodyBBDTDecision",
                            "Hlt2TopoMu4BodyBBDTDecision",
                            "Hlt2SingleMuonDecision",
                            "Hlt2SingleMuonHighPTDecision",
                            "Hlt2SingleMuonVHighPTDecision",
                            "Hlt2SingleMuonLowPTDecision",
                            "Hlt2DiMuonDetachedJPsiDecision"
                               ],
                    },

        'TTools'       :  {
                            'GlobalTools' :  ['TupleToolKinematic', 'TupleToolEventInfo', 'TupleToolGeometry', 'TupleToolRecoStats',
                                              'TupleToolTrigger','TupleToolTISTOS','TupleToolPrimaries','TupleToolPropertime'],
                            'TrackTools'  :  ['TupleToolTrackInfo', 'TupleToolTrackIsolation','TupleToolVtxIsoln'], #,'TupleToolIsoGeneric'],
                            'PidTools'    :  ['TupleToolPid','TupleToolANNPID','TupleToolMuonPid'],
                            'SLTools'     :  ['TupleToolSLTools', 'TupleToolDocas', 'TupleToolApplyIsolation', 'TupleToolTauMuDiscrVars', 'TupleToolSLTruth'],
                            'MCTools'     :  ['TupleToolMCTruth', 'TupleToolMCBackgroundInfo', 'TupleToolL0Data']#, 'MCTupleToolHierarchy']
                        },
        'SlimTTools'   :  {
                            'GlobalTools' :  ['TupleToolKinematic', 'TupleToolEventInfo', 'TupleToolGeometry', 'TupleToolRecoStats',
                                              'TupleToolTrigger','TupleToolTISTOS','TupleToolPropertime'],
                            'TrackTools'  :  [],
                            'PidTools'    :  ['TupleToolPid','TupleToolANNPID','TupleToolMuonPid'],
                            'SLTools'     :  ['TupleToolSLTools', 'TupleToolDocas', 'TupleToolApplyIsolation', 'TupleToolTauMuDiscrVars', 'TupleToolSLTruth'],
                            'MCTools'     :  ['TupleToolMCTruth', 'TupleToolMCBackgroundInfo', 'TupleToolL0Data']#, 'MCTupleToolHierarchy']
                        },

        'LoKi_Variables' :   {
                             'B_LoKis' : {
                                             "LK_BPVFDCHI2": "BPVVDCHI2",  # CHI2 distance from best PV
                                             "LK_VCHI2": "VFASPF(VCHI2)",
                                             "LK_VCHI2DOF": "VFASPF(VCHI2/VDOF)",
                                             "LK_BPVIPCHI2": "BPVIPCHI2()",  # IP CHI2 relativeto BPV
                                             "LK_BPVDIRA": "BPVDIRA",
                                             "LK_ETA": "ETA",  # pseudorapidity
                                             "LK_LTIME": "BPVLTIME()"#,  # proper life time (mm)
                                             #"DTF_CTAU": "DTF_CTAU ( 0 , False , strings('D0') )",
                                             #"DTF_CTAUSIGNIFICANCE": "DTF_CTAUSIGNIFICANCE ( 0 , False, strings('D0') )"
                                         },

                             'D0_LoKis' : {
                                             "LK_BPVFDCHI2": "BPVVDCHI2",  # CHI2 distance from best PV
                                             "LK_VCHI2": "VFASPF(VCHI2)",
                                             "LK_VCHI2DOF": "VFASPF(VCHI2/VDOF)",
                                             "LK_BPVIPCHI2": "BPVIPCHI2()",  # IP CHI2 relativeto BPV
                                             "LK_BPVDIRA": "BPVDIRA",
                                             "LK_ETA": "ETA",  # pseudorapidity
                                             "LK_LTIME": "BPVLTIME()"#,  # proper life time (mm)
                                             #"DTF_CTAU": "DTF_CTAU ( 1 , False, strings('D0') )",
                                             #"DTF_CTAUSIGNIFICANCE": "DTF_CTAUSIGNIFICANCE ( 1 , False, strings('D0') )"
                                          },

                             'Mu_LoKis'  : {
                                             "LK_TRCHI2DOF": "TRCHI2DOF",  # return tracks with low chi2 PDOF
                                             "LK_TRGHOSTPROB": "TRGHOSTPROB",  # track ghost prob
                                             "LK_ETA": "ETA",
                                             "LK_MIPCHI2PV": "MIPCHI2DV(PRIMARY)"  # min chi2 distance relative to PV (3 sigma, I think)
                                           },

                            'Daughter_LoKis'  : {
                                             "LK_TRCHI2DOF": "TRCHI2DOF",  # return tracks with low chi2 PDOF
                                             "LK_TRGHOSTPROB": "TRGHOSTPROB",  # track ghost prob
                                             "LK_ETA": "ETA",
                                             "LK_MIPCHI2PV": "MIPCHI2DV(PRIMARY)"  # min chi2 distance relative to PV (3 sigma, I think)
                                           },
                            },

    }# end of properties

    def accessProperty(self, key):
        """
        Helper function to access the keys in the class
        :param: decay-specific key
        :return: values in properties dict, for a given key
        """
        return self.__properties__[key]

    def setProperty(self, key, value):
        """
        Helper function to set the value of arg keys in the class
        :param: key: decay-specific key
        :param: value: decay-specific value
        :return: set value in properties dict, for a given key
        """
        self.__properties__[key] = value

    def InitializeSlim(self):
        """
        Reconfigure properties for slim_line tuples
        """
        if self.slim:
            self.__properties__['TTools'] = self.__properties__['SlimTTools']

    def ApplySelection(self, mode, tesLoc):
        """
        Make Selection Objects and store them to be added to DV.UserAlgorithms() later
        """

        # jspi selection
        if mode == 'B2JpsiMuNuX':

            # we have to build the decay
            Muons = DataOnDemand('Phys/StdAllNoPIDsMuons/Particles')
            #Muons = DataOnDemand('Phys/StdAllLooseMuons/Particles')
            MuonFilter = FilterDesktop( mode+'_MuFilter' )
            MuonFilter.Code = """ (PT>750*MeV) & (P>3*GeV) & (P<100*GeV) & (BPVIPCHI2()>4.8) & (ETA>2) & (ETA<5) """
            MuonSel = Selection(mode+'_MuSel', Algorithm = MuonFilter, RequiredSelections = [ Muons ] )

            Jpsis = DataOnDemand(Location=tesLoc)
            JpsiFilter = FilterDesktop( mode+'_JpsiFilter' )
            JpsiFilter.Code = """ (PT>2*GeV) & ( ADMASS('J/psi(1S)')<95*MeV ) """
            JpsiSel = Selection(mode+'_JpsiSel', Algorithm = JpsiFilter, RequiredSelections = [ Jpsis ] )

            B_plus_CombCut = """ ( AM > 3*GeV ) & ( AM < 8*GeV  ) & ( ACUTDOCACHI2(30,'') ) & ( ACUTDOCA(0.15*mm,'') ) """
            B_plus_MothCut = """ ( VFASPF(VCHI2) < 25 ) & ( MM > 3*GeV ) & ( MM < 8*GeV ) """

            B_plus = CombineParticles( mode+'_B_plus_Cands',
                                       DecayDescriptors = ["[B_c- -> J/psi(1S) mu- ]cc"],
                                       CombinationCut   = B_plus_CombCut,
                                       MotherCut        = B_plus_MothCut )
            B_plus_Sel = Selection( mode+'_B_plus_Sel', Algorithm = B_plus, RequiredSelections = [ JpsiSel, MuonSel ] )

            ## For Jpsi MC we run our normal selection (J/psi stripping + mu) but no trigger selection
            if self.sim:
                self.SelSeq = SelectionSequence( mode+'_SubSelSeq', TopSelection = B_plus_Sel )
                self.NTuple.Inputs = [ self.SelSeq.outputLocation() ]

            ## For Jpsi Data we have to slim down the tuple with a trigger selection
            else:
                trigFilter = TisTosParticleTagger( mode+'_TrigFilter' )
                trigFilter.TisTosSpecs = { "Hlt1.*Track.*Decision%TOS":0 ,
                                       "Hlt2.*Topo.*Decision%TOS":0, "Hlt2.*Mu.*Decision%TOS":0 }
                trigFilter.ProjectTracksToCalo = False
                trigFilter.CaloClustForNeutral = False
                trigFilter.CaloClustForCharged = False
                trigFilter.TOSFrac = {4:0.0, 5:0.0}
                trigSel = Selection( mode+'_TrigSel', Algorithm = trigFilter, RequiredSelections = [ B_plus_Sel ] )

                # add the sub selection cuts
                #slimSel = FilterDesktop( mode+'_SlimSelFilter')
                #slimSel.Code = """
                               #INTREE( (ABSID=='mu+') & (P<100*GeV) & (PT>750*MeV) & (P>3*GeV) )
                               #"""
                #     keep PID offline
                #     now create sub selection
                #subSel = Selection( mode+'_SubSel', Algorithm = slimSel, RequiredSelections = [trigSel] )
                #if self.verbose: subSel = PrintSelection( subSel )
                #self.SelSeq = SelectionSequence( mode+'_SubSelSeq', TopSelection = trigSel )

                self.SelSeq = SelectionSequence( mode+'_SubSelSeq', TopSelection = trigSel )
                self.NTuple.Inputs = [ self.SelSeq.outputLocation() ]

		# d0 selection
        else:

            ## FOR D0 MC we run no selection but just output the Strippingline ##
            if self.sim:
                self.SelSeq = None # this helps out later on
                self.NTuple.Inputs = [ tesLoc ]

            ## For D0 Data we have to slim down the tuples with a trigger selection and loose PID selection
            else:
                trigFilter = TisTosParticleTagger( mode+'_TrigFilter' )
                trigFilter.TisTosSpecs = { "Hlt1.*Track.*Decision%TOS":0 ,
                                       "Hlt2.*Topo.*Decision%TOS":0, "Hlt2.*Mu.*Decision%TOS":0 }
                trigFilter.ProjectTracksToCalo = False
                trigFilter.CaloClustForNeutral = False
                trigFilter.CaloClustForCharged = False
                trigFilter.TOSFrac = {4:0.0, 5:0.0}
                trigSel = Selection( mode+'_TrigSel', Algorithm = trigFilter, RequiredSelections = [ DataOnDemand(Location=tesLoc) ] )

                # add the sub selection cuts
                slimSel = FilterDesktop( mode+'_SlimSelFilter')
                slimSel.Code = """
                               INTREE( (ABSID=='K+')  & (P<100*GeV) ) &
                               INTREE( (ABSID=='pi+') & (P<100*GeV) ) &
                               INTREE( (ABSID=='mu+') & (P<100*GeV) & (PT>1500*MeV) & (P>10*GeV) & (BPVIPCHI2()>4.8) & (ETA>2) & (ETA<5) ) &
															 INTREE( (ABSID=='D0')  & (PT>1000*MeV) & (ADMASS('D0')<60*MeV) & (BPVIPCHI2()>2) ) &
															 INTREE( (ABSID=='B+')  & (MM>3*GeV) )
															 & (DOCA(1,2)<0.05*mm)
                               """
															 #INTREE( (ABSID=='B+')  & (DOCACUT(0.05*mm,'')) & (MM>3*GeV) )
                #     now create sub selection
                subSel = Selection( mode+'_SubSel', Algorithm = slimSel, RequiredSelections = [trigSel] )
                self.SelSeq = SelectionSequence( mode+'_SubSelSeq', TopSelection = subSel )

                self.NTuple.Inputs = [ self.SelSeq.outputLocation() ]

    def InitializeDTT(self, mode):
        """
        Initialize decay tree tuple to access members and add tools
        :param mode: str key, decay-specific
        :return: dtt constructor with descriptor and member branches
        """
        self.NTuple = DecayTreeTuple(mode + 'Tuple')

        # set inputs and rootintes
        self.NTuple.Decay = self.accessProperty('DecayDescriptors')[mode]
        self.NTuple.addBranches(self.accessProperty('TupleBranches')[mode])
        if self.accessProperty('StrippingLines')[mode] is not None:
            tesLoc = None
            rootInTES = None
            stream = self.accessProperty('Streams')[mode]
            if self.sim and mode != 'B2JpsiMuNuX': stream = 'Bc2D0MuNu.Strip'
            if self.inputFormat == 'DST':
                tesLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream,
                                                                  self.accessProperty('StrippingLines')[mode])
            if self.inputFormat == 'MDST':
                tesLoc    = 'Phys/{0}/Particles'.format(self.accessProperty('StrippingLines')[mode])
                rootInTES = '/Event/{0}'.format(self.accessProperty('Streams')[mode])
            # MC for jpsi implements restripping so need different TES
            if self.sim is True and mode == 'B2JpsiMuNuX':
                tesLoc     = 'Phys/{0}/Particles'.format(self.accessProperty('StrippingLines')[mode])
                rootInTES  = '/Event/{0}'.format(self.accessProperty('Streams')[mode])

            #if self.slim:
            if self.sim and mode != 'B2JpsiMuNuX':
                self.NTuple.Inputs = [ tesLoc ]
            else:
								self.ApplySelection(mode, tesLoc)

            if rootInTES: DaVinci().RootInTES = rootInTES
        else:
            raise ValueError('Incorrect InputType: select an appropriate InputType: (M)DST')
        return self.NTuple

    def makeNTuple(self, mode):
        """
        Configure DecayTreeTuple
        :param mode: string, decay-specific key
        :return: booked and configure DTT
        """
        NTuple = self.InitializeDTT(mode)

        # needed to configure default tools - patch
        NTuple.ToolList = []

        for tool in self.accessProperty('TTools')['GlobalTools'] + self.accessProperty('TTools')['TrackTools'] + self.accessProperty('TTools')['PidTools']:
            if tool =='TupleToolGeometry':
                bookedtool = NTuple.addTupleTool(tool)
                bookedtool.FillMultiPV = True
            elif tool == 'TupleToolTrackInfo':
                bookedtool = NTuple.addTupleTool(tool)
                bookedtool.Verbose = True
            elif tool == 'TupleToolTrackIsolation':
                bookedtool = NTuple.addTupleTool(tool)
                bookedtool.Verbose = True
            elif tool == 'TupleToolTrigger':
                bookedtool = NTuple.addTupleTool(tool)
                bookedtool.VerboseL0 = True
                bookedtool.VerboseHlt1 = True
                bookedtool.VerboseHlt2 = True
            else:
                NTuple.addTupleTool(tool)

        if self.sim is True:
            for MCtool in self.accessProperty('TTools')['MCTools']:
                if MCtool == 'TupleToolMCTruth': # and mode =='B2JpsiMuNuX'
                    default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
                    rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
                    mctruth = NTuple.addTupleTool(MCtool)
                    mctruth.addTool(MCMatchObjP2MCRelator)
                    mctruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
                    mctruth.ToolList = [ 'MCTupleToolHierarchy', 'MCTupleToolKinematic' ]
                elif MCtool == 'TupleToolL0Data':
                    #for branch in NTuple.Branches.keys():
                        #particlebank = getattr(NTuple, branch)
                    l0data = NTuple.addTupleTool(MCtool)
                    l0data.TCKList = self.accessProperty('L0TCKs')[self.name][self.year]
                else:
                    NTuple.addTupleTool(MCtool)

        for b in NTuple.Branches.keys():
            # query the trigger lists and merge them into one list
            trigListAll = []
            for triggerset in self.accessProperty('TrigLines').itervalues():
                trigListAll += triggerset

            # query the member branch, add and configure particle members and tools
            particlebank = getattr(NTuple, b)

            # Triggers
            booked_bTool = particlebank.addTupleTool('TupleToolTISTOS')
            if not self.slim:
                booked_bTool.VerboseL0   = True
                booked_bTool.VerboseHlt1 = True
                booked_bTool.VerboseHlt2 = True
                booked_bTool.TriggerList = trigListAll
            else:
              if b == 'B_plus':
                booked_bTool.VerboseL0   = True
                booked_bTool.VerboseHlt1 = True
                booked_bTool.VerboseHlt2 = True
                booked_bTool.TriggerList = self.accessProperty('TrigLines')['trigListL0'] + self.accessProperty('TrigLines')['trigListHlt1'] + self.accessProperty('TrigLines')['trigListHlt2']
              if b == 'Jpsi' or b == 'D0':
                booked_bTool.VerboseL0 = True
                booked_bTool.VerboseHlt1 = True
                booked_bTool.VerboseHlt2 = True
                booked_bTool.TriggerList = self.accessProperty('TrigLines')['trigListL0'] + self.accessProperty('TrigLines')['trigListHlt1'] + self.accessProperty('TrigLines')['trigListHlt2']
              if b == 'Mu_plus':
                booked_bTool.VerboseL0   = True
                booked_bTool.TriggerList = self.accessProperty('TrigLines')['trigListL0'] + self.accessProperty('TrigLines')['trigListHlt1']

            # LoKis
            b_hybrid = particlebank.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_'+b)
            b_preamble = []
            b_hybrid.Preambulo = b_preamble
            if b == 'B_plus':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['B_LoKis']
            if b == 'D0' or b == 'Jpsi':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['D0_LoKis']
            if b == 'Mu_plus':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['Mu_LoKis']
            if b != 'B_plus' and b != 'D0' and b != 'Jpsi' and b != 'Mu_plus':  # this is bloody horrible
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['Daughter_LoKis']

            # # mother B-specific tools
            if b == 'B_plus':

                #SLtools
                for SLtool in self.accessProperty('TTools')['SLTools']:
                    if SLtool == 'TupleToolSLTools':
                        SL_bookedtool = particlebank.addTupleTool(SLtool)
                        SL_bookedtool.Bmass = 6273.7
                        SL_bookedtool.VertexCov = True
                        SL_bookedtool.MomCov = True

                    if SLtool == 'TupleToolDocas':
                        SL_bookedtool= particlebank.addTupleTool(SLtool)
                        if mode == 'B2JpsiMuNuX':
                            SL_bookedtool.Name = ['Jpsi_Mu_plus']
                            SL_bookedtool.Location1 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['Jpsi'])]
                            SL_bookedtool.Location2 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['Mu_plus'])]
                        else:
                            SL_bookedtool.Name = ['D0_Mu_plus']
                            SL_bookedtool.Location1 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['D0'])]
                            SL_bookedtool.Location2 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['Mu_plus'])]

                    if SLtool == 'TupleToolApplyIsolation':
                        SL_bookedtool= particlebank.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
                        SL_bookedtool.WeightsFile="weights.xml"

                    if SLtool == 'TupleToolTauMuDiscrVars':
                        SL_bookedtool= particlebank.addTupleTool('TupleToolTauMuDiscrVars/TupleToolTauMuDiscrVars')
                        SL_bookedtool.Bmass = 6273.7

                    # --- this causes bugs!
                    if SLtool == 'TupleToolSLTruth' and self.sim is True:
                        SL_bookedtool = particlebank.addTupleTool('TupleToolSLTruth/TupleToolSLTruth')


                # DecayTreeFitter
                if not self.slim:
                    particlebank.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV
                    particlebank.ConsB.constrainToOriginVertex = False #neutrino, constraining tracks to PV -> worsens
                    #the vertex resolution and hence the mass resolution too
                    particlebank.ConsB.Verbose = True
                    if mode == 'B2JpsiMuNuX':
                        particlebank.ConsB.daughtersToConstrain = ['J/psi(1S)'] #constrain daughters to D0 mass, improve track
                    else:
                        particlebank.ConsB.daughtersToConstrain = ['D0'] #constrain daughters to D0 mass, improve track
                    #and mass resolution
                    particlebank.ConsB.UpdateDaughters = True #store info on final state tracks

        return NTuple

    def Restrip(self, restripLines_list, strip_version, EventNodeKiller):
        """
        Function to implement the restripping of MC

        """

        # Clean up previous stripping banks
        # Remove Decisions from the MC production
        eventNodeKiller = EventNodeKiller('Stripkiller')
        eventNodeKiller.Nodes = ['/Event/AllStreams', '/Event/Strip']

        confs = buildersConf()
        if self.name.startswith("Bc2JpsiMuNu"):
            if strip_version in ["stripping21", "stripping21r1", "stripping21r0p2","stripping21r0p2"]:
							  streams = buildStreamsFromBuilder(confs,'FullDSTDiMuon')
            if strip_version in ["stripping24r2","stripping28r2", "stripping29r2", "stripping34"]:
								streams = buildStreamsFromBuilder(confs,'DiMuonInherit')
        if self.name.startswith("Bc2D0MuNu"):  streams = buildStreamsFromBuilder(confs,'B2DMuNuX')

        MyStream = StrippingStream("MyStream")
        MyLines = restripLines_list

        for stream in streams:
            for line in stream.lines:
                if line.name() in MyLines:
                    MyStream.appendLines([line])

        from Configurables import ProcStatusCheck
        filterBadEvents = ProcStatusCheck()

        # HDRLocation = "SomeNonExistingLocation"
        sc = StrippingConf(Streams=[MyStream],
                           MaxCandidates=2000,
                           AcceptBadEvents=False,
                           BadEventSelection=filterBadEvents)

        # this should implement tje clearing of strip and performing restripping
        DaVinci().appendToMainSequence([eventNodeKiller])
        DaVinci().appendToMainSequence([sc.sequence()])

    def readLFN(self):
        """
        Wrapper for IOHelper() function
        :return: read in LFN from string
        """
        IOHelper().inputFiles(self.lfn, clear=True)

    # set DV configurables
    #if MDST: DaVinci().RootInTES = '/Event/{0}'.format(stream)
    def apply_configuration(self):
        """
        Main sequence, book top level sequence and decay-specific member sequences
        :return: Gaudisequences to producte outputfiles
        """
        if self.lfn is not None: self.readLFN()

        topSequence = GaudiSequencer('MasterSequence')
        topSequence.IgnoreFilterPassed=True  #allow all tuples to fill per event, ignoring fails

        restripLines = []
        # Loop and configure decay sequences, one per mode
        for decay in self.accessProperty('DecayModes')[self.name]:
            DecaySequence = GaudiSequencer(decay+'_Sequence')

            restripLines.append('Stripping'+self.accessProperty('StrippingLines')[decay])

            # implement pre-filters
            strip_filter = LoKi__HDRFilter('StripPassFilter_' + decay, Code="HLT_PASS('Stripping" + self.accessProperty('StrippingLines')[decay] + "Decision')")
            # for sim we have already restripped so don't need this filter (this also causes problems when trying to restrip older MC)
            if not self.sim: DecaySequence.Members+=[strip_filter]
            PV_filter = LoKi__VoidFilter('MultiPVFilter_'+ decay, Code="CONTAINS('Rec/Vertex/Primary') >= 1 ")
            DecaySequence.Members+=[PV_filter]

            # configure DTT
            dtt = self.makeNTuple(decay) # need to make one to initialise sub selection
            if hasattr(self,'SelSeq'): DecaySequence.Members+=[self.SelSeq.sequence()]
            DecaySequence.Members+=[dtt]

            # after config, add the sequence to MasterSequence:w
            topSequence.Members += [DecaySequence]

        restripVersion = self.accessProperty('StrippingVersions')[self.name][self.year]

        # DaVinci configurables
        if self.sim is True:
            if 'Bc2JpsiMuNuX' in self.name : self.Restrip(restripLines, restripVersion, EventNodeKiller)
            DaVinci().UserAlgorithms += [SMEAR('StateSmear')]
        else:
            DaVinci().UserAlgorithms+=[SCALER('StateScale')]

        DaVinci().UserAlgorithms += [topSequence]
        fileBaseName = 'Bc2D0MuNuX'
        if 'Bc2JpsiMuNuX' in self.name: fileBaseName = 'Bc2JpsiMuNuX'
        if self.slim: fileBaseName += 'Slim'
        DaVinci().TupleFile = fileBaseName + '.root'
        DaVinci().HistogramFile = fileBaseName + '_DVHistos.root'
        DaVinci().InputType = self.inputFormat
        DaVinci().PrintFreq = 5000
        DaVinci().DataType = self.year
        DaVinci().Simulation = self.sim
        DaVinci().Lumi = not DaVinci().Simulation
        DaVinci().EvtMax = self.maxEvts

        # for emergencies
        #from Configurables import MessageSvc
        #MessageSvc().OutputLevel = VERBOSE

        #this bit needs deleting once switching to autoDB in ganga
        #DaVinci().DDDBtag = 'Sim08-20130503-1'
        #DaVinci().CondDBtag = 'Sim08-20130503-1-vc-md100'

# END OF CLASS

# NOTES FOR LOCAL TESTING

# Local testing is done by running python RunDVTest.py

# If you need to get some local testing files to run the tests then use python FetchTestDatasets.py

#files = ['../../TestFiles/MC_D0MuNu_2017_00115807_00000002_1.bc2d0munu.strip.dst']
#DV = DaVinciConfig( 'Bc2D0MuNuX_D02KPi', 'DST', True, '2017', 1000, False, False, files )
#DV.apply_configuration()
