import future
from argparse import ArgumentParser
parser = ArgumentParser(usage="python GangaDownloadOutput.py")
parser.add_argument('-l','--logfile', default=None, help='File checker log.json file')
parser.add_argument('-o','--outdir', default=None, help='Location to download output to')
parser.add_argument('-u','--user',   default='mkenzie', help='lxplus username for grid file truncation')
parser.add_argument('-f','--force', default=False, action="store_true", help='Force overwrite if target already exists')
parser.add_argument('-d','--dryrun', default=False, action="store_true", help='Don\'t actually do any downloading')
parser.add_argument('-p','--noproxy', default=False, action="store_true", help='Do not reissue proxy')
opts = parser.parse_args()

import json
import os
import subprocess

assert( os.path.exists( opts.logfile ) )
if not opts.dryrun and not os.path.exists( opts.outdir ): 
  subprocess.run('mkdir -p %s'%opts.outdir, shell=True)
  if not opts.noproxy: 
    subprocess.run('lhcb-proxy-init', shell=True)

f = open( opts.logfile )
log = json.load( f )

for jid, files in log.items():
  outfiles = log[jid]['output']
  for outf in outfiles:
    if 'DVHistos' in outf: continue
    endpath = os.path.join( opts.outdir, '%s_%s'%(jid,outf.split(opts.user+'/')[-1].replace('/','_')) )
    if not opts.force and os.path.exists(endpath):
      print( 'Target file looks to already exist so won\'t overwrite it')
      continue

    dwnld_cmd  = 'lb-dirac dirac-dms-get-file -l %s -D %s'%(outf,opts.outdir)
    rename_cmd = 'mv %s/%s %s'%(opts.outdir,os.path.basename(outf),endpath)
    clean_cmd  = 'lb-dirac dirac-dms-remove-files %s'%outf

    if opts.dryrun:
      print (dwnld_cmd)
      print (rename_cmd)
      print (clean_cmd)
    else:
      subprocess.run( dwnld_cmd , shell=True, check=True )
      subprocess.run( rename_cmd, shell=True, check=True )
      if os.path.exists(endpath): 
        subprocess.run( clean_cmd, shell=True, check=True )

f.close()

