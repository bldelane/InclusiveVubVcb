### RUN WITH PYTHON BY DEFAULT ###
# if you want to read in the jobs then run ganga GangaCheck.py 1#

import os
import json
import sys

job_dict = {
    'D0MuNu'   : { '2011': { 'MU' : None,
                             'MD' : None },
                   '2012': { 'MU' : None,
                             'MD' : None },
                   '2015': { 'MU' : None,
                             'MD' : None },
                   '2016': { 'MU' : 119,
                             'MD' : 118 },
                   '2017': { 'MU' : None,
                             'MD' : None },
                   '2018': { 'MU' : None,
                             'MD' : None },
                  },

    'JpsiMuNu' : { '2011': { 'MU' : None,
                             'MD' : None },
                   '2012': { 'MU' : None,
                             'MD' : None },
                   '2015': { 'MU' : None,
                             'MD' : None },
                   '2016': { 'MU' : 121,
                             'MD' : 120 },
                   '2017': { 'MU' : None,
                             'MD' : None },
                   '2018': { 'MU' : None,
                             'MD' : None },
                  }
            }

job_status = {}
lfn_dict = {}
url_dict = {}

def StatStr( status ):
  conv = { ''}

def printJobs(job_dict,job_status):
  print '**** JOB STATUS ****'
  for mode in sorted(job_dict.keys()):
    for year in sorted(job_dict[mode].keys()):
      for polarity in sorted(job_dict[mode][year].keys()):
        jId = job_dict[mode][year][polarity]
        if not jId: continue
        tot = 0
        for status in job_status[mode][year][polarity].keys():
          tot += len( job_status[mode][year][polarity][status] )
        done = 0 if 'completed' not in job_status[mode][year][polarity].keys() else len( job_status[mode][year][polarity]['completed'] )
        print( '{:8} {:4} {:2} (jID={:<3}) : Total Done  {:4} /{:4} '.format( mode, year, polarity, jId, done,tot ) )
        for status in job_status[mode][year][polarity].keys():
          print ( '{:29} {:>10} {:4}'.format('',status, len( job_status[mode][year][polarity][status] ) ) )

def printLFNs(lfn_dict, url=False):
  print '**** LFN LIST ****' if not url else '**** URL LIST ****'
  for mode in sorted(lfn_dict.keys()):
    for year in sorted(lfn_dict[mode].keys()):
      for polarity in sorted(lfn_dict[mode][year].keys()):
        if not lfn_dict[mode][year][polarity]: continue
        print( '{:8} {:4} {:2} : '.format( mode, year, polarity ) )
        for subid in sorted(lfn_dict[mode][year][polarity].keys()):
          print( '\t {:4} : {:20} '.format( subid, lfn_dict[mode][year][polarity][subid] ) )

def printURLs(url_dict):
  printLFNs(url_dict,True)

def saveDict(dic, fname):
  os.system('mkdir -p log')
  f = open('log/{0}'.format(fname),'w')
  json.dump(dic,f)
  f.close()

def loadDict(fname):
  f = open('log/{0}'.format(fname))
  dic = json.load(f)
  f.close()
  return dic

def collectInfo(job_dict, job_status, lfn_dict, url_dict, load=False):
  if load:
    print '---- LOADING JOB STATUS ----'
    for mode in job_dict.keys():
      job_status[mode] = {}
      for year in job_dict[mode].keys():
        job_status[mode][year] = {}
        for polarity in job_dict[mode][year].keys():
          jId = job_dict[mode][year][polarity]
          if not jId: continue
          subjobs = jobs(jId).subjobs
          job_status[mode][year][polarity] = {}
          for sj in subjobs:
            if sj.status not in job_status[mode][year][polarity].keys(): job_status[mode][year][polarity][sj.status] = []
            job_status[mode][year][polarity][sj.status].append( sj.id )

    saveDict(job_status,'ganga_jobs.json')
    printJobs(job_dict,job_status)

    print '---- LOADING OUTPUTFILE LISTS ----'
    for mode in job_dict.keys():
      lfn_dict[mode] = {}
      url_dict[mode] = {}
      for year in job_dict[mode].keys():
        lfn_dict[mode][year] = {}
        url_dict[mode][year] = {}
        for polarity in job_dict[mode][year].keys():
          lfn_dict[mode][year][polarity] = {}
          url_dict[mode][year][polarity] = {}
          jId = job_dict[mode][year][polarity]
          if not jId: continue
          subjobs = jobs(jId).subjobs
          for sj in subjobs:
            if sj.status=='completed':
              lfn_dict[mode][year][polarity][sj.id] = sj.outputfiles[1].lfn
              url_dict[mode][year][polarity][sj.id] = sj.outputfiles[1].accessURL()[0]

    saveDict(lfn_dict,'ganga_lfns.json')
    saveDict(url_dict,'ganga_urls.json')

  else:
    job_status = loadDict('ganga_jobs.json')
    lfn_dict = loadDict('ganga_lfns.json')
    url_dict = loadDict('ganga_urls.json')

  if not load: printJobs(job_dict,job_status)
  printLFNs(lfn_dict)
  printURLs(url_dict)


collectInfo(job_dict,job_status,lfn_dict,url_dict,len(sys.argv)>1)

