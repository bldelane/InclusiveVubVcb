from __future__ import print_function

import json
import os

remote_loc = 'phskbk@aspin.epp.warwick.ac.uk:/storage/epp2/phskbk/gangadir/workspace/phskbk/LocalXML/'

job_map_fi = 'log/data_job_map.py'

import subprocess

import importlib
import pprint

jmap = None
if os.path.exists(job_map_fi):
	spec = importlib.util.spec_from_file_location('module',job_map_fi)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	if hasattr(module, 'job_map'):
		jmap = module.job_map
		#pp = pprint.PrettyPrinter(indent=2)
		#pp.pprint(jmap)

if not jmap: sys.exit('No job map found')

for mode, mitems in jmap.items():
  for year, yitems in mitems.items():
    for pol, jid in yitems.items():
      if jid is None: continue
      print( mode, year, pol, jid )
      outf = f'log/Data_Bc2{mode}_{year}_{pol}.json'
      inf  = os.path.join(remote_loc,str(jid),'output','file_tracker.json')
      
      cmd = 'scp %s %s'%(inf,outf)
      #print(cmd)
      subprocess.run( cmd, shell=True )
