# Issue type: (Potential) Bug

## Motivation

Outline the nature of the bug, if found, or the potential for a bug in the source code (e.g. making sure that a weighted Boost histogram stores the err**2 in `view().variance`).

## Proposed resolution

Brief outline of what needs to be implemented to fix the bug or the sanity checks that need to be performed to ensure a bug-free codebase.

## How-to

Brief outline of how to run the committed code. Contextualise within a `Snakefile` if needed.

## Next steps

Brief outline of the next steps, if any.

## Checks

List the sanity checks needed to be performed to close the issue.

## Check-list 

To ensure a clear documentation and a comprehensive tracking of developments, please complete the following actions:

- [ ] Assign label(s)
- [ ] Assign milestone
- [ ] CC relevant developers

Before closing the issue:

- [ ] CC reviewers or developers, if needed
- [ ] Is code self-explanatory: naming of variables is clear and comments inline where needed
- [ ] Header at the top of committed executables: 
    - [ ] Description of the code: purpose and outline of steps
    - [ ] Author and email


/cc @bldelane
