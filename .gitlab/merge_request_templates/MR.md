# Merge Request Title: 

## Motivation

Contextualise by outlining what stage of the analysis has been accomplished with this MR.

## MR description

Detailed description of the code implemented. Please emphasise the features added with this MR.

## How-to

Brief outline of how to run the committed code. Contextualise within a `Snakefile` if needed.

## Clarity

Please provide an evaluation of the clarity level of the documentation and code (specifically, is the code commented? are the variables self-explanatory?). Please tick the level of clarity of code:

- [ ] Bare: `README.md` and alike are missing. The code is not very well documented. **Please try to avoid merging if the code is in this state and spend some time documenting it; this will pay off in the long run.**
- [ ] Acceptable: any other analyst should be able to understand what is being implemented by this MR with a couple of questions. Code has some comments and variables are reasonably named. 
- [ ] Complete: `README.md` exists, code is commented, headers are in place and variables are self-explanatory. I have spent time making sure that this is very clear and ready out of the box. _This is the gold standard_.

## Checks

Outline the sanity checks performed to verify that the code is bug-free and a brief explanation of the dummy-tests you performed to verify that the results are compatible with expectation.

## Check-list 

To ensure a clear documentation and a comprehensive tracking of developments, please complete the following actions:

- [ ] Assign label(s)
- [ ] Assign milestone
- [ ] CC relevant developers

Before submitting MR:

- [ ] Branch has been rebased. Expect any conflicts in the MR to stem from alterations implemented by the author in this branch and _not from a branch that is not up-to-date with uncorrelated changes in `master`_. If this is not clear, please refer to this link to understand the advantages of rebasing a branch: [rebasing on gitlab](https://docs.gitlab.com/ee/topics/git/git_rebase.html)
- [ ] **Assign reviewer(s)**
- [ ] Code clarity evaluation performed.
- [ ] If code clairity level is `Bare`: opened issue and documented the steps needed to improve upon the clarity level.
- [ ] All sanity checks have passed. Attached plots and numbers to demonstrated this. 
- [ ] Opened issue to outline how does the ANA need to describe the accomplished improvements implemented with this MR.
 

/cc @bldelane
