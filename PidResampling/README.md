## PID Resampling

PID resampling is done using a wrapper for the Meerkat package. This script is called `pidgen.py` and will configure what is required based on options passsed (year, polarity etc.).

Multiple jobs can be submitted to the lxplus condor batch system using the `submit.py` script.

For example to run the PID resampling for 2018 MC files:

```bash
python submit.py -m D0MuNu -m JpsiMuNu -y 2018 -p MD -p MU
```

It's also possible to submit jobs to get the templates with systematic variations:

```bash
python submit.py -m D0MuNu -m JpsiMuNu -y 2018 -p MD -p MU -v stat_0 -v stat_1 -v stat_2 -v stat_3 -v stat_4 -v syst_1
```
