# setup before hand must be in LHCb software stack Urania environment
# lb-run -c best --nightly lhcb-head Urania/HEAD bash

from __future__ import print_function
import os

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--infile' , required=True, help='Path to input file raw MC tuple')
parser.add_argument('-o','--outfile', default=None , help='Path to output file (default will be the input with _pidgen added)')
parser.add_argument('-m','--mode'   , default='D0MuNu' , choices=['D0MuNu','D0MuNu_K3Pi','JpsiMuNu'], help='Mode to run')
parser.add_argument('-y','--year'   , default='2018'   , choices=['2011','2012','2015','2016','2017','2018'], help='Year')
parser.add_argument('-p','--magpol' , default='MD'     , choices=['MU','MD'], help='Magnet polarity')
parser.add_argument('-v','--variate', default=None     , help='Variation option to pass to PIDGen.py e.g. stat_0, syst_0...')
parser.add_argument('-f','--ntrscale', default=None    , help='Scaling for nTracks')
opts = parser.parse_args()

if opts.outfile is None:
  opts.outfile = opts.infile.replace('.root','_pidgen.root')

if opts.variate is not None:
  opts.outfile = opts.outfile.replace('.root','_%s.root'%opts.variate)

# Name of the calibration dataset
dataset = None
if opts.magpol=='MD'  : dataset = 'MagDown'
elif opts.magpol=='MU': dataset = 'MagUp'
else: raise RuntimeError('Not a recognised magnet polarity', opts.magpol)
dataset += '_'+opts.year

# Name of the input tree
# Could also include ROOT directory, e.g. "Dir/Ntuple"
input_tree = None
if   opts.mode == 'D0MuNu':      input_tree = 'B2DMuNuX_D02KPiTuple/DecayTree'
elif opts.mode == 'D0MuNu_K3Pi': input_tree = 'B2DMuNuX_D02K3PiTuple/DecayTree'
elif opts.mode == 'JpsiMuNu':    input_tree = 'B2JpsiMuNuXTuple/DecayTree'
else: raise RuntimeError('Not a valid mode', opts.mode)

# Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name w/o branch name)
# e.g. if the ntuple contains "pion_PT", it should be just "PT"
ptvar  = "PT"
etavar = None
pvar   = "P"
ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!

seed = None   # No initial seed
# seed = 1    # Alternatively, could set initial random seed

# Dictionary of tracks with their PID variables, in the form {branch name}:{pidvars}
# For each track branch name, {pidvars} is a dictionary in the form {ntuple variable}:{pid config},
#   where
#     {ntuple variable} is the name of the corresponding ntuple PID variable without branch name,
#   and
#     {pid_config} is the string describing the PID configuration.
# Run PIDCorr.py without arguments to get the full list of PID configs
reco = '_Brunel' if opts.year in ['2015','2016','2017','2018'] else ''
tune = 'MC15TuneV1_' if opts.year in ['2015','2016','2017','2018'] else 'V3'

tracks = {
	'Mu_plus' : {
								"PIDmu"		 : "mu_CombDLLmu_IsMuon%s"%reco,
								"ProbNNmu" : "mu_%sProbNNmu%s"%(tune,reco),
							}
          }

if opts.mode == 'D0MuNu' or opts.mode == 'D0MuNu_K3Pi':
  tracks['K_minus'] = {
      "PIDK"     : "K_CombDLLK%s"%reco,
      }
  tracks['Pi_1']    = {
      "PIDK"     : "pi_CombDLLK%s"%reco,
      }

  if opts.mode == 'D0MuNu_K3Pi':
    tracks['Pi_2']    = {
        "PIDK"     : "pi_CombDLLK%s"%reco,
        }
    tracks['Pi_3']    = {
        "PIDK"     : "pi_CombDLLK%s"%reco,
        }

elif opts.mode == 'JpsiMuNu':
	
  tracks['Mu_1']    = {
      "PIDmu"		 : "mu_CombDLLmu_IsMuon%s"%reco,
      "ProbNNmu" : "mu_%sProbNNmu%s"%(tune,reco),
      }
  tracks['Mu_2']    = {
      "PIDmu"		 : "mu_CombDLLmu_IsMuon%s"%reco,
      "ProbNNmu" : "mu_%sProbNNmu%s"%(tune,reco),
      }

else: raise RuntimeError('Not a valid mode', opts.mode)

lower_pids = {
	#'Mu_plus' : { "PIDmu"  : -0.01 },
						 }

# IF ON LXPLUS: if /tmp exists and is accessible, use for faster processing
# IF NOT: use /tmp if you have enough RAM
temp_folder = '/tmp'
# ELSE: use current folder
#temp_folder = '.'

# make sure we don't overwrite local files and prefix them with random strings
import string
import random
rand_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))  # get 10 random chars for temp_file prefix

temp_file_prefix = temp_folder + '/' + rand_string  # prefix temp files with folder and unique ID

output_tree = input_tree.split("/")[-1]

tmpinfile = opts.infile
tmpoutfile = "%s_tmp1.root"%temp_file_prefix
treename = input_tree
for track, subst in tracks.iteritems() :
  for var, config in subst.iteritems() :
    command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDGen.py"
    command += " -m %s_%s" % (track, ptvar)
    if etavar:
      command += " -e %s_%s" % (track, etavar)
    elif pvar:
      command += " -q %s_%s" % (track, pvar)
    else:
      print('Specify either ETA or P branch name per track')
      sys.exit(1)
    command += " -n %s" % ntrvar
    command += " -t %s" % treename
    command += " -p %s_%s_corr" % (track, var)
    command += " -c %s" % config
    command += " -d %s" % dataset
    command += " -i %s" % tmpinfile
    command += " -o %s" % tmpoutfile
    if seed :
      command += " -s %d" % seed
    if track in lower_pids.keys():
      if var in lower_pids[track].keys():
        command += " -l %4.2f" % lower_pids[track][var]
    if opts.variate is not None:
      command += " -v %s" % opts.variate
    if opts.ntrscale is not None:
      command += " -f %s" % opts.ntrscale
    command += " -a"

    treename = output_tree
    tmpinfile = tmpoutfile
    if 'tmp1' in tmpoutfile:
      tmpoutfile = tmpoutfile.replace('tmp1', 'tmp2')
    else :
      tmpoutfile = tmpoutfile.replace('tmp2', 'tmp1')

    print(command)
    os.system(command)

if "root://" in opts.outfile:
  print("xrdcp %s %s" % (tmpinfile, opts.outfile))
  os.system("xrdcp %s %s" % (tmpinfile, opts.outfile))
else:
  print("mv %s %s" % (tmpinfile, opts.outfile))
  os.system("mv %s %s" % (tmpinfile, opts.outfile))


