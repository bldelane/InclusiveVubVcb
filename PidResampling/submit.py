# write bash scripts to execute our PID and submit them
from __future__ import print_function
import os
from tabulate import tabulate

mode_choices = [ 'D0MuNu', 'D0MuNu_K3Pi', 'JpsiMuNu']
year_choices = [ '2011','2012','2015','2016','2017','2018']
pol_choices  = [ 'MU', 'MD' ]
var_choices  = [ 'default', 'stat_0', 'stat_1', 'stat_2', 'stat_3', 'stat_4', 'syst_1' ]
resub_choices = [ 'Queued','Failed','Running','NotCreated','All' ]
queue_choices = [ 'longlunch','workday','tomorrow' ]

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-l', '--loc'     , default='/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC/', help='Base path location for files')
parser.add_argument('-m', '--mode'    , default=[], action='append', choices = mode_choices, help='Mode(s) to run. Can pass multiple times. Allowed values: {'+', '.join(mode_choices)+'}' )
parser.add_argument('-y', '--year'    , default=[], action='append', choices = year_choices, help='Year(s) to run. Can pass multiple times. Allowed values: {'+', '.join(year_choices)+'}' )
parser.add_argument('-p', '--polarity', default=[], action='append', choices = pol_choices , help='Magnet polarity(ies) to run. Can pass multiple times. Allowed values: {'+', '.join(pol_choices)+'}' )
parser.add_argument('-v', '--variate' , default=[], action='append', choices = var_choices , help='Variation choices. Can pass multiple times. Allowed values: {'+', '.join(var_choices)+'}' )
parser.add_argument('-S', '--submit'  , default=False, action='store_true', help='Submit jobs')
parser.add_argument('-C', '--check'   , default=False, action='store_true', help='Check for missing files')
parser.add_argument('-R', '--resub'   , default=None,  choices=resub_choices, help='Resubmit missing files. Allowed values: {'+', '.join(resub_choices)+'}')
parser.add_argument('-q', '--queue'   , default="workday", choices=queue_choices, help='Queue to submit to. Allowed values: {'+', '.join(queue_choices)+'}')
opts = parser.parse_args()

if len(opts.variate)==0: opts.variate.append('default')

def write_script(command, script_name):
  
  subf = open('%s/sub/%s'%(os.getcwd(),script_name),'w')
  subf.write('#!/bin/bash\n\n')
  subf.write('source /cvmfs/lhcb.cern.ch/lib/LbEnv\n')

  subf.write('rm -f %s.done\n'%subf.name)
  subf.write('rm -f %s.fail\n'%subf.name)
  subf.write('touch %s.run \n\n'%subf.name)

  subf.write('cp %s/pidgen.py .\n\n'%os.getcwd())

  subf.write('if ( lb-run -c best Urania/v10r0 %s ); then\n'%command)
  subf.write('  touch %s.done\n'%subf.name)
  subf.write('  rm -f %s.run \n'%subf.name)
  subf.write('else\n')
  subf.write('  touch %s.fail\n'%subf.name)
  subf.write('  rm -f %s.run \n'%subf.name)
  subf.write('fi\n')

  subf.close()

  os.system('chmod +x %s'%subf.name)

def check_corr_file(fname, mode):
  if not os.path.exists(fname): return False
  from ROOT import TFile, TTree
  tf = TFile(fname)
  tr = tf.Get("DecayTree")
  #tr.Print("*corr")
  if not tr: return False
  if not hasattr(tr,'Mu_plus_PIDmu_corr'): return False
  if mode=='JpsiMuNu':
    if not hasattr(tr,'Mu_1_PIDmu_corr'): return False
    if not hasattr(tr,'Mu_2_PIDmu_corr'): return False
  else:
    if not hasattr(tr,'K_minus_PIDK_corr'): return False
    if not hasattr(tr,'Pi_1_PIDK_corr'): return False
    if mode=='D0MuNu_K3Pi':
      if not hasattr(tr,'Pi_2_PIDK_corr'): return False
      if not hasattr(tr,'Pi_3_PIDK_corr'): return False
  tf.Close()
  return True

#print('%-30s'%'Script', '%-10s'%'Mode', ' %4s '%'Year', '%4s'%'Pol', ' %-8s '%'Variate', '%s'%'Filename')
#print(''.join( [ '-' for i in range(100) ] ) )
print_rows = []
scripts = []

for mode in opts.mode:
  for year in opts.year:
    for pol in opts.polarity:

      flist = None
      
      if mode=='D0MuNu':
        flist = [ 'Bc2D0MuNu', 'Bc2D0gMuNu', 'Bc2D0pi0MuNu', 'Bu2D0MuNu' ]
      
      elif mode=='D0MuNu_K3Pi':
        flist = [ 'Bc2D0MuNu_K3Pi', 'Bc2D0gMuNu_K3Pi', 'Bc2D0pi0MuNu_K3Pi', 'Bu2D0MuNu_K3Pi' ]

      elif mode=='JpsiMuNu':
        flist = [ 'Bc2JpsiMuNu', 'Bc2JpsiTauNu' ]
        if year=='2015': flist = [ 'Bc2JpsiMuNu' ]

      else:
        raise RuntimeError('Not a valid mode', mode)

      if year=='2018':

        if mode=='D0MuNu'     : flist += [ 'Bc2D0MuNu_Kis', 'Bc2D0gMuNu_Kis', 'Bc2D0pi0MuNu_Kis', 'Bc2D0MuNu_ISGW2', 'Bc2D0gMuNu_ISGW2', 'Bc2D0pi0MuNu_ISGW2']
        if mode=='D0MuNu_K3Pi': flist += [ 'Bc2D0MuNu_K3Pi_ISGW2', 'Bc2D0gMuNu_K3Pi_ISGW2', 'Bc2D0pi0MuNu_K3Pi_ISGW2']
      
      if year=='2012' or year=='2016':

        if mode=='JpsiMuNu'   : flist += [ 'Bc2JpsiPi' ]

      for fil in flist:
        path = os.path.join( opts.loc, mode.replace('_K3Pi',''), year, pol )
        infname = 'MC_%s_%s_%s.root'%(fil,year,pol)
        if not os.path.exists( os.path.join(path,infname) ):
          raise RuntimeError('No such file', infname, 'in', path)
        for var in opts.variate:
          if var != 'default' and ('_Kis' in infname or '_ISGW2' in infname): continue

          script_name = '%s_%s_%s.sh'%(fil,year,pol)

          command = 'python pidgen.py'
          command += ' -i %s' % os.path.join(path,infname)
          command += ' -m %s' % mode
          command += ' -y %s' % year
          command += ' -p %s' % pol
          if var != 'default':
            command += ' -v %s' % var
            os.system('mkdir -p %s/pidsyst'%path)
            command += ' -o %s/pidsyst/%s'%(path,infname.replace('.root','_pidgen.root'))
            script_name = script_name.replace('.log','_%s.log'%var)

          if year=='2011' or year=='2012':
            command += ' -f 1.2'
          else:
            command += ' -f 1.4'
          
          status = 'CREATED'
          if not opts.check: 
            write_script(command, script_name)
            scripts.append(script_name)
          else:
            fil = os.path.exists('sub/'+script_name)
            done = os.path.exists('sub/'+script_name+'.done')
            fail = os.path.exists('sub/'+script_name+'.fail')
            run  = os.path.exists('sub/'+script_name+'.run')
            queued = not done and not fail and not run and fil
            if done: status = 'DONE'
            if fail: status = 'FAILED'
            if run:  status = 'RUNNING'
            if queued: status = 'QUEUED'
            if not fil: status = 'NOT CREATED'
            
            if done:
              outfname = os.path.join(path,infname).replace('.root','_pidgen.root')
              if var != 'default': outfname = os.path.join(path,pidsyst,infname.replace('.root','_pidgen_%s.root'%var))
              corr = check_corr_file(outfname, mode)
              if not corr: status = 'INCOMPLETE'

            if opts.resub is not None:
              resubmit = False
              if fail   and ( 'Failed' in opts.resub or 'All' in opts.resub ): resubmit = True 
              if run    and ( 'Running' in opts.resub or 'All' in opts.resub ): resubmit = True 
              if queued and ( 'Queued' in opts.resub or 'All' in opts.resub ): resubmit = True
              if not fil and ('NotCreated' in opts.resub or 'All' in opts.resub ): resubmit = True
              if resubmit:
                write_script(command, script_name)
                scripts.append(script_name)

          print_rows.append( [script_name, mode, year, pol, var, status, infname] )

print(tabulate(print_rows,headers=['Script','Mode','Year','Pol','Variate','Status','Filename']))

if opts.check and not opts.resub:
  import sys
  sys.exit()

if opts.resub:
  print('Resubmitting missing files')

# can now write the file which lists all the jobs
jlist = open('sub/joblist.txt','w')
for s in scripts: 
  jlist.write('sub/'+s+'\n')
jlist.close()
print('Job list written to sub/joblist.txt')

# and can now write the file for submission
sfile = open('sub/job.sub','w')
sfile.write('executable = $(subfile)\n')
sfile.write('arguments =\n')
sfile.write('output = $(subfile).out\n')
sfile.write('error = $(subfile).err\n')
sfile.write('log = sub/job.log\n')
sfile.write('+JobFlavour = "%s"\n'%opts.queue)
sfile.write('queue subfile from sub/joblist.txt\n')
sfile.close()
os.system('chmod +x %s'%sfile.name)
print('Submission file written to', sfile.name)

# submit
if opts.submit:
  os.system('condor_submit %s'%sfile.name)
