import os
import sys

## START OF CONFIG
# Read comments and check vars
# at least until end of config section

# List of input ROOT files with MC ntuples. Format:
#   (inputfile, outputfile, dataset)
files = [
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MD/MC_Bc2D0MuNu_D0MuNu_2016_MD.root", "MC_Bc2D0MuNu_D0MuNu_2016_MD_pidgen.root", "MagDown_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MU/MC_Bc2D0MuNu_D0MuNu_2016_MU.root", "MC_Bc2D0MuNu_D0MuNu_2016_MU_pidgen.root", "MagUp_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MD/MC_Bc2D0gMuNu_D0MuNu_2016_MD.root", "MC_Bc2D0gMuNu_D0MuNu_2016_MD_pidgen.root", "MagDown_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MU/MC_Bc2D0gMuNu_D0MuNu_2016_MU.root", "MC_Bc2D0gMuNu_D0MuNu_2016_MU_pidgen.root", "MagUp_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MD/MC_Bc2D0pi0MuNu_D0MuNu_2016_MD.root", "MC_Bc2D0pi0MuNu_D0MuNu_2016_MD_pidgen.root", "MagDown_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MU/MC_Bc2D0pi0MuNu_D0MuNu_2016_MU.root", "MC_Bc2D0pi0MuNu_D0MuNu_2016_MU_pidgen.root", "MagUp_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MD/MC_Bu2D0MuNu_D0MuNu_2016_MD.root", "MC_Bu2D0MuNu_D0MuNu_2016_MD_pidgen.root", "MagDown_2016"),
  ("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v3/MC/D0MuNu/2016/MU/MC_Bu2D0MuNu_D0MuNu_2016_MU.root", "MC_Bu2D0MuNu_D0MuNu_2016_MU_pidgen.root", "MagUp_2016"),
]

# Name of the input tree
# Could also include ROOT directory, e.g. "Dir/Ntuple"
input_tree = "B2DMuNuX_D02KPiTuple/DecayTree"

# Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name w/o branch name)
# e.g. if the ntuple contains "pion_PT", it should be just "PT"
ptvar  = "PT"
etavar = None
pvar   = "P"
ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!

seed = None   # No initial seed
# seed = 1    # Alternatively, could set initial random seed

# Dictionary of tracks with their PID variables, in the form {branch name}:{pidvars}
# For each track branch name, {pidvars} is a dictionary in the form {ntuple variable}:{pid config},
#   where
#     {ntuple variable} is the name of the corresponding ntuple PID variable without branch name,
#   and
#     {pid_config} is the string describing the PID configuration.
# Run PIDCorr.py without arguments to get the full list of PID configs
tracks = {
	'Mu_plus' : {
								"PIDmu"		 : "mu_CombDLLmu_Brunel",
								#"isMuon"	 : "mu_CombDLLmu_IsMuon_Brunel",
								# we only really use the two above but add the following for completeness
								#"PIDK"								: "mu_CombDLLK_Brunel",
								"ProbNNmu" : "mu_MC15TuneV1_ProbNNmu_Brunel",
								#"ProbNNk"  : "mu_MC15TuneV1_ProbNNK_Brunel",
								#"ProbNNpi" : "mu_MC15TuneV1_ProbNNpi_Brunel",
							},
	'K_minus' : {
								"PIDK"     : "K_CombDLLK_Brunel",
								#"ProbNNk"  : "K_MC15TuneV1_ProbNNK_Brunel_Mod2",
								#"ProbNNpi" : "K_MC15TuneV1_ProbNNpi_Brunel",
							},
	'Pi_1'    : {
								"PIDK"     : "pi_CombDLLK_Brunel",
								#"ProbNNk"  : "pi_MC15TuneV1_ProbNNK_Brunel",
								#"ProbNNpi" : "pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",
							},
}

lower_pids = {
	#'Mu_plus' : { "PIDmu"  : -0.01 },
						 }

# IF ON LXPLUS: if /tmp exists and is accessible, use for faster processing
# IF NOT: use /tmp if you have enough RAM
temp_folder = '/tmp'
# ELSE: use current folder
#temp_folder = '.'

## END OF CONFIG


# make sure we don't overwrite local files and prefix them with random strings
import string
import random
rand_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))  # get 10 random chars for temp_file prefix

temp_file_prefix = temp_folder + '/' + rand_string  # prefix temp files with folder and unique ID

output_tree = input_tree.split("/")[-1]

for input_file, output_file, dataset in files :
  tmpinfile = input_file
  tmpoutfile = "%s_tmp1.root" % temp_file_prefix
  treename = input_tree
  for track, subst in tracks.iteritems() :
    for var, config in subst.iteritems() :
      command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDGen.py"
      command += " -m %s_%s" % (track, ptvar)
      if etavar:
        command += " -e %s_%s" % (track, etavar)
      elif pvar:
        command += " -q %s_%s" % (track, pvar)
      else:
        print('Specify either ETA or P branch name per track')
        sys.exit(1)
      command += " -n %s" % ntrvar
      command += " -t %s" % treename
      command += " -p %s_%s_corr" % (track, var)
      command += " -c %s" % config
      command += " -d %s" % dataset
      command += " -i %s" % tmpinfile
      command += " -o %s" % tmpoutfile
      if seed :
        command += " -s %d" % seed
      if track in lower_pids.keys():
				if var in lower_pids[track].keys():
					command += " -l %4.2f" % lower_pids[track][var]

      treename = output_tree
      tmpinfile = tmpoutfile
      if 'tmp1' in tmpoutfile:
        tmpoutfile = tmpoutfile.replace('tmp1', 'tmp2')
      else :
        tmpoutfile = tmpoutfile.replace('tmp2', 'tmp1')

      print(command)
      os.system(command)

  if "root://" in output_file:
    print("xrdcp %s %s" % (tmpinfile, output_file))
    os.system("xrdcp %s %s" % (tmpinfile, output_file))
  else:
    print("mv %s %s" % (tmpinfile, output_file))
    os.system("mv %s %s" % (tmpinfile, output_file))

