"""Loading utilities"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
from utils.header import *
from tqdm import tqdm
from typing import List, Dict, Union


def check_argpath(func):
    """decorator to check input path exists"""

    def wrapper(feats_path, _key, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, _key)
        return features

    return wrapper


# read  feature list
@check_argpath
def read_feats(
    feats_path: str,
    _key: str,
) -> Union[list, dict]:
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, "r") as stream:
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    try:
        _key in in_dict
        feature_list = in_dict[_key]
    except ValueError:
        print(f"'{_key}' key not in dict")

    return feature_list


# read in a BOI file
def yml_to_dict(
    _path: "yml file",
) -> dict:
    with open(_path, "r") as stream:
        try:
            parsed_yaml = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return parsed_yaml


@timing
def load_concat_ROOT_file(
    _files: list,
    _branches: list,
    _library="pd",
    _cut: "string" = None,
    _name: "string, TeX format" = None,
    **kwargs,
) -> pd.DataFrame:
    """wrapper for uproot.concatenate"""
    df = uproot.concatenate(
        files=_files, expressions=_branches, cut=_cut, library=_library, **kwargs
    )
    df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df


@timing
def oneshot_load_ROOT_file(
    file: "string",
    branches: list,
    library="pd",
    cut: "string" = None,
    name: "string, TeX format" = None,
    max_entries: "cap on number of events read in" = None,
    **kwargs,
) -> "pandas dataframe, with name for labels in plots, supports entry_stop":
    events = uproot.open(f"{file}")
    df = events.arrays(
        expressions=branches,
        cut=cut,
        library=library,
        entry_stop=max_entries,
        **kwargs,
    )
    df.name = name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df


@timing
def batched_load_ROOT_file(
    file: "string",
    branches: list,
    library="pd",
    cut: "string" = None,
    name: "string, TeX format" = None,
    max_entries: "cap on number of events read in" = None,
    batch_size="200 MB",
    **kwargs,
) -> "pandas dataframe, with name for labels in plots, supports entry_stop":
    """wrapper for uproot iterate"""
    events = uproot.open(f"{file}")
    bevs = events.num_entries_for(batch_size, branches, entry_stop=max_entries)
    tevs = events.num_entries
    nits = round(tevs / bevs + 0.5)
    aggr = []
    for batch in tqdm(
        events.iterate(
            expressions=branches,
            cut=cut,
            library=library,
            entry_stop=max_entries,
            step_size=batch_size,
            **kwargs,
        ),
        total=nits,
        ascii=True,
        desc=f"batches loaded",
    ):
        aggr.append(batch)

    df = pd.concat(aggr)
    if cut is None:
        assert len(df) == events.num_entries
    df.name = name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df
