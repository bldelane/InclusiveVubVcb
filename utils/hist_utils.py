"""Manipulate histograms"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from builtins import breakpoint
import boost_histogram as bh
from utils.header import timing

@timing
def to_hist(
    data:list,  
    bins:list, 
    range:list, 
    wts:list=None, 
    )->"boost-histogram":
    
    if wts is not None:
        hist = bh.histogram( bh.axis.regular(bins,*range), storage=bh.storage.weight() )
        hist.fill( data, weight = wts )
    else:
        hist = bh.histogram( bh.axis.regular(bins,*range) )
        hist.fill( data )

    return hist     

    