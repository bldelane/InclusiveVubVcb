"""Common analysis modules"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from argparse import ArgumentParser
import os, glob
from pathlib import Path

# from scipy import stats
import numpy as np
import boost_histogram as bh
import pandas as pd
import yaml
import json
import pyhf
import uproot
import time

# pyhf.set_backend("numpy", "minuit") # get uncertainties and covariance
pyhf.set_backend(
    "numpy", pyhf.optimize.minuit_optimizer(verbose=1, errordef=1.0)
)  # get uncertainties and covariance
import pickle
from uncertainties import *
from matplotlib import gridspec
from tabulate import tabulate
import json, requests, jsonschema

# cosmetics
import mplhep as hep
import matplotlib.pyplot as plt

# plt.style.use(hep.style.LHCb2)
# plt.rcParams.update({
#     "text.usetex": True,
#     "font.family": "serif",
#     #"font.serif": ["Palatino"],
#     "axes.labelsize": 20,
#     "axes.titlesize": 20,
#     "xaxis.labellocation": "center",
#     "yaxis.labellocation": "center",
#     "legend.fontsize": 17,
#     "ytick.labelsize": 20,
#     "xtick.labelsize": 20,
# })
plt.style.use("science")


def timing(func):
    """decorator to print function execution time [ms]"""

    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = func(*args, **kwargs)
        time2 = time.time()
        print(
            "{:s} function took {:.3f} ms".format(
                func.__name__, (time2 - time1) * 1000.0
            )
        )

        return ret

    return wrap
