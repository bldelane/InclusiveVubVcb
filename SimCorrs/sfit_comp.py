from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-y','--year',default='2016',help='Year')
parser.add_argument('-f','--refit',default=False,action='store_true', help='Rerun the fit and recompute the sweights')
parser.add_argument('-u','--unbinned',default=False, action='store_true', help='Do the fit unbinned')
parser.add_argument('-F','--nbins_fit',default=200,type=int,help='Number of fit bins for binned fit')
parser.add_argument('-P','--nbins_plot',default=100,type=int,help='Number of plot bins')
parser.add_argument('-i','--interactive',default=False,action='store_true', help='Show plots at end')
args = parser.parse_args()

import os
os.system(f'mkdir -p plots/{args.year}')

import pandas as pd
import numpy as np

from iminuit import Minuit
from iminuit.cost import ExtendedUnbinnedNLL, ExtendedBinnedNLL
from scipy.stats import crystalball, expon, norm

from SWeighter import SWeight

import matplotlib.pyplot as plt
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.size": 14,
    "text.latex.preamble":r"\usepackage{amsmath}",
    })

mrange = (6000,6600)

data = pd.read_pickle(f'dfs/df_data_{args.year}.pkl')
mc   = pd.read_pickle(f'dfs/df_sim_{args.year}.pkl')

def mass_cdf(x, ns, nb, mu, sg, lb):
#def mass_cdf(x, ns, nb, f, mu, sg, a1, a2, n1, n2, lb):

  # signal
  #cb1 = crystalball(a1,n1,mu,sg)
  #cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  #cb1n = np.diff(cb1.cdf(mrange))
  #cb2n = np.diff(cb2.cdf(mrange))
  #invx = mrange[1]-x+mrange[0]
  #sig = f * cb1.cdf(x)/cb1n + (1-f)*(1-cb2.cdf(invx)/cb2n)
  gaus = norm(mu,sg)
  gausn = np.diff(gaus.cdf(mrange))
  sig = gaus.cdf(x)/gausn

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.cdf(x)/expn

  return ns * sig + nb * bkg

def mass_pdf(x, ns, nb, mu, sg, lb, comps=None):
#def mass_pdf(x, ns, nb, f, mu, sg, a1, a2, n1, n2, lb, comps=None):
  if comps is None:
    comps = ['sig','bkg']

  tot = 0

  # signal
  #cb1 = crystalball(a1,n1,mu,sg)
  #cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  #cb1n = np.diff(cb1.cdf(mrange))
  #cb2n = np.diff(cb2.cdf(mrange))
  #invx = mrange[1]-x+mrange[0]
  #sig = f * cb1.pdf(x)/cb1n + (1-f)*(cb2.pdf(invx)/cb2n)
  gaus = norm(mu,sg)
  gausn = np.diff(gaus.cdf(mrange))
  sig = gaus.pdf(x)/gausn
  if 'sig' in comps: tot += ns*sig

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.pdf(x)/expn
  if 'bkg' in comps: tot += nb*bkg

  return tot

def fmass_pdf(x, ns, nb, f, mu, sg, a1, a2, n1, n2, lb):
  return (ns+nb, mass_pdf(x,ns,nb,f,mu,sg,a1,a2,n1,n2,lb))

## for binned fit make the histogram
nh, xe = np.histogram( data['B_plus_M'].to_numpy(), bins=args.nbins_fit, range=mrange )
cx = 0.5*(xe[1:]+xe[:-1])

cost = ExtendedUnbinnedNLL( data['B_plus_M'].to_numpy(), fmass_pdf ) if args.unbinned else ExtendedBinnedNLL( nh, xe, mass_cdf )
mi = Minuit( cost,
             ns = 80e3,
             nb = 20e3,
             mu = 6280,
             sg = 20,
             #f = 0.5,
             #a1 = 1.5,
             #n1 = 3,
             #a2 = 1.5,
             #n2 = 3,
             lb = 400 )

mi.limits['ns'] = (0,100e3)
mi.limits['nb'] = (0,100e3)
mi.limits['mu'] = (6200,6340)
mi.limits['sg'] = (0,50)
#mi.limits['f']  = (0,1)
#mi.limits['a1'] = (0,10)
#mi.limits['a2'] = (0,10)
#mi.fixed['n1'] = True
#mi.fixed['n2'] = True
mi.limits['lb'] = (0,5000)

if args.refit:
  print('Running fit')
  mi.migrad()
  mi.hesse()
  print(mi)

  # plot fit
  fig, ax = plt.subplots(2,1,figsize=(6,6), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
  pnh, pxe = np.histogram(data['B_plus_M'].to_numpy(), bins=args.nbins_plot, range=mrange)
  pcx = 0.5*(pxe[1:]+pxe[:-1])

  # plot data histogram
  ax[0].errorbar(
    x=pcx,
    y=pnh,
    xerr=0.5 * (pxe[1:]-pxe[:-1]),
    yerr=pnh**0.5,
    fmt='.',
    color="black",
    markersize=5,
    elinewidth=1.1,
    capsize=1,
    capthick=1,
    label = f'{args.year} Data'
    )

  # plot fitted pdf every 400 points
  x = np.linspace(*mrange,400)
  N = (mrange[1]-mrange[0])/args.nbins_plot

  bkg = N*mass_pdf(x,*mi.values,comps=['bkg'])
  sig = N*mass_pdf(x,*mi.values,comps=['sig'])
  tot = N*mass_pdf(x,*mi.values)

  ax[0].plot(x, bkg, 'r--', label='Combinatorial')
  ax[0].plot(x, sig, 'g:', label='Signal')
  ax[0].plot(x, tot, 'b-', label='Total PDF')

  # make the pull
  pull = (pnh-N*mass_pdf(pcx,*mi.values))/(pnh**0.5)

  # pull box
  ax[1].set_ylim(bottom=-5.5, top=5.5)
  ax[1].axhline(y=5, color="red", ls="--", lw=1)
  ax[1].axhline(y=-5, color="red", ls="--", lw=1)
  ax[1].axhspan(-3, 3, alpha=0.1, facecolor="green")
  ax[1].axhline(y=0, color="green", ls="--", lw=1, alpha=1.)

  # plot pull
  ax[1].errorbar(
    x=pcx,
    y=pull,
    xerr=0.5 * (pxe[1:]-pxe[:-1]),
    yerr=np.ones_like(pcx),
     fmt='.',
    color="black",
    markersize=5,
    elinewidth=1.1,
    capsize=1,
    capthick=1
  )

  # labels
  ax[1].set_xlabel(r"$m(J/\psi \pi^+)$ [MeV$/c^2$]", loc="center")
  ax[0].set_ylabel(rf"Events / {(mrange[1]-mrange[0])/args.nbins_plot} MeV$/c^2$")
  ax[1].set_ylabel(r'Pulls [$\sigma$]', loc="center")
  ax[0].legend(fontsize=15)

  fig.tight_layout()
  fig.savefig(f'plots/{args.year}/massfit.pdf')

  # now compute the weights
  spdf = lambda x: mass_pdf(x,*mi.values,comps=['sig'])
  bpdf = lambda x: mass_pdf(x,*mi.values,comps=['bkg'])

  print('Computing sWeights')
  sweighter = SWeight( data['B_plus_M'].to_numpy(), [spdf,bpdf], [mi.values['ns'],mi.values['nb']], (mrange,), compnames=('sig','bkg'), checks=True)

  sws = sweighter.getWeight(0,data['B_plus_M'].to_numpy())
  bws = sweighter.getWeight(1,data['B_plus_M'].to_numpy())

  data['sw'] = sws
  data['bw'] = bws

  data.to_pickle(f'dfs/df_data_{args.year}_sws.pkl')

  # plot sWeights check
  fig, ax = plt.subplots()
  swp = sweighter.getWeight(0,x)
  bwp = sweighter.getWeight(1,x)
  ax.plot(x, swp, 'b--', label='Signal')
  ax.plot(x, bwp, 'r:' , label='Background')
  ax.plot(x, swp+bwp, 'k-', label='Sum')
  ax.set_xlabel('$m(J/\psi\pi^+)$ [MeV$/c^2$]')
  ax.set_ylabel('Weight')
  ax.legend()
  fig.tight_layout()
  fig.savefig(f'plots/{args.year}/weights.pdf')


# now make a plot for each var
data = pd.read_pickle(f'dfs/df_data_{args.year}_sws.pkl')

# use boost histogram for weighted hists
import boost_histogram as bh

# pars to plot
popts = { 'B_plus_DIRA_OWNPV'   : (50,0.9995,1),
          'B_plus_P'            : (50,0,0.5e6),
          'B_plus_PT'           : (50,0,30000),
          'B_plus_LK_LTIME'     : (50,0,0.01),
          'B_plus_ENDVERTEX_CHI2': (50,0,25),
          'B_plus_DOCA_Jpsi_Mu_plus': (50,0,0.15),
          'B_plus_MCORR'        : (50,6000,9000),
          'B_plus_MCORRERR'     : (50,0,2500),
          'nTracks'             : (50,0,1000),
          'Jpsi_P'              : (0,300e3),
          'Jpsi_PT'             : (0,20000),
          'Mu_plus_IPCHI2_OWNPV': (50,0,1000),
          'Mu_plus_P'           : (50,0,100000),
          'Mu_plus_PT'          : (50,0,10000),
          'Mu_plus_PIDmu_corr'  : (50,-20,0),
          'Mu_1_PIDmu_corr'     : (50, -2, 15),
          'Mu_2_PIDmu_corr'     : (50, -2, 15),
        }

for par in popts.keys():
  fig,ax = plt.subplots(2,1,figsize=(6,6), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
  bins = 50
  range = None
  if len(popts[par])==1: bins = popts[par][0]
  elif len(popts[par])==2: range = popts[par]
  elif len(popts[par])==3:
    bins = popts[par][0]
    range = (popts[par][1],popts[par][2])
  # auto bin on MC
  nmc, xe = np.histogram( mc[par].to_numpy(), bins=bins, range=range )
  nmc = nmc/np.sum(nmc)
  # make data hist
  dhist = bh.Histogram( bh.axis.Regular(bins,xe[0],xe[-1]), storage=bh.storage.Weight() )
  dhist.fill( data[par].to_numpy(), weight=data['sw'].to_numpy() )
  errs = dhist.variances()**0.5
  N = dhist.sum().value
  cx = 0.5*(xe[1:]+xe[:-1])
  ax[0].plot(cx, nmc, 'b-', ds='steps-mid', label='Simulation')
  ax[0].errorbar( cx, dhist.values()/N, errs/N, fmt='ko', label='sWeighted Data')
  ax[0].legend()
  ax[0].set_ylabel('Events')
  pull = (dhist.values()/N - nmc ) / (errs/N)
  ax[1].plot((xe[0],xe[-1]),(0,0),'b-')
  ax[1].errorbar( cx, pull, yerr=1, fmt='ko' )
  ax[1].set_xlabel(par,usetex=False)
  ax[1].set_ylabel('Pull')
  ax[1].set_ylim(-3,3)
  fig.tight_layout()
  fig.savefig(f'plots/{args.year}/{par}_sw.pdf')

if args.interactive: plt.show()

