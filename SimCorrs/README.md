# Simulation Corrections

Implement selection for Bc -> JpsiPi using the ANA note tables and selection on the normalisation channel data samples (those with `StdNoPIDsMuon`) and the MC provided by Alison. Then fit this distribution for Bc signal peak and make comparison of sWeighted data with MC.

A script called `comp.py` which simply looks up the relevant files on `eos` and converts to a `DataFrame` holding minimal variables and having applied the Bc -> JpsiPi selection.

A script called `sfit_comp.py` which fits the data in `B_plus_M` and produces sWeights for the Bc peak. Then makes plots comparing the MC to the sWeighted data.

Create data frame for data:
```bash
python comp.py -y 2016
```

Create data frame for simulation:
```bash
python comp.py -y 2016 -S
```

Then make sWeight comparison plots:
```bash
python sfit_comp.py -y 2016 -f
```

