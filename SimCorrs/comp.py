from argparse import ArgumentParser
import uproot
import glob
import numpy as np
from tqdm import tqdm
import pandas as pd

parser = ArgumentParser()
parser.add_argument('-y','--year', default='2016', help='Year')
parser.add_argument('-S','--sim' , default=False, action='store_true', help='Simulation?')
parser.add_argument('-n','--nfiles', default=-1, type=int, help='Number of files to process')
args = parser.parse_args()

import os
os.system('mkdir -p plots')
os.system(f'mkdir -p dfs/{args.year}')

data = not args.sim

aliases = None

if data:
    datafiles = glob.glob(f'/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA/JpsiMuNu/{args.year}/*/*.root')
    datatrees = [ f+':B2JpsiMuNuXTuple/DecayTree' for f in datafiles ]
    aliases   = { 'Mu_1_PIDmu_corr' : 'Mu_1_PIDmu',
                  'Mu_2_PIDmu_corr' : 'Mu_2_PIDmu',
                  'Mu_plus_PIDmu_corr' : 'Mu_plus_PIDmu'}
else:
    datafiles = glob.glob(f'/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC/JpsiMuNu/{args.year}/*/MC_Bc2JpsiPi_{args.year}_*_pidgen.root')
    datatrees = [ f+':DecayTree' for f in datafiles ]

if args.nfiles>0:
    datatrees = datatrees[:args.nfiles]

branches = [ 'B_plus_M', 'B_plus_P', 'B_plus_PT', 'B_plus_DIRA_OWNPV',
             'B_plus_MCORR', 'B_plus_MCORRERR',
             'B_plus_LK_LTIME', 'B_plus_ENDVERTEX_CHI2', 'B_plus_DOCA_Jpsi_Mu_plus', 'nTracks',
             'Jpsi_M', 'Jpsi_P', 'Jpsi_PT', 
             'Mu_plus_P', 'Mu_plus_PT', 'Mu_plus_PIDmu', 'Mu_plus_IPCHI2_OWNPV', 
             'Mu_1_P', 'Mu_1_PT', 'Mu_1_PIDmu', 
             'Mu_2_P', 'Mu_2_PT', 'Mu_2_PIDmu',
             'Mu_1_PIDmu_corr', 'Mu_2_PIDmu_corr', 'Mu_plus_PIDmu_corr',
           ]

print('Found', len(datafiles), 'files. Will now select and frameify')

cut_dict = {
'l0'   :
    '(Jpsi_L0MuonDecision_TOS | Jpsi_L0DiMuonDecision_TOS)',
'hlt1' :
    '(B_plus_Hlt1TwoTrackMVADecision_TOS | B_plus_Hlt1TrackMuonMVADecision_TOS | B_plus_Hlt1TrackMVADecision_TOS | B_plus_Hlt1TrackMuonDecision_TOS | B_plus_Hlt1TrackAllL0Decision_TOS | B_plus_Hlt1SingleMuonDecision_TOS)',
'hlt2' :
    '(B_plus_Hlt2Topo2BodyDecision_TOS | B_plus_Hlt2Topo3BodyDecision_TOS | B_plus_Hlt2Topo4BodyDecision_TOS | B_plus_Hlt2TopoMu2BodyDecision_TOS | B_plus_Hlt2TopoMu3BodyDecision_TOS | B_plus_Hlt2TopoMu4BodyDecision_TOS | B_plus_Hlt2SingleMuonDecision_TOS | B_plus_Hlt2Topo2BodyBBDTDecision_TOS | B_plus_Hlt2Topo3BodyBBDTDecision_TOS | B_plus_Hlt2Topo4BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu4BodyBBDTDecision_TOS)' ,

# invert muon selection
'detector_conditions' :
    '(Mu_plus_isMuon==False) & (Mu_plus_PIDmu<0) & (Mu_plus_PIDK<0)', # & (Mu_plus_hasMuon) & (Mu_plus_NShared==0) & (Mu_plus_InMuonAcc>0)',

#'MuonID':
  #'(Mu_plus_ProbNNpi*(1-Mu_plus_ProbNNk>0.8)',

'chi2_conditions':
    '(Mu_plus_IPCHI2_OWNPV>4.8)',# & ( (Mu_1_IPCHI2_OWNPV>9) | (Mu_2_IPCHI2_OWNPV>9) ) & (Mu_1_PIDmu>0) & (Mu_2_PIDmu>0)',

'kinematic_conditions' :
  '(Mu_plus_PT>750) & (Mu_plus_P>1e3) & (Jpsi_PT>2000) & (Jpsi_M>3040) & (Jpsi_M<3150) & ((Jpsi_ENDVERTEX_CHI2/Jpsi_ENDVERTEX_NDOF)<9) & (Mu_1_PT>900) & (Mu_2_PT>900)',

'Mother B hadron':
    '(B_plus_DOCA_Jpsi_Mu_plus<0.15) & (B_plus_ISOLATION_BDT<0.2) & ((B_plus_ENDVERTEX_Z-B_plus_PV_Z)>0) & (B_plus_ENDVERTEX_CHI2<25)',

'B_mass':
    '(B_plus_M>6000) & (B_plus_M<6600)',

}


cuts = ' & '.join( cut_dict.values() )

#print(cuts)

test = uproot.open( datatrees[0] )

import pandas as pd
#df = pd.DataFrame()

import matplotlib.pyplot as plt

ntrees = len(datatrees)

for i, tree in tqdm(enumerate(datatrees),total=ntrees,desc='Progress'):
    with uproot.open(tree) as tr:
        df = tr.arrays( branches, cut=cuts, aliases=aliases, library='pd' )
        df.to_pickle(f'dfs/{args.year}/df_temp_{i}.pkl')

#for i, batch in enumerate(tqdm(uproot.iterate( datatrees, branches, cuts, aliases=aliases, library='pd' ), total=len(datatrees), desc='Progress')):
  ##df = df.append(batch)
  #batch.to_pickle(f'dfs/df_data_{args.year}_{i}.pkl')

df = pd.concat( [pd.read_pickle(f'dfs/{args.year}/df_temp_{i}.pkl') for i in range(ntrees)] ) 
print(df)
if data:
    df.to_pickle(f'df_data_{args.year}.pkl')
else:
    df.to_pickle(f'df_sim_{args.year}.pkl')
os.system('rm -f dfs/df_temp_*.pkl')

fig, ax = plt.subplots(1,2,figsize=(12,4))
ax[0].hist( df['B_plus_M'], bins=100, range=(6000,6600) )
ax[0].set_xlabel('B_plus_M')
ax[1].hist( df['Jpsi_M'], bins=100, range=(3000,3200) )
ax[1].set_xlabel('Jpsi_M')
fig.tight_layout()
fig.savefig(f'plots/df_check_{args.year}.pdf')
