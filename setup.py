import os
from setuptools import setup, find_packages

# Utility function to read the README file
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "Bc2D0MuNuX",
    version = "0.0.1",
    author = "Blaise Delaney",
    author_email = "blaise.delaney@cern.ch",
    description = ("Package for analysis preservation"),
    license = "MIT",
    url = "https://gitlab.cern.ch/bldelane/InclusiveVubVcb",
    packages=find_packages(),
    long_description=read('README.md'),
)