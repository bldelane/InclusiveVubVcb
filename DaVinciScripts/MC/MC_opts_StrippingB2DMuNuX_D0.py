#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
#DaVinci Versions:
#
# Runs I,II Stream: Semileptonic (DST)
#
#    -stripping 21r1p1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21r0p1 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#    -stripping 24r1 (estripping of 2015 MC): DV v38r1p6
#    -stripping 28r1 (restripping for 2016 MC): DV v41r4p4, DST
#    -stripping 29 (concurrent stripping of 2017 pp data): DV v42r6p1, DST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst

#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

#MC needs smear
from Configurables import TrackSmearState as SMEAR
smear = SMEAR('StateSmear')



#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, years
run_year = '2011' #2011, 2012, 2015, 2016, 2017

line =  'B2DMuNuX_D0' #run I (available for run II too)


# *NOTE*: check the input files for the correct sim version
# *NOTE*: sim versions
#               - 2011 B+ mode: 08a
#               - 2012 B+ mode: 08a
#               - 2012 Bc (signal) mode: 08e

sim = '08a' #08{a,e}, 09{b}

#set to -1 for full run at submission
evts = -1


#**********SELECT SL TOOLS B MASS HYPOTHESIS

#----------------------------------------------------



#----------------------------------------------------
#Clean up previous stripping banks
#Remove Decisions from the MC production
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#----------------------------------------------------


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
    

if run_year=='2017': strip='stripping29'
if run_year=='2016': strip='stripping28r1' #safe to use on MC
if run_year=='2015': strip='stripping24r1' #safe to use on MC
if run_year=='2012': strip='stripping21r0p1'
if run_year=='2011': strip='stripping21r1p1'

config=strippingConfiguration(strip)
archive=strippingArchive(strip)
streams=buildStreams(stripping=config,archive=archive)


MyStream = StrippingStream("MyStream")
MyLines = ['Stripping'+line]
    
for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] )
    
from Configurables import ProcStatusCheck
filterBadEvents=ProcStatusCheck()
    
# HDRLocation = "SomeNonExistingLocation",
sc=StrippingConf( Streams= [ MyStream ],
                  MaxCandidates = 2000,
                  AcceptBadEvents = False,
                  BadEventSelection = filterBadEvents )




#----------------------------------------------------
#tupletools
#note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daugher final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('MCTuple_B2D0MuNuX_D02KPi') #name of tuple withing the root file
dtt.ToolList = []
dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'] 

#add PV array
TTG = dtt.addTupleTool('TupleToolGeometry')
TTG.FillMultiPV = True

track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
track_tool.Verbose = True
dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
dtt.addTupleTool('TupleToolPrimaries') #PV info 
track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
track_ils.Verbose = True


TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
TT.VerboseL0 = True
TT.VerboseHlt1 = True
TT.VerboseHlt2 = True

dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 

##################################################################################################
# seems to cause MC to crash, omit for MC opts file                                              #
# RICH PID not available on run II                                                               #
# if run_year=='2011' or run_year=='2012':                                                       #
#    dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi) #
##################################################################################################


#----------------------------------------------------
#MC truth
dtt.addTupleTool("TupleToolMCTruth")
dtt.addTupleTool("TupleToolMCBackgroundInfo")
#----------------------------------------------------


#restrip using line of interest from data and corresponding DV version 
#note the path starting @ Phys
dtt.Inputs = ['Phys/B2DMuNuX_D0/Particles']

#branches and naming for dauughter article(s)
dtt.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0

dtt.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC', 
                 'Pi_plus'   :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC', 
                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})


# ***** Works only on DST (all used MC are DST)  ****************
dtt.addTupleTool('TupleToolConeIsolation')
dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits
#****************************************************************


from Configurables import TupleToolSLTools
dtt.ReFitPVs = True
SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools')

#select properly Bmass

#reconstruct B+ and Bc MC with Bc mass hypothesis to study the corrected mass error in the hope of 
#having B+ candidates with worse error on the corrected mass 
SLLtool.Bmass = 6273.7#5279.3#6273.7

SLLtool.VertexCov = True
SLLtool.MomCov = True

#----------------------------------------------------



#----------------------------------------------------
#import DOCA tools from SL tools
from Configurables import TupleToolSLTruth

B_plus_docas = dtt.B_plus.addTupleTool("TupleToolDocas")
B_plus_docas.Name = ["D0_Mu_plus"]
B_plus_docas.Location1 =[ '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC' ] #D0 tagged 
B_plus_docas.Location2 =[ '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC' ] #muon tagged 

#----------------------------------------------------

#----------------------------------------------------


#----------------------------------------------------
#configure Isolation Tools
from Configurables import TupleToolApplyIsolation
dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #cloned from RLc repo
dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
#----------------------------------------------------


#----------------------------------------------------
# TRIGGER LINES 
# list of trigger lines on the B

#RUN II: add Hlt1TwoTrackMVA 

trigListL0 = [ "L0HadronDecision",
               "L0MuonDecision", 
               "L0DiMuonDecision"
                ]

trigListHlt1 = [ "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1DiMuonLowMassDecision",
                 "Hlt1DiMuonHighMassDecision"
                 ]

trigListHlt2 = [    "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2

B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
B_TisTos.VerboseL0 = True
B_TisTos.VerboseHlt1 = True
B_TisTos.VerboseHlt2 = True
B_TisTos.TriggerList = trigListAll 

D_TisTos = dtt.D0.addTupleTool('TupleToolTISTOS')
D_TisTos.VerboseL0 = True
D_TisTos.VerboseHlt1 = True
D_TisTos.VerboseHlt2 = True
D_TisTos.TriggerList = trigListAll 

K_TisTos = dtt.K_minus.addTupleTool('TupleToolTISTOS')
K_TisTos.VerboseL0 = True
K_TisTos.VerboseHlt1 = True
K_TisTos.VerboseHlt2 = True
K_TisTos.TriggerList = trigListAll 

Pi_TisTos = dtt.Pi_plus.addTupleTool('TupleToolTISTOS')
Pi_TisTos.VerboseL0 = True
Pi_TisTos.VerboseHlt1 = True
Pi_TisTos.VerboseHlt2 = True
Pi_TisTos.TriggerList = trigListAll 

Mu_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
Mu_TisTos.VerboseL0 = True
Mu_TisTos.VerboseHlt1 = True
Mu_TisTos.VerboseHlt2 = True
Mu_TisTos.TriggerList = trigListAll
#----------------------------------------------------


#----------------------------------------------------
#LoKis
B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
B_plus_preamble = []
B_plus_hybrid.Preambulo = B_plus_preamble 

B_plus_hybrid.Variables = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
	     "VCHI2"  : "VFASPF(VCHI2/VDOF)",
	     "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()", #proper life time (mm)
         "DTF_CTAU" : "DTF_CTAU ( 0 , True , strings('D0') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 0 , True, strings('D0') )"
    }

D0_hybrid = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/D0") 
D0_preamble = []
D0_hybrid.Preambulo = D0_preamble 

D0_hybrid.Variables = {
         "DTF_CTAU" : "DTF_CTAU ( 1 , True, strings('D0') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 1 , True, strings('D0') )"
    }

Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus") 
Mu_plus_preamble = []
Mu_plus_hybrid.Preambulo = Mu_plus_preamble 

Mu_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Pi_plus_hybrid = dtt.Pi_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_plus") 
Pi_plus_preamble = []
Pi_plus_hybrid.Preambulo = Pi_plus_preamble 

Pi_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


K_minus_hybrid = dtt.K_minus.addTupleTool("LoKi::Hybrid::TupleTool/K_minus_plus") 
K_minus_preamble = []
K_minus_hybrid.Preambulo = K_minus_preamble 

K_minus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)"#min chi2 distance relative to PV (3 sigma, I think)
        }
#----------------------------------------------------


#----------------------------------------------------
#implement DecayTreeFitter

dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV of origin
dtt.B_plus.ConsB.constrainToOriginVertex = True #take into account the neutrino
dtt.B_plus.ConsB.Verbose = True
dtt.B_plus.ConsB.daughtersToConstrain = ['D0'] #constrain K Pi to D0
dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks

#----------------------------------------------------


DaVinci().appendToMainSequence( [ eventNodeKiller ] )   
DaVinci().InputType = 'DST'
DaVinci().DataType = run_year
DaVinci().TupleFile = 'B2D0MuNu.root'
#DaVinci().HistogramFile = "DVHistos.root"
DaVinci().UserAlgorithms += [smear]
DaVinci().UserAlgorithms += [dtt]
DaVinci().PrintFreq = 1000


DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax =  evts
DaVinci().appendToMainSequence( [ sc.sequence() ] )

#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 

if sim=="sim08a" and run_year=='2011':
    DaVinci().DDDBtag = 'Sim08-20130503' 
    DaVinci().CondDBtag = 'Sim08-20130503-vc-md100'

if sim=="sim08a" and run_year=='2012':
    DaVinci().DDDBtag= 'Sim08-20130503-1' 
    DaVinci().CondDBtag = 'Sim08-20130503-1-vc-md100'

if sim=="sim08e" and run_year=='2011':
    DaVinci().DDDBtag = 'dddb-20130929' 
    DaVinci().CondDBtag = 'sim-20130522-vc-md100'

if sim=="sim08e" and run_year=='2012':
    DaVinci().DDDBtag = 'dddb-20130929-1' 
    DaVinci().CondDBtag = 'sim-20130522-1-vc-md100'

if sim=="sim09b" and run_year=='2016':
    DaVinci().DDDBtag = 'dddb-20150724' 
    DaVinci().CondDBtag='sim-20161124-2-vc-md100'

