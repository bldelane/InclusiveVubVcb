#Ganga script for submission to run for NTuples for MC 
#
#Make sure the GaudiExec() directory is the correct lb-checked out environment
#with DV, Analysis (correct compatible version), SLTools and Isolation Tools with 
#weights.xm fie
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch


#--- MC --- Bc->D0MuNu Mode:
#DaVinci Versions:
#
# Runs I,II Stream: Semileptonic (DST)
#
#    -stripping 21r1p1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21r0p1 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#    -stripping 24r1 (estripping of 2015 MC): DV v38r1p6->v38r1p7
#    -stripping 28r1 (restripping for 2016 MC): DV v41r4p4, DST -> v41r4p5
#    -stripping 29 (concurrent stripping of 2017 pp data): DV v42r6p1, DST
#

#--- MC ---Bc->JpsiMuNu Mode:
#DaVinci Versions:
#
# Run I, Stream: Dimuon (DST)
#
#    -stripping 21r1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#
# Run II, Stream: Dimuon (DST)
#
#    -stripping 24 (estripping of 2015 data): DV v38r1p3
#
# Run II, Stream: Leptonic (DST) (same cuts as Dimuon)
#
#    - !!!--> stripping 28 (restripping for 2016 data): DV v41r4p3   *** mDST
#    - !!!--> stripping 29 (concurrent stripping of 2017 pp data): DV v42r5p1   *** mDST

import sys
import os

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#----------------
#Data for D0 Mode
# -> Local in temp/:
#j.application.readInputData('/afs/cern.ch/work/b/bldelane/private/Data/D0temp/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py')

#MC11 - B+->D~0(->K+pi-)MuNu channel
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/BMode/MC_2011_12873002_Beam3500GeV2011MagDownNu2Pythia8_Sim08a_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py',
#'/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/BMode/MC_2011_12873002_Beam3500GeV2011MagUpNu2Pythia8_Sim08a_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py']

#MC12
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2012/BMode/MC_2012_12873002_Beam4000GeV2012MagUpNu2.5Pythia8_Sim08a_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py','/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2012/BMode/MC_2012_12873002_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08a_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py']

#MC12 Signal Mode Bc->D0MuNu
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2012/BMode/SigMode/MC_2012_14573002_Beam4000GeV2012MagDownNu2.5BcVegPy_Sim08e_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py','/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2012/BMode/SigMode/MC_2012_14573002_Beam4000GeV2012MagUpNu2.5BcVegPy_Sim08e_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py']

#MC16
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2016/BMode/MC_2016_12873441_Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping26NoPrescalingFlagged_ALLSTREAMS.DST.py',
#'/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2016/BMode/MC_2016_12873441_Beam6500GeV2016MagUpNu1.625nsPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping26NoPrescalingFlagged_ALLSTREAMS.DST.py']






#-------------------
#Data for J/Psi Mode
# -> Local in temp/:
#j.application.readInputData('/afs/cern.ch/work/b/bldelane/private/Data/temp/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21r1_DIMUON.DST.py')

#MC11 - Bc->JpsiMuNu sim08a
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/JpsiMode/MC_2011_14543005_Beam3500GeV2011MagDownNu2BcVegPy_Sim08a_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py','/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/JpsiMode/MC_2011_14543005_Beam3500GeV2011MagUpNu2BcVegPy_Sim08a_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py']

#MC11 - Bc->JpsiMuNu sim08e
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/JpsiMode/MC_2011_14543010_Beam3500GeV2011MagDownNu2BcVegPy_Sim08e_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py','/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2011/JpsiMode/MC_2011_14543010_Beam3500GeV2011MagUpNu2BcVegPy_Sim08e_Digi13_Trig0x40760037_Reco14a_Stripping20r1NoPrescalingFlagged_ALLSTREAMS.DST.py']

#MC16
#DST=['/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2016/JpsiMode/MC_2016_14543009_Beam6500GeV2016MagDownNu1.625nsBcVegPyPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping26NoPrescalingFlagged_ALLSTREAMS.DST.py','/var/pcfst/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2016/JpsiMode/MC_2016_14543009_Beam6500GeV2016MagUpNu1.625nsBcVegPyPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping26NoPrescalingFlagged_ALLSTREAMS.DST.py']




#DATA
#DST=['DATAfiles/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21r1_DIMUON.DST.py',
#     'DATAfiles/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21r1_DIMUON.DST.py']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




#Option scripts
D0mode_opts = '/MC/MC_opts_StrippingB2DMuNuX_D0.py'
Jpsimode_opts = '/MC/MC_opts_StrippingBc2JPsiMu.py'

for i in range(len(DST)):
    
    polarity = str(DST[i])
    print '\n***********************************************************************************************************************************'
    print 'Sumitting \n%s'%polarity 
    print '***********************************************************************************************************************************\n'
    
    #j = Job(name='Bc2JpsiMuNu')
    j = Job(name='B2hMuNu')
    j.comment='%s'%polarity
    
    #set application= lb-checked out DV environment
    myApp = GaudiExec()
    user_release_area = os.path.expandvars('$HOME') + '/private'
    
    #--------------------------------------------------------------------
    # ***** CHANGE DV ENVIRONMENT TO BUILD FOR DIFFERENT STRIPPINGS *****
    
    #DaVinciDev_v39r1p1 : stripping {21r1(0)(p1), 21} 
    #DaVinciDev_v38r1p6->v38r1p7 : stripping 24r1 
    #DaVinciDev_v41r4p4->v41r4p5 : stripping 28r1 
    #DaVinciDev_v42r6p1 : stripping 29 #should update to v29r2 
    

    # *************************************************
    # *NOTE* must select the appropriate DV version
    LocalDV = '%s/DaVinci_DevX/DaVinciDev_v39r1p1' % user_release_area
    myApp.directory = '%s/DaVinci_DevX/DaVinciDev_v39r1p1' % user_release_area
    # *************************************************
    
    j.application = myApp
    
    # ***** select opts file for decay mode *********** 
    cwd = os.getcwd() #current working directory
    
    # *NOTE*: choose decay mode opts file
    #j.application.options = [cwd+D0mode_opts] 
    j.application.options = [cwd+Jpsimode_opts] 
    
    # *NOTE* must select the appropriate build platform
    # to match the DV build
    j.application.platform='x86_64-slc6-gcc49-opt'

    # *NOTE*: GaudiExec object to auto fect DB tags for MC 
    # see documentation: help(GaudiExec) on ganga prompt
    #j.application.autoDBtags = True
    # *************************************************
    #--------------------------------------------------------------------
    
    
    #import weights.xml for BDT in TupleToolIsolationTools.*
    j.inputfiles=[LocalFile(LocalDV+'/Phys/DecayTreeTuple/src/weights.xml')]
    
    
    #run on dataset (mag up and down)
    
    #-------------------------------------------
    #*NOTE*----->j.application.readInputData(polarity)
    #-------------------------------------------
    
    #configure output
    #j.outputfiles = [LocalFile('*.root')]
    j.outputfiles = [LocalFile('*.root')]
    
    # *NOTE* = maxFiles set 1 for test on ganga with local backend
    j.backend = Dirac() #Dirac()
    j.splitter = SplitByFiles(filesPerJob=10, maxFiles=-1, ignoremissing = True)
    
    #-------------------------------------------
    #*NOTE*----->j.submit()
    #-------------------------------------------

