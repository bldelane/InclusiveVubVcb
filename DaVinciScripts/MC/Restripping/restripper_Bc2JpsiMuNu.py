#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
#DaVinci Versions:
#
# Run I, Stream: Dimuon (DST)
#
#    -stripping 21r1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#
# Run II, Stream: Dimuon (DST)
#
#    -stripping 24 (estripping of 2015 data): DV v38r1p3
#
# Run II, Stream: Leptonic (DST) (same cuts as Dimuon)
#
#    - !!!--> stripping 28 (restripping for 2016 data): DV v41r4p3   *** mDST
#    - !!!--> stripping 29 (concurrent stripping of 2017 pp data): DV v42r5p1   *** mDST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst

#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
#from PhysConf.Filters import LoKi_Filters
#from GaudiConf import IOHelper
#from LoKiPhys.decorators import *
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,stripDSTStreamConf,stripDSTElements)


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, year
run_year = '2012' #2011, 2012, 2015, 2016, 2017

line = 'Bc2JpsiMuLine' #run I (available for  II too)

sim = 'sim08e' #sim08{a,e}, sim09{b}

#set to -1 for full run at submission
evts = 1000

#*NOTE*: change based on polarity
# 1-to-1 correspondence mag polarity and outTuple
suffix ='MC_2012_14543006_MagDown_Sim08eFlagged'
#----------------------------------------------------

#Chris recomments doing this 
# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }


#----------------------------------------------------
#Clean up previous stripping banks
#Remove Decisions from the MC production
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#----------------------------------------------------


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
    
if run_year=='2017': strip='stripping29r2'
if run_year=='2016': strip='stripping28r1' #safe to use on MC
if run_year=='2015': strip='stripping24r1' #safe to use on MC
if run_year=='2012': strip='stripping21'
if run_year=='2011': strip='stripping21r1'

config=strippingConfiguration(strip)
archive=strippingArchive(strip)
streams=buildStreams(stripping=config,archive=archive)


MyStream = StrippingStream("MyStream")
MyLines = ['Stripping'+line]
    
for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] )
    
from Configurables import ProcStatusCheck
filterBadEvents=ProcStatusCheck()
    
# HDRLocation = "SomeNonExistingLocation",
sc=StrippingConf( Streams= [ MyStream ],
                  MaxCandidates = 2000,
                  AcceptBadEvents = False,
                  BadEventSelection = filterBadEvents )



# need to pack the info in the DST. If not done, this causes issues in the vertex TES
#------------------------------------------------------------------------------------
#
# Configuration of SelDSTWriter
#
enablePacking = True

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False)
    }





dstWriter = SelDSTWriter(	"MyDSTWriter",
				StreamConf = SelDSTWriterConf,
				MicroDSTElements = SelDSTWriterElements,
				OutputFileSuffix = suffix,
				SelectionSequences = sc.activeStreams()
				)


DaVinci().appendToMainSequence( [ eventNodeKiller ] )   
DaVinci().InputType = 'DST'
DaVinci().DataType = run_year
DaVinci().HistogramFile = '%s.root' %suffix
DaVinci().TupleFile = 'Bc2JpsiMuNu.root'
#DaVinci().HistogramFile = "DVHistos.root"
DaVinci().PrintFreq = 1000


DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax =  evts
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 

if sim=="sim08a" and run_year=='2011':
    DaVinci().DDDBtag = 'Sim08-20130503' 
    DaVinci().CondDBtag = 'Sim08-20130503-vc-md100'

if sim=="sim08a" and run_year=='2012':
    DaVinci().DDDBtag= 'Sim08-20130503-1' 
    DaVinci().CondDBtag = 'Sim08-20130503-1-vc-md100'

if sim=="sim08e" and run_year=='2011':
    DaVinci().DDDBtag = 'dddb-20130929' 
    DaVinci().CondDBtag = 'sim-20130522-vc-md100'

if sim=="sim08e" and run_year=='2012':
    DaVinci().DDDBtag = 'dddb-20130929-1' 
    DaVinci().CondDBtag = 'sim-20130522-1-vc-md100'

if sim=="sim09b" and run_year=='2016':
    DaVinci().DDDBtag = 'dddb-20150724' 
    DaVinci().CondDBtag='sim-20161124-2-vc-md100'

