#------------------------------
#  Restripper submission file
#------------------------------


###########################################################
#*NOTE*: make sure to launch ganga with slc6-48 to allow it
# to pick up the correct gcc version for the build
###########################################################


#----------------------------------------------------------
#*NOTE*: change suffix in restripping opts file accordingly
#----------------------------------------------------------




#-------------------------------------------------------------------------------------------------------------------------------------------------
# jpsi mode MC
#-------------

##################################################################################################
# Note: use only 14543010 and 14543006 (note the cuts of dilepton in acceptance reasonable, so can
# combine these samples)
##################################################################################################



# *NOTE*: following George's procedure: directly query the BK providing the evt+std location using
# Dirac Portal consulting by 'simulation condition' instead of 'event type'

#2011
#polarity='MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST'
#polarity='MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST'


#2012
#polarity='MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543010/ALLSTREAMS.DST'
#polarity='MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543010/ALLSTREAMS.DST'


#polarity='MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543006/ALLSTREAMS.DST'
polarity='MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543006/ALLSTREAMS.DST'
#-------------------------------------------------------------------------------------------------------------------------------------------------




#Job config
#----------
print '---------------------------------------------'
print '***CAUTION***: make sure to select slc6_gcc48'
print '---------------------------------------------'

#since not using SL at restripping stage: just use DV out of the box
j = Job(application=DaVinci(version='v36r1p5'), name='restrip_B2hMuNu')
j.comment='%s'%polarity


#options: restripper living is the DV user area; should move
j.application.optsfile = [File('restripper_Bc2JpsiMuNu.py')]

# Query DB 
data = BKQuery(polarity).getDataset()
j.inputdata = data


#*NOTE*: change backend and length of submission accordingly
j.backend = Local()
j.outputfiles = [LocalFile('*.dst')] #[DiracFile('*.dst')]
j.splitter = SplitByFiles(filesPerJob=10, maxFiles=1, ignoremissing = True)
j.submit()
