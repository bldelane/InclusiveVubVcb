#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
#DaVinci Versions:
#
# Run I, Stream: Dimuon (DST)
#
#    -stripping 21r1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#
# Run II, Stream: Dimuon (DST)
#
#    -stripping 24 (estripping of 2015 data): DV v38r1p3
#
# Run II, Stream: Leptonic (DST) (same cuts as Dimuon)
#
#    - !!!--> stripping 28 (restripping for 2016 data): DV v41r4p3   *** mDST
#    - !!!--> stripping 29 (concurrent stripping of 2017 pp data): DV v42r5p1   *** mDST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst


#--------------------------------------------------------------------------------------------------
#*NOTE*: opts file for ganga post-restrip;
#Instructions: in ganga run '%ganga options_file.py' to execute the opts file without shutting down
#When submitting need to speficy:
#    - input data (by querying the ouput LFN of job, see twiki)
#    - submit
#    - probably set comment == output_generating_job.comment
#--------------------------------------------------------------------------------------------------


#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

#MC needs smear
from Configurables import TrackSmearState as SMEAR
smear = SMEAR('StateSmear')

# Use the local input data
IOHelper().inputFiles([
], clear=True)


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, year
run_year = '2011' #2011, 2012, 2015, 2016, 2017

line = 'Bc2JpsiMuLine' #run I (available for  II too)

sim = 'sim08e' #sim08{a,e}, sim09{b}

#set to -1 for full run at submission
evts = -1
#----------------------------------------------------


'''
#----------------------------------------------------
#Clean up previous stripping banks
#Remove Decisions from the MC production
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
#----------------------------------------------------


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
    
if run_year=='2017': strip='stripping29r2'
if run_year=='2016': strip='stripping28r1' #safe to use on MC
if run_year=='2015': strip='stripping24r1' #safe to use on MC
if run_year=='2012': strip='stripping21'
if run_year=='2011': strip='stripping21r1'

config=strippingConfiguration(strip)
archive=strippingArchive(strip)
streams=buildStreams(stripping=config,archive=archive)


MyStream = StrippingStream("MyStream")
MyLines = ['Stripping'+line]
    
for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] )
    
from Configurables import ProcStatusCheck
filterBadEvents=ProcStatusCheck()
    
# HDRLocation = "SomeNonExistingLocation",
sc=StrippingConf( Streams= [ MyStream ],
                  MaxCandidates = 2000,
                  AcceptBadEvents = False,
                  BadEventSelection = filterBadEvents )
'''

#look at events only passing relevant stripping line, optimise CPU
fltrs = LoKi_Filters (
            STRIP_Code = "HLT_PASS_RE('StrippingBc2JpsiMuLineDecision')" #check
            )
DaVinci().EventPreFilters = fltrs.filters('Filters')


#----------------------------------------------------
#tupletools
#note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daugher final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('TupleBc2JpsiMuNu_Jpsi2MuMu') #name of tuple withing the root file
dtt.ToolList = []
dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'] 

#add PV array
TTG = dtt.addTupleTool('TupleToolGeometry')
TTG.FillMultiPV = True


track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
track_tool.Verbose = True
dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
dtt.addTupleTool('TupleToolPrimaries') #PV info 
track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
track_ils.Verbose = True


TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
TT.VerboseL0 = True
TT.VerboseHlt1 = True
TT.VerboseHlt2 = True

dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 


#----------------------------------------------------
#MC truth
dtt.addTupleTool("TupleToolMCTruth")
dtt.addTupleTool("TupleToolMCBackgroundInfo")
#----------------------------------------------------


#restrip using line of interest from data and corresponding DV version 
#note the path starting @ Phys

#*NOTE*: prepended 'MyStream' after exploring with Bender
dtt.Inputs = ['/Event/MyStream/Phys/Bc2JpsiMuLine/Particles']

#branches and naming for daughter article(s)
dtt.Decay = '[B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged Jpsi
dtt.addBranches({'B_plus'           :  '[B_c+ -> (J/psi(1S) -> mu+ mu-) mu+]CC',
                 'Jpsi'             :  '[B_c+ -> ^(J/psi(1S) -> mu+ mu-) mu+]CC',
                 'Mu_plus_d'        :  '[B_c+ -> (J/psi(1S) -> ^mu+ mu-) mu+]CC', 
                 'Mu_minus_d'       :  '[B_c+ -> (J/psi(1S) -> mu+ ^mu-) mu+]CC', 
                 'Mu_plus'          :  '[B_c+ -> (J/psi(1S) -> mu+ mu-) ^mu+]CC'}) 


# ***** Not working on Run II  ****************
#valid of DST
#if run_year=='2011' or run_year=='2012' or run_year=='2015': 
#    dtt.addTupleTool('TupleToolConeIsolation')
    
#RICH PID not available in run II 
if run_year=='2011' or run_year=='2012': 
    dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi)
#**************************************************


# ***** Not working on s29 - potential patch: use DV v42r6p1 (Alison's suggestion) ****************
if run_year=='2011' or run_year=='2012': 
    dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits
    #as of s24r1 does not seem to work, possibly due to MDST; tested on recommended latest DV
    dtt.addTupleTool('TupleToolConeIsolation')
#******************************************



#----------------------------------------------------
#SL Tools
from Configurables import TupleToolSLTools
dtt.ReFitPVs = True
SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools')

#select correct B{c,+} mass
SLLtool.Bmass = 6273.7
SLLtool.VertexCov = True
SLLtool.MomCov = True
#----------------------------------------------------

#----------------------------------------------------
#configure Isolation Tools
from Configurables import TupleToolApplyIsolation
dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #added to input sandbox in ganga job file
dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
#----------------------------------------------------

#----------------------------------------------------
#import DOCA tools from SL tools
from Configurables import TupleToolSLTruth

B_plus_docas = dtt.B_plus.addTupleTool("TupleToolDocas")
B_plus_docas.Name = ["Jpsi_Mu_plus"]
B_plus_docas.Location1 =[ '[B_c+ -> ^(J/psi(1S) -> mu+ mu-) mu+]CC' ] #jpsi tagged 
B_plus_docas.Location2 =[ '[B_c+ -> (J/psi(1S) -> mu+ mu-) ^mu+]CC' ] #bachelor muon tagged 

#----------------------------------------------------

#----------------------------------------------------
# TRIGGER LINES 
# list of trigger lines on the B

#RUN II: add Hlt1TwoTrackMVA 
#added dimuon lines from R(Jpsi)

trigListL0 = [ "L0HadronDecision",
               "L0MuonDecision", 
               "L0DiMuonDecision"
                ]

trigListHlt1 = [ "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1DiMuonLowMassDecision",
                 "Hlt1DiMuonHighMassDecision"
                 ]

trigListHlt2 = [    "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision",
                    "Hlt2DiMuonDetachedJPsiDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2

B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
B_TisTos.VerboseL0 = True
B_TisTos.VerboseHlt1 = True
B_TisTos.VerboseHlt2 = True
B_TisTos.TriggerList = trigListAll 

Jpsi_TisTos = dtt.Jpsi.addTupleTool('TupleToolTISTOS')
Jpsi_TisTos.VerboseL0 = True
Jpsi_TisTos.VerboseHlt1 = True
Jpsi_TisTos.VerboseHlt2 = True
Jpsi_TisTos.TriggerList = trigListAll 

Mu_plus_d_TisTos = dtt.Mu_plus_d.addTupleTool('TupleToolTISTOS')
Mu_plus_d_TisTos.VerboseL0 = True
Mu_plus_d_TisTos.VerboseHlt1 = True
Mu_plus_d_TisTos.VerboseHlt2 = True
Mu_plus_d_TisTos.TriggerList = trigListAll 

Mu_minus_d_TisTos = dtt.Mu_minus_d.addTupleTool('TupleToolTISTOS')
Mu_minus_d_TisTos.VerboseL0 = True
Mu_minus_d_TisTos.VerboseHlt1 = True
Mu_minus_d_TisTos.VerboseHlt2 = True
Mu_minus_d_TisTos.TriggerList = trigListAll 

Mu_plus_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
Mu_plus_TisTos.VerboseL0 = True
Mu_plus_TisTos.VerboseHlt1 = True
Mu_plus_TisTos.VerboseHlt2 = True
Mu_plus_TisTos.TriggerList = trigListAll
#----------------------------------------------------


#----------------------------------------------------
#LoKis
B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
B_plus_preamble = []
B_plus_hybrid.Preambulo = B_plus_preamble 

#*NOTE*: kill DTF as CPU-consuming and not used in Analysis

B_plus_hybrid.Variables = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
	     "VCHI2"  : "VFASPF(VCHI2/VDOF)",
	     "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()", #proper life time (mm)
#         "DTF_CTAU" : "DTF_CTAU ( 0 , True , strings('J/psi(1S)') )",  #Ask Alison how to use this exactly;
#         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 0 , True, strings('J/psi(1S)') )"
    }

Jpsi_hybrid = dtt.Jpsi.addTupleTool("LoKi::Hybrid::TupleTool/Jpsi") 
Jpsi_preamble = []
Jpsi_hybrid.Preambulo = Jpsi_preamble 

Jpsi_hybrid.Variables = {
#         "DTF_CTAU" : "DTF_CTAU ( 1 , True, strings('J/psi(1S)') )",  #Ask Alison how to use this exactly;
#         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 1 , True, strings('J/psi(1S)') )"
    }

Mu_plus_d_hybrid = dtt.Mu_plus_d.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus_d") 
Mu_plus_d_preamble = []
Mu_plus_d_hybrid.Preambulo = Mu_plus_d_preamble 

Mu_plus_d_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Mu_minus_d_hybrid = dtt.Mu_minus_d.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_minus_d") 
Mu_minus_d_preamble = []
Mu_minus_d_hybrid.Preambulo = Mu_minus_d_preamble 

Mu_minus_d_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/Mu_plus") 
Mu_plus_preamble = []
Mu_plus_hybrid.Preambulo = Mu_plus_preamble 

Mu_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)"#min chi2 distance relative to PV (3 sigma, I think)
        }

#---------------------------------------------------


#*NOTE*: remove DTF as it causes the vertex fit to crash, probably due to neutrino 
###----------------------------------------------------
###implement DecayTreeFitter
##
#dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV of origin
#dtt.B_plus.ConsB.constrainToOriginVertex = True #take into account the neutrino
#dtt.B_plus.ConsB.Verbose = True #changed to true
#dtt.B_plus.ConsB.daughtersToConstrain = ['J/psi(1S)'] #constrain K Pi to Jpsi
#dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks
##
###----------------------------------------------------


#DaVinci().appendToMainSequence( [ eventNodeKiller ] )   
DaVinci().InputType = 'DST'
DaVinci().DataType = run_year
DaVinci().TupleFile = 'Bc2JpsiMuNu.root'
#DaVinci().HistogramFile = "DVHistos.root"
DaVinci().UserAlgorithms += [smear]
DaVinci().UserAlgorithms += [dtt]
DaVinci().PrintFreq = 1000


DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax =  evts
#DaVinci().appendToMainSequence( [ sc.sequence() ] )

#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 

if sim=="sim08a" and run_year=='2011':
    DaVinci().DDDBtag = 'Sim08-20130503' 
    DaVinci().CondDBtag = 'Sim08-20130503-vc-md100'

if sim=="sim08a" and run_year=='2012':
    DaVinci().DDDBtag= 'Sim08-20130503-1' 
    DaVinci().CondDBtag = 'Sim08-20130503-1-vc-md100'

if sim=="sim08e" and run_year=='2011':
    DaVinci().DDDBtag = 'dddb-20130929' 
    DaVinci().CondDBtag = 'sim-20130522-vc-md100'

if sim=="sim08e" and run_year=='2012':
    DaVinci().DDDBtag = 'dddb-20130929-1' 
    DaVinci().CondDBtag = 'sim-20130522-1-vc-md100'

if sim=="sim09b" and run_year=='2016':
    DaVinci().DDDBtag = 'dddb-20150724' 
    DaVinci().CondDBtag='sim-20161124-2-vc-md100'
