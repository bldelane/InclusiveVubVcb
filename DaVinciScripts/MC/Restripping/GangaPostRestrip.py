# Ganga script to perform teh tuple production via DV using the restripped dsts
# must define iput data and submit

#--------------------------------------------------------------------------------------------------
#*NOTE*: opts file for ganga post-restrip;
#Instructions: in ganga run '%ganga options_file.py' to execute the opts file without shutting down
#When submitting need to speficy:
#    - input data (by querying the ouput LFN of job, see twiki)
#    - submit
#    - probably set comment == output_generating_job.comment
#--------------------------------------------------------------------------------------------------

Jpsimode_opts = 'MCoptsPostRestrip.py'

j = Job(name='Ntuplelize_postRestrip_B2hMuNu')

#set application= lb-checked out DV environment
myApp = GaudiExec()
user_release_area = os.path.expandvars('$HOME') + '/private'

# *************************************************
# *NOTE* must select the appropriate DV version
LocalDV = '%s/DaVinci_DevX/DaVinciDev_v39r1p1' % user_release_area
myApp.directory = '%s/DaVinci_DevX/DaVinciDev_v39r1p1' % user_release_area
# *************************************************

j.application = myApp

# ***** select opts file for decay mode *********** 
cwd = os.getcwd() #current working directory

j.application.options = [Jpsimode_opts] 

j.application.platform='x86_64-slc6-gcc49-opt'

# *NOTE*: GaudiExec object to auto fect DB tags for MC 
# see documentation: help(GaudiExec) on ganga prompt
#j.application.autoDBtags = True
# *************************************************
#--------------------------------------------------------------------

#import weights.xml for BDT in TupleToolIsolationTools.*
j.inputfiles=[LocalFile(LocalDV+'/Phys/DecayTreeTuple/src/weights.xml')]

#run on dataset (mag up and down)
#-------------------------------------------------
#*NOTE*----->j.application.readInputData(polarity)
#-------------------------------------------------

#configure output
j.outputfiles = [LocalFile('*.root')]

# *NOTE* = maxFiles set 1 for test on ganga with local backend
j.backend = Dirac() #Dirac()
j.splitter = SplitByFiles(filesPerJob=10, maxFiles=-1, ignoremissing = True)

#-------------------------------------------
#*NOTE*----->j.submit()
#-------------------------------------------

