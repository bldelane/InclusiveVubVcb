# Submission logs for bookkeeping for restripped MC 

Submissions details:
```bash
- Dirac backend()
- evnts = -1 
- DiracFile() output 
```

Jobs submitted on 29/09/18 
--------------------------
```bash
303 MC/2011/Beam3500GeV-2011-MagUp-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST
304 MC/2011/Beam3500GeV-2011-MagDown-Nu2-BcVegPy/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/14543010/ALLSTREAMS.DST
305 MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543010/ALLSTREAMS.DST
306 MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543010/ALLSTREAMS.DST
307 MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543010/ALLSTREAMS.DST
308 MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14543006/ALLSTREAMS.DST
```
