# Restripping

Run I MC for Bc->JpsiMuNu requires a decoupling of restripping and generation of tuples. 

- Restripping is implemented using `DVv36r1p5` to match Stripping 21 MC productions. This is a
  necessary as the files are in S21 and S21r1. This is implemented using `Ganga_restripper.py` for
  ganga submission of `restripper_Bc2JpsiMuNu.py`, the dedicated restripping opts file. Note how,
  in addition to restripping+Ntupleization in one file, now `DSTwriter` objects are necessary and
  must implement packing of TES locations. 


- Ntuples are generated with `DVv39r1p1` to match the data Ntuples opts. Use dedicated files
  `GangaPostRestrip.py` for submission of `GangaPostRestrip`.

- The `Ganga_restripper.py` file **queries the Dirac Bookkeeping**, it does not use the locally
  installed `.dst.py` files. __This is different from the other options files used for both data and
  MC.__

- `Ganga_restripper.py` **queries thh `Dirac Backend()` and the output fies are stored as
  `DiracFiles`**. To set the output LFNs as input for the `restripper.py` jobs: 
```python
ds = LHCbDataset()
for sj in j.subjobs.select(status=“completed”):
    lfns = []
    for lfn in sj.backend.getOutputDataLFNs():
        lfns.append('LFN:'+lfn.lfn)
    ds.extend(lfns)
new_job.inputdata = ds
```

  *This is a standalone submission pipeline, all tasks related to the Ntuple production of Run I MC
  for the Jpsi mode are availble in this sub-repository*.



