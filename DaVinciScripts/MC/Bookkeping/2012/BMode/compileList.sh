#!/usr/bin/env zsh
echo "Please check that you have fun the command SetupProject Gauss"

MCPATH=/r01/lhcb/delaney/Bc2hMuNuX/MC/MCfiles/SelectedMC/2012/BMode/

echo $MCPATH

totfiles=$(ls -lt $MCPATH*.py | wc -l)
counter=1

for file in $MCPATH*.py
do
    echo "Files $counter of $totfiles"
    name=$(echo $file)
    echo "file name: $name"
    year=$(echo $file | grep -o "201." | head -1)
    events=$(grep -m 1 -i "events" $file | cut -c 19-)
    dddb=$(grep -m 1 -i "DDDB" $file | cut -c 6-)
    condb=$(grep -m 1 -i "CONDDB" $file | cut -c 6-)
    simcond=$(grep -i "sim0." $file | cut -c 6-)
    isFlagged=$(echo $file | grep -o "Flagged" | head -1)
    isFiltered=$(echo $file | grep -o "Filtered" | head -1)
    ReDecay=$(echo $file | grep -o -E -i "Sim...ReDecay.." )
    DV=$(grep -A1 "DaVinci" $file | head -2 | cut -c 6-) 
    dkevt=$(grep "events" $file | awk '{print $2;}')
    echo "dec evt $dkevt"
    
    echo "##### start of file scan #####" >> MClist.log
    echo "File name: $name" >> MClist.log
    echo "Year: $year" >> MClist.log 
    echo "Number of events: $events" >> MClist.log 
    echo "DDDB? $dddb" >> MClist.log 
    echo "CONDDB? $condb" >> MClist.log
    echo "" >> MClist.log
    echo "---start checking for sim cond ---" >> MClist.log
    echo "sim cond: $simcond" >> MClist.log
    echo "---end of check for sim cond ---" >> MClist.log
    echo "" >> MClist.log
    echo "Flagged? $isFlagged" >> MClist.log
    echo "Filtered? $isFiltered" >> MClist.log
    echo "Redecay? $ReDecay" >> MClist.log
    echo "" >> MClist.log
    #echo "" >> MClist.log
    echo "---start checking for DaVinci version ---" >> MClist.log
    echo "DaVinci: $DV" >> MClist.log
    echo "---end of check for DaVinci version ---" >> MClist.log
    echo "" >> MClist.log
    ((counter++))
    #routine to find decay descriptor
    cd $DECFILESROOT
    cd dkfiles/
    grep "$dkevt" *.dec
    test=$(grep -l  $dkevt *.dec) 
    decay=$(grep "Descriptor" $test | cut -c3-)
    echo $test
    echo $decay
    cd $MCPATH
    echo $decay >> MClist.log
    echo "##### end of file scan #####" >> MClist.log
    echo "" >> MClist.log
    echo "" >> MClist.log
    echo ""
done
