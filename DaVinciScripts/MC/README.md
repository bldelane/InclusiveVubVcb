# Scripts for creation of MC NTuples

Options scripts and ganga submission files. *Always test locally with `backend==Local()`
beforehand*. 

- `MC_Ganga_B2hMuNu.py` = ganga submission file for both D0 and Jpsi modes
- `MC_opts_StrippingB2DMuNuX_D0.py` = opts file for Bc->D0(->Kpi)MuNuX
- `MC_opts_StrippingBc2JPsiMu.py` = opts file for Bc->JspiMuNu

### Use: launch ganga, edit files, submit all in one interactive session


```bash
ganga --no-mon #suppress monitoring loop

!vim <files> #edit in vim  

%ganga Ganga_B2hMuNu.py #submit
```

#### Form Factor Choice

To match the SLWG production, pick only simulation (m)dst files generated with Ebert FF.

#### Restripping

All MC submission, except for Run I MC for Jpsi mode, implent restrip and NTuplization. Make sure to
implement a consistent restripping to match the stripping of data. Overall the DV version for
Ntuplizaion should match the conditions on data. 
