#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
# Ganga submission file for Ganga submission for DATA for channels:
#  - Bc->D0(->K pi) Mu Nu X
#  - Bc->D0(->K 3pi) Mu Nu X
#
# Note on stripping lines:
#   - B2DMuNuX_D0Line is used for both Run I and Run II for D0->KPi
#   - B2DMuNuX_D0_K3PiLine is used for Run II only of D0->K3Pi, as not available for Run I as of
#   19/10/18
#   - b2D0MuXK3PiCharmFromBSemiLine
#
# DaVinci Versions:
#
#    Runs I,II Stream: Semileptonic (DST) and Charm (MDST)
#
#    -stripping 21r1p1 (Incremental restripping of 2011 data): DV v36r1p5 (stripping+tuples) {DV
#    v39r1p1 was suggested as the correct version but now deprecated since v36r1p5 is the right
#    version and issues of compatibility between the re-stripping and tuplization (TTGeometry) on
#    run I Jpsi MC}
#    -stripping 21r0p1 (Incremental stripping of 2012 data (autumn 2014)): DV v36r1p5
#    -stripping 24r1 (Full restripping of 2015 data): DV v38r1p3->v38r1p7
#    -stripping 28r1 (Full restripping  for 2016 data): DV v41r4p3->v41r4p5, DST
#    -stripping 29r2 (Full restripping of 2017 pp data): DV v42r5p1->v42r7p3, DST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst
#=======================================================================================================================

#=======================================================================================================================
# TODO Look into SLTRUTH Implementation
# TODO possible error: k3pi-specific tools if loop condition
# TODO AutoDB() Tags in Ganga
# TODO Double-check stripping lines
# TODO test and print IgnoreFilter=True
# TODO check function to implement restrip
# TODO check if mDST: need to use ROOTINTES
# TODO look into restripping extras for s21
# TODO implement doctrings for member functions
# TODO add mother ID tools
# TODO implement RunI-specific configurablesi
# TODO implement MC: {EventNodeKiller as a member function, TES with 'MyStream', ROOTINTES IN  DV}
# TODO finish documenting the class with doctring. Add all members.
# TODO Implement EVent node killer in apply_config function
# TODO check that default DTT tools implemeny booked opys, as technically not part of list
# TODO DTF CTAU doesnt seem to do much; test with a bunch of dst and if substantial improvement,
# remove  for CPU time
#=======================================================================================================================

#=======================================================================================================================
#                                        Class Prototype for Bc2D0MuNuX
#=======================================================================================================================


# import tools and LoKis
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

# imports for restripping, scale, smear, prefilters
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import EventNodeKiller
from Configurables import TrackSmearState as SMEAR
from Configurables import TrackScaleState as SCALER
from Configurables import LoKi__HDRFilter
from Configurables import LoKi__VoidFilter
from Configurables import TupleToolSLTruth
from Configurables import TupleToolSLTools


class DaVinciConfig():
    '''
    Class to configure the NTuples per model

    *Key idea*: label the tuple-specific tools by the corresponding key in the various dicts

    Structure:
        - Top Level Gaudisequence: member of DV UserAlgorithms; per-event run the DTTs; needs IgnoreFilterPass;
        - Decay-specific Gaudisequences: ensemble of tools, each member of the Top Level Sequence.
    '''

    def __init__(self, name, inputFormat, sim, year, maxEvts, lfn=None):
        self.name = name
        self.inputFormat = inputFormat
        self.sim = sim
        self.year = year
        self.maxEvts = maxEvts
        self.lfn = lfn


    __properties__ = {
        """
        Dictionaries of configurables for DaVinci and DecayTreeTuple, DecayTreeFitter and PrintDecayTree
        
        Arguments:
            - DataType: run year; default None
            - Simulation: by default set to False
            - InputType: by default set to 'DST'
            - DecayModes: selection sequences, decay-specific; each will be assigned to a GaudiSequence
            - Streams: labelled by keys of DecayModes
            - StrippingLines: labelled by keys of DecayModes
            - DecayDescriptors : labelled by keys of DecayModes
            - TupleBranches : labelled by keys of DecayModes
            - TTools: TupleTools; included:{default DV tools, common tools, SL tools, MC tools}
            - LoKi_Variables: functors applied at particle level 
            - TrigLInes : L0, HLT1, HLT2 triggerr lines
        """
        
        # DV Config
        'DataType'     : None,
        'Simulation'   : None,
        'InputType'    : None,
        'nEvents'      : None,

        # instantiate the keys for the decay modes and corresponding configurables
        'DecayModes'       : ['B2DMuNuX_D02KPi', 'B2DMuNuX_D02K3Pi_Run2'],

        'Streams'               : {
                                    'B2DMuNuX_D02KPi' : 'Semileptonic',
                                    'B2DMuNuX_D02K3Pi_Run2'    : 'Semileptonic',
                                  },

        'StrippingLines'        : { 'B2DMuNuX_D02KPi'          :       'B2DMuNuX_D0',
                                    'B2DMuNuX_D02K3Pi_Run2'    :       'B2DMuNuX_D0_K3Pi'
                                  },

        'DecayDescriptors'       : {
                                    'B2DMuNuX_D02KPi' : '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC',
                                    'B2DMuNuX_D02K3Pi_Run2' : '[Xb -> ^([[D0]cc -> ^K- ^pi+ ^pi- ^pi+]CC) ^mu+]CC'
                                    },

        'TupleBranches'         : {
                                    'B2DMuNuX_D02KPi' : {'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                                                         'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                                                         'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC',
                                                         'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC',
                                                         'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'},

                                    'B2DMuNuX_D02K3Pi_Run2' : {'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                                                               'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                                                               'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+ pi- pi+]CC) mu+]CC',
                                                               'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+ pi- pi+]CC) mu+]CC',
                                                               'Pi_2'      :  '[Xb -> ([[D0]cc -> K- pi+ ^pi- pi+]CC) mu+]CC',
                                                               'Pi_3'      :  '[Xb -> ([[D0]cc -> K- pi+ pi- ^pi+]CC) mu+]CC',
                                                               'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC'}
                                    },

        'TrigLines': {
            'trigListL0': ["L0HadronDecision",
                           "L0MuonDecision",
                           "L0DiMuonDecision"],

            'trigListHlt1': ["Hlt1TrackAllL0Decision",
                             "Hlt1TrackMVADecision",
                             "Hlt1TrackMuonDecision",
                             "Hlt1TrackMuonMVADecision",
                             "Hlt1TwoTrackMVADecision",
                             "Hlt1DiMuonLowMassDecision",
                             "Hlt1DiMuonHighMassDecision"
                             ],

            'trigListHlt2': [
                            "Hlt2Topo2BodyDecision",
                            "Hlt2Topo3BodyDecision",
                            "Hlt2Topo4BodyDecision",
                            "Hlt2Topo2BodyBBDTDecision",
                            "Hlt2Topo3BodyBBDTDecision",
                            "Hlt2Topo4BodyBBDTDecision",
                            "Hlt2TopoMu2BodyDecision",
                            "Hlt2TopoMu3BodyDecision",
                            "Hlt2TopoMu4BodyDecision",
                            "Hlt2TopoMu2BodyBBDTDecision",
                            "Hlt2TopoMu3BodyBBDTDecision",
                            "Hlt2TopoMu4BodyBBDTDecision",
                            "Hlt2SingleMuonDecision"
                               ],
                    },

        'TTools'       : {
                            'BasicTools'  :       ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'],
                            'CommonTools' :       ['TupleToolGeometry',
                                                   'TupleToolTrackInfo', 'TupleToolRecoStats',
                                                   'TupleToolPrimaries', 'TupleToolSubMass', 'TupleToolTrackIsolation',
                                                   'TupleToolTrigger', 'TupleToolPropertime', 'TupleToolTISTOS', 'TupleToolSubMass'],
                             'SLTools'    :       ['TupleToolSLTools', 'TupleToolDocas'],
                             'MCTools'    :       ['TupleToolMCTruth', 'TupleToolBackgroundInfo']
                        },

        'LoKi_Variables' :   {
                             'B_LoKis' : {
                                             "BPVFDCHI2": "BPVVDCHI2",  # CHI2 distance from best PV
                                             "VCHI2": "VFASPF(VCHI2)",
                                             "VCHI2DOF": "VFASPF(VCHI2/VDOF)",
                                             "BPVIPCHI2": "BPVIPCHI2()",  # IP CHI2 relativeto BPV
                                             "BPVDIRA": "BPVDIRA",
                                             "ETA": "ETA",  # pseudorapidity
                                             "LTIME": "BPVLTIME()",  # proper life time (mm)
                                             "DTF_CTAU": "DTF_CTAU ( 0 , False , strings('D0') )",
                                             "DTF_CTAUSIGNIFICANCE": "DTF_CTAUSIGNIFICANCE ( 0 , False, strings('D0') )"
                                         },

                             'D0_LoKis' : {
                                             "DTF_CTAU": "DTF_CTAU ( 1 , False, strings('D0') )",
                                             "DTF_CTAUSIGNIFICANCE": "DTF_CTAUSIGNIFICANCE ( 1 , False, strings('D0') )"
                                          },

                             'Mu_LoKis'  : {
                                             "TRCHI2DOF": "TRCHI2DOF",  # return tracks with low chi2 PDOF
                                             "TRGHOSTPROB": "TRGHOSTPROB",  # track ghost prob
                                             "ETA": "ETA",
                                             "MIPCHI2PV": "MIPCHI2DV(PRIMARY)"  # min chi2 distance relative to PV (3 sigma, I think)
                                           },

                            'Daughter_LoKis'  : {
                                             "TRCHI2DOF": "TRCHI2DOF",  # return tracks with low chi2 PDOF
                                             "TRGHOSTPROB": "TRGHOSTPROB",  # track ghost prob
                                             "ETA": "ETA",
                                             "MIPCHI2PV": "MIPCHI2DV(PRIMARY)"  # min chi2 distance relative to PV (3 sigma, I think)
                                           },
                            },

    }# end of properties


    def accessProperty(self, key):
        """
        Helper function to access the keys in the class
        :param: decay-specific key
        :return: values in properties dict, for a given key
        """
        return self.__properties__[key]

    def setProperty(self, key, value):
        """
        Helper function to set the value of arg keys in the class
        :param: key: decay-specific key
        :param: value: decay-specific value
        :return: set value in properties dict, for a given key
        """
        self.__properties__[key] = value

    def InitializeDTT(self, mode):
        """
        Initialize decay tree tuple to access members and add tools
        :param mode: str key, decay-specific
        :return: dtt constructor with descriptor and member branches
        """
        NTuple = DecayTreeTuple(mode + 'Tuple')

        # set inputs and rootintes
        NTuple.Decay = self.accessProperty('DecayDescriptors')[mode]
        NTuple.addBranches(self.accessProperty('TupleBranches')[mode])
        if self.accessProperty('StrippingLines')[mode] is not None:
            if self.inputFormat == 'DST':
                NTuple.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(self.accessProperty('Streams')[mode],
                                                                      self.accessProperty('StrippingLines')[mode]) ]
            if self.inputFormat == 'MDST':
                NTuple.Inputs = ['Phys/{0}/Particles'.format(self.accessProperty('StrippingLines')[mode]) ]
                DaVinci().RootInTES = '/Event/{0}'.format(self.accessProperty('Streams')[mode])
        else:
            raise ValueError('Incorrect InputType: select an appropriate InputType: (M)DST')
        return NTuple

    def makeNTuple(self, mode):
        """
        Configure DecayTreeTuple
        :param mode: string, decay-specific key
        :return: booked and configure DTT
        """
        NTuple = self.InitializeDTT(mode)

        # needed to configure default tools - patch
        NTuple.ToolList = []
        NTuple.ToolList+=self.accessProperty('TTools')['BasicTools']

        for commontool in self.accessProperty('TTools')['CommonTools']:
            if commontool =='TupleToolGeometry':
                bookedtool = NTuple.addTupleTool(commontool)
                bookedtool.FillMultiPV = True
            if commontool == 'TupleToolTrackInfo':
                bookedtool = NTuple.addTupleTool(commontool)
                bookedtool.Verbose = True
            if commontool == 'TupleToolTrackIsolation':
                bookedtool = NTuple.addTupleTool(commontool)
                bookedtool.Verbose = True
            if commontool == 'TupleToolTrigger':
                bookedtool = NTuple.addTupleTool(commontool)
                bookedtool.VerboseL0 = True
                bookedtool.VerboseHlt1 = True
                bookedtool.VerboseHlt2 = True
        if self.accessProperty('Simulation') is True:
            for MCtool in self.accessProperty('TTols')['MCTools']:
                NTuple.addTupleTool(MCtool)


        for b in NTuple.Branches.keys():
            # query the trigger lists and merge them into one list
            trigListAll = []
            for triggerset in self.accessProperty('TrigLines').itervalues():
                trigListAll += triggerset

            # query the member branch, add and configure particle members and tools
            particlebank = getattr(NTuple, b)

            booked_bTool = particlebank.addTupleTool('TupleToolTISTOS')
            booked_bTool.VerboseL0 = True
            booked_bTool.VerboseHlt1 = True
            booked_bTool.VerboseHlt2 = True
            booked_bTool.TriggerList = trigListAll

            # LoKis
            b_hybrid = particlebank.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_'+b)
            b_preamble = []
            b_hybrid.Preambulo = b_preamble
            if b == 'B_plus':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['B_LoKis']
            if b == 'D0':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['D0_LoKis']
            if b == 'Mu_plus':
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['Mu_LoKis']
            if b != 'B_plus' and b != 'D0' and b != 'Mu_plus':  # this is bloody horrible
                b_hybrid.Variables = self.accessProperty('LoKi_Variables')['Daughter_LoKis']

            # # mother B-specific tools
            if b == 'B_plus':

                #SLtools
                for SLtool in self.accessProperty('TTools')['SLTools']:
                    if SLtool == 'TupleToolSLTools':
                        SL_bookedtool = particlebank.addTupleTool(SLtool)
                        SL_bookedtool.Bmass = 6273.7
                        SL_bookedtool.VertexCov = True
                        SL_bookedtool.MomCov = True

                    if SLtool == 'TupleToolDocas':
                        SL_bookedtool= particlebank.addTupleTool(SLtool)
                        SL_bookedtool.Name = ['D0_Mu_plus']
                        SL_bookedtool.Location1 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['D0'])]
                        SL_bookedtool.Location2 = ['{0}'.format(self.accessProperty('TupleBranches')[mode]['Mu_plus'])]

                # DecayTreeFitter
                particlebank.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV
                particlebank.ConsB.constrainToOriginVertex = False #neutrino, constraining tracks to PV -> worsens
                #the vertex resolution and hence the mass resolution too
                particlebank.ConsB.Verbose = True
                particlebank.ConsB.daughtersToConstrain = ['D0'] #constrain daughters to D0 mass, improve track
                #and mass resolution
                particlebank.ConsB.UpdateDaughters = True #store info on final state tracks

        return NTuple

    def Restrip(self, restripLines_list, EventNodeKiller):
        """
        Function to implement the restripping of MC

        Stripping versions per line:
        -   StrippingB2DMuNuX_D0: {s21r0p1, s21r1p1, s24r1, s28r1, s29r2}
        -   StrippingB2DMuNuX_D0_K3Pi: {s24r1, s28r1, s29r2}
        -   Strippingb2D0MuXK3PiCharmFromBSemiLine: {s21r0p1, s21r1p1}
        """

        # Clean up previous stripping banks
        # Remove Decisions from the MC production
        eventNodeKiller = EventNodeKiller('Stripkiller')
        eventNodeKiller.Nodes = ['/Event/AllStreams', '/Event/Strip']

        if self.accessProperty('DataType') == '2017': strip = 'stripping29r2'
        if self.accessProperty('DataType') == '2016': strip = 'stripping28r1'
        if self.accessProperty('DataType') == '2015': strip = 'stripping24r1'
        if self.accessProperty('DataType') == '2012': strip = 'stripping21r0p1'
        if self.accessProperty('DataType') == '2011': strip = 'stripping21r1p1'
        else: raise ValueError('Incorrect DataType: select an appropriate run year')

        config = strippingConfiguration(strip)
        archive = strippingArchive(strip)
        streams = buildStreams(stripping=config, archive=archive)

        MyStream = StrippingStream("MyStream")
        MyLines = restripLines_list

        for stream in streams:
            for line in stream.lines:
                if line.name() in MyLines:
                    MyStream.appendLines([line])

        from Configurables import ProcStatusCheck
        filterBadEvents = ProcStatusCheck()

        # HDRLocation = "SomeNonExistingLocation"
        sc = StrippingConf(Streams=[MyStream],
                           MaxCandidates=2000,
                           AcceptBadEvents=False,
                           BadEventSelection=filterBadEvents)

    def readLFN(self):
        """
        Wrapper for IOHelper() function
        :return: read in LFN from string
        """
        IOHelper().inputFiles([self.lfn], clear=True)

    # set DV configurables
    #if MDST: DaVinci().RootInTES = '/Event/{0}'.format(stream)
    def apply_configuration(self):
        """
        Main sequence, book top level sequence and decay-specific member sequences
        :return: Gaudisequences to producte outputfiles
        """
        if self.lfn is not None: self.readLFN()

        topSequence = GaudiSequencer('MasterSequence')
        topSequence.IgnoreFilterPassed=True  #allow all tuples to fill per event, ignoring fails

        restripLines = []
        # Loop and configure decay sequences, one per mode
        for decay in self.accessProperty('DecayModes'):
            DecaySequence = GaudiSequencer(decay+'_Sequence')

            restripLines+='Stripping'+self.accessProperty('StrippingLines')[decay]

            # implement pre-filters
            strip_filter = LoKi__HDRFilter('StripPassFilter_' + decay, Code="HLT_PASS('Stripping" + self.accessProperty('StrippingLines')[decay] + "Decision')")
            PV_filter = LoKi__VoidFilter('MultiPVFilter_'+ decay, Code="CONTAINS('Rec/Vertex/Primary') >= 1 ")
            DecaySequence.Members+=[strip_filter]
            DecaySequence.Members+=[PV_filter]

            # configure DTT
            DecaySequence.Members+=[self.makeNTuple(decay)]

            # after config, add the sequence to MasterSequence
            topSequence.Members += [DecaySequence]

        # DaVinci configurables
        if self.accessProperty('Simulation') is True:
            DaVinci().UserAlgorithms += [self.Restrip(restripLines, EventNodeKiller)]
            DaVinci().UserAlgorithms += [SMEAR('StateSmear')]
        if self.accessProperty('Simulation') is not True:
            DaVinci().UserAlgorithms+=[SCALER('StateScale')]

        DaVinci().UserAlgorithms += [topSequence]
        DaVinci().TupleFile = 'Bc2D0MuNuX.root'
        DaVinci().InputType = self.inputFormat
        DaVinci().PrintFreq = 1000
        DaVinci().DataType = self.year
        DaVinci().HistogramFile = 'Bc2D0MuNuX_DVHistos.root'
        if self.accessProperty('Simulation') is not True:
            DaVinci().Simulation = False
            DaVinci().Lumi = not DaVinci().Simulation

        DaVinci().EvtMax = self.maxEvts
# END OF CLASS


localLFNs = {
                '2011' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/00050877_00019045_1.semileptonic.dst',
                '2012' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/00051179_00027853_1.semileptonic.dst',
                '2015' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/00069078_00017887_1.semileptonic.dst',
                '2016' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/00070452_00008532_1.semileptonic.dst',
                '2017' : '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/00071957_00032399_1.semileptonic.dst',
                'fullSample' : None
            }

DV = DaVinciConfig('Bc2DMuNuX', 'DST', False, '2017', -1, localLFNs['2017'])
DV.apply_configuration()


