#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
# Ganga submission file for Ganga submission for DATA for channels:
#  - Bc->D0(->K pi) Mu Nu X
#  - Bc->D0(->K 3pi) Mu Nu X
#
# Note on stripping lines:
#   - B2DMuNuX_D0Line is used for both Run I and Run II for D0->KPi
#   - B2DMuNuX_D0_K3PiLine is used for Run II only of D0->K3Pi, as not available for Run I as of
#   19/10/18
#   - b2D0MuXK3PiCharmFromBSemiLine
# 
# DaVinci Versions:
#
#    Runs I,II Stream: Semileptonic (DST) and Charm (MDST)
#
#    -stripping 21r1p1 (Incremental restripping of 2011 data): DV v36r1p5 (stripping+tuples) {DV
#    v39r1p1 was suggested as the correct version but now deprecated since v36r1p5 is the right
#    version and issues of compatibility between the re-stripping and tuplization (TTGeometry) on
#    run I Jpsi MC}
#    -stripping 21r0p1 (Incremental stripping of 2012 data (autumn 2014)): DV v36r1p5
#    -stripping 24r1 (Full restripping of 2015 data): DV v38r1p3->v38r1p7
#    -stripping 28r1 (Full restripping  for 2016 data): DV v41r4p3->v41r4p5, DST
#    -stripping 29r2 (Full restripping of 2017 pp data): DV v42r5p1->v42r7p3, DST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst
#=======================================================================================================================

#=======================================================================================================================
# TODO Look into SLTRUTH Implementation
# TODO possible error: k3pi-specific tools if loop condition
# TODO AutoDB() Tags in Ganga
# TODO Double-check stripping lines
# TODO test and print IgnoreFilter=True
# TODO check function to implement restrip
# TODO check if mDST: need to use ROOTINTES
# TODO look into restripping extras for s21 
#=======================================================================================================================


#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

# imports for restripping
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import EventNodeKiller



# Scaler for DATA
from Configurables import TrackScaleState as SCALER
scaler = SCALER('StateScale')
# Smear for MC
from Configurables import TrackSmearState as SMEAR
smear = SMEAR('StateSmear')


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, years
run_year = '2015' #2011, 2012, 2015, 2016, 2017

#stream = 'Charm' for Charmline, D2K2Pi Mode, Run I
#note that Charm is mDST 
stream = 'Semileptonic' #Charm

#implement the restripping and smearing 
isMC = False #True 

#MC version
sim = '08a' #08{a,e}, 09{b}

#set to -1 for full run at submission
evts=100
#----------------------------------------------------


# Use the local input data
IOHelper().inputFiles([

        # data, SEMILEPTONICS
        #--------------------
        #'CORRUPTED: /usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/11/00050877_00019045_1.semileptonic.dst' 
        #'/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/12/00051179_00027853_1.semileptonic.dst' 
        '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/00069078_00017887_1.semileptonic.dst' 
        #'/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/16/00070452_00008532_1.semileptonic.dst' 
        #'/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/17/00071957_00032399_1.semileptonic.dst' 

        ], clear=True)


def FilterForStrippingLine(line):
    """
    Filter to look for events with relevant stripping line, DecReports, at least one PV
    """
    fltrs = LoKi_Filters(
               # try to save time by checking DecReports Exist and that line is not empty
                VOID_DecRep_Code = "EXISTS('/Event/Strip/Phys/DecReports')",
                STRIP_Code = "HLT_PASS_RE('Stripping{0}Decision')".format(line),
                VOID_PV_Code = " CONTAINS    ('Rec/Vertex/Primary') >= 1 " #check that there is at
               # least one PV
                )
    DaVinci().EventPreFilters = fltrs.filters('Filters')


def Restrip(line, EventNodeKiller, Configurables=None):
    """ 
    Function to implement the restripping of MC
    
    Stripping versions per line:
    -   StrippingB2DMuNuX_D0: {s21r0p1, s21r1p1, s24r1, s28r1, s29r2}
    -   StrippingB2DMuNuX_D0_K3Pi: {s24r1, s28r1, s29r2} 
    -   Strippingb2D0MuXK3PiCharmFromBSemiLine: {s21r0p1, s21r1p1} 
    """
    
    #Clean up previous stripping banks
    #Remove Decisions from the MC production
    eventNodeKiller = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
    
    if run_year=='2017': strip='stripping29r2'
    if run_year=='2016': strip='stripping28r1' 
    if run_year=='2015': strip='stripping24r1'
    if run_year=='2012': strip='stripping21r0p1'
    if run_year=='2011': strip='stripping21r1p1'
    
    config=strippingConfiguration(strip)
    archive=strippingArchive(strip)
    streams=buildStreams(stripping=config,archive=archive)
    
    
    MyStream = StrippingStream("MyStream")
    MyLines = ['Stripping'+line]
        
    for stream in streams:
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines( [ line ] )
        
    from Configurables import ProcStatusCheck
    filterBadEvents=ProcStatusCheck()
        
    # HDRLocation = "SomeNonExistingLocation"
    sc=StrippingConf( Streams= [ MyStream ],
                      MaxCandidates = 2000,
                      AcceptBadEvents = False,
                      BadEventSelection = filterBadEvents )


#----------------------------------------------------
# DECLARE TRIGGER LINES 
# RUN II: add Hlt1TwoTrackMVA 

trigListL0 = [ "L0HadronDecision",
               "L0MuonDecision", 
               "L0DiMuonDecision"
                ]

trigListHlt1 = [ "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1DiMuonLowMassDecision",
                 "Hlt1DiMuonHighMassDecision"
                 ]

trigListHlt2 = [    "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2
#-----------------------------------------------------


#-----------------------------------------------------------------------------------------------
# LoKi Functors
# DTF vertex constrain set to false due to neutrino -> avoid worsening mass resolution

#B_plus_hybrid.Variables = {
B_LoKis = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
        "VCHI2"    : "VFASPF(VCHI2)",
        "VCHI2DOF" : "VFASPF(VCHI2/VDOF)",
        "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()", #proper life time (mm)
         "DTF_CTAU" : "DTF_CTAU ( 0 , False , strings('D0') )",  
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 0 , False, strings('D0') )"
    }

#D0_hybrid.Variables = {
D0_LoKis = {
         "DTF_CTAU" : "DTF_CTAU ( 1 , False, strings('D0') )",  
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 1 , False, strings('D0') )"
    }

#Mu_plus_hybrid.Variables = {
Mu_LoKis = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }

Pi_LoKis = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }

K_LoKis = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)"#min chi2 distance relative to PV (3 sigma, I think)
        }
#-----------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------
#          *****************************************************************
#          ***** CREATE TUPLES FOR ALL MODES, FOR ALL STRIPPING LINES ******
#          *****************************************************************

booked_tuples = []


#=======================================================================================================================
# declare tuples to avoid reference before assignment
#-----------------------------------------------------

dtt_B2D0MuNu_D02KPi = DecayTreeTuple('TupleB2D0Mu_D02KPi') #K Pi mode, Run I and Run II
dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine = DecayTreeTuple('TupleB2D0Mu_D02K3Pi_B2DMuNuXLine') #K3Pi mode,
#available only for Run II as of 19/10/18 (restripping request sumbitted w/ medium priority)
dtt_B2D0MuNu_D02K3Pi_CharmLine = DecayTreeTuple('TupleB2D0Mu_D02KPi_CharmLine') #K3Pi mode, mDST
#line, used for Run I
#=======================================================================================================================


# Note: SL stream has DST as input
if stream=='Semileptonic':
    DaVinci().InputType = 'DST'
    
    # B->D0(->KPi)MuNuX
    #------------------
    #jump to stripping line
    lineKPi = 'B2DMuNuX_D0' #s21r1p1, s21r0p1, s24r1, s28r1, s29r2
    #FilterForStrippingLine(lineKPi)

    #dtt_B2D0MuNu_D02KPi.IgnoreFilterPassed=True #pass if errors in filling tuple

    if not isMC:
        dtt_B2D0MuNu_D02KPi.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, lineKPi)]
        #scale
        DaVinci().UserAlgorithms += [scaler] #data
    if isMC:
        dtt_B2D0MuNu_D02KPi.Inputs = ['Phys/{0}/Particles'.format(lineKPi)]
        #restrip
        DaVinci().appendToMainSequence( [ Restrip(lineKPi, EventNodeKiller) ] )   
        #smear
        DaVinci().UserAlgorithms += [smear] #mc

    dtt_B2D0MuNu_D02KPi.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0
    dtt_B2D0MuNu_D02KPi.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                     'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                     'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC', 
                     'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC', 
                     'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})
   
    
    # B->D0(->K3Pi)MuNuX Run II
    #--------------------------
    # jump to stripping line
    lineK3Pi = 'B2DMuNuX_D0_K3Pi' #s24r1, s28r1, s29r2
    #FilterForStrippingLine(lineK3Pi)
    
    #dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.IgnoreFilterPassed=True
    
    if not isMC:
        dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, lineK3Pi)]
        #scale
        DaVinci().UserAlgorithms += [scaler] #data
    if isMC:
        dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.Inputs = ['Phys/{0}/Particles'.format(lineKPi)]
        # restrip
        DaVinci().appendToMainSequence( [ Restrip(lineK3Pi, EventNodeKiller) ] )   
        # smear
        DaVinci().UserAlgorithms += [smear] #mc
    
    dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+ ^pi- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0
    dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                     'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                     'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+ pi- pi+]CC) mu+]CC', 
                     'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+ pi- pi+]CC) mu+]CC', 
                     'Pi_2'      :  '[Xb -> ([[D0]cc -> K- pi+ ^pi- pi+]CC) mu+]CC', 
                     'Pi_3'      :  '[Xb -> ([[D0]cc -> K- pi+ pi- ^pi+]CC) mu+]CC', 
                     'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC'})

    
    # book the Tuples from the SL stream
    booked_tuples+=[dtt_B2D0MuNu_D02KPi]
    booked_tuples+=[dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine]
    
    print '================================================='
    print '*************** booked tuples*******************:', booked_tuples
    print '================================================='

    '''
# Note: Charm stream has mDST as input 
if stream=='Charm':

    # *NOTE*: DATA and MC might be DST, in which case absorb the InputType into if isMC conditions
    DaVinci().InputType == 'MDST'

    # B->D0(->K3Pi)MuNuX Run I, mDST
    #-------------------------------
    #jumpt to stripping line
    lineCharm = 'b2D0MuXK3PiCharmFromBSemiLine' #s21r1p1, s21r0p1
    FilterForStrippingLine(lineCharm)
    dtt_B2D0MuNu_D02K3Pi_CharmLine.IgnoreFilterPassed=True

    # handle the Inputs and TES depending on input file format
    if not isMC:
        dtt_B2D0MuNu_D02K3Pi_CharmLine.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, lineCharm)]
        # scale
        DaVinci().UserAlgorithms += [scaler] #data

    if isMC:
        dtt_B2D0MuNu_D02K3Pi_CharmLine.Inputs = ['Phys/{0}/Particles'.format(lineCharm)]
        # restrip
        DaVinci().appendToMainSequence( [ Restrip(lineCharm, EventNodeKiller) ] )   
        # smear
        DaVinci().UserAlgorithms += [smear] #mc

    # PATCH: verify whether CHARM data and/or MC are MDST; if so, RootInTES
    if DaVinci().InputType == 'MDST':
        DaVinci().RootInTES = '/Event/{0}'.format(stream)


    dtt_B2D0MuNu_D02K3Pi_CharmLine.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+ ^pi- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0
    dtt_B2D0MuNu_D02K3Pi_CharmLine.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                     'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                     'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+ pi- pi+]CC) mu+]CC', 
                     'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+ pi- pi+]CC) mu+]CC', 
                     'Pi_2'      :  '[Xb -> ([[D0]cc -> K- pi+ ^pi- pi+]CC) mu+]CC', 
                     'Pi_3'      :  '[Xb -> ([[D0]cc -> K- pi+ pi- ^pi+]CC) mu+]CC', 
                     'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC'})

#   book the Tuples from  Charm stream
    booked_tuples+=[dtt_B2D0MuNu_D02K3Pi_CharmLine]
#-----------------------------------------------------------------------------------------------

    '''

#-----------------------------------------------------------------------------------------------
#                  ***************************************************
#                  ***** add the same TupleTools for all NTuples *****
#                  ***************************************************

# note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daughter final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

for dtt in booked_tuples:
   
    dtt.ToolList = []
    dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo']
    
    #add PV array
    TTG = dtt.addTupleTool('TupleToolGeometry')
    TTG.FillMultiPV = True
    
    track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
    track_tool.Verbose = True
    dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
    dtt.addTupleTool('TupleToolPrimaries') #PV info 
    track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
    track_ils.Verbose = True
    
    TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
    TT.VerboseL0 = True
    TT.VerboseHlt1 = True
    TT.VerboseHlt2 = True
    
    dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 
    
    #RICH PID not available on run II
    if run_year=='2011' or run_year=='2012': 
        dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi)
    
    # ***** Not working on s29 - potential patch: use DV v42r6p1 (Alison's suggestion) ****************
    #dtt.addTupleTool('TupleToolConeIsolation')
    #if run_year=='2011' or run_year=='2012': 
    #    dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits
    #******************************************
    

    #------------------
    # mc specific tools
    #------------------
    if isMC:    
        dtt.addTupleTool("TupleToolMCTruth")
        dtt.addTupleTool("TupleToolMCBackgroundInfo")
        dtt.addTupleTool("TupleToolRecoStats")


    #-------------
    #Trigger lines
    #-------------
    B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
    B_TisTos.VerboseL0 = True
    B_TisTos.VerboseHlt1 = True
    B_TisTos.VerboseHlt2 = True
    B_TisTos.TriggerList = trigListAll 
    
    D_TisTos = dtt.D0.addTupleTool('TupleToolTISTOS')
    D_TisTos.VerboseL0 = True
    D_TisTos.VerboseHlt1 = True
    D_TisTos.VerboseHlt2 = True
    D_TisTos.TriggerList = trigListAll 
    
    K_TisTos = dtt.K_minus.addTupleTool('TupleToolTISTOS')
    K_TisTos.VerboseL0 = True
    K_TisTos.VerboseHlt1 = True
    K_TisTos.VerboseHlt2 = True
    K_TisTos.TriggerList = trigListAll 
    
    #add extra pions for 3pi mode
    Pi_1_TisTos = dtt.Pi_1.addTupleTool('TupleToolTISTOS')
    Pi_1_TisTos.VerboseL0 = True
    Pi_1_TisTos.VerboseHlt1 = True
    Pi_1_TisTos.VerboseHlt2 = True
    Pi_1_TisTos.TriggerList = trigListAll 
    
    Mu_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
    Mu_TisTos.VerboseL0 = True
    Mu_TisTos.VerboseHlt1 = True
    Mu_TisTos.VerboseHlt2 = True
    Mu_TisTos.TriggerList = trigListAll


    #---------------------------------
    #configure SLTools to the Bc+ Mass
    #---------------------------------
    from Configurables import TupleToolSLTools
    dtt.ReFitPVs = True
    SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools') 
    SLLtool.Bmass = 6273.7
    SLLtool.VertexCov = True
    SLLtool.MomCov = True

    
    #--------------
    # LoKi Functors
    #--------------
    B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
    B_plus_preamble = []
    B_plus_hybrid.Preambulo = B_plus_preamble 
    ##B_plus_hybrid.Variables = B_LoKis
    B_plus_hybrid.Variables = B_LoKis 

    D0_hybrid = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/D0") 
    D0_preamble = []
    D0_hybrid.Preambulo = D0_preamble 
    D0_hybrid.Variables = D0_LoKis
    
    Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus") 
    Mu_plus_preamble = []
    Mu_plus_hybrid.Preambulo = Mu_plus_preamble 
    Mu_plus_hybrid.Variables = Mu_LoKis
    
    Pi_1_hybrid = dtt.Pi_1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_1_plus") 
    Pi_1_preamble = []
    Pi_1_hybrid.Preambulo = Pi_1_preamble 
    Pi_1_hybrid.Variables = Pi_LoKis
    
    K_minus_hybrid = dtt.K_minus.addTupleTool("LoKi::Hybrid::TupleTool/K_minus_plus") 
    K_minus_preamble = []
    K_minus_hybrid.Preambulo = K_minus_preamble 
    K_minus_hybrid.Variables = K_LoKis


    #-------------------------
    #implement DecayTreeFitter
    #-------------------------
    dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV
    dtt.B_plus.ConsB.constrainToOriginVertex = False #neutrino, constraining tracks to PV -> worsens
    #the vertex resolution and hence the mass resolution too
    dtt.B_plus.ConsB.Verbose = True
    dtt.B_plus.ConsB.daughtersToConstrain = ['D0'] #constrain daughters to D0 mass, improve track
    #and mass resolution
    dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks

    
    #end of loop
#-----------------------------------------------------------------------------------------------



#-----------------------------------------------------------------------------------------------
#                      *************************************
#                      ***** Decay-mode-specific tools *****
#                      *************************************


'''
for dtt in booked_tuples:   #sloopy! consider a dictionary of streams
    if dtt == dtt_B2D0MuNu_D02KPi:

        #***************************************************
        #*NOTE*: SLTRUTH causes error; need to investigate;
        from Configurables import TupleToolSLTruth
        #***************************************************

        #-------------------------------
        #import DOCA tools from SL tools
        #-------------------------------
        B_plus_docas = dtt_B2D0MuNu_D02KPi.B_plus.addTupleTool("TupleToolDocas")
        B_plus_docas.Name = ["D0_Mu_plus"]
        B_plus_docas.Location1 =[ '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC' ] #D0 tagged
        B_plus_docas.Location2 =[ '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC' ] #muon tagged

    # extra 2 pions
    if dtt != dtt_B2D0MuNu_D02KPi:

        #***************************************************
        #*NOTE*: SLTRUTH causes error; need to investigate;
        from Configurables import TupleToolSLTruth
        #***************************************************

        #-------------------------------
        #import DOCA tools from SL tools
        #-------------------------------
        B_plus_docas = dtt.B_plus.addTupleTool("TupleToolDocas")
        B_plus_docas.Name = ["D0_Mu_plus"]
        B_plus_docas.Location1 =[ '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC' ] #D0 tagged
        B_plus_docas.Location2 =[ '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC' ] #muon tagged

        #-----------------------------------
        # Trigger lines for additional pions
        #-----------------------------------
        Pi_2_TisTos = dtt.Pi_2.addTupleTool('TupleToolTISTOS')
        Pi_2_TisTos.VerboseL0 = True
        Pi_2_TisTos.VerboseHlt1 = True
        Pi_2_TisTos.VerboseHlt2 = True
        Pi_2_TisTos.TriggerList = trigListAll
    
        Pi_3_TisTos = dtt.Pi_3.addTupleTool('TupleToolTISTOS')
        Pi_3_TisTos.VerboseL0 = True
        Pi_3_TisTos.VerboseHlt1 = True
        Pi_3_TisTos.VerboseHlt2 = True
        Pi_3_TisTos.TriggerList = trigListAll

        #-----------------------------------
        # LoKi functors for additional pions
        #-----------------------------------
        Pi_2_hybrid = dtt.Pi_2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_2_plus")
        Pi_2_preamble = []
        Pi_2_hybrid.Preambulo = Pi_2_preamble
        Pi_2_hybrid.Variables = Pi_LoKis

        Pi_3_hybrid = dtt.Pi_3.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_3_plus")
        Pi_3_preamble = []
        Pi_3_hybrid.Preambulo = Pi_3_preamble
        Pi_3_hybrid.Variables = Pi_LoKis
#-----------------------------------------------------------------------------------------------
'''



# NOT USING ISOLATION TOOLS
##--------------------------------------------------------------------------------
##configure Isolation Tools
#from Configurables import TupleToolApplyIsolation
#dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
#dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #cloned from RLc repo
#dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
##---------------------------------------------------------------------------------


    
for dtt in booked_tuples:
    DaVinci().UserAlgorithms += [dtt]

DaVinci().TupleFile = 'Bc2D0MuNuX.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = run_year
DaVinci().HistogramFile = 'Bc2D0MuNuX_DVHistos.root'
if not isMC:
    DaVinci().Simulation = False
    DaVinci().Lumi = not DaVinci().Simulation #Lumi True

DaVinci().EvtMax = evts

#needs checking and implement different tags for the charm MC
#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 


# -------------------------------------------
# *NOTE*: Ganga has an autoD() functionality, 
# probably worth using that to handle all MC

#if not isMC:
#    if run_year=='2011':
#        DaVinci().CondDBtag = 'cond-20150409'
#        DaVinci().DDDBtag = 'dddb-20171030-1'
#    if run_year=='2012':
#        DaVinci().CondDBtag = 'cond-20150409-1'
#        DaVinci().DDDBtag = 'dddb-20171030-2'
#    if run_year=='2015':
#        DaVinci().CondDBtag = 'cond-20171211'
#        DaVinci().DDDBtag = 'dddb-20171030-3'
#    if run_year=='2016':
#        DaVinci().CondDBtag = 'cond-20170325'
#        DaVinci().DDDBtag = 'dddb-20171030-3'
#    if run_year=='2017':
#        DaVinci().CondDBtag = 'cond-20170724'
#        DaVinci().DDDBtag = 'dddb-20171030-3'
# -------------------------------------------

