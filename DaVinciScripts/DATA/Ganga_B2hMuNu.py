#Ganga script for submission to run for NTuples of B2D0MuNu an Bc2D0MuNu
#
#Make sure the GaudiExec() directory is the correct lb-checked out environment
#with DV, Analysis (correct compatible version), SLTools and Isolation Tools with 
#weights.xm fie
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch

#Bc->JpsiMuNu Mode:
#DaVinci Versions:
#
# Run I, Stream: Dimuon (DST)
#
#    -stripping 21r1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#
# Run II, Stream: Dimuon (DST)
#
#    -stripping 24 (restripping of 2015 data): DV v38r1p3->v38r1p7 (patch for TISTOS bug fix)
#
# Run II, Stream: Leptonic (DST) (same cuts as Dimuon)
#
#    - !!!--> stripping 28 (restripping for 2016 data): DV v41r4p3->v41r4p5   *** mDST
#    - !!!--> stripping 29 (concurrent stripping of 2017 pp data): DV v42r5p1   *** mDST


# *NOTE*: due to compatibility issues (as of teh 28 of June), Run II data has more recent stripping
# versions but older DV versions; use the older DV versions for the moment. e.g.:
# strupping29->stripping29r2 but still use DV for s29 ie v38r1p3


import sys
import os

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#----------------
#Data for D0 Mode

#DST=['DATA/DATAfiles/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py',
#     'DATA/DATAfiles/D0Mode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_RealData_Reco14_Stripping21r1p1a_SEMILEPTONIC.DST.py']
#DST=['DATA/DATAfiles/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py',
#     'DATA/DATAfiles/D0Mode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21r0p1a_SEMILEPTONIC.DST.py']
#DST=['DATA/DATAfiles/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1p1_SEMILEPTONIC.DST.py',
#     'DATA/DATAfiles/D0Mode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1p1_SEMILEPTONIC.DST.py']
#DST=['DATA/DATAfiles/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py',
#     'DATA/DATAfiles/D0Mode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1p1_SEMILEPTONIC.DST.py']
#DST=['DATA/DATAfiles/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py',
#     'DATA/DATAfiles/D0Mode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_SEMILEPTONIC.DST.py']


#-------------------
#Data for J/Psi Mode

#DST=['DATA/DATAfiles/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagDown_Real_Data_Reco14_Stripping21r1_DIMUON.DST.py',
#     'DATA/DATAfiles/JpsiMode/11/LHCb_Collision11_90000000_Beam3500GeVVeloClosedMagUp_Real_Data_Reco14_Stripping21r1_DIMUON.DST.py']
#DST=['DATA/DATAfiles/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagDown_RealData_Reco14_Stripping21_DIMUON.DST.py',
#     'DATA/DATAfiles/JpsiMode/12/LHCb_Collision12_90000000_Beam4000GeVVeloClosedMagUp_RealData_Reco14_Stripping21_DIMUON.DST.py']
#DST=['DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py',
#     'DATA/DATAfiles/JpsiMode/15/LHCb_Collision15_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco15a_Stripping24r1_LEPTONIC.MDST.py']
DST=['DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py',
     'DATA/DATAfiles/JpsiMode/16/LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco16_Stripping28r1_LEPTONIC.MDST.py']
#DST=['DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagDown_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py', 
#     'DATA/DATAfiles/JpsiMode/17/LHCb_Collision17_90000000_Beam6500GeVVeloClosedMagUp_RealData_Reco17_Stripping29r2_LEPTONIC.MDST.py']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




#Option scripts
D0mode_opts = '/DATA/ntuple_opts_StrippingB2DMuNuX_D0.py'
Jpsimode_opts = '/DATA/ntuple_opts_StrippingBc2JPsiMu.py'

for i in range(len(DST)):
    
    polarity = str(DST[i])
    print '\n***********************************************************************************************************************************'
    print 'Submitting \n%s'%polarity 
    print '***********************************************************************************************************************************\n'
    
    #j = Job(name='Bc2JpsiMuNu')
    j = Job(name='B2hMuNu')
    j.comment='%s'%polarity
    
    #set application= lb-checked out DV environment
    myApp = GaudiExec()

    user_release_area = os.path.expandvars('$HOME') + '/private'
    
    #--------------------------------------------------------------------
    # ***** CHANGE DV ENVIRONMENT TO BUILD FOR DIFFERENT STRIPPINGS *****
    
    #DaVinciDev_v39r1p1 : stripping {21r1, 21} (dimuon)
    #DaVinciDev_v38r1p3->v38r1p7 : stripping 24r1 (dimuon)
    #DaVinciDev_v41r4p3->v41r4p5 : stripping 28r1 (leptonic)
    #DaVinciDev_v42r5p1->v42r7p3 : stripping 29r2 (leptonic)
    #same applies to D0 mode for Semileptonic stream
    
    # *****PATCH: DaVinciDev_v42r6p1 for Run II

    # *************************************************
    # *NOTE*: select the appropriate DV version
    #------------------------------------------
    LocalDV = '%s/DaVinci_DevX/DaVinciDev_v41r4p5' % user_release_area
    myApp.directory = '%s/DaVinci_DevX/DaVinciDev_v41r4p5' % user_release_area
    # *************************************************
    
    j.application = myApp
    
    # ***** select opts file for decay mode *********** 
    cwd = os.getcwd() #current working directory
   
    # *NOTE*: choose decay mode 
    #--------------------------
    #j.application.options = [cwd+D0mode_opts] 
    j.application.options = [cwd+Jpsimode_opts] 
    
    
    # *NOTE* must select the appropriate build platform to match the DV build
    #------------------------------------------------------------------------
    j.application.platform='x86_64-slc6-gcc49-opt'
    # *************************************************
    
    #--------------------------------------------------------------------
    
    
    #import weights.xml for BDT in TupleToolIsolationTools.*
    j.inputfiles=[LocalFile(LocalDV+'/Phys/DecayTreeTuple/src/weights.xml')]
    
    
    #run on dataset (mag up and down)
    j.application.readInputData(polarity)
    
    #configure output
    #j.outputfiles = [LocalFile('*.root')]
    j.outputfiles = [LocalFile('*.root')]
    
    # *NOTE* = maxFiles set 1 for test on ganga with local backend
    #configure submission
    j.backend = Dirac() #Dirac()
    j.splitter = SplitByFiles(filesPerJob=10, maxFiles=-1, ignoremissing = True)
    j.submit()



