#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
#DaVinci Versions:
#
# Runs I,II Stream: Semileptonic (DST)
#
#    -stripping 21r1p1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21r0p1 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#    -stripping 24 (estripping of 2015 data): DV v38r1p3->v38r1p7
#    -stripping 28 (restripping for 2016 data): DV v41r4p3->v41r4p5, DST
#    -stripping 29 (concurrent stripping of 2017 pp data): DV v42r5p1->v42r7p3, DST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst

#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

#configure SL Isolation Tools, SL mass correction and momentum scaling
#from SemileptonicCommonTools import * 
from Configurables import TrackScaleState as SCALER
scaler = SCALER('StateScale')


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, years
run_year = '2017' #2011, 2012, 2015, 2016, 2017

stream = 'Semileptonic' 
line = 'B2DMuNuX_D0' #run I (available for run II too)

isMC = False

#set to -1 for full run at submission
evts=-1
#----------------------------------------------------


#look at events with relevant stripping line
fltrs = LoKi_Filters (
            STRIP_Code = "HLT_PASS_RE('StrippingB2DMuNuX_D0Decision')" #check
            )
DaVinci().EventPreFilters = fltrs.filters('Filters')


#----------------------------------------------------
#tupletools
#note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daugher final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('TupleB2D0Mu_D02KPi') #name of tuple withing the root file
dtt.ToolList = []
dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'] 

#add PV array
TTG = dtt.addTupleTool('TupleToolGeometry')
TTG.FillMultiPV = True

track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
track_tool.Verbose = True
dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
dtt.addTupleTool('TupleToolPrimaries') #PV info 
track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
track_ils.Verbose = True


TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
TT.VerboseL0 = True
TT.VerboseHlt1 = True
TT.VerboseHlt2 = True

dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 

#RICH PID not available on run II
if run_year=='2011' or run_year=='2012': 
    dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi)


if not isMC:
    dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)] 

#branches and naming for dauughter article(s)
dtt.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0

dtt.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC', 
                 'Pi_plus'   :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC', 
                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})


# ***** Not working on s29 - potential patch: use DV v42r6p1 (Alison's suggestion) ****************
dtt.addTupleTool('TupleToolConeIsolation')
if run_year=='2011' or run_year=='2012': 
    dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits
#******************************************


from Configurables import TupleToolSLTools
dtt.ReFitPVs = True
SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools') #out of the box SL tools - ask Patrick on how to configure
SLLtool.Bmass = 6273.7
SLLtool.VertexCov = True
SLLtool.MomCov = True

#----------------------------------------------------


#----------------------------------------------------
#import DOCA tools from SL tools
from Configurables import TupleToolSLTruth

B_plus_docas = dtt.B_plus.addTupleTool("TupleToolDocas")
B_plus_docas.Name = ["D0_Mu_plus"]
B_plus_docas.Location1 =[ '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC' ] #D0 tagged 
B_plus_docas.Location2 =[ '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC' ] #muon tagged 

#----------------------------------------------------


#----------------------------------------------------
#configure Isolation Tools
from Configurables import TupleToolApplyIsolation
dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #cloned from RLc repo
dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
#----------------------------------------------------


#----------------------------------------------------
# TRIGGER LINES 
# list of trigger lines on the B

#RUN II: add Hlt1TwoTrackMVA 

trigListL0 = [ "L0HadronDecision",
               "L0MuonDecision", 
               "L0DiMuonDecision"
                ]

trigListHlt1 = [ "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1DiMuonLowMassDecision",
                 "Hlt1DiMuonHighMassDecision"
                 ]

trigListHlt2 = [    "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2

B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
B_TisTos.VerboseL0 = True
B_TisTos.VerboseHlt1 = True
B_TisTos.VerboseHlt2 = True
B_TisTos.TriggerList = trigListAll 

D_TisTos = dtt.D0.addTupleTool('TupleToolTISTOS')
D_TisTos.VerboseL0 = True
D_TisTos.VerboseHlt1 = True
D_TisTos.VerboseHlt2 = True
D_TisTos.TriggerList = trigListAll 

K_TisTos = dtt.K_minus.addTupleTool('TupleToolTISTOS')
K_TisTos.VerboseL0 = True
K_TisTos.VerboseHlt1 = True
K_TisTos.VerboseHlt2 = True
K_TisTos.TriggerList = trigListAll 

Pi_TisTos = dtt.Pi_plus.addTupleTool('TupleToolTISTOS')
Pi_TisTos.VerboseL0 = True
Pi_TisTos.VerboseHlt1 = True
Pi_TisTos.VerboseHlt2 = True
Pi_TisTos.TriggerList = trigListAll 

Mu_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
Mu_TisTos.VerboseL0 = True
Mu_TisTos.VerboseHlt1 = True
Mu_TisTos.VerboseHlt2 = True
Mu_TisTos.TriggerList = trigListAll
#----------------------------------------------------


#----------------------------------------------------
#LoKis
B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
B_plus_preamble = []
B_plus_hybrid.Preambulo = B_plus_preamble 

B_plus_hybrid.Variables = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
	     "VCHI2"  : "VFASPF(VCHI2/VDOF)",
	     "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()", #proper life time (mm)
         "DTF_CTAU" : "DTF_CTAU ( 0 , True , strings('D0') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 0 , True, strings('D0') )"
    }

D0_hybrid = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/D0") 
D0_preamble = []
D0_hybrid.Preambulo = D0_preamble 

D0_hybrid.Variables = {
         "DTF_CTAU" : "DTF_CTAU ( 1 , True, strings('D0') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 1 , True, strings('D0') )"
    }

Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus") 
Mu_plus_preamble = []
Mu_plus_hybrid.Preambulo = Mu_plus_preamble 

Mu_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Pi_plus_hybrid = dtt.Pi_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_plus") 
Pi_plus_preamble = []
Pi_plus_hybrid.Preambulo = Pi_plus_preamble 

Pi_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


K_minus_hybrid = dtt.K_minus.addTupleTool("LoKi::Hybrid::TupleTool/K_minus_plus") 
K_minus_preamble = []
K_minus_hybrid.Preambulo = K_minus_preamble 

K_minus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)"#min chi2 distance relative to PV (3 sigma, I think)
        }

#----------------------------------------------------


#----------------------------------------------------
#implement DecayTreeFitter

dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV of origin
dtt.B_plus.ConsB.constrainToOriginVertex = True #take into account the neutrino
dtt.B_plus.ConsB.Verbose = True
dtt.B_plus.ConsB.daughtersToConstrain = ['D0'] #constrain K Pi to D0
dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks

#----------------------------------------------------


DaVinci().UserAlgorithms += [scaler]
DaVinci().UserAlgorithms += [dtt]

DaVinci().TupleFile = 'B2D0MuNu.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = run_year


#run on data: s28 & s29 are leptonic and mDST
if not isMC:
    DaVinci().Simulation = False
    DaVinci().Lumi = not DaVinci().Simulation #Lumi True
    DaVinci().InputType = 'DST'

# Only ask for luminosity information when not using simulated data
DaVinci().EvtMax = evts


#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 
if not isMC:
    if run_year=='2011':
        DaVinci().CondDBtag = 'cond-20150409'
        DaVinci().DDDBtag = 'dddb-20171030-1'
    if run_year=='2012':
        DaVinci().CondDBtag = 'cond-20150409-1'
        DaVinci().DDDBtag = 'dddb-20171030-2'
    if run_year=='2015':
        DaVinci().CondDBtag = 'cond-20171211'
        DaVinci().DDDBtag = 'dddb-20171030-3'
    if run_year=='2016':
        DaVinci().CondDBtag = 'cond-20170325'
        DaVinci().DDDBtag = 'dddb-20171030-3'
    if run_year=='2017':
        DaVinci().CondDBtag = 'cond-20170724'
        DaVinci().DDDBtag = 'dddb-20171030-3'
