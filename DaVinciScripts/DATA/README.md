# Scripts for creation of data NTuples

Options scripts and ganga submission files. *Always test locally with `backend==Local()`
beforehand*. 

- `Ganga_B2hMuNu.py` = ganga submission file for both D0 and Jpsi modes
- `ntuple_opts_StrippingB2DMuNuX_D0.py` = opts file for Bc->D0(->Kpi)MuNuX
- `ntuple_opts_Strippingb2D0MuXB2DMuNuXLine.py` = opts file for Bc->D0(->K3pi)MuNuX
- `ntuple_opts_StrippingBc2JPsiMu.py` = opts file for Bc->JspiMuNu

### Use: launch ganga, edit files, submit all in one interactive session


```bash
ganga --no-mon #suppress monitoring loop

!vim <files> #edit in vim  

%ganga Ganga_B2hMuNu.py #submit
```
