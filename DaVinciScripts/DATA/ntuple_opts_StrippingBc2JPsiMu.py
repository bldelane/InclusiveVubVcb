#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
#DaVinci Versions:
#
# Run I, Stream: Dimuon (DST)
#
#    -stripping 21r1 (incremental restripping of 2011 data): DV v39r1p1
#    -stripping 21 (legacy stripping of 2012 data (autumn 2014)): DV v39r1p1
#

# Run II, Stream: Leptonic (MDST) (same cuts as Dimuon)
#
#    - !!!--> stripping 24r1 (restripping for 2015 data TISTOS bug fixed): DV v38r1p7   *** mDST
#    - !!!--> stripping 28r1(restripping for 2016 data): DV v41r4p3->check for updated version   *** mDST
#    - !!!--> stripping 29r2 (concurrent stripping of 2017 pp data): DV v42r5p1->check for updated
#    version   *** mDST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst

#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *
import sys
import os

from Configurables import TrackScaleState as SCALER

# *NOTE*: scale on data, smear on MC
# take care of this with either an if confidition
# or two parallel submissions
scaler = SCALER('StateScale')

################################################################
#                ***** PARAMETERS TO CHANGE *****

run_year = '2016' #2011, 2012, 2015, 2016, 2017
isMC = False

# *NOTE*: place this in a MC submission file
#----------------------------------------------------
# sim = '09' #08, 09


# select appropriate stream, depending on run
#----------------------------------------------------
# stream21 = 'Dimuon' #up to stream 21 
# stream29 = 'Leptonic' #up to stream 29
# *NOTE*: stripping 24r1 (2015 TISTOS bug fixed) now implements bc2jpsimuline to s24r1 *leptonic* MDST

if run_year=='2015' or run_year=='2016' or run_year=='2017': 
    stream = 'Leptonic'
if run_year=='2011' or run_year=='2012': 
    stream = 'Dimuon'

# line is the same for both streams
line = 'Bc2JpsiMuLine' #run I (available for  II too)


# set to -1 for full stream submission
#-------------------------------------
evts = -1

################################################################



#look at events with relevant stripping line
fltrs = LoKi_Filters (
            STRIP_Code = "HLT_PASS_RE('StrippingBc2JpsiMuLineDecision')" #check
            )
DaVinci().EventPreFilters = fltrs.filters('Filters')


#----------------------------------------------------
#tupletools
#note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daugher final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('TupleBc2JpsiMuNu_Jpsi2MuMu') #name of tuple withing the root file
dtt.ToolList = []
dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'] 

#add PV array
TTG = dtt.addTupleTool('TupleToolGeometry')
TTG.FillMultiPV = True


track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
track_tool.Verbose = True
dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
dtt.addTupleTool('TupleToolPrimaries') #PV info 
track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
track_ils.Verbose = True


TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
TT.VerboseL0 = True
TT.VerboseHlt1 = True
TT.VerboseHlt2 = True

dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 

#run on data: s41r1 & s28 & s29 are leptonic and mDST
if not isMC:
    if run_year=='2015' or run_year=='2016' or run_year=='2017': #may need to add condition for MC
        dtt.Inputs = ['Phys/{0}/Particles'.format(line)]
        DaVinci().RootInTES = '/Event/{0}'.format(stream)
    else:
        dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)] 


#branches and naming for daughter article(s)
dtt.Decay = '[B_c+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged Jpsi
dtt.addBranches({'B_plus'           :  '[B_c+ -> (J/psi(1S) -> mu+ mu-) mu+]CC',
                 'Jpsi'             :  '[B_c+ -> ^(J/psi(1S) -> mu+ mu-) mu+]CC',
                 'Mu_plus_d'        :  '[B_c+ -> (J/psi(1S) -> ^mu+ mu-) mu+]CC', 
                 'Mu_minus_d'       :  '[B_c+ -> (J/psi(1S) -> mu+ ^mu-) mu+]CC', 
                 'Mu_plus'          :  '[B_c+ -> (J/psi(1S) -> mu+ mu-) ^mu+]CC'}) 




# ***** Not working on Run II  ****************
#valid of DST
#if run_year=='2011' or run_year=='2012' or run_year=='2015': 
#    dtt.addTupleTool('TupleToolConeIsolation')
    
#RICH PID not available in run II 
if run_year=='2011' or run_year=='2012': 
    dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi)
#**************************************************


# ***** Not working on s29 - potential patch: use DV v42r6p1 (Alison's suggestion) ****************
if run_year=='2011' or run_year=='2012': 
    dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits
    #as of s24r1 does not seem to work, possibly due to MDST; tested on recommended latest DV
    dtt.addTupleTool('TupleToolConeIsolation')
#******************************************

#----------------------------------------------------
#SL Tools
from Configurables import TupleToolSLTools
dtt.ReFitPVs = True
SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools') #out of the box SL tools - ask Patrick on how to configure
SLLtool.Bmass = 6273.7
SLLtool.VertexCov = True
SLLtool.MomCov = True
#----------------------------------------------------

#----------------------------------------------------
#import DOCA tools from SL tools
from Configurables import TupleToolSLTruth

B_plus_docas = dtt.B_plus.addTupleTool("TupleToolDocas")
B_plus_docas.Name = ["Jpsi_Mu_plus"]
B_plus_docas.Location1 =[ '[B_c+ -> ^(J/psi(1S) -> mu+ mu-) mu+]CC' ] #jpsi tagged 
B_plus_docas.Location2 =[ '[B_c+ -> (J/psi(1S) -> mu+ mu-) ^mu+]CC' ] #bachelor muon tagged 

#----------------------------------------------------

#----------------------------------------------------
#configure Isolation Tools
from Configurables import TupleToolApplyIsolation
dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #added to input sandbox in ganga job file
dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
#----------------------------------------------------


#----------------------------------------------------
# TRIGGER LINES 
# list of trigger lines on the B

#RUN II: add Hlt1TwoTrackMVA 
#added dimuon lines from R(Jpsi)

trigListL0 = [     "L0HadronDecision",
                   "L0MuonDecision", 
                   "L0DiMuonDecision"
                   ]

trigListHlt1 = [    "Hlt1TrackAllL0Decision",
                    "Hlt1TrackMVADecision",
                    "Hlt1TrackMuonDecision",
                    "Hlt1TrackMuonMVADecision",
                    "Hlt1TwoTrackMVADecision",
                    "Hlt1DiMuonLowMassDecision",
                    "Hlt1DiMuonHighMassDecision"
                    ]

trigListHlt2 = [    "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision",
                    "Hlt2DiMuonDetachedJPsiDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2

B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
B_TisTos.VerboseL0 = True
B_TisTos.VerboseHlt1 = True
B_TisTos.VerboseHlt2 = True
B_TisTos.TriggerList = trigListAll 

Jpsi_TisTos = dtt.Jpsi.addTupleTool('TupleToolTISTOS')
Jpsi_TisTos.VerboseL0 = True
Jpsi_TisTos.VerboseHlt1 = True
Jpsi_TisTos.VerboseHlt2 = True
Jpsi_TisTos.TriggerList = trigListAll 

Mu_plus_d_TisTos = dtt.Mu_plus_d.addTupleTool('TupleToolTISTOS')
Mu_plus_d_TisTos.VerboseL0 = True
Mu_plus_d_TisTos.VerboseHlt1 = True
Mu_plus_d_TisTos.VerboseHlt2 = True
Mu_plus_d_TisTos.TriggerList = trigListAll 

Mu_minus_d_TisTos = dtt.Mu_minus_d.addTupleTool('TupleToolTISTOS')
Mu_minus_d_TisTos.VerboseL0 = True
Mu_minus_d_TisTos.VerboseHlt1 = True
Mu_minus_d_TisTos.VerboseHlt2 = True
Mu_minus_d_TisTos.TriggerList = trigListAll 

Mu_plus_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
Mu_plus_TisTos.VerboseL0 = True
Mu_plus_TisTos.VerboseHlt1 = True
Mu_plus_TisTos.VerboseHlt2 = True
Mu_plus_TisTos.TriggerList = trigListAll
#----------------------------------------------------


#----------------------------------------------------
#LoKis
B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
B_plus_preamble = []
B_plus_hybrid.Preambulo = B_plus_preamble 

B_plus_hybrid.Variables = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
	     "VCHI2"  : "VFASPF(VCHI2/VDOF)",
	     "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()", #proper life time (mm)
         "DTF_CTAU" : "DTF_CTAU ( 0 , True , strings('J/psi(1S)') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 0 , True , strings('J/psi(1S)') )"
    }

Jpsi_hybrid = dtt.Jpsi.addTupleTool("LoKi::Hybrid::TupleTool/Jpsi") 
Jpsi_preamble = []
Jpsi_hybrid.Preambulo = Jpsi_preamble 

Jpsi_hybrid.Variables = {
         "DTF_CTAU" : "DTF_CTAU ( 1 , True, strings('J/psi(1S)') )",  #Ask Alison how to use this exactly;
         "DTF_CTAUSIGNIFICANCE" : "DTF_CTAUSIGNIFICANCE ( 1 , True, strings('J/psi(1S)') )"
    }

Mu_plus_d_hybrid = dtt.Mu_plus_d.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus_d") 
Mu_plus_d_preamble = []
Mu_plus_d_hybrid.Preambulo = Mu_plus_d_preamble 

Mu_plus_d_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Mu_minus_d_hybrid = dtt.Mu_minus_d.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_minus_d") 
Mu_minus_d_preamble = []
Mu_minus_d_hybrid.Preambulo = Mu_minus_d_preamble 

Mu_minus_d_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/Mu_plus") 
Mu_plus_preamble = []
Mu_plus_hybrid.Preambulo = Mu_plus_preamble 

Mu_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)"#min chi2 distance relative to PV (3 sigma, I think)
        }

#---------------------------------------------------


#----------------------------------------------------
#implement DecayTreeFitter

dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV of origin
dtt.B_plus.ConsB.constrainToOriginVertex = True #take into account the neutrino
dtt.B_plus.ConsB.Verbose = True #changed to true
dtt.B_plus.ConsB.daughtersToConstrain = ['J/psi(1S)'] #constrain K Pi to Jpsi
dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks

#----------------------------------------------------


DaVinci().UserAlgorithms += [scaler]
DaVinci().UserAlgorithms += [dtt]

DaVinci().TupleFile = 'Bc2JpsiMuNu.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = run_year

#run on data: s28 & s29 are leptonic and mDST
if not isMC:
    DaVinci().Simulation = False
    DaVinci().Lumi = not DaVinci().Simulation #Lumi True
    if run_year=='2015' or run_year=='2016' or run_year=='2017': #NOTE 2015-7 LEPTONIC MDST 
        DaVinci().InputType = 'MDST'
    else:
        DaVinci().InputType = 'DST'

# Only ask for luminosity information when not using simulated data
DaVinci().EvtMax = evts


#tag assistant here: http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/dbase/conddb/release_notes.html
#as of January 2018 
if not isMC:
    if run_year=='2011':
        DaVinci().CondDBtag = 'cond-20150409'
        DaVinci().DDDBtag = 'dddb-20171030-1'
    if run_year=='2012':
        DaVinci().CondDBtag = 'cond-20150409-1'
        DaVinci().DDDBtag = 'dddb-20171030-2'
    if run_year=='2015':
        DaVinci().CondDBtag = 'cond-20171211'
        DaVinci().DDDBtag = 'dddb-20171030-3'
    if run_year=='2016':
        DaVinci().CondDBtag = 'cond-20170325'
        DaVinci().DDDBtag = 'dddb-20171030-3'
    if run_year=='2017':
        DaVinci().CondDBtag = 'cond-20170724'
        DaVinci().DDDBtag = 'dddb-20171030-3'


