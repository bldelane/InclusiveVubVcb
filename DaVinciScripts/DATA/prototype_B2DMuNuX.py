#Options file for Ganga submission for Bc2JpsiMuNu
#
#__author__ = Blaise R. Delaney, University of Cambridge
#__email__ = blaise.delaney@cern.ch
#
# Ganga submission file for Ganga submission for DATA for channels:
#  - Bc->D0(->K pi) Mu Nu X
#  - Bc->D0(->K 3pi) Mu Nu X
#
# Note on stripping lines:
#   - B2DMuNuX_D0Line is used for both Run I and Run II for D0->KPi
#   - B2DMuNuX_D0_K3PiLine is used for Run II only of D0->K3Pi, as not available for Run I as of
#   19/10/18
#   - b2D0MuXK3PiCharmFromBSemiLine
# 
# DaVinci Versions:
#
#    Runs I,II Stream: Semileptonic (DST) and Charm (MDST)
#
#    -stripping 21r1p1 (Incremental restripping of 2011 data): DV v36r1p5 (stripping+tuples) {DV
#    v39r1p1 was suggested as the correct version but now deprecated since v36r1p5 is the right
#    version and issues of compatibility between the re-stripping and tuplization (TTGeometry) on
#    run I Jpsi MC}
#    -stripping 21r0p1 (Incremental stripping of 2012 data (autumn 2014)): DV v36r1p5
#    -stripping 24r1 (Full restripping of 2015 data): DV v38r1p3->v38r1p7
#    -stripping 28r1 (Full restripping  for 2016 data): DV v41r4p3->v41r4p5, DST
#    -stripping 29r2 (Full restripping of 2017 pp data): DV v42r5p1->v42r7p3, DST
#
# To get tags:
# e.g. : lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst
#=======================================================================================================================

#=======================================================================================================================
# TODO Look into SLTRUTH Implementation
# TODO possible error: k3pi-specific tools if loop condition
# TODO AutoDB() Tags in Ganga
# TODO Double-check stripping lines
# TODO test and print IgnoreFilter=True
# TODO check function to implement restrip
# TODO check if mDST: need to use ROOTINTES
# TODO look into restripping extras for s21 
#=======================================================================================================================


#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

# imports for restripping
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import EventNodeKiller



# Scaler for DATA
from Configurables import TrackScaleState as SCALER
scaler = SCALER('StateScale')
# Smear for MC
from Configurables import TrackSmearState as SMEAR
smear = SMEAR('StateSmear')


#----------------------------------------------------
# ***** PARAMETERS TO CHANGE *****

#Runs, years
run_year = '2015' #2011, 2012, 2015, 2016, 2017

#stream = 'Charm' for Charmline, D2K2Pi Mode, Run I
#note that Charm is mDST 
stream = 'Semileptonic' #Charm

#implement the restripping and smearing 
isMC = False #True 

#MC version
sim = '08a' #08{a,e}, 09{b}

#set to -1 for full run at submission
evts=-1
#----------------------------------------------------


# Use the local input data
IOHelper().inputFiles([
        '/usera/delaney/private/Bc2D0MuNuX/DaVinciScripts/DATA/Bookkeping/D0Mode/15/00069078_00017887_1.semileptonic.dst'
        ], clear=True)


lineKPi = 'B2DMuNuX_D0' #s21r1p1, s21r0p1, s24r1, s28r1, s29r2
lineK3Pi = 'B2DMuNuX_D0_K3Pi'  # s24r1, s28r1, s29r2
#

dtt_B2D0MuNu_D02KPi = DecayTreeTuple('TupleB2D0Mu_D02KPi') #K Pi mode, Run I and Run II
dtt_B2D0MuNu_D02KPi.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, lineKPi)]
dtt_B2D0MuNu_D02KPi.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0
dtt_B2D0MuNu_D02KPi.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC',
                 'Pi_1'      :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC',
                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})


dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine = DecayTreeTuple('TupleB2D0Mu_D02K3Pi_B2DMuNuXLine') #K3Pi mode,
dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, lineK3Pi)]
dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+ ^pi- ^pi+]CC) ^mu+]CC'  # conjugate, note that Bc and B+ decay to oppositely charged D0
dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine.addBranches({'B_plus': '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                                               'D0': '[Xb -> ^([[D0]cc -> K- pi+ pi- pi+]CC) mu+]CC',
                                               'K_minus': '[Xb -> ([[D0]cc -> ^K- pi+ pi- pi+]CC) mu+]CC',
                                               'Pi_1': '[Xb -> ([[D0]cc -> K- ^pi+ pi- pi+]CC) mu+]CC',
                                               'Pi_2': '[Xb -> ([[D0]cc -> K- pi+ ^pi- pi+]CC) mu+]CC',
                                               'Pi_3': '[Xb -> ([[D0]cc -> K- pi+ pi- ^pi+]CC) mu+]CC',
                                               'Mu_plus': '[Xb -> ([[D0]cc -> K- pi+ pi- pi+]CC) ^mu+]CC'})

from Configurables import GaudiSequencer
mysequencer = GaudiSequencer('Sequence')
mysequencer_k3pi = GaudiSequencer('Sequence_k3pi')
#


'''
# from PhysConf.Filters import LoKi_Filters
fltrsK3pi = LoKi_Filters(
    #                # try to save time by checking DecReports Exist and that line is not empty
    VOID_DecRep_Code_K3pi = "EXISTS('/Event/Strip/Phys/DecReports')"#,
    # STRIP_Code_K3pi = "HLT_PASS_RE('Stripping{0}Decision')".format(lineKPi),
    # VOID_PV_Code_K3pi = " CONTAINS    ('Rec/Vertex/Primary') >= 1 " #check that there is at
    #                # least one PV
)
'''


from Configurables import LoKi__HDRFilter
from Configurables import LoKi__VoidFilter
'''
how to implement all 3 levels of filters?
'''

sf = LoKi__HDRFilter( 'StripPassFilter', Code="HLT_PASS('Stripping"+lineKPi+"Decision')", Location="/Event/Strip/Phys/DecReports" )
vf = LoKi__VoidFilter( 'MultiPVFilter', Code="CONTAINS('Rec/Vertex/Primary') >= 1 ")
vf_k3pi = LoKi__VoidFilter( 'MultiPVFilter_k3pi', Code="CONTAINS('Rec/Vertex/Primary') >= 1 ")
sf_k3pi = LoKi__HDRFilter( 'StripPassFilter_k3pi', Code="HLT_PASS('Stripping"+lineK3Pi+"Decision')", Location="/Event/Strip/Phys/DecReports" )


from Configurables import PrintDecayTree
'''
Check correct behaviour
'''

pt = PrintDecayTree(name = 'DTT_KPi_PrintTree', Inputs = [ '/Event/{0}/Phys/{1}/Particles'.format(stream, lineKPi) ])
pt_k3pi = PrintDecayTree(name = 'DTT_K3Pi_PrintTree', Inputs = [ '/Event/{0}/Phys/{1}/Particles'.format(stream, lineK3Pi) ])



'''
correct order of sequences?
'''

mysequencer.Members += [ sf ]
mysequencer.Members += [ vf ]
mysequencer.Members += [dtt_B2D0MuNu_D02KPi]
mysequencer.Members += [ pt ]

mysequencer_k3pi.Members += [ sf_k3pi ]
mysequencer_k3pi.Members += [ vf_k3pi ]
mysequencer_k3pi.Members += [dtt_B2D0MuNu_D02K3Pi_B2DMuNuXLine]
mysequencer_k3pi.Members += [ pt_k3pi ]


# top sequence, handles the event by event loop; must attach here all the NTupling sequences and flag IgnoreFilterPassed
TopSequence = GaudiSequencer('TopLevelSequence')
TopSequence.Members += [mysequencer]
TopSequence.Members += [mysequencer_k3pi]
TopSequence.IgnoreFilterPassed = True


DaVinci().UserAlgorithms += [ scaler ]
DaVinci().UserAlgorithms += [ TopSequence]


DaVinci().TupleFile = 'Bc2D0MuNuX.root'
DaVinci().PrintFreq = 1000
DaVinci().InputType = 'DST'
DaVinci().DataType = run_year
DaVinci().HistogramFile = 'Bc2D0MuNuX_DVHistos.root'
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation #Lumi True
DaVinci().EvtMax = evts


