#Author: Blaise R. Delaney
#Email: delaney@hep.phy.cam.ac.uk
#Institution: Cavendish Laboratory, University of Cambridge

#basic imports
from Configurables import DaVinci 
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
from LoKiPhys.decorators import *

#configure SL Isolation Tools, SL mass correction and momentum scaling
#from SemileptonicCommonTools import * 
from Configurables import TrackScaleState as SCALER
scaler = SCALER('StateScale')

# Use the local input data
IOHelper().inputFiles([
 
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000014_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000028_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000055_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000056_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000070_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000084_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000098_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000112_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000126_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000140_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000154_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000168_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000182_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000211_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000212_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000226_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000241_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000256_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000270_1.semileptonic.dst',
'/afs/cern.ch/work/b/bldelane/private/LFN_s21r1/00041840_00000284_1.semileptonic.dst'
  
    ], clear=True)



#stream and stripping line 
stream = 'Semileptonic'
line = 'b2D0MuXB2DMuNuXLine' #run I (available for run II too)

#look at events with relevant stripping line
fltrs = LoKi_Filters (
            STRIP_Code = "HLT_PASS_RE('Strippingb2D0MuXB2DMuNuXLineDecision')" #check
            )
DaVinci().EventPreFilters = fltrs.filters('Filters')


#----------------------------------------------------
#tupletools
#note: DecayTreeTuple includes by default:

#   -TupleToolKinematic
#   -TupleToolPid (on daugher final state cands)
#   -TupleToolANNPID (on daughter final state cands)
#   -TupleToolGeometry (IP, vertex position etc..)
#   -TupleToolEventInfo

#create the nTuple within the output .ROOT file with assigned name, from stripping line
dtt = DecayTreeTuple('TupleB2D0Mu_D02KPi') #name of tuple withing the root file
dtt.ToolList = []
dtt.ToolList += ['TupleToolKinematic', 'TupleToolPid', 'TupleToolANNPID', 'TupleToolEventInfo'] 

#add PV array
TTG = dtt.addTupleTool('TupleToolGeometry')
TTG.FillMultiPV = True

track_tool = dtt.addTupleTool('TupleToolTrackInfo') #tracks, verbose
track_tool.Verbose = True
dtt.addTupleTool('TupleToolRecoStats') #Reco stats, from RecSummary  
dtt.addTupleTool('TupleToolPrimaries') #PV info 
track_ils =dtt.addTupleTool('TupleToolTrackIsolation')
track_ils.Verbose = True
dtt.addTupleTool('TupleToolConeIsolation')

TT = dtt.addTupleTool('TupleToolTrigger') #trigger lines request (?)
TT.VerboseL0 = True
TT.VerboseHlt1 = True
TT.VerboseHlt2 = True

dtt.addTupleTool('TupleToolRICHPid') #RICH info for long lived tracks, eg (e, mu, k, p, pi)
dtt.addTupleTool('TupleToolPropertime') #proper time, in ns 


dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)] 
dtt.Decay = '[Xb -> ^([[D0]cc -> ^K- ^pi+]CC) ^mu+]CC' #conjugate, note that Bc and B+ decay to oppositely charged D0


#branches and naming for dauughter article(s)
dtt.addBranches({'B_plus'    :  '[Xb -> ([[D0]cc -> K- pi+]CC) mu+]CC',
                 'D0'        :  '[Xb -> ^([[D0]cc -> K- pi+]CC) mu+]CC',
                 'K_minus'   :  '[Xb -> ([[D0]cc -> ^K- pi+]CC) mu+]CC', 
                 'Pi_plus'   :  '[Xb -> ([[D0]cc -> K- ^pi+]CC) mu+]CC', 
                 'Mu_plus'   :  '[Xb -> ([[D0]cc -> K- pi+]CC) ^mu+]CC'})

dtt.B_plus.addTupleTool('TupleToolTrackTime') #track the average residual time of a track from OT hits

from Configurables import TupleToolSLTools
dtt.ReFitPVs = True
SLLtool = dtt.B_plus.addTupleTool(TupleToolSLTools, 'TupleToolSLTools') #out of the box SL tools - ask Patrick on how to configure
SLLtool.Bmass = 6273.7
SLLtool.VertexCov = True
SLLtool.MomCov = True

#----------------------------------------------------




#----------------------------------------------------
#configure Isolation Tools
from Configurables import TupleToolApplyIsolation
dtt.B_plus.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
dtt.B_plus.TupleToolApplyIsolation.WeightsFile="weights.xml" #cloned from RLc repo
dtt.B_plus.addTupleTool('TupleToolApplyIsolation/TupleToolApplyIsolation')
#----------------------------------------------------



#----------------------------------------------------
# TRIGGER LINES 
# list of trigger lines on the B

#RUN II: add Hlt1TwoTrackMVA 

trigListL0 = [      "L0HadronDecision" ,
                    "L0MuonDecision" ]

trigListHlt1 = [ "Hlt1TrackAllL0Decision",
                    "Hlt1TrackMVADecision",
                    "Hlt1TrackMuonDecision",
                    "Hlt1TrackMuonMVADecision", 
                    "Hlt1TwoTrackMVADecision"
                    ] 

trigListHlt2 = [ "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision"
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    "Hlt2TopoMu2BodyBBDTDecision",
                    "Hlt2TopoMu3BodyBBDTDecision",
                    "Hlt2TopoMu4BodyBBDTDecision",
                    "Hlt2SingleMuonDecision"
                    ]

trigListAll = trigListL0 + trigListHlt1 + trigListHlt2
B_TisTos = dtt.B_plus.addTupleTool('TupleToolTISTOS')
B_TisTos.VerboseL0 = True
B_TisTos.VerboseHlt1 = True
B_TisTos.VerboseHlt2 = True
B_TisTos.TriggerList = trigListAll 

D_TisTos = dtt.D0.addTupleTool('TupleToolTISTOS')
D_TisTos.VerboseL0 = True
D_TisTos.VerboseHlt1 = True
D_TisTos.VerboseHlt2 = True
D_TisTos.TriggerList = trigListAll 

K_TisTos = dtt.K_minus.addTupleTool('TupleToolTISTOS')
K_TisTos.VerboseL0 = True
K_TisTos.VerboseHlt1 = True
K_TisTos.VerboseHlt2 = True
K_TisTos.TriggerList = trigListAll 

Pi_TisTos = dtt.Pi_plus.addTupleTool('TupleToolTISTOS')
Pi_TisTos.VerboseL0 = True
Pi_TisTos.VerboseHlt1 = True
Pi_TisTos.VerboseHlt2 = True
Pi_TisTos.TriggerList = trigListAll 

Mu_TisTos = dtt.Mu_plus.addTupleTool('TupleToolTISTOS')
Mu_TisTos.VerboseL0 = True
Mu_TisTos.VerboseHlt1 = True
Mu_TisTos.VerboseHlt2 = True
Mu_TisTos.TriggerList = trigListAll
#----------------------------------------------------





#----------------------------------------------------
#LoKis
B_plus_hybrid = dtt.B_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B_plus") 
B_plus_preamble = []
B_plus_hybrid.Preambulo = B_plus_preamble 

B_plus_hybrid.Variables = {
	     "BPVFDCHI2" : "BPVVDCHI2", #CHI2 distance from best PV
	     "VCHI2"  : "VFASPF(VCHI2/VDOF)",
	     "BPVIPCHI2" : "BPVIPCHI2()", #IP CHI2 relativeto BPV
	     "BPVDIRA" : "BPVDIRA",
         "ETA" : "ETA", #pseudorapidity
	     "LTIME" : "BPVLTIME()" #proper life time (mm)
    }


Mu_plus_hybrid = dtt.Mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu_plus") 
Mu_plus_preamble = []
Mu_plus_hybrid.Preambulo = Mu_plus_preamble 

Mu_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


Pi_plus_hybrid = dtt.Pi_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi_plus") 
Pi_plus_preamble = []
Pi_plus_hybrid.Preambulo = Pi_plus_preamble 

Pi_plus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }


K_minus_hybrid = dtt.K_minus.addTupleTool("LoKi::Hybrid::TupleTool/K_minus_plus") 
K_minus_preamble = []
K_minus_hybrid.Preambulo = K_minus_preamble 

K_minus_hybrid.Variables = {
	     "TRCHI2DOF": "TRCHI2DOF", #return tracks with low chi2 PDOF
	     "TRGHOSTPROB" : "TRGHOSTPROB", #track ghost prob
         "ETA" : "ETA",
	     "MIPCHI2PV" : "MIPCHI2DV(PRIMARY)" #min chi2 distance relative to PV (3 sigma, I think)
        }

#----------------------------------------------------




#----------------------------------------------------
#implement DecayTreeFitter

dtt.B_plus.addTupleTool('TupleToolDecayTreeFitter/ConsB') #constrain to PV of origin
#dtt.B_plus.ConsB.constrainToOriginVertex = True #take into account the neutrino
dtt.B_plus.ConsB.Verbose = True
dtt.B_plus.ConsB.daughtersToConstrain = ['D0'] #constrain K Pi to D0
dtt.B_plus.ConsB.UpdateDaughters = True #store info on final state tracks

#----------------------------------------------------





#DaVinci.RootInTes() for mdst 
DaVinci().UserAlgorithms += [scaler]
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = '/afs/cern.ch/work/b/bldelane/public/ROOTfiles/B2D0Mu_D02KPi_'+line+'.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2011'
DaVinci().Simulation = False
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1



# MC files conditions; find them in BK; for real data: default is the largest and the latest
#DaVinci().CondDBtag = ''
#DaVinci().DDDBtag = ''
