# Discussion logs for Ganga Submissions

A compiled list of the most pressing updates regarding Ganga submissions.

---

### Discussion with Alison, 05/10/2018    

- DTF should be used, but setting `vertex_constrains=False`. Reason: forcing the muon and daughter hadron ($`D^0`$ and $`J/\psi`$) tracks to point to the $`pp`$ PV causes the tracks and daughter hadron mass resolution to be compromised, as the neutrino is not reconstructed. All the while, DTF itself is useful to improve the mass resolution of the $`D^0`$ and $`J/\psi`$ and momentum resolution.
- The correct DV version to use for Run I is `v36r1p5` for both restripping and NTuple production for data and MC. Assuming the SL tools work fine. 
- Take a look in the gitlab repo of Alison to produce an **opts file containing the `Kpi` and `K3pi` modes** as a template. She also implements cuts on the pairwise cone of K's and pi's to veto cones that are too small. _Ask her about this before resubmitting (with the correct DV version!)._