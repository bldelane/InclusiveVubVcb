# Submission log of Dirac jobs for data and MC NTuples

Up-to-date submission logs. Document structure: main table with logs marked by date below.

Path to ganga jobs: `/usera/delaney/Analysis/gangadir/workspace/delaney/LocalXML/`

## Data and MC 

|   Channel  | Data/MC  |  Job ID   |  Input  |  Summary  | Comment |
| :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
|   D0(->K pi)       |   MC   |     170 | B+ 2012 Up Sim08a | 100% complete | |
|   D0(->K pi)       |   MC   |     171 | B+ 2012 Down Sim08a | 97% complete | **One subjobs keeps failing regardless of backend()** |
|   D0(->K pi)       |   MC   |     172 | Bc 2012 Down Sim08e | 100% complete |  |
|   D0(->K pi)       |   MC   |     173 | Bc 2012 Up Sim08e | 83.3% complete | **One subjobs keeps failing regardless of backend()**  |
|   D0(->K pi)       |   MC   |     202 | B+ 2011 Down Sim08e | **100% failed** | **Fix submission**  |
|   D0(->K pi)       |   MC   |     203 | B+ 2011 Up Sim08e | **100% failed** |  **Fix submission** |
|   D0(->K pi)       |   Data   |     178 |  2011 Down | 99.5% complete |  |
|   D0(->K pi)       |   Data   |     179 |  2011 Up | 78.7 %% complete |  |
|   D0(->K pi)       |   Data   |     180 |  2012 Down |  96.1% failed, 3.9%  complete | **Resubmit w/o DTF and small sj size** |
|   D0(->K pi)       |   Data   |     181 |  2012 Up |  25.3% failed, 31.1%  complete | **Resubmit w/o DTF and small sj size** |
|   D0(->K pi)       |   Data   |     184 |  2015 Down |  force-fail, 34.5%  complete | **Resubmit w/o DTF and small sj size** |
|   D0(->K pi)       |   Data   |     185 |  2015 Up |  84.1%  complete, rest resubmitted |  |
|   D0(->K pi)       |   Data   |     188 |  2016 Up |  **killed** | **Force fail status** |
|   D0(->K pi)       |   Data   |     189 |  2016 Down |  **killed** | **CPU-efficient submission required: s21r1p1 SL DST** |
|   D0(->K pi)       |   Data   |     194 |  2017 Up |  **killed** | **CPU-efficient submission required** |
|   D0(->K pi)       |   Data   |     195 |  2017 Down |  **killed** | **CPU-efficient submission required** |

