# Cloning DaVinci and Making a local build with SLTools 

This is probably taking a couple of redundant step but seems to work fine with me

1. `lb-dev --name DaVinciDev DaVinci/vXXrYpZ`
2. `git lb-use DaVinci; git lb-use Analysis`
3.  Match the correct version of Analysis by inspecting the [LHCb DaVinci Project](http://lhcbdoc.web.cern.ch/lhcbdoc/davinci/); command: `git lb-checkout Analysis/vXrY Phys/DecayTreeTuple`
4.  `it lb-use SemileptonicCommonTools https://:@gitlab.cern.ch:8443/lhcb-slb/SemileptonicCommonTools.git`
5.  `git lb-checkout SemileptonicCommonTools/master Phys/SemileptonicCommonTools`
6.  *Deprecated*: Copy the tools of interest from `Phys/SemileptonicCommonTools/src/` to `Phys/DecayTreeTuple/src/`; *Replaced by*: delete source files for TTTauTools
7.  `make configure`
8.  `make`
9.  `make install`


### A couple of points

The important thing is that the build of the local DV repository is performed using the appropriate platform. Inspect ~/.bashrc for the `SetupLHCb` and platform selection command `LbLogin -c x86_64-<slc6/centos7>-gcc<version>-opt`.
The list of platforms supported by any given DV version can be found in `/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/<DAVINCI_version>/InstallArea`.

##### A snippet from my local DaVinciDev folder

```
In order to proceed with a correct submission of a ganga job, the following apply:

    - Ganga is platform independent. This means that Ganga may be launched in its most recent
      version.
    - The DV builds depend on the platform (slc7 or centOS7) and the gcc compiler version
    - To browse the DV build requirements: ls
      /cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/<DAVINCI_version>/InstallArea
    - For builds that require slc6, must accordingly ssh into an slc6 machine
    - In the correct environment (slc6 or centos7), select the correct gcc version by LbLogin -c
      x86_64-<slc6/centos7>-gcc<version>-opt
    - Finally, in the job preparation file, the required platform must be specified in the field j.application.platform='x86_64-<slc6/centos7>-gcc<version>-opt'


 Note, for restripping MC configurations, look at the code in: /cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r368/options/DaVinci

 *Note*: when restripping to file, make sure to use enable Packing, and track chi2 tightening.

 Also, for opts file, should implement filter at the stripping line stage.
```
