#ntuplizer for B(c)+ -> D(s)+D0(bar) data and MC
#lb-run DaVinci v37r2p4 gaudirun.py bcdpdztup.py localfilesMC.py
#lb-run DaVinci v38r0 gaudirun.py bcdpdztup.py localfile.py

#lb-run -c x86_64-slc6-gcc48-opt DaVinci v36r1p1 gaudirun.py bcdpdztup.py localfilesMC.py

# To get tags:
#lb-run Bender/latest get-dbtags /lhcb/MC/2012/ALLSTREAMS.DST/00049331/0000/00049331_00000002_2.AllStreams.dst

MC=False
sim="08"

year=2017
#year=2015
#year=2012
#year=2011


from GaudiConf import IOHelper
from Configurables import DaVinci,DecayTreeTuple,MCDecayTreeTuple,SubstitutePID,FilterDesktop
from PhysSelPython.Wrappers import Selection, MergedSelection, SelectionSequence, DataOnDemand
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import TupleToolMCTruth, MCTupleToolP2VV


if not MC: 
    #from Configurables import CondDB
    #CondDB ( LatestGlobalTagByDataType = str(year) )
    DaVinci().RootInTES = '/Event/Bhadron'
    DaVinci().InputType = 'MDST'
    
    from Configurables import TrackScaleState as SCALER
    scaler = SCALER('StateScale')
    DaVinci().UserAlgorithms += [scaler]

if MC:
    from Configurables import TrackSmearState as SMEAR
    smear = SMEAR('StateSmear')
    DaVinci().UserAlgorithms += [smear]
    
    from LHCbKernel.Configuration import DEBUG
    from Configurables import LoKi__VoidFilter, LoKi__Hybrid__CoreFactory
    workaroundAlg = LoKi__VoidFilter("Workaround_LHCBPS1417",
            Preambulo=[ "# FIXME: temporary workaround for LHCBPS-1417",
                        "import cppyy",
                        "cppyy.gbl.gSystem.Load('libLHCbKernelDict.so')",
                        "from LoKiNumbers.decorators import *" ],
            Code="FALL",
            OutputLevel=DEBUG)
    factoryName = "WorkaroundFactory"
    workaroundAlg.addTool(LoKi__Hybrid__CoreFactory, name=factoryName)
    factory = getattr(workaroundAlg, factoryName)
    factory.Modules = ["LoKiCore.basic"]
    workaroundAlg.Factory = factory.getFullName()
    
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive
    
    if year==2017: stripping='stripping29'
    if year==2015: stripping='stripping24'
    if year==2012: stripping='stripping21'
    if year==2011: stripping='stripping21r1'
    config=strippingConfiguration(stripping)
    archive=strippingArchive(stripping)
    streams=buildStreams(stripping=config,archive=archive)
    
    MyStream = StrippingStream("MyStream")
    if year>=2015: 
        MyLines = [ 'StrippingBc2DD0D2HHHD02HHBeauty2CharmLine',
                    'StrippingBc2DD0D2HHHD02KHHHBeauty2CharmLine' ]
    else: 
        MyLines = [ 'StrippingB2D0DBeauty2CharmLine',
                    'StrippingB2D0DD02K3PiBeauty2CharmLine' ]
    
    for stream in streams:
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines( [ line ])
    
    from Configurables import ProcStatusCheck
    filterBadEvents=ProcStatusCheck()
    
    # HDRLocation = "SomeNonExistingLocation",
    sc=StrippingConf( Streams= [ MyStream ],
                      MaxCandidates = 2000,
                      AcceptBadEvents = False,
                      BadEventSelection = filterBadEvents)


# list of trigger lines
triggerListL0 = [ "L0HadronDecision" ]

triggerListHlt1 = [ "Hlt1TrackAllL0Decision",
                    "Hlt1TrackMVADecision"]

triggerListHlt2 = [ "Hlt2Topo2BodyDecision",
                    "Hlt2Topo3BodyDecision",
                    "Hlt2Topo4BodyDecision",
                    "Hlt2Topo2BodyBBDTDecision",
                    "Hlt2Topo3BodyBBDTDecision",
                    "Hlt2Topo4BodyBBDTDecision",
                    "Hlt2IncPhiDecision",
                    "Hlt2IncPhiSidebandsDecision"]
	
triggerListAll = triggerListL0 + triggerListHlt1 + triggerListHlt2

Dp2Ds = SubstitutePID('Dp2Ds',
                      Code="DECTREE('[Xb -> D+ [D~0]cc]CC')",
                      Substitutions = {'Xb -> ^(D+ -> K+ pi+ K-) D0': 'D_s+','Xb -> ^(D- -> K- pi- K+) D0': 'D_s-'}
                      )

if year==2015:
    location_Kpi='Phys/Bc2DD0D2HHHD02HHBeauty2CharmLine/Particles'
    location_Kpipipi='Phys/Bc2DD0D2HHHD02KHHHBeauty2CharmLine/Particles'
else:
    location_Kpi='Phys/B2D0DBeauty2CharmLine/Particles'
    location_Kpipipi='Phys/B2D0DD02K3PiBeauty2CharmLine/Particles'

Dp2DsKpiSel = Selection('Dp2DsKpiSel',
                        Algorithm=Dp2Ds,
                        RequiredSelections = [DataOnDemand(Location = location_Kpi)])
Dp2DsKpipipiSel = Selection('Dp2DsKpipipiSel',
                            Algorithm=Dp2Ds,
                            RequiredSelections = [DataOnDemand(Location = location_Kpipipi)])
Dp2DsKpiSeq = SelectionSequence('Dp2DsKpiSeq', TopSelection=Dp2DsKpiSel)
Dp2DsKpipipiSeq = SelectionSequence('Dp2DsKpipipiSeq',TopSelection=Dp2DsKpipipiSel)


dtt_DpDzKpi = DecayTreeTuple('TupleBc2DpDzKpi')
dtt_DpDzKpi.Inputs = [Dp2DsKpiSeq.outputLocation()]
dtt_DpDzKpi.Decay =            '[ (Xb -> ^(Xc ->  ^(pi+|K+) ^pi+ ^K-) [^([D~0]cc -> ^K+ ^pi- )]CC )]CC'
dtt_DpDzKpi.addBranches({'Bc': '[^(Xb ->  (Xc ->   (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi- )]CC )]CC',
                         'Dp': '[ (Xb -> ^(Xc ->   (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi- )]CC )]CC',
                         'p1': '[ (Xb ->  (Xc ->  ^(pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi- )]CC )]CC',
                         'p2': '[ (Xb ->  (Xc ->   (pi+|K+) ^pi+  K-) [ ([D~0]cc ->  K+  pi- )]CC )]CC',
                         'p3': '[ (Xb ->  (Xc ->   (pi+|K+)  pi+ ^K-) [ ([D~0]cc ->  K+  pi- )]CC )]CC',
                         'Dz': '[ (Xb ->  (Xc ->   (pi+|K+)  pi+  K-) [^([D~0]cc ->  K+  pi- )]CC )]CC',
                         'z1': '[ (Xb ->  (Xc ->   (pi+|K+)  pi+  K-) [ ([D~0]cc -> ^K+  pi- )]CC )]CC',
                         'z2': '[ (Xb ->  (Xc ->   (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+ ^pi- )]CC )]CC'})

dtt_DpDzKpipipi = DecayTreeTuple('TupleBc2DpDzKpipipi')
dtt_DpDzKpipipi.Inputs = [Dp2DsKpipipiSeq.outputLocation()]
dtt_DpDzKpipipi.Decay =            '[ (Xb -> ^(Xc -> ^(pi+|K+) ^pi+ ^K-) [^([D~0]cc -> ^K+ ^pi- ^pi+ ^pi-)]CC )]CC'
dtt_DpDzKpipipi.addBranches({'Bc': '[^(Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'Dp': '[ (Xb -> ^(Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'p1': '[ (Xb ->  (Xc -> ^(pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'p2': '[ (Xb ->  (Xc ->  (pi+|K+) ^pi+  K-) [ ([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'p3': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+ ^K-) [ ([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'Dz': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [^([D~0]cc ->  K+  pi-  pi+  pi-)]CC )]CC',
                             'z1': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc -> ^K+  pi-  pi+  pi-)]CC )]CC',
                             'z2': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+ ^pi-  pi+  pi-)]CC )]CC',
                             'z3': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi- ^pi+  pi-)]CC )]CC',
                             'z4': '[ (Xb ->  (Xc ->  (pi+|K+)  pi+  K-) [ ([D~0]cc ->  K+  pi-  pi+ ^pi-)]CC )]CC'})

#dtt_DpDzKpi.Bc.addTupleTool('TupleToolDecayTreeFitter/ConsB')
#dtt_DpDzKpi.Bc.ConsB.constrainToOriginVertex = True
#dtt_DpDzKpi.Bc.ConsB.Verbose = True
#dtt_DpDzKpi.Bc.ConsB.daughtersToConstrain = ['D+','D_s+','D0']
#dtt_DpDzKpi.Bc.ConsB.UpdateDaughters = True


if MC:
    mctupleDpDz = MCDecayTreeTuple("MCDecayTreeTupleDpDz")
    mctupleDpDz.Decay =            '[ (B_c+ => ^(D_s+|D+) ^D~0 )]CC'
    mctupleDpDz.addBranches({'Bc': '[^(B_c+ =>  (D_s+|D+)  D~0 )]CC',
                             'Dp': '[ (B_c+ => ^(D_s+|D+)  D~0 )]CC',
                             'Dz': '[ (B_c+ =>  (D_s+|D+) ^D~0 )]CC'})
    mctupleDpDz.ToolList = [
        "MCTupleToolHierarchy",
        "MCTupleToolKinematic",
        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
        ]
    mctupleDpDz.addTupleTool("TupleToolEventInfo")

    mctupleDPDz = MCDecayTreeTuple("MCDecayTreeTupleDPDz")
    mctupleDPDz.Decay =            '[ (B_c+ => ^((D*_s+|D*(2010)+) -> ^(D_s+|D+) ^(pi0|gamma) )  ^D~0 )]CC'
    mctupleDPDz.addBranches({'Bc': '[^(B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )   D~0 )]CC',
                             'DP': '[ (B_c+ => ^((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )   D~0 )]CC',
                             'Dp': '[ (B_c+ =>  ((D*_s+|D*(2010)+) -> ^(D_s+|D+)  (pi0|gamma) )   D~0 )]CC',
                             'sp': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+) ^(pi0|gamma) )   D~0 )]CC',
                             'Dz': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )  ^D~0 )]CC'})
    mctupleDPDz.ToolList = [
        "MCTupleToolHierarchy",
        "MCTupleToolKinematic",
        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
        ]
    mctupleDPDz.addTupleTool("TupleToolEventInfo")

    mctupleDpDZ = MCDecayTreeTuple("MCDecayTreeTupleDpDZ")
    mctupleDpDZ.Decay =            '[ (B_c+ => ^(D_s+|D+) ^(D*(2007)~0 -> ^D~0 ^(pi0|gamma) ) )]CC'
    mctupleDpDZ.addBranches({'Bc': '[^(B_c+ =>  (D_s+|D+)  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'Dp': '[ (B_c+ => ^(D_s+|D+)  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'DZ': '[ (B_c+ =>  (D_s+|D+) ^(D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'Dz': '[ (B_c+ =>  (D_s+|D+)  (D*(2007)~0 -> ^D~0  (pi0|gamma) ) )]CC',
                             'sz': '[ (B_c+ =>  (D_s+|D+)  (D*(2007)~0 ->  D~0 ^(pi0|gamma) ) )]CC'})
    mctupleDpDZ.ToolList = [
        "MCTupleToolHierarchy",
        "MCTupleToolKinematic",
        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
        ]
    mctupleDpDZ.addTupleTool("TupleToolEventInfo")

    mctupleDPDZ = MCDecayTreeTuple("MCDecayTreeTupleDPDZ")
    mctupleDPDZ.Decay =            '[ (B_c+ => ^((D*_s+|D*(2010)+) -> ^(D_s+|D+) ^(pi0|gamma) ) ^(D*(2007)~0 -> ^D~0 ^(pi0|gamma) ) )]CC'
    mctupleDPDZ.addBranches({'Bc': '[^(B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'DP': '[ (B_c+ => ^((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'Dp': '[ (B_c+ =>  ((D*_s+|D*(2010)+) -> ^(D_s+|D+)  (pi0|gamma) )  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'sp': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+) ^(pi0|gamma) )  (D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'DZ': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) ) ^(D*(2007)~0 ->  D~0  (pi0|gamma) ) )]CC',
                             'Dz': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )  (D*(2007)~0 -> ^D~0  (pi0|gamma) ) )]CC',
                             'sz': '[ (B_c+ =>  ((D*_s+|D*(2010)+) ->  (D_s+|D+)  (pi0|gamma) )  (D*(2007)~0 ->  D~0 ^(pi0|gamma) ) )]CC'})
    mctupleDPDZ.ToolList = [
        "MCTupleToolHierarchy",
        "MCTupleToolKinematic",
        "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
        ]
    mctupleDPDZ.addTupleTool("TupleToolEventInfo")




comvars={"ID"       : "ID",
         "M"        : "M",
         "MM"       : "MM",
         "PX"       : "PX",
         "PY"       : "PY",
         "PZ"       : "PZ",
         "P"        : "P",
         "ETA"      : "ETA",
         "FD_BPV"        : "BPVVD",
         "FD_BPV_SIGNED" : "BPVVDSIGN",
         "FDCHI2_BPV"    : "BPVVDCHI2",
         "RHO_BPV" : "BPVVDRHO",
         "Z_BPV"   : "BPVVDZ",
         "VCHI2"    : "VFASPF(VCHI2)",
         "VCHI2DOF" : "VFASPF(VCHI2/VDOF)",
         "BPVLTIME" : "BPVLTIME()",
         "BPVLTCHI2" : "BPVLTCHI2()",
         "DIRA_BPV" : "BPVDIRA",
         "BPVIP"    : "BPVIP()",
         "BPVIPCHI2": "BPVIPCHI2()",
         "ENDVTX_X" : "VFASPF(VX)",
         "ENDVTX_Y" : "VFASPF(VY)",
         "ENDVTX_Z" : "VFASPF(VZ)",
         "LT_BPV"        : "BPVLTIME('PropertimeFitter/ProperTime::PUBLIC')",
         "LTCHI2_BPV"    : "BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC')",
         "LTFITCHI2_BPV" : "BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC')"}

Bcvars={"LV01"     : "LV01",
        "LV02"     : "LV02"}

DTFvars={
        "DTF_M_Bc"    : "DTF_FUN ( M , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAU_Bc" : "DTF_CTAU ( 0 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAU_Dp" : "DTF_CTAU ( 1 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAU_Dz" : "DTF_CTAU ( 2 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAUS_Bc": "DTF_CTAUSIGNIFICANCE ( 0 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAUS_Dp": "DTF_CTAUSIGNIFICANCE ( 1 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CTAUS_Dz": "DTF_CTAUSIGNIFICANCE ( 2 , True, strings(['D+','D_s+','D0']) )",
        "DTF_CHI2"    : "DTF_CHI2 ( True, strings(['D+','D_s+','D0']) )",
        "DTF_CHI2NDOF": "DTF_CHI2NDOF ( True, strings(['D+','D_s+','D0']) )",
}

Dpvars={"LV01"     : "LV01",
        "LV02"     : "LV02",
        "LV03"     : "LV03",
        "M12"      : "M12",
        "M13"      : "M13",
        "M23"      : "M23",
        "WM_pipiK" : "WM('pi+','pi+','K-')",
        "WM_KpiK"  : "WM('K+','pi+','K-')",
        "WM_piKK"  : "WM('pi+','K+','K-')"} 

Dzvars={"LV01"     : "LV01",
        "LV02"     : "LV02",
        "WM_piK"   : "WM('pi+','K-')"}

Bcvars.update(comvars)
Bcvars.update(DTFvars)
Dpvars.update(comvars)
Dzvars.update(comvars)


hvars={"ID"   : "ID",
       "PX"   : "PX",
       "PY"   : "PY",
       "PZ"   : "PZ",
       "PT"   : "PT",
       "P"    : "P",
       "ETA"  : "ETA",
       "PIDK" : "PIDK",
       "PIDp" : "PIDp",
       "TRCHI2DOF" : "TRCHI2DOF",
       "MIPCHI2DV" : "MIPCHI2DV(PRIMARY)",
       "MIPDV" : "MIPDV(PRIMARY)"}







for dtt in (dtt_DpDzKpi,dtt_DpDzKpipipi):
    dtt.ToolList=[]
    dtt.addTupleTool("TupleToolPropertime")
    if MC: 
        dtt.addTupleTool("TupleToolMCTruth")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolAngles")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolDecayType")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolEventType")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolHierarchy")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolInteractions")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolKinematic")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolP2VV")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolPID")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolPrimaries")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolPrompt")
        #dtt.TupleToolMCTruth.addTupleTool("MCTupleToolReconstructed")
        dtt.addTupleTool("TupleToolMCBackgroundInfo")
        dtt.addTupleTool("TupleToolRecoStats")
        #dtt.addTupleTool("TupleToolGeneration")
    dtt.addTupleTool("TupleToolEventInfo")
    dtt.addTupleTool("TupleToolPrimaries")
    tttt = dtt.Bc.addTupleTool("TupleToolTISTOS")
    tttt.VerboseL0 = True
    tttt.VerboseHlt1 = True
    tttt.VerboseHlt2 = True
    tttt.TriggerList = triggerListAll
    dtt.Bc.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.Bc.LoKiTool.Variables = Bcvars
    dtt.Dp.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.Dp.LoKiTool.Variables = Dpvars
    dtt.p1.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.p1.LoKiTool.Variables = hvars
    dtt.p2.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.p2.LoKiTool.Variables = hvars
    dtt.p3.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.p3.LoKiTool.Variables = hvars
    dtt.Dz.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.Dz.LoKiTool.Variables = Dzvars
    dtt.z1.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.z1.LoKiTool.Variables = hvars
    dtt.z2.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
    dtt.z2.LoKiTool.Variables = hvars
dtt_DpDzKpipipi.z3.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
dtt_DpDzKpipipi.z3.LoKiTool.Variables = hvars
dtt_DpDzKpipipi.z4.addTupleTool('LoKi::Hybrid::TupleTool/LoKiTool')
dtt_DpDzKpipipi.z4.LoKiTool.Variables = hvars


if MC:
    from Configurables import EventNodeKiller
    eventNodeKiller = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
    DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first


# Configure DaVinci
if MC: DaVinci().EventPreFilters.insert(0, workaroundAlg)
#if not MC: DaVinci().RootInTES = '/Event/Bhadron'
if MC: DaVinci().InputType = 'DST'
#if not MC: DaVinci().InputType = 'MDST'
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = str(year)
if MC: 
    DaVinci().Simulation = True
    DaVinci().Lumi = not DaVinci().Simulation
if not MC: DaVinci().Simulation = False
#DaVinci().EvtMax = 1000
DaVinci().EvtMax = -1

if MC: DaVinci().appendToMainSequence([sc.sequence()]) 

DpDzKpiSeq=GaudiSequencer('DpDzKpiSeq')
DpDzKpiSeq.Members +=[Dp2DsKpiSeq.sequence()]
DpDzKpiSeq.Members +=[dtt_DpDzKpi]
DpDzKpipipiSeq=GaudiSequencer('DpDzKpipipiSeq')
DpDzKpipipiSeq.Members +=[Dp2DsKpipipiSeq.sequence()]
DpDzKpipipiSeq.Members +=[dtt_DpDzKpipipi]
DaVinci().UserAlgorithms += [DpDzKpiSeq,DpDzKpipipiSeq]
if MC: DaVinci().UserAlgorithms += [mctupleDpDz,mctupleDPDz,mctupleDpDZ,mctupleDPDZ]

if not MC and year==2017:
    DaVinci().DDDBtag = 'dddb-20150724'
    DaVinci().CondDBtag = 'cond-20170510'
if not MC and year==2015:
#    DaVinci().DDDBtag = 'dddb-20150724'
#    DaVinci().CondDBtag = 'cond-20150828'
    DaVinci().DDDBtag = 'dddb-20150724'
    DaVinci().CondDBtag = 'cond-20170323'
if not MC and year==2012:
#    DaVinci().DDDBtag = 'dddb-20130929'
#    DaVinci().CondDBtag = 'cond-20141107'
    DaVinci().DDDBtag = 'dddb-20150928'
    DaVinci().CondDBtag = 'cond-20150409-1'
if not MC and year==2011:
#    DaVinci().DDDBtag = 'dddb-20130929'
#    DaVinci().CondDBtag = 'cond-20141107'
    DaVinci().DDDBtag = 'dddb-20160318-1'
    DaVinci().CondDBtag = 'cond-20150409'
if MC and sim=="08":
    DaVinci().CondDBtag = 'sim-20130522-1-vc-md100' 
    DaVinci().DDDBtag = 'dddb-20130929-1'
if MC and sim=="09":
    DaVinci().CondDBtag = 'sim-20160321-2-vc-md100' 
    DaVinci().DDDBtag = 'dddb-20150928'

