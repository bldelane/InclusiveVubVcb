"""Appraise the feasibility of deriving a combinatorial template from the aggregated MC"""

from sys import breakpointhook
import uproot, pandas

if __name__ == "__main__":

    # ingredients for the paths & container
    __storage__ = []
    magpols = ("MU", "MD")
    years = ("2011", "2012", "2015", "2016", "2017", "2018")
    modes = (
        "MC_Bc2D0MuNu",
        "MC_Bc2D0gMuNu",
        "MC_Bc2D0pi0MuNu",
        "MC_Bc2D0MuNu_K3Pi",
        "MC_Bc2D0gMuNu_K3Pi",
        "MC_Bc2D0pi0MuNu_K3Pi",
    )

    for y in years:
        for m in modes:
            for p in magpols:
                __storage__.append(
                    f"root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC/D0MuNu/{y}/{p}/{m}_{y}_{p}_pidgen.root:DecayTree"
                )

    comb_truth_sel = "abs(B_plus_TRUEID) != 541 and \
        abs(B_plus_TRUEID) != 511 and \
        abs(B_plus_TRUEID) != 521 and \
        abs(B_plus_TRUEID) != 531 and \
        abs(B_plus_TRUEID) != 5122 and \
        abs(D0_TRUEID)==421 and \
        abs(Mu_plus_TRUEID)==13"
    # Load the data
    mc = uproot.concatenate(
        __storage__,
        filter_name=["B_plus_MCORR", "*TRUEID"],
        library="pd",
        num_entries=100,
    )

    breakpoint()
