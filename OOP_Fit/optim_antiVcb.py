from builtins import breakpoint
from sigchannel import main as run_fit
from bmlfit import (
    sig_hi_base_sel,
)
import numpy as np
from pandas import read_pickle, DataFrame
import boost_histogram as bh


def to_punzi(
    sig_d: float,
    sig_n: float,
    b_y: float,
    a: int = 5,
) -> float:
    """Calculate Punzi at a-sigma level"""

    sig_eff = sig_n / sig_d
    return sig_eff / (a / 2 + np.sqrt(b_y))


def max_fom(
    punzis: dict[str, float],
) -> float:
    """Find the maximum FoM"""
    idx_max = np.argmax(np.array(list(punzis.values())))
    antiVcb_wp = np.array(list(punzis.keys()))[idx_max]

    return antiVcb_wp


def to_eff(
    sig_n: float,
    sig_d: float,
) -> float:
    """Calculate eff FoM"""
    return sig_n / sig_d


def calc_int(
    df: DataFrame,
    sel: str,
    wts_branch: str | list[str] | None = None,
    bins: int = 30,
    range: list[float] = [4_200, 7_200],
    antiVcb_extracut: float | None = None,
    antiComb_extracut: float | None = None,
) -> float:
    """Compute how many candidates in the template, post-sel"""

    # apply selection, ameded
    if antiVcb_extracut is not None:
        sel = f"{sel} & (XGB_antiVcb > {antiVcb_extracut})"
    if antiComb_extracut is not None:
        sel = f"{sel} & (XGB_antiComb > {antiComb_extracut})"
    df = df.query(sel)

    # we must account for the fact that the templates may be weighted
    if wts_branch is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        if not isinstance(wts_branch, list):
            wts_branch = [wts_branch]
        if len(wts_branch) > 1:
            wts = df[wts_branch[0]] * df[wts_branch[1]]
        else:
            wts = df[wts_branch[0]]
        whist.fill(df.B_plus_MCORR, weight=wts)
        return whist.view().sum().value
    else:
        whist = bh.Histogram(bh.axis.Regular(bins, *range))
        whist.fill(df.B_plus_MCORR)
        return whist.sum()


if __name__ == "__main__":

    # global
    ANTICOMB_WORKING_POINT = 0.0
    ANTIVCB_WORKING_POINT = 0.0
    STORAGE = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal"

    # book hi-lev sel
    hilev_sel = sig_hi_base_sel(
        _k_antiComb=ANTICOMB_WORKING_POINT,
        _k_antiVcb=ANTIVCB_WORKING_POINT,
    ).build()

    # establish scale point from the fit
    baseline_fitres = run_fit(
        config="nominal",  # templates
        return_standalone=True,  # return fit results obj
        custom_t_antiComb=ANTICOMB_WORKING_POINT,  # global
        custom_t_antiVcb=0.0,  # global
        ws_statboost_antiComb=0.0,  # WS-specific
    )

    # load dfs before high-level selection
    combined_cf = read_pickle(
        f"{STORAGE}/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_cf_sw_xgb.pkl",
    )
    combined_misid = read_pickle(
        f"{STORAGE}/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_misid_hadron_enriched_misid_sw_xgb.pkl"
    )
    combined_mc = read_pickle(
        f"{STORAGE}/MC/combined/D0MuNu/5.x_invfb/Bc2D0MuNuX_xgb.pkl"
    )

    # book the integrals, post-soft high-level selection
    cf_dnm = calc_int(
        df=combined_cf,
        sel=hilev_sel,
        wts_branch=["sw"],
    )
    misid_dnm = calc_int(
        df=combined_misid,
        sel=hilev_sel,
        wts_branch=["misid_w", "sw"],
    )
    mc_dnm = calc_int(
        df=combined_mc,
        sel=hilev_sel,
        wts_branch=None,
    )

    punzis_CF = {}
    punzis_misID = {}

    for _t in np.linspace(0, 1.0, 100, endpoint=True):

        punzis_CF[str(_t)] = to_punzi(
            sig_d=mc_dnm,
            sig_n=calc_int(
                df=combined_mc, sel=hilev_sel, wts_branch=None, 
                antiVcb_extracut=_t
            ),
            b_y=cf_dnm
            * (
                calc_int(
                    df=combined_cf, sel=hilev_sel, 
                    wts_branch="sw", 
                    antiVcb_extracut=_t
                )
                / cf_dnm
            ),
        )

        punzis_misID[str(_t)] = to_punzi(
            sig_d=mc_dnm,
            sig_n=calc_int(
                df=combined_mc, sel=hilev_sel, wts_branch=None, 
                antiVcb_extracut=_t
            ),
            b_y=misid_dnm
            * (
                calc_int(
                    df=combined_misid,
                    sel=hilev_sel,
                    wts_branch=["sw", "misid_w"],
                    antiVcb_extracut=_t,
                )
                / misid_dnm
            ),
        )


print("\nLoop complete")
conts = (punzis_CF, punzis_misID)
cont_names = ("anti-CF", "anti-misID")
for zip_cont in zip(conts, cont_names):
    print(f"Max FoM attained for {zip_cont[1]}: {max_fom(zip_cont[0])}")
