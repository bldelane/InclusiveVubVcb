"""Perform fit to the signal channel"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
from collections import namedtuple
from operator import inv
from struct import unpack
from unittest import result
from bmlfit import sigchannel_spec_samples
from bmlfit import (
    tarray,
    Template,
    BZObs,
    BZTemplate,
    calc_BZ_scalefactors,
    sig_hi_base_sel,
)
from bmlfit import source_sel as _source_sel
from bmlfit.sigchannel_spec_samples import Sig, MisID, Comb, CF
import pandas as pd
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod
from uncertainties import ufloat
from hist import Hist
import numpy as np
from pathlib import Path
import json
from abc import ABC, abstractmethod
import json, requests, jsonschema
import pyhf
import os
from tabulate import tabulate
from typing import Callable, Optional
import cabinetry
import numpy.typing as npt
from dataclasses import dataclass, field
from functools import wraps, partial
import warnings
import matplotlib as mpl
import mplhep as hep
from copy import copy

pyhf.set_backend(
    # https://scikit-hep.org/pyhf/_generated/pyhf.optimize.opt_minuit.minuit_optimizer.html
    # 0.5 errordef for NLL
    "numpy",
    pyhf.optimize.minuit_optimizer(verbose=1, errordef=0.5),
)  # get uncertainties and covariance


plt.style.use("science")


# XXX: docs needed
def build_validate_workspace(path: str) -> "pyhf workspace":
    """Build and validate the model and observation"""
    # validate workspace
    workspace = json.load(open(path))
    schema = requests.get(
        "https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json"
    ).json()
    # If no exception is raised by validate(), the instance is valid.
    jsonschema.validate(instance=workspace, schema=schema)
    workspace = pyhf.Workspace(workspace)

    return workspace


def plot_fitted_model(workspace, pdf, bestfit_pars, ax) -> None:
    """very raw plot comparing fit results"""
    proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)
    ax.step(
        x=np.arange(len(proj_fit)),
        y=proj_fit,
        where="post",
        lw=1.5,
        ls="-",
        alpha=0.7,
    )
    ax.errorbar(
        x=np.arange(len(proj_fit)) + 0.5,
        y=workspace.data(pdf)[: len(proj_fit)],
        fmt=".",
        color="black",
    )

    ax.set_title(r"LHCb Unofficial $5.x$ fb$^{-1}$")
    ax.legend()
    ax.set_ylabel(rf"Bohm\&Zech-scaled events / 100 MeV")
    ax.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]")


def to_table(results: dict, tex: bool = False) -> None:
    """Print yield results

    Parameters
    ----------
    results : str
       output of the fit

    tex: bool
        if True, print in latex format. Default: False
    """
    table = []
    for k in results.keys():
        if "beeston" not in k:  # don't want to print BB
            if "cf" not in k:  # round to int
                table.append([k, round(results[k].n), round(results[k].s)])
            else:
                table.append([k, results[k].n, results[k].s])

    print(tabulate(table, headers=["Parameter", "cval", "err"], tablefmt="fancy_grid"))
    if tex:
        print(tabulate(table, headers=["Parameter", "cval", "err"], tablefmt="latex"))


# decorator to verify the correctness of the likelihood minimisation
def NLLmin_check(func: Callable, verbose: bool = False) -> Callable:
    """Exploit the fact that we are using iminuit in the backend to verify the correcteness
    of the NLL minimisation

    if verbose: print all the parameters and the relevant limit
    """

    @wraps(func)
    def wrapping(*args, **kwargs):
        fit_result, result_obj = func(*args, **kwargs)

        # firstly, sanity check: the iminuit errordef if 0.5
        # ref: https://scikit-hep.org/pyhf/_generated/pyhf.optimize.opt_minuit.minuit_optimizer.html
        assert (
            pyhf.optimizer.errordef == 0.5
        ), "MinimiserError: iminuit errordef is not 0.5"

        # validity checks
        assert result_obj.minuit.valid == True, "MinuitError: NLL minimum not valid"
        assert (
            result_obj.minuit.accurate == True
        ), "MinuitError: NLL minimum not accurate"
        assert (
            result_obj.minuit.fmin.has_made_posdef_covar == False
        ), "MinuitError: Hessian matrix forced pos def"
        if result_obj.minuit.fmin.has_parameters_at_limit == True:
            print("Fitted parameters report:")
            if verbose:
                for p in result_obj.minuit.params:
                    print(
                        f"{p.name}, value = {p.value} +/- {p.error}; allowed range = [{p.lower_limit}, {p.upper_limit}]",
                    )
        return fit_result, result_obj

    return wrapping


@NLLmin_check
def run_minimisation(
    data: object, pdf: object, verbose: bool = True
) -> tuple[npt.ArrayLike, ...] | tuple[namedtuple, ...]:
    """wrapper of the pyhf minisation to make `return_uncertainties` attribute private
    and True by default. Additionally, this enables the converge-checks decorator

    if verbose is True, print the iminuit fmin object
    """
    fit_result, result_obj = pyhf.infer.mle.fit(
        data, pdf, return_result_obj=True, return_uncertainties=True
    )
    if verbose:
        print(result_obj.minuit.fmin)
    return fit_result, result_obj


def write_schema(spec: dict, path: str) -> None:
    """Persist to file the model spec

    Parameters
    ----------
    spec: str
        model dict spec
    path: str
        path where to save the json file

    Returns
    -------
        json file
    """
    Path(f"model").mkdir(parents=True, exist_ok=True)
    spec = (
        str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
    )
    SCHEMA = path
    with open(SCHEMA, "w") as outfile:
        outfile.write(spec)
    print(f"SUCCESS: schema {SCHEMA!r} written to file")


class CompleteSpec(ABC):
    """Abstract class to generate per-component specs"""

    @abstractmethod
    def write(self):
        pass


class SignalSpec(CompleteSpec):
    def __init__(self, channel_dict):
        self._channel_dict = channel_dict
        self.outpath = "model/sigchannel_xgb.json"

    @property
    def channel_dict(self):
        return self._channel_dict

    def write(self):
        write_schema(self.channel_dict, path=self.outpath)


def __init_comps_dict__() -> dict:
    """initialise the fit components dict"""
    return {
        "signal_sample": None,
        "comb_sample": None,
        "cf_sample": None,
        "misid_sample": None,
    }


def __init_th_dict__() -> dict:
    """initialise the histogram templates dict"""
    return {
        "signal_sample": None,
        "comb_sample": None,
        "cf_sample": None,
        "misid_sample": None,
    }


def derive_CF_constr(unsc_cf_hist: Hist) -> ufloat:
    """Extract the CF yield constraint

    Parameters
    ----------
    unsc_cf_hist : Template
        Non BZ-scaled CF template histogram

    Returns
    -------
    ufloat
        ufloat holding the sum() view value and variance of the Hist template
    """
    # from PDG, get the BFs D0->Kpi
    CF = ufloat(3.950e-2, 0.031e-2)
    DCS = ufloat(1.50e-4, 0.07e-4)
    # scale factor for CF_y -> B+_y constraint in the fit
    R = DCS / CF
    CF_norm = ufloat(unsc_cf_hist.sum().value, unsc_cf_hist.sum().variance ** 0.5)

    return CF_norm * R


def build_schema(
    signal_sample: dict,
    comb_sample: dict,
    cf_sample: dict,
    misid_sample: dict,
    obs: dict,
    year: str,  # allowe
) -> dict:
    """assemble the json schema

    Parameters
    ---------
    dict
        per-sample specs

    Returns
    -------
    dict
        dict for model spec
    """

    spec = {
        "channels": [
            {
                "name": f"singlechannel_xgb_{year}",
                "samples": [signal_sample, comb_sample, cf_sample, misid_sample],
            },
        ],
        "observations": [
            {
                "name": f"singlechannel_xgb_{year}",
                "data": obs.tolist(),
            },
        ],
        "measurements": [
            {
                "name": f"sig_y_extraction",
                "config": {
                    "poi": f"{signal_sample['name']}_y",
                    "parameters": [
                        # bounds on floatig normalisations
                        # NOTE: the CF yield is constrained, hence no entry below
                        {
                            "name": f"{signal_sample['name']}_y",
                            "bounds": [[0.0, obs.sum()]],
                            "inits": [1e2],
                        },
                        {
                            "name": f"{comb_sample['name']}_y",
                            "bounds": [[0.0, obs.sum()]],
                            "inits": [1e3],
                        },
                        {
                            "name": f"{misid_sample['name']}_y",
                            "bounds": [[0.0, obs.sum()]],
                            "inits": [1e3],
                        },
                    ],
                },
            }
        ],
        "version": "1.0.0",
    }
    return spec


def gen_fit_comps(
    return_standalone: bool,
    custom_t_antiComb: Optional[float] = None,
    custom_t_antiVcb: Optional[float] = None,
    ws_statboost_antiComb: Optional[float] = None,
) -> Callable:
    """Closure to enable, where required, the propagation of the standalone
    histogram schemas for plotting purposes and the s_i BZ scale factors

    Parameters
    ----------
    return_standalone: bool
        If True, return model schema and FitResContainer dataclass from which to construct the fit result

    custom_t_antiComb: Optional[float], default is None
        If not None, override the antiComb XGB response cut. Else, keep the default.

    custom_t_antiVcb: Optional[float], default is None
        If not None, override the antiVcb XGB response cut. Else, keep the default.

    ws_statboost_antiComb: Optional[float], default is None
        If not None, override the WS antiComb response cut. NOTE: this assumes no sculpting.

    Returns
    -------
    model: Callable
        Function that assembles the fit model and, when `return_standalone` is True, a dict of template schemas
    """

    def build(year: str) -> dict | tuple[dict, FitCompsContainer]:
        """Build the model spec

        Parameters
        ----------
        year: str
            Allowed options ['2016', '2017','2018', '5.x_invfb']

        Returns
        -------
        dict | tuple[dict, dict, npt.ArrayLike]
            Fit model spec or tuple of spec, the standalone templates and obs, and the s_i BZ scale factors
        """
        # check validity of year keyword
        assert year in ["2016", "2017", "2018", "5.x_invfb"], "KeyError: invalid year"

        # intialise the dict holding fit componets
        comps_dict = __init_comps_dict__()

        # intialise the dict holding hist componets
        th_dict = __init_comps_dict__()

        # load the samples
        print("Loading samples...")
        __storage_path = (
            "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
        )

        container = (
            {}
        )  # slow, would be nice to run into something like a tuple or dataclass
        paths = {
            "dcs": f"{__storage_path}/D0MuNu/{year}/Bc2D0MuNu_dcs_sw_xgb.pkl",
            "cf": f"{__storage_path}/D0MuNu/{year}/Bc2D0MuNu_cf_sw_xgb.pkl",
            # XXX: misid needs full dataset; ok for now, as floating norm
            "misid": f"{__storage_path}/D0MuNu/2018/Bc2D0MuNuX_misid_hadron_enriched_misid_sw_xgb.pkl",
            "comb": f"{__storage_path}/D0MuNu/{year}/Bc2D0MuNu_ws_sw_xgb.pkl",
            "sig": f"{__storage_path.replace('DATA', 'MC')}/D0MuNu/{year}/Bc2D0MuNuX_xgb.pkl",
        }

        # load D0mu data samples
        for id in ("dcs", "cf", "misid", "comb", "sig"):
            container[id] = pd.read_pickle(paths[id])
        print("SUCCESS: loading complete")

        # init nominal selection dataclass
        hi_lev_sel_obj = sig_hi_base_sel()

        # override the default antiComb response cut
        if custom_t_antiComb is not None:
            hi_lev_sel_obj._k_antiComb = custom_t_antiComb
        # override the default antiComb response cut
        if custom_t_antiVcb is not None:
            hi_lev_sel_obj._k_antiVcb = custom_t_antiVcb

        # partial to hide dependency in building the per-component selection, post potential override
        print(f"Init hi-level sel object: {hi_lev_sel_obj}")
        source_sel = partial(_source_sel, base_sel=hi_lev_sel_obj.build(), silent=True)

        # generate the BZ s_i weights
        # ---------------------------
        obs_df = container["dcs"].query(f"{source_sel('obs')}")
        dcs_tarray = tarray(
            name="obs",
            year=year,
            data=obs_df.XGB_antiComb,
            weights=obs_df.sw,
        )
        unscaled_obs = Template(dcs_tarray)
        s_i = calc_BZ_scalefactors(unscaled_obs)

        # NOTE: proceed with s_i-scaled templates
        # =======================================
        # MisID
        # -----
        misid_df = container["misid"].query(f"{source_sel('misid')}")
        misid_tarray = tarray(
            name="misid",
            year=year,
            data=misid_df.XGB_antiComb,
            weights=misid_df.sw * misid_df.misid_w,
        )
        # build scaled template and geenrate the spec in the model and the standalone fit component
        misid_si_templ = MisID(BZTemplate(tinput=misid_tarray, bz=s_i))
        comps_dict["misid_sample"] = misid_si_templ.to_spec()

        # CF
        # --
        cf_df = container["cf"].query(f"{source_sel('cf')}")
        cf_tarray = tarray(
            name="cf",
            year=year,
            data=cf_df.XGB_antiComb,
            weights=cf_df.sw,
        )
        cf_si_templ = CF(
            templ=BZTemplate(tinput=cf_tarray, bz=s_i),
            yconstr=derive_CF_constr(
                Template(cf_tarray).to_hist(silent=True)
            ),  # unscaled histogram -> yield constr from DCS / CF D0 BF ratio
        )
        comps_dict["cf_sample"] = cf_si_templ.to_spec()
        # Comb
        # ----
        # NOTE: we load in the kde npy array, which should have all revent weights applied
        # with open("tmp/kde/nominal_ws_kde.npy", "rb") as f:
        #     kde_template = np.load(f)

        # comb_tarray = tarray(
        #     name="comb",
        #     year=year,
        #     data=kde_template,
        # )

        # try raw template, no KDE

        if ws_statboost_antiComb is not None:
            # shallow copy
            # NOTE: this overriding must only exist in the WS scope
            ws_specific_HLS_obj = copy(hi_lev_sel_obj)
            ws_specific_HLS_obj._k_antiComb = ws_statboost_antiComb  # override
            shape_unbiased_sel = _source_sel(
                tname="ws", base_sel=ws_specific_HLS_obj.build()
            )
        else:
            # compatible with the other templates
            shape_unbiased_sel = source_sel("ws")

        comb_df = container["comb"].query(shape_unbiased_sel)
        comb_tarray = tarray(
            name="comb",
            year=year,
            data=comb_df.XGB_antiComb,
            weights=comb_df.sw,
        )

        comb_si_templ = Comb(BZTemplate(tinput=comb_tarray, bz=s_i))
        comps_dict["comb_sample"] = comb_si_templ.to_spec()

        # Signal MC template
        # ------------------
        # FIXME: BF-weighted signal models, should use eff-weighted RBF
        inclSig_df = container["sig"].query(f"{source_sel('SemiExclSig')}")
        inclSig_tarray = tarray(
            name="SemiExclSig", year=year, data=inclSig_df.XGB_antiComb
        )  # no weights in MC
        sig_si_templ = Sig(BZTemplate(tinput=inclSig_tarray, bz=s_i))
        comps_dict["signal_sample"] = sig_si_templ.to_spec()

        # DCS obs
        # -------
        dcs_df = container["dcs"].query(f"{source_sel('obs')}")
        obs_tarray = tarray(
            name="obs",
            year=year,
            data=dcs_df.XGB_antiComb,
            weights=dcs_df.sw,
        )
        si_obs = BZObs(
            tinput=obs_tarray, bz=s_i
        ).make_hist()  # cvals and stds of si-weighted, sweighted DCS data
        comps_dict["obs"] = si_obs.n

        if return_standalone:
            # return standalone schemas and observation, and s_i
            th_dict["misid_sample"] = misid_si_templ.to_standalone_h()
            th_dict["cf_sample"] = cf_si_templ.to_standalone_h()
            th_dict["comb_sample"] = comb_si_templ.to_standalone_h()
            th_dict["signal_sample"] = sig_si_templ.to_standalone_h()
            return build_schema(**comps_dict, year=year), FitCompsContainer(
                _obs=si_obs, _ths=th_dict, _s_i=s_i
            )
        else:
            # just return the total fit model schema
            return build_schema(**comps_dict, year=year)

    return build


def execute_fit(run_cab: bool = False) -> Callable:
    """Closure to enable the cabinetry machinery to be execute ahead of the BML fit in pyhf"""

    def fit(
        workspace: "pyhf workspace",
        resultspath: str = "results/sigchannel.log",
    ) -> list[dict, any, ...]:
        """run the fit"""
        pdf = workspace.model(
            measurement_name="sig_y_extraction",
            modifier_settings={
                "normsys": {
                    "interpcode": "code1"
                },  # checked: much better (consistency-wise) than code4
            },
        )
        data = workspace.data(pdf)

        fit_result, result_obj = run_minimisation(data, pdf)
        bestfit_pars, par_uncerts = fit_result.T

        # prelminary fit in cabinetry
        if run_cab:
            # ==========================================================
            # NOTE: cabinetry uses errordef=1.0 for the minuit optimizer
            # Therefore check for consistency with the fit result before
            # using the cabinetry infrastructure
            # ==========================================================
            fit_results = cabinetry.fit.fit(pdf, data, custom_fit=False)

            model_pred = cabinetry.model_utils.prediction(pdf)
            figures = cabinetry.visualize.data_mc(model_pred, data)

            model_pred_postfit = cabinetry.model_utils.prediction(
                pdf, fit_results=fit_results
            )
            _ = cabinetry.visualize.data_mc(model_pred_postfit, data)

        # write to dedicated file; repristinate outoput after
        fit_results = {}
        os.makedirs(Path(resultspath).parent.absolute(), exist_ok=True)
        fres = open(resultspath, "w")
        for k, v in pdf.config.par_map.items():
            lo_idx = v["slice"].start
            hi_idx = v["slice"].stop
            if k == "beeston_barlow":
                fit_results[f"{k}"] = bestfit_pars[lo_idx:hi_idx]
            if k != "beeston_barlow":
                fit_results[k] = ufloat(bestfit_pars[lo_idx], par_uncerts[lo_idx])
                print(
                    f"Extracted value of {k} = {bestfit_pars[lo_idx]} +/- {par_uncerts[lo_idx]}",
                    file=fres,
                )
        fres.close()
        # print results to screeen
        to_table(fit_results)

        # plotting segment
        fig, ax = plt.subplots()
        plot_fitted_model(workspace, pdf, bestfit_pars, ax=ax)
        [plt.savefig(f"tmp/fitdev/postfit_si.{ext}") for ext in ("pdf", "png", "eps")]

        return pdf, fit_results, par_uncerts

    return fit


# init the fit configs, based on whether we want to include cabinetry
run_bml_fit = execute_fit(run_cab=False)
cab2bml_fit = execute_fit(run_cab=True)


@dataclass(frozen=False, kw_only=True, slots=True)
class FitCompsContainer:
    """Dataclass serving as container of all the info necessary to plot results"""

    # init attributes should be private, as much as dataclasses allow
    _obs: npt.ArrayLike = field(repr=False)
    _ths: dict = field(repr=False)
    _s_i: npt.ArrayLike = field(repr=False)

    # set post-init, post-fit; thus, public
    postfit_pars: dict = field(init=False, repr=True)
    # postfit model
    pdf: object = field(init=False, repr=True)

    # post-plotting we can set a container of yields, converting normsys to normalisation
    fl_y: dict = field(default_factory=dict, init=False, repr=True)

    def unpack_postfit_pars(self) -> list:
        """Unpack the fit results dict, to extract where appropriate only the
        central values
        """
        _bestfit_pars = []
        for k in self.pdf.config.par_order:
            if k == "beeston_barlow":
                _bestfit_pars.extend(self.postfit_pars[k].tolist())
            if k != "beeston_barlow":
                _bestfit_pars.append(self.postfit_pars[k].n)
        return _bestfit_pars

    def postfit_plot(
        self,
        phys: bool = False,
        outpath: str = "plots",
        range: list = [0, 1],
        bins: int = 20,
        killBB: bool = False,
    ) -> None:
        """Plot the fit result, with the BZ-scaled comps or the up-scaling back to the physical candidates"""
        with plt.style.context("science"):
            mpl.rcParams["axes.prop_cycle"] = mpl.cycler(
                color=["#d6604d", "#67001f", "#878787", "#bababa"]
            )
            fig, (ax, ax_p) = plt.subplots(
                2,
                1,
                gridspec_kw={"height_ratios": [5, 1]},
                sharex=True,
            )

            inverse_scale_factors = np.ones(
                len(self._s_i)
            )  # plot the BZ-scaled fit, ie what seen by the minimiser
            if phys:
                # revent the BZ scaling
                inverse_scale_factors = self._s_i

            edges = np.array(
                np.linspace(*range, bins, endpoint=False).tolist() + [range[-1]]
            )

            proj_fit = self.pdf.expected_data(
                self.unpack_postfit_pars(), include_auxdata=False
            )
            ax.errorbar(
                x=(edges[1:] + edges[:-1]) / 2,
                xerr=(edges[1] - edges[0]) / 2,
                y=self._obs.n * inverse_scale_factors,
                yerr=self._obs.s * inverse_scale_factors,
                fmt=".",
                color="black",
                markersize=3,
                label="DCS data",
            )
            ax.step(
                x=edges[:-1],
                y=proj_fit * inverse_scale_factors,
                where="post",
                lw=1,
                ls="-",
                label="Total fit model",
                color="tab:blue",
            )

            plt_config = {
                "signal_sample": {
                    "label": r"$B_c^+ \to D^{(*)0} \mu^+ \nu_{\mu}$",
                    "hist": None,
                },
                "comb_sample": {
                    "label": r"Combinatorial $D^{0} \mu^+$",
                    "hist": None,
                },
                "cf_sample": {
                    "label": r"$b \to c \, \mu^+ \nu_{\mu}$",
                    "hist": None,
                },
                "misid_sample": {
                    "label": r"Muonic misID",
                    "hist": None,
                },
            }

            # init namedtuples to save the yields of the histograms
            Yield = namedtuple("Yield", ["id", "cval", "std"])

            # build the template histograms using the appropriate POI and nuisances
            for id, fitcomp in self._ths.items():
                _template_model = pyhf.Model(
                    fitcomp,
                    poi_name=None,
                    modifier_settings={"normsys": {"interpcode": "code1"}},
                )
                _template_model_pars = []
                for _par in _template_model.config.par_order:
                    if isinstance(self.postfit_pars[_par], np.ndarray):
                        # handle the fact that BB is saved as a numpy array of cvals
                        if killBB:
                            # neglect the BB scaling in the templates and replace with 1s
                            _template_model_pars.extend(
                                np.ones(len(self.postfit_pars[_par])).tolist()
                            )
                        else:
                            # include BB nuisance parameters, same for all templates
                            _template_model_pars.extend(
                                self.postfit_pars[_par].tolist()
                            )
                    else:
                        # add the ufloat cvals
                        _template_model_pars.append(self.postfit_pars[_par].n)

                plt_config[id]["hist"] = _template_model.expected_data(
                    _template_model_pars, include_auxdata=False
                )

                # assign a proxy 'effective' yields by summing over the histograms bins
                # this accounts for BB
                self.fl_y[id] = Yield(
                    id,
                    np.sum(plt_config[id]["hist"] * inverse_scale_factors),
                    np.NaN,  # FIXME: implement a routine to get the error from hist
                )

            # stack the hists
            stacking_order = [
                plt_config[_c]["hist"] * inverse_scale_factors
                for _c in (
                    "signal_sample",
                    "cf_sample",
                    "comb_sample",
                    "misid_sample",
                )
            ]
            stacked_labels = [
                plt_config[_c]["label"]
                for _c in (
                    "signal_sample",
                    "cf_sample",
                    "comb_sample",
                    "misid_sample",
                )
            ]

            hep.histplot(
                stacking_order,
                edges,
                ax=ax,
                stack=True,
                histtype="fill",
                label=stacked_labels,
            )
            ax.legend(loc="center", bbox_to_anchor=(0.5, -0.9), ncol=2)
            ax.set_ylabel("Candidates")
            ax.set_title("LHCb Unofficial $5.4$ fb$^{-1}$ (13 TeV)")

            # pulls
            centres = (edges[:-1] + edges[1:]) / 2
            ypred = proj_fit * inverse_scale_factors
            pulls = (self._obs.n * inverse_scale_factors - ypred) / (
                self._obs.s * inverse_scale_factors
            )

            ax_p.set_ylim(-5.5, 5.5)
            ax_p.axhline(0, color="tab:blue", lw=0.5, ls="--")
            ax_p.axhspan(-3, 3, facecolor="tab:blue", alpha=0.25)
            ax_p.bar(
                x=centres,
                height=pulls,
                color="black",
                width=(edges[1] - edges[0]),
            )
            ax.set_ylim(bottom=0)
            ax_p.set_xlabel(r"$m_{corr}(D^0\mu^+)$ [MeV/$c^2$]")
            ax_p.set_ylabel(r"Pulls $[\sigma]$")

            p = Path(outpath).mkdir(exist_ok=True, parents=True)
            [plt.savefig(f"{outpath}/postfit.{ext}") for ext in ("pdf", "png", "eps")]


def main(config: str = "nominal", **kwargs) -> FitCompsContainer:
    """Run the entire NLL minimisation and return the fit results.
    This is decoupled from the plotter frontend to enable closures devoted to monitoring performance.

    Parameters
    ------–---
    config: str, default is 'nominal'
        ID to map the configuration considered in the evaluation of the systematics
    kwargs:
        Overrise selection on the XGB responses, passed to `gen_fit_comps` closure

    Returns
    -------
    res_cont: FitCompsContainer
        Dataclass containing obs, template histograms and results
    """

    schema, res_cont = gen_fit_comps(**kwargs)(year="5.x_invfb")
    model_spec = SignalSpec(schema)
    model_spec.write()
    workspace = build_validate_workspace(model_spec.outpath)

    # assign to the fit results container, post minimisation
    (res_cont.pdf, res_cont.postfit_pars, _,) = run_bml_fit(
        workspace=workspace
    )  # enable the cabinetry machinery ahead of the pyhf bml fit

    # plot fit results after NLL minimisation
    res_cont.postfit_plot(phys=False, killBB=False)
    return res_cont


if __name__ == "__main__":
    # postfit_obj = main(
    #     config="nominal",  # templates
    #     return_standalone=True,  # return fit results obj
    #     custom_t_antiComb=None,  # global
    #     custom_t_antiVcb=None,  # global
    #     ws_statboost_antiComb=None,  # WS-specific
    # )
    # ==========================================================
    # # loop to understand the impact of the loose antiComb cuts
    # ----------------------------------------------------------
    sig_ys = {}
    comb_ys = {}
    _cuts = [0.0, 0.1]
    for _t in _cuts:
        _pfo = main(
            config="nominal",  # templates
            return_standalone=True,  # return fit results obj
            custom_t_antiComb=_t,  # global
            custom_t_antiVcb=None,  # global
            ws_statboost_antiComb=_t,  # WS-specific
        )
        sig_ys[str(_t)] = _pfo.postfit_pars["SemiExclSig_5.x_invfb_y"]
        comb_ys[str(_t)] = _pfo.postfit_pars["comb_5.x_invfb_y"]

    sig_cvals = np.array([x.n for x in list(sig_ys.values())])
    sig_stds = np.array([x.s for x in list(sig_ys.values())])

    comb_cvals = np.array([x.n for x in list(comb_ys.values())])
    comb_stds = np.array([x.s for x in list(comb_ys.values())])

    baseline_sig_y = ufloat(3947, 627)
    baseline_comb_y = ufloat(77554, 1189)

    fig, ax = plt.subplots()
    ax.errorbar(
        x=_cuts,
        y=comb_cvals,
        # yerr=stds,
        fmt=".",
    )
    ax.set_xlabel(r"$t_{comb}^{WS}$")
    ax.set_ylabel(r"$N_{sig}$")
    ax.set_title(r"LHCb Unofficial $5.x$ fb$^{-1}$")
    ax.set_ylim(bottom=0)
    plt.savefig("tmp_sig_y_test.pdf")
    # ==========================================================
