# OOP binned maximum likelihood fit (`bmlfit`)

A OOP framework to fit the signal and normalisation modeds, supplying the appropriate likelihood terms (modifiers) to account for the sources of systematic uncertainties. This module relies on the `pyhf` and `cabinetry` packages to executed the BML fits.

## Spec 

Comment on design pattern used. 

[insert inheritance diagrame here]

## RFE 
- [ ] `Setter` methods for the high-level selection cut values. This is necessary for the XGB optimisation(s).
- [ ] Complete plotting front-end.
- [ ] Refactoring of `sigchannel`: port the channel-agnostic methods to a base file.
- [ ] Likelihood modifiers for signal FF alternative templates.
- [ ] Toys and wrapper for discovery metrics.
- [ ] Efficiency report when performing the high-level selection.

## Unit tests
- [ ] Setter for high-level selection for protected nominal values.
- [ ] FS selection of templates
- [ ] Absence of empty bins
- [ ] No replication of likelihood modifiers
- [ ] CF constraint: physically sensible and contrained normalisation 