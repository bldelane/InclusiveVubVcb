from abc import ABC, abstractmethod
from sre_constants import SUCCESS
from bmlfit import BZObs, BZTemplate
from uncertainties import ufloat


# NOTE: different subsclasses neeeded as spec components have different modifiers
class Spec(ABC):
    """Abstract class to generate per-component specs"""

    @abstractmethod
    def to_spec(self):
        pass


class Sig(Spec):
    """Subsclass to generate dcs observation sample"""

    # NOTE: inherit from BZTemplate class
    def __init__(self, templ: BZTemplate) -> None:
        super().__init__()
        self._templ = templ

    @property
    def templ(self):
        return self._templ

    def to_spec(self) -> dict:
        """Geneate the dict required for the model spec."""
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        # assemble the sample spec dict
        sample = {
            "name": self.templ.id(),
            "data": (BZttuple.n).tolist(),
            "modifiers": [
                {
                    "name": f"beeston_barlow",
                    "type": "staterror",
                    "data": (BZttuple.s).tolist(),
                },  # branch-speficific per-bin variation
                {
                    "name": f"{self.templ.id()}_y",
                    "type": "normfactor",
                    "data": None,
                },  # NOTE: floating normalisation
            ],
        }
        print(f"SUCCESS: constructed {self.templ.id()!r} sample spec dict\n")
        return sample

    def to_standalone_h(self) -> dict:
        """retun dict that accepts the modifiers to generate a histogram component in the fit"""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        standalone_h = {
            "channels": [
                {
                    "name": "singlechannel_xgb",
                    "samples": [
                        {
                            "name": f"{self.templ.id()}",
                            "data": (BZttuple.n).tolist(),
                            "modifiers": [
                                {
                                    "name": f"beeston_barlow",
                                    "type": "staterror",
                                    "data": (BZttuple.s).tolist(),
                                },  # branch-speficific per-bin variation
                                {
                                    "name": f"{self.templ.id()}_y",
                                    "type": "normfactor",
                                    "data": None,
                                },  # NOTE: floating normalisation
                            ],
                        }
                    ],
                }
            ]
        }
        return standalone_h


class MisID(Spec):
    """Subsclass to generate dcs MisID sample."""

    # NOTE: inherit from BZTemplate class
    def __init__(self, templ: BZTemplate) -> None:
        super().__init__()
        self._templ = templ

    @property
    def templ(self):
        return self._templ

    def to_spec(self) -> dict:
        """Geneate the dict required for the model spec."""
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        # assemble the sample spec dict
        sample = {
            "name": self.templ.id(),
            "data": (BZttuple.n).tolist(),
            "modifiers": [
                {
                    "name": f"beeston_barlow",
                    "type": "staterror",
                    "data": (BZttuple.s).tolist(),
                },  # branch-speficific per-bin variation
                {
                    "name": f"{self.templ.id()}_y",
                    "type": "normfactor",
                    "data": None,
                },  # NOTE: floating normalisation
            ],
        }
        print(f"SUCCESS: constructed {self.templ.id()!r} sample spec dict\n")
        return sample

    def to_standalone_h(self) -> dict:
        """retun dict that accepts the modifiers to generate a histogram component in the fit"""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        standalone_h = {
            "channels": [
                {
                    "name": "singlechannel_xgb",
                    "samples": [
                        {
                            "name": f"{self.templ.id()}",
                            "data": (BZttuple.n).tolist(),
                            "modifiers": [
                                {
                                    "name": f"beeston_barlow",
                                    "type": "staterror",
                                    "data": (BZttuple.s).tolist(),
                                },  # branch-speficific per-bin variation
                                {
                                    "name": f"{self.templ.id()}_y",
                                    "type": "normfactor",
                                    "data": None,
                                },  # NOTE: floating normalisation
                            ],
                        }
                    ],
                }
            ]
        }
        return standalone_h


class Comb(Spec):
    """Subsclass to generate dcs MisID sample."""

    # NOTE: inherit from BZTemplate class
    def __init__(self, templ: BZTemplate) -> None:
        super().__init__()
        self._templ = templ

    @property
    def templ(self):
        return self._templ

    def to_spec(self) -> dict:
        """Geneate the dict required for the model spec."""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        # assemble the sample spec dict
        sample = {
            "name": self.templ.id(),
            "data": (BZttuple.n).tolist(),
            "modifiers": [
                {
                    "name": f"beeston_barlow",
                    "type": "staterror",
                    "data": (BZttuple.s).tolist(),
                },  # branch-speficific per-bin variation
                {
                    "name": f"{self.templ.id()}_y",
                    "type": "normfactor",
                    "data": None,
                },  # NOTE: floating normalisation
            ],
        }
        print(f"SUCCESS: constructed {self.templ.id()!r} sample spec dict\n")
        return sample

    def to_standalone_h(self) -> dict:
        """retun dict that accepts the modifiers to generate a histogram component in the fit"""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        standalone_h = {
            "channels": [
                {
                    "name": "singlechannel_xgb",
                    "samples": [
                        {
                            "name": f"{self.templ.id()}",
                            "data": (BZttuple.n).tolist(),
                            "modifiers": [
                                {
                                    "name": f"beeston_barlow",
                                    "type": "staterror",
                                    "data": (BZttuple.s).tolist(),
                                },  # branch-speficific per-bin variation
                                {
                                    "name": f"{self.templ.id()}_y",
                                    "type": "normfactor",
                                    "data": None,
                                },  # NOTE: floating normalisation
                            ],
                        }
                    ],
                }
            ]
        }
        return standalone_h


class CF(Spec):
    """Subsclass to generate dcs CF sample.
    Yield constraint included.
    """

    # NOTE: inherit from BZTemplate class
    def __init__(self, templ: BZTemplate, yconstr: ufloat) -> None:
        super().__init__()
        self._templ = templ
        self._yconstr = yconstr

    @property
    def templ(self):
        return self._templ

    @property
    def yconstr(self):
        return self._yconstr

    def to_spec(self) -> dict:
        """Geneate the dict required for the model spec."""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        # assemble the sample spec dict
        sample = {
            "name": self.templ.id(),
            "data": (BZttuple.n).tolist(),
            "modifiers": [
                {
                    "name": f"beeston_barlow",
                    "type": "staterror",
                    "data": (BZttuple.s).tolist(),
                },  # branch-speficific per-bin variation
                {
                    "name": f"{self.templ.id()}_y",
                    "type": "normsys",
                    "data": {
                        "lo": self.yconstr.n - self.yconstr.s,
                        "hi": self.yconstr.n + self.yconstr.s,
                    },  # NOTE: constrained normalisation
                },
            ],
        }
        print(f"SUCCESS: constructed {self.templ.id()!r} sample spec dict\n")
        return sample

    def to_standalone_h(self) -> dict:
        """retun dict that accepts the modifiers to generate a histogram component in the fit"""

        # FIXME: code duplication
        # build the tuple holding cvals and errs, scaled by BZ
        BZttuple = self.templ.make_hist()

        standalone_h = {
            "channels": [
                {
                    "name": "singlechannel_xgb",
                    "samples": [
                        {
                            "name": f"{self.templ.id()}",
                            "data": (BZttuple.n).tolist(),
                            "modifiers": [
                                {
                                    "name": f"beeston_barlow",
                                    "type": "staterror",
                                    "data": (BZttuple.s).tolist(),
                                },  # branch-speficific per-bin variation
                                {
                                    "name": f"{self.templ.id()}_y",
                                    "type": "normsys",
                                    "data": {
                                        "lo": self.yconstr.n - self.yconstr.s,
                                        "hi": self.yconstr.n + self.yconstr.s,
                                    },
                                },  # NOTE: constrained normalisation
                            ],
                        }
                    ],
                }
            ]
        }
        return standalone_h
