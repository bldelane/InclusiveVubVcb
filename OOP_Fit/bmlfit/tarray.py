"""Base for the data used for the signal templates"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import numpy.typing as npt
from dataclasses import dataclass, field

# NOTE: make hi-level sel private class properties to protect against unwanted changes
@dataclass(frozen=False, kw_only=True, slots=True)
class sig_hi_base_sel:
    """NOTE: make hi-level sel private class properties to protect against unwanted changes"""

    _k_antiComb: float = 0.66  # punzi
    _k_antiVcb: float = 0.375  # 90% exclusive efficiency on signal
    _LTIME_sup: float = 0.002
    _visM_inf: float = 3_500  # anti b->c
    _visM_sup: float = 6285.0  # PDG m(Bc)

    def build(self) -> str:
        return f"XGB_antiComb>{self._k_antiComb} and \
			XGB_antiVcb>{self._k_antiVcb} and \
			B_plus_FIT_LTIME<{self._LTIME_sup} and \
			B_plus_M>{self._visM_inf} and \
			B_plus_M<={self._visM_sup}"


def source_sel(
    tname: str,
    base_sel: str = sig_hi_base_sel().build(),
    silent: bool = False,
    debug: bool = True,
) -> str:  # >=py3.10
    """Impose FS relative-sign selection based on template tname

    Parameters
    ----------
    tname: str
            unique identifier for the templates ['dcs' | 'misid' | 'cf' | 'ws' | 'D0MuNu' | 'DstMuNu' | 'inclSig']
    basel_sel:
            hi-lev sel, set by dataclas sig_hi_base_sel() from its private attributes
    silent: bool, default is False
            suppress the print statements
    debug: bool, default is False
            if True, print a summary of the identifier (`tname`) and the resulting selection string.
    Returns
    -------
    str
            Case-matched hi-level selection
    """
    match tname.split():
        # if silent, suppress the print statements
        case ["obs" | "dcs" | "misid"] if silent:
            _sel = f"{base_sel} and (K_minus_ID*Mu_plus_ID)>0"
        case ["cf"] if silent:
            _sel = f"{base_sel} and (K_minus_ID*Mu_plus_ID)<0"
        case ["comb" | "ws"] if silent:
            _sel = f"{base_sel} and (D0_ID*Mu_plus_ID)<0"
        case ["D0MuNu" | "DstMuNu" | "SemiExclSig" | "sig"] if silent:
            _sel = f"{base_sel}"

            # NOTE order matters
        case ["obs" | "misid" | "dcs"]:
            print("DCS FS sign compliance enforced")
            _sel = f"{base_sel} and (K_minus_ID*Mu_plus_ID)>0"
        case ["cf"]:
            print("CF FS sign compliance enforced")
            _sel = f"{base_sel} and (K_minus_ID*Mu_plus_ID)<0"
        case ["comb" | "ws"]:
            print("WS FS sign compliance enforced")
            _sel = f"{base_sel} and (D0_ID*Mu_plus_ID)<0"
        case ["D0MuNu" | "DstMuNu" | "SemiExclSig" | "sig"]:
            print("Signal MC, no FS relative-sign compliance condition necessary")
            _sel = f"{base_sel}"

        case other:
            print(f"Unknown tname: {other!r}.")

    if debug:
        print(f"Selection report:\nID: '{tname}'\nSelection: {_sel}\n")

    return _sel


@dataclass(frozen=True, kw_only=True, slots=True)
class tarray:  # >=py3.10
    """Dataclass: emplate array to store the candidates' value in fit var, and the relevant weights

    Attributes
    ----------
    name : str
            unique identifier for the tarray and fit component; this is use in the fit spec
    year : str
            unique year identifier component
    data: npt.ArrayLike
            fit variable candidates
    weights: npt.Array = None
            per-event weights
    _id: str(init=False)
            name_year, set in __post_init__

    Methods
    -------
    __post_init__:
            set the _if and _sel private attributes post-initialisation
    """

    name: str
    year: str
    data: npt.ArrayLike
    weights: npt.ArrayLike = None
    _id: str = field(init=False)

    # RFE: unit testing to verify that the data loaded is compatible with the expected FS selection
    def __post_init__(self) -> None:
        object.__setattr__(self, "_id", f"{self.name}_{self.year}")
        print(
            f"SUCCESS: {self.name!r} {self.year!r} tarray loaded with identifier {self._id!r}"
        )

        # check that the year identifier is permitted
        if self.year not in ["2016", "2017", "2018", "5.x_invfb"]:
            raise ValueError("Invalid year identifier")
        # RFE: check consistency of the data and weights dimensions
