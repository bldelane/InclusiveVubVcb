"""Build model and fit the signal channel"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from collections import namedtuple
from bmlfit import Template
import numpy as np


def calc_BZ_scalefactors(Template) -> namedtuple:
    """compute the Bohm-Zech scale factors from weighted observation
    Nucl.Instrum.Meth.A 748 (2014) 1-6

    Parameters
    ----------
    Template: Template
        Class specifying unscaled template
    """
    # SANITY: assert the template is unscaled
    assert Template.ttype == "unscaled template"
    obs_hist = Template.to_hist()
    assert (
        obs_hist.view().value.min() > 0
    ), "ValueError: the obs contains non-positive bins"

    # extract s_i scalefactors
    s_i = (
        obs_hist.view().variance / obs_hist.view().value
    )  # for weighted fits, sqrt(sum(w**2)) is err; correctness check: done

    print("\nSUCCESS: constructed BZ scalefactors\n")
    return s_i


def main():
    pass


if __name__ == "__main__":
    main()
