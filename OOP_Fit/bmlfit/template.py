"""Base class for the signal fit to the DCS data"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import hist
from hist import Hist
import numpy as np
import numpy.typing as npt
from collections import namedtuple
import numpy.typing as npt
from bmlfit import tarray

# parent template class
class Template:
    # protected members: bins, range in mcorr
    def __init__(self, tinput: tarray) -> None:
        self._bins = 30
        self._mrange = [3_500, 7_200]
        self._ttype = "unscaled template"
        self._tinput = tinput

    @property
    def tinput(self):
        return self._tinput

    @property
    def bins(self):
        return self._bins

    @property
    def mrange(self):
        return self._mrange

    @property
    def ttype(self):
        return self._ttype

    def id(self) -> str:
        return f"{self.tinput.name}_{self.tinput.year}"

    # generate the template histogra,
    def to_hist(self, silent=False) -> Hist:
        if self.tinput.weights is not None:
            bhist = Hist(
                hist.axis.Regular(
                    self.bins,
                    *self.mrange,
                    name="data",
                    label=r"$m_{corr}(D^0\mu^+)$ [MeV/$c^2$]",
                ),
                storage=hist.storage.Weight(),
            )
            bhist.fill(self.tinput.data, weight=self.tinput.weights)
            if silent == False:
                print(f"Weighted {self.id()!r} histogram filled")
        if self.tinput.weights is None:
            bhist = Hist(
                hist.axis.Regular(
                    self.bins,
                    *self.mrange,
                    name="data",
                    label=r"$m_{corr}(D^0\mu^+)$ [MeV/$c^2$]",
                )
            )
            bhist.fill(self.tinput.data)
            if silent == False:
                print(f"Regular {self.id()!r} histogram filled")

        return bhist


class BZObs(Template):
    """BZ-scaled observations. The histogram inherited from the Template class
    is not normalised prior to the BZ scaling"""

    def __init__(
        self,
        tinput: tarray,
        bz: npt.ArrayLike,
    ) -> None:
        super().__init__(tinput)
        self._bz = bz
        self._ttype = "BZ-scaled observation data hist"

    @property
    def bz(self):
        return self._bz

    @property
    def ttype(self):
        return self._ttype

    def make_hist(self) -> npt.ArrayLike:
        """Construct unnormalised obs hist and subsequently BZ-scale"""
        # contruct nametuple with fields n (cval) and s (std)
        BZTempl = namedtuple("BZTempl", ["n", "s"])

        # use inherited method to generate unscaled template hist
        unscaled_hist = self.to_hist()

        # instantiate the nametuple and retrun a BZ-scaled template (NOTE: immutable)
        bzt = BZTempl(
            unscaled_hist.view().value / self.bz,  # n
            unscaled_hist.view().variance ** 0.5 / self.bz,  # s
        )
        return bzt


class BZTemplate(Template):
    """BZ-scaled fit template. The histogram inherited from the Template class
    is normalised prior to the BZ scaling"""

    def __init__(
        self,
        tinput: tarray,
        bz: npt.ArrayLike,
    ) -> None:
        super().__init__(tinput)
        self._bz = bz
        self._ttype = "BZ-scaled template"

    @property
    def bz(self):
        return self._bz

    @property
    def ttype(self):
        return self._ttype

    def make_hist(self) -> npt.ArrayLike:
        """normalise histogram and subsequently BZ-scale"""
        # contruct nametuple with fields n (cval) and s (std)
        NormHist = namedtuple("NormHist", ["n", "s"])
        BZTempl = namedtuple("BZTempl", ["n", "s"])

        # use inherited method to generate unscaled template hist
        unscaled_hist = self.to_hist()
        try:
            _norm = unscaled_hist.view().value.sum()
            print("Sourcing norm of weighted hist")
        except:
            _norm = unscaled_hist.view().sum()
            print("Sourcing norm of non-weighted hist")

        # normalise histogram
        try:
            normhist = NormHist(
                unscaled_hist.view().value / _norm,
                unscaled_hist.view().variance ** 0.5 / _norm,
            )
            print("-> created normalised weighted hist ")
        except:
            normhist = NormHist(
                unscaled_hist.view() / _norm,
                unscaled_hist.view() ** 0.5 / _norm,
            )
            print("-> created normalised non-weighted hist ")

        # SANITY: check normalisation to unity
        assert (
            np.abs(normhist.n.sum() - 1.0) < 1e-5
        ), "NormError: template histogram non 1-normalised"

        # instantiate the nametuple and retrun a BZ-scaled template (NOTE: immutable)
        bzt = BZTempl(
            normhist.n / self.bz,  # n
            normhist.s / self.bz,  # s
        )
        return bzt
