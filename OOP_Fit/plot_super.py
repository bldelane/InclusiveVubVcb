"""Plotting base"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import numpy as np
import matplotlib.pyplot as plt
import boost_histogram as bh
import mplhep as hep

plt.style.use("science")


def plot_data(
    data, ax, bins, range, wts=None, _isnorm=False, label=None, color="black", **kwargs
):
    nh, xe = np.histogram(data, bins=bins, range=range)
    cx = 0.5 * (xe[1:] + xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=wts)
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance ** 0.5

    if _isnorm:
        norm = np.sum(nh)
        _nh = nh
        _err = err
        # FIXME: somethimes the division by norm breaks because of type issues
        nh = _nh / norm
        err = _err / norm

    ax.errorbar(
        cx,
        nh,
        yerr=err,
        xerr=(xe[1] - xe[0]) / 2.0,
        fmt=".",
        color=color,
        label=label,
        **kwargs,
    )

    return nh, cx, err


def plot_hist_err(
    data, ax, bins, range, wts=None, _isnorm=False, label=None, color=None, **kwargs
):
    nh, xe = np.histogram(data, bins=bins, range=range)
    cx = 0.5 * (xe[1:] + xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=wts)
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance ** 0.5

    if _isnorm:
        norm = np.sum(nh)
        _nh = nh
        _err = err
        # FIXME: somethimes the division by norm breaks because of type issues
        nh = _nh / norm
        err = _err / norm

    hep.histplot(nh, xe, yerr=err, ax=ax, color=color, label=label, **kwargs)

    return nh, cx, err


def norm_hist(
    _branch,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
) -> "hist":
    """plot normalised hist; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w /= len(_branch)
    if _sw is not None:
        norm_w *= _sw  # not the sweighted hist, then normalised - sloppy but OK for now

    _ax.hist(_branch, weights=norm_w, **kwargs)


def norm_errobar(
    _branch,
    _bins,
    _range,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
) -> "errorbar":
    """plot normalised errorbar; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w /= len(_branch)
    if _sw is not None:
        norm_w *= _sw  # not the sweighted hist, then normalised - sloppy but OK for now

    pop, e = np.histogram(_branch, weights=norm_w, bins=_bins, range=_range)
    assert abs(np.sum(pop) - 1.0) < 1e-5
    _ax.errorbar(x=(e[1:] + e[:-1]) / 2, y=pop, xerr=(e[1:] - e[:-1]) / 2, **kwargs)
