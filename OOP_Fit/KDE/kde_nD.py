"""Perform the n-dimensional KDE of the WS combinatorial & resample to generate a high-stats template. 
Validated against the WS data subjected to a looser cut."""

from builtins import breakpoint
import sys
from urllib.parse import non_hierarchical

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.hist_utils import to_hist
from utils.plot_super import (
    plot_data,
    plot_hist_err,
)  # HACK relative import - need to move to Utils
import matplotlib.pyplot as plt
import kalepy as kale
import os, glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import uproot

plt.style.use("science")

# FIXME: this is a hacky path appendix
sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit/bmlfit")
from tarray import sig_hi_base_sel

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.plot_super import plot_data, norm_hist

# cleanup
for f in glob.glob("/home/blaised/private/Bc2D0MuNuX/OOP_Fit/tmp/kde/*"):
    os.remove(f)

# build the hi-level selection, removing the XGB cuts
SEL = sig_hi_base_sel()
SEL._k_antiComb = 0.0
# SEL._k_antiVcb = 0.0
# SEL._visM_inf = 2_500
# SEL._visM_sup = 8_000
# SEL._LTIME_sup = 0.01
YEAR = "5.x_invfb"

sel_str = f"{SEL.build()} and (D0_ID*Mu_plus_ID)<0"
print(f"Initialised the hi-level selection")

# load the dataframe of interest; these must have sweghts applied
__storage_path = (
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
)

ws = pd.read_pickle(f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNu_ws_sw_xgb.pkl").query(
    f"{sel_str} and D0_M>1835 and D0_M<1910"
)
assert (ws.D0_ID * ws.Mu_plus_ID).max() < 0, "FSChargeError: WS signature not respected"
print("Loading complete")


# data take in mcorr vs antiComb XGB with 10_000 events
obs_b = [
    "B_plus_MCORR",
    "XGB_antiComb",
    # "XGB_antiVcb",
    # "B_plus_M",
    # "B_plus_FIT_LTIME",
    # "D0_M",
]
obs = ws[obs_b]

data = obs.to_numpy().transpose()

kde = kale.KDE(
    data,
    # weights=wts.to_numpy(),
    # reflect=[
    #     [ws.B_plus_MCORR.min(), ws.B_plus_MCORR.max()],
    #     [0, 1],
    #     [0, 1],
    #     [ws.B_plus_M.min(), ws.B_plus_M.max()],
    # ],
    # bandwidth=[0.4, 0.4],  # , 0.05, 0.1],
    kernel="gaussian",
)
res_data = kde.resample(size=10 * len(ws))
print(f"Size of resampled data: {res_data.shape} events")

# init KDE dataframe
_data = {}
for _c in range(len(obs_b)):
    _data[obs_b[_c]] = res_data[_c]
kde_df = pd.DataFrame.from_dict(_data)

fig, axs = plt.subplots(ncols=2, nrows=3, figsize=(7, 6))
fig.subplots_adjust(hspace=0.4, wspace=0.3)
axs = axs.ravel()

BINS = 50
plt_config = {
    "B_plus_MCORR": {
        "range": [ws.B_plus_MCORR.min(), ws.B_plus_MCORR.max()],
        "bins": BINS,
        "label": r"$m_{corr}(D^+ \mu^{+})$ [MeV/$c^{2}$]",
    },
    "B_plus_M": {
        "range": [ws.B_plus_M.min(), ws.B_plus_M.max()],
        "bins": BINS,
        "label": r"$m(D^+\mu^{+})$ [MeV/$c^{2}$]",
    },
    "XGB_antiComb": {
        "range": [0, 1],
        "bins": BINS,
        "label": r"$\kappa_{comb}$",
    },
    "XGB_antiVcb": {
        "range": [0, 1],
        "bins": BINS,
        "label": r"$\kappa_{b\to c}$",
    },
    "B_plus_FIT_LTIME": {
        "range": [ws.B_plus_FIT_LTIME.min(), ws.B_plus_FIT_LTIME.max()],
        "bins": BINS,
        "label": r"$\tau(X_b)$",
    },
    "D0_M": {
        "range": [ws.D0_M.min(), ws.D0_M.max()],
        "bins": BINS,
        "label": r"Charm mass [MeV/$c^{2}$]",
    },
}

for i in range(len(obs_b)):
    plot_data(
        data=ws[obs_b[i]],
        ax=axs[i],
        **{key: value for key, value in plt_config[obs_b[i]].items() if key != "label"},
        isnorm=True,
        markersize=2,
        elinewidth=1.0,
        capsize=0.0,
        label="WS data",
    )
    norm_hist(
        _branch=kde_df[obs_b[i]],
        _ax=axs[i],
        ls=":",
        **{key: value for key, value in plt_config[obs_b[i]].items() if key != "label"},
        histtype="step",
        label="KDE",
        color="tab:blue",
        _isnorm=True,
    )
    axs[i].set_xlabel(plt_config[obs_b[i]]["label"])
    axs[i].set_ylabel("Candidates, normalised")
    axs[i].legend()
[plt.savefig(f"test_kde.{ext}") for ext in ("png", "pdf")]

# exploit uproot to generate save the KDE-resampled data
TOROOT = False

if TOROOT:
    with uproot.recreate(
        f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/{YEAR}/Bc2D0MuNuX_kde.root"
    ) as file:
        file["DecayTree"] = kde_df
else:
    kde_df.to_pickle(
        f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/{YEAR}/Bc2D0MuNuX_kde_kde_sw.pkl"
    )


# standalone: check correlations / sculpting
# ------------------------------------------
# mpl.rcParams.update(mpl.rcParamsDefault)
# with sns.axes_style("white"):
#     mask = np.zeros_like(ws.corr())
#     mask[np.triu_indices_from(mask)] = True
#     f, ax = plt.subplots(figsize=(20, 17))
#     ax = sns.heatmap(
#         ws.corr(),
#         annot=True,
#         cmap="coolwarm",
#         fmt=".2f",
#         vmin=-1.0,
#         vmax=1.0,
#         mask=mask,
#         square=True,
#         cbar_kws={"label": "Correlation [Arbitrary Units]"},
#     )
#     # ax.set_xticklabels(rotation=75, fontsize=10)
#     # ax.set_yticklabels(fontsize=10)
#     # ax.figure.axes[-1].yaxis.label.set_size(25)
#     [plt.savefig(f"test_corr.{ext}") for ext in ("pdf", "png")]

# fig, ax = plt.subplots()
# for k in (0.0, 0.1, 0.3, 0.7):
#     ax.hist(
#         ws.query(f"XGB_antiComb>{k}").D0_M,
#         # weights=ws.query(f"XGB_antiComb>{k}").sw,
#         density=True,
#         label=str(k),
#         bins=30,
#         # range=[4_200, 7_200],
#         histtype="step",
#     )
#     ax.legend()
# plt.savefig("test_sculpting.png")
