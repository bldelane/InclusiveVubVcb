"""Perform the n-dimensional KDE of the WS combinatorial & resample to generate a high-stats template. 
Validated against the WS data subjected to a looser cut."""

from builtins import breakpoint
import sys
from urllib.parse import non_hierarchical

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.hist_utils import to_hist
from utils.plot_super import (
    plot_data,
    plot_hist_err,
)  # HACK relative import - need to move to Utils
import matplotlib.pyplot as plt
import kalepy as kale
import os, glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import uproot

plt.style.use("science")

# FIXME: this is a hacky path appendix
sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit/bmlfit")
from tarray import sig_hi_base_sel

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.plot_super import plot_data, norm_hist

if __name__ == "__main__":

    # load the WS data with nominal selection, modulo anti-combination XGB response cut
    SEL = sig_hi_base_sel()

    sel_str = f"{SEL.build()}"
    print(f"Initialised the hi-level selection")

    # load the dataframe of interest; these must have sweghts applied
    __storage_path = (
        "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
    )
    ws = pd.read_pickle(
        f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNu_ws_sw_xgb.pkl"
    ).query(f"{sel_str} and B_plus_MCORR<10_000 and (D0_ID*Mu_plus_ID)<0")

    assert (
        ws.D0_ID * ws.Mu_plus_ID
    ).max() < 0, "FSChargeError: WS signature not respected"

    kde = pd.read_pickle(
        f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNuX_kde_kde_sw.pkl"
    ).query(f"XGB_antiComb>0.875 and B_plus_MCORR<10_000")
    print("Loading complete")

    plt_config = {
        "range": [ws.B_plus_MCORR.min(), ws.B_plus_MCORR.max()],
        "bins": 50,
    }

    fig, ax = plt.subplots()
    plot_data(
        data=ws.B_plus_MCORR,
        # wts=ws.sw,
        ax=ax,
        **plt_config,
        isnorm=True,
        markersize=2,
        elinewidth=1.0,
        capsize=0.0,
        label="WS data",
    )
    plot_data(
        data=kde.B_plus_MCORR,
        # wts=kde.sw,
        ax=ax,
        **plt_config,
        label="KDE",
        col="darkorange",
        isnorm=True,
        markersize=2,
        elinewidth=1.0,
        capsize=0.0,
    )
    ax.set_xlabel(r"$m_{corr}(D^+\mu^{+})$ [MeV/$c^{2}$]")
    ax.set_ylabel("Candidates, normalised")
    ax.legend()
    [plt.savefig(f"validate_sw_kde.{ext}") for ext in ("png", "pdf")]
