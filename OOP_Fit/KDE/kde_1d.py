"""Perform the 1D KDE of teh WS combinatorial & resample to generate a high-stats template. 
Validated against the WS data subjected to a looser cut."""

from builtins import breakpoint
import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.hist_utils import to_hist
from utils.header import *
from plot_super import (
    plot_data,
    plot_hist_err,
)  # HACK relative import - need to move to Utils
import matplotlib.pyplot as plt
import kalepy as kale
import os, glob

# cleanup

for f in glob.glob("/home/blaised/private/Bc2D0MuNuX/OOP_Fit/tmp/kde/*"):
    os.remove(f)

# FIXME this reads in the WS data without the antiComb cut applied before antiVcb, see ANA
basecut = 0.7
ws = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(
    f"target==2 and \
        B_plus_M>3500 and B_plus_M<6285. and \
        B_plus_FIT_LTIME<0.002 and \
        XGB_antiVcb>0.4"  # NOTE: relaxed cut, we will need to understand if it's compatible with the template
)
assert (ws.D0_ID * ws.Mu_plus_ID).max() < 0

# ATTEMPT KDE with soft antiComb cut
plt_conf = {"bins": 30, "range": [4_200, 7_200], "_isnorm": True}
fig, (ax, ax_p) = plt.subplots(
    2,
    1,
    gridspec_kw={"height_ratios": [5, 1]},
    sharex=True,
)
nh, cx, err = plot_data(
    data=ws.query(f"XGB_antiComb>{basecut}").B_plus_MCORR,
    ax=ax,
    wts=ws.query(f"XGB_antiComb>{basecut}").sw,
    **plt_conf,
)
ax.plot([], [], "", color="white", label=r"Train, $\chi_{comb}>$ %s" % basecut)
# NOTE: up-shift sWeights to ensure that they are non-negative

rescaled_sw = ws.query(f"XGB_antiComb>{basecut}").sw - np.min(
    ws.query(f"XGB_antiComb>{basecut}").sw
)
assert rescaled_sw.min() >= 0, "Negative entry in rescaled sWeights"
# resample from KDE
res = kale.resample(
    ws.query(f"XGB_antiComb>{basecut}").B_plus_MCORR,
    weights=rescaled_sw,
    size=1_000_000,
)

# save nominal
with open("OOP_Fit/tmp/kde/nominal_ws_kde.npy", "wb") as f:
    np.save(f, res)

knh, kcx, kerr = plot_hist_err(res, ax=ax, edges=True, color="tab:blue", **plt_conf)
ax.legend()

# PULL PLOT
ax_p.set_ylim(-5.5, 5.5)
ax_p.axhline(0, color="forestgreen", lw=0.5, ls="--")
ax_p.axhline(5, color="red", lw=0.5, ls=":")
ax_p.axhline(-5, color="red", lw=0.5, ls=":")
ax_p.axhspan(-3, 3, facecolor="forestgreen", alpha=0.1)
PULLS = (nh - knh) / np.sqrt(err**2 + kerr**2)
ax_p.bar(x=cx, height=PULLS, color="black", width=(cx[1] - cx[0]))
ax.set_ylim(bottom=0)
ax.set_ylabel("Candidates, normalised")
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
ax_p.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV$/c^2$]")

[plt.savefig(f"OOP_Fit/tmp/kde/test_kde.{ext}") for ext in ("pdf", "png", "eps")]

# what happens if we try to compare to the nominal template?
for kappa in [0.1, 0.3, 0.4, 0.5, 0.7]:
    fig, (ax, ax_p) = plt.subplots(
        2,
        1,
        gridspec_kw={"height_ratios": [5, 1]},
        sharex=True,
    )
    ax.plot([], [], "", color="white", label=r"Validation, $\chi_{comb}>$ %s" % kappa)
    nh, cx, err = plot_data(
        data=ws.query(f"XGB_antiComb>{kappa}").B_plus_MCORR,
        ax=ax,
        wts=ws.query(f"XGB_antiComb>{kappa}").sw,
        **plt_conf,
    )
    knh, kcx, kerr = plot_hist_err(res, ax=ax, edges=True, color="tab:blue", **plt_conf)
    ax.legend()

    # PULL PLOT
    ax_p.set_ylim(-5.5, 5.5)
    ax_p.axhline(0, color="forestgreen", lw=0.5, ls="--")
    ax_p.axhline(5, color="red", lw=0.5, ls=":")
    ax_p.axhline(-5, color="red", lw=0.5, ls=":")
    ax_p.axhspan(-3, 3, facecolor="forestgreen", alpha=0.1)
    PULLS = (nh - knh) / np.sqrt(err**2 + kerr**2)
    ax_p.bar(x=cx, height=PULLS, color="black", width=(cx[1] - cx[0]))
    ax.set_ylim(bottom=0)
    ax.set_ylabel("Candidates, normalised")
    ax.set_title("LHCb Unofficial 2018 (13 TeV)")
    ax_p.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV$/c^2$]")
    [
        plt.savefig(f"OOP_Fit/tmp/kde/val_kde_{kappa}.{ext}")
        for ext in ("pdf", "png", "eps")
    ]

# compare what happens when train KDE on 0.7
basecut = 0.3
ws_loose = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(
    f"target==2 and \
        B_plus_M>3500 and B_plus_M<6285. and \
        B_plus_FIT_LTIME<0.002 and \
        XGB_antiVcb>0.4 and \
        XGB_antiComb>{basecut}"  # NOTE: relaxed cut, we will need to understand if it's compatible with the template
)
assert (ws_loose.D0_ID * ws_loose.Mu_plus_ID).max() < 0

# sW
rescaled_sw = ws_loose.sw - np.min(ws_loose.sw)
assert rescaled_sw.min() >= 0, "Negative entry in rescaled sWeights"

# resample from KDE
res_loose = kale.resample(ws_loose.B_plus_MCORR, weights=rescaled_sw, size=1_000_000)
# save nominal
with open("OOP_Fit/tmp/kde/syst_ws_kde.npy", "wb") as f:
    np.save(f, res_loose)

with plt.style.context("high-vis"):
    fig, ax = plt.subplots()
    nh1, cx1, err1 = plot_data(res, ax=ax, label="Nominal", **plt_conf)
    nh2, cx2, err2 = plot_hist_err(
        res_loose, ax=ax, edges=True, label="Loose", color="#d53e4f", **plt_conf
    )
    chisqndf = np.sum((nh1 - nh2) ** 2 / (err1**2 + err2**2)) / 30.0
    ax.plot([], [], "", color="white", label=r"$\chi^2/$ndf = {:2f}".format(chisqndf))
    ax.legend()
    ax.set_ylabel("Candidates, normalised")
    ax.set_title("LHCb Unofficial 2018 (13 TeV)")
    ax.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV$/c^2$]")
    [
        plt.savefig(f"OOP_Fit/tmp/kde/kde_comparison_{kappa}.{ext}")
        for ext in ("pdf", "png", "eps")
    ]
