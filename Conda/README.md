# Conda Setup for Analysis pipeline

## Setting up Conda

The file ```Requirements.yml``` contains the requirements to build the HEPENV; run ```conda install Requirements.yml```; 

*Note*: defaults and conda-forge set as the main channels for the conda setup;

# Snakemake

## Running conda on in batch mode

run: ```snakemake --cluster wrappers/condor_wrapper.py --jobs <job number> snake_rule_in_same_directory_as_snakefile```

The main idea is that snakemake rule specification picks up the target file and works backwards for the dependencies; the wrapper allows to prep submission prep scripts per-job for condor.
