# Analysis Pipeline: Snakemake + Condor

The pipeline uses the standard Snakemake workflow. In order to do this, a custom python3 conda environment must be either sourced or created locally.

## Setting the Conda Environment

- The first prerequisite is `conda` (and `pip`). To set up the machinery you'll need a local installation.
- We suggest installation of `miniconda` and on top of that a build of a custom enrironment as detailed below. For local installation of `miniconda`:
  ```bash
  wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  sh Miniconda3-latest-Linux-x86_64.sh "${HOME}/miniconda"
  ```
- You can then build the environment as follows
```bash
conda create -n Bc2D0MuNuXEnv python=3.7
conda install -c conda-forge root
conda install -c conda-forge uproot
conda install -c conda-forge xrootd
conda install -c anaconda pyyaml
conda install -c conda-forge -c bioconda snakemake
conda install -c conda-forge hep_ml
pip install --user root-pandas
pip install --user matplotlib
```
- Before activating the environment you will need to make sure you have a valid grid proxy first. So from a fresh shell you can set things up using:
```bash
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lhcb-proxy-init
conda activate Bc2D0MuNuXEnv
```
- We have dumped our exact environment into the file `env/environment.yml` so in principle you can run the pipeline with this environment using `snakemake --conda --conda-prefix env/environment.yml` (**this has not been tested**).

## Main Idea

Snakemake works by compiling a DAG of dependencies linking rules. `Snakefile` is where the workflow and the corresponding rules are defined.
The configuration parameters that span the entire workflow are defined in `CONFIG/config.yml`.
`rule all` is the final target at the bottom of the DAG, all dependencies and wildcard allocation will proceed in this bottom-up fashion.

The workflow configuration is handled by the `CONFIG/config.yml` file which contains the following:

 - The eos path to look for the ntuples in (parameter called `topdir`)
 - A list of the years to run on (parameter called `years`)
 - A list of the years that MC is available for (`mcyears`)
 - A list of the channels to run on (`channels`)
 - A list of the magnet polarities to run on (`magpols`)
 - A list of the MC decays to run on (`mcdecs`)
 - There is then a lot of configuration stored in a dictionary called `channel_namespace`. This contains the configuration used by each rule in the workflow so things like:
   - What tuples to run on
   - What branches to read in and write out for each rule
   - What selection to apply in each rule

There are two main rules which feed into `rule all`, these are `rule data` and `rule simulation`. These will then invoke various other `rules` depending on whether the relevant
files exist or not. Each rule runs some code (usually a `python` script which lives in the `Executables` directory) and stores the `stdout` and `stderr` in a log file (which will
be put in the `logs` directory) and keeps a benchmark of its CPU time and memory usage (which will be put in the `benchmarks` directory).

The full workflow reads the input ntuples (which is >5000 files), applies the relevant selection, pruning and slimming and reduces them into the files needed for the
Fitter (~30 files). It is a highly parallelised workflow which is suited to running on a batch cluster and can be turned around in a few hours (depending on how many cores are
available).

Currently the output location of all files is hard-coded to a path on the Cambridge cluster, namely `/r01/lhcb/Bc2D0MuNuX/pipeline`. This should be changed and become part of the `CONFIG/config.yml`.

A graphic visualisation of the full DAG (dependencies within the workflow) can be found [here](dag.pdf) but it's so vast it's very hard to read. Much better is too look at the
smaller [data_dag](data_dag.pdf) and [mc_dag.pdf](mc_dag.pdf) figures (some more details on those are below). The full dag visulation is produced with
```bash
snakemake --dag | dot -Tpdf > dag.pdf
```

### The `data` rule

This rule simply checks the relevant files have been produced and makes a file called `DATA`. If the relevant files are not there it will invoke the various other rules to create them. You can run just this part of the workflow with `snakemake DATA`.

The ntuples produced by the `DaVinci` script run on `ganga` are stored in SLWG `eos` area. There are some 400 subfiles for each decay channel, data taking year and magnet polarity. Therefore some 5000 files in total. The snakemake workflow keeps these as subfiles for as long as possible so that selection can be applied in parallel on a batch cluster.

The `data` rule will invoke the following rules:

 1. Apply the selection to the data (`rule apply_selection_data`)
 2. Split out the signal and misid components (`rule split_misid`)
 3. Add some angular variables (`rule angvars_data`)
 4. Merge each of the subfiles into one file (`rule merge_data`)
 5. Combine the magnet polarities (`rule comb_mag_pols_data`)
 6. Run event mixing to produce a combinatorial background sample (`rule event_mix_data`)
 7. Compute some kinematic observables for the combinatorial sample (`rule add_comb_obs_data`)
 8. Reweight the combinatorial sample to match the signal kinematics (`rule reweight_comb_data`)
 9. Apply the mass cuts and slim the tuples ready for the fitter (`rule prepare_fit_files_data`)

A graphic visualisation of the DAG (dependecy of each rule on others) for the data can be seen [here](data_dag.pdf) for just the 2016 data and only 4 subfiles per channel and magnet polarity. This
was produced by making a slice of the full list of subjobs (see L40 commented in [Snakefile](Snakefile)) after changing the [CONFIG/config.yml](CONFIG/config.yml) script to run
on only 2016 with the command `snakemake DATA --dag | dot -Tpdf > data_dag.pdf`

To run the full workflow on all of the data you will need to use some batch resources. We use the condor system (available on `lxplus`) and you can run it like this (**NOTE you are
probably going to want to do this in a screen or tmux session**):
```bash
snakemake DATA --cluster CONFIG/condor_wrapper.py --jobs 1000 --latency-wait 120
```

### The `simulation` rule

The simulation is a bit more straightfoward. The `simulation` rule has the same basic structure in that it will check for the relevant files and make a file called `MC`. If the
relevant files are not present it will invoke whichever rules are needed to produce them. As the simulation files are much smaller there is only one subfile for each decay mode,
in each year and each polarity.

The `simulation` rule will invoke the following rules:

 1. Apply the selection to the MC (`rule apply_selection_sim`)
 2. Add some angular variables (`rule angvars_sim`)
 3. Combine the magnet polarities (`rule merge_sim`)
 4. Apply the mass cuts and slim the tuples ready for the fitter (`rule prepare_fit_files_sim`)
 5. Check that each decay expected for each channel and year has been made (`rule finalize_sim`)

A graphic visualisation of the DAG (dependecy of each rule on others) for the simulation can be seen [here](mc_dag.pdf) for just the 2016 simulation. This was produced
after changing the [CONFIG/config.yml](CONFIG/config.yml) script to run
on only the 2016 simulation (variable `mcyears`) with the command `snakemake MC --dag | dot -Tpdf > mc_dag.pdf`

To run the full workflow on all of the simulation it's easiest to use the batch system again (**NOTE you are
probably going to want to do this in a screen or tmux session**):
```bash
snakemake MC --cluster CONFIG/condor_wrapper.py --jobs 20 --latency-wait 120
```
Although it's quick enough that this can be run locally in less than an hour if you provide several cores:
```bash
snakemake MC -j8
```

## A few tips in practice

- We have a lot of files. Building the DAG of jobs is pretty slow (and has to be done every time). So best practise is to crack open a `screen` or `tmux` session. Enter the relevant command and go make a coffee.

- You can run the entire workflow using just one command:
```bash
snakemake --cluster CONFIG/condor_wrapper.py --jobs 2000 --latency-wait 120
```

- In practise this is difficult to monitor and check the progress of and hard to debug if something fails.
- The slowest part of the pipeline is actually accessing the files on `eos` over `xrootd` and running the first part of the selection.
- I usually run this bit first on subsets of years. So for example I would edit the `CONFIG/config.yml` file to do just 2011 and 2012 and then do something like:
```bash
snakemake --until apply_selection_data --cluster CONFIG/condor_wrapper --jobs 1000 --latency-wait 120
```
- I would then add 2015 and 2016 back into `CONFIG/config.yml` (note that 2011 and 2012 don't now need to be removed because snakemake is smart enough to know they have just been run already). You may however need to remove the `DATA` file in your local directory so snakemake reruns the workflow. You can then just execute the same command as above.
- Then do the same for 2017 and 2018.
- If this all goes successfully then you have done the hard part which is getting the slimmed tuples off eos and into your local file system.
- You can then reasonably safely run the rest of the workflow in one go
```bash
snakemake --until data --cluster CONFIG/condor_wrapper.py --jobs 2000 --latency-wait 120
```

- I suspect that our directory structure is not best suited to the snakemake system but it is easy for us to interpret and a pain to change. For example using many wildcards in this kind of format `{channel}/{year}/{magpol}/{prefix}.root` means that snakemake can make many matches more than the ones we want. I put in some `wildcard_constraints` to try and handle this but it means that building the DAG is very slow.

- There have been some teething problems with the snakemake xrootd protocol. In particular an error which says there are multiple matching filenames. It looks like this:
```
Traceback (most recent call last):
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/__init__.py", line 547, in snakemake
    export_cwl=export_cwl)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/workflow.py", line 674, in execute
    success = scheduler.schedule()
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/scheduler.py", line 267, in schedule
    run = self.job_selector(needrun)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/scheduler.py", line 391, in job_selector
    c = list(map(self.job_reward, jobs))  # job rewards
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/scheduler.py", line 461, in job_reward
    input_size = job.inputsize
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/jobs.py", line 302, in inputsize
    self._inputsize = sum(f.size for f in self.input)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/jobs.py", line 302, in <genexpr>
    self._inputsize = sum(f.size for f in self.input)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/io.py", line 123, in wrapper
    return func(self, *args, **kwargs)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/io.py", line 137, in wrapper
    kwargs)
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/remote/XRootD.py", line 67, in size
    return self._xrd.file_size(self.remote_file())
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/remote/XRootD.py", line 139, in file_size
    return self._get_statinfo(filename).size
  File "/usera/mkenzie/miniconda3/envs/Bc2D0MuNuXEnv/lib/python3.7/site-packages/snakemake/remote/XRootD.py", line 132, in _get_statinfo
    assert len(matches) == 1
AssertionError
```
  - I can't figure out what the issue is. Clearly the snakemake xrootd protocol thinks it is matching multiple file names so is not sure what to do. 
  - What's weird is that this only happens for certain jobs some of the time. So if you keep resubmitting you will eventually process everything. 
  - One potential solution I can think of is to either rename every file for the ntuples on eos or setup some softlinks for them with different filenames. Where the long random prefix string is remove and we label files as e.g. `Bc2D0MuNuXSlim_0.root` instead of e.g. `0_2020_04_363964_363964198_Bc2D0MuNuXSlim.root`