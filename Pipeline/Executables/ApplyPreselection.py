#!/usr/bin/env python

# --- SKIMMER SCRIPT TO IMPLEMENT TRIGGER SELECTION AND SKIM SELECTION BRANCHES ---

# PyROOT file to skim the data and MC Tuples to copy trees to root files with only selection
# variables written to file 

# __author__: Blaise Delaney
# __email__: blaise.delaney@cern.ch

#----------------------------------------------------------------------------
# *USE*: python ApplyPreselection STRIPPED_ROOTFILE FILTERED_ROOTFILE <flags>
#----------------------------------------------------------------------------

import ROOT as r
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse


# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]

parser = argparse.ArgumentParser(description='Wildcards-enabled for Selection of pairs (dir, sel)')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-m','--mode', dest='mode', help='Channel [D0MuNu/JpsiMuNu]', required=True)
parser.add_argument('-p', '--pars', type=pair, nargs='+', dest='pars', help='pair: (tuple, stripping condition)', required=True)



def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string



def Select(tree_directory, sel_file):

    selection = ParseSelection(sel_file)
    
    print(f"Full path checks: {stripped_file_name}, {tree_directory}Tuple/DecayTree")
    #print(f"Full path checks: {stripped_file_name}, {tree_directory}/DecayTree")
    
    decaytree = stripped_file.Get(f"{tree_directory}Tuple/DecayTree")
    #decaytree = stripped_file.Get(f"{tree_directory}/DecayTree")
    population = int(decaytree.GetEntries())
    print(f"Stripped population: {population}")
    
    # read in dict of relevant branches, while killing the rest
    with open(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/{mode}/SelectionBranches.yml", 'r') as stream:
    #with open(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/JpsiMuNu/LocalSelectionBranches.yml", 'r') as stream:
        inbranches = load(stream, Loader=Loader)
    # set all branches to status == 0
    decaytree.SetBranchStatus("*", 0)
    # activate branches of interest
    [ decaytree.SetBranchStatus(b, 1) for b in inbranches.values() ]
    
    D02KPi_dir = target_file.mkdir(f"{tree_directory}Tuple")
    D02KPi_dir.cd()
    
    print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print('Selection in place:')
    print('-------------------')
    print(selection)
    
    target_tree = decaytree.CopyTree(selection)
    print( "\n===> Entries written to skimmed file:", target_tree.GetEntries() )
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    
    
    print(f"\nSkimmed tree saved to {target_file_name}/{tree_directory}Tuple")





# start keeping time
import time
start = time.time()

stripped_file_name = vars(parser.parse_args())['input']
target_file_name = vars(parser.parse_args())['output']
mode =vars(parser.parse_args())['mode']

stripped_file = r.TFile.Open(stripped_file_name)
target_file = r.TFile.Open(target_file_name, "recreate")

conf_pair =vars(parser.parse_args())['pars'][0] 

#patch to accommodate for both tuples in Bc2D0MuNu (sig, misID) present in stripping
if stripped_file_name.find("MC") != -1:

    tup_dir=conf_pair[0]
    sel_conf=conf_pair[1]
    print(f"\nSkimming configuration: dir={tup_dir}, sel={sel_conf}")
    print(f"\n===> Executing Selection Stage for parameters above")
    
    Select(tup_dir, sel_conf) 

else:
    if stripped_file_name.find("Jpsi") != -1:
    
        tup_dir=conf_pair[0]
        sel_conf=conf_pair[1]
        print(f"\nSkimming configuration: dir={tup_dir}, sel={sel_conf}")
        print(f"\n===> Executing Selection Stage for parameters above")
        
        Select(tup_dir, sel_conf) 
    else:
        #signal
        sig_tup_dir=conf_pair[0]
        sig_sel_conf=conf_pair[1]
        print(f"\nSkimming configuration: dir={sig_tup_dir}, sel={sig_sel_conf}")
        print(f"\n===> Executing Selection Stage for parameters above")
        
        Select(sig_tup_dir, sig_sel_conf) 
        
        #misID
        misID_sel_conf = os.path.splitext(f"{sig_sel_conf}")[0]+'_FakeMuon.yml'
        print(f"\nSkimming configuration: dir={sig_tup_dir}_FakeMuon, sel={misID_sel_conf}")
        print(f"\n===> Executing Selection Stage for parameters above")
        
        Select(sig_tup_dir+"_FakeMuon", misID_sel_conf) 
   


target_file.Write()
target_file.Close()

    
end = time.time()
print( 'Compilation time in seconds: ', (end - start) )
print( '\n           ------ SKIMMING COMPLETED -----\n' )
