import sys
f = sys.argv[1]

from ROOT import TObject, TFile

tf = TFile(f,"update")

import array

tree = tf.Get('DecayTree')

lt = array.array('f',[-99])

ltbr = tree.Branch("B_plus_FIT_LTIME", lt, "B_plus_FIT_LTIME/F")

for ev in range(tree.GetEntries()):
  tree.GetEntry(ev)
  lt[0] = (tree.B_plus_M*tree.B_plus_FD_OWNPV)/(tree.B_plus_P*3.e2)
  ltbr.Fill()

tree.Write("", TObject.kOverwrite)

tf.Close()
