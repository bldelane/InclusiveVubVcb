#!/usr/bin/env python

# --- WRITE TO FILE ONLY FIT BRANCHES ---

# __author__: Blaise Delaney
# __email__: blaise.delaney@cern.ch


import ROOT as r
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse


parser = argparse.ArgumentParser(description='Wildcards-enabled for Selection of pairs (dir, sel)')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-m','--mode', dest='mode', help='Channel [D0MuNu/JpsiMuNu]', required=True)
parser.add_argument('-d','--dirs', nargs='+', dest='tuple_dirs', help='NTuple directories', required=True)


def WriteVars(tree_directory):

    print(f"Full path checks: {stripped_file_name}, {tree_directory}Tuple/DecayTree")
    
    decaytree = stripped_file.Get(f"{tree_directory}Tuple/DecayTree")
    population = int(decaytree.GetEntries())
    print(f"Stripped population: {population}")
    
    # read in dict of relevant branches, while killing the rest
    with open(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/{mode}/FitVariables.yml", 'r') as stream:
        inbranches = load(stream, Loader=Loader)
    
    # set all branches to status == 0
    decaytree.SetBranchStatus("*", 0)
    # activate branches of interest
    [ decaytree.SetBranchStatus(b, 1) for b in inbranches.values() ]
    
    D02KPi_dir = target_file.mkdir(f"{tree_directory}Tuple")
    D02KPi_dir.cd()
    
    print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++") 
    target_tree = decaytree.CloneTree()
    print( "===> Entries written to fitting file:", target_tree.GetEntries() )
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++") 
    print(f"\nFitting tree saved to {target_file_name}/{tree_directory}Tuple")

    target_tree.Write()


stripped_file_name = vars(parser.parse_args())['input']
target_file_name = vars(parser.parse_args())['output']
mode =vars(parser.parse_args())['mode']

stripped_file = r.TFile.Open(stripped_file_name)
target_file = r.TFile.Open(target_file_name, "recreate")

    
for dir in vars(parser.parse_args())['tuple_dirs']: 
    print(dir)
    WriteVars(str(dir))


target_file.Write()
target_file.Close()

print(" ===== READY FOR FIT ===== ")
    
