import argparse
parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-d','--dirs', dest='dir', help='Tuple directory in ROOT file', default=None)
opts = parser.parse_args()

import ROOT, pandas

from ROOT import TMath, TGraph, TCanvas, TColor, TFile
from ROOT import kBlue, kGreen, kRed, kRed

import uproot, pandas
from root_pandas import *
import numpy as np
# enable multithreading
#ROOT.ROOT.EnableImplicitMT() #---> maybe revisit this later?
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys
import os

RDF = ROOT.ROOT.RDataFrame
ROOT.gInterpreter.Declare('#include "{}"'.format("common/EventMixing_D0Mu.h"))

'''
# ==================================== 2D h(R,Z) for sammpling ====================================

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
original_df = RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"{eospath}Blaise/Bc2D0MuNuX_FIT_COMPONENTS/DATA/OFFLINESELECTED_noVisMcuts.root")

treename="DecayTree"
treefile = "SVRZ_forSampling.root"

df_augmented_4vecs = original_df.Filter("(B_plus_M>6280 && (K_minus_ID*Mu_plus_ID>0)) || (B_plus_M>5400 && (K_minus_ID*Mu_plus_ID<0) )") \
                                .Define("comb_B_FlightDir_R", "sqrt( (B_plus_SV_X - B_plus_OWNPV_X)*(B_plus_SV_X - B_plus_OWNPV_X) + (B_plus_SV_Y - B_plus_OWNPV_Y)*(B_plus_SV_Y - B_plus_OWNPV_Y) )") \
                                .Define("comb_B_FlightDir_Z", "(B_plus_SV_Z - B_plus_OWNPV_Z)")

minR = df_augmented_4vecs.Min("comb_B_FlightDir_R").GetValue()
maxR = df_augmented_4vecs.Max("comb_B_FlightDir_R").GetValue()
minZ = df_augmented_4vecs.Min("comb_B_FlightDir_Z").GetValue()
maxZ = df_augmented_4vecs.Max("comb_B_FlightDir_Z").GetValue()

c = TCanvas()
SV_RZ = df_augmented_4vecs.Histo2D(("h", "SV 2D hist(R,Z)",40, 0, 1, 40, 0, 20), "comb_B_FlightDir_R", "comb_B_FlightDir_Z")
SV_RZ.Draw("COLZ")
SV_RZ.GetXaxis().SetTitle("#it{B} Flight Distance #it{R}")
SV_RZ.GetYaxis().SetTitle("#it{B} Flight Distance #it{Z}")
c.Print("SVRZ.pdf")
hist2dfile = TFile(treefile, "RECREATE")
SV_RZ.Write()
hist2dfile.Write()
hist2dfile.Close()
# ==============================================================================================
'''


# ==================================== EVENT MIXING SECTION ====================================

def LoadLocalTree(infile, treeloc, branch_list, maxentries=None):
    
    if maxentries is not None:
      print(f"Warning. Enganing test mode: will read in {maxentries} events")
      events = uproot.open(f"{infile}:{treeloc}")
      df = events.arrays(branch_list, entry_stop=maxentries, library="pd")
    
    if maxentries is None:
      print(f"Reading in full dataset with {maxentries} events")
      events = uproot.open(f"{infile}:{treeloc}")
      df = events.arrays(branch_list, library="pd")
    
    print("Booked branches :", df.keys())

    return df


infile_name = opts.input
print(f"COMMENCING EVENT MIXING ON: {infile_name}")
tree_directory = opts.dir

# this is a bit risky and really should be passed as an option
# assign daughter branches from hadron in B decay
if infile_name.find("Jpsi") != -1:
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
else:
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"

branches_of_interest= ["B_plus_SV_X",
                       "B_plus_SV_Y",
                       "B_plus_SV_Z",
                       "B_plus_OWNPV_X",
                       "B_plus_OWNPV_Y",
                       "B_plus_OWNPV_Z",
                       f"{hadron}_IPCHI2_OWNPV",
                       f"{hadron}_PX", f"{hadron}_PY", f"{hadron}_PZ", f"{hadron}_M",
                       "Mu_plus_PX", "Mu_plus_PY", "Mu_plus_PZ",
                       f"{had_daughter_1}_PX", f"{had_daughter_1}_PY", f"{had_daughter_1}_PZ",
                       f"{had_daughter_2}_PX", f"{had_daughter_2}_PY", f"{had_daughter_2}_PZ",
                       f"{had_daughter_2}_ID", "Mu_plus_ID"]


# all that is needed is the signal tree, no need to loop over signal and or misID
if tree_directory:
  OriginalDF = LoadLocalTree(infile_name, f"{tree_directory}Tuple/DecayTree", branch_list=branches_of_interest, maxentries=None)
else:
  OriginalDF = LoadLocalTree(infile_name, "DecayTree", branch_list=branches_of_interest, maxentries=None)

print(f"====>DATAFRAME LOADED: {OriginalDF.shape[0]} events.")

# Implement Event Mixing and reset entry order for a new concat
# to avoid preserving the event order in the original df
B_Observables = [col for col in OriginalDF if col.startswith('B_plus')]
hadron_Observables = [col for col in OriginalDF if col.startswith(f"{hadron}") or col.startswith(f"{had_daughter_1}") or col.startswith(f"{had_daughter_2}") ]  #hadron and daughters for CosXY(Z) variables
Muon_Observables = [col for col in OriginalDF if col.startswith('Mu_plus') ]



# merge columns, regardless of indexEventMixedDF = pandas.concat([MuonEvents, D0Events, BEvents], axis=1)
# each branch a different seed
N_MIX = 15
MIX_DICT= {}
if infile_name.find("Jpsi") != -1:
    for n in range(N_MIX):
        # sample with replacement for Jpsi-n-iteration then collate.
        BEvents = OriginalDF[B_Observables].sample(frac=1, replace=True, random_state = n*7).reset_index(drop=True)
        hadronEvents = OriginalDF[hadron_Observables].sample(frac=1, replace=True, random_state= n*111).reset_index(drop=True)
        MuonEvents = OriginalDF[Muon_Observables].sample(frac=1, replace=True, random_state=n*61).reset_index(drop=True)
        MIX_DICT[f"JpsiDF_mix_{n}"] = pandas.concat([MuonEvents, hadronEvents, BEvents], axis=1)
    EventMixedDF = pandas.concat(list(MIX_DICT.values()), axis=0)
else:
    BEvents = OriginalDF[B_Observables].sample(frac=1, random_state=71).reset_index(drop=True)
    hadronEvents = OriginalDF[hadron_Observables].sample(frac=1, random_state=111).reset_index(drop=True)
    MuonEvents = OriginalDF[Muon_Observables].sample(frac=1, random_state=23).reset_index(drop=True)
    EventMixedDF = pandas.concat([MuonEvents, hadronEvents, BEvents], axis=1)

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# overwrite SV; implement vectorization;
# first, create columns with random indices applied
if tree_directory:
  upperBregion = pandas.DataFrame(RDF(f"{tree_directory}Tuple/DecayTree", f"{infile_name}").Filter("B_plus_M>6280").AsNumpy())
else:
  upperBregion = pandas.DataFrame(RDF("DecayTree", f"{infile_name}").Filter("B_plus_M>6280").AsNumpy())

#random indices
r_ind = np.random.randint(low=0, high=upperBregion.shape[0], size = EventMixedDF.shape[0], dtype="int")

# now vectorize the sampler; need no att SV's but flight distances to attach to the PV's to get to SV's
EventMixedDF["B_plus_flight_to_SV_X"] = (upperBregion.iloc[r_ind]["B_plus_SV_X"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_X"]).reset_index(drop=True)
EventMixedDF["B_plus_flight_to_SV_Y"] = (upperBregion.iloc[r_ind]["B_plus_SV_Y"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_Y"]).reset_index(drop=True)
EventMixedDF["B_plus_flight_to_SV_Z"] = (upperBregion.iloc[r_ind]["B_plus_SV_Z"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_Z"]).reset_index(drop=True)

# now get synthetic SV's, where we sampled really how much a B has flown and attach them to PV's per event
EventMixedDF["B_plus_SV_X"] = EventMixedDF["B_plus_OWNPV_X"] + EventMixedDF["B_plus_flight_to_SV_X"]
EventMixedDF["B_plus_SV_Y"] = EventMixedDF["B_plus_OWNPV_Y"] + EventMixedDF["B_plus_flight_to_SV_Y"]
EventMixedDF["B_plus_SV_Z"] = EventMixedDF["B_plus_OWNPV_Z"] + EventMixedDF["B_plus_flight_to_SV_Z"]
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

target_file_name = opts.output
EventMixedDF.to_root(target_file_name, key="DecayTree") #==============================================================================================

print("+++++ MIXING COMPLETE +++++")
