
"""Read and filter file(s) from /eos/"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

import uproot
from argparse import ArgumentParser
import numpy as np
from tqdm import tqdm
from pathlib import Path
# pick up the names for the files on xrootd 
from snakemake.remote.XRootD import RemoteProvider as XRootDRemoteProvider

parser = ArgumentParser()
parser.add_argument('-y','--year', default='2018', help='Year')
parser.add_argument('-d','--data', action="store_true")
parser.add_argument('-m','--mc',   action="store_false")
parser.add_argument('-c','--channel', 
    default=["Bc2D0MuNu", "Bc2D0gMuNu", "Bc2D0pi0MuNu"],
    required=True
    )
parser.add_argument('-E','--eospath',  
    default = "/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4",
    help="main path on eos to the ROOT files")    
args = parser.parse_args()

# pick up the names for the files on xrootd 
from snakemake.remote.XRootD import RemoteProvider as XRootDRemoteProvider
XRootD = XRootDRemoteProvider(stay_on_remote=True)
# data
MU_data_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+"/MU/{n}.root").n
MD_data_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+"/MD/{n}.root").n

# mc
if 
MU_mc_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MU/MC_Bc2JpsiPi_"+args.year+"_{n}_pidgen.root").n
MD_mc_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MD/MC_Bc2JpsiPi_"+args.year+"_{n}_pidgen.root").n

