#!/usr/bin/env python

# --- SKIMMER SCRIPT TO IMPLEMENT TRIGGER SELECTION AND SKIM SELECTION BRANCHES ---

# selection - {D0_M vars} as later needed for SWeighting

# __author__: Blaise Delaney
# __email__: blaise.delaney@cern.ch

#----------------------------------------------------------------------------
# *USE*: python ApplyPreselection STRIPPED_ROOTFILE FILTERED_ROOTFILE <flags>
#----------------------------------------------------------------------------

import ROOT as r
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse


# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]

parser = argparse.ArgumentParser(description='Wildcards-enabled for Selection of pairs (dir, sel)')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-m','--mode', dest='mode', help='Channel [D0MuNu/JpsiMuNu]', required=True)
parser.add_argument('-p', '--pars', type=pair, nargs='+', dest='pars', help='pair: (tuple, stripping condition)', required=True)



def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string



def Select(tree_directory, sel_file):

    selection = ParseSelection(sel_file)
    
    print(f"Full path checks: {stripped_file_name}, {tree_directory}Tuple/DecayTree")
    #print(f"Full path checks: {stripped_file_name}, {tree_directory}/DecayTree")
    
    decaytree = stripped_file.Get(f"{tree_directory}Tuple/DecayTree")
    #decaytree = stripped_file.Get(f"{tree_directory}/DecayTree")
    
    population = int(decaytree.GetEntries())
    print(f"Stripped population: {population}")

    #no need to turn off-on branches here as inherent from preselection and 
    # computation of angular variables 

    D02KPi_dir = target_file.mkdir(f"{tree_directory}Tuple")
    D02KPi_dir.cd()
    print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print('Selection in place (STAGE I):')
    print('-------------------')
    print(selection)
    
    target_tree = decaytree.CopyTree(selection)
    print( "\n===> Entries written to offline-selected (STAGE I) file:", target_tree.GetEntries() )
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    if tree_directory=="B2DMuNuX_D02KPi": assert target_tree.GetEntries() > 0 #can have misID tuples empty  after selection
    
    print(f"\nOffline-selected (STAGE I) tree saved to {target_file_name}/{tree_directory}Tuple")





# start keeping time
import time
start = time.time()

stripped_file_name = vars(parser.parse_args())['input']
target_file_name = vars(parser.parse_args())['output']
mode =vars(parser.parse_args())['mode']

stripped_file = r.TFile.Open(stripped_file_name)
target_file = r.TFile.Open(target_file_name, "recreate")

for pair in vars(parser.parse_args())['pars']:
    tup_dir=pair[0]
    sel_conf=pair[1]
    print(f"\nSkimming configuration: dir={tup_dir}, sel={sel_conf}")
    print(f"\n===> Executing Selection Stage for parameters above")
    
    Select(tup_dir, sel_conf) 


target_file.Write()
target_file.Close()

    
end = time.time()
print( 'Compilation time in seconds: ', (end - start) )
print( '\n           ------ OFFILE SELECTION STAGE I COMPLETED -----\n' )
