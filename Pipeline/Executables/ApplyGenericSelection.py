#!/usr/bin/env python

# --- MAKE IT AS SIMPLE BUT GENERAL AS IT NEEDS TO BE ---

# PyROOT file to loop tuples and apply selections whilst tracking the efficiency

# __author__: Matthew Kenzie
# __email__: matthew.kenzie@cern.ch

#----------------------------------------------------------------------------
# *USE*: python SplitMisID.py <opts>
#----------------------------------------------------------------------------

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'      , required=True                 , help='Input ROOT file')
parser.add_argument('-o','--output'     , required=True                 , help='Output ROOT file')
parser.add_argument('-e','--efficiency' , default="eff.log"             , help='Output Efficiency file')
parser.add_argument('-b','--inbranches' , default=None                  , help='Branch.yml file for branches to read (default will do all)')
parser.add_argument('-B','--outbranches', default=None                  , help='Branch.yml file for branches to write (default will be same as --inbranches)')
parser.add_argument('-s','--selection'  , required=True, action="append", help='Selection.yml files (can pass multiple times for multiple selections)')
parser.add_argument('-t','--tupledir'   , default=[]   , action="append", help='Tuple directories (can pass multiple times for multiple directories)')
parser.add_argument('-T','--treename'   , default='DecayTree'           , help='Tree name')
opts = parser.parse_args()
if len(opts.tupledir)==0: opts.tupledir = ["."]

import os

# efficiency def
def eff(npass, ntotal):
  if ntotal>0:
    eff = float(npass)/float(ntotal)
    eff_err = ( (eff*(1.-eff))/float(ntotal) )**0.5
  else:
    eff = 0.
    eff_err = 0.
  return (100.*eff,100.*eff_err)

# get selection string
import yaml
sel_list = []
for selfile in opts.selection:
  assert( os.path.exists( selfile ) )
  sel = {}
  with open(selfile,'r') as f:
    sel = yaml.safe_load(f)

  sel_name = 'Unnamed%d'%len(sel_list)
  if "name" in sel.keys():
    sel_name = sel["name"]
    sel.pop("name")

  sel_str = "( " + " && ".join(sel.values()) + " )"

  sel_list.append( { "name" : sel_name, "cuts" : sel_str } )

print('Selection in place:')
for sel in sel_list:
  print('  ', sel["name"], ':', sel["cuts"] )

# get in and out branches
inbranches = []
if opts.inbranches:
  inbf = open(opts.inbranches)
  inbranches = yaml.load( inbf, Loader=yaml.Loader )
  inbf.close()
  inbranches = list(inbranches.values())
print('Branches to read in:')
print(inbranches)

outbranches = inbranches.copy()
if opts.outbranches:
  outbf = open(opts.outbranches)
  outbranches = yaml.load( outbf, Loader=yaml.Loader )
  outbf.close()
  outbranches = list(outbranches.values())
print('Branches to write out:')
print(outbranches)

# begin reading / writing files
from ROOT import TFile
print('Will read file:', opts.input)
print('Will write file:', opts.output)
print('Will write efficiency to:', opts.efficiency)

inf  = TFile.Open(opts.input)
outf = TFile.Open(opts.output,'recreate')
efff = open(opts.efficiency,'w')

# now loop tuples
for tup_dir in opts.tupledir:
  intree = inf.Get(opts.treename) if tup_dir=="." else inf.Get(os.path.join(tup_dir,opts.treename))
  if not intree:
    print('Cannot find tree in inputfile')
    continue
  assert(intree)

  intree.SetBranchStatus("*",0)
  [ intree.SetBranchStatus( branch, 1 ) for branch in inbranches if branch in intree.GetListOfBranches() ]

  print()

  if tup_dir!=".":
    out_str = 'Tuple: '+tup_dir
    print(out_str)
    efff.write(out_str+'\n')

  # now work out the efficiencies
  out_str = '{:^27} | {:^38} | {:^38} |'.format('','Exclusive','Cummulative')
  print(out_str)
  efff.write(out_str+'\n')
  inevents = intree.GetEntries()
  cummalative_sel = []
  passes_this = []
  passes_cumm = []
  out_str
  for sel in sel_list:
    cummalative_sel.append( sel["cuts"] )
    this_sel_str = sel["cuts"]
    cumm_sel_str = " && ".join(cummalative_sel)
    passes_this.append( intree.GetEntries( this_sel_str ) )
    passes_cumm.append( intree.GetEntries( cumm_sel_str ) )

    pt = passes_this[-1]
    efft, effte = eff( pt, inevents )
    pc = passes_cumm[-1]
    pl = passes_cumm[-2] if len(passes_cumm)>1 else inevents
    effc, effce = eff( pc, pl )
    out_str = "  Sel: %-20s | %6d / %-6d = (%6.3f +/- %6.3f)%% | %6d / %-6d = (%6.3f +/- %6.3f)%% |"%(sel["name"],pt,inevents,efft,effte,pc,pl,effc,effce)
    print(out_str)
    efff.write(out_str+'\n')

  effs, effse = eff(passes_cumm[-1],inevents)
  out_str = "  TOTAL: %18s | %6d / %-6d = (%6.3f +/- %6.3f)%% |\n"%('',passes_cumm[-1],inevents,effs,effse)
  print(out_str)
  efff.write(out_str)

  print('Entries in: ', intree.GetEntries())
  print('Writing output')

  outtree = intree.CopyTree( " && ".join(cummalative_sel) )

  assert( outtree.GetEntries() == passes_cumm[-1] )

  # #==========================================================
  # # COMMENT OUT IF RUNNING ON MC W/O PID CORRECTION
  # # -----------------------------------------------
  if tup_dir!=".":
    out_dir = outf.mkdir(tup_dir)
    out_dir.cd()
  # #==========================================================

  if outbranches != inbranches:
    outtree.SetBranchStatus("*",0)
    [ outtree.SetBranchStatus( branch, 1 ) for branch in outbranches if branch in outtree.GetListOfBranches() ]

    slimtree = outtree.CloneTree()
    slimtree.Write()
    print('Entries out: ', slimtree.GetEntries() )
    outtree.SetDirectory(0)
  else:
    outtree.Write()
    print('Entries out: ', outtree.GetEntries() )

efff.close()
outf.Close()
inf.Close()