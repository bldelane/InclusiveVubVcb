import numpy as np
import uproot
from hep_ml import reweight
import pandas as pd
from hep_ml.metrics_utils import ks_2samp_weighted
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
from root_pandas import to_root
import pandas

from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys
import os
import argparse

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("common/plotting/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);


# enable multithreading
#ROOT.ROOT.EnableImplicitMT() #---> maybe revisit this later?
ROOT.gInterpreter.Declare('#include "{}"'.format("common/EventMixing_D0Mu.h"))
RDF = ROOT.ROOT.RDataFrame

parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-m','--merged-file', dest='mergedfile',  help='Input ROOT file post-merging', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-d','--dirs', dest='dir', help='Tuple directory in ROOT file', default=None)
opts = parser.parse_args()

# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]


def LoadLocalTree(infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.open(infile)[f"{tupledir}DecayTree"]
    if maxentries is not None:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df


infile_name = vars(parser.parse_args())['input'] # post raw mixed
mergedfile_file_name = vars(parser.parse_args())['mergedfile'] # post raw mixed
tree_directory = vars(parser.parse_args())['dir']

# signal tuple, upper mass band for combinatorial (no DCS/CF trick to make it jpsi-compatible)
print(f"LOADING ORIGINAL DF: {mergedfile_file_name}")
if opts.dir: upperBmass_RDF = RDF(f"{tree_directory}Tuple/DecayTree", mergedfile_file_name).Filter(f"B_plus_M>6280")
else: upperBmass_RDF = RDF("DecayTree", mergedfile_file_name).Filter(f"B_plus_M>6280")
high_mass_data_df= pandas.DataFrame(upperBmass_RDF.AsNumpy())


# assign daughter branches from hadron in B decay
if infile_name.find("Jpsi") != -1:
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
    N_RANGE=5000000
else:
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"
    N_RANGE=1000000


print(f"LOADING EVENT MIXED DF: {infile_name}")
# raw evt mixed tuple
# CPU: cap of 1000000 events reweighted and written to file for sel+fit
evtmixed_df= pandas.DataFrame(RDF("DecayTree", infile_name).Range(N_RANGE).AsNumpy())
print("checkng maxima: ", max(evtmixed_df["B_plus_PT"]), max(evtmixed_df["B_plus_Lab_P"]))

print("Loading complete.")



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++ SETUP LOOP FOR GBR_PLOTS +++++
GBR_branches = [f"{hadron}_PT", f"Mu_plus_PT", f"{hadron}_ETA", f"Mu_plus_ETA",f"B_plus_FD_OWNPV"]

print(f"Reweighting set of vars: ", GBR_branches)

#set reweighter with k-folding for unbiased evaluation
reweighter_base = reweight.GBReweighter(n_estimators=150,
                                        learning_rate=0.1, max_depth=5, min_samples_leaf=100, )#make it ~10% order in min sample split
reweighter = reweight.FoldingReweighter(reweighter_base, n_folds=2)

reweighter.fit(original = evtmixed_df.query("B_plus_M>6280")[GBR_branches],
               target = high_mass_data_df[GBR_branches])

# k-folding should take care of biases, see documentation
folding_weights = reweighter.predict_weights(
    original=evtmixed_df[GBR_branches],
                            )
# assign weights over whole range, k-fold takes care of biases
evtmixed_df["GBR_w"] = folding_weights


#++++++++ Write to target file ++++++++++
# write to target file upating combinatorial and copying over the sig and misID

target_file_name = vars(parser.parse_args())['output']

# restrict to velo and visible mass range in stripping
evtmixed_df.to_root(f"{target_file_name}", key="DecayTree")

print("=== SUCCESS: GBReweighting complete. ===")
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




# ===== PLOTTING SEGMENT =====
# reload target RDF, check sanity
upperCombinatorial_RDF= RDF(f"DecayTree", f"{target_file_name}").Filter("B_plus_M>6280")
print("Plotting initialised...[upper data mass band vs EvtMix+GBR]")

# Read YAML file of variables for plotting
with open(f"common/plotting/EventMixing/{hadron}/PlotPars.yml", 'r') as stream:
        to_plot = yaml.safe_load(stream)


# blanket statement, note how vals will be different between D0 and Jpsi, but things should be consistent
# between data and combinatorial; amend later
cut_string = f"CosXY_Mu_plus_{hadron}>-0.4 && CosXYZ_Mu_plus_{had_daughter_2}<0.999 && CosXYZ_Mu_plus_{had_daughter_1}<0.999"

def Plot(var, data_RDF, GBR_RDF, minvar, maxvar, binsvar, xlabelvar, pdfvar):
    c1 = TCanvas()
    original_shape = data_RDF.Histo1D(("hdata", "Upper data sideband", binsvar, minvar, maxvar), var)
    original_shape.SetLineColor( kWhite)
    original_shape.SetMarkerColor( kGray+2)
    original_shape.SetFillColorAlpha( kGray+2, .5)

    original_shape.Scale(1./original_shape.Integral())

    evtmix_shape = GBR_RDF.Histo1D(("hEvtMix", "Raw event mixing upper sideband", binsvar, minvar, maxvar), var)
    evtmix_shape.SetLineColor( kBlack)
    evtmix_shape.SetMarkerColor( kBlack)
    evtmix_shape.Scale(1./evtmix_shape.Integral())

    GBR_B_shape = GBR_RDF.Histo1D(("hBvars", "GBR with B observables", binsvar, minvar, maxvar), var, "GBR_w")
    GBR_B_shape.SetLineColor( kAzure+1)
    GBR_B_shape.SetMarkerColor( kAzure+1)
    GBR_B_shape.Scale(1./GBR_B_shape.Integral())

    original_shape.GetYaxis().SetTitle("Arbitrary Units")
    original_shape.GetXaxis().SetTitle(xlabelvar)

    original_shape.Draw("E2 L")
    evtmix_shape.Draw("EPSAME")
    GBR_B_shape.Draw("EPSAME")


    legend=TLegend(0.5,0.7,0.8,0.9)
    legend.SetTextFont(132)
    legend.SetTextSize(0.03)
    legend.AddEntry("hdata","2016 Data #it{m_{B}}>6280 MeV/c^{2}","pf")
    legend.AddEntry("hEvtMix", "EvtMix #it{m_{B}}>6280 MeV/c^{2}", "lep")
    legend.AddEntry("hBvars", "GBR(PT,#eta,FD(#it{B})) #it{m_{B}}>6280 MeV/c^{2}", "lep")

    legend.Draw()

    c1.Print(f"common/plotting/EventMixing/{hadron}/PLOTS/"+pdfvar+".pdf")


for branch in to_plot.keys():

    print("===> Plotting ", branch)
    Plot(branch, upperBmass_RDF.Filter(f"B_plus_M>6280 && {cut_string}"), upperCombinatorial_RDF.Filter(f"{cut_string}"), **to_plot[branch])
















