#!/usr/bin/env python

# --- SPLIT OUT SIGNAL AND MISID INTO SEPARATE FILES ---

# PyROOT file to loop the data tuples and split out into two files

# __author__: Matthew Kenzie
# __email__: matthew.kenzie@cern.ch

#----------------------------------------------------------------------------
# *USE*: python SplitMisID.py <opts>
#----------------------------------------------------------------------------

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'      , required=True                 , help='Input ROOT file')
parser.add_argument('-o','--outsig'     , required=True                 , help='Output Signal ROOT file')
parser.add_argument('-m','--outmisid'   , required=True                 , help='Output MisID ROOT file')
parser.add_argument('-e','--effsig'     , required=True                 , help='Output Signal Efficiency file')
parser.add_argument('-E','--effmisid'   , required=True                 , help='Output MisID  Efficiency file')
parser.add_argument('-s','--selection'  , required=True                 , help='Selection file for signal requirement. MisID will be the inversion.')
parser.add_argument('-t','--tupledir'   , default=[]   , action="append", help='Tuple directories (can pass multiple times for multiple directories)')
parser.add_argument('-T','--treename'   , default='DecayTree'           , help='Tree name')
opts = parser.parse_args()
if len(opts.tupledir)==0: opts.tupledir = ["."]

import os

# efficiency def
def eff(npass, ntotal):
  if ntotal>0:
    eff = float(npass)/float(ntotal)
    eff_err = ( (eff*(1.-eff))/float(ntotal) )**0.5
  else:
    eff = 0.
    eff_err = 0.
  return (100.*eff,100.*eff_err)

# get selection string
import yaml
assert( os.path.exists( opts.selection ) )
sel = {}
with open(opts.selection,'r') as f:
  sel = yaml.safe_load(f)

sel_name = 'Unnamed'
if "name" in sel.keys():
  sel_name = sel["name"]
  sel.pop("name")

sel_str = "( " + " && ".join(sel.values()) + " )"
print('Selection in place:')
print('  ', sel_name, ':', sel_str )

# begin reading / writing files
from ROOT import TFile, TChain
print('Will read file:', opts.input)
print('Will write sig file:', opts.outsig)
print('Will write sig efficiency to:', opts.effsig)
print('Will write misid file:', opts.outmisid)
print('Will write misid efficiency to:', opts.effmisid)

intree = TChain(opts.treename)
for tup_dir in opts.tupledir:
  tree_path = opts.treename if tup_dir=="." else os.path.join(tup_dir,opts.treename)
  intree.Add( os.path.join(opts.input,tree_path) )

assert(intree)

print('Entries in: ', intree.GetEntries())

sig_file = TFile.Open(opts.outsig, "recreate")
sig_tree = intree.CloneTree(0)
sig_eff  = open(opts.effsig,'w')
mis_file = TFile.Open(opts.outmisid, "recreate")
mis_tree = intree.CloneTree(0)
mis_eff  = open(opts.effmisid,'w')

# calculate efficiency
inevents = intree.GetEntries()
passevs  = intree.GetEntries( sel_str )
failevs  = inevents - passevs
effp, effpe = eff( passevs, inevents )
efff, efffe = eff( failevs, inevents )
print('{:^27} | {:^38} | {:^38} |'.format('','Signal','MisID'))
print("  Sel: %-20s | %6d / %-6d = (%6.3f +/- %6.3f)%% | %6d / %-6d = (%6.3f +/- %6.3f)%% |"%(sel_name, passevs, inevents, effp,effpe, failevs, inevents, efff, efffe))
sig_eff.write('%6d / %-6d = (%6.3f +/- %6.3f)%%\n'%(passevs,inevents,effp,effpe))
mis_eff.write('%6d / %-6d = (%6.3f +/- %6.3f)%%\n'%(failevs,inevents,efff,efffe))
sig_eff.close()
mis_eff.close()

print('Writing output')

for ev in range(inevents):
  intree.GetEntry(ev)
  if intree.Query("",sel_str,"",1,ev).GetRowCount():
    sig_tree.Fill()
  else:
    mis_tree.Fill()

sig_file.cd()
sig_tree.Write()
print('Entries out (sig):', sig_tree.GetEntries() )

mis_file.cd()
mis_tree.Write()
print('Entries out (misid):', mis_tree.GetEntries() )

sig_file.Close()
mis_file.Close()

