import ROOT 

from yaml import load, Loader
import yaml
from optparse import OptionParser

def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string

RDF = ROOT.ROOT.RDataFrame

#remote_path = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/Blaise/raw_MCFILES/D0MuNu/2016/"
remote_path = "/r02/lhcb/mkenzie/Bc2D0MuNuX/ntuples/MC/"

def ComputeEfficiency(mcfile):

    simchain = RDF("B2DMuNuX_D02KPiTuple/DecayTree", remote_path+mcfile)
    augmented_for_selection = simchain.Define("D0_2Vec", "TVector2(D0_PX, D0_PY)") \
                       .Define("Mu_2Vec", "TVector2(Mu_plus_PX, Mu_plus_PY)") \
                       .Define("Mu_3Vec", "TVector3(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                       .Define("K_3Vec", "TVector3(K_minus_PX, K_minus_PY, K_minus_PZ)") \
                       .Define("Pi_3Vec", "TVector3(Pi_1_PX, Pi_1_PY, Pi_1_PZ)") \
                       .Define("CosXYZ_Mu_plus_Pi_1", "Mu_3Vec.Dot(Pi_3Vec)/(Mu_3Vec.Mag()*Pi_3Vec.Mag())") \
                       .Define("CosXYZ_Mu_plus_K_minus", "Mu_3Vec.Dot(K_3Vec)/(Mu_3Vec.Mag()*K_3Vec.Mag())") \
                       .Define("CosXY_Mu_plus_D0", "(Mu_2Vec.X()*D0_2Vec.X() + Mu_2Vec.Y()*D0_2Vec.Y())/(sqrt(D0_2Vec.X()*D0_2Vec.X()+D0_2Vec.Y()*D0_2Vec.Y())*sqrt(Mu_2Vec.X()*Mu_2Vec.X()+Mu_2Vec.Y()*Mu_2Vec.Y()))")

    # enforce truthmatching
    #augmented_for_selection=augmented_for_selection.Filter("B_plus_BKGCAT==50 && D0_BKGCAT==0")
    #augmented_for_selection=augmented_for_selection.Filter("B_plus_BKGCAT==40 && D0_BKGCAT==0")
    augmented_for_selection=augmented_for_selection.Filter("B_plus_BKGCAT==110")
                        
    preselection_path = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/SelectionConfigs/Preselection/Bc2D0MuNuX_AllCabibbo.yml"
    offline_selection_path = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/SelectionConfigs/Offline_Selection/Bc2D0MuNuX_AllCabibbo.yml"
    
    presel_string = ParseSelection(preselection_path)
    offsel_string = ParseSelection(offline_selection_path)
    
    
    print("-------------------------------------------------------------------------------")
    print(f"Computing efficiencies for file {remote_path}{mcfile}")
    #print("Criteria:\n")
    #print("==> PRESELECTION: ", presel_string)
    #print()
    #print("==> OFFLINE (before MVA): ", offsel_string)
    
    presel_filtered = augmented_for_selection.Filter(presel_string, "Preselection Relative to Stripping Sample")
    offsel_filtered_wrtpresel = presel_filtered.Filter(offsel_string, "Selection relative to Preselection")
    offsel_filtered_wrtstrip = augmented_for_selection.Filter(offsel_string, "Selection relative to truthmatched Stripping Sample")
    
    fully_selected = augmented_for_selection.Filter(presel_string + " && " + offsel_string, "Full Selection Relative to truthmatched Stripping Sample")
    
    
    print("Stats Report:\n")
    p = presel_filtered.Report()
    s = offsel_filtered_wrtpresel.Report()
    s_strip = offsel_filtered_wrtstrip.Report()
    p.Print()
    s.Print()
    s_strip.Print()
    
    print("\n===>Full Selection Efficiency wrt truthmatched Stripping Sample:")
    full = fully_selected.Report()
    full.Print()
    print("\n                        ===  END OF CALCULATION ===                            ")
    print("-------------------------------------------------------------------------------")
    print("\n\n")

    fully_selected.Snapshot("DecayTree","invTruth_MC16_Bc2D0MuNuX.root")

#ComputeEfficiency("MC_Bu2D0XMuNu_D0MuNu_2016_M*.root")
#ComputeEfficiency("MC_Bc2D0MuNu_D0MuNu_2016_M*.root")
#ComputeEfficiency("MC_Bc2D0gMuNu_D0MuNu_2016_M*.root")
#ComputeEfficiency("MC_Bc2D0pi0MuNu_D0MuNu_2016_M*.root")
#
#
#print("===> TOTAL EFFICIENCY ON ALL SIGNAL MC:")
#ComputeEfficiency("MC_Bc2D0*2016_M*.root")
ComputeEfficiency("MC*2016*.root")
