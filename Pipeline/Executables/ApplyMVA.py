#!/usr/bin/env python

from ROOT import TMVA, TFile, TH1F, TTree, TString, TChain
import numpy as np
import math 
import os, sys
from yaml import load, Loader
import argparse


parser = argparse.ArgumentParser(description='Wildcards-enabled for TMVA::Reader()')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-m','--mode', dest='mode', help='Channel [D0MuNu/JpsiMuNu]', required=True)
parser.add_argument('-d','--dirs', nargs='+', dest='tuple_dirs', help='NTuple directories', required=True)


# choice of TMVA methods
methods = ['BDT']

selected_infile = TFile(vars(parser.parse_args())['input'])
outfile = TFile(vars(parser.parse_args())['output'], "RECREATE")
mode =vars(parser.parse_args())['mode']

def RunReader(tree_directory):

    print(f"Full path checks: {selected_infile}, {tree_directory}Tuple/DecayTree")
    intree = selected_infile.Get(f"{tree_directory}Tuple/DecayTree")
    population = int(intree.GetEntries())
    print("===== Intialising the MVA Reader() Stage =====")
    print(f"Post-selection ROOT file population: {population}")


    # instantiate TMVA objects
    TMVA.Tools.Instance()
    TMVA.PyMethodBase.PyInitialize();
    reader = TMVA.Reader( "!Color:!Silent" )
    
    
    from array import array 
    logD0PT = array('f', [0])
    logMuPT = array('f', [0])
    CosXY_Mu_plus_D0 = array('f', [0])
    logD0IPCHI2 = array('f', [0])
    logMuIPCHI2 = array('f', [0])
    logD0FDCHI2 = array('f', [0])
    logBVTXCHI2 = array('f', [0])
    acosBDIRA = array('f', [0])
    
    B_plus_MCORR = array('f', [0])
    D0_M = array('f', [0])
    B_plus_LTIME = array('f', [0])
    FitVar_El = array('f', [0])
    FitVar_Mmiss2 = array('f', [0])
    FitVar_q2 = array('f', [0])
    
    
    reader.AddVariable( "logD0PT := log(D0_PT)", logD0PT )
    reader.AddVariable( "logMuPT := log(Mu_plus_PT)", logMuPT)
    reader.AddVariable( "CosXY_Mu_plus_D0", CosXY_Mu_plus_D0)
    reader.AddVariable( "logD0IPCHI2 := log(D0_IPCHI2_OWNPV)", logD0IPCHI2 )
    reader.AddVariable( "logMuIPCHI2 := log(Mu_plus_IPCHI2_OWNPV)", logMuIPCHI2 )
    reader.AddVariable( "logD0FDCHI2 := log(D0_FDCHI2_OWNPV)",logD0FDCHI2 )
    reader.AddVariable( "logBVTXCHI2 := log(B_plus_ENDVERTEX_CHI2)", logBVTXCHI2 )
    reader.AddVariable( "acosBDIRA := acos(B_plus_DIRA_OWNPV)", acosBDIRA)
    
    
    reader.AddSpectator( "B_plus_MCORR",  B_plus_MCORR )
    reader.AddSpectator( "D0_M",  D0_M )
    reader.AddSpectator( "B_plus_LTIME",  B_plus_LTIME )
    reader.AddSpectator( "FitVar_El",  FitVar_El )
    reader.AddSpectator( "FitVar_Mmiss2",  FitVar_Mmiss2 )
    reader.AddSpectator( "FitVar_q2",  FitVar_q2 )
    
    
    # book classifier with exported weight
    for methodName in methods: 
        weightfile = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/MVA/dataset/weights/TMVAClassification_" + methodName + ".weights.xml"
        name = TString(methodName)
        reader.BookMVA( name, weightfile );
    
    
    # evaluation on DCS data
    nevt = intree.GetEntries()
    
    newtree = intree.CloneTree(0)
    
    for methodName in methods :
        leave0 = methodName+"_output"+"/D"
        leafValue0 = array("d", [0.0])
        
        newBranch = newtree.Branch(methodName+"_RealD0", leafValue0, leave0)
    
        for i in range(nevt):
           
            if i%10000==0: print (f"Processing event {i} out of {nevt}")
            intree.GetEntry(i)
    
    
            logD0PT[0]= math.log(intree.GetLeaf("D0_PT").GetValue())
            logMuPT[0]= math.log(intree.GetLeaf("Mu_plus_PT").GetValue())
          
            CosXY_Mu_plus_D0[0]= intree.GetLeaf("CosXY_Mu_plus_D0").GetValue()
          
            logD0IPCHI2[0]= math.log(intree.GetLeaf("D0_IPCHI2_OWNPV").GetValue())
            logMuIPCHI2[0]= math.log(intree.GetLeaf("Mu_plus_IPCHI2_OWNPV").GetValue())
            
            logD0FDCHI2[0]= math.log(intree.GetLeaf("D0_FDCHI2_OWNPV").GetValue())
            logBVTXCHI2[0]= math.log(intree.GetLeaf("B_plus_ENDVERTEX_CHI2").GetValue())
            acosBDIRA[0]= math.acos(intree.GetLeaf("B_plus_DIRA_OWNPV").GetValue())
           
            B_plus_MCORR[0]=intree.GetLeaf("B_plus_MCORR").GetValue()
            D0_M[0]=intree.GetLeaf("D0_M").GetValue()
            B_plus_LTIME[0]=intree.GetLeaf("B_plus_LTIME").GetValue()
            FitVar_El[0]=intree.GetLeaf("FitVar_El").GetValue()
            FitVar_Mmiss2[0]=intree.GetLeaf("FitVar_Mmiss2").GetValue()
            FitVar_q2[0]=intree.GetLeaf("FitVar_q2").GetValue()
     
    
            eval0 =  reader.EvaluateMVA(methodName) 
            leafValue0[0]=eval0
    
            newtree.Fill()

        
        # save fit vars only 
        new_dir = outfile.mkdir(f"{tree_directory}Tuple")
        new_dir.cd()
        fittree=newtree.CloneTree()
        fittree.Write()
             


for dir in vars(parser.parse_args())['tuple_dirs']: 
    RunReader(str(dir))

outfile.Write()
outfile.Close()
