# This is Matt's implementation
# Found that the other one ran out of memory

import argparse
parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input',  dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-d','--dirs',   dest='dir',    help='Tuple directory in ROOT file', default=None)
parser.add_argument('-s','--sample', dest='sample', help='Location of the sampling tree', required=True)
parser.add_argument('-R','--remake', dest='remake', help='Remake sampling tree', default=False, action="store_true")
parser.add_argument('-D','--decay' , dest='decay' , help='Is it D0MuNu or JpsiMuNu', required=True, choices=['D0MuNu','JpsiMuNu'])
opts = parser.parse_args()

import sys
if opts.decay=='D0MuNu':
  hadron = "D0"
  had_daughter_1 = "Pi_1"
  had_daughter_1_mass = 139.57
  had_daughter_2 = "K_minus"
  had_daughter_2_mass = 493.68
elif opts.decay=='JpsiMuNu':
  hadron = "Jpsi"
  had_daughter_1 = "Mu_1"
  had_daughter_1_mass = 105.66
  had_daughter_2 = "Mu_2"
  had_daughter_2_mass = 105.66
else: sys.exit()

import ROOT as r
r.gInterpreter.Declare('#include "common/EventMixing_D0Mu.h"')

treepath = 'DecayTree'
if opts.dir: treepath = opts.dir+'/DecayTree'

inf  = r.TFile(opts.input)
tree = inf.Get(treepath)

# make subset tree for sampling SV dist
if opts.remake:
  tree.SetBranchStatus("*",0)
  tree.SetBranchStatus("B_plus_SV_X",1)
  tree.SetBranchStatus("B_plus_SV_Y",1)
  tree.SetBranchStatus("B_plus_SV_Z",1)
  tree.SetBranchStatus("B_plus_OWNPV_X",1)
  tree.SetBranchStatus("B_plus_OWNPV_Y",1)
  tree.SetBranchStatus("B_plus_OWNPV_Z",1)
  tree.SetBranchStatus("B_plus_M",1)
  sampf = r.TFile(opts.sample,'recreate')
  sampt = tree.CloneTree(0)

  for ev in range(tree.GetEntries()):
    tree.GetEntry(ev)
    if tree.B_plus_M>6285: sampt.Fill()

  sampt.AutoSave()
  sampf.Close()

sampf = r.TFile(opts.sample)
sampt = sampf.Get('DecayTree')

sampents = sampt.GetEntries()

# now make the event mixing tree
tree.SetBranchStatus("*",1)
evtmixf = r.TFile(opts.output,'recreate')
evtmixt = tree.CloneTree(0)

treeents = tree.GetEntries()
# make a sample of the equivalent size (probably overkill)
# going to loop every event in the tree and take:
#   - a random B+ FlightDistance from tree above
#   - B+ PV from ev
#   - D0    from ev+10
#   - mu    from ev+20
treeents = 100
for ev in range(treeents):
  # B+ plus PV
  tree.GetEntry(ev)
  B_plus_PV_X = tree.B_plus_OWNPV_X
  B_plus_PV_Y = tree.B_plus_OWNPV_Y
  B_plus_PV_Z = tree.B_plus_OWNPV_Z
  B_plus_PV_3vec = r.gen3Vec( B_plus_PV_X, B_plus_PV_Y, B_plus_PV_Z )
  # B+ FD
  rint = np.random.randint(sampents)
  sampt.GetEntry(rint)
  B_plus_flight_to_SV_X = sampt.B_plus_SV_X - sampt.B_plus_OWNPV_X
  B_plus_flight_to_SV_Y = sampt.B_plus_SV_Y - sampt.B_plus_OWNPV_Y
  B_plus_flight_to_SV_Z = sampt.B_plus_SV_Z - sampt.B_plus_OWNPV_Z
  B_plus_flight_to_SV_3vec = r.gen3Vec( B_plus_flight_to_SV_X, B_plus_flight_to_SV_Y, B_plus_flight_to_SV_Z )

  B_plus_SV_X = B_plus_PV_X + B_plus_flight_to_SV_X
  B_plus_SV_Y = B_plus_PV_Y + B_plus_flight_to_SV_Y
  B_plus_SV_Z = B_plus_PV_Z + B_plus_flight_to_SV_Z
  B_plus_SV_3vec = r.gen3Vec( B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z )

  # D0/Jpsi and daughters
  had_ent = ev+10 if ev+10<treeents else ev+10-treeents
  tree.GetEntry(had_ent)
  had_PX = getattr(tree,f"{hadron}_PX")
  had_PY = getattr(tree,f"{hadron}_PY")
  had_PZ = getattr(tree,f"{hadron}_PZ")
  had_M  = getattr(tree,f"{hadron}_M")
  had_4vec = r.gen4Vec( had_PX, had_PY, had_PZ, had_M )

  had_d1_PX = getattr(tree,f"{had_daughter_1}_PX")
  had_d1_PY = getattr(tree,f"{had_daughter_1}_PY")
  had_d1_PZ = getattr(tree,f"{had_daughter_1}_PZ")
  had_d1_M  = getattr(tree,f"{had_daughter_1}_M")
  had_d1_4vec = r.gen4Vec( had_d1_PX, had_d1_PY, had_d1_PZ, had_d1_M)

  had_d2_PX = getattr(tree,f"{had_daughter_2}_PX")
  had_d2_PY = getattr(tree,f"{had_daughter_2}_PY")
  had_d2_PZ = getattr(tree,f"{had_daughter_2}_PZ")
  had_d2_M  = getattr(tree,f"{had_daughter_2}_M")
  had_d2_4vec = r.gen4Vec( had_d2_PX, had_d2_PY, had_d2_PZ, had_d2_M)

  # Mu_plus
  mu_ent = ev+20 if ev+20<treeents else ev+20-treeents
  tree.GetEntry(mu_ent)
  mu_PX = getattr(tree,"Mu_plus_PX")
  mu_PY = getattr(tree,"Mu_plus_PY")
  mu_PZ = getattr(tree,"Mu_plus_PZ")
  mu_M  = getattr(tree,"Mu_plus_M")
  mu_4vec = r.gen4Vec( mu_PX, mu_PY, mu_PZ, mu_M )

  B_plus_4vec = had_4vec + mu_4vec

  tree.GetEntry(ev)

  setattr(evtmixt, "B_plus_OWNPV_X", B_plus_PV_X)
  setattr(evtmixt, "B_plus_OWNPV_Y", B_plus_PV_Y)
  setattr(evtmixf, "B_plus_OWNPV_Z", B_plus_PV_Z)

  setattr(evtmixt, "B_plus_SV_X", B_plus_SV_X)
  setattr(evtmixt, "B_plus_SV_Y", B_plus_SV_Y)
  setattr(evtmixt, "B_plus_SV_Z", B_plus_SV_Z)

  setattr(evtmixt, "B_plus_FD_OWNPV_X", B_plus_flight_to_SV_X)
  setattr(evtmixt, "B_plus_FD_OWNPV_Y", B_plus_flight_to_SV_Y)
  setattr(evtmixt, "B_plus_FD_OWNPV_Z", B_plus_flight_to_SV_Z)

  setattr(evtmixt, "B_plus_DIRA_OWNPV", r.ComputeDIRA( B_plus_flight_to_SV_3vec, B_plus_4vec ) )

  setattr(evtmixt, "B_plus_P" , B_plus_4vec.P() )
  setattr(evtmixt, "B_plus_PT", B_plus_4vec.PT() )
  setattr(evtmixt, "B_plus_PX", B_plus_4vec.Px() )
  setattr(evtmixt, "B_plus_PY", B_plus_4vec.Py() )
  setattr(evtmixt, "B_plus_PZ", B_plus_4vec.Pz() )
  setattr(evtmixt, "B_plus_M" , B_plus_4vec.M() )
  #setattr(evtmixt, "B_plus_ETA" , B_plus_4vec.Eta() ) # added by ang vars

  setattr(evtmixt, "B_plus_MCORR", r.ComputeMCORR( B_plus_SV_3vec, B_plus_PV_3vec, B_plus_4vec ) )
  setattr(evtmixt, "B_plus_MCORRERR", r.ComputeMCORRERR( B_plus_SV_3vec, B_plus_PV_3vec, B_plus_4vec ) )

  setattr(evtmixt,f"{hadron}_PX",had_PX)
  setattr(evtmixt,f"{hadron}_PY",had_PY)
  setattr(evtmixt,f"{hadron}_PZ",had_PZ)
  setattr(evtmixt,f"{hadron}_M" ,had_M )
  setattr(evtmixt,f"{hadron}_P" ,had_4vec.P() )
  setattr(evtmixt,f"{hadron}_PT" ,had_4vec.PT() )
  #setattr(evtmixt,f"{hadron}_ETA" ,had_4vec.ETA() ) # added by ang vars

  setattr(evtmixt,f"{had_daughter_1}_PX",had_d1_PX)
  setattr(evtmixt,f"{had_daughter_1}_PY",had_d1_PY)
  setattr(evtmixt,f"{had_daughter_1}_PZ",had_d1_PZ)
  setattr(evtmixt,f"{had_daughter_1}_M" ,had_d1_M )
  setattr(evtmixt,f"{had_daughter_1}_P" ,had_4vec.P() )
  setattr(evtmixt,f"{had_daughter_1}_PT" ,had_4vec.PT() )
  #setattr(evtmixt,f"{had_daughter_1}_ETA" ,had_4vec.ETA() ) # added by ang vars

  setattr(evtmixt,f"{had_daughter_2}_PX",had_d2_PX)
  setattr(evtmixt,f"{had_daughter_2}_PY",had_d2_PY)
  setattr(evtmixt,f"{had_daughter_2}_PZ",had_d2_PZ)
  setattr(evtmixt,f"{had_daughter_2}_M" ,had_d2_M )
  setattr(evtmixt,f"{had_daughter_2}_P" ,had_4vec.P() )
  setattr(evtmixt,f"{had_daughter_2}_PT" ,had_4vec.PT() )
  #setattr(evtmixt,f"{had_daughter_2}_ETA" ,had_4vec.ETA() ) # added by ang vars






