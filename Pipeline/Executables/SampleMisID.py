#!/usr/bin/env python

# --- SKIMMER SCRIPT TO IMPLEMENT TRIGGER SELECTION AND SKIM SELECTION BRANCHES ---

# PyROOT file to skim the data and MC Tuples to copy trees to root files with only selection
# variables written to file 

# __author__: Blaise Delaney
# __email__: blaise.delaney@cern.ch

#----------------------------------------------------------------------------
# *USE*: python ApplyPreselection STRIPPED_ROOTFILE FILTERED_ROOTFILE <flags>
#----------------------------------------------------------------------------

import ROOT as r
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse
import ROOT, uproot, pandas
ROOT.ROOT.EnableImplicitMT()
RDF = ROOT.ROOT.RDataFrame
import pandas as pd
import glob
from root_pandas import to_root


# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]

parser = argparse.ArgumentParser(description='Wildcards-enabled for Selection of pairs (dir, sel)')

parser.add_argument('-i','--input',    dest='input',  help='pair: Input ROOT file, input directory', required=True)
parser.add_argument('-o','--output',   dest='targetfile',  help='Output ROOT file', required=True)
parser.add_argument('-s','--selection'  , required=True, help='Selection file for signal requirement. MisID will be the inversion.')
opts = parser.parse_args()


def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return sel

# get selection string
assert( os.path.exists( opts.selection ) )
sel = {}
with open(opts.selection,'r') as f:
  sel = yaml.safe_load(f)


PIDcond = sel["MuonID"]
print('Selection in place:')
print('  ', sel["name"], ':', PIDcond )


# open input file 
infile_name = vars(parser.parse_args())['input']
targetfile_name = vars(parser.parse_args())['targetfile']

# take in sel_misID ROOT file only, post-efficiencies
rdf_in_misID = RDF(f"DecayTree", infile_name)
population_misID = rdf_in_misID.Count().GetValue()
#assert( population_misID > 0 )

print(f"Just a check: taking in the following file: {infile_name}")
print(f"..and writing to {targetfile_name}")
print(f"\nFull path checks: {infile_name}, DecayTree")
print(f"ORIGIN UNSAMPLED misID mode population: {population_misID}")



# ========================= sampling, if needed  ================================
# NOTE: want to sample only D0 mode, as only here have a FakeMuon stripping line
#prescale_factor = 0.02
prescale_factor = 1.0
print(f"CHECKING INFILE NAME: {infile_name}", infile_name.find("Jpsi"))
if infile_name.find("Jpsi") == -1: # no Jpsi in namespace 
    print("Executing sampling of FakeMuon-inherited events")
    raw_line_misID = rdf_in_misID.Filter(f"!{PIDcond} && Mu_plus_PIDmu>0") 
    failedPID_tmp_noprescale_df = pd.DataFrame(raw_line_misID.AsNumpy())
    PRESCALED_failedPID_df = failedPID_tmp_noprescale_df.sample(frac=prescale_factor).reset_index(drop=True)
    PRESCALED_failedPID_df["fakemuon_w"] = 1.0 # every event counts
    print(f"N events failing the cut {PIDcond} and with PIDmu>0 : {raw_line_misID.Count().GetValue()}")
    print(f"prescale factor: {prescale_factor} => writing to file {raw_line_misID.Count().GetValue()} x {prescale_factor} events from this stripping line")
    
    print("...Now adding the FakeMuon Line")
    fakemuon_df = pd.DataFrame(rdf_in_misID.Filter("Mu_plus_PIDmu<0").AsNumpy())
    fakemuon_df["fakemuon_w"] = 1/0.02 # effective stats: reciprocal of the prescale in FakeMuonLine 
    fakemuon_population = rdf_in_misID.Filter("Mu_plus_PIDmu<0").Count().GetValue()
    print(f"fakemuon population: {fakemuon_population}")
    TOTAL_MISID_DF = pd.concat([PRESCALED_failedPID_df, fakemuon_df], axis=0, ignore_index=True)
    print(TOTAL_MISID_DF.columns.values)
# if jpsi mode, just write to file
else:
    TOTAL_MISID_DF = pd.DataFrame(rdf_in_misID.AsNumpy())

# write misID
TOTAL_MISID_DF.to_root(targetfile_name, key=f"DecayTree", mode="a")
print(f"TOTAL MISID POPULATION POST-SAMPLING (NOTE: no sampling for Jpsi mode): ", RDF(f"DecayTree", targetfile_name).Count().GetValue())
print( '\n------ MISID TREE UPDATE COMPLETE -----\n' )
print( f"\n------ OUTPUT TREE: {targetfile_name} -----\n" )


