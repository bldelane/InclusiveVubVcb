import numpy as np
import uproot
from hep_ml import reweight
import pandas as pd
from hep_ml.metrics_utils import ks_2samp_weighted
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
from root_pandas import to_root
import pandas

from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse

ROOT.gROOT.SetBatch(True) 
ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);


def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string


# enable multithreading 
ROOT.ROOT.EnableImplicitMT() #---> maybe revisit this later?
ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/EventMixing_D0Mu.h"))
RDF = ROOT.ROOT.RDataFrame

parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-c','--combinatorial', dest='GBRfile',  help='Input GBR ROOT', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-d','--dirs', dest='dir', help='Tuple directory in ROOT file', required=True)
parser.add_argument('-s','--sel', dest='sel', help='Mass+angular cuts config file.', required=True)

# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]

infile_name = vars(parser.parse_args())['input'] # post raw mixed 
tree_directory = vars(parser.parse_args())['dir'] 
GBR_file_name = vars(parser.parse_args())['GBRfile'] 
target_file_name = vars(parser.parse_args())['output'] 
sel_file_name = vars(parser.parse_args())['sel']

print("Sanity check of input files")
print(f"--- Signal and misID: {infile_name}")
print(f"--- Combinatorial: {GBR_file_name}")
print(f"--- Directory: {tree_directory}")
print(f"--- Selection: {sel_file_name}")


# assign daughter branches from hadron in B decay
if infile_name.find("Jpsi") != -1:
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
else:
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57 
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"

mass_ang_cuts = ParseSelection(sel_file_name)

print(f"LOADING DATAFRAMES WITH CUTS: {mass_ang_cuts}")
SIGNAL_RDF = RDF(f"{tree_directory}Tuple/DecayTree", infile_name).Filter(f"{mass_ang_cuts}")
MISID_RDF = RDF(f"{tree_directory}_FakeMuonTuple/DecayTree", infile_name).Filter(f"{mass_ang_cuts}")
GBR_RDF = RDF("DecayTree", GBR_file_name).Filter(f"{mass_ang_cuts}")

# read in dict of relevant branches, while killing the rest
with open(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/{hadron}MuNu/FitVariables.yml", 'r') as stream:
    inbranches = load(stream, Loader=Loader)

fit_branches = list(inbranches.values())
print(fit_branches)
print("Writing to file only branches of interest")
pandas.DataFrame(SIGNAL_RDF.AsNumpy())[fit_branches].to_root(target_file_name, key=f"{tree_directory}Tuple/DecayTree", mode='a')
pandas.DataFrame(MISID_RDF.AsNumpy())[fit_branches].to_root(target_file_name, key=f"{tree_directory}_FakeMuonTuple/DecayTree", mode='a')
pandas.DataFrame(GBR_RDF.AsNumpy())[fit_branches].to_root(target_file_name, key=f"EventMixedTuple/DecayTree", mode='a')















