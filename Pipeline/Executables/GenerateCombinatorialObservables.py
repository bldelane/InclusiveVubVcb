import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy
from root_pandas import to_root
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("common/plotting/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys
import os
import argparse

# enable multithreading
ROOT.ROOT.EnableImplicitMT() #---> maybe revisit this later?
ROOT.gInterpreter.Declare('#include "{}"'.format("common/EventMixing_D0Mu.h"))
RDF = ROOT.ROOT.RDataFrame

parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-d','--dirs', dest='dir', help='Tuple directory in ROOT file', default=None)

# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]


# =================================== SYNTHETIC OBSERVABLES ====================================
infile_name = vars(parser.parse_args())['input'] # post raw mixed
tree_directory = vars(parser.parse_args())['dir']

# target: raw event mixed RDF, no need for parser for this, set in previous rule

evtMixed_df= RDF("DecayTree", infile_name)

# assign daughter branches from hadron in B decay
if infile_name.find("Jpsi") != -1:
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
else:
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"

# generate observables using saved B, hadron and muon banks post event mixing
CombObs_RDF = evtMixed_df.Define(f"CosXY_Mu_plus_{hadron}",              f"GenerateCosXYD0Mu({hadron}_PX, {hadron}_PY, Mu_plus_PX, Mu_plus_PY)") \
                         .Define(f"CosXYZ_Mu_plus_{had_daughter_2}",     f"GenerateCosXYZ_hMu({had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                         .Define(f"CosXYZ_Mu_plus_{had_daughter_1}",     f"GenerateCosXYZ_hMu({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                         .Define(f"B_plus_4Vec",                         f"Combine2Body({hadron}_PX, {hadron}_PY, {hadron}_PZ, {hadron}_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                         .Define(f"B_plus_PV_3Vec",                      f"gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                         .Define(f"B_plus_SV_3Vec",                      f"gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                         .Define(f"BflightVector",                       f"subtract3Vec( B_plus_SV_3Vec, B_plus_PV_3Vec)") \
                         .Define(f"B_plus_DIRA_OWNPV",                   f"ComputeDIRA(BflightVector, B_plus_4Vec)") \
                         .Define(f"Mu_plus_4Vec",                        f"gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                         .Define(f"{hadron}_4Vec",                       f"gen4Vec({hadron}_PX, {hadron}_PY, {hadron}_PZ, {hadron}_M)") \
                         .Define(f"B_plus_MCORR",                        f"ComputeMCORR(B_plus_SV_3Vec, B_plus_PV_3Vec,B_plus_4Vec)") \
                         .Define(f"B_plus_MCORRERR",                     f"ComputeMCORRERR(B_plus_SV_3Vec, B_plus_PV_3Vec, B_plus_4Vec)") \
                         .Define(f"B_plus_M",                            f"B_plus_4Vec.M()") \
                         .Define(f"FitVar_Mmiss2",                       f"ComputeMmiss2( BflightVector, B_plus_4Vec, {hadron}_4Vec, Mu_plus_4Vec)") \
                         .Define(f"FitVar_El",                           f"ComputeEl(B_plus_4Vec, BflightVector, Mu_plus_4Vec)") \
                         .Define(f"B_plus_FD_OWNPV",                     f"ComputeFD(BflightVector)") \
                         .Define(f"B_plus_P",                            f"B_plus_4Vec.P()") \
                         .Define(f"B_plus_Lab_P",                        f"Compute_B_EstMomentum( BflightVector, B_plus_4Vec, {hadron}_4Vec, Mu_plus_4Vec )") \
                         .Define(f"B_plus_PT",                           f"B_plus_4Vec.Pt()") \
                         .Define(f"B_plus_TAU",                          f"(B_plus_FD_OWNPV*6275.6/B_plus_Lab_P)") \
                         .Define(f"B_plus_ETA",                          f"B_plus_4Vec.Eta()") \
                         .Define(f"B_plus_SV_R",                         f"sqrt( (B_plus_SV_3Vec.X()*B_plus_SV_3Vec.X()) + (B_plus_SV_3Vec.Y()*B_plus_SV_3Vec.Y()) )") \
                         .Define(f"Mu_plus_PT",                          f"Mu_plus_4Vec.Pt()") \
                         .Define(f"Mu_plus_P",                           f"Mu_plus_4Vec.P()") \
                         .Define(f"Mu_plus_ETA",                         f"Mu_plus_4Vec.Eta()") \
                         .Define(f"{hadron}_PT",                         f"{hadron}_4Vec.Pt()") \
                         .Define(f"{hadron}_P",                          f"{hadron}_4Vec.P()") \
                         .Define(f"{hadron}_ETA",                        f"{hadron}_4Vec.Eta()") \
                         .Define(f"B_plus_FD_OWNPV_X",                   f"BflightVector.X()")\
                         .Define(f"B_plus_FD_OWNPV_Y",                   f"BflightVector.Y()")\
                         .Define(f"B_plus_FD_OWNPV_Z",                   f"BflightVector.Z()")\
                         .Define(f"Mu_plus_{had_daughter_1}_4Vec",       f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass})") \
                         .Define(f"Mu_plus_{had_daughter_2}_4Vec",       f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                         .Define(f"{had_daughter_1}_{had_daughter_2}_4Vec", f"Combine2Body({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass}, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                         .Define(f"Mu_plus_{had_daughter_1}_M",          f"Mu_plus_{had_daughter_1}_4Vec.M()") \
                         .Define(f"Mu_plus_{had_daughter_2}_M",          f"Mu_plus_{had_daughter_2}_4Vec.M()") \
                         .Define(f"{had_daughter_1}_{had_daughter_2}_M", f"{had_daughter_1}_{had_daughter_2}_4Vec.M()") \
                         .Define(f"B_plus_FIT_LTIME", f"((B_plus_M*B_plus_FD_OWNPV)/B_plus_P)/3e2") \



#++++++++ Write to target file with selection ++++++++++
# write to target file upating combinatorial and copying over the sig and misID

target_file_name = vars(parser.parse_args())['output']

# restrict to velo and visible mass range in stripping
CombObs_RDF.Filter("B_plus_M>2200 && B_plus_M<8000 && B_plus_SV_R<6").Snapshot("DecayTree", target_file_name)

print("=== SUCCESS: new observables added to raw event mixing file. ===")
