import ROOT 

from yaml import load, Loader
import yaml
from optparse import OptionParser

def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string

RDF = ROOT.ROOT.RDataFrame

raw_data = RDF("B2DMuNuX_D02KPiTuple/DecayTree", "/r01/lhcb/delaney/FIT/2016/mergedFIT_2016_MD_Bc2D0MuNuX.root")

print(raw_data.Filter("B_plus_M>5400").Count().GetValue())

# purpose: sample for uBoost for flatness
#trainingSample = raw_data.Filter("(K_minus_ID*Mu_plus_ID<0) && B_plus_M>5400 && (D0_M>1864.8-35 && D0_M<1864.8+35)", "Cuts for CF D0 Secondaries") #CF high mass band; reason: target real D0, with mass constraint

#trainingSample.Snapshot("B2DMuNuX_D02KPiTuple/DecayTree", "MVATS_noMCORRERRcuts.root")
