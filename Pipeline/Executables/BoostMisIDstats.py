#!/usr/bin/env python

# --- SKIMMER SCRIPT TO IMPLEMENT TRIGGER SELECTION AND SKIM SELECTION BRANCHES ---

# PyROOT file to skim the data and MC Tuples to copy trees to root files with only selection
# variables written to file 

# __author__: Blaise Delaney
# __email__: blaise.delaney@cern.ch

#----------------------------------------------------------------------------
# *USE*: python ApplyPreselection STRIPPED_ROOTFILE FILTERED_ROOTFILE <flags>
#----------------------------------------------------------------------------

import ROOT as r
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys 
import os
import argparse


# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]

parser = argparse.ArgumentParser(description='Wildcards-enabled for Selection of pairs (dir, sel)')

parser.add_argument('-i','--input',    dest='input',  help='pair: Input ROOT file, input directory', required=True)
parser.add_argument('-o','--output',   dest='targetfile',  help='Output ROOT file', required=True)
parser.add_argument('-m','--mode', dest='mode', help='Channel [D0MuNu/JpsiMuNu]', required=True)
parser.add_argument('-p', '--pars', type=pair, nargs='+', dest='pars', help='pair: (tuple, selection condition)', required=True)



def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return sel
print(vars(parser.parse_args())['pars'])
tup_dir = vars(parser.parse_args())['pars'][0][0] #{name}Tuple/DecayTree
sel_conf = vars(parser.parse_args())['pars'][0][1] #configfile for offline selection


## start keeping time
import time
start = time.time()

# key for yaml files
mode =vars(parser.parse_args())['mode']

# ugly but can catch a bug
PIDcond = ParseSelection(sel_conf)['Bachelor_Muon_PIDmu'] # PIDmu condition

# open input file 
infile_name = vars(parser.parse_args())['input']
infile = r.TFile.Open(infile_name)  


#----> All should inherit the branches from preselection

#signal directory check
decaytree = infile.Get(f"{tup_dir}Tuple/DecayTree") 
population = int(decaytree.GetEntries())
print(f"\nFull path checks: {infile_name}, {tup_dir}Tuple/DecayTree")
print(f"ORIGIN signal mode populationpopulation: {population}")

targetfile_name = vars(parser.parse_args())['targetfile']
targetfile = r.TFile.Open( targetfile_name, "recreate") 


# ---- signal
signal_dir = targetfile.mkdir(f"{tup_dir}Tuple")
signal_dir.cd()

print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print(f"Saving events passing cut {PIDcond}")

signal_tree = decaytree.CopyTree(PIDcond)
signal_tree.Write()
print( f"===> entries written to {targetfile_name}/{tup_dir}Tuple:", signal_tree.GetEntries() )
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


# ---- misID
fakemu_dir = targetfile.mkdir(f"{tup_dir}_FakeMuonTuple")
fakemu_dir.cd()

print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print(f"Saving events passing cut !({PIDcond})")

misID_tree = decaytree.CopyTree(f"!({PIDcond})")
print( f"===> Entries written to {targetfile_name}/{tup_dir}_FakeMuonTuple:", misID_tree.GetEntries() )
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

# now add what did *not* survive to pre-existent misID, if any
misIDlist = r.TList()
print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("...Now adding ORIGIN and !PIDMu trees...")
misIDlist.Add(misID_tree)

# ---- FakeMuon line 
try: 
    fakemu_decaytree = infile.Get(f"{tup_dir}_FakeMuonTuple/DecayTree")
    fakemu_population = int(fakemu_decaytree.GetEntries())
    print(f"\nFull path checks: {infile_name}, {tup_dir}_FakeMuonTuple/DecayTree")
    print(f"ORIGIN FAKEMUON population: {fakemu_population}")
    misIDlist.Add(fakemu_decaytree)
except:
    print(f"No pre-existent misID directory {tup_dir}Tuple")
    pass

mergedFakeMuonTree = r.TTree.MergeTrees(misIDlist)
mergedFakeMuonTree.SetName("DecayTree")

mergedFakeMuonTree.Write()

print( f"===> Entries written to {targetfile_name}/B2DMuNuX_D02KPi_FakeMuonTuple:", mergedFakeMuonTree.GetEntries() )
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")



targetfile.Write()
targetfile.Close()



end = time.time()
print( 'Compilation time in seconds: ', (end - start) )
print( '\n           ------ MISID TREE UPDATE COMPLETE -----\n' )
