import numpy as np
import uproot
from hep_ml import reweight
import pandas as pd
from hep_ml.metrics_utils import ks_2samp_weighted
from sklearn.model_selection import train_test_split

RemotePath = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/"

def LoadLocalTree(infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.open(infile)[f"{tupledir}DecayTree"]
    if maxentries is not None: 
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df 


GBRvars = ["comb_Mu_plus_P", "comb_Mu_plus_PT", "comb_D0_P", "comb_D0_PT"]

# upper B data 
target_df = LoadLocalTree("Pre_Combinatorial.root", "", branch_list=GBRvars.append("comb_B_plus_M"), maxentries=1e5)
print(f"Target entries:{target_df.shape[0]} - pre-mass cut")

#target_df = target_df.query("comb_B_plus_M>6280")
target_df = target_df.query("comb_B_plus_M>6280 and comb_B_plus_DIRA>0.999 and CosXY_Mu_plus_D0<0.999")
print(f"Target entries:{target_df.shape[0]} - post-mass cut")

origin_df = LoadLocalTree("ReMixed.root", "", branch_list=["*"], maxentries=1e5)
print(f"Origin entries:{origin_df.shape[0]} - pre-mass cut")

#origin_df = origin_df.query("comb_B_plus_M>6280")
origin_df = target_df.query("comb_B_plus_M>6280 and comb_B_plus_DIRA>0.999 and CosXY_Mu_plus_D0<0.999")
print(f"Origin entries:{origin_df.shape[0]} - post-mass cut")

#GBRvars = ["Mu_plus_PX", "Mu_plus_PY", "Mu_plus_PZ", "D0_PX", "D0_PY", "D0_PZ"]
GBRvars = ["comb_Mu_plus_P", "comb_Mu_plus_PT", "comb_D0_P", "comb_D0_PT"]

print("loading done")    
#set reweighter with k-folding for unbiased evaluation
reweighter_base = reweight.GBReweighter(n_estimators=150, 
                                        learning_rate=0.1, max_depth=5, min_samples_leaf=1000, )
                                        #gb_args={'subsample': 0.4})

reweighter = reweight.FoldingReweighter(reweighter_base, n_folds=2)
reweighter.fit(original=origin_df[GBRvars],
               target=target_df[GBRvars] ) 

# extract the final weights; 
folding_weights = reweighter.predict_weights(
    original= origin_df[GBRvars],
                            )
print("MVA trained")

origin_df["GBR_w"] = folding_weights


from root_pandas import to_root
origin_df.to_root("GBR_ReMixed.root", key='DecayTree')

