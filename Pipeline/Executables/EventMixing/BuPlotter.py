import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml

ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

RDF = ROOT.ROOT.RDataFrame
eospath = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/Blaise/Bc2D0MuNuX_FIT_COMPONENTS/DATA/"

#ORIGINAL_DATA_RDF = RDF("DecayTree", eospath+"SIG_OffSel_NewVars_noVisMCuts.root").Filter("(K_minus_ID*Mu_plus_ID<0)")
#EVTMIXED_RDF = RDF("DecayTree", "MC16_Bu2D0MuNu.root")
ORIGINAL_DATA_RDF = RDF("DecayTree", "invTruth_MC16_Bc2D0MuNuX.root")
EVTMIXED_RDF = RDF("DecayTree", "Combinatorial_noVisMCuts.root")


def Plot(var, minvar, maxvar, binsvar, xlabelvar, pdfvar):
  
    
    c1 = TCanvas()
    synthetic_shape_sampled= EVTMIXED_RDF.Histo1D(("h1s", "Synthetic Var mB=6279", binsvar, minvar, maxvar), "comb_"+var, "GBR_w")
    original_shape = ORIGINAL_DATA_RDF.Histo1D(("h2", "Original Var mB=6279", binsvar, minvar, maxvar), var)
    
    
    synthetic_shape_sampled.SetLineColor( kTeal-1)
    synthetic_shape_sampled.SetMarkerColor( kTeal-1)
    synthetic_shape_sampled.SetFillColorAlpha( kTeal-1, .5)
    
    
    original_shape.SetLineColor( kAzure+1)
    original_shape.SetMarkerColor( kAzure+1)
    original_shape.SetFillColorAlpha( kAzure+1, .5)
    
    
    synthetic_shape_sampled.SetLineWidth( 2 )
    original_shape.SetLineWidth( 2 )
    
    original_shape.GetYaxis().SetTitle("Arbitrary Units")
    original_shape.GetXaxis().SetTitle(xlabelvar)
    
    synthetic_shape_sampled.Scale(1./synthetic_shape_sampled.Integral())
    original_shape.Scale(1./original_shape.Integral())
   
    
    original_shape.Draw("EP")
    synthetic_shape_sampled.Draw("EPSAME")
    
    
    #original_shape.GetYaxis().SetRangeUser(0.0, 0.2) 
    
    legend=TLegend(0.55,0.7,0.7,0.9)
    legend.SetTextFont(132)
    legend.SetTextSize(0.05)
    legend.AddEntry("h2","MC #it{B}_{c}#rightarrow D^{0}#mu#nuX 2016, B_BKGCAT=110","l")
    legend.AddEntry("h1s","EvtMixer+GBR","lf")
    legend.Draw()
    
    c1.Print("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PLOTS/BuShapes/"+pdfvar+".pdf")


# Read YAML file
with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/BuPlotPars.yml", 'r') as stream:
        to_plot = yaml.safe_load(stream)

for branch in to_plot.keys():
    
    print("===> Plotting ", branch)
    Plot(branch, **to_plot[branch])
    


