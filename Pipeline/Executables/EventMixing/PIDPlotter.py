import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml

#ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/lhcbStyle.C")
#ROOT.gStyle.SetTitleXSize(0.07);
#ROOT.gStyle.SetTitleYSize(0.07);
#ROOT.gStyle.SetTitleXOffset(0.95);
#ROOT.gStyle.SetTitleYOffset(1.1);
#ROOT.gStyle.SetPadLeftMargin(0.15);
#ROOT.gStyle.SetPadRightMargin(0.08);
#ROOT.gStyle.SetOptStat(0);
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

RDF = ROOT.ROOT.RDataFrame

ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.h"))

df = RDF("B2DMuNuX_D02KPiTuple/DecayTree", "/r01/lhcb/delaney/FIT/2016/OFFLINESELECTED_noVisMcuts.root")
df = df.Define("KPi_4vec", "Combine2Body(K_minus_PX, K_minus_PY, K_minus_PZ, KM, Pi_1_PX, Pi_1_PY, Pi_1_PZ, mPi") \
       .Define("KMu_4vec", "Combine2Body(K_minus_PX, K_minus_PY, K_minus_PZ, KM, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, mPi) \ 
       .Define("PiMu_4vec", "Combine2Body(Pi_1_PX, Pi_1_PY, Pi_1_PZ, KM, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, mPi) \ 

def Plot(var, ybins, ymin, ymax, ylabelvar, pdfvar):
  
    
    c1 = TCanvas()
    hist= df.Filter("K_minus_ID*Mu_plus_ID>0").Histo2D(("h", "Particle ID vs D0 mass", 35, 1790, 1940, ybins, ymin, ymax), "D0_M", var)
    
    hist.GetYaxis().SetTitle(ylabelvar)
    hist.GetXaxis().SetTitle("m(#it{K}#pi) [MeV/c^{2}]")
    
    
    hist.Draw("COLZ")
    
    c1.Print("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PID/"+pdfvar+".pdf")


# Read YAML file
with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PIDPars.yml", 'r') as stream:
        to_plot = yaml.safe_load(stream)

for branch in to_plot.keys():
    
    print("===> Plotting ", branch)
    Plot(branch, **to_plot[branch])
    


