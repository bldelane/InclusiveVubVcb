import ROOT
    
from ROOT import TMath, TGraph, TCanvas, TColor, TFile
from ROOT import kBlue, kGreen, kRed, kRed

import uproot, pandas
from root_pandas import to_root
import numpy as np
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?





# ==================================== EVENT MIXING SECTION ====================================
# path to remote files on eos

def LoadRemoteTree(RemotePath, infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.rootio.xrootd(RemotePath+infile)[f"{tupledir}DecayTree"]
    if maxentries is not None: 
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df

def LoadLocalTree(infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.open(infile)[f"{tupledir}DecayTree"]
    if maxentries is not None: 
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df

OriginalDF = LoadLocalTree("Pre_Combinatorial.root","", ["*"])  
print(f"Pre-mass cut: Pre_Combinatorial #entries: {OriginalDF.shape[0]}")

OriginalDF = OriginalDF.query("comb_B_plus_M>6280")
print(f"Pre-mass cut: Pre_Combinatorial #entries: {OriginalDF.shape[0]}")



''' prepocess for observables recalculation '''
OriginalDF["comb_Mu_plus_PX"] = OriginalDF["Mu_plus_PX"]
OriginalDF["comb_Mu_plus_PY"] = OriginalDF["Mu_plus_PY"]
OriginalDF["comb_Mu_plus_PZ"] = OriginalDF["Mu_plus_PZ"]

OriginalDF["comb_D0_PX"] = OriginalDF["D0_PX"]
OriginalDF["comb_D0_PY"] = OriginalDF["D0_PY"]
OriginalDF["comb_D0_PZ"] = OriginalDF["D0_PZ"]

OriginalDF["comb_D0_M"] = OriginalDF["D0_M"] # OK, now have all observables that are needed

# branches of interest
brOI = ["comb_Mu_plus_PX", "comb_Mu_plus_PY", "comb_Mu_plus_PZ",
        "comb_D0_PX", "comb_D0_PY", "comb_D0_PZ",
        "comb_D0_M", 
        "B_plus_OWNPV_X", "B_plus_OWNPV_Y", "B_plus_OWNPV_Z", 
        "B_plus_SV_X", "B_plus_SV_Y", "B_plus_SV_Z"]

OriginalDF = OriginalDF[brOI]

for key in OriginalDF.keys():
    if key.startswith("comb_"):
        newkey = key.replace("comb_", "")
        OriginalDF[newkey] = OriginalDF[key]
        del OriginalDF[key]

print(OriginalDF.keys())
print(f"====>DATAFRAME LOADED: {OriginalDF.shape[0]} events.")

# Implement Event Mixing and reset entry order for a new concat 
# to avoid preserving the event order in the original df
B_Observables = [col for col in OriginalDF if col.startswith('B_plus')]
D0_Observables = [col for col in OriginalDF if col.startswith('D0') or col.startswith('K_minus') or col.startswith('Pi_1') ] #able to tag DCS/CF and CosXYZ_MuH
Muon_Observables = [col for col in OriginalDF if col.startswith('Mu_plus') ] 

BEvents = OriginalDF[B_Observables].sample(frac=1).reset_index(drop=True)
D0Events = OriginalDF[D0_Observables].sample(frac=1).reset_index(drop=True)
MuonEvents = OriginalDF[Muon_Observables].sample(frac=1).reset_index(drop=True)


# merge columns, regardless of indexEventMixedDF = pandas.concat([MuonEvents, D0Events, BEvents], axis=1)
EventMixedDF = pandas.concat([MuonEvents, D0Events, BEvents], axis=1)

# write to ROOT filEventMixedDF.to_root("EvtMixed_noObservables.root", key="B2DMuNuX_D02KPiTuple/DecayTree")
EventMixedDF.to_root("PreReMixed.root", key="DecayTree")
print(f"Post mixing: PreReMixed #entries: {EventMixedDF.shape[0]}")
# ==============================================================================================

# load into RDF a data file on eos
ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.h"))
RDF = ROOT.ROOT.RDataFrame
evtMixed_df= RDF("DecayTree", "PreReMixed.root")



CombinatoricShapesDF = evtMixed_df.Define("B_comb_4Vec", "Combine2Body(D0_PX, D0_PY, D0_PZ, D0_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                                  .Define("Muon_comb_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                                  .Define("D0_comb_4Vec", "gen4Vec(D0_PX, D0_PY, D0_PZ, D0_M)") \
                                    .Define("B_comb_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                                    .Define("B_comb_SV_3Vec_Sampling", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                                        .Define("BflightVector_sampled", "subtract3Vec( B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec)") \
                                        .Define("comb_B_plus_MCORR", "ComputeMCORR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec,B_comb_4Vec)") \
                                        .Define("comb_B_plus_MCORRERR", "ComputeMCORRERR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec, B_comb_4Vec)") \
                                        .Define("comb_B_plus_M", "B_comb_4Vec.M()") \
                                        .Define("comb_FitVar_Mmiss2", "ComputeMmiss2( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec)") \
                                        .Define("comb_FitVar_El",    "ComputeEl(B_comb_4Vec, BflightVector_sampled, Muon_comb_4Vec)") \
                                        .Define("comb_B_plus_DIRA", "ComputeDIRA(BflightVector_sampled, B_comb_4Vec)") \
                                        .Define("comb_B_plus_FD_OWNPV", "ComputeFD(BflightVector_sampled)") \
                                        .Define("comb_B_plus_P", "B_comb_4Vec.P()") \
                                        .Define("B_plus_CoM_P", "Compute_B_EstMomentum( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec )") \
                                        .Define("comb_B_plus_PT", "B_comb_4Vec.Pt()") \
                                        .Define("B_plus_TAU", "(comb_B_plus_FD_OWNPV*6275.6/B_plus_CoM_P)") \
                                        .Define("comb_B_plus_ETA", "B_comb_4Vec.Eta()") \
                                        .Define("CosXY_Mu_plus_D0",          "GenerateCosXYD0Mu(D0_PX, D0_PY, Mu_plus_PX, Mu_plus_PY)") \
                                        .Define("comb_Mu_plus_PT", "Muon_comb_4Vec.Pt()") \
                                        .Define("comb_Mu_plus_P", "Muon_comb_4Vec.P()") \
                                        .Define("comb_Mu_plus_ETA", "Muon_comb_4Vec.Eta()") \
                                        .Define("comb_D0_PT", "D0_comb_4Vec.Pt()") \
                                        .Define("comb_D0_P", "D0_comb_4Vec.P()") \
                                        .Define("comb_D0_ETA", "D0_comb_4Vec.Eta()") \

print(f"Post mixing: PreReMixed #entries: {CombinatoricShapesDF.Count().GetValue()}")
selComb_df  = CombinatoricShapesDF.Filter("comb_B_plus_M>2200 && comb_B_plus_M<8000") # match stripping and selection to recover shapes
# what to apply weights on and save to file; match offline + stripping visM cuts 
fullRange_GBR_df = pandas.DataFrame(selComb_df.AsNumpy())
print(f"====>ReMixed Generated: {fullRange_GBR_df.shape[0]} events.")

from root_pandas import to_root
fullRange_GBR_df.to_root('ReMixed.root', key='DecayTree')

