import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy 
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?


ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.h"))
EventMixed_file = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EvtMixed_noObservables_JpsiMuNu.root" 


# =================================== SYNTHETIC OBSERVABLES ====================================

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
evtMixed_df= RDF("B2JpsiMuNuXTuple/DecayTree", EventMixed_file)

# ==== histogram of 2D (R,Z) combinatorial SV info
#f = ROOT.TFile("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/SVRZ_forSampling.root")
#ROOT.gInterpreter.ProcessLine("auto histo = h;")
SetMomentumBudget_RDF = evtMixed_df.Define("CosXY_Mu_plus_Jpsi",          "GenerateCosXYD0Mu(Jpsi_PX, Jpsi_PY, Mu_plus_PX, Mu_plus_PY)") \
                                   .Define("CosXYZ_Mu_plus_Mu_1", "GenerateCosXYZ_hMu(Mu_1_PX, Mu_1_PY, Mu_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                   .Define("CosXYZ_Mu_plus_Mu_2",          "GenerateCosXYZ_hMu(Mu_2_PX, Mu_2_PY, Mu_2_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \




CombinatoricShapesDF = SetMomentumBudget_RDF.Define("B_comb_4Vec", "Combine2Body(Jpsi_PX, Jpsi_PY, Jpsi_PZ, Jpsi_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                                  .Define("B_comb_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                                  .Define("B_comb_SV_3Vec_Sampling", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                                  .Define("BflightVector_sampled", "subtract3Vec( B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec)") \
                                  .Define("comb_B_plus_DIRA", "ComputeDIRA(BflightVector_sampled, B_comb_4Vec)") \
                                  .Define("Muon_comb_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                                  .Define("Jpsi_comb_4Vec", "gen4Vec(Jpsi_PX, Jpsi_PY, Jpsi_PZ, Jpsi_M)") \
                                        .Define("comb_B_plus_MCORR", "ComputeMCORR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec,B_comb_4Vec)") \
                                        .Define("comb_B_plus_MCORRERR", "ComputeMCORRERR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec, B_comb_4Vec)") \
                                        .Define("comb_B_plus_M", "B_comb_4Vec.M()") \
                                        .Define("comb_FitVar_Mmiss2", "ComputeMmiss2( BflightVector_sampled, B_comb_4Vec, Jpsi_comb_4Vec, Muon_comb_4Vec)") \
                                        .Define("comb_FitVar_El",    "ComputeEl(B_comb_4Vec, BflightVector_sampled, Muon_comb_4Vec)") \
                                        .Define("comb_B_plus_FD_OWNPV", "ComputeFD(BflightVector_sampled)") \
                                        .Define("comb_B_plus_P", "B_comb_4Vec.P()") \
                                        .Define("B_plus_CoM_P", "Compute_B_EstMomentum( BflightVector_sampled, B_comb_4Vec, Jpsi_comb_4Vec, Muon_comb_4Vec )") \
                                        .Define("comb_B_plus_PT", "B_comb_4Vec.Pt()") \
                                        .Define("B_plus_TAU", "(comb_B_plus_FD_OWNPV*6275.6/B_plus_CoM_P)") \
                                        .Define("comb_B_plus_ETA", "B_comb_4Vec.Eta()") \
                                        .Define("comb_B_plus_SV_R",       "sqrt( (B_comb_SV_3Vec_Sampling.X()*B_comb_SV_3Vec_Sampling.X()) + (B_comb_SV_3Vec_Sampling.Y()*B_comb_SV_3Vec_Sampling.Y()) )") \
                                        .Define("comb_B_plus_SV_X",       "B_comb_SV_3Vec_Sampling.X()") \
                                        .Define("comb_B_plus_SV_Y",       "B_comb_SV_3Vec_Sampling.Y()") \
                                        .Define("comb_B_plus_SV_Z",       "B_comb_SV_3Vec_Sampling.Z()") \
                                        .Define("comb_Mu_plus_PT", "Muon_comb_4Vec.Pt()") \
                                        .Define("comb_Mu_plus_P", "Muon_comb_4Vec.P()") \
                                        .Define("comb_Mu_plus_ETA", "Muon_comb_4Vec.Eta()") \
                                        .Define("comb_Jpsi_PT", "Jpsi_comb_4Vec.Pt()") \
                                        .Define("comb_Jpsi_P", "Jpsi_comb_4Vec.P()") \
                                        .Define("comb_Jpsi_ETA", "Jpsi_comb_4Vec.Eta()") \
                                        .Define("comb_B_plus_FD_OWNPV_X", "BflightVector_sampled.X()")\
                                        .Define("comb_B_plus_FD_OWNPV_Y", "BflightVector_sampled.Y()")\
                                        .Define("comb_B_plus_FD_OWNPV_Z", "BflightVector_sampled.Z()")
                                        #.Define("B_comb_SV_3Vec_Sampling", "sampleRZ(histo, B_plus_OWNPV_X,B_plus_OWNPV_Y,B_plus_OWNPV_Z)") \
                                        #.Define("CosXY_Mu_plus_Jpsi",          "GenerateCosXYJpsiMu(Jpsi_PX, Jpsi_PY, Mu_plus_PX, Mu_plus_PY)") \
                                        #.Define("CosXYZ_Mu_plus_Mu_2",          "GenerateCosXYZ_hMu(Mu_2_PX, Mu_2_PY, Mu_2_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                        #.Define("CosXYZ_Mu_plus_Mu_1",          "GenerateCosXYZ_hMu(Mu_1_PX, Mu_1_PY, Mu_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \


selComb_df  = CombinatoricShapesDF.Filter("comb_B_plus_M>2200 && comb_B_plus_M<8000") # match stripping and selection to recover shapes
selComb_df.Snapshot("DecayTree", f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EvtMix_Jpsi/Pre_Combinatorial_JpsiMuNu.root")


