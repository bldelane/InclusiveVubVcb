import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

DATAang_cut_string = " CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999 && CosXY_Mu_plus_D0>-0.4 && B_plus_M>3500 && B_plus_M<6280"
EVTMIXang_cut_string = " CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999 && CosXY_Mu_plus_D0>-0.4 && comb_B_plus_M>3500 && comb_B_plus_M<6280"

RDF = ROOT.ROOT.RDataFrame
DATA_RDF= RDF("DecayTree", "SIG_OffSel_NewVars_noVisMCuts.root").Filter(DATAang_cut_string)
DATA_RDF.Snapshot("DecayTree", "TUPLES/SIG.root")

RDF = ROOT.ROOT.RDataFrame
MISID_RDF= RDF("DecayTree", "MISID_OffSel_NewVars_noVisMCuts.root").Filter(DATAang_cut_string)
MISID_RDF.Snapshot("DecayTree", "TUPLES/MISID.root")

RDF = ROOT.ROOT.RDataFrame
EVTMIX_RDF= RDF("DecayTree", "DaughterKinematics/daughter_PT_ETA___FitRange_Combinatorial_noVisMCuts.root").Filter(EVTMIXang_cut_string)
EVTMIX_RDF.Snapshot("DecayTree", "TUPLES/Combinatorial.root")

RDF = ROOT.ROOT.RDataFrame
MC_D0_RDF= RDF("B2DMuNuX_D02KPiTuple/DecayTree", "/r01/lhcb/delaney/FIT/MC/2016/fit_MC_Bc2D0gMuNu_2016.root").Filter(DATAang_cut_string)
MC_D0_RDF.Snapshot("B2DMuNuX_D02KPiTuple/DecayTree", "TUPLES/fit_MC_Bc2D0gMuNu_2016.root")

RDF = ROOT.ROOT.RDataFrame
MC_D0g_RDF= RDF("B2DMuNuX_D02KPiTuple/DecayTree", "/r01/lhcb/delaney/FIT/MC/2016/fit_MC_Bc2D0MuNu_2016.root").Filter(DATAang_cut_string)
MC_D0g_RDF.Snapshot("B2DMuNuX_D02KPiTuple/DecayTree", "TUPLES/fit_MC_Bc2D0MuNu_2016.root")

RDF = ROOT.ROOT.RDataFrame
MC_D0pi0_RDF= RDF("B2DMuNuX_D02KPiTuple/DecayTree", "/r01/lhcb/delaney/FIT/MC/2016/fit_MC_Bc2D0pi0MuNu_2016.root").Filter(DATAang_cut_string)
MC_D0pi0_RDF.Snapshot("B2DMuNuX_D02KPiTuple/DecayTree", "TUPLES/fit_MC_Bc2D0pi0MuNu_2016.root")

