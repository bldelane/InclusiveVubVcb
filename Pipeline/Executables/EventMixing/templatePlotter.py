import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml

ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

RDF = ROOT.ROOT.RDataFrame


EvtMixed_df = RDF("DecayTree", "Pre_Combinatorial.root")
ReMixed_df = RDF("DecayTree", "ReMixed.root")
GBR_ReMixed_df = RDF("DecayTree", "GBR_ReMixed.root")

def Plot(var, minvar, maxvar, binsvar, xlabelvar, pdfvar):
  
    
    c1 = TCanvas()

    
    EvtMix_shape = EvtMixed_df.Filter("comb_B_plus_M>6280").Histo1D(("hMix", "Synthetic Var mB=6279", binsvar, minvar, maxvar), "comb_"+var)
    ReMix_shape =  ReMixed_df.Filter("comb_B_plus_M>6280").Histo1D(("hReMix", "Synthetic Var mB=6279", binsvar, minvar, maxvar),"comb_"+var)
    GBR_ReMix_shape =  GBR_ReMixed_df.Filter("comb_B_plus_M>6280").Histo1D(("hGBRReMix", "Synthetic Var mB=6279", binsvar, minvar, maxvar), "comb_"+var, "GBR_w")
    
    
    EvtMix_shape.SetLineColor(kBlack)
    EvtMix_shape.SetMarkerColor(kBlack)
    
    ReMix_shape.SetLineColor(kRed)
    ReMix_shape.SetMarkerColor(kRed)
    
    GBR_ReMix_shape.SetLineColor(kAzure)
    GBR_ReMix_shape.SetMarkerColor(kAzure)
    
    EvtMix_shape.Scale(1./EvtMix_shape.Integral())
    ReMix_shape.Scale(1./ReMix_shape.Integral())
    GBR_ReMix_shape.Scale(1./GBR_ReMix_shape.Integral())
   
    EvtMix_shape.Draw("EP")
    ReMix_shape.Draw("EPSAME")
    GBR_ReMix_shape.Draw("EPSAME")
    
    EvtMix_shape.GetYaxis().SetTitle("Arbitrary Units")
    EvtMix_shape.GetXaxis().SetTitle(xlabelvar)
    

    
    legend=TLegend(0.6,0.7,0.8,0.9)
    legend.SetTextFont(132)
    legend.SetTextSize(0.03)
    
    
    legend.AddEntry("hMix","EvtMix #it{m_{B}}>6280 MeV/c^{2}","lep")
    legend.AddEntry("hReMix","ReMix #it{m_{B}}>6280 MeV/c^{2}","lep")
    legend.AddEntry("hGBRReMix","ReMix+GBR #it{m_{B}}>6280 MeV/c^{2}","lep")
    
    legend.Draw()
    
    c1.Print("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PLOTS/"+pdfvar+".pdf")


# Read YAML file
with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PlotPars.yml", 'r') as stream:
        to_plot = yaml.safe_load(stream)

for branch in to_plot.keys():
    
    print("===> Plotting ", branch)
    Plot(branch, **to_plot[branch])
    


