import ROOT, pandas
    
from ROOT import TMath, TGraph, TCanvas, TColor, TFile
from ROOT import kBlue, kGreen, kRed, kRed

import uproot, pandas
from root_pandas import to_root
import numpy as np
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?
RDF = ROOT.ROOT.RDataFrame


#ROOT.gInterpreter.Declare('#include "{}"'.format("EventMixing_D0Mu.h"))
eospath = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/"

'''
# ==================================== 2D h(R,Z) for sammpling ====================================

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
original_df = RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"{eospath}Blaise/Bc2D0MuNuX_FIT_COMPONENTS/DATA/OFFLINESELECTED_noVisMcuts.root")

treename="DecayTree"
treefile = "SVRZ_forSampling.root"

df_augmented_4vecs = original_df.Filter("(B_plus_M>6280 && (K_minus_ID*Mu_plus_ID>0)) || (B_plus_M>5400 && (K_minus_ID*Mu_plus_ID<0) )") \
                                .Define("comb_B_FlightDir_R", "sqrt( (B_plus_SV_X - B_plus_OWNPV_X)*(B_plus_SV_X - B_plus_OWNPV_X) + (B_plus_SV_Y - B_plus_OWNPV_Y)*(B_plus_SV_Y - B_plus_OWNPV_Y) )") \
                                .Define("comb_B_FlightDir_Z", "(B_plus_SV_Z - B_plus_OWNPV_Z)")

minR = df_augmented_4vecs.Min("comb_B_FlightDir_R").GetValue()
maxR = df_augmented_4vecs.Max("comb_B_FlightDir_R").GetValue()
minZ = df_augmented_4vecs.Min("comb_B_FlightDir_Z").GetValue()
maxZ = df_augmented_4vecs.Max("comb_B_FlightDir_Z").GetValue()

c = TCanvas()
SV_RZ = df_augmented_4vecs.Histo2D(("h", "SV 2D hist(R,Z)",40, 0, 1, 40, 0, 20), "comb_B_FlightDir_R", "comb_B_FlightDir_Z")
SV_RZ.Draw("COLZ")
SV_RZ.GetXaxis().SetTitle("#it{B} Flight Distance #it{R}")
SV_RZ.GetYaxis().SetTitle("#it{B} Flight Distance #it{Z}")
c.Print("SVRZ.pdf")
hist2dfile = TFile(treefile, "RECREATE")
SV_RZ.Write()
hist2dfile.Write()
hist2dfile.Close()
# ==============================================================================================
'''


# ==================================== EVENT MIXING SECTION ====================================
# path to remote files on eos

def LoadRemoteTree(RemotePath, infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.rootio.xrootd(RemotePath+infile)[f"{tupledir}DecayTree"]
    if maxentries is not None: 
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df

def LoadLocalTree(infile, tupledir, branch_list, maxentries=None):
    tmp_tree = uproot.open(infile)[f"{tupledir}DecayTree"]
    if maxentries is not None: 
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False, entrystop=maxentries)
    else:
        tmp_df = tmp_tree.pandas.df(branch_list, flatten=False)
    return tmp_df

OriginalDF = LoadLocalTree("/r01/lhcb/delaney/FIT/JpsiMuNu/2016/OffSel_Bc2JpsiMuNu_noVisMcuts.root","B2JpsiMuNuXTuple/",  
                            branch_list=["B_plus_SV_X", 
                                       "B_plus_SV_Y",
                                       "B_plus_SV_Z",
                                       "B_plus_OWNPV_X", 
                                       "B_plus_OWNPV_Y",
                                       "B_plus_OWNPV_Z",
                                       "Jpsi_PX", "Jpsi_PY", "Jpsi_PZ", "Jpsi_M",
                                       "Mu_plus_PX", "Mu_plus_PY", "Mu_plus_PZ", 
                                       "Mu_1_PX", "Mu_1_PY", "Mu_1_PZ", 
                                       "Mu_2_PX", "Mu_2_PY", "Mu_2_PZ", 
                                       "B_plus_M"],
                            )

print(f"====>DATAFRAME LOADED: {OriginalDF.shape[0]} events.")

# Implement Event Mixing and reset entry order for a new concat 
# to avoid preserving the event order in the original df
B_Observables = [col for col in OriginalDF if col.startswith('B_plus')]
Jpsi_Observables = [col for col in OriginalDF if col.startswith('Jpsi') or col.startswith('Mu_1') or col.startswith('Mu_2') ] #Jpsi->Mu Mu
bachMuon_Observables = [col for col in OriginalDF if col.startswith('Mu_plus') ] 

for i in range(2000):
    BEvents = OriginalDF[B_Observables].sample(frac=1).reset_index(drop=True)
    JpsiEvents = OriginalDF[Jpsi_Observables].sample(frac=1).reset_index(drop=True)
    bachMuonEvents = OriginalDF[bachMuon_Observables].sample(frac=1).reset_index(drop=True)
    
    
    # merge columns, regardless of indexEventMixedDF = pandas.concat([MuonEvents, D0Events, BEvents], axis=1)
    EventMixedDF = pandas.concat([bachMuonEvents, JpsiEvents, BEvents], axis=1)
    
    
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # overwrite SV; implement vectorization;
    # first, create columns with random indices applied
    upperBregion = pandas.DataFrame(RDF("B2JpsiMuNuXTuple/DecayTree", "/r01/lhcb/delaney/FIT/JpsiMuNu/2016/OffSel_Bc2JpsiMuNu_noVisMcuts.root").Filter("B_plus_M>6280").AsNumpy())  
    
    #random indices
    r_ind = np.random.randint(low=0, high=upperBregion.shape[0], size = EventMixedDF.shape[0], dtype="int") 
    
    # now vectorize the sampler; need no att SV's but flight distances to attach to the PV's to get to SV's
    EventMixedDF["B_plus_flight_to_SV_X"] = (upperBregion.iloc[r_ind]["B_plus_SV_X"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_X"]).reset_index(drop=True)  
    EventMixedDF["B_plus_flight_to_SV_Y"] = (upperBregion.iloc[r_ind]["B_plus_SV_Y"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_Y"]).reset_index(drop=True) 
    EventMixedDF["B_plus_flight_to_SV_Z"] = (upperBregion.iloc[r_ind]["B_plus_SV_Z"]).reset_index(drop=True) - (upperBregion.iloc[r_ind]["B_plus_OWNPV_Z"]).reset_index(drop=True)  
    
    # now get synthetic SV's, where we sampled really how much a B has flown and attach them to PV's per event
    EventMixedDF["B_plus_SV_X"] = EventMixedDF["B_plus_OWNPV_X"] + EventMixedDF["B_plus_flight_to_SV_X"] 
    EventMixedDF["B_plus_SV_Y"] = EventMixedDF["B_plus_OWNPV_Y"] + EventMixedDF["B_plus_flight_to_SV_Y"] 
    EventMixedDF["B_plus_SV_Z"] = EventMixedDF["B_plus_OWNPV_Z"] + EventMixedDF["B_plus_flight_to_SV_Z"] 
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    
    # write to ROOT file
    EventMixedDF.to_root(f"EvtMix_Jpsi/EvtMixed_noObservables_JpsiMuNu_{i}.root", key="B2JpsiMuNuXTuple/DecayTree")
    # ==============================================================================================

print("n-th Evt Mixing calls complete")


# -------------------------------------------
hadd_command_string = '''export PATH=$(getconf PATH)
export HOME=/usera/delaney
. /lhcb/scripts/lhcb-setup.sh
lb-run DaVinci/latest hadd -fk EvtMix_Jpsi/EvtMixed_noObservables_JpsiMuNu.root /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EvtMix_Jpsi/EvtMixed_noObservables_JpsiMuNu_*.root
rm /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EvtMix_Jpsi/EvtMixed_noObservables_JpsiMuNu_*.root'''  

with open("EvtMix_Jpsi/output.txt", "w") as f:
    print(hadd_command_string, file=f)

import subprocess, os

subprocess.call(["bash", "EvtMix_Jpsi/output.txt" ])
#-------------------------------------------

