import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?


# recompute some vars in dat
ROOT.gInterpreter.Declare('#include "{}"'.format("EventMixing_D0Mu.h"))
EventMixed_file = "OFFLINESELECTED_noVisMcuts.root" 
#EventMixed_file = "fit_MC_Bc2D0pi0MuNu_2016.root" 


# =================================== SYNTHETIC OBSERVABLES ====================================

#***************
#     DATA
#***************
# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
signal_df= RDF("B2DMuNuX_D02KPiTuple/DecayTree", EventMixed_file)


newVars_signal_df= signal_df.Define("B_comb_4Vec", "Combine2Body(D0_PX, D0_PY, D0_PZ, D0_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                                  .Define("Muon_comb_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                                  .Define("D0_comb_4Vec", "gen4Vec(D0_PX, D0_PY, D0_PZ, D0_M)") \
                                    .Define("B_comb_SV_3Vec_Sampling", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                                    .Define("B_comb_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                                    .Define("BflightVector_sampled", "subtract3Vec( B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec)") \
                                        .Define("comb_B_plus_FD_OWNPV", "ComputeFD(BflightVector_sampled)") \
                                        .Define("comb_FitVar_Mmiss2", "ComputeMmiss2( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec)") \
                                        .Define("comb_FitVar_El",    "ComputeEl(B_comb_4Vec, BflightVector_sampled, Muon_comb_4Vec)") \
                                        .Define("B_plus_CoM_P", "Compute_B_EstMomentum( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec )") \
                                        .Define("B_plus_TAU", "(comb_B_plus_FD_OWNPV*6275.6/B_plus_CoM_P)") \
                                        .Define("D0_ETA", "D0_comb_4Vec.Eta()") \
                                        .Define("Mu_plus_ETA", "Muon_comb_4Vec.Eta()") \
                                        .Define("B_plus_FD_OWNPV_X", "BflightVector_sampled.X()")\
                                        .Define("B_plus_FD_OWNPV_Y", "BflightVector_sampled.Y()")\
                                        .Define("B_plus_FD_OWNPV_Z", "BflightVector_sampled.Z()")\
                                        .Define("CosXY_Mu_plus_D0",          "GenerateCosXYD0Mu(D0_PX, D0_PY, Mu_plus_PX, Mu_plus_PY)") \
                                        .Define("CosXYZ_Mu_plus_Pi_1",          "GenerateCosXYZ_hMu(Pi_1_PX, Pi_1_PY, Pi_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                        .Define("CosXYZ_Mu_plus_K_minus",          "GenerateCosXYZ_hMu(K_minus_PX, K_minus_PY, K_minus_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \

#out_df = pandas.DataFrame(newVars_signal_df.AsNumpy())
#from root_pandas import to_root
#out_df.to_root(EventMixed_file, key='B2DMuNuX_D02KPiTuple/DecayTree')


#***************
#  FakeMuon
#***************
# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
misID_df= RDF("B2DMuNuX_D02KPi_FakeMuonTuple/DecayTree", EventMixed_file)

newVars_misID_df= misID_df.Define("B_comb_4Vec", "Combine2Body(D0_PX, D0_PY, D0_PZ, D0_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                                  .Define("Muon_comb_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                                  .Define("D0_comb_4Vec", "gen4Vec(D0_PX, D0_PY, D0_PZ, D0_M)") \
                                    .Define("B_comb_SV_3Vec_Sampling", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                                    .Define("B_comb_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                                    .Define("BflightVector_sampled", "subtract3Vec( B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec)") \
                                        .Define("comb_B_plus_FD_OWNPV", "ComputeFD(BflightVector_sampled)") \
                                        .Define("comb_FitVar_Mmiss2", "ComputeMmiss2( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec)") \
                                        .Define("comb_FitVar_El",    "ComputeEl(B_comb_4Vec, BflightVector_sampled, Muon_comb_4Vec)") \
                                        .Define("B_plus_CoM_P", "Compute_B_EstMomentum( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec )") \
                                        .Define("B_plus_TAU", "(comb_B_plus_FD_OWNPV*6275.6/B_plus_CoM_P)") \
                                        .Define("Mu_plus_ETA", "Muon_comb_4Vec.Eta()") \
                                        .Define("D0_ETA", "D0_comb_4Vec.Eta()") \
                                        .Define("B_plus_FD_OWNPV_X", "BflightVector_sampled.X()")\
                                        .Define("B_plus_FD_OWNPV_Y", "BflightVector_sampled.Y()")\
                                        .Define("B_plus_FD_OWNPV_Z", "BflightVector_sampled.Z()")\
                                        .Define("CosXY_Mu_plus_D0",          "GenerateCosXYD0Mu(D0_PX, D0_PY, Mu_plus_PX, Mu_plus_PY)") \
                                        .Define("CosXYZ_Mu_plus_Pi_1",          "GenerateCosXYZ_hMu(Pi_1_PX, Pi_1_PY, Pi_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                        .Define("CosXYZ_Mu_plus_K_minus",          "GenerateCosXYZ_hMu(K_minus_PX, K_minus_PY, K_minus_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \

newVars_misID_df.Snapshot("DecayTree", "MISID_OffSel_NewVars_noVisMCuts.root")
newVars_signal_df.Snapshot("DecayTree", "SIG_OffSel_NewVars_noVisMCuts.root") 

