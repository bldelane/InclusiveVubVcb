import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?


# recompute some vars in dat
EventMixed_file = "SIG_OffSel_NewVars_noVisMCuts.root" 
#Combinatorial_file = "Pre_Combinatorial.root"

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
input_DATAdf= RDF("DecayTree", EventMixed_file).Filter("( B_plus_M>6280 )")
input_DATAdf.Snapshot("DecayTree", "UppermB_DATA_NoVisMCuts.root")

#input_COMBdf= RDF("DecayTree", Combinatorial_file).Filter("comb_B_plus_M>6280")
#input_COMBdf.Snapshot("DecayTree", "UppermB_COMB_NoVisMCuts.root")

