import numpy as np
import uproot
from hep_ml import reweight
import pandas as pd
from hep_ml.metrics_utils import ks_2samp_weighted
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from root_pandas import to_root
import pandas

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame

offlineselection_angular_cuts_string = "CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_2<0.999 && CosXYZ_Mu_plus_Mu_1<0.999"
combinatorial_angular_cuts_string = "CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_2<0.999 && CosXYZ_Mu_plus_Mu_1<0.999"
#combinatorial_angular_cuts_string = "comb_B_plus_DIRA>0.999"


#upperBmass_RDF = RDF("DecayTree", "OffSel_Bc2JpsiMuNu_SIG_noVisMCuts.root").Filter(f"B_plus_M>6280")
#EvtMixGBR_RDF = RDF("DecayTree", "Pre_Combinatorial_JpsiMuNu.root")
upperBmass_RDF = RDF("B2JpsiMuNuXTuple/DecayTree", "/r01/lhcb/delaney/FIT/JpsiMuNu/2016/GBR_2016_Bc2JpsiMuNuX.root").Filter(f"B_plus_M>6280")
EvtMixGBR_RDF = RDF("EventMixedTuple/DecayTree", "/r01/lhcb/delaney/FIT/JpsiMuNu/2016/GBR_2016_Bc2JpsiMuNuX.root")
training_sample = pandas.DataFrame(EvtMixGBR_RDF.AsNumpy())
#training_sample = pandas.DataFrame(EvtMixGBR_RDF.Filter(f"B_plus_M>6280").AsNumpy())
#FitRange_evtmixed = pandas.DataFrame(EvtMixGBR_RDF.Filter("B_plus_M<6280").AsNumpy())
data = pandas.DataFrame(upperBmass_RDF.AsNumpy())



# bookkeeping
#training_sample["comb_Mu_plus_PX"] = training_sample["Mu_plus_PX"]
#training_sample["comb_Mu_plus_PY"] = training_sample["Mu_plus_PY"]
#training_sample["comb_Mu_plus_PZ"] = training_sample["Mu_plus_PZ"]
#training_sample["comb_Jpsi_PX"] = training_sample["Jpsi_PX"]
#training_sample["comb_Jpsi_PY"] = training_sample["Jpsi_PY"]
#training_sample["comb_Jpsi_PZ"] = training_sample["Jpsi_PZ"]
#training_sample["comb_B_plus_CoM_P"] = training_sample["B_plus_CoM_P"]
#training_sample["comb_B_plus_TAU"] = training_sample["B_plus_TAU"]
#training_sample["comb_B_plus_DIRA_OWNPV"] = training_sample["comb_B_plus_DIRA"]
#training_sample["comb_CosXY_Mu_plus_Jpsi"] = training_sample["CosXY_Mu_plus_Jpsi"]
#training_sample["comb_CosXYZ_Mu_plus_Mu_2"] = training_sample["CosXYZ_Mu_plus_Mu_2"]
#training_sample["comb_CosXYZ_Mu_plus_Mu_1"] = training_sample["CosXYZ_Mu_plus_Mu_1"]
#training_sample["comb_Jpsi_M"] = training_sample["Jpsi_M"]
#
#del training_sample["B_plus_TAU"]
#del training_sample["comb_B_plus_DIRA"]
#del training_sample["CosXY_Mu_plus_Jpsi"]
#del training_sample["CosXYZ_Mu_plus_Mu_2"]
#del training_sample["CosXYZ_Mu_plus_Mu_1"]
#del training_sample["Jpsi_M"]
#del training_sample["Mu_plus_PX"]
#del training_sample["Mu_plus_PY"]
#del training_sample["Mu_plus_PZ"]
#del training_sample["Jpsi_PX"]
#del training_sample["Jpsi_PY"]
#del training_sample["Jpsi_PZ"]


config = {
            
            "daughter_PT_ETA" : {
                "evtMix_vars" : ["Jpsi_PT", "Mu_plus_PT", "Jpsi_ETA", "Mu_plus_ETA","B_plus_FD_OWNPV"], 
                "data_vars" : ["Jpsi_PT", "Mu_plus_PT", "Jpsi_ETA", "Mu_plus_ETA", "B_plus_FD_OWNPV"],
                "prefix": "daughter_PT_ETA__"
                },
            
            
           # "daughter_components" : {
           #     "evtMix_vars" : ["B_plus_CoM_P","comb_B_plus_PT", "comb_B_plus_FD_OWNPV"], 
           #     "data_vars" : ["B_plus_CoM_P","B_plus_PT", "B_plus_FD_OWNPV"],
           #     "prefix": "daughter_XYZ_"
           #     },
            
          
        }

def GBR(evtMix_vars, data_vars, prefix):
    

    print(f"Reweighting set of vars: {evtMix_vars, data_vars}")
    #set reweighter with k-folding for unbiased evaluation
    reweighter_base = reweight.GBReweighter(n_estimators=30, 
                                            learning_rate=0.1, max_depth=3, min_samples_leaf=1000, )

    reweighter = reweight.FoldingReweighter(reweighter_base, n_folds=2)
    
    reweighter.fit(original = training_sample.query("B_plus_M>6280")[evtMix_vars],
                   target = data[data_vars]) 

    training_weights = reweighter.predict_weights(
        original=training_sample[evtMix_vars],
                                )
    
    #folding_weights = reweighter.predict_weights(
    #    original=FitRange_evtmixed[evtMix_vars],
    #                            )

    training_sample["GBR_w"] = training_weights
    #FitRange_evtmixed["GBR_w"] = folding_weights

    def evalGBR(branch):
        print('KS over = ', branch, ks_2samp_weighted(training_sample["comb_"+branch], data[branch], 
                                                     weights1=training_weights, weights2=np.ones(data.shape[0], dtype=float)))
            
    #evalGBR("FitVar_Mmiss2")
    #evalGBR("FitVar_El")
    #evalGBR("B_plus_MCORR")
    #evalGBR("B_plus_M")
    #evalGBR("B_plus_MCORRERR")
    #evalGBR("B_plus_FD_OWNPV")
    #evalGBR("B_plus_P")
    #evalGBR("B_plus_CoM_P")
    

    
    #rom root_pandas import to_root
    #training_sample.to_root(f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_UPPER_Combinatorial_noVisMCuts_JpsiMuNu.root", key='DecayTree')
    #FitRange_evtmixed.to_root(f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_FitRange_Combinatorial_noVisMCuts_JpsiMuNu.root", key='DecayTree')
    training_sample.query("B_plus_M>6280").to_root(f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_FitRange_Combinatorial_noVisMCuts_JpsiMuNu.root", key='DecayTree')

for key in config.keys():
    GBR(config[key]["evtMix_vars"], config[key]["data_vars"], config[key]["prefix"])


'''
#------------------------------------- Implement Cuts for Fitting -------------------------------------

prefix = "daughter_PT_ETA__" 

SIGNAL_RDF= RDF("DecayTree", "OffSel_Bc2JpsiMuNu_SIG_noVisMCuts.root").Filter("B_plus_M>3500 && B_plus_M<6280 && B_plus_MCORRERR<650 && CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_1<0.999 && CosXYZ_Mu_plus_Mu_2<0.999")

MISID_RDF= RDF("DecayTree", "OffSel_Bc2JpsiMuNu_MISID_noVisMCuts.root").Filter("B_plus_M>3500 && B_plus_M<6280 && B_plus_MCORRERR<650 && CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_1<0.999 && CosXYZ_Mu_plus_Mu_2<0.999")

Combinatorial_RDF = RDF("DecayTree",f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_FitRange_Combinatorial_noVisMCuts_JpsiMuNu.root").Filter("comb_B_plus_M>3500 && comb_B_plus_M<6280 && comb_B_plus_MCORRERR<650 && CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_1<0.999 && CosXYZ_Mu_plus_Mu_2<0.999")

  

SIGNAL_RDF.Snapshot("DecayTree", "JpsiMuNuX/FIT_SIGNAL_Bc2D0MuNuX.root")
MISID_RDF.Snapshot("DecayTree", "JpsiMuNuX/FIT_MISID_Bc2D0MuNuX.root")
Combinatorial_RDF.Snapshot("DecayTree", "JpsiMuNuX/FIT_Combinatorial_Bc2D0MuNuX.root")
'''
