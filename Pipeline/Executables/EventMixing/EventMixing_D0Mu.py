import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal
import uproot, pandas, numpy 
import os
from hep_ml import reweight

ROOT.gROOT.LoadMacro("lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);

# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?


ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.h"))
EventMixed_file = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EvtMixed_noObservables.root" 


# =================================== SYNTHETIC OBSERVABLES ====================================

# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame
evtMixed_df= RDF("B2DMuNuX_D02KPiTuple/DecayTree", EventMixed_file)


# ==== histogram of 2D (R,Z) combinatorial SV info
#f = ROOT.TFile("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/SVRZ_forSampling.root")
#ROOT.gInterpreter.ProcessLine("auto histo = h;")
SetMomentumBudget_RDF = evtMixed_df.Define("CosXY_Mu_plus_D0",          "GenerateCosXYD0Mu(D0_PX, D0_PY, Mu_plus_PX, Mu_plus_PY)") \
                                   .Define("CosXYZ_Mu_plus_K_minus", "GenerateCosXYZ_hMu(K_minus_PX, K_minus_PY, K_minus_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                   .Define("CosXYZ_Mu_plus_Pi_1",          "GenerateCosXYZ_hMu(Pi_1_PX, Pi_1_PY, Pi_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \




CombinatoricShapesDF = SetMomentumBudget_RDF.Define("B_comb_4Vec", "Combine2Body(D0_PX, D0_PY, D0_PZ, D0_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                                  .Define("B_comb_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                                  .Define("B_comb_SV_3Vec_Sampling", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                                  .Define("BflightVector_sampled", "subtract3Vec( B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec)") \
                                  .Define("comb_B_plus_DIRA", "ComputeDIRA(BflightVector_sampled, B_comb_4Vec)") \
                                  .Define("Muon_comb_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                                  .Define("D0_comb_4Vec", "gen4Vec(D0_PX, D0_PY, D0_PZ, D0_M)") \
                                        .Define("comb_B_plus_MCORR", "ComputeMCORR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec,B_comb_4Vec)") \
                                        .Define("comb_B_plus_MCORRERR", "ComputeMCORRERR(B_comb_SV_3Vec_Sampling, B_comb_PV_3Vec, B_comb_4Vec)") \
                                        .Define("comb_B_plus_M", "B_comb_4Vec.M()") \
                                        .Define("comb_FitVar_Mmiss2", "ComputeMmiss2( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec)") \
                                        .Define("comb_FitVar_El",    "ComputeEl(B_comb_4Vec, BflightVector_sampled, Muon_comb_4Vec)") \
                                        .Define("comb_B_plus_FD_OWNPV", "ComputeFD(BflightVector_sampled)") \
                                        .Define("comb_B_plus_P", "B_comb_4Vec.P()") \
                                        .Define("B_plus_CoM_P", "Compute_B_EstMomentum( BflightVector_sampled, B_comb_4Vec, D0_comb_4Vec, Muon_comb_4Vec )") \
                                        .Define("comb_B_plus_PT", "B_comb_4Vec.Pt()") \
                                        .Define("B_plus_TAU", "(comb_B_plus_FD_OWNPV*6275.6/B_plus_CoM_P)") \
                                        .Define("comb_B_plus_ETA", "B_comb_4Vec.Eta()") \
                                        .Define("comb_B_plus_SV_R",       "sqrt( (B_comb_SV_3Vec_Sampling.X()*B_comb_SV_3Vec_Sampling.X()) + (B_comb_SV_3Vec_Sampling.Y()*B_comb_SV_3Vec_Sampling.Y()) )") \
                                        .Define("comb_B_plus_SV_X",       "B_comb_SV_3Vec_Sampling.X()") \
                                        .Define("comb_B_plus_SV_Y",       "B_comb_SV_3Vec_Sampling.Y()") \
                                        .Define("comb_B_plus_SV_Z",       "B_comb_SV_3Vec_Sampling.Z()") \
                                        .Define("comb_Mu_plus_PT", "Muon_comb_4Vec.Pt()") \
                                        .Define("comb_Mu_plus_P", "Muon_comb_4Vec.P()") \
                                        .Define("comb_Mu_plus_ETA", "Muon_comb_4Vec.Eta()") \
                                        .Define("comb_D0_PT", "D0_comb_4Vec.Pt()") \
                                        .Define("comb_D0_P", "D0_comb_4Vec.P()") \
                                        .Define("comb_D0_ETA", "D0_comb_4Vec.Eta()") \
                                        .Define("comb_B_plus_FD_OWNPV_X", "BflightVector_sampled.X()")\
                                        .Define("comb_B_plus_FD_OWNPV_Y", "BflightVector_sampled.Y()")\
                                        .Define("comb_B_plus_FD_OWNPV_Z", "BflightVector_sampled.Z()")
                                        #.Define("B_comb_SV_3Vec_Sampling", "sampleRZ(histo, B_plus_OWNPV_X,B_plus_OWNPV_Y,B_plus_OWNPV_Z)") \
                                        #.Define("CosXY_Mu_plus_D0",          "GenerateCosXYD0Mu(D0_PX, D0_PY, Mu_plus_PX, Mu_plus_PY)") \
                                        #.Define("CosXYZ_Mu_plus_Pi_1",          "GenerateCosXYZ_hMu(Pi_1_PX, Pi_1_PY, Pi_1_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                                        #.Define("CosXYZ_Mu_plus_K_minus",          "GenerateCosXYZ_hMu(K_minus_PX, K_minus_PY, K_minus_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \



# =================================== Perform reweighting and write to file  ========================================

##upper visM; origin sample in training for GBR
#evtmix_uppermass_df  = CombinatoricShapesDF.Filter("comb_B_plus_M>6280 && comb_B_plus_M<8000 && comb_B_SV_R<6.") # match stripping and selection to recover shapes + **UPPER MASS FOR TRAINING**
#
## === train on on several vars
#trainGBR_df_CoM = pandas.DataFrame(evtmix_uppermass_df.AsNumpy(columns=["B_plus_CoM_P"]))
#trainGBR_df_PT = pandas.DataFrame(evtmix_uppermass_df.AsNumpy(columns=["B_plus_CoM_P", "comb_B_plus_PT"]))
#trainGBR_df_ETA = pandas.DataFrame(evtmix_uppermass_df.AsNumpy(columns=["B_plus_CoM_P", "comb_B_plus_PT", "comb_B_plus_ETA"]))


# FULL COMBINATORIAL FOR OUTPUT
#selComb_df  = CombinatoricShapesDF.Filter("comb_B_plus_M>2200 && comb_B_plus_M<8000 && comb_B_plus_SV_R<6.") # match stripping and selection to recover shapes
selComb_df  = CombinatoricShapesDF.Filter("comb_B_plus_M>2200 && comb_B_plus_M<8000") # match stripping and selection to recover shapes
selComb_df.Snapshot("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Pre_Combinatorial.root")


# what to apply weights on and save to file; match offline + stripping visM cuts 
#fullRange_GBR_df = pandas.DataFrame(selComb_df.AsNumpy())



## upper visM: DATA 
#eospath = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/"
#original_df = RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/SIG_OffSel_NewVars_noVisMCuts.root") 
#selected_DATA_df = original_df.Filter("B_plus_M>6280")
#
#target_df_CoM = pandas.DataFrame(selected_DATA_df.AsNumpy(columns=["B_plus_CoM_P"])) # reweighter branches
#target_df_PT = pandas.DataFrame(selected_DATA_df.AsNumpy(columns=["B_plus_CoM_P", "B_plus_PT"])) # reweighter branches
#target_df_ETA = pandas.DataFrame(selected_DATA_df.AsNumpy(columns=["B_plus_CoM_P", "B_plus_PT", "B_plus_ETA"])) # reweighter branches
#
#
## define base reweighter
#reweighter_base = reweight.GBReweighter(n_estimators=50, 
#                                        learning_rate=0.1, max_depth=3, min_samples_leaf=1000, 
#                                        )
## k=2 folds
#reweighter_CoM = reweight.FoldingReweighter(reweighter_base, n_folds=2)
#reweighter_CoM.fit(original=trainGBR_df_CoM,
#               target=target_df_CoM, 
#             ); 
#folding_weights_CoM = reweighter_CoM.predict_weights(fullRange_GBR_df[["B_plus_CoM_P"]]) #apparently unbiased, see example on github
#
#reweighter_PT = reweight.FoldingReweighter(reweighter_base, n_folds=2)
#reweighter_PT.fit(original=trainGBR_df_PT,
#               target=target_df_PT, 
#             ); 
#folding_weights_PT = reweighter_PT.predict_weights(fullRange_GBR_df[["B_plus_CoM_P", "comb_B_plus_PT"]]) #apparently unbiased, see example on github
#
#reweighter_ETA = reweight.FoldingReweighter(reweighter_base, n_folds=2)
#reweighter_ETA.fit(original=trainGBR_df_ETA,
#               target=target_df_ETA, 
#             ); 
#folding_weights_ETA = reweighter_ETA.predict_weights(fullRange_GBR_df[["B_plus_CoM_P", "comb_B_plus_PT", "comb_B_plus_ETA"]]) #apparently unbiased, see example on github
#
#
## write to file
#fullRange_GBR_df["GBR_w_CoM"] = folding_weights_CoM
#fullRange_GBR_df["GBR_w_PT"] = folding_weights_PT
#fullRange_GBR_df["GBR_w_ETA"] = folding_weights_ETA

#from root_pandas import to_root
#fullRange_GBR_df.to_root('Combinatorial_OffSel_NewVars_noVisMcuts.root', key='DecayTree')
#fullRange_GBR_df.to_root('/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Pre_Combinatorial.root', key='DecayTree')
# ================================================================================================================
