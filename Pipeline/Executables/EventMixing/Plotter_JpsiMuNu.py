import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml

ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

RDF = ROOT.ROOT.RDataFrame
path = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/"


#angular cut
DATAang_cut_string = "CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_2<0.999 && CosXYZ_Mu_plus_Mu_1<0.999"
EVTMIXang_cut_string = "CosXY_Mu_plus_Jpsi>-0.4 && CosXYZ_Mu_plus_Mu_1<0.999 && CosXYZ_Mu_plus_Mu_2<0.999 && B_plus_SV_R<6"
#EVTMIXang_cut_string = " comb_B_plus_DIRA_OWNPV>0.999 "

# upper mass data
ORIGINAL_DATA_RDF = RDF("DecayTree", "OffSel_Bc2JpsiMuNu_SIG_noVisMCuts.root").Filter(f"B_plus_M>6280 && {DATAang_cut_string}")

# GBR schemes
GBR_Bvars = RDF("DecayTree", path+"daughter_PT_ETA___FitRange_Combinatorial_noVisMCuts_JpsiMuNu.root").Filter(EVTMIXang_cut_string)
#GBR_comps = RDF("DecayTree", path+"daughter_XYZ__UPPER_Combinatorial_noVisMCuts_JpsiMuNu.root").Filter(EVTMIXang_cut_string)


def Plot(var, minvar, maxvar, binsvar, xlabelvar, pdfvar):
  
    
    c1 = TCanvas()
    original_shape = ORIGINAL_DATA_RDF.Histo1D(("hdata", "Upper data sideband", binsvar, minvar, maxvar), var)
    original_shape.SetLineColor( kWhite)
    original_shape.SetMarkerColor( kGray+2)
    original_shape.SetFillColorAlpha( kGray+2, .5)

    original_shape.Scale(1./original_shape.Integral())
    
    evtmix_shape = GBR_Bvars.Histo1D(("hEvtMix", "Raw event mixing upper sideband", binsvar, minvar, maxvar), var)
    evtmix_shape.SetLineColor( kBlack)
    evtmix_shape.SetMarkerColor( kBlack)
    evtmix_shape.Scale(1./evtmix_shape.Integral())

    GBR_B_shape = GBR_Bvars.Histo1D(("hBvars", "GBR with B observables", binsvar, minvar, maxvar), var, "GBR_w")
    GBR_B_shape.SetLineColor( kAzure+1)
    GBR_B_shape.SetMarkerColor( kAzure+1)
    GBR_B_shape.Scale(1./GBR_B_shape.Integral())

    #GBR_comps_shape = GBR_comps.Histo1D(("hComps", "GBR with daughters and FD comps", binsvar, minvar, maxvar), "comb_"+var, "GBR_w")
    #GBR_comps_shape.SetLineColor( kRed+1)
    #GBR_comps_shape.SetMarkerColor( kRed+1)
    #GBR_comps_shape.Scale(1./GBR_comps_shape.Integral())
    
    original_shape.GetYaxis().SetTitle("Arbitrary Units")
    original_shape.GetXaxis().SetTitle(xlabelvar)
    
    
    original_shape.Draw("E2 L")
    evtmix_shape.Draw("EPSAME")
    GBR_B_shape.Draw("EPSAME")
    #GBR_comps_shape.Draw("EPSAME")
    
    
    legend=TLegend(0.6,0.7,0.8,0.9)
    legend.SetTextFont(132)
    legend.SetTextSize(0.03)
    legend.AddEntry("hdata","2016 Data #it{m_{B}}>6280 MeV/c^{2}","pf")
    legend.AddEntry("hEvtMix", "EvtMix #it{m_{B}}>6280 MeV/c^{2}", "lep")
    legend.AddEntry("hBvars", "EvtMix+GBR{P, PT, B_FD} #it{m_{B}}>6280 MeV/c^{2}", "lep")
    #legend.AddEntry("hComps", "EvtMix+GBR{B_PT, B_Plab, B_FD} #it{m_{B}}>6280 MeV/c^{2}", "lep")

    legend.Draw()
    
    c1.Print("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PLOTS/"+pdfvar+".pdf")


# Read YAML file
#with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/PlotPars_JpsiMuNu.yml", 'r') as stream:
with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/Jpsi/PlotPars.yml", 'r') as stream:
        to_plot = yaml.safe_load(stream)

for branch in to_plot.keys():
    
    print("===> Plotting ", branch)
    Plot(branch, **to_plot[branch])
    


