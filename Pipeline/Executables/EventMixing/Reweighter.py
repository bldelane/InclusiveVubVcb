import numpy as np
import uproot
from hep_ml import reweight
import pandas as pd
from hep_ml.metrics_utils import ks_2samp_weighted
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import ROOT
from root_pandas import to_root
import pandas

ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.h"))
# load into RDF a data file on eos
RDF = ROOT.ROOT.RDataFrame

offlineselection_angular_cuts_string = "CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999"
combinatorial_angular_cuts_string = "CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999"
#combinatorial_angular_cuts_string = "comb_B_plus_DIRA>0.999"


upperBmass_RDF = RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/SIG_OffSel_NewVars_noVisMCuts.root").Filter(f"B_plus_M>6280")

print("upper data mass band loaded")

training_sample = pandas.DataFrame(RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Pre_Combinatorial.root").Filter("comb_B_plus_M>6280").AsNumpy())

print("training samples loaded")

FitRange_evtmixed = pandas.DataFrame(RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Pre_Combinatorial.root").Filter("comb_B_plus_M<6280").AsNumpy())

print("Fit Range evt mix sample loaded")

data = pandas.DataFrame(upperBmass_RDF.AsNumpy())

print("Loading complete.")

# bookkeeping
#training_sample["comb_Mu_plus_PX"] = training_sample["Mu_plus_PX"]
#training_sample["comb_Mu_plus_PY"] = training_sample["Mu_plus_PY"]
#training_sample["comb_Mu_plus_PZ"] = training_sample["Mu_plus_PZ"]
#training_sample["comb_D0_PX"] = training_sample["D0_PX"]
#training_sample["comb_D0_PY"] = training_sample["D0_PY"]
#training_sample["comb_D0_PZ"] = training_sample["D0_PZ"]
#training_sample["comb_B_plus_CoM_P"] = training_sample["B_plus_CoM_P"]
#training_sample["comb_B_plus_TAU"] = training_sample["B_plus_TAU"]
#training_sample["comb_B_plus_DIRA_OWNPV"] = training_sample["comb_B_plus_DIRA"]
#training_sample["comb_CosXY_Mu_plus_D0"] = training_sample["CosXY_Mu_plus_D0"]
#training_sample["comb_CosXYZ_Mu_plus_Pi_1"] = training_sample["CosXYZ_Mu_plus_Pi_1"]
#training_sample["comb_CosXYZ_Mu_plus_K_minus"] = training_sample["CosXYZ_Mu_plus_K_minus"]
#training_sample["comb_D0_M"] = training_sample["D0_M"]
#
#del training_sample["B_plus_TAU"]
#del training_sample["comb_B_plus_DIRA"]
#del training_sample["CosXY_Mu_plus_D0"]
#del training_sample["CosXYZ_Mu_plus_Pi_1"]
#del training_sample["CosXYZ_Mu_plus_K_minus"]
#del training_sample["D0_M"]
#del training_sample["Mu_plus_PX"]
#del training_sample["Mu_plus_PY"]
#del training_sample["Mu_plus_PZ"]
#del training_sample["D0_PX"]
#del training_sample["D0_PY"]
#del training_sample["D0_PZ"]


config = {
            
            "daughter_PT_ETA" : {
                "evtMix_vars" : ["comb_D0_PT", "comb_Mu_plus_PT", "comb_D0_ETA", "comb_Mu_plus_ETA","comb_B_plus_FD_OWNPV"], 
                "data_vars" : ["D0_PT", "Mu_plus_PT", "D0_ETA", "Mu_plus_ETA", "B_plus_FD_OWNPV"],
                "prefix": "daughter_PT_ETA__"
                },
            
            
            #"daughter_components" : {
            #    "evtMix_vars" : ["B_plus_CoM_P","comb_B_plus_PT", "comb_B_plus_FD_OWNPV"], 
            #    "data_vars" : ["B_plus_CoM_P","B_plus_PT", "B_plus_FD_OWNPV"],
            #    "prefix": "daughter_XYZ_"
            #    },
            
          
        }

def GBR(evtMix_vars, data_vars, prefix):
    

    print(f"Reweighting set of vars: {evtMix_vars, data_vars}")
    #set reweighter with k-folding for unbiased evaluation
    reweighter_base = reweight.GBReweighter(n_estimators=150, 
                                            learning_rate=0.1, max_depth=5, min_samples_leaf=1000, )

    reweighter = reweight.FoldingReweighter(reweighter_base, n_folds=2)
    
    reweighter.fit(original = training_sample[evtMix_vars],
                   target = data[data_vars]) 

    #training_weights = reweighter.predict_weights(
    #    original=training_sample[evtMix_vars],
                               # )
    
    folding_weights = reweighter.predict_weights(
        original=FitRange_evtmixed[evtMix_vars],
                                )

    #training_sample["GBR_w"] = training_weights
    FitRange_evtmixed["GBR_w"] = folding_weights

    def evalGBR(branch):
        print('KS over = ', branch, ks_2samp_weighted(training_sample["comb_"+branch], data[branch], 
                                                     weights1=training_weights, weights2=np.ones(data.shape[0], dtype=float)))
            
    #evalGBR("FitVar_Mmiss2")
    #evalGBR("FitVar_El")
    #evalGBR("B_plus_MCORR")
    #evalGBR("B_plus_M")
    #evalGBR("B_plus_MCORRERR")
    #evalGBR("B_plus_FD_OWNPV")
    #evalGBR("B_plus_P")
    #evalGBR("B_plus_CoM_P")
    

    
    #rom root_pandas import to_root
    #training_sample.to_root(f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_UPPER_Combinatorial_noVisMCuts.root", key='DecayTree')
    FitRange_evtmixed.to_root(f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_FitRange_Combinatorial_noVisMCuts.root", key='DecayTree')

for key in config.keys():
    GBR(config[key]["evtMix_vars"], config[key]["data_vars"], config[key]["prefix"])


#------------------------------------- Implement Cuts for Fitting -------------------------------------

prefix = "daughter_PT_ETA__" 

SIGNAL_RDF= RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/SIG_OffSel_NewVars_noVisMCuts.root").Filter("B_plus_M>3500 && B_plus_M<6280 && B_plus_MCORRERR<650 && CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999")

MISID_RDF= RDF("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/MISID_OffSel_NewVars_noVisMCuts.root").Filter("B_plus_M>3500 && B_plus_M<6280 && B_plus_MCORRERR<650 && CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999")

Combinatorial_RDF = RDF("DecayTree",f"/var/clus/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/DaughterKinematics/{prefix}_FitRange_Combinatorial_noVisMCuts.root").Filter("comb_B_plus_M>3500 && comb_B_plus_M<6280 && comb_B_plus_MCORRERR<650 && CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.999 && CosXYZ_Mu_plus_K_minus<0.999")

  

SIGNAL_RDF.Snapshot("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/D0MuNuX/FIT_SIGNAL_Bc2D0MuNuX.root")
MISID_RDF.Snapshot("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/D0MuNuX/FIT_MISID_Bc2D0MuNuX.root")
Combinatorial_RDF.Snapshot("DecayTree", "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/D0MuNuX/FIT_Combinatorial_Bc2D0MuNuX.root")

