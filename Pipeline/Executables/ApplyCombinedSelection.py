#!/usr/bin/env python

# --- SKIMMER SCRIPT TO IMPLEMENT SELECTION AND SKIM SELECTION BRANCHES ---

# PyROOT file to skim the data and MC Tuples to copy trees to root files with only selection
# variables written to file

# __author__: Matthew Kenzie
# __email__: matthew.kenzie@cern.ch

#----------------------------------------------------------------------------
# *USE*: python ApplyCombinedSelection <options>
#----------------------------------------------------------------------------

import ROOT as r
from yaml import load, Loader
import yaml
import sys
import os
import argparse


parser = argparse.ArgumentParser(description='python ApplyCombinedSelection.py <opts>')
parser.add_argument('-i','--input',     dest='input',     required=True, help='Input ROOT file')
parser.add_argument('-o','--output',    dest='output',    required=True, help='Output ROOT file')
parser.add_argument('-b','--branches',  dest='branches',  required=True, help='Branch.yml file')
parser.add_argument('-s','--selection', dest='selection', required=True, help='Selection file')
parser.add_argument('-t','--tupledir',  dest='tupledir',  default="None", help='Tuple directory or directories comma seperated')
opts = parser.parse_args()


def ParseSelection(yamlselfile):
    sel = {}
    with open(yamlselfile, 'r') as stream:
        try:
            sel= yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    selection_string = "( " + " && ".join(list(sel.values())) + " )"
    return selection_string



def Select(tree_directory, sel_file, branch_file):

    selection = ParseSelection(sel_file)

    # this is an utterly stupid way of doing it but oh well
    if tree_directory=="None":
        print(f"Full path checks: {opts.input}, DecayTree")
        decaytree = input_file.Get("DecayTree")
    else:
        print(f"Full path checks: {opts.input}, {tree_directory}/DecayTree")
        decaytree = input_file.Get(f"{tree_directory}/DecayTree")
    if not decaytree: return

    population = int(decaytree.GetEntries())
    print(f"Stripped population: {population}")

    # read in dict of relevant branches, while killing the rest
    with open(branch_file, 'r') as stream:
    #with open(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/JpsiMuNu/LocalSelectionBranches.yml", 'r') as stream:
        inbranches = load(stream, Loader=Loader)
    # set all branches to status == 0
    decaytree.SetBranchStatus("*", 0)
    # activate branches of interest
    [ decaytree.SetBranchStatus(b, 1) for b in inbranches.values() if b in decaytree.GetListOfBranches() ]

    if tree_directory != "None":
        D02KPi_dir = target_file.mkdir(f"{tree_directory}")
        D02KPi_dir.cd()

    print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print('Selection in place:')
    print('-------------------')
    print(selection)

    target_tree = decaytree.CopyTree(selection)
    print( "\n===> Entries written to skimmed file:", target_tree.GetEntries() )
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


    if tree_directory=="None":
        print(f"\nSkimmed tree saved to {opts.output}/DecayTree")
    else:
        print(f"\nSkimmed tree saved to {opts.output}/{tree_directory}/DecayTree")





# start keeping time
import time
start = time.time()

input_file = r.TFile.Open(opts.input)
target_file = r.TFile.Open(opts.output, "recreate")

# loop directories
for tup_dir in opts.tupledir.split(","):
  print(f"\nSkimming configuration: dir={tup_dir}, sel={opts.selection}")
  print(f"\n===> Executing Selection Stage")
  Select(tup_dir, opts.selection, opts.branches)

target_file.Write()
target_file.Close()

end = time.time()
print( 'Compilation time in seconds: ', (end - start) )
print( '\n           ------ SKIMMING COMPLETED -----\n' )
