#!/usr/bin/env python

"""
Script to perform calculation of simple angular variables and store them in new ROOT files.

__author__ : Matthew Kenzie
__email__: matthew.kenzie@cern.ch
"""
import argparse
parser = argparse.ArgumentParser(description='Parameters for I/O + calculation of angular variables')
parser.add_argument('-i','--input', dest='input',  help='Input ROOT file', required=True)
parser.add_argument('-o','--output', dest='output', help='Output ROOT file', required=True)
parser.add_argument('-t','--tupledir', dest='tupledir', help='Tuple Directory (if any)', default=None)
opts = parser.parse_args()

from root_pandas import read_root, to_root
import ROOT, pandas
from yaml import load, Loader
import yaml
from optparse import OptionParser
import sys
import os

ROOT.gInterpreter.Declare('#include "{}"'.format("common/EventMixing_D0Mu.h"))
ROOT.ROOT.EnableImplicitMT()

# custom pair type: pair of strings separated by comma
def pair(arg):
    return [str(x) for x in arg.split(',')]


def cosXYZ(df, track1, track2):
    """Compute cos of 3D angle between tracks from 3-momentum vector and add to df
    """
    df.eval(f"CosXYZ_{track1}_{track2} = ({track1}_PX*{track2}_PX + {track1}_PY*{track2}_PY + {track1}_PZ*{track2}_PZ)/( (({track1}_PX**2 + {track1}_PY**2 + {track1}_PZ**2)**(.5)) * (({track2}_PX**2+{track2}_PY**2+{track2}_PZ**2)**(.5))   )", inplace=True)

def cosXY(df, track1, track2):
    """Compute cos of 3D angle between tracks from 2-momentum vector and add to df
    """
    df.eval(f"CosXY_{track1}_{track2} = ({track1}_PX*{track2}_PX + {track1}_PY*{track2}_PY)/( (({track1}_PX**2 + {track1}_PY**2)**(.5)) * (({track2}_PX**2+{track2}_PY**2)**(.5))   )", inplace=True)


infile_name = opts.input
# assign daughter branches from hadron in B decay
if infile_name.find("Jpsi") != -1:
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
else:
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"


target_file_name = opts.output

# add angular vars and mass combinations mu+hadron_daughters
def ComputeVars():
    print(f"Full path checks: {infile_name}")

    RDF = ROOT.ROOT.RDataFrame
    input_tr = "DecayTree"
    if opts.tupledir: input_tr = opts.tupledir +"/DecayTree"
    input_df= RDF(f"{input_tr}", f"{infile_name}")
    #assert input_df.Count().GetValue()>0

    input_df = input_df.Define(f"CosXY_Mu_plus_{hadron}",          f"GenerateCosXYD0Mu({hadron}_PX, {hadron}_PY, Mu_plus_PX, Mu_plus_PY)") \
                             .Define(f"CosXYZ_Mu_plus_{had_daughter_1}", f"GenerateCosXYZ_hMu({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                             .Define(f"CosXYZ_Mu_plus_{had_daughter_2}", f"GenerateCosXYZ_hMu({had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                             .Define(f"Mu_plus_{had_daughter_1}_4Vec", f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass})") \
                             .Define(f"Mu_plus_{had_daughter_2}_4Vec", f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                             .Define(f"{had_daughter_1}_{had_daughter_2}_4Vec", f"Combine2Body({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass}, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                             .Define(f"Mu_plus_{had_daughter_1}_M", f"Mu_plus_{had_daughter_1}_4Vec.M()") \
                             .Define(f"Mu_plus_{had_daughter_2}_M", f"Mu_plus_{had_daughter_2}_4Vec.M()") \
                             .Define(f"{had_daughter_1}_{had_daughter_2}_M", f"{had_daughter_1}_{had_daughter_2}_4Vec.M()") \
                             .Define("B_plus_4Vec", f"Combine2Body({hadron}_PX, {hadron}_PY, {hadron}_PZ, {hadron}_M, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                             .Define("B_plus_PV_3Vec", "gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
                             .Define("B_plus_SV_3Vec", "gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
                             .Define("BflightVector", "subtract3Vec( B_plus_SV_3Vec, B_plus_PV_3Vec)") \
                             .Define("Mu_plus_4Vec", "gen4Vec(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ,105.66)") \
                             .Define(f"{hadron}_4Vec", f"gen4Vec({hadron}_PX, {hadron}_PY, {hadron}_PZ, {hadron}_M)") \
                             .Define(f"{had_daughter_1}_4Vec", f"gen4Vec({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass})") \
                             .Define(f"{had_daughter_2}_4Vec", f"gen4Vec({had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                             .Define("B_plus_Lab_P", f"Compute_B_EstMomentum( BflightVector, B_plus_4Vec, {hadron}_4Vec, Mu_plus_4Vec )") \
                             .Define("B_plus_TAU", "(B_plus_FD_OWNPV*6275.6/B_plus_Lab_P)") \
                             .Define(f"B_plus_ETA", f"B_plus_4Vec.Eta()") \
                             .Define(f"{hadron}_ETA", f"{hadron}_4Vec.Eta()") \
                             .Define(f"{had_daughter_1}_ETA", f"{had_daughter_1}_4Vec.Eta()") \
                             .Define(f"{had_daughter_2}_ETA", f"{had_daughter_2}_4Vec.Eta()") \
                             .Define(f"Mu_plus_ETA", f"Mu_plus_4Vec.Eta()") \
                             .Define(f"B_plus_FIT_LTIME", f"((B_plus_M*B_plus_FD_OWNPV)/B_plus_P)/3e2") \


    ''' # deprecated
    input_df = read_root(f"{infile_name}", f"{tree_directory}Tuple/DecayTree") # read comprehensively all branches
    assert input_df.shape[0]>0

    print(f"Input population: {input_df.shape[0]}")
    # misID cuts
    cosXYZ(input_df, "Mu_plus", had_daughter_1)
    cosXYZ(input_df, "Mu_plus", had_daughter_2)

    # combinatorial cuts
    cosXY(input_df, "Mu_plus", hadron)
   '''

    # patch, not sure how to specify tdirectory in RDF technology
    pd_outDF = pandas.DataFrame(input_df.AsNumpy())
    pd_outDF.to_root(target_file_name, key=f"DecayTree", mode='a')
    #pd_outDF.to_root(target_file_name, key=f"{tree_directory}Tuple/DecayTree", mode='a')

    # catch duplicates
    if pd_outDF.shape[0]>0:
        output_df = read_root(f"{target_file_name}", f"DecayTree", columns=["FitVar*"]) # read only proxy branches I know will be in tuple
        assert input_df.Count().GetValue()==output_df.shape[0], "ERROR: duplicates are probably being generated! Input DF #entries != Output DF #entries"

# start keeping time
import time
start = time.time()

#remove target if existent, this is necessary for root_pandas to work well
if os.path.exists(target_file_name): os.remove(target_file_name)

print(f"\nCalculation of angular variables configuration")
print(f"\n===> Executing calculations for parameters above")
ComputeVars()
print(f"===> done!\n")


end = time.time()
print( 'Compilation time in seconds: ', (end - start) )
print( '\n           ------ EXECUTION COMPLETED -----\n' )

