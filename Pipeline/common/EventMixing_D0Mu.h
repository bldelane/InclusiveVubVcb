// Header file for C++ functions to run the pyROOT script for EventMixing 
// Following from tutorial: header is declared to the
// ROOT C++ interpreter prior to the start of the analysis via the
// `ROOT.gInterpreter.Declare()` function.


#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "TCanvas.h"
#include "TH1D.h"
#include "TLatex.h"
#include "Math/Vector4D.h"
#include "TMath.h"
#include "TStyle.h"
#include "TH2F.h"
#include "TLorentzVector.h" 
#include "TVector3.h" 
#include "TVector2.h" 
#include "TRandom.h" 

using namespace ROOT::VecOps;
using FourVec = ROOT::Math::PxPyPzMVector;
using ThreeVec = ROOT::Math::XYZVector;
using RNode = ROOT::RDF::RNode;
using rvec_f = const RVec<double> &;
using rvec_i = const RVec<int> &;

// compute invariany mass from 2-daughter combinations 
auto Combine2Body(float d1_px, float d1_py, float d1_pz, float d1_M,
                       float d2_px, float d2_py, float d2_pz, float d2_M)
 {
//    ROOT::Math::PxPyPzMVector  p1(d1_px, d1_py, d1_pz, d1_M);
//    ROOT::Math::PxPyPzMVector  p2(d2_px, d2_py, d2_pz, d2_M);
//    return (p1+p2);
     TLorentzVector p1;
     TLorentzVector p2;
     p1.SetXYZM( d1_px, d1_py, d1_pz, d1_M); 
     p2.SetXYZM( d2_px, d2_py, d2_pz, d2_M);
     return (p1+p2);
 };

// lambda, this can exist now in the scope of a function and fed into a Map 
// iterator; see example from *Model* in RDataFrame tutorials page;
auto gen4Vec = [](float px, float py, float pz, float m){
    //return ROOT::Math::PxPyPzMVector(px, py, pz, m);
     TLorentzVector FourVec;
     FourVec.SetXYZM(px, py, pz, m);
     return FourVec;
};

auto gen3Vec = [](float x, float y, float z){
    //return ROOT::Math::XYZVector(x, y, z);
    TVector3 V;
    V.SetXYZ(x, y, z);
    return V;
};

auto subtract3Vec = [](auto SV, auto PV){
    TVector3 FlightV;
    FlightV = SV-PV;
    return FlightV;
};

auto ComputeMCORR = [](auto SV, auto PV, auto B){
    TVector3 FlightV;
    FlightV = SV-PV;
    auto Pperp = B.Perp(FlightV);
    return sqrt(Pperp*Pperp + B.M2()) + Pperp;
};



auto sampleRZ(TH2D* hist2d, double& PVX, double& PVY, double& PVZ)
{
    //interested in the fact that these guys must exist only within the scope of the function
    
    TVector3 SV;
    TVector3 PV;

    double R;
    double Z;
    hist2d->GetRandom2(R, Z);
    TRandom3 RND;
    RND.SetSeed(0);
    double angle = RND.Uniform(0, 2*TMath::Pi());
    double X = R * cos(angle);
    double Y = R * sin(angle);
  
    //ThreeVec SV; 
    //ThreeVec PV(PVX, PVY, PVZ);
    
    PV.SetXYZ(PVX, PVY, PVZ);
    SV = PV + TVector3(X, Y, Z);
    return SV;
};

auto GenerateSV = [](float K_ovx, float K_ovy, float K_ovz, float Mu_ovx, float Mu_ovy, float Mu_ovz, TLorentzVector& D0, TLorentzVector& Muon)
{
    
    TVector3 P1(K_ovx,  K_ovy,  K_ovz  );
    TVector3 P2(Mu_ovx, Mu_ovy, Mu_ovz );

    TVector3 D1;
    TVector3 D2;

    D1 = D0.Vect().Unit();
    D2 = Muon.Vect().Unit();
    
    TVector3 n1 = D1.Cross(D2.Cross(D1));
    TVector3 n2 = D2.Cross(D1.Cross(D2));


    TVector3 C1 = P1 + (P2-P1).Dot(n2)/(D1.Dot(n2)) * D1;
    TVector3 C2 = P2 + (P1-P2).Dot(n1)/(D2.Dot(n1)) * D2;

 	TVector3 SV; 
	SV = (C1 + C2) * 0.5;
    return SV;
};


auto ComputeMmiss2 = [] (TVector3& B_direction, TLorentzVector& B, TLorentzVector& D0, TLorentzVector& Mu)
{
    const double norm = sqrt(B_direction.Mag2());
    const double B_cosangle_X = B_direction.X()/norm;
    const double B_cosangle_Y = B_direction.Y()/norm;
    const double B_cosangle_Z = B_direction.Z()/norm;
    const double B_angle_Z = acos(B_cosangle_Z);

    // B(c) estimated momentum.
    const double B_M = 6275.6;//5279.61;//6275.6;
    const double observed_B_M = B.M();
    const double observed_B_PZ = B.Pz();
    const double B_P = B_M/observed_B_M*observed_B_PZ*sqrt(1.+tan(B_angle_Z)*tan(B_angle_Z));
    TLorentzVector B_PXYZE;
    B_PXYZE.SetPxPyPzE(B_P*B_cosangle_X,B_P*B_cosangle_Y,B_P*B_cosangle_Z,sqrt(B_P*B_P+B_M*B_M));

    TLorentzVector neu4mom;
    neu4mom = (B_PXYZE-D0-Mu);
    
    double Mmiss2;
    Mmiss2 = ( neu4mom.E()*neu4mom.E() - neu4mom.Px()*neu4mom.Px() - neu4mom.Py()*neu4mom.Py() - neu4mom.Pz()*neu4mom.Pz() );
    return Mmiss2;
};

auto Compute_B_EstMomentum = [] (TVector3& B_direction, TLorentzVector& B, TLorentzVector& D0, TLorentzVector& Mu) 
{
    const double norm = sqrt(B_direction.Mag2());
    const double B_cosangle_X = B_direction.X()/norm;
    const double B_cosangle_Y = B_direction.Y()/norm;
    const double B_cosangle_Z = B_direction.Z()/norm;
    const double B_angle_Z = acos(B_cosangle_Z);

    // B(c) estimated momentum.
    const double B_M = 6275.6;//5279.61;//6275.6;
    const double observed_B_M = B.M();
    const double observed_B_PZ = B.Pz();
    const double B_P = B_M/observed_B_M*observed_B_PZ*sqrt(1.+tan(B_angle_Z)*tan(B_angle_Z));

    return B_P;
};

auto ComputeEl = [] (TLorentzVector& B, TVector3& B_direction, TLorentzVector& mu)
{
    const double B_beta = B.P()/sqrt(B.P()*B.P()+B.M()*B.M());
    const double norm = sqrt(B_direction.Mag2());
    const double B_cosangle_X = B_direction.X()/norm;
    const double B_cosangle_Y = B_direction.Y()/norm;
    const double B_cosangle_Z = B_direction.Z()/norm;
    const double B_angle_Z = acos(B_cosangle_Z);

    const double bx = -B_beta*B_cosangle_X;
    const double by = -B_beta*B_cosangle_Y;
    const double bz = -B_beta*B_cosangle_Z;
    const double b2 = B_beta*B_beta;
    const double gamma = 1./::sqrt(1.-b2);
    
    const double bp = bx*mu.X()+by*mu.Y()+bz*mu.Z();
    const double gamma2 = b2>0?(gamma-1.)/b2:0.;

    const double mu_PXYZE_Brestframe_X = mu.X() + gamma2*bp*bx + gamma*bx*mu.E();
    const double mu_PXYZE_Brestframe_Y = mu.Y() + gamma2*bp*by + gamma*by*mu.E();
    const double mu_PXYZE_Brestframe_Z = mu.Z() + gamma2*bp*bz + gamma*bz*mu.E();
    const double mu_PXYZE_Brestframe_E = gamma*(mu.E() + bp);

    TLorentzVector mu_PXYZE_Brestframe;
    mu_PXYZE_Brestframe.SetPxPyPzE(mu_PXYZE_Brestframe_X,mu_PXYZE_Brestframe_Y,mu_PXYZE_Brestframe_Z,mu_PXYZE_Brestframe_E);

    const double El = mu_PXYZE_Brestframe.E();
    return El;
};

auto ComputeDIRA = [](auto Bflightdir, TLorentzVector& B)
{
    //cosine angle(B flight direction, B momentum)
    TVector3 unit_flightDir = Bflightdir.Unit();
    TVector3 Bmomentum = B.Vect();
    TVector3 unit_Bmom = Bmomentum.Unit();

    return unit_flightDir.Dot(unit_Bmom);
};

auto ComputeFD = [](auto BflightVec)
{
    return BflightVec.Mag();
};

auto GenerateCosXYD0Mu(float D0x, float D0y, float Mux, float Muy)
{
    TVector2 D02Vec(D0x, D0y);
    TVector2 Mu2Vec(Mux, Muy);
    auto CosXY_D0Mu = ( D02Vec.X()*Mu2Vec.X() + D02Vec.Y()*Mu2Vec.Y() )/ (D02Vec.Mod() * Mu2Vec.Mod() );
    return CosXY_D0Mu;
};


auto GenerateCosXYZ_hMu(float hx, float hy, float hz, float Mux, float Muy, float Muz)
{
    TVector3 h3Vec(hx, hy, hz);
    TVector3 Mu3Vec(Mux, Muy, Muz);
    auto CosXYZ_hMu = h3Vec.Dot(Mu3Vec)/(h3Vec.Mag()*Mu3Vec.Mag());
    return CosXYZ_hMu;
};


auto ComputeMCORRERR = [](auto SV, auto PV, auto B)
   {

    int nToy = 100;
    std::vector<double> MCORRv(nToy);
    
    TRandom3 RND;
    RND.SetSeed(0);

    for(int n=0; n<nToy; ++n)
    {

    TVector3 NewSV;

    //mean of RECO ERR
    NewSV.SetX(RND.Gaus(SV.X(), 0.022 ));//0.06169 ));
    NewSV.SetY(RND.Gaus(SV.Y(), 0.022 ));//0.01546));
    NewSV.SetZ(RND.Gaus(SV.Z(), 0.385 ));//0.01326 ));
    
    MCORRv[n] = ComputeMCORR(NewSV, PV, B);
    }
    
    
    double MCORRMu =0.0;
    for(auto mc: MCORRv)
      MCORRMu += mc;
    MCORRMu /= nToy;
    
    double Chisq = 0.0;
    for(auto mc: MCORRv)
      Chisq += (mc - MCORRMu)*(mc - MCORRMu);

    return sqrt(Chisq/nToy);

   };
