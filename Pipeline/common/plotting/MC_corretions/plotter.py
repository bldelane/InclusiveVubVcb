import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend, TLine, gPad
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml
from pathlib import Path
import pandas
from root_pandas import read_root
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import matplotlib
import uproot, pandas
plt.style.use(hep.style.LHCb)
matplotlib.rcParams['text.usetex'] = True
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)

# read v2 tuples only; JpsiMuNu MC built with StdNoPidsMuons (ie no isMuon applied); 
# compare different trigger strategies
eospath = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v2"
jpsimunu_16_df = pandas.DataFrame()
jpsipi_16_df = pandas.DataFrame()

branches_of_interest = ["*L0*MuonDecision_TOS", "B_plus_Hlt1*TOS", "B_plus_Hlt2*TOS", ]
plotting_branches = ["Jpsi_PT", "B_plus_PT", "B_plus_TRUETAU", "Mu_1_LK_ETA", "Mu_2_LK_ETA", "Mu_1_PT", "Mu_2_PT", "B_plus_LK_ETA"]
branches_of_interest+=plotting_branches


jpsipi_trigger_scheme = "( Jpsi_L0MuonDecision_TOS or Jpsi_L0DiMuonDecision_TOS ) and \
( ( B_plus_Hlt1TwoTrackMVADecision_TOS or B_plus_Hlt1TrackMuonMVADecision_TOS or B_plus_Hlt1TrackMVADecision_TOS or B_plus_Hlt1TrackMuonDecision_TOS ) or \
( B_plus_Hlt1TrackAllL0Decision_TOS or B_plus_Hlt1SingleMuonDecision_TOS ) ) and \
( ( B_plus_Hlt2Topo2BodyDecision_TOS or B_plus_Hlt2Topo3BodyDecision_TOS or B_plus_Hlt2Topo4BodyDecision_TOS or B_plus_Hlt2TopoMu2BodyDecision_TOS or \
B_plus_Hlt2TopoMu3BodyDecision_TOS or B_plus_Hlt2TopoMu4BodyDecision_TOS or B_plus_Hlt2SingleMuonDecision_TOS ) or\
( B_plus_Hlt2Topo2BodyBBDTDecision_TOS or B_plus_Hlt2Topo3BodyBBDTDecision_TOS or B_plus_Hlt2Topo4BodyBBDTDecision_TOS or B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS or \
B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS or B_plus_Hlt2TopoMu4BodyBBDTDecision_TOS ) )"    

jpsimunu_trigger_scheme = "( Mu_plus_L0MuonDecision_TOS ) and \
( ( B_plus_Hlt1TwoTrackMVADecision_TOS or B_plus_Hlt1TrackMuonMVADecision_TOS or B_plus_Hlt1TrackMVADecision_TOS or B_plus_Hlt1TrackMuonDecision_TOS ) or \
( B_plus_Hlt1TrackAllL0Decision_TOS or B_plus_Hlt1SingleMuonDecision_TOS ) ) and \
( ( B_plus_Hlt2Topo2BodyDecision_TOS or B_plus_Hlt2Topo3BodyDecision_TOS or B_plus_Hlt2Topo4BodyDecision_TOS or B_plus_Hlt2TopoMu2BodyDecision_TOS or \
B_plus_Hlt2TopoMu3BodyDecision_TOS or B_plus_Hlt2TopoMu4BodyDecision_TOS or B_plus_Hlt2SingleMuonDecision_TOS ) or\
( B_plus_Hlt2Topo2BodyBBDTDecision_TOS or B_plus_Hlt2Topo3BodyBBDTDecision_TOS or B_plus_Hlt2Topo4BodyBBDTDecision_TOS or B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS or \
B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS or B_plus_Hlt2TopoMu4BodyBBDTDecision_TOS ) )"    


for df in uproot.pandas.iterate([f"{eospath}/MC/JpsiMuNu/2016/MD/MC_Bc2JpsiMuNu_2016_MD_pidgen.root"], "DecayTree", branches=branches_of_interest, flatten=False):
    
    tmp_nominal_df = df.query(jpsimunu_trigger_scheme)
    jpsimunu_16_df = jpsimunu_16_df.append(tmp_nominal_df, ignore_index=True)
    tmp_jpsipi_df = df.query(jpsipi_trigger_scheme)
    jpsipi_16_df = jpsipi_16_df.append(tmp_jpsipi_df, ignore_index=True)


   
fig = plt.figure(figsize=[18,25])
fig.tight_layout()
        
ax = plt.subplot(4, 2, 1)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=25)
plt.xlabel(r'$p_T(B)$', fontsize=25)

plt.hist(jpsimunu_16_df["B_plus_PT"], histtype="step", color="steelblue", range=[0, 25000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["B_plus_PT"], histtype="step", color="crimson", range=[0, 25000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=15)



ax = plt.subplot(4, 2, 2)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=25)
plt.xlabel(r'$\eta(B)$', fontsize=25)

plt.hist(jpsimunu_16_df["B_plus_LK_ETA"], histtype="step", color="steelblue", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["B_plus_LK_ETA"], histtype="step", color="crimson", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=15)


ax = plt.subplot(4, 2, 3)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=25)
plt.xlabel(r'$\tau(B)$', fontsize=25)

plt.hist(jpsimunu_16_df["B_plus_TRUETAU"], histtype="step", color="steelblue", range=[0, 0.005], bins=30, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["B_plus_TRUETAU"], histtype="step", color="crimson", range=[0, 0.005], bins=30, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=15)

ax = plt.subplot(4, 2, 4)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=25)
plt.xlabel(r'$p_T(J/\psi)$', fontsize=25)

plt.hist(jpsimunu_16_df["Jpsi_PT"], histtype="step", color="steelblue", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["Jpsi_PT"], histtype="step", color="crimson", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=15)

ax = plt.subplot(4, 2, 5)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=20)
plt.tick_params(axis='both', which='minor', labelsize=20)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=25)
plt.xlabel(r'$\eta(\mu^{OS})$', fontsize=25)
plt.hist(jpsimunu_16_df["Mu_1_LK_ETA"], histtype="step", color="steelblue", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["Mu_1_LK_ETA"], histtype="step", color="crimson", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=16)

ax = plt.subplot(4, 2, 6)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=21)
plt.tick_params(axis='both', which='minor', labelsize=21)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=26)
plt.xlabel(r'$\eta(\mu^{SS})$', fontsize=26)
plt.hist(jpsimunu_16_df["Mu_2_LK_ETA"], histtype="step", color="steelblue", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["Mu_2_LK_ETA"], histtype="step", color="crimson", range=[1, 6], bins=12, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=16)

ax = plt.subplot(4, 2, 7)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=21)
plt.tick_params(axis='both', which='minor', labelsize=21)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=26)
plt.xlabel(r'$p_T(\mu^{OS})$', fontsize=26)
plt.hist(jpsimunu_16_df["Mu_1_PT"], histtype="step", color="steelblue", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["Mu_1_PT"], histtype="step", color="crimson", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=16)

ax = plt.subplot(4, 2, 8)
plt.minorticks_on()
plt.tick_params(axis='both', which='major', labelsize=21)
plt.tick_params(axis='both', which='minor', labelsize=21)
plt.ylabel(r'Events [Arbitrary Units]', fontsize=26)
plt.xlabel(r'$p_T(\mu^{SS})$', fontsize=26)
plt.hist(jpsimunu_16_df["Mu_2_PT"], histtype="step", color="steelblue", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $\mu$ TOS", density=True)
plt.hist(jpsipi_16_df["Mu_2_PT"], histtype="step", color="crimson", range=[0, 10000], bins=100, label=r"$B_c^{+}\rightarrow J/\psi\mu\nu$ 2016 MC, L0 $J/\psi$ TOS", density=True)
leg = plt.legend(loc='best', fontsize=16)

Path("PLOTS").mkdir(parents=True, exist_ok=True)
plt.savefig("PLOTS/L0_trig_diff.pdf")
