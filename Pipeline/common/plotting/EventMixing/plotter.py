#=========================================================================================================
# A (sloppy) master script to generate all possible plots pertinent to the event mixing section of the ANA
# __author__ : Blaise Delaney
# __email__ : blaise.delaney@gmail.com
#=========================================================================================================

#======================= CONFIG ================================
# --- activate/turn off plots ---
# modes + year
plotMixGBR = True
isD0MuNu = False
isJpsiMuNu = True
YEAR = 2016

# plot Cos shapes
plotCos = False

# plot D0 mass from data
plotD0M = False

# plot B_M vs B_MCORR
visMcut = False
#===============================================================

# --- PATHS ---
master_DATA_path = "/r01/lhcb/Bc2D0MuNuX/pipeline/DATA/combined"
master_MC_path = "/r01/lhcb/Bc2D0MuNuX/pipeline/MC/combined"  


import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend, TLine, gPad
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
import uproot, pandas, numpy 
from root_pandas import to_root
import os
from ROOT.TMath import ACos
import yaml
from pathlib import Path
import pandas
from root_pandas import read_root
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use(hep.style.LHCb)
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True

ROOT.gROOT.LoadMacro("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/lhcbStyle.C")
ROOT.gStyle.SetTitleXSize(0.07);
ROOT.gStyle.SetTitleYSize(0.07);
ROOT.gStyle.SetTitleXOffset(0.95);
ROOT.gStyle.SetTitleYOffset(1.1);
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.08);
ROOT.gStyle.SetOptStat(0);
# enable multithreading 
#ROOT.EnableImplicitMT() #---> maybe revisit this later?

RDF = ROOT.ROOT.RDataFrame

if plotMixGBR is True:
    if isD0MuNu is True:
        """Plots EvtMix+GBR shapes for D0MuNu with set ang cuts"""
    
        LABEL = f"D0MuNu{YEAR}"
    
# D0 M
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #angular cuts
        DATAang_cut_string = "CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_Pi_1<0.9997 && CosXYZ_Mu_plus_K_minus<0.9997"
        EVTMIXang_cut_string = "CosXY_Mu_plus_D0>-0.4 && CosXYZ_Mu_plus_K_minus<0.9997 && CosXYZ_Mu_plus_Pi_1<0.9997 && B_plus_SV_R<6. && B_plus_DIRA_OWNPV>0.999"
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        # upper mass data
        ORIGINAL_DATA_RDF = RDF("DecayTree", f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_sig.root").Filter(f"B_plus_M>6280 && {DATAang_cut_string}")
        # GBR schemes
        GBR_Bvars = RDF("DecayTree", f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_comb.root").Filter(f"B_plus_M>6280 && {EVTMIXang_cut_string}")
        
        
        def PlotD0MuNu(var, minvar, maxvar, binsvar, xlabelvar, pdfvar):
            c1 = TCanvas()
            
            DCS_shape = ORIGINAL_DATA_RDF.Filter("K_minus_ID*Mu_plus_ID>0").Histo1D(("h_data_DCS", "Upper data sideband DCS", binsvar, minvar, maxvar), var)
            DCS_shape.SetLineColor( kWhite)
            DCS_shape.SetMarkerColor( kOrange)
            DCS_shape.SetFillColorAlpha( kOrange, .3)
        
            CF_shape = ORIGINAL_DATA_RDF.Filter("K_minus_ID*Mu_plus_ID<0").Histo1D(("h_data_CF", "Upper data sideband CF", binsvar, minvar, maxvar), var)
            CF_shape.SetLineColor( kWhite)
            CF_shape.SetMarkerColor( kRed+1)
            CF_shape.SetFillColorAlpha( kRed+1, .3)
            
            evtmix_shape = GBR_Bvars.Histo1D(("hEvtMix", "Raw event mixing upper sideband", binsvar, minvar, maxvar), var)
            evtmix_shape.SetLineColor( kBlack)
            evtmix_shape.SetMarkerColor( kBlack)
        
            GBR_B_shape = GBR_Bvars.Histo1D(("hBvars", "GBR with B observables", binsvar, minvar, maxvar), var, "GBR_w")
            GBR_B_shape.SetLineColor( kAzure+2)
            GBR_B_shape.SetMarkerColor( kAzure+2)
        
            DCS_shape.Scale(1./DCS_shape.Integral())
            CF_shape.Scale(1./CF_shape.Integral())
            GBR_B_shape.Scale(1./GBR_B_shape.Integral())
            evtmix_shape.Scale(1./evtmix_shape.Integral())
        
            
            CF_shape.GetYaxis().SetTitle("Arbitrary Units")
            CF_shape.GetXaxis().SetTitle(xlabelvar)
            
            
            CF_shape.Draw("E2 SAME")
            DCS_shape.Draw("E2 SAME")
            evtmix_shape.Draw("EP SAME")
            GBR_B_shape.Draw("EP SAME")
            
            if var in ["D0_PT", "Mu_plus_PT", "D0_ETA", "Mu_plus_ETA", "B_plus_FD_OWNPV"]: 
                legend=TLegend(0.6,0.7,0.85,0.9)
                legend.SetTextFont(132)
                legend.SetTextSize(0.03)
                legend.AddEntry("h_data_DCS","DCS Data #it{m_{B}}>6280 MeV/c^{2}","epf")
                legend.AddEntry("h_data_CF","CF Data #it{m_{B}}>6280 MeV/c^{2}","epf")
                legend.AddEntry("hEvtMix", "EvtMix #it{m_{B}}>6280 MeV/c^{2}", "lep")
                legend.AddEntry("hBvars", "EvtMix+GBR #it{m_{B}}>6280 MeV/c^{2}", "lep")
                legend.Draw()
            
            Path(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/{LABEL}").mkdir(parents=True, exist_ok=True)
            c1.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/{LABEL}/"+pdfvar+".pdf")
        
        
        # Read YAML file
        with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/D0/PlotPars.yml", 'r') as stream:
                to_plot = yaml.safe_load(stream)
        
        for branch in to_plot.keys():
            
            print("===> Plotting ", branch)
            PlotD0MuNu(branch, **to_plot[branch])
        
    if isJpsiMuNu is True:
        """Plots EvtMix+GBR shapes for D0MuNu with set ang cuts"""
    
        LABEL = f"JpsiMuNu{YEAR}"
    
        ## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ##angular cuts
        DATAang_cut_string = " CosXY_Mu_plus_Jpsi > -0.8 && CosXYZ_Mu_plus_Mu_1 < 0.9997 && CosXYZ_Mu_plus_Mu_2 < 0.9997  "
        EVTMIXang_cut_string = " CosXY_Mu_plus_Jpsi > -0.8 && CosXYZ_Mu_plus_Mu_1 < 0.9997 && CosXYZ_Mu_plus_Mu_2 < 0.9997 && B_plus_SV_R<6. "
        ## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        # upper mass data
        ORIGINAL_DATA_RDF = RDF("DecayTree", f"{master_DATA_path}/JpsiMuNu/{YEAR}/Bc2JpsiMuNuX_sig.root").Filter(f"B_plus_M>6280 && {DATAang_cut_string}")
        # GBR schemes
# D0 M
        GBR_Bvars = RDF("DecayTree", f"{master_DATA_path}/JpsiMuNu/{YEAR}/Bc2JpsiMuNuX_comb.root").Filter(f"B_plus_M>6280 && {EVTMIXang_cut_string}")
        
        
        def PlotJpsiMuNu(var, minvar, maxvar, binsvar, xlabelvar, pdfvar):
            c1 = TCanvas()
            
            original_shape = ORIGINAL_DATA_RDF.Histo1D(("h_data_DCS", "Upper data sideband", binsvar, minvar, maxvar), var)
            original_shape.SetLineColor( kWhite)
            original_shape.SetMarkerColor( kGray+2)
            original_shape.SetFillColorAlpha( kGray+2, .3)
            
            evtmix_shape = GBR_Bvars.Histo1D(("hEvtMix", "Raw event mixing upper sideband", binsvar, minvar, maxvar), var)
            evtmix_shape.SetLineColor( kBlack)
            evtmix_shape.SetMarkerColor( kBlack)

            GBR_B_shape = GBR_Bvars.Histo1D(("hBvars", "GBR with B observables", binsvar, minvar, maxvar), var, "GBR_w")
            GBR_B_shape.SetLineColor( kRed)
            GBR_B_shape.SetMarkerColor( kRed)
        
            original_shape.Scale(1./original_shape.Integral())
            GBR_B_shape.Scale(1./GBR_B_shape.Integral())
            evtmix_shape.Scale(1./evtmix_shape.Integral())
        
            original_shape.GetYaxis().SetTitle("Arbitrary Units")
            original_shape.GetXaxis().SetTitle(xlabelvar)
            
            original_shape.Draw("E2  SAME")
            evtmix_shape.Draw("EP SAME")
            GBR_B_shape.Draw("EP SAME")
            
            if var in ["Jpsi_PT", "Mu_plus_PT", "Jpsi_ETA", "Mu_plus_ETA", "B_plus_FD_OWNPV"]: 
                legend=TLegend(0.6,0.7,0.85,0.9)
                legend.SetTextFont(132)
                legend.SetTextSize(0.03)
                legend.AddEntry("h_data_DCS","LHCb Data #it{m_{B}}>6280 MeV/c^{2}","epf")
                legend.AddEntry("hEvtMix", "EvtMix #it{m_{B}}>6280 MeV/c^{2}", "lep")
                legend.AddEntry("hBvars", "EvtMix+GBR #it{m_{B}}>6280 MeV/c^{2}", "lep")
                legend.Draw()
            
            Path(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/{LABEL}").mkdir(parents=True, exist_ok=True)
            c1.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/{LABEL}/"+pdfvar+".pdf")
        
        
        # Read YAML file
        with open("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/Jpsi/PlotPars.yml", 'r') as stream:
                to_plot = yaml.safe_load(stream)
        
        for branch in to_plot.keys():
            
            print("===> Plotting ", branch)
            PlotJpsiMuNu(branch, **to_plot[branch])


# D0 M
if plotD0M is True:
            columns = ROOT.std.vector("string")()
            for c in ("D0_M"):
                    columns.push_back(c)
    
            ORIGINAL_DATA_RDF = RDF("DecayTree", f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_sig.root", columns)
            DCS_D0_M = ORIGINAL_DATA_RDF.Filter("(K_minus_ID*Mu_plus_ID>0) && B_plus_M>6280").Histo1D(("h_D0M_DCS", "Upper data sideband DCS D0M", 50, 1790, 1940), "D0_M")
            CF_D0_M = ORIGINAL_DATA_RDF.Filter("(K_minus_ID*Mu_plus_ID<0) && B_plus_M>5400").Histo1D(("h_D0M_CF", "Upper data sideband CF D0M", 50, 1790, 1940), "D0_M")
            
            # CF D0 M
            c_D0M_FC = TCanvas()
            CF_D0_M.SetLineColor(kBlack)
            CF_D0_M.SetMarkerColor(kBlack)
            CF_D0_M.GetXaxis().SetTitle("#it{m(K#pi)} [MeV/c^{2}]")
            CF_D0_M.Draw("EP")

            legend=TLegend(0.6,0.7,0.9,0.9)
            legend.SetTextSize(0.03)
            legend.AddEntry("h_D0M_CF","CF 2016 Data #it{m_{B}}>5400 MeV/c^{2}","lep")
            legend.Draw()

            c_D0M_FC.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/D0_M_CF_highMB_{YEAR}.pdf")
            
            # DCS D0 M
            c_D0M_DCS = TCanvas()
            DCS_D0_M.SetLineColor(kBlack)
            DCS_D0_M.SetMarkerColor(kBlack)
            DCS_D0_M.GetXaxis().SetTitle("#it{m(K#pi)} [MeV/c^{2}]")
            DCS_D0_M.Draw("EP")
            
            
            legend=TLegend(0.6,0.7,0.9,0.9)
            legend.SetTextSize(0.03)
            legend.AddEntry("h_D0M_DCS","DCS 2016 Data #it{m_{B}}>6280 MeV/c^{2}","lep")
            legend.Draw()

            c_D0M_DCS.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/D0_M_DCS_highMB_{YEAR}.pdf")

    

# CosXY(Z) cuts
if plotCos is True:
            columns = ROOT.std.vector("string")()
            for c in ("CosXY_Mu_plus_D0", "CosXYZ_Mu_plus_Pi_1", "CosXYZ_Mu_plus_K_minus", "D0_M"):
                    columns.push_back(c)
    
            HIGHM_DATA_RDF = RDF("DecayTree", f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_sig.root", columns).Filter("B_plus_M>6280")
            D0_sideband_DATA_RDF = RDF("DecayTree", f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_sig.root", columns).Filter("D0_M<1865-35 || D0_M>1865+35")
            D0MuNu_MC_RDF = RDF("DecayTree", f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0MuNu.root", columns)
            D0gMuNu_MC_RDF = RDF("DecayTree", f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0gMuNu.root", columns)
            D0pi0MuNu_MC_RDF = RDF("DecayTree", f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0pi0MuNu.root", columns)
            
            # CosXY(Mu, D0) 
            c_CosXY_MuD0 = TCanvas()
            
            h_CosXY_Mu_D0_UpperM = HIGHM_DATA_RDF.Histo1D(("h_CosD0Mu_UpperM", "Upper data sideband CosMuD0", 20, -1, 1), "CosXY_Mu_plus_D0")
            h_CosXY_Mu_D0_UpperM.Scale(1./h_CosXY_Mu_D0_UpperM.Integral()) 
            h_CosXY_Mu_D0_UpperM.SetLineColor(kBlack)
            h_CosXY_Mu_D0_UpperM.SetMarkerColor(kBlack)
            h_CosXY_Mu_D0_UpperM.SetFillColorAlpha(kBlack, .2)
            
            h_CosXY_Mu_D0_D0sidebands = D0_sideband_DATA_RDF.Histo1D(("h_CosD0Mu_D0sidebands", "Dz sideband CosMuD0", 20, -1, 1), "CosXY_Mu_plus_D0")
            h_CosXY_Mu_D0_D0sidebands.Scale(1./h_CosXY_Mu_D0_D0sidebands.Integral()) 
            h_CosXY_Mu_D0_D0sidebands.SetLineColor(kTeal-8)
            h_CosXY_Mu_D0_D0sidebands.SetMarkerColor(kTeal-8)
            
            
            h_CosXY_Mu_D0_D0MuNuMC = D0MuNu_MC_RDF.Histo1D(("h_CosD0Mu_D0MuNuSim", "D0MuNu Sim CosMuD0", 20, -1, 1), "CosXY_Mu_plus_D0")
            h_CosXY_Mu_D0_D0MuNuMC.Scale(1./h_CosXY_Mu_D0_D0MuNuMC.Integral()) 
            h_CosXY_Mu_D0_D0MuNuMC.SetLineColor(kAzure)
            h_CosXY_Mu_D0_D0MuNuMC.SetMarkerColor(kAzure)
            h_CosXY_Mu_D0_D0MuNuMC.SetFillColorAlpha(kAzure, .3)
           
            h_CosXY_Mu_D0_D0gMuNuMC = D0gMuNu_MC_RDF.Histo1D(("h_CosD0Mu_D0gMuNuSim", "D0gMuNu Sim CosMuD0", 20, -1, 1), "CosXY_Mu_plus_D0")
            h_CosXY_Mu_D0_D0gMuNuMC.Scale(1./h_CosXY_Mu_D0_D0gMuNuMC.Integral()) 
            h_CosXY_Mu_D0_D0gMuNuMC.SetLineColor(kViolet+6)
            h_CosXY_Mu_D0_D0gMuNuMC.SetMarkerColor(kViolet+6)
            h_CosXY_Mu_D0_D0gMuNuMC.SetFillColorAlpha(kViolet+6, .3)

            h_CosXY_Mu_D0_D0pi0MuNuMC = D0pi0MuNu_MC_RDF.Histo1D(("h_CosD0Mu_D0pi0MuNuSim", "D0pi0MuNu Sim CosMuD0", 20, -1, 1), "CosXY_Mu_plus_D0")
            h_CosXY_Mu_D0_D0pi0MuNuMC.Scale(1./h_CosXY_Mu_D0_D0pi0MuNuMC.Integral()) 
            h_CosXY_Mu_D0_D0pi0MuNuMC.SetLineColor(kAzure+1)
            h_CosXY_Mu_D0_D0pi0MuNuMC.SetMarkerColor(kAzure+1)
            h_CosXY_Mu_D0_D0pi0MuNuMC.SetFillColorAlpha(kAzure+1, .3)

            
            h_CosXY_Mu_D0_D0sidebands.Draw("EP SAME")
            h_CosXY_Mu_D0_D0sidebands.GetXaxis().SetTitle("#it{cos_{XY}(D^{0}#mu^{+})}")
            h_CosXY_Mu_D0_UpperM.Draw("HIST  SAME")
            h_CosXY_Mu_D0_UpperM.Draw("EP  SAME")
            h_CosXY_Mu_D0_D0MuNuMC.Draw("EP SAME")
            h_CosXY_Mu_D0_D0gMuNuMC.Draw("EP SAME")
            h_CosXY_Mu_D0_D0pi0MuNuMC.Draw("EP SAME")
            
            c_CosXY_MuD0.Update()
            line = TLine(-0.4,gPad.GetUymin(),-0.4,gPad.GetUymax())
            line.SetLineStyle(9)
            line.SetLineColor(kRed)
            line.Draw()
            

            legend=TLegend(0.5,0.7,0.7,0.9)
            legend.SetTextFont(132)
            legend.SetTextSize(0.04)
            legend.AddEntry("h_CosD0Mu_UpperM","LHCb Data #it{m_{B}}>6280 MeV/c^{2}","lep")
            legend.AddEntry("h_CosD0Mu_D0sidebands","LHCb Data #it{D^{0}} sidebands","lep")
            legend.AddEntry("h_CosD0Mu_D0MuNuSim","#it{B_{c}#rightarrow D^{0}#mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosD0Mu_D0gMuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#gamma) #mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosD0Mu_D0pi0MuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#pi^{0}) #mu^{+}#nu} 2016 MC","epl")
            legend.Draw()

            c_CosXY_MuD0.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/D0MuNu_CosMuD0_{YEAR}.pdf")
            

            # CosXYZ(Mu,pi)
            c_CosXYZ_MuPi = TCanvas()
            
            h_CosXYZ_Mu_Pi_UpperM = HIGHM_DATA_RDF.Histo1D(("h_CosXYZMuPi_UpperM", "Upper data sideband CosXYZMuPi", 20, 0.998, 1.), "CosXYZ_Mu_plus_Pi_1")
            h_CosXYZ_Mu_Pi_UpperM.Scale(1./h_CosXYZ_Mu_Pi_UpperM.Integral()) 
            h_CosXYZ_Mu_Pi_UpperM.SetLineColor(kBlack)
            h_CosXYZ_Mu_Pi_UpperM.SetMarkerColor(kBlack)
            h_CosXYZ_Mu_Pi_UpperM.SetFillColorAlpha(kBlack, .2)
                          
            h_CosXYZ_Mu_Pi_D0sidebands = D0_sideband_DATA_RDF.Histo1D(("h_CosXYZMuPi_D0sidebands", "Dz sideband CosMuPi", 20, 0.998, 1.), "CosXYZ_Mu_plus_Pi_1")
            h_CosXYZ_Mu_Pi_D0sidebands.Scale(1./h_CosXYZ_Mu_Pi_D0sidebands.Integral()) 
            h_CosXYZ_Mu_Pi_D0sidebands.SetLineColor(kTeal-8)
            h_CosXYZ_Mu_Pi_D0sidebands.SetFillColorAlpha(kTeal-8,.3)
            h_CosXYZ_Mu_Pi_D0sidebands.SetMarkerColor(kTeal-8)
                          
                          
            h_CosXYZ_Mu_Pi_D0MuNuMC = D0MuNu_MC_RDF.Histo1D(("h_CosXYZMuPi_D0MuNuSim", "D0MuNu Sim CosMuPi", 20, 0.998, 1.), "CosXYZ_Mu_plus_Pi_1")
            h_CosXYZ_Mu_Pi_D0MuNuMC.Scale(1./h_CosXYZ_Mu_Pi_D0MuNuMC.Integral()) 
            h_CosXYZ_Mu_Pi_D0MuNuMC.SetLineColor(kAzure)
            h_CosXYZ_Mu_Pi_D0MuNuMC.SetMarkerColor(kAzure)
            h_CosXYZ_Mu_Pi_D0MuNuMC.SetFillColorAlpha(kAzure, .3)
                          
            h_CosXYZ_Mu_Pi_D0gMuNuMC = D0gMuNu_MC_RDF.Histo1D(("h_CosXYZMuPi_D0gMuNuSim", "D0gMuNu Sim CosMuPi", 20, 0.998, 1.), "CosXYZ_Mu_plus_Pi_1")
            h_CosXYZ_Mu_Pi_D0gMuNuMC.Scale(1./h_CosXYZ_Mu_Pi_D0gMuNuMC.Integral()) 
            h_CosXYZ_Mu_Pi_D0gMuNuMC.SetLineColor(kViolet+6)
            h_CosXYZ_Mu_Pi_D0gMuNuMC.SetMarkerColor(kViolet+6)
            h_CosXYZ_Mu_Pi_D0gMuNuMC.SetFillColorAlpha(kViolet+6, .3)
                          
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC = D0pi0MuNu_MC_RDF.Histo1D(("h_CosXYZMuPi_D0pi0MuNuSim", "D0pi0MuNu Sim CosXYZMuPi", 20, 0.998, 1.), "CosXYZ_Mu_plus_Pi_1")
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC.Scale(1./h_CosXYZ_Mu_Pi_D0pi0MuNuMC.Integral()) 
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC.SetLineColor(kAzure+1)
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC.SetMarkerColor(kAzure+1)
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC.SetFillColorAlpha(kAzure+1, .3)

            
            h_CosXYZ_Mu_Pi_D0sidebands.Draw("HIST SAME")
            h_CosXYZ_Mu_Pi_D0sidebands.Draw("EP SAME")
            h_CosXYZ_Mu_Pi_D0sidebands.GetXaxis().SetTitle("#it{cos_{XYZ}(#mu#pi)}")
            h_CosXYZ_Mu_Pi_UpperM.Draw("HIST  SAME")
            h_CosXYZ_Mu_Pi_UpperM.Draw("EP  SAME")
            h_CosXYZ_Mu_Pi_D0MuNuMC.Draw("EP SAME")
            h_CosXYZ_Mu_Pi_D0gMuNuMC.Draw("EP SAME")
            h_CosXYZ_Mu_Pi_D0pi0MuNuMC.Draw("EP SAME")
            
            c_CosXYZ_MuPi.Update()
            line = TLine(0.9997,gPad.GetUymin(),0.9997,gPad.GetUymax())
            line.SetLineStyle(9)
            line.SetLineColor(kRed)
            line.Draw()
            

            legend=TLegend(0.2,0.7,0.5,0.9)
            legend.SetTextFont(132)
            legend.SetTextSize(0.04)
            legend.AddEntry("h_CosXYZMuPi_UpperM","LHCb Data #it{m_{B}}>6280 MeV/c^{2}","lep")
            legend.AddEntry("h_CosXYZMuPi_D0sidebands","LHCb Data #it{D^{0}} sidebands","lep")
            legend.AddEntry("h_CosXYZMuPi_D0MuNuSim","#it{B_{c}#rightarrow D^{0}#mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosXYZMuPi_D0gMuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#gamma) #mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosXYZMuPi_D0pi0MuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#pi^{0}) #mu^{+}#nu} 2016 MC","epl")
            legend.Draw()

            c_CosXYZ_MuPi.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/D0MuNu_CosXYZMuPi_{YEAR}.pdf")
            
            
            # CosXYZ(Mu,K)
            c_CosXYZ_MuK = TCanvas()
            
            h_CosXYZ_Mu_K_UpperM = HIGHM_DATA_RDF.Histo1D(("h_CosXYZMuK_UpperM", "Upper data sideband CosXYZMuK", 20, 0.998, 1.), "CosXYZ_Mu_plus_K_minus")
            h_CosXYZ_Mu_K_UpperM.Scale(1./h_CosXYZ_Mu_K_UpperM.Integral()) 
            h_CosXYZ_Mu_K_UpperM.SetLineColor(kBlack)
            h_CosXYZ_Mu_K_UpperM.SetMarkerColor(kBlack)
            h_CosXYZ_Mu_K_UpperM.SetFillColorAlpha(kBlack, .2)
                        
            h_CosXYZ_Mu_K_D0sidebands = D0_sideband_DATA_RDF.Histo1D(("h_CosXYZMuK_D0sidebands", "Dz sideband CosMuK", 20, 0.998, 1.), "CosXYZ_Mu_plus_K_minus")
            h_CosXYZ_Mu_K_D0sidebands.Scale(1./h_CosXYZ_Mu_K_D0sidebands.Integral()) 
            h_CosXYZ_Mu_K_D0sidebands.SetLineColor(kTeal-8)
            h_CosXYZ_Mu_K_D0sidebands.SetFillColorAlpha(kTeal-8, .3)
            h_CosXYZ_Mu_K_D0sidebands.SetMarkerColor(kTeal-8)
                        
                        
            h_CosXYZ_Mu_K_D0MuNuMC = D0MuNu_MC_RDF.Histo1D(("h_CosXYZMuK_D0MuNuSim", "D0MuNu Sim CosMuK", 20, 0.998, 1.), "CosXYZ_Mu_plus_K_minus")
            h_CosXYZ_Mu_K_D0MuNuMC.Scale(1./h_CosXYZ_Mu_K_D0MuNuMC.Integral()) 
            h_CosXYZ_Mu_K_D0MuNuMC.SetLineColor(kAzure)
            h_CosXYZ_Mu_K_D0MuNuMC.SetMarkerColor(kAzure)
            h_CosXYZ_Mu_K_D0MuNuMC.SetFillColorAlpha(kAzure, .3)
                        
            h_CosXYZ_Mu_K_D0gMuNuMC = D0gMuNu_MC_RDF.Histo1D(("h_CosXYZMuK_D0gMuNuSim", "D0gMuNu Sim CosMuK", 20, 0.998, 1.), "CosXYZ_Mu_plus_K_minus")
            h_CosXYZ_Mu_K_D0gMuNuMC.Scale(1./h_CosXYZ_Mu_K_D0gMuNuMC.Integral()) 
            h_CosXYZ_Mu_K_D0gMuNuMC.SetLineColor(kViolet+6)
            h_CosXYZ_Mu_K_D0gMuNuMC.SetMarkerColor(kViolet+6)
            h_CosXYZ_Mu_K_D0gMuNuMC.SetFillColorAlpha(kViolet+6, .3)
                        
            h_CosXYZ_Mu_K_D0pi0MuNuMC = D0pi0MuNu_MC_RDF.Histo1D(("h_CosXYZMuK_D0pi0MuNuSim", "D0pi0MuNu Sim CosXYZMuK", 20, 0.998, 1.), "CosXYZ_Mu_plus_K_minus")
            h_CosXYZ_Mu_K_D0pi0MuNuMC.Scale(1./h_CosXYZ_Mu_K_D0pi0MuNuMC.Integral()) 
            h_CosXYZ_Mu_K_D0pi0MuNuMC.SetLineColor(kAzure+1)
            h_CosXYZ_Mu_K_D0pi0MuNuMC.SetMarkerColor(kAzure+1)
            h_CosXYZ_Mu_K_D0pi0MuNuMC.SetFillColorAlpha(kAzure+1, .3)

            
            h_CosXYZ_Mu_K_D0sidebands.Draw("HIST SAME")
            h_CosXYZ_Mu_K_D0sidebands.Draw("EP SAME")
        
            h_CosXYZ_Mu_K_D0sidebands.GetXaxis().SetTitle("#it{cos_{XYZ}(#mu K)}")
            h_CosXYZ_Mu_K_UpperM.Draw("HIST  SAME")
            h_CosXYZ_Mu_K_UpperM.Draw("EP  SAME")
            h_CosXYZ_Mu_K_D0MuNuMC.Draw("EP SAME")
            h_CosXYZ_Mu_K_D0gMuNuMC.Draw("EP SAME")
            h_CosXYZ_Mu_K_D0pi0MuNuMC.Draw("EP SAME")
            
            c_CosXYZ_MuK.Update()
            line = TLine(0.9997, gPad.GetUymin(), 0.9997 ,gPad.GetUymax())
            line.SetLineStyle(9)
            line.SetLineColor(kRed)
            line.Draw()
            

            legend=TLegend(0.2,0.7,0.5,0.9)
            legend.SetTextFont(132)
            legend.SetTextSize(0.04)
            legend.AddEntry("h_CosXYZMuK_UpperM","LHCb Data #it{m_{B}}>6280 MeV/c^{2}","lep")
            legend.AddEntry("h_CosXYZMuK_D0sidebands","LHCb Data #it{D^{0}} sidebands","lep")
            legend.AddEntry("h_CosXYZMuK_D0MuNuSim","#it{B_{c}#rightarrow D^{0}#mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosXYZMuK_D0gMuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#gamma) #mu^{+}#nu} 2016 MC","epl")
            legend.AddEntry("h_CosXYZMuK_D0pi0MuNuSim","#it{B_{c}#rightarrow D^{*}(#rightarrow D^{0}#pi^{0}) #mu^{+}#nu} 2016 MC","epl")
            legend.Draw()

            c_CosXYZ_MuK.Print(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/D0MuNu_CosXYZMuK_{YEAR}.pdf")


if visMcut is True:
    
        columns=["B_plus_M", "B_plus_MCORR", "FitVar_Mmiss2", "K_minus_ID", "Mu_plus_ID"]
        DATA16 = read_root(f"{master_DATA_path}/D0MuNu/{YEAR}/Bc2D0MuNuX_sig.root", columns=columns)
        D0MC = read_root(f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0MuNu.root", columns=columns) 
        D0gMC = read_root(f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0gMuNu.root", columns=columns) 
        D0pi0MC = read_root(f"{master_MC_path}/D0MuNu/{YEAR}/Bc2D0pi0MuNu.root", columns=columns) 

        my_cmap = plt.cm.plasma
        my_cmap.set_under('w',1)
        
        fig = plt.figure(figsize=[20,15])
        fig.tight_layout()
        
        ax = plt.subplot(2, 2, 1)
        plt.minorticks_on()
        plt.tick_params(axis='both', which='major', labelsize=20)
        plt.tick_params(axis='both', which='minor', labelsize=20)
        plt.text(5000, 3500, r'\textbf{LHCb 2016 DCS Data}', fontsize=25) 
        plt.ylabel(r'Corrected Mass [Mev/c$^{2}$]', fontsize=25)
        plt.xlabel(r'Visible mass $m(D^{0}\mu^{+}$) [Mev/c$^{2}$]', fontsize=25)
        
        
        _ = plt.hist2d(DATA16.query("K_minus_ID*Mu_plus_ID>0")["B_plus_M"], 
                       DATA16.query("K_minus_ID*Mu_plus_ID>0")["B_plus_MCORR"],  
                       bins=[100,100], range=[[2500, 8000], [2500, 8000] ], 
                       cmap=my_cmap, vmin=1)
        
        plt.axvline(x=3500, color="red", linestyle="--", lw=2, label = r"$m(D^{0}\mu^{+}) = 3500$ Mev/c$^{2}$")
        
        
        leg = plt.legend(loc='lower right', fontsize=25)
        
        
        ax = plt.subplot(2, 2, 2)
        plt.minorticks_on()
        plt.tick_params(axis='both', which='major', labelsize=20)
        plt.tick_params(axis='both', which='minor', labelsize=20)
        plt.text(5000, 3500, r'\textbf{$B_{c}^{+}\rightarrow D^{0}\mu\nu$ 2016 MC}', fontsize=25) 
        plt.ylabel(r'Corrected Mass [Mev/c$^{2}$]', fontsize=25)
        plt.xlabel(r'Visible mass $m(D^{0}\mu^{+}$) [Mev/c$^{2}$]', fontsize=25)
        
        _ = plt.hist2d(D0MC["B_plus_M"], 
                       D0MC["B_plus_MCORR"],  
                       bins=[100,100], range=[[2500, 8000], [2500, 8000] ], 
                       cmap=my_cmap, vmin=1)
        
        
        plt.axvline(x=3500, color="red", linestyle="--", lw=2, label = r"$m(D^{0}\mu^{+}) = 3500$ Mev/c$^{2}$")
        
        leg = plt.legend(loc='lower right', fontsize=25)
        
        ax = plt.subplot(2, 2, 3)
        plt.minorticks_on()
        plt.tick_params(axis='both', which='major', labelsize=20)
        plt.tick_params(axis='both', which='minor', labelsize=20)
        plt.text(5000, 3500, r'\textbf{$B_{c}^{+}\rightarrow [D^{0}\gamma]_{D^{*0}}\mu\nu$ 2016 MC}', fontsize=25) 
        plt.ylabel(r'Corrected Mass [Mev/c$^{2}$]', fontsize=25)
        plt.xlabel(r'Visible mass $m(D^{0}\mu^{+}$) [Mev/c$^{2}$]', fontsize=25)
        
        _ = plt.hist2d(D0gMC["B_plus_M"], 
                       D0gMC["B_plus_MCORR"],  
                       bins=[100,100], range=[[2500, 8000], [2500, 8000] ], 
                       cmap=my_cmap, vmin=1)
        
        
        plt.axvline(x=3500, color="red", linestyle="--", lw=2, label = r"$m(D^{0}\mu^{+}) = 3500$ Mev/c$^{2}$")
        
        leg = plt.legend(loc='lower right', fontsize=25)
        
        ax = plt.subplot(2, 2, 4)
        plt.minorticks_on()
        plt.tick_params(axis='both', which='major', labelsize=20)
        plt.tick_params(axis='both', which='minor', labelsize=20)
        plt.text(5000, 3500, r'\textbf{$B_{c}^{+}\rightarrow[ D^{0}\pi^{0}]_{D^{*0}}\mu\nu$ 2016 MC}', fontsize=25) 
        plt.ylabel(r'Corrected Mass [Mev/c$^{2}$]', fontsize=25)
        plt.xlabel(r'Visible mass $m(D^{0}\mu^{+}$) [Mev/c$^{2}$]', fontsize=25)
        
        _ = plt.hist2d(D0pi0MC["B_plus_M"], 
                       D0pi0MC["B_plus_MCORR"],  
                       bins=[100,100], range=[[2500, 8000], [2500, 8000] ], 
                       cmap=my_cmap, vmin=1)
        
        
        plt.axvline(x=3500, color="red", linestyle="--", lw=2, label = r"$m(D^{0}\mu^{+}) = 3500$ Mev/c$^{2}$")
        
        leg = plt.legend(loc='lower right', fontsize=25)

        plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/plotting/EventMixing/PLOTS/visMcut_{YEAR}.pdf")


