# ===== selection-dedicated rules =====

configfile: "/usera/delaney/Bc2D0MuNuX/Pipeline/CONFIG/config.yml"
PREFIX_MU, = XRootD.glob_wildcards(config['topdir']+"/v1/DATA/"+config['channel']+"/"+config['year']+"/"+"MU/dv_tuples/{prefix}_"+config['namespace']+"Slim.root")
PREFIX_MD, = XRootD.glob_wildcards(config['topdir']+"/v1/DATA/"+config['channel']+"/"+config['year']+"/"+"MD/dv_tuples/{prefix}_"+config['namespace']+"Slim.root")
#chain for correct format in blacklisted operator
PREFIX = list(chain(PREFIX_MU, PREFIX_MD))


def filter_combinator(combinator):
    def filtered_combinator(*args, **kwargs):
        for PL in combinator(*args, **kwargs):
            """blacklist mismatch magpol/lfn"""
            if (PL[2][1] == 'MU' and PL[3][1] in PREFIX_MU) or (PL[2][1] == 'MD' and PL[3][1] in PREFIX_MD):  
                yield(PL)
    return filtered_combinator

# product operator in expand with check for mismatch 
filtered_product = filter_combinator(product)

# load branches to propagate until offline selection
# here apply preselection (trigger and basic cuts)
rule apply_preselection:
    input:
        XRootD.remote("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v1/DATA/{channel}/{year}/{magpol}/dv_tuples/{prefix}_{mode}Slim.root")
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['preselection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
        #fakemuon_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['fakemuon']['preselection_config'], 
        #fakemuon_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['fakemuon']['tupledir'] 
    benchmark:
        "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Benchmarks/{channel}_{year}_{magpol}_{prefix}_{mode}_apply_preselection.txt"
    output:
        protected("/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_{prefix}_{mode}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyPreselection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"

# add events with Mu_PIDmu<X to misID
rule boost_misID_stats_with_PIDmu:
    input:
        "/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_{prefix}_{mode}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
    benchmark:
        "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Benchmarks/{channel}_{year}_{magpol}_{prefix}_{mode}_boost_misID_with_PIDmu.txt"
    output:
        temp("/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_misIDboosted_{prefix}_{mode}.root")
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/BoostMisIDstats.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection} "

# compute angular variables
rule compute_angvars:
    input:
        "/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_misIDboosted_{prefix}_{mode}.root"
    params:
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
        fakemuon_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['fakemuon']['tupledir'] 
    benchmark:
        "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Benchmarks/{channel}_{year}_{magpol}_{prefix}_{mode}_compute_angvars.txt"
    output:
        temp("/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_angvars_{prefix}_{mode}.root")
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ComputeAngularVariables.py --input {input} --output {output}  --dirs {params.signal_tupledir} {params.fakemuon_tupledir}"

# apply offline selection minus mass cuts and angular cuts
rule apply_offline_selection_stage_one:
    input:
        "/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/presel_angvars_{prefix}_{mode}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['selection_config'], 
        fakemuon_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['fakemuon']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
        fakemuon_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['fakemuon']['tupledir'] 
    benchmark:
        "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Benchmarks/{channel}_{year}_{magpol}_{prefix}_{mode}_apply_selection_stage_one.txt"
    output:
        "/r01/lhcb/delaney/FIT/{channel}/{year}/{magpol}/sel_stage1_{prefix}_{mode}.root"
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        # *NOTE* here apply the same selection to signal and misID
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyOfflineSelection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection} {params.fakemuon_tupledir},{params.fakemuon_selection}"


#rule apply_MVA:
#    input:
#       "/r01/lhcb/delaney/FIT/{year}/{magpol}/sel_stage1_{prefix}_{mode}.root"
#    params:
#        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'], 
#        fakemuon_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['fakemuon']['tupledir'] 
#    benchmark:
#        "/usera/delaney/private/Bc2D0MuNuX/Pipeline/Benchmarks/{year}_{magpol}_{prefix}_{mode}_apply_MVA.txt"
#    output:
#        temp("/r01/lhcb/delaney/FIT/{year}/{magpol}/fit_{prefix}_{mode}.root")
#    shell:
#        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyMVA.py  --input {input} --output {output} --mode {config[channel]} --dirs {params.signal_tupledir} {params.fakemuon_tupledir} "



# === merge to reweight via GBR ===
rule prepare_merge_executable:
    input:
        expand("/r01/lhcb/delaney/FIT/{channel}/{year}/{magpols}/sel_stage1_{prefix}_{mode}.root", filtered_product, channel=config['channel'], year=config['year'], magpols=['MU', 'MD'], prefix=PREFIX, mode=config['namespace']) 
    params:
        mergedfile ="/r01/lhcb/delaney/FIT/"+config['channel']+"/"+config['year']+"/"+"merged_"+config['year']+"_"+config['namespace']+".root",   
    output:
        execsh = temp("hadd-exec.sh")
    shell:
        # gosh, this is ugly
        "echo export PATH=$(getconf PATH) >>{output}\n"
        "echo export HOME=/usera/delaney >> {output}\n"
        "echo . /lhcb/scripts/lhcb-setup.sh >> {output}\n"
        "echo lb-run DaVinci/latest hadd -fk {params.mergedfile} {input} >> {output}\n"

rule merge:
    input:
        execsh = "hadd-exec.sh"
    output:
        rules.prepare_merge_executable.params.mergedfile
    run:
        import subprocess
        import os 
        subprocess.call([ "bash", input.execsh ])

rule event_mix:
    input:
        #"/r01/lhcb/delaney/FIT/JpsiMunu/2016/merged_2016_Bc2JpsiMuNuX.root"
        rules.merge.output
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
    output:
        temp("/r01/lhcb/delaney/FIT/{channel}/{year}/raw_evtmix_{prefix}_{mode}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        # *NOTE* here apply the same selection to signal and misID
        # concerns only sig->evtmixed dir in same file
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMix.py --input {input} --output {output} --dirs {params.signal_tupledir}"

rule compute_comb_observables:
    input:
        mixedfile = "/r01/lhcb/delaney/FIT/{channel}/{year}/raw_evtmix_{prefix}_{mode}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/{channel}/{year}/evtmix_{prefix}_{mode}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/GenerateCombinatorialObservables.py --input {input.mixedfile} --output {output} --dirs {params.signal_tupledir} "

# GBReweighting
rule GBReweight:
    input:
        "/r01/lhcb/delaney/FIT/{channel}/{year}/evtmix_{prefix}_{mode}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
        mergedfile = rules.merge.output  
    output:
        protected("/r01/lhcb/delaney/FIT/{channel}/{year}/GBR_{prefix}_{mode}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/Reweighter.py --input {input} --merged-file {params.mergedfile} --output {output} --dirs {params.signal_tupledir}"

# Selection stage II + fitvars
rule forFit:
    input:
        evtmix = "/r01/lhcb/delaney/FIT/{channel}/{year}/GBR_{prefix}_{mode}.root",
        sig = rules.merge.output
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.mode][ config['channel'] ]['signal']['final_selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/{channel}/{year}/FIT_{prefix}_{mode}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/PrepareFittingSamples.py --input {input.sig} --combinatorial {input.evtmix} --output {output} --dirs {params.signal_tupledir} --sel {params.signal_selection}"

