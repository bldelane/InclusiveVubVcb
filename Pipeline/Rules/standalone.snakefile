# ==== standalone rules for Condor interface =====

rule runReweighter:
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Reweighter.py -b"

rule runEvtMixer:
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/EventMixing_D0Mu.py"

rule runEvtMixPlotter:
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/EventMixing/Plotter.py -b"  

rule runSWeights:
    #output:
    #    "/usera/delaney/private/Bc2D0MuNuX/Fitter/Bc2D0MuNu_DATA_2016_wsw.root"
    shell:
        "root -l -q -b /usera/delaney/private/Bc2D0MuNuX/Fitter/SWeights.C"

rule runPrepareHistograms:
    #output:
    #    "/usera/delaney/private/Bc2D0MuNuX/Fitter/Bc2D0MuNu_DATA_2016_wsw.root"
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Fitter/PrepareHistograms.py -b"

rule runSelectionLocally:
    input:
        "/r01/lhcb/delaney/gangadir/workspace/delaney/LocalXML/DATA16_Bc2JpsiMuNu.root"
    params:
        signal_selection = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/SelectionConfigs/noL0_Bc2JpsiMuNuX.yml", 
        signal_tupledir = "TupleBc2JpsiMuNu_Jpsi2MuMu" 
    output:
        protected("/r01/lhcb/delaney/FIT/TriggerStudy/DATA16_Bc2JpsiMuNu.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplySelection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"# {params.fakemuon_tupledir},{params.fakemuon_selection}"

# ======================================================================================================================

# ***** RULES TO PREPARE SWEIGHTED DATA FOR MVA TRAINING 

# step 1: apply offline selection w/o angular variables (and no constraint on B_plus_M)

# apply offline selection minus B reco mass cuts and IPCHI2 cuts (for later, MVA stage against real D0 in combinatorial)
# apply to sweighted data to create a sample for training the MVA; need to tag DSC event to make the process fast!
rule apply_offline_selection_forMVA:
    input:
       XRootD.remote("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/Blaise/Bc2D0MuNu_DATA_2016_wsw.root") 
    params:
        signal_tupledir = "B2DMuNuX_D02KPi",
        signal_selection = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/SelectionConfigs/Offline_Selection/forMVAtraining/trainMVA_Bc2D0MuNuX_AllCabibbo_NOANGVARS.yml"
    output:
        protected("/r01/lhcb/delaney/FIT/trainMVA_noangvars_Bc2D0MuNu_DATA_2016_wsw.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplySelection_NODIR.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"


# compute angular variables
rule add_angvars:
    input:
        "/r01/lhcb/delaney/FIT/trainMVA_noangvars_Bc2D0MuNu_DATA_2016_wsw.root"
    params:
        signal_tupledir = "B2DMuNuX_D02KPi"
    output:
        protected("/r01/lhcb/delaney/FIT/trainMVA_angvars_Bc2D0MuNu_DATA_2016_wsw.root")
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ComputeAngularVariables.py --input {input} --output {output}  --dirs {params.signal_tupledir}"

# apply cuts on angvars
rule apply_full_selection_forMVA:
    input:
        "/r01/lhcb/delaney/FIT/trainMVA_angvars_Bc2D0MuNu_DATA_2016_wsw.root"
    params:
        signal_tupledir = "B2DMuNuX_D02KPi",
        signal_selection = "/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/SelectionConfigs/Offline_Selection/forMVAtraining/trainMVA_Bc2D0MuNuX_AllCabibbo_ONLYANGVARS.yml"
    output:
        protected("/r01/lhcb/delaney/FIT/trainMVA_Bc2D0MuNu_DATA_2016_wsw.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyOfflineSelection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"


# ======================================================================================================================

