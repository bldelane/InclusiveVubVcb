# ===== SIMULATION WORKFLOW =====
configfile: "/usera/delaney/Bc2D0MuNuX/Pipeline/CONFIG/config.yml"

#truthmatching
rule truthmatch_simulation:
    input:
        XRootD.remote("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/fit_input_files/Blaise/raw_MCFILES/"+config['channel']+"/{year}/MC_{decay}.root")
    params:
        truthmatching_config = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][ config['channel'] ]['signal']['truthmatching_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
    output:
        temp("/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Truthmatched_MC_{decay}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyPreselection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.truthmatching_config}"

# compute angular variables
rule apply_preselection_to_simulation:
    input:
        "/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Truthmatched_MC_{decay}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][ config['channel'] ]['signal']['preselection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Preselection_Applied_MC_{decay}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyPreselection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"

# compute angular variables
rule compute_angvars_in_simulation:
    input:
        "/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Preselection_Applied_MC_{decay}.root"
    params:
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Computed_AngularVariables_MC_{decay}.root")
    shell:
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ComputeAngularVariables.py --input {input} --output {output}  --dirs {params.signal_tupledir}"

# apply offline selection minus D0 mass cuts and IPCHI2 cuts (for later, MVA stage against real D0 in combinatorial)
rule apply_offline_selection_stage_one_to_simulation:
    input:
        "/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Computed_AngularVariables_MC_{decay}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][ config['channel'] ]['signal']['selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Selection_noMassCuts_MC_{decay}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyOfflineSelection.py --input {input} --output {output} --mode {config[channel]} --pars {params.signal_tupledir},{params.signal_selection}"

# patch: apply mass cuts
# Selection stage II + fitvars
rule forFit_to_simulation:
    input:
        "/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/Selection_noMassCuts_MC_{decay}.root"
    params:
        signal_selection = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][ config['channel'] ]['signal']['final_selection_config'], 
        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
    output:
        protected("/r01/lhcb/delaney/FIT/MC/{sim_mode}/{year}/MCFIT_MC_{decay}.root")
    shell:
        # supply dir and sel as pair as follows: --pars dir,sel (i.e. comma-separated)
        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/Prepare_Simulation_FittingSamples.py --input {input} --output {output} --dirs {params.signal_tupledir} --sel {params.signal_selection}"


#rule apply_MVA_to_simulation:
#    input:
#        "/r01/lhcb/delaney/FIT/MC/{year}/simsel_stage1_TruthTrig_MC_{sim_mode}.root"
#    params:
#        signal_tupledir = lambda wildcards: config['channel_namespace'][wildcards.sim_mode][config['channel']]['signal']['tupledir'],  
#    output:
#        protected("/r01/lhcb/delaney/FIT/MC/{year}/fit_MC_{sim_mode}.root")
#    shell:
#        "python /usera/delaney/private/Bc2D0MuNuX/Pipeline/Executables/ApplyMVA.py  --input {input} --output {output} --mode {config[channel]} --dirs {params.signal_tupledir}"

