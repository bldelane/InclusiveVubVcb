import os
os.system('mkdir -p mds')
os.system('mkdir -p jsons')
import sys
files = sys.argv[1:]
import json
from tabulate import tabulate

tab_rows = [] # for printed output
out_dict = {} # for .py output

for fil in files:
  
  with open(fil,'rb') as f:
    cfg = json.load(f)

  evtype = cfg["evtType"]
  evdesc = cfg["evtTypeDesc"]
  year   = cfg["APPCONFIG_file"].split('-')[2]
  pol    = cfg["APPCONFIG_file"].split('-')[3]
  channel = 'None'
  if 'Bc_JpsiMuNu' in evdesc: channel = 'Bc2JpsiMuNu'
  if 'Bc_D0munu' in evdesc: channel = 'Bc2D0MuNu'
  if 'Bc_D0munu,Kpipipi' in evdesc: channel = 'Bc2D0MuNu_K3Pi'
  if 'Bc_Dst0munu,D0pi0' in evdesc: channel = 'Bc2D0pi0MuNu'
  if 'Bc_Dst0munu,D0g' in evdesc: channel = 'Bc2D0gMuNu'
  if 'Bu_D0munu,Kpi' in evdesc: channel = 'Bu2D0MuNu'
  if 'ffKiselev' in evdesc: channel += '_Kis'

  eff = cfg["Hadron Counters"][0]["value"]
  err = cfg["Hadron Counters"][0]["error"]

  tab_rows.append( [ evtype, evdesc, channel, year, pol, eff, err] )

  if channel not in out_dict.keys(): out_dict[channel] = {}
  if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
  if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = { 'eff': eff, 'err': err }

print(tabulate(tab_rows,headers=['EventType','Desc','Channel','Year','Pol','Eff','Err']))

with open('mds/GeneratorLevel.md','w') as f:
  f.write(tabulate(tab_rows,headers=['EventType','Desc','Channel','Year','Pol','Eff','Err'],tablefmt='github'))

with open('jsons/GeneratorLevel.json','w') as f:
  json.dump(out_dict, f, indent=2)
