from argparse import ArgumentParser
parser = ArgumentParser(description='Efficiency Calculator')
parser.add_argument("-y","--years",default=[],action="append",help='Years')
parser.add_argument("-c","--channels",default=[],action="append",help='Channels')
opts = parser.parse_args()

if len(opts.years)==0: opts.years = ['2011','2012','2015','2016','2017','2018']
if len(opts.channels)==0: opts.channels = ['D0MuNu','JpsiMuNu']

import os
from XRootD import client
from XRootD.client.flags import OpenFlags

run_sum = {}
for chan in opts.channels:
  run_sum[chan] = { 'run1' : [0.,0.], 'run2' : [0.,0.] }
  for year in opts.years:
    for pol in ['MU','MD']:
      path = os.path.join('root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v2/DATA/',chan,year,pol,'lumi.log')
      with client.File() as f:
        f.open(path, OpenFlags.READ)
        status, data = f.read()
        lines = data.decode("utf-8").splitlines()
        for line in lines:
          if line.startswith("TOTAL"):
            print('{:8s}'.format(chan), year, pol,  line)
            els = line.split()
            nfiles = int(els[2])
            lumi    = float(els[5])
            lumierr = float(els[7].strip(')pb'))

            if year in ['2011','2012']: run = 'run1'
            if year in ['2015','2016','2017','2018']: run = 'run2'

            run_sum[chan][run][0] += lumi
            run_sum[chan][run][1] += lumierr

for chan, runcfg in run_sum.items():
  for run, lum in runcfg.items():
    print( '{:8s}'.format(chan), run, lum[0], lum[1] )


