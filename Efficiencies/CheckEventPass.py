import sys
fname = sys.argv[1]

import ROOT as r
tf = r.TFile(fname)
tree = tf.Get("B2DMuNuX_D02KPiTuple/DecayTree")
tree.SetBranchStatus("*",0)
tree.SetBranchStatus("eventNumber",1)
tree.SetBranchStatus("runNumber",1)

evs = tree.GetEntries()

enSet = set()
rnSet = set()

for ev in range(evs):
  tree.GetEntry(ev)
  enSet.add( tree.eventNumber )
  rnSet.add( tree.runNumber )

print( len(enSet), len(rnSet) )
tf.Close()
