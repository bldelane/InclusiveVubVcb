import os
import ROOT as r
import sys
files = sys.argv[1:]

lumi = 0.
lumiErr = 0.
for f in files:
  tf = r.TFile(f)
  t = tf.Get('GetIntegratedLuminosity/LumiTuple')
  lumif = 0.
  lumifErr = 0.
  for ev in range(t.GetEntries()):
    t.GetEntry(ev)
    lumif += t.IntegratedLuminosity
    lumifErr += t.IntegratedLuminosityErr
    lumi += lumif
    lumiErr += lumifErr
  tf.Close()
  print(' {:50s} : ({:4.1f} +/- {:-3.1f})pb'.format(os.path.basename(f),lumif,lumifErr))

print 'TOTAL ({:4d} files): ({:6.1f} +/- {:-5.1f})pb'.format(len(files),lumi, lumiErr)

