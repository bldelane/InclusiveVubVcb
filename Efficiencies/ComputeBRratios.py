import uncertainties as u

Dz_Kpi_CF  = u.ufloat( 3.950e-2, 0.031e-2)
Dz_Kpi_DCS = u.ufloat( 1.364e-4, 0.026e-4)
Jpsi_mumu  = u.ufloat( 5.961e-2, 0.033e-2)

print('Ratio of Dz_CF  / Jpsi_mumu =', Dz_Kpi_CF/Jpsi_mumu)
print('Ratio of Dz_CF  / Dz_DCS    =', Dz_Kpi_CF/Dz_Kpi_DCS)
print('Ratio of Dz_DCS / Dz_CF     =', Dz_Kpi_DCS/Dz_Kpi_CF)
print('Ratio of Dz_DCS / Jpsi_mumu =', Dz_Kpi_DCS/Jpsi_mumu)
