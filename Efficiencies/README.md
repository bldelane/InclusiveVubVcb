# Efficiencies

This section contains instructions for computing the efficiencies of:
1. [Generator Level](#generate-mc-stats-tables)
2. [DaVinci](#generate-davinci-stats)
3. [Selection](#compute-selection-efficiencies)
4. [Total Efficiency](#compute-the-total-efficiency)
5. [Getting the Luminosity](#compute-the-luminosities)

### Generate MC stats tables

This should be run on lxplus.

This should only need to be done once ever and the stats tables should be created by the MC liaison. I ended up running some missing ones myself. I also found some problems with the `JpsiTauNu` samples. There's some more info at this [JIRA task](https://its.cern.ch/jira/browse/LHCBGAUSS-2308).

Once the stats tables files are created (see below for instrcutions on how to make them if needed) you can collate geneartor level efficiencies by running

```bash
python GetGenStats.py
```

This will produce the `mds/GeneratorLevel.md` file for easy viewing on git and also the `jsons/GeneratorLevel.json` file for easily loading into python elsewhere.

#### If you need to create the tables from scratch

If you need to create them then first you need to get the ProdIDs of the samples:

```bash
python GetProdIds.py
```

Then you need to follow the instructions for getting the latest MCStatsTools package: https://gitlab.cern.ch/lhcb-datapkg/MCStatTools

Clone it here, get a grid certificate and then get `lb-dirac` environment

```bash
git lb-clone-pkg McStatTools
lhcb-proxy-init
lb-dirac
cd MCStatTools/scripts
```

Then you can simply run the `DownloadAndBuildStat.py` script with a comma separated list of the ProdIds (output from the script above).

```bash
python DownloadAndBuildStat.py <comma_sep_list_of_prod_ids>
```

This will produce a bunch of files/folders like:
 - `GenLogs-<prodid>`
 - `Prod_00<prodid>_Generation_log.json.gz`
 - `Generation_<simcond>_pid-<prodid>.json` (these are the ones we really care about)
 - `Generation_<simcond>.html` (these are the nicely displayed sim stats tables)

To then keep track of the generator level efficiencies run the `GetGenStatsFromLocalTables.py` script passing the `Generation_<simcond>_pid-<prodid>.json` files above. This will produce the `mds/GeneratorLevel.md` file for easy viewing on git and also the `jsons/GeneratorLevel.json` file for easily loading into python elsewhere. For example:

```bash
python GetGenStats.py McStatTools/scripts/Generation_Sim09-Beam*_pid-*.json
```

### Generate DaVinici stats

This is also designed to run on lxplus.

As mentioned in "post-processing MC" in the `NTupleProduction` section once an MC job has finished you can run the `NTupleProduction/ganga_eff.py` script to get the efficiency over the job. This can be run for individual job numbers with the `-j` argument:

```bash
python ganga_eff.py -j <jobid1> -j <jobid2> -j <jobiid3> -v -f
```

Or it can be run over all jobs in the a given dictionary file (like the one stored in `NTupleProduction/log/mc_job_map.py`):

```bash
python ganga_eff.py -r <path_to_job_map> -v -f
```

Once this hass been done for all jobs we can collate the information with the `GetDVStats.py` script:

```bash
python GetDVStats.py
```

This will print the efficiencies for each sample in `mc_job_map.py` and also produce the `mds/StrippingLevel.md` and the `jsons/StrippingLevel.json` files.

### Compute Selection Efficiencies

This should be run where the pipeline output is stored (nominally at cambridge).

This is run in the script `GetSelStats.py`. There are no options to pass but you may (will) need to edit the top of the file to set the right path to look up files in and set which channels / years / polarities you want to run on. This looks up the `<channel>_eff.log` files produced by the pipeline. Simply run:

```bash
python GetSelStats.py
```

This will print the efficiencies for each sample and also produce the `mds/SelectionLevel.md` and `jsons/SelectionLevel.json` files.

### Compute the Total efficiency

The above three values are then merged together by the `GetTotalEff.py` script. Again this needs editing to specify channels / years / polarities. It will read the `.json` files created above and merge the efficiencies where relevant or multiply them. It produces `mds/Total.md` and `jsons/Total.json`.

```bash
python GetTotalEff.py
```

### Compute the luminosities

This step should be run on lxplus (direct access to `/eos/lhcb/wg` space)

The procedure is different for Runs1 and Runs2.

**In Run1** the lumi tuples produced in DaVinci are reliable so it is straightfoward to simply read this information from the tuples. Thus something like this will probably suffice:

```bash
mkdir -p lumis
for mode in D0MuNu JpsiMuNu; do
    for year in 2011 2012; do
        for polarity in MU MD; do
            lb-run DaVinci/v45r1 python GetRun1Lumi.py /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA/${mode}/${year}/${polarity}/*.root 2>&1 | tee lumis/${mode}_${year}_${polarity}.log
        done
    done
done
```

**In Run2** one has to look up the lumi per RunNumber in a `.csv` file. The latest `run2_pp_lumi.csv.gz` is obtainable from the [LumiTwiki](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/LHCbLuminosity). A version from May 2021 is stored in the `lumis` directory. The script to run this would be:

```bash
mkdir -p lumis
for mode in D0MuNu JpsiMuNu; do
    for year in 2015 2016 2017 2018; do
        for polarity in MU MD; do
            lb-run DaVinci/v45r1 python GetRun2Lumi.py /eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/DATA/${mode}/${year}/${polarity}/*.root 2>&1 | tee lumis/${mode}_${year}_${polarity}.log
        done
    done
done
```

### Make the latex tables for the ANA

Simple one off script (from which you can comment the bits you don't need in the `__main__()` function):

```bash
python AnaTables.py
```
