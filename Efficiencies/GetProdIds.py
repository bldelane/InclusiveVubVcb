import subprocess
import importlib
import os

fname = os.path.join(os.getcwd(),'..','NTupleProduction','GangaSubmit.py')
if not os.path.exists(fname):
  raise RuntimeError('Cannot find GangaSubmit.py file')

print('### Loading bookkeeping paths from ../NTupleProduction/GangaSubmit.py ###')
spec = importlib.util.spec_from_file_location('module',  fname)
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)
if not hasattr(module,'input_mc_bk_paths'):
  raise RuntimeError('No dic input_mc_bk_paths in file')
paths = module.input_mc_bk_paths

#keys = ['Bc2D0MuNu','Bc2D0gMuNu','Bc2D0pi0MuNu','Bu2D0MuNu','Bc2D0MuNu_Kis','Bc2D0gMuNu_Kis','Bc2D0pi0MuNu_Kis','Bc2D0MuNu_K3Pi','Bc2D0gMuNu_K3Pi','Bc2D0pi0MuNu_K3Pi','Bc2JpsiMuNu','Bc2JpsiTauNu']
keys = ['Bc2JpsiTauNu']

print('### Getting ProdIDs using lb-dirac ###')
print('%-20s'%'Decay', 'Year', 'Pol', 'ProdID')
print(''.join( [ '-' for i in range(40) ] ) )

outf = open('prodids.log','w')

prodids = []

for key in keys:
  mode = 'JpsiMuNu' if 'Jpsi' in key else 'D0MuNu'
  if key not in paths[mode].keys():
    raise RuntimeError(key, 'not in path dict')

  for year, cfg in paths[mode][key].items():
    for pol, path in cfg.items():
      if path=='': continue
      command = 'lb-dirac dirac-bookkeeping-prod4path -B \'%s\''%path
      proc = subprocess.run( [command], capture_output=True, shell=True, text=True )
      lines = proc.stdout.split('\n')
      a = [ l for l in lines if 'Simulation' in l ]
      if len(a)!=1:
        raise RuntimeError('Not sure what to do')

      prodid = a[0].split(': ')[1]
      print( '%-20s'%key, '%4s'%year, ' %2s'%pol, prodid )
      outf.write( '%-20s %4s %2s %8s\n'%(key,year,pol,prodid ) )
      prodids.append(prodid)

outf.close()

print('Written ProdIDs to', outf.name)

print('ProdIDs:')
print(','.join(prodids))
