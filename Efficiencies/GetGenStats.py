import subprocess
import importlib
import os
from tabulate import tabulate
from glob import glob
import json

stats_path = '/eos/project/l/lhcbwebsites/www/projects/STATISTICS/SIM09STAT/SemiLep-WG/'

def json_has_right_sim_vers(fil,simVers):
  try:
    with open(fil) as f:
      a = json.load(f)
      if a['infoProd']['simVersion'] == simVers:
        return True
      else:
        return False
  except:
    return False

fname = os.path.join(os.getcwd(),'..','NTupleProduction','GangaSubmit.py')
if not os.path.exists(fname):
  raise RuntimeError('Cannot find GangaSubmit.py file')

print('### Loading bookkeeping paths from ../NTupleProduction/GangaSubmit.py ###')
spec = importlib.util.spec_from_file_location('module',  fname)
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)
if not hasattr(module,'input_mc_bk_paths'):
  raise RuntimeError('No dic input_mc_bk_paths in file')
paths = module.input_mc_bk_paths

channels = ['Bc2D0MuNu','Bc2D0gMuNu','Bc2D0pi0MuNu','Bu2D0MuNu','Bc2D0MuNu_Kis','Bc2D0gMuNu_Kis','Bc2D0pi0MuNu_Kis','Bc2D0MuNu_ISGW2','Bc2D0gMuNu_ISGW2','Bc2D0pi0MuNu_ISGW2','Bc2D0MuNu_K3Pi','Bc2D0gMuNu_K3Pi','Bc2D0pi0MuNu_K3Pi','Bc2JpsiMuNu','Bc2JpsiTauNu']

taunu_temp = { '2011_MD': 59233, '2011_MU' : 59235,
               '2012_MD': 59229, '2012_MU' : 59231,
               '2016_MD': 72434, '2016_MU' : 72430,
               '2017_MD': 89991, '2017_MU' : 89969
             }

tab_rows = [] # for printed output
out_dict = {} # for .py output

for channel in channels:
  mode = 'JpsiMuNu' if 'Jpsi' in channel else 'D0MuNu'
  if channel not in paths[mode].keys() and not '_ISGW2' in channel:
    raise RuntimeError(channel, 'not in path dict')

  if '_ISGW2' in channel:
    revchannel = channel.split('_ISGW2')[0]
    paths[mode][channel] = paths[mode][revchannel]

  for year, cfg in paths[mode][channel].items():
    if '_ISGW2' in channel and year!='2018': continue
    for pol, path in cfg.items():
      if path=='': continue
      eventType = path.split('/')[-2]
      simCond   = path.split('/')[3]
      simVers   = path.split('/')[4]

      # look up file in lhcb statistics
      options = []
      for fil in glob(os.path.join(stats_path,'Sim09-'+simCond, '*.json')):
        if os.path.basename(fil).startswith(f'Evt{eventType}'):
          options.append(fil)

      json_file = None
      if len(options)==0:
        print('WARNING. No stats found for', channel, year, pol, eventType, 'so will skip it')
        continue
        # temp hack
        #if channel=='Bc2JpsiTauNu' and f'{year}_{pol}' in taunu_temp.keys():
          #prodid = taunu_temp[f'{year}_{pol}']
          #json_file  = f'McStatTools/scripts/Generation_Sim09-{simCond}_pid-{prodid}.json'
          #if not os.path.exists(json_file):
            #json_file = None
            #continue
      else:
        # default to the first options file found
        json_file = options[0]
        # otherwise match condition
        for opt in options:
          if json_has_right_sim_vers(opt, simVers):
            json_file = opt
            break
      if not json_file:
        print('WARNING. Didn\'t find an appropriate file matching the sim version')
        continue

      with open(json_file) as f:
        cfg = json.load(f)

      eff = cfg["Hadron Counters"][0]["value"]
      err = cfg["Hadron Counters"][0]["error"]

      tab_rows.append( [channel, year, pol, eff, err] )

      if channel not in out_dict.keys(): out_dict[channel] = {}
      if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
      if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = { 'eff': eff, 'err': err }

print(tabulate(tab_rows,headers=['Channel','Year','Pol','Eff','Err']))

with open('mds/GeneratorLevel.md','w') as f:
  f.write(tabulate(tab_rows,headers=['Channel','Year','Pol','Eff','Err'],tablefmt='github'))

with open('jsons/GeneratorLevel.json','w') as f:
  json.dump(out_dict, f, indent=2)

with open('latex/GeneratorLevel.tex','w') as f:
  f.write(tabulate(tab_rows,headers=['Channel','Year','Pol','Eff','Err'],tablefmt='latex'))
