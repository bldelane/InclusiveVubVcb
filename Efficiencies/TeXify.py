"""
Rough executable to convert the json efficiency values to TeX tables for ANA

__author__: blaise.delaeny@cern.ch
"""

from tabulate import tabulate
import json
import numpy as np
import os
from uncertainties import ufloat

# when running the code, get these summaries out
json_path = "/usera/delaney/private/effs/InclusiveVubVcb/Efficiencies/jsons"

# if dir not exists, create a TeX dir
tex_path = f"{os.path.split(json_path)[0]}/tex"
os.system(f"mkdir -p {tex_path}")

modes = ["Bc2D0MuNu", "Bc2D0pi0MuNu", "Bc2D0gMuNu"]
years = ["2016", "2017","2018"]

labels = {}
labels["Bc2D0MuNu"] = "$B_c^{+}\\rightarrow D^0 \\mu^+ \\nu_{\\mu}$"
labels["Bc2D0gMuNu"] = "$B_c^{+}\\rightarrow D^{*} (\\rightarrow D^0 \\gamma) \\, \\mu^+ \\nu_{\\mu}$"
labels["Bc2D0pi0MuNu"] = "$B_c^{+}\\rightarrow D^{*} (\\rightarrow D^0 \\pi^0) \\, \\mu^+ \\nu_{\\mu}$"


for summary in os.listdir(json_path):
	
	if (summary!="Lumi.json" and summary!="SelectionLevel.json"):
		with open(f"{json_path}/{summary}") as json_file:
			eff_dicts = json.load(json_file)
		
		# table of effs
		headers = ["Decay mode", "Year", "Magnet Polarity", "Efficiency $\\varepsilon$ [\%]"]

		for y in years:
			# # book outfile names
			out_tex_file = f"{tex_path}/{os.path.splitext(summary)[0]}_{y}.tex"
			# remove old tex summaries	
			os.remove(out_tex_file) if os.path.exists(out_tex_file) else None
			
			body = []
			for m in modes:

				MU_eff = ufloat(eff_dicts[m][y]["MU"]["eff"], eff_dicts[m][y]["MU"]["err"])
				body.append([f"{labels[m]}", f"{y}", "\\textit{MagUp}", ("${:10.4f}$".format(MU_eff*100)).replace("+/-", "\\pm") ])
				MD_eff = ufloat(eff_dicts[m][y]["MD"]["eff"], eff_dicts[m][y]["MD"]["err"])
				body.append([f"{labels[m]}", f"{y}", "\\textit{MagDown}", ("${:10.4f}$".format(MD_eff*100)).replace("+/-", "\\pm") ])
		
			print(tabulate(body, headers, tablefmt="latex_raw", colalign=("left",)), 
			file=open(out_tex_file, "a"))