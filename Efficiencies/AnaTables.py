import json
from tabulate import tabulate
import os
os.system('mkdir -p latex/ANA/')

pol_map = { 'MU' : r'\textit{MU}', 'MD' : r'\textit{MD}' }
chanel_map = { 'Bc2D0MuNu'    : r'$\Bc \to \Dz \mup \neum$',
               'Bc2D0pi0MuNu' : r'$\Bc \to \Dz \piz \mup \neum$',
               'Bc2D0gMuNu'   : r'$\Bc \to \Dz \gamma \mup \neum$',
               'Bu2D0MuNu'    : r'$\Bp \to \Dzb \mup \neum$',
               'Bc2JpsiMuNu'  : r'$\Bc \to \jpsi \mup \neum$',
               'Bc2JpsiTauNu' : r'$\Bc \to \jpsi \taup \neum$'
              }

def totals_table():

  with open('jsons/Total.json') as f:
      totals = json.load(f)

  table_rows = []

  for channel, ccfg in totals.items():
      for year, ycfg in ccfg.items():
          for pol, pcfg in ycfg.items():

              table_rows.append( [ chanel_map[channel], year, pol_map[pol] ] )

              for stage, scfg in pcfg.items():
                  eff = scfg['eff']
                  err = scfg['err']

                  #eff_str = '$({:5.2f} \\pm {:4.2f})\\%$'.format(eff*100,err*100)
                  eff_str = '${:5.2f} \\pm {:4.2f}$'.format(eff*100,err*100)
                  table_rows[-1] += [ eff_str ]


  with open('latex/ANA/Total_All.tex','w') as f:
      print(tabulate(table_rows,headers=['Decay mode','Year','Polarity','Generator','Stripping','Selection','Total'],tablefmt='latex_raw'),file=f)

def trigger_table():

  with open('jsons/Trigger.json') as f:
    trigger = json.load(f)

  table_rows = []

  for channel, ccfg in trigger.items():
      for year, ycfg in ccfg.items():
          for pol, pcfg in ycfg.items():
            table_rows.append( [ chanel_map[channel], year, pol_map[pol], pcfg['pass'], pcfg['proc'], pcfg['l0'], pcfg['hlt1'], pcfg['hlt2'], pcfg['eff'] ] )

  with open('latex/ANA/TriggerLevel.tex','w') as f:
    print(tabulate(table_rows, headers=['Decay mode','Year','Polarity','Triggered','Processed',r'$\eps_{\lone}$',r'$\eps_{hlt1}$',r'$\eps_{hlt2}$',r'$\eps_{Trig}$'], floatfmt='5.2%', tablefmt='latex_raw').replace('%',r'\%'), file=f)

def generator_table():

  with open('jsons/GeneratorLevel.json') as f:
    generator = json.load(f)

  table_rows = []

  for channel, ccfg in generator.items():
      if '_Kis' in channel: continue
      if '_K3Pi' in channel: continue
      for year, ycfg in ccfg.items():
          if year in ['2011','2012']: continue
          for pol, pcfg in ycfg.items():
            table_rows.append( [ chanel_map[channel], year, pol_map[pol], pcfg['eff'], pcfg['err'] ] )

  with open('latex/ANA/GeneratorLevel.tex','w') as f:
    print(tabulate(table_rows, headers=['Decay mode','Year','Polarity','Efficiency','Error'], floatfmt='5.2%', tablefmt='latex_raw').replace('%',r'\%'), file=f)

def stripping_table():

  with open('jsons/StrippingLevel.json') as f:
    strip = json.load(f)

  table_rows = []

  for channel, ccfg in strip.items():
      if '_Kis' in channel: continue
      if '_K3Pi' in channel: continue
      if 'Dsx' in channel: continue
      if 'Dx' in channel: continue
      if 'Psi2S' in channel: continue
      if 'ChiC' in channel: continue
      for year, ycfg in ccfg.items():
          if year in ['2011','2012']: continue
          for pol, pcfg in ycfg.items():
            table_rows.append( [ chanel_map[channel], year, pol_map[pol], pcfg['eff'], pcfg['err'] ] )

  with open('latex/ANA/StrippingLevel.tex','w') as f:
    print(tabulate(table_rows, headers=['Decay mode','Year','Polarity','Efficiency','Error'], floatfmt='5.2%', tablefmt='latex_raw').replace('%',r'\%'), file=f)

def selection_table():

  with open('jsons/SelectionLevel.json') as f:
    sel = json.load(f)

  for channel, ccfg in sel.items():
      out_str = ''
      for year, ycfg in ccfg.items():
          for pol, pcfg in ycfg.items():
            table_rows = []
            for stage, scfg in pcfg.items():
              table_rows.append( [ stage, scfg['pass'], scfg['proc'], scfg['eff'], scfg['err'] ] )
            text = tabulate(table_rows,headers=['Stage','Passed','Processed','Efficiency','Error'], floatfmt='5.2%', tablefmt='latex_raw').split('\n')
            rows = [r'\resizebox{0.48\textwidth}{!}{'] + \
                   [ text[0] ] + \
                   [r'\hline'] + \
                   [r'\hline'] + \
                   [r'\multicolumn{5}{c}{'+chanel_map[channel]+' '+year+' '+pol_map[pol]+r'} \\'] + \
                   [r'\hline'] + \
                   text[1:-3] + \
                   [r'\hline'] + \
                   text[-3:] + \
                   [ '}' ]
            out_str += '\n'.join(rows) +'\n'

      with open(f'latex/ANA/SelectionLevel_{channel}.tex','w') as f:
        print(out_str.replace('%',r'\%'),file=f)

if __name__ == "__main__":
  totals_table()
  trigger_table()
  generator_table()
  stripping_table()
  selection_table()
