import os
import sys
files = sys.argv[1:]
import uproot
import numpy as np
import pandas

# get unique run numbers

all_runs = np.empty(0)

for f in files:
  print(os.path.basename(f))
  try:
    tree = uproot.open(f+':B2DMuNuX_D02KPiTuple/DecayTree')
  except:
    try:
      tree = uproot.open(f+':B2JpsiMuNuXTuple/DecayTree')
    except:
      print('WARNING: tree not found')
      continue
  run_numbers = tree['runNumber'].array(library='np')
  unq_runs = np.unique(run_numbers)
  all_runs = np.concatenate((all_runs,unq_runs))

all_runs = np.unique(all_runs)
print( 'Found', all_runs.size, 'unique run numbers' )

# look them up in the lumi.csv
df = pandas.read_csv("lumis/run2_pp_lumi.csv")
# get slice of matching runs
df = df[df['run'].isin(all_runs)]
# sum the lumi
lumi = df['lumi'].sum() * 1000
print('TOTAL ( {:d} files ): ({:6.1f} +/- {:3.1f})pb'.format(len(files), lumi, 0.02*lumi)) # lumi err is 2%


