
basepath = '/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC/'
channels = ['Bc2D0MuNu','Bc2JpsiMuNu']
pols = ['MD','MU']
years = ['2016','2017','2018']

l0   = 'Mu_plus_L0MuonDecision_TOS'
hlt1 = 'B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TwoTrackMVADecision_TOS || B_plus_Hlt1TrackMuonDecision_TOS || B_plus_Hlt1TrackMuonMVADecision_TOS || B_plus_Hlt1SingleMuonDecision_TOS'
hlt2 = 'B_plus_Hlt2Topo2BodyDecision_TOS || B_plus_Hlt2Topo3BodyDecision_TOS || B_plus_Hlt2Topo4BodyDecision_TOS || B_plus_Hlt2TopoMu2BodyDecision_TOS || B_plus_Hlt2TopoMu3BodyDecision_TOS || B_plus_Hlt2TopoMu4BodyDecision_TOS || B_plus_Hlt2SingleMuonDecision_TOS'

import ROOT as r
import uproot
import json

print_rows = []
out_dict = {}

for channel in channels:
  for year in years:
    for pol in pols:
      mode = 'JpsiMuNu' if 'Jpsi' in channel else 'D0MuNu'
      #tf = r.TFile(f'{basepath}/{mode}/{year}/{pol}/MC_{channel}_{year}_{pol}_pidgen.root')
      #tr = tf.Get('DecayTree')
      tr = uproot.open(f'{basepath}/{mode}/{year}/{pol}/MC_{channel}_{year}_{pol}_pidgen.root:DecayTree')
      nevs = tr.num_entries
      cut_str_np = f'({l0}) & ({hlt1}) & ({hlt2})'.replace('||','|')

      nl0 = len(tr.arrays( ['B_plus_M'], cut=f'({l0})'.replace('||','|') ) )
      nhlt1 = len(tr.arrays( ['B_plus_M'], cut=f'({hlt1})'.replace('||','|') ) )
      nhlt2 = len(tr.arrays( ['B_plus_M'], cut=f'({hlt2})'.replace('||','|') ) )

      ntrg = len(tr.arrays( ['B_plus_M'], cut=cut_str_np))
      #nevs = tr.GetEntries()
      #ntrg = tr.GetEntries( f'({l0}) && ({hlt1}) && ({hlt2})' )

      effl0 = nl0 / nevs
      effhlt1 = nhlt1 / nevs
      effhlt2 = nhlt2 / nevs
      eff = ntrg / nevs
      print(channel, year, pol, effl0, effhlt1, effhlt2, eff)
      print_rows.append( [ channel, year, pol, ntrg, nevs, effl0, effhlt1, effhlt2, eff ] )

      if channel not in out_dict.keys(): out_dict[channel] = {}
      if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
      if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = { 'pass': ntrg, 'proc': nevs, 'l0': effl0, 'hlt1': effhlt1, 'hlt2': effhlt2, 'eff': eff }

from tabulate import tabulate
print(tabulate(print_rows,headers=['Channel','Year','Pol','nTriggered','nTotal','L0','Hlt1','Hlt2','Eff'],floatfmt='5.2%'))
print(tabulate(print_rows,headers=['Channel','Year','Pol','nTriggered','nTotal','L0','Hlt1','Hlt2','Eff'],floatfmt='5.2%',tablefmt='latex'))

with open('jsons/Trigger.json','w') as f:
  json.dump(out_dict, f, indent=2)
