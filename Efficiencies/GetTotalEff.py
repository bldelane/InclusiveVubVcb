channels = ['Bc2D0MuNu','Bc2D0pi0MuNu','Bc2D0gMuNu','Bu2D0MuNu','Bc2JpsiMuNu','Bc2JpsiTauNu','Bc2D0MuNu_Kis','Bc2D0gMuNu_Kis','Bc2D0pi0MuNu_Kis','Bc2D0MuNu_ISGW2','Bc2D0gMuNu_ISGW2','Bc2D0pi0MuNu_ISGW2']
years = ['2016','2017','2018']
pols  = ['MU','MD']

import os
import json
from tabulate import tabulate
import numpy as np

def load(fname):
  if not os.path.exists(fname):
    raise RuntimeError('No such file', fname)
  with open(fname) as f:
    obj = json.load(f)
    return obj

def slice_cfg(dic, channel, year, pol):
  try:
    piece = dic[channel][year][pol]
  except:
    try:
      pl = 'MagUp' if pol=='MU' else 'MagDown'
      piece = dic[channel][year][pl]
    except:
      raise KeyError('Could not get', channel, year, pol, 'from dict')
  return piece

def get_print_format(eff, err):
    return f'({eff*100:5.2f} +/- {err*100:4.2f})%'

def total_from_sel(dic, verbose=True):

  if verbose: print(tabulate( [ [key] + list(item.values()) for key, item in dic.items()] ) )

  # naive thing
  # no longer needed
  #all_effs = np.array( [ item['eff'] for item in dic.values() if item['err'] is not None ] )
  #all_errs = np.array( [ item['err'] for item in dic.values() if item['err'] is not None ] )
  #naive_eff = np.prod( all_effs )
  #naive_err = naive_eff * sum( (all_errs/all_effs)**2 )**0.5
  #if verbose: print('Naive: ', naive_eff, '+/-', naive_err)

  #trig_eff = dic['Trigger']['eff']
  #trig_err = dic['Trigger']['err']
  #sel_proc = dic['MainSelection']['proc']
  #sel_pass = dic['MuonPID']['pass']
  #sel_eff = float(sel_pass)/float(sel_proc)
  #sel_err = ( (sel_eff*(1-sel_eff)) / float(sel_proc) )**0.5
  #if dic['VisibleMass']['eff'] == 1:
    #mass_eff = dic['MassCuts']['eff']
    #mass_err = dic['MassCuts']['err']
  #else:
    #mass_proc = dic['MassCuts']['proc']
    #mass_pass = dic['VisibleMass']['pass']
    #mass_eff = float(mass_pass)/float(mass_proc)
    #mass_err = ( (mass_eff*(1-mass_eff)) / float(mass_proc) )**0.5

  #effs = np.array( [ trig_eff, sel_eff, mass_eff] )
  #errs = np.array( [ trig_err, sel_err, mass_err] )
  #tot_eff = np.prod( effs )
  #tot_err = tot_eff * sum( (errs/effs)**2 )**0.5
  #if verbose: print('Comp:  ', tot_eff, '+/-', tot_err)

  #if not abs(naive_eff-tot_eff)<1e-5:
    #raise RuntimeError('Efficiencies are not self-consistent')

  if 'Total' not in dic.keys():
      raise RuntimeError('No \'Total\' key found in Selection log file')

  tot_eff = dic['Total']['eff']
  tot_err = dic['Total']['err']

  return (tot_eff, tot_err)

gen_dict   = load('jsons/GeneratorLevel.json')
strip_dict = load('jsons/StrippingLevel.json')
sel_dict   = load('jsons/SelectionLevel.json')

print_rows = []
out_dict = {}

for channel in channels:
  for year in years:
    if ('_Kis' in channel or '_ISGW2' in channel) and year!='2018': continue
    for pol in pols:

      res = {}

      # Generator
      gen   = slice_cfg(gen_dict, channel, year, pol)
      gen_eff, gen_err = gen['eff'], gen['err']
      gen_prnt = get_print_format( gen_eff, gen_err )
      res['Generator'] = {'eff': gen_eff, 'err': gen_err}

      # Stripping
      strip = slice_cfg(strip_dict, channel, year, pol)
      strip_eff, strip_err = strip['eff'], strip['err']
      strip_prnt = get_print_format( strip_eff, strip_err )
      res['Stripping'] = {'eff': strip_eff, 'err': strip_err}

      # Selection
      sel   = slice_cfg(sel_dict, channel, year, pol)
      sel_eff, sel_err = total_from_sel(sel, verbose=False)
      sel_prnt = get_print_format( sel_eff, sel_err )
      res['Selection'] = {'eff': sel_eff, 'err': sel_err}

      # TOTAL
      effs = np.array( [ gen_eff, strip_eff, sel_eff ] )
      errs = np.array( [ gen_err, strip_err, sel_err ] )
      eff = np.prod( effs )
      err = eff * sum( (errs/effs)**2 )**0.5
      tot_prnt = get_print_format( eff, err )
      res['Total'] = {'eff': eff, 'err': err}

      # append to printer
      print_rows.append( [channel, year, pol, gen_prnt, strip_prnt, sel_prnt, tot_prnt] )

      # add to output dict
      if channel not in out_dict.keys(): out_dict[channel] = {}
      if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
      if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = res

# print to screen
print(tabulate(print_rows, headers=['Channel','Year','Pol','Generator', 'Stripping','Selection','Total']))

# make .md
with open('mds/Total.md','w') as f:
  print(tabulate(print_rows, headers=['Channel','Year','Pol','Generator', 'Stripping','Selection','Total'],tablefmt='github'), file=f)

# make .json
with open('jsons/Total.json','w') as f:
  json.dump(out_dict, f, indent=2)

# make .tex
with open('latex/Total.tex','w') as f:
  for i, row in enumerate(print_rows):
      for j, el in enumerate(row):
          print_rows[i][j] = print_rows[i][j].replace('(','$(').replace(')%',r')\%$').replace('+/-',r'\pm')
  print(tabulate(print_rows, headers=['Channel','Year','Pol','Generator', 'Stripping','Selection','Total'],tablefmt='latex_raw'), file=f)

