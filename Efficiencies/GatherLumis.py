modes = ['D0MuNu','JpsiMuNu']
years = ['2011','2012','2015','2016','2017','2018']
pols  = ['MU','MD']

import os
from tabulate import tabulate
import json

print_rows = []
out_dict = {}

for mode in modes:
  out_dict[mode] = {}
  for year in years:
    out_dict[mode][year] = {}
    for pol in pols:
      fname = f'lumis/{mode}_{year}_{pol}.log'
      if not os.path.exists(fname): print('WARNING: ',mode,year,pol,'not found')
      with open(fname) as f:
        tline = f.readlines()[-1].replace('files )','files)').replace('(','').replace(')pb','')
      
      lumi = float(tline.split()[3])
      err  = float(tline.split()[5])
      print_rows.append( [mode, year, pol, lumi, err] )

      out_dict[mode][year][pol] = {'lumi': lumi, 'err': err}

print(tabulate(print_rows,headers=['Mode','Year','Pol','Lumi','Err']))

# md
with open('mds/Lumi.md','w') as f:
  print(tabulate(print_rows,headers=['Mode','Year','Pol','Lumi','Err'],tablefmt='github'), file=f)

# json
with open('jsons/Lumi.json','w') as f:
  json.dump(out_dict,f, indent=2)

