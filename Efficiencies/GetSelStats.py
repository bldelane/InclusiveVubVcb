import os
from glob import glob
from tabulate import tabulate
import uproot
import json

basepath = '/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/MC'
channels = ['Bc2D0MuNu','Bc2D0pi0MuNu','Bc2D0gMuNu','Bu2D0MuNu','Bc2JpsiMuNu','Bc2JpsiTauNu','Bc2D0MuNu_Kis','Bc2D0gMuNu_Kis','Bc2D0pi0MuNu_Kis','Bc2D0MuNu_ISGW2','Bc2D0gMuNu_ISGW2','Bc2D0pi0MuNu_ISGW2']
#years = ['2011','2012','2015','2016','2017','2018']
years = ['2016','2017','2018']
pols  = ['MU','MD']

def read_eff_log(fname):
  ret = {}
  if not os.path.exists(fname):
    raise RuntimeError('No such file at', fname)
  with open(fname) as f:
    for line in f.readlines():
      if line.startswith('Tuple'): continue
      if 'Cummulative' in line: continue
      if 'Truthmatching' in line: continue
      if line.split()[0]=='TOTAL:': continue
      stage = line.split()[1]
      passed = int(line.split()[11])
      proc   = int(line.split()[13])
      eff    = float(line.split()[15].replace('(',''))/100.
      err    = float(line.split()[17].replace(')%',''))/100.
      ret[stage] = { 'pass': passed, 'proc': proc, 'eff': eff, 'err': err }
  return ret

def get_vis_eff(fname, blank=False, npass=None):
  if blank: return { 'VisibleMass' : { 'pass': npass, 'proc': npass, 'eff': 1, 'err': None } }
  else:
    rootpath = fname.replace('_eff.log','.root')
    if not os.path.exists(rootpath):
      raise RuntimeError('No such file at', rootpath)

    tr = uproot.open(rootpath+":DecayTree")
    mcorr = tr.arrays('B_plus_MCORR',library='np')['B_plus_MCORR']
    proc   = mcorr.size
    passed = mcorr[mcorr>3500].size
    eff    = float(passed)/float(proc)
    err    = ( (eff*(1-eff)) / float(proc) )**0.5
    return { 'VisibleMass': {'pass': passed, 'proc': proc, 'eff': eff, 'err': err } }

out_dict = {}
print_rows = []
print_sub_rows = []

for channel in channels:
  mode = 'JpsiMuNu' if 'Bc2Jpsi' in channel else 'D0MuNu'
  fname    = f'{channel}_eff.log'
  for year in years:
    if ('_Kis' in channel or '_ISGW2' in channel) and year!='2018': continue
    # mass cuts applied after samples are merged
    fitpath  = os.path.join(basepath,'fit',mode,year,fname)
    fiteffs  = read_eff_log(fitpath)
    # special extra efficiency for visible mass (unity for JpsiMuNu)
    #viseffs  = get_vis_eff(fitpath, mode=='JpsiMuNu', npass=fiteffs['MassCuts']['pass'])
    # selection performed for MD and MU seperately
    for pol in pols:
      selpath  = os.path.join(basepath,'sel',mode,year,pol,fname)
      seleffs  = read_eff_log(selpath)

      # combine them
      # selection efficienies are per-polarity
      # so let's make a sub_total at this point
      sst_nproc = seleffs[list(seleffs.keys())[0]]['proc']
      sst_npass = seleffs[list(seleffs.keys())[-1]]['pass']
      sst_eff   = float(sst_npass)/float(sst_nproc)
      sst_err   = ( (sst_eff*(1.-sst_eff))/float(sst_nproc) )**0.5

      effs = {**seleffs}
      sub_rows = [ [key, item['pass'], item['proc'], item['eff'], item['err']] for key,item in seleffs.items() ]

      # the mass cuts are then applied when writing the `fit` files (and applied to sum of both polarities)
      # and we skip the truth match criteria
      # so look up the mass cut efficiency and then get an estimate of evs from there
      nprev = sst_npass
      for key, item in fiteffs.items():
          npass = int(round(item['eff']*nprev))
          # add to printer
          sub_rows.append( [ key, npass, nprev, item['eff'], item['err'] ] )
          # add to output
          effs[key] = { 'pass': npass, 'proc': nprev, 'eff':item['eff'], 'err': item['err'] }
          nprev = npass

      # now attempt to make a total
      tot_nproc = sst_nproc
      tot_npass = nprev
      eff = float(tot_npass)/float(tot_nproc)
      err = ( ( eff*(1.-eff))/float(tot_nproc) )**0.5
      # add to printer
      sub_rows.append( ['Total', tot_npass, tot_nproc, eff, err ] )
      # add to output
      effs['Total'] = { 'pass': tot_npass, 'proc': tot_nproc, 'eff': eff, 'err': err }

      # append to printer
      print_rows.append(  [ channel, year, pol, fname ] )
      print_sub_rows.append( sub_rows )

      # add to output dic
      if channel not in out_dict.keys(): out_dict[channel] = {}
      if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
      if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = effs

# print to screen
lines = tabulate(print_rows,tablefmt='plain').split('\n')
width = max( [len(l) for l in lines] )
for i, line in enumerate(lines):
  print( '='*width )
  print(line)
  print( '='*width )
  tablines = tabulate(print_sub_rows[i],headers=['Stage','Passed','Processed','Efficiency','Error']).split('\n')
  prlines  = tablines[:-1] + [tablines[1]] + [tablines[-1]]
  print( '   '+'\n   '.join(prlines) )

# print to md
with open('mds/SelectionLevel.md','w') as f:
  # print contents first
  print('### Contents:', file=f)
  for row in print_rows:
    print( '  -', '['+' '.join( row[:-1] )+']'+'(#'+'-'.join( r.lower() for r in row[:-1] )+')', file=f )
    #print('[',row[0], row[1], row[2], ']')# , '(#'+'-'.join( [r.lower() for r in row[:2] ] ) )
  for i, row in enumerate(print_rows):
    print('###', row[0], row[1], row[2], file=f)
    print( '   '+tabulate(print_sub_rows[i],headers=['Stage','Passed','Processed','Efficiency','Error'],tablefmt='github').replace('\n','\n   '), file=f )

# dump to json
with open('jsons/SelectionLevel.json','w') as f:
  json.dump(out_dict, f, indent=2)

# dump to latex
with open('latex/SelectionLevel.tex','w') as f:
  for i, line in enumerate(lines):
      f.write(' '.join( line.split()[:-1] )+'\n')
      f.write(tabulate(print_sub_rows[i], headers=['Stage','Passed','Processed','Efficiency','Error'],tablefmt='latex'))
      f.write('\n')
      f.write('\n')
