import os
os.system('mkdir -p mds')
os.system('mkdir -p jsons')
import importlib
import os
import subprocess
from tabulate import tabulate
import json

fname = os.path.join(os.getcwd(),'..','NTupleProduction','log','mc_job_map.py')
if not os.path.exists(fname):
  raise RuntimeError('Cannot find mc_job_map.py file')
spec = importlib.util.spec_from_file_location('module',  fname)
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)
if not hasattr(module,'job_map'):
  raise RuntimeError('No job map in file')
jmap = module.job_map

remote_loc = 'phskbk@aspin.epp.warwick.ac.uk:/home/epp/phskbk/gangadir/workspace/phskbk/LocalXML/'
alt_remote_loc = 'phskbk@aspin.epp.warwick.ac.uk:/storage/epp2/phskbk/gangadir/workspace/phskbk/LocalXML'

print_rows = [] # for printed output
out_dict = {} # for .py output

for channel, cfg in jmap.items():
  for year, pcfg in cfg.items():
    for pol, jid in pcfg.items():
      if jid is None: continue
      print(channel, year, pol, jid)
      if jid<10 or jid in [40,41,42,43,44,45]: loc = alt_remote_loc
      else: loc = remote_loc

      subprocess.run(['scp',os.path.join(loc,str(jid),'output','eff.log'), '/tmp/tmp_eff.log'], capture_output=True, check=True)

      with open('/tmp/tmp_eff.log') as f:
        line = f.readlines()[-1]
        passed = int(line.split()[2])
        proc   = int(line.split()[4])
        eff    = float(line.split()[6].replace('(',''))/100.
        err    = float(line.split()[8].replace(')%',''))/100.

      print_rows.append( [channel, year, pol, passed, proc, eff, err] )

      if channel not in out_dict.keys(): out_dict[channel] = {}
      if year not in out_dict[channel].keys(): out_dict[channel][year] = {}
      if pol not in out_dict[channel][year].keys(): out_dict[channel][year][pol] = { 'pass': passed, 'proc': proc, 'eff': eff, 'err': err }

print(tabulate(print_rows, headers=['Channel','Year','Pol','Passed','Processed','Efficiency', 'Error'] ) )

with open('mds/StrippingLevel.md','w') as f:
  f.write(tabulate(print_rows, headers=['Channel','Year','Pol','Passed','Processed','Efficiency', 'Error'], tablefmt='github' ) )

with open('jsons/StrippingLevel.json','w') as f:
  json.dump(out_dict, f, indent=2)

with open('latex/StrippingLevel.tex','w') as f:
  f.write(tabulate(print_rows, headers=['Channel','Year','Pol','Passed','Processed','Efficiency', 'Error'], tablefmt='latex' ) )

