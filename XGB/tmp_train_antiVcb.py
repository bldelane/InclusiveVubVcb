"""Placeholder script to run the two-class classifiers.
This script assumes that the antiComb XGB does not sculpt the MCORRERR and LTIME
distribs in the CF and sig samples.

NOTE: ASSUMPTION: the antiVcb XGB is uncorrelated in the training with the antiComb XGB.
This is likely sensisble owing to the makeup of the cf data and, trivially for the signal MC. 
In deployment, this is not quite likely. But for the WS data, the important thing is to KDE in 4D pre-cuts.
"""

import numpy as np
import pandas as pd
from XGB_base import (
    XGBBuilder,
    XGBScalerBuilder,
    BinaryClassifier,
    antiVcb_config_dict,
    plot_2d_dec_bdy,
)
from collections import namedtuple
import matplotlib.pyplot as plt
import sys

import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import read_feats

if __name__ == "__main__":
    __storage_path = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch"
    # train on full 16-18 samples aggregated
    # build a ScalerClassifier and train with WS data, passing sWeights
    clf = XGBBuilder(
        features=list(read_feats("yml/features.yml", "antiVcb").keys()),
        hpars_config=antiVcb_config_dict,
        name="antiVcb",
    ).build_new_clf(
        # semi-exclusive simulation
        sig_path=f"{__storage_path}/nominal/MC/combined/D0MuNu/5.x_invfb/Bc2D0MuNuX.pkl",
        # real-charm background
        # NOTE: train on composite bkg
        bkg_path=[
            f"{__storage_path}/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_cf_sw.pkl",
        ],
    )

    # train, no scaling imposed here
    clf.prep_run_train(balanced=True)

    # save the model to default path
    clf.save_model()

    # perform inferece on the sig and bkg samples
    clf.bkg_sample["XGB_antiVcb"] = clf.run_siglike_inference(X=clf.bkg_sample)
    clf.sig_sample["XGB_antiVcb"] = clf.run_siglike_inference(X=clf.sig_sample)

    # plot the 2D decision boundary
    plot_2d_dec_bdy(
        sig=clf.sig_sample,
        cf=clf.bkg_sample,
        model=clf.model,
        xgb_cut=0.5,
        plotname="antiVcb_dec_bdy_latest",
    )
