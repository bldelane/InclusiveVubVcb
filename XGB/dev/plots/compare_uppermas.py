import matplotlib.pyplot as plt
import pandas as pd

plt.style.use("science")

import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils.plot_super import plot_data

_sel = "B_plus_M>5500 and B_plus_M<6285"
cf = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_sig_cf_sw.pkl"
).query(_sel)
ws = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/DpMuNu/2018/Bc2DpMuNuX_sig_ws_sw.pkl"
).query(_sel)

plt_config = {
    "bins": 50,
    "range": [3_000, 18_000],
    "isnorm": True,
    "capsize": 0,
    "elinewidth": 1,
}
fig, ax = plt.subplots()
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
plot_data(
    ax=ax,
    data=cf.B_plus_MCORR,
    wts=cf.sw,
    **plt_config,
    label="sW'd CF data",
    col="darkorange",
)

plot_data(
    ax=ax,
    data=ws.B_plus_MCORR,
    wts=ws.sw,
    **plt_config,
    label="sW'd WS data",
    col="black",
)
ax.plot([], [], "", label=r"$m(D^{0/+}\mu^+)>5500$ MeV$/c^2$", color="white")

ax.legend(bbox_to_anchor=(0.5, -0.4), loc="center")
ax.set_xlabel(r"$m_{corr}(D^{0/+}\mu^+)$ [MeV]")
ax.set_ylabel("Candidates, normalised")
plt.savefig("uppermass_cf_ws.pdf")
