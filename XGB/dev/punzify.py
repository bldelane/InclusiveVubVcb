"""Compute the Punzi figure of merit
"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from re import I, M
from tkinter import W
from header import *
from uncertainties import ufloat
from preprocess_utils import read_feats, timing, check_argpath
from fit import build_execute_fit
import sys
from pandas import read_pickle
from preprocess_utils import norm_hist

def calc_eff(
    N:"pop",
    k:"pass"
)->ufloat:
    eff_n = k/N
    eff_s = (1/N)*np.sqrt( k*(1.-k/N) )

    return ufloat(eff_n, eff_s)

def calc_punzi(
    sig_eff:"signal passing the t cut",
    bkg:"background counter", # is it the total bkg?
    a:"significance level"=5,
)->ufloat:
    p = sig_eff / (a/2 + (bkg)**.5)

    return p

parser = ArgumentParser(description="sweight config")
parser.add_argument('-r','--resp',  
    default = "/usera/delaney/private/Bc2D0MuNuX/DpMuNu/response.yml",
    help="Candidate cut values on the xgb response")    
opts = parser.parse_args()

thresholds = read_feats(opts.resp, _key="xgb")

dz  = pandas.read_pickle(f"/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/DATA/combined/DpMuNu/2018/xgb/inference/dz.pkl")
dst = pandas.read_pickle(f"/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/DATA/combined/DpMuNu/2018/xgb/inference/dst.pkl")
GLOBAL_sig_den = len(dz)+len(dst)

bkk  = {}
pz_n  = {}
pz_s  = {}
for t in thresholds:
    res = build_execute_fit(t)
    bkk[t] = res
    pz_n[t] = calc_punzi(
        sig_eff = calc_eff(N = GLOBAL_sig_den, k=res["D0_y"].n+res["Dst_y"].n),
        bkg = res["comb_y_2018"]
    ).n
    pz_s[t] = calc_punzi(
        sig_eff = calc_eff(N = GLOBAL_sig_den, k=res["D0_y"].n+res["Dst_y"].n),
        bkg = res["comb_y_2018"]
    ).s



Path(f"/usera/delaney/private/Bc2D0MuNuX/DpMuNu/plots/punzi").mkdir(parents=True, exist_ok=True)
fig, ax = plt.subplots()
ax.errorbar(
        x = thresholds,
        y = list(pz_n.values()),
        yerr = list(pz_s.values()),
        elinewidth=1.5,
        color="tab:blue",
        fmt=".",
        label = r"$a=5$",
    )
ax.fill_between(
    thresholds,
    np.array(list(pz_n.values())) - np.array(list(pz_s.values())),
    np.array(list(pz_n.values())) + np.array(list(pz_s.values())),
    alpha=.3
)
ax.legend(fontsize=30, loc="upper left")
hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax) 
ax.set_ylabel(r"$\frac{\varepsilon(t)}{a/2 + \sqrt{B(t)}}$", fontsize=30)
ax.set_xlabel(r"$t$", fontsize=30)

plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/DpMuNu/plots/punzi/scan_a5.pdf")
plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/DpMuNu/plots/punzi/scan_a5.png")
plt.close()


dcs = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/dcs.pkl").query("xgb>0.8")
cf = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/cf.pkl").query("xgb>0.8")
ws = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/ws.pkl").query("xgb>0.8")
misid = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/misid.pkl").query("xgb>0.8")
dz = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/dz.pkl").query("xgb>0.8")
dst = read_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/inference/dst.pkl").query("xgb>0.8")


# ensure MCORR is not sculpted in any template
import seaborn as sns
sns.set_palette("magma")

templates = [dcs, cf, ws, misid, dz, dst] 
fig, axs = plt.subplots()
for i in range(len(templates)): 
    df = templates[i]
    norm_hist(
            _branch = df["B_plus_MCORR"],
            _ax = ax,
            bins=25,
            histtype="step"
        )
    ax.legend(fontsize=10)
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax) 
    ax.set_ylabel("Candidates, normalised", fontsize=30)
    ax.set_xlabel(r"$m_{corr}(D\mu^+)$   [MeV/$c^2$]", fontsize=30)

plt.savefig(f"{opts.plotsdir}/check/all_samples.pdf")
plt.savefig(f"{opts.plotsdir}/check/all_samples.png")
plt.close()