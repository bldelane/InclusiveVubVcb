"""Utilities to perform the preprocessing required to maximise the MVA performance.

This should be able to take in a ROOT file, persist BOI, scale and save a pkl/npy dataframe
"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from bdb import Breakpoint
from importlib.metadata import entry_points
from header import * # basic utilities and cosmetics
from sklearn.model_selection import train_test_split
from scipy.stats import pearsonr
from xgboost import XGBClassifier
import pandas as pd
from sklearn.metrics import accuracy_score
#from sweight import *
import scipy
import seaborn as sn
from sklearn.preprocessing import QuantileTransformer
import yaml
import time
from tqdm import tqdm
import warnings
from termcolor2 import c

# cosmetics
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "axes.labelsize": 20,
    "axes.titlesize": 20,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 17,
    "ytick.labelsize": 20,
    "xtick.labelsize": 20,
})




# ==============================
#     MANIPULATION utilities 
# ==============================
def constrain(
    feat:"feature/column/branch",
    n_sigma=5, #"+/- n std dev of retention window",
)->"dataframe[features]":
    """retain if falling within a window"""

    mean = np.mean(feat)
    std  = np.std(feat)
    
    retained = feat[(feat>=mean-n_sigma*std) & (feat<=mean+n_sigma*std)]
    feat[(feat<mean-n_sigma*std)] = np.min(retained)
    feat[(feat>mean+n_sigma*std)] = np.max(retained)
    
    return feat

def Mscale(
    df:"pandas df",
    feats:"training features list",
)->"dataframe with variables of range O(1), and withing 5sigma of mean":
    # MeV->GeV
    _gev_sc = [
        "D0_PT",
        "Mu_plus_PT",
        "B_plus_PT",
    ]; assert(x in feats for x in _gev_sc)
    # log
    _log_sc = [
        "B_plus_FDCHI2_OWNPV",
        "D0_FDCHI2_OWNPV",
        "B_plus_IPCHI2_OWNPV",
        "D0_IPCHI2_OWNPV",
        "K_minus_IPCHI2_OWNPV",
        "Mu_plus_IPCHI2_OWNPV",
    ]; assert(x in feats for x in _log_sc)
    
    for v in _gev_sc:
        df[v] = df[v] / 1000.
    for v in _log_sc:
        df[v] = np.log(df[v])
    assert("B_plus_DOCA_D0_Mu_plus" in feats)
    df["B_plus_DOCA_D0_Mu_plus"] = df["B_plus_DOCA_D0_Mu_plus"]*100.

    # constrain range
    df[feats] = df[feats].apply(lambda x: constrain(x), axis=0)
    return df


# ==============================
#      PLOTTING utilities 
# ==============================
def plot_data(data, ax, bins, range, wts=None, label=None, col=None)->"plt object":
    """Errorbar plot. Accepts weighted data and computes correct error."""
    nh, xe = np.histogram(data,bins=bins,range=range)
    cx = 0.5*(xe[1:]+xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
        whist.fill( data, weight = wts )
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance**0.5

    ax.errorbar(cx, nh, 
        yerr=err, 
        xerr = (xe[1]-xe[0])/2.,
        fmt='.', 
        color="black",
        markersize=5,
        elinewidth=1.1,
        capsize=1, 
        markeredgewidth=1,
        label=label,
    )

def norm_hist(
    _branch,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
)->"hist":
    """plot normalised hist; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w/=len(_branch)
    if _sw is not None:
        norm_w*=_sw # not the sweighted hist, then normalised - sloppy but OK for now
    
    _ax.hist(
        _branch,
        weights = norm_w,
        **kwargs
    )

if __name__ == '__main__':
    parser = ArgumentParser(description="filter & scale ROOT files [r01/.../combined]")
    parser.add_argument('-i','--inpath',  default="/home/blaised/private/Bc2D0MuNuX/tmp", 
        help="path to file for dpmunu post-offline selection")    
    parser.add_argument('-f','--feats',  
        default = "features.yml",
        help="path yaml file with all training features")    
    parser.add_argument('-m','--mrange',  
        default = [5_500,10_000],
        help="MCORR range to consider in the proxy fit for Punzi")    
    parser.add_argument('-p','--plotsdir',  
        default="plots/mva", 
        help="where to dump the MVA plots for Punzi optim")       
    parser.add_argument('-t','--test',  action="store_true", 
        help="TEST mode, with 1e6 max entries per dataframe")    
    opts = parser.parse_args()

    FEATURES = read_feats(opts.feats, _key="features")
    MCORR_cut = f"(B_plus_MCORR>={opts.mrange[0]}) & (B_plus_MCORR<={opts.mrange[-1]})"

    samples = { # key:{path suffix, label},
        "DpMuNu": {
            "suffix" : "DATA/combined/DpMuNu/2018/Bc2DpMuNuX_sig.root:DecayTree",
            "label"  : r"WS $D^+\mu^+$ data",
            "cut"    : f"(D0_ID*Mu_plus_ID<0) & {MCORR_cut}", # WS data 
        },
        "CF": {
            "suffix" : "DATA/combined/D0MuNu/2018/Bc2D0MuNuX_sig.root:DecayTree",
            "label"  : r"CF data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID<0) & {MCORR_cut}"
        },
        "DCS": {
            "suffix" : "DATA/combined/D0MuNu/2018/Bc2D0MuNuX_sig.root:DecayTree",
            "label"  : r"DCS data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID>0) & {MCORR_cut}"
        },
        "misID": {
            "suffix" : "DATA/combined/D0MuNu/2018/Bc2D0MuNuX_misid.root:DecayTree",
            "label"  : r"DCS misID data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID>0) & {MCORR_cut}"
        },           
        "D0MuNu": {
            "suffix" : "MC/combined/D0MuNu/2018/Bc2D0MuNu.root:DecayTree",
            "label"  : r"$B_c^+ \to D^0 \mu^+ \nu_{\mu}$",
            "cut"    : f"{MCORR_cut}"
        },           
        "DstMuNu": {
            "suffix" : "MC/combined/D0MuNu/2018/Bc2D0*MuNu.root:DecayTree", # for the moment, glob both D0g and Dzpi0
            "label"  : r"$B_c^+ \to D^{0*} \mu^+ \nu_{\mu}$",
            "cut"    : f"{MCORR_cut}"
        },           
    }


    if opts.test: 
        N_MAX = 1_000_000
    if not opts.test:
        N_MAX = None

    dcs = Mscale(
            df = batched_load_ROOT_file(
        _file = f"{opts.inpath}/{samples['DCS']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['DCS']['cut'],
        _name = samples['DCS']['label'],
        _max_entries = N_MAX
        ),
    feats=FEATURES
    )
    
    cf = Mscale(
        df = batched_load_ROOT_file(
        _file = f"{opts.inpath}/{samples['CF']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['CF']['cut'],
        _name = samples['CF']['label'],
        _max_entries = N_MAX
        ),
    feats = FEATURES
    )
    
    ws = Mscale(
        df = batched_load_ROOT_file(
        _file = f"{opts.inpath}/{samples['DpMuNu']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['DpMuNu']['cut'],
        _name = samples['DpMuNu']['label'],
        _max_entries = N_MAX
        ),
    feats = FEATURES
    )
    
    misid = Mscale(
        df = batched_load_ROOT_file(
        _file = f"{opts.inpath}/{samples['misID']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['misID']['cut'],
        _name = samples['misID']['label'],
        _max_entries = N_MAX
        ),
    feats = FEATURES
    )
    
    dz = Mscale(
        df = oneshot_load_ROOT_file(
        _file = f"{opts.inpath}/{samples['D0MuNu']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['D0MuNu']['cut'],
        _name = samples['D0MuNu']['label'],
        ),
    feats = FEATURES
    )

    dst = Mscale(
        df = load_concat_ROOT_file(
        _files = f"{opts.inpath}/{samples['DstMuNu']['suffix']}",
        _branches = FEATURES + ["B_plus_MCORR", "B_plus_M", "B_plus_FIT_LTIME"],
        _cut = samples['DstMuNu']['cut'],
        _name = samples['DstMuNu']['label'],
        ),
    feats = FEATURES
    )

    import seaborn as sns
    sns.set_palette("viridis")

    # plot variables seen by the MVA 
    Path(f"{opts.plotsdir}/preprocessed").mkdir(parents=True, exist_ok=True)
    fig, axs = plt.subplots(nrows=3, ncols=7, figsize=(50, 21))
    plt.tight_layout()
    axs = axs.ravel()
    for i in range(len(FEATURES)):
        c = FEATURES[i]
        ax = axs[i]
        for df in (dcs, cf, ws, misid, dz, dst):
            norm_hist(
                _branch = df[c],
                _ax = ax,
                label=df.name,
                bins=25,
                histtype="step"
            )
        ax.legend(fontsize=20)
        hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax) 
        ax.set_ylabel("Candidates, normalised", fontsize=30)
        ax.set_xlabel(r"\texttt{"+f"{c.replace('_','-')}"+r"}", fontsize=30)
    
    plt.savefig(f"{opts.plotsdir}/preprocessed/all_samples.pdf")
    plt.savefig(f"{opts.plotsdir}/preprocessed/all_samples.png")
    plt.close()


    # save the processed dfs
    Path(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed").mkdir(parents=True, exist_ok=True)
    dcs.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/dcs.pkl")
    cf.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/cf.pkl")
    ws.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/ws.pkl")
    misid.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/misid.pkl")
    dz.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/dz.pkl")
    dst.to_pickle(f"{opts.inpath}/DATA/combined/DpMuNu/2018/xgb/preprocessed/dst.pkl")