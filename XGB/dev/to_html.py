from builtins import breakpoint
import os
import glob
import numpy as np 

html = """<!DOCTYPE html>
<html>
<head>
<style>
* {
  box-sizing: border-box;
}

.img-container {
  float: left;
  width: 300px;
  height: 300px;
  padding: 5px;
}

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
</style>
</head>
<body>

<h2>Anti-combinatorial MVA studies</h2>

"""


# ===================================
#         Training set sel
# -----------------------------------
html+="""
<h>Candidate feature distributions - scaled</h>""" 

PARENT_DIR = "plots/mva/feat_sel/scaled"
distrs = [file for file in os.listdir(f"{PARENT_DIR}") if file.endswith('.pdf')]
for d in distrs:
    html+=f"""
        <img src='{PARENT_DIR}/{d}' alt='{d}' style='width:50%'>
    """
breakpoint()
# ===================================


# ===================================
#           MC corrections
# -----------------------------------
html+="""
<h>Candidate feature distributions - data/MC checks </h>

""" 
PARENT_DIR = "SimCorrs/plots/2016/xgb"
distrs = np.array([file for file in os.listdir(f"{PARENT_DIR}") if file.endswith('.pdf')])
# ===================================
# reformat for blocks of 4 plots
if len(distrs)%4==0: 
  distrs = distrs.reshape(-1, 4)
  x_dim = 4
if len(distrs)%3==0: 
  distrs = distrs.reshape(-1, 3)
  x_dim = 3

for block in distrs:

    html+=f"""<div class='clearfix'>"""

    html+=f"""<div class='img-container'>
        <embed src='{PARENT_DIR}/{block[0]}'
        width="100%"
        height="100%"
        frameBorder="0"
        ></embed>
      </div>
      <div class='img-container'>
        <embed src='{PARENT_DIR}/{block[1]}'
        width="100%"
        height="100%"
        frameBorder="0"
        ></embed>
      </div>
      <div class='img-container'>
        <embed src='{PARENT_DIR}/{block[2]}'
        width="100%"
        height="100%"
        frameBorder="0"
        ></embed>
      </div>
    """
    if x_dim==4:
      html+=f"""<div class='img-container'>
        <embed src='{PARENT_DIR}/{block[3]}'
        width="100%"
        height="100%"
        frameBorder="0"
        ></embed>
      </div>
      """
    
    html+="""</div>
    
    """

html+="""
</body>
</html>
"""

html+="""
<h2>Appendix</h2>
"""
with open("index.html", "w") as f:
    print(html, file=f)
