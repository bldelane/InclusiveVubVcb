# === imports ===
# analysis python modules
from argparse import ArgumentParser
import os, glob
from pathlib import Path
#from scipy import stats
import numpy as np
import boost_histogram as bh
import pandas as pd
import yaml
import json
import pyhf
import uproot
#pyhf.set_backend("numpy", "minuit") # get uncertainties and covariance
pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer(verbose=1, errordef=1.)) # get uncertainties and covariance
import pickle
from uncertainties import *
from matplotlib import gridspec
from tabulate import tabulate
import json, requests, jsonschema

#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
# plt.style.use(hep.style.LHCb2)
# plt.rcParams.update({
#     "text.usetex": True,
#     "font.family": "serif",
#     #"font.serif": ["Palatino"],
#     "axes.labelsize": 20,
#     "axes.titlesize": 20,
#     "xaxis.labellocation": "center",
#     "yaxis.labellocation": "center",
#     "legend.fontsize": 17,
#     "ytick.labelsize": 20,
#     "xtick.labelsize": 20,
# })
plt.style.use('science')

# ========================
#       Plotting 
# ========================
def plot_data(data, ax, bins, range, wts=None, _isnorm=False, label=None, col=None):
    nh, xe = np.histogram(data,bins=bins,range=range)
    cx = 0.5*(xe[1:]+xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
        whist.fill( data, weight = wts )
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance**0.5

    if _isnorm:
        nh /= np.sum(nh)        
        err /= np.sum(err)        

    ax.errorbar(cx, nh, 
        yerr=err, 
        xerr = (xe[1]-xe[0])/2.,
        fmt='.', 
        color="black",
        markersize=5,
        elinewidth=1.1,
        capsize=1, 
        markeredgewidth=1,
        label=label,
    )

def norm_hist(
    _branch,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
)->"hist":
    """plot normalised hist; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w/=len(_branch)
    if _sw is not None:
        norm_w*=_sw # not the sweighted hist, then normalised - sloppy but OK for now
    
    _ax.hist(
        _branch,
        weights = norm_w,
        **kwargs
    )

def norm_errobar(
    _branch,
    _bins,
    _range,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
)->"errorbar":
    """plot normalised errorbar; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w/=len(_branch)
    if _sw is not None:
        norm_w*=_sw # not the sweighted hist, then normalised - sloppy but OK for now
    
    pop, e = np.histogram(
        _branch,
        weights = norm_w,
        bins = _bins,
        range = _range
    )
    assert(abs(np.sum(pop)-1.0)<1e-5)
    _ax.errorbar(
        x = (e[1:]+e[:-1])/2,
        y = pop,
        xerr =(e[1:]-e[:-1])/2,
        **kwargs 
    )