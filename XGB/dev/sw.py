"""Isolate the real-charm D+/0 component in the m(K-pi (pi) ) with sweights & check correlation with corrected mass
"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"
__storage__= "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined" 

from builtins import breakpoint
from load_utils import yml_to_dict
import uproot
import matplotlib.pyplot as plt
import boost_histogram as bh
import pandas as pd
import numpy as np
import os, yaml
from argparse import ArgumentParser
from pathlib import Path
#from scipy.stats import crystalball, expon
from numba_stats import crystalball, expon
from numba_stats import crystalball_ex
from numba_stats import crystalball as CB
from iminuit import Minuit
from iminuit.cost import ExtendedUnbinnedNLL
import mplhep as hep
from sweights import kendall_tau # for an independence test
from sweights import plot_indep_scatter
from iminuit.pdg_format import pdg_format
from sweights import SWeight # for classic sweights
from sweights import Cow     # for custom orthogonal weight functions
from tqdm import tqdm
import warnings
from termcolor2 import c
import warnings

# cosmetics
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "axes.labelsize": 20,
    "axes.titlesize": 20,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 17,
    "ytick.labelsize": 20,
    "xtick.labelsize": 20,
})

parser = ArgumentParser(description="sweight config")
parser.add_argument('-C','--channel',  
    choices=["D0MuNu","DpMuNu","JpsiMuNu"],  
    required=True, 
    help="channel, post presel+loose momentum & PID selection [D0MuNu|DpMuNu|JpisMuNu]")    
parser.add_argument('-y','--year', 
    choices=["2011","2012","2015","2016","2017","2018"], 
    required=True,
    help="year of data taking/MC")       
parser.add_argument('-p','--plotsdir',  
    default="/home/blaised/private/Bc2D0MuNuX/XGB/plots/sw", 
    help="where to dump the plots for sWeights studies")    
parser.add_argument('-n','--pidcut', 
    action="store_true", 
    help="implement cut on muon ProbNNmu response>0.99 [default:False]")    
parser.add_argument('-s','--skim',  action="store_true", 
    help="read and persist only B_M(corr), D mass (+probnnmu if no cut) [default:skim]")    
parser.add_argument('-x','--entry_stop',  action="store_true", 
    help="cap on number of candidates read in [default:False]")    
parser.add_argument('-m','--masscut',  action="store_true", 
    help="impose lower-mass-bound at 3500 [default:False]")    
parser.add_argument('-c','--fitbranches', 
    default="/home/blaised/private/Bc2D0MuNuX/Pipeline/CONFIG/Branches/D0MuNu/FitVariables.yml",
    help="yml file with fit branches")    
parser.add_argument('-f','--features', 
    default="/home/blaised/private/Bc2D0MuNuX/XGB/yml/features.yml",
    help="yml file with training variables")    
parser.add_argument('-B','--allbranches', 
    action="store_true",
    help="read in all branches of input df")    
parser.add_argument('-F','--fitsel', 
    action="store_true",
    help="implement full offline selection [default:False]")    
parser.add_argument('-T','--comp', 
    choices=["sig", "misid_hadron_enriched"],
    help="fit component [choices:sig|misid]")    
opts = parser.parse_args()

with open(f"{opts.fitbranches}", 'r') as stream:
    FIT_BRANCHES = list(yaml.safe_load(stream).values())
with open(f"{opts.features}", 'r') as stream:
    TRAINING_VARS = [*yml_to_dict(opts.features)]

# patch
if opts.pidcut:
    opts.plotsdir = "/home/blaised/private/Bc2D0MuNuX/comb_study/plots/sw/ProbNN_sel"

def plot_indep_scatter_h(x, y, xlabel, ylabel, reduction_factor=1, save=None, show=False, 
    **kwargs):
    """Taken from the sweights repo
    Plot scatter of two variables.
    Plot a scatter graph of two variables and write the kendall tau
    coefficient.
    """
    try:
        import matplotlib.pyplot as plt
        import uncertainties as u
    except Exception:
        raise RuntimeError(
            """matplotlib and uncertainties packages must be installed to plot
            independence \npip install matplotlib \npip install
            uncertainties"""
        )

    fig, ax = plt.subplots()
    #ax.scatter(x[::reduction_factor], y[::reduction_factor], s=1)
    ax.hist2d(
        x=x[::reduction_factor],
        y=y[::reduction_factor],
        bins=30,
        cmap="Blues",
    )
    tau, err, pval = kendall_tau(x, y)
    ax.text(
        0.7,
        0.9,
        r"$\tau = " + str(u.ufloat(tau, err)).replace("+/-", r"\pm") + "$",
        transform=ax.transAxes,
        backgroundcolor="w",
    )
    ax.text(
        0.7,
        0.8,
        f"$p = {pval:.2f}$",
        transform=ax.transAxes,
        backgroundcolor="w",
    )
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.tight_layout()
    if save:
        fig.savefig(save)
    if show:
        plt.show()
    return fig, ax

# plotting routine
def plot_data(data, ax, bins, range, wts=None, label=None, col=None):
    nh, xe = np.histogram(data,bins=bins,range=range)
    cx = 0.5*(xe[1:]+xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
        whist.fill( data, weight = wts )
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance**0.5

    ax.errorbar(cx, nh, 
        yerr=err, 
        xerr = (xe[1]-xe[0])/2.,
        fmt='.', 
        color="black",
        markersize=5,
        elinewidth=1.,
        capsize=1, 
        markeredgewidth=1,
        label=label,
    )

def plot_wts(x, sw, bw,xlabel,save_name="(s)weights",title=None):
    fig,ax = plt.subplots()
    ax.plot(x, sw, 'b--', label='Signal')
    ax.plot(x, bw, 'r:' , label='Background')
    ax.plot(x, sw+bw, 'k-', label='Sum')
    ax.set_xlabel(xlabel)
    ax.set_ylabel('Weight [Arbitrary Units]')
    if title: ax.set_title(title)
    fig.tight_layout()
    plt.savefig(f"{opts.plotsdir}/{save_name}.pdf")
    plt.savefig(f"{opts.plotsdir}/{save_name}.png")

def sweight_dz_m():

    def model(x:"data againt which nll minimisation is performed",
        Ns, Nf, # yields: sig, fake-charm
        #f, mu, sg, a1, a2, n1, n2, # signal model parameters 
        mu, sg, a1, n1, # 1CB signal model parameters 
        lb, # exponential for fake charm
        comps=["sig","comb"], # processes included in the mass fit
    )->"fit Model where the yields are floated for extented unbinned NLL":
        
        # signal: two CB 
        #cb1 = crystalball(a1,n1, mu,sg)
        cb1_pdf = CB.pdf(x, a1,n1, mu,sg)
        cb1_cdf = CB.cdf(mrange, a1,n1, mu,sg)
        #cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
        #cb1 = crystalball_ex(bl,ml,sl,br,mr,sr,loc)
        #cb1_N = np.diff(cb1.cdf(mrange)) # normalisation due to restricted range
        #cb1_N = np.diff(crystalball_ex.cdf(x, bl,ml,sl,br,mr,sr,loc))
        #cb1_N = np.diff(cb1.cdf(mrange)) # normalisation due to restricted range
        cb1_N = np.diff(cb1_cdf)
        #cb2_N = np.diff(cb2.cdf(mrange)) # normalisation due to restricted range

        #invx = mrange[1]-x+mrange[0]
        #sig = f * cb1.pdf(x)/cb1_N + (1-f)*(1-cb2.pdf(invx)/cb2_N) 
        sig = cb1_pdf/cb1_N 
        
        # exponentail fake charm
        fake_charm_N = np.diff(expon.cdf(mrange, mrange[0], lb))
        #fake_charm_N = np.diff(fake_charm.cdf(mrange))  
        
        # putting everything together
        tot = 0
        if "sig" in comps:
            tot+=Ns * sig
        if "comb" in comps:
            tot+=Nf * expon.pdf(x, mrange[0], lb) / fake_charm_N 

        return tot

    # define mass pdf for iminuit fitting
    def mpdf(x, Ns, Nf, 
        # f, mu, sg, a1, a2, n1, n2,
        #bl, ml, sl, br, mr, sr, loc,
        mu, sg, a1, n1,
        lb,
        ):
        return (
            Ns+Nf, 
            model(
                x, Ns, Nf, mu, sg, a1, n1, lb,
                #x, Ns, Nf, bl, ml, sl, br, mr, sr, loc, lb,
            ) 
        )

    def plot_pull(
        data:"observation",
        ax:"pad",
        bins:"number of bins",
        range:"observable range considered in the fit",
        wts=None, # "weight->conditions the error calc",
    )->"pull plot":
        nh, xe = np.histogram(data,bins=bins,range=range)
        cx = 0.5*(xe[1:]+xe[:-1])

        fitted_model = N*model(cx,*mi.values)

        if wts is None:
            err = nh**0.5 # poisson error
        if wts is not None:
            ValueError("Need to integrate weighted histogram error")
        
        # make the pull
        pull = (nh-fitted_model)/(err) 

        # plot
        ax.fill_between(x, -3, 3, alpha=0.1, facecolor='forestgreen')
        ax.plot(data, np.zeros_like(data), c='forestgreen', ls='--', lw=1)
        ax.errorbar(
            x=cx,
            y=pull,
            xerr=0.5 * (xe[1:]-xe[:-1]),
            yerr=np.ones_like(cx),
            color="black",
            markersize=5,
            elinewidth=1,
            capsize=1, 
            fmt='.',
            markeredgewidth=1,
        )
        ax.set_ylabel(r'Pulls [$\sigma$]', loc="center")
        ylim = ax.get_ylim()
        y = max(abs(ylim[0]),abs(ylim[1]))
        ax.set_ylim(-5,5)

    # === fitting segment ===
    par_init={
        "Ns" : 5_000,
        "Nf" : 30_000,
        #"f": 0.5,
        "mu":1869.,
        "sg":10,
        "a1":2,
        #"a2":2,
        "n1":3,
        #"n2":3,
        "lb":1000,
    }
        
    if opts.masscut:
        visM_lo = 3500
    if not opts.masscut:
        visM_lo = 0

    if opts.skim==False:
        _expr = []
    if opts.skim:
        _expr = ["B_plus_MCORR", "D0_M", "B_plus_M", "Mu_plus_ProbNNmu"]

    # WS data and lower visM bound
    if opts.channel=="D0MuNu":
        _sel = f"(K_minus_ID*Mu_plus_ID>0)" # dcs
        _label = "DCS"
    else:
        _sel = f"(D0_ID*Mu_plus_ID<0)" # WS
        _label = "WS"
    
    obs_label=f"LHCb {_label} data"
    
    # check misID contamination
    if opts.pidcut:
        _sel+=" & (Mu_plus_ProbNNmu>0.99)"
        obs_label+=r", ProbNN$\mu>0.99$"

    print(c(f"Running with the following selection in place: {_sel}").cyan)

    events = uproot.open(
    f"{__storage__}/{opts.channel}/{opts.year}/Bc2{opts.channel}X_{opts.comp}.root:DecayTree")
    
    for b in list(events.keys()):
        if b in FIT_BRANCHES or b in TRAINING_VARS:
            _expr.append(b) # what needs to be propagated to the fitter

    # add fit vars
    for v in FIT_BRANCHES: 
        if v not in _expr:
            try:
                v in events.keys()
            except:
                print(f"{v} not in ROOT file. Skip loading.")
                pass
            else:
                print(f"{v} added to the variable persistence set")
    _expr.append("Mu_plus_ProbNNmu")
    _expr.append("misid_w")
    print(c(f"Full variable persistence set:").blue.underline)
    print(_expr)
    
    if opts.entry_stop:
        _stop = int(events.num_entries / 10)
    if not opts.entry_stop:
        _stop = None
        print(c("Full candidate set loaded, no cap.").magenta) 

    if opts.allbranches: _expr=None
    batch_size = "200 MB"
    bevs = events.num_entries_for(batch_size, _expr, entry_stop=_stop)
    tevs = events.num_entries
    nits = round(tevs/bevs+0.5)
    dataset = pd.DataFrame()
    container = []
    for batch in tqdm(events.iterate( 
        _expr, 
        library='pd', 
        step_size=batch_size, 
        entry_stop=_stop, 
        cut=_sel,
        ), total=nits,ascii=True,desc=f"/combined/ {_label} data, visM>{visM_lo} GeV"):
        assert(batch.B_plus_M.min()-visM_lo>1e-5)
        container.append(batch)
    
    dataset = pd.concat(container)
    breakpoint()
    if opts.pidcut: 
        assert(np.min(dataset.Mu_plus_ProbNNmu)>0.99)
        warnings.warn("ProbNNmu>0.99 selection in place.")
    if visM_lo>100:
        warnings.warn("Nonzero lower visM cut")
    assert(dataset.B_plus_M.min()>visM_lo)
    print(c(f"Loading complete: taken in {len(dataset)} candidates.\nExtracting sWeights..."))

    if opts.fitsel:
        print(c("Implementing FULL *nominal* offline selection").red)
        # implement also the final selection
        OFFICIAL_SELECTION = "( B_plus_FD_OWNPV<25 and\
         B_plus_SV_RXY<2 and\
        CosXY_Mu_plus_D0>-0.8 and \
        CosXYZ_Mu_plus_Pi_1 < 0.9997 and \
        CosXYZ_Mu_plus_K_minus < 0.9997 and \
        log(D0_FDCHI2_OWNPV)<9 and \
        log(D0_IPCHI2_OWNPV)<7.5 and \
        FitVar_El<3000 and \
        FitVar_Mmiss2>-0.5e7 and \
        FitVar_Mmiss2<0.7e7 and \
        log(K_minus_IPCHI2_OWNPV)<9 and \
        log(Mu_plus_IPCHI2_OWNPV)<9 and \
        Mu_plus_K_minus_M<5800 and \
        Mu_plus_Pi_1_M<5500 and \
        Mu_plus_PIDp<0 and \
        Mu_plus_PIDK<0 and \
        Mu_plus_ProbNNghost<0.1 and \
        (B_plus_FIT_LTIME/B_plus_MCORRERR)<0.00001 and \
        (B_plus_MCORRERR<650) and \
        B_plus_M>3500 and \
        B_plus_M<6275.6 and \
        (B_plus_FIT_LTIME>0.000 and B_plus_FIT_LTIME<=0.005) and \
        (D0_M>=1810 and D0_M<=1920)\
        )" # removed: (B_plus_MCORR>=4200 and B_plus_MCORR<=7200) and \
        dataset = dataset.query(OFFICIAL_SELECTION)
        raise Exception("WARNING: selection is deprecated. Please run without the cuts and optimise via XGB.") # deprecated 
    
    # === sFit ===
    mrange = [np.min(dataset.D0_M), np.max(dataset.D0_M)]
    BINS = int(100)
    Path(f"{opts.plotsdir}").mkdir(parents=True, exist_ok=True)

    mi = Minuit( ExtendedUnbinnedNLL(dataset.D0_M, mpdf, verbose=0), **par_init)
    mi.limits['Ns'] = (1e-10,len(dataset))
    mi.limits['Nf'] = (1e-10,len(dataset))
    #mi.limits['f'] = (0,1)
    mi.limits['mu'] = (1869.-30., 1869.+30.)
    mi.limits['sg'] = (0,20)
    mi.limits['a1'] = (0,20)
    #mi.limits['a2'] = (0,20)
    mi.limits['n1'] = (0,20)
    #mi.limits['n2'] = (0,20)
    mi.limits['lb'] = (0,1e10)
    # fixed
    mi.fixed['n1'] = True
    #mi.fixed['n2'] = True

    mi.migrad()
    mi.hesse()
    [print(i) for i in mi.params]
    
    print(c("mi sanity checks").yellow.underline)
    if mi.valid:
        print(c("mi valid: ").yellow, c(mi.valid).green)
    if not mi.valid:
        print(c("mi valid: ").yellow, c(mi.valid).red)
        #warnings.warn("WARNING: mi not valid/accurate. Continue at your own risk.")
    if mi.accurate:
        print(c("mi accurate: ").yellow, c(mi.accurate).green)
    if not mi.accurate:
        print(c("mi accurate: ").yellow, c(mi.accurate).red)
        #warnings.warn("WARNING: mi not valid/accurate. Continue at your own risk.")
    

    # === plotting segment ===
    binning_config = {
        "bins" : BINS,
        "range": mrange,
    }
    fig, ax = plt.subplots(2,1, figsize=(10, 9), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
    ax0 = ax[0]
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax0)
    plot_data(
        data = dataset.D0_M,
        ax = ax[0],
        label = obs_label,
        **binning_config
    )
    x = np.linspace(*mrange,int(400))
    N = (mrange[1]-mrange[0])/BINS
    ax0.plot(x, N*model(x,*mi.values), 'tab:blue', ls="-", lw=2, label='Total fit model' )

    # plot bkg
    bkg = N*model(x,*mi.values,comps=["comb"])
    ax0.fill_between(x, 0, bkg, alpha=0.7, facecolor='#8856a7', label='Fake charm')

    # plot sig
    sig = N*model(x,*mi.values,comps=["sig"])
    ax0.fill_between(x, bkg, bkg+sig, alpha=0.7, facecolor='#9ebcda', label='Real charm')
    if opts.masscut: ax0.plot([], [], "", label=r"$m(D^+\mu^+)>$"+f"{int(visM_lo)}"+r"MeV/$c^2$", color="white")
    ax0.set_ylim(bottom=0)
    ax0.set_ylabel(r"Candidates /"+f"({int((mrange[-1]-mrange[0])/BINS)} "+r"MeV$/c^2)$")
    legend = ax0.legend(loc="upper left", fontsize=18)
    legend.get_frame().set_edgecolor("white")


    plot_pull(
        data = dataset.D0_M,
        ax   = ax[1],
        **binning_config 
    )
    ax[1].set_xlabel(r"$m(K^-\pi^+ \pi^-)$   [MeV/$c^2$]")

    plt.savefig(f"{opts.plotsdir}/mKpi(pi)_fit_{_label}_{opts.comp}.png")
    plt.savefig(f"{opts.plotsdir}/mKpi(pi)_fit_{_label}_{opts.comp}.pdf")


    # === adopt the Kendall tau coeff to inspect the level of correlation with MCORR === 
    # -> aim for something in the neighbourhood of 0
    kts = kendall_tau(dataset.D0_M,dataset.B_plus_MCORR)
    print(c("Kendall tau:").green, c(pdg_format( kts[0], kts[1] )).green)
    plot_indep_scatter(
        dataset.D0_M,
        dataset.B_plus_MCORR,
        # xlabel=r"$m(K^-\pi^+\pi^+)$ [MeV/$c^2$]",
        # ylabel=r"$m_{corr}(D^+ \mu^+)$ [MeV/$c^2$]",
        reduction_factor=1,
        save=f"{opts.plotsdir}/kendall_D0M_BMCORR_{_label}.pdf"
        )

    # compare sweights and COW
    spdf = lambda m: model(m,*mi.values,comps=["sig"])
    bpdf = lambda m: model(m,*mi.values,comps=["comb"])

    # make the sweighter
    sweighter = SWeight(
        dataset.D0_M.to_numpy(), 
        [spdf,bpdf], 
        [mi.values['Ns'],mi.values['Nf']], 
        (mrange,), method='summation', 
        compnames=('sig','comb'), 
        verbose=True, checks=True
        )

    for meth, cls in zip( ['SW',], [sweighter,] ):

        # plot weights
        x = np.linspace(*mrange,400)
        swp = cls.get_weight(0,x)
        bwp = cls.get_weight(1,x)
        plot_wts(x, swp, bwp, xlabel=r"$m(K^-\pi^+ \pi^-)$   [MeV/$c^2$]")

    # === book columns in the dataframe ===
    dataset["sw"] = sweighter.get_weight(0,dataset.D0_M)
    dataset["bw"] = sweighter.get_weight(1,dataset.D0_M)

    # corrected mass
    fig, _ax = plt.subplots()
    plot_data(
        dataset.B_plus_MCORR,
        ax=_ax,
        bins=30,
        range=[4_200, 7_200],
        wts = dataset.sw,
        label=obs_label,
    )
    _ax.legend()
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=_ax)
    _ax.set_xlabel(r"$m_{\textrm{corr}}(D^+ \mu^+)$   [MeV/$c^2$]")
    _ax.set_ylabel(r"Candidates /"+f"({int((7_200-4_200)/30)} "+r"MeV$/c^2)$")
    _ax.set_ylim(bottom=0)
    plt.savefig(f"{opts.plotsdir}/(s)weighted_BMCORR_{_label}.pdf")
    plt.savefig(f"{opts.plotsdir}/(s)weighted_BMCORR_{_label}.png")

    # D mass, sanity check
    fig, _ax = plt.subplots()
    plot_data(
        dataset.D0_M,
        ax=_ax,
        bins=100,
        range=[dataset.D0_M.min(), dataset.D0_M.max()],
        wts = dataset.sw,
        label=obs_label,
    )
    _ax.legend()
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=_ax)
    _ax.set_xlabel(r"$m(K^-\pi^+ \pi^-)$   [MeV/$c^2$]")
    _ax.set_ylabel(r"Candidates /"+f"({int((dataset.D0_M.max()-dataset.D0_M.min())/100)} "+r"MeV$/c^2)$")
    plt.savefig(f"{opts.plotsdir}/(s)weighted_DM_{_label}.pdf")
    plt.savefig(f"{opts.plotsdir}/(s)weighted_DM_{_label}.png")

    # if not opts.pidcut:
    #     # investigate the real-charm probNN response
    #     fig, _ax = plt.subplots()
    #     plot_data(
    #         dataset.Mu_plus_ProbNNmu,
    #         ax=_ax,
    #         bins=100,
    #         range=[dataset.Mu_plus_ProbNNmu.min(), dataset.Mu_plus_ProbNNmu.max()],
    #         wts = dataset.sw,
    #         label=r"(s)Weighted WS data",
    #     )
    #     _ax.legend()
    #     hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=_ax)
    #     _ax.set_xlabel(r"Muon ProbNN$\mu$ response [Arbitrary Units]")
    #     _ax.set_ylabel(r"Candidates / (0.01 A.U.)")
    #     plt.savefig(f"{opts.plotsdir}/(s)weighted_ProbNNmu.pdf")
    #     plt.savefig(f"{opts.plotsdir}/(s)weighted_ProbNNmu.png")

    # write to pkl
    outfile = f"sw_{opts.channel}" 
    if opts.pidcut: 
        outfile+="_ProbNNcut"
    print(c(f"SUCESS: written sW file to {__storage__}/{opts.channel}/{opts.year}/{outfile}_{opts.comp}.pkl").green.underline)
    dataset.to_pickle(f"{__storage__}/{opts.channel}/{opts.year}/{outfile}_{opts.comp}.pkl")

if __name__ == '__main__':
    sweight_dz_m()