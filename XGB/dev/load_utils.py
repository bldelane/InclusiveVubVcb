"""Loading utilities"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from builtins import breakpoint
from header import *
import time 
from tqdm import tqdm

def timing(func):
    """decorator to print function execution time [ms]"""
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = func(*args, **kwargs)
        time2 = time.time()
        print('{:s} function took {:.3f} ms'.format(func.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

def check_argpath(func):
    """decorator to check input path exists"""
    def wrapper(feats_path, _key, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, _key)
        return features 
    return wrapper

# read  feature list
@check_argpath
def read_feats(
    feats_path:str,
    _key:str,
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    try:
        _key in in_dict
        feature_list = in_dict[_key]
    except ValueError:
        print(f"'{_key}' key not in dict")
    
    return feature_list

# read in a BOI file
def yml_to_dict(
    _path:"yml file",
)->dict:
    with open(_path, 'r') as stream:
        try:
            parsed_yaml=yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    
    return parsed_yaml

@timing
def load_concat_ROOT_file(
    _files:list,
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    **kwargs
)->"pandas dataframe, with name for labels in plots":
    """wrapper for uproot.concatenate"""
    df = uproot.concatenate(
        files=_files,
        expressions=_branches,
        cut=_cut,
        library=_library,
        **kwargs
    ); df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df

@timing
def oneshot_load_ROOT_file(
    _file:"string",
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    _max_entries:"cap on number of events read in"=None,
    **kwargs
)->"pandas dataframe, with name for labels in plots, supports entry_stop":
    events = uproot.open(
        f"{_file}"
    )
    df = events.arrays(
        expressions=_branches,
        cut=_cut,
        library=_library,
        entry_stop=_max_entries,
        **kwargs
    ); df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df

@timing
def batched_load_ROOT_file(
    _file:"string",
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    _max_entries:"cap on number of events read in"=None,
    _batch_size = "200 MB",
    **kwargs
)->"pandas dataframe, with name for labels in plots, supports entry_stop":
    """wrapper for uproot iterate"""
    events = uproot.open(
        f"{_file}"
    )
    bevs = events.num_entries_for(_batch_size, _branches, entry_stop=_max_entries)
    tevs = events.num_entries
    nits = round(tevs/bevs+0.5)
    aggr = []
    for batch in tqdm(events.iterate(
        expressions=_branches,
        cut=_cut,
        library=_library,
        entry_stop=_max_entries,
        step_size=_batch_size,
        **kwargs
        ), total=nits,ascii=True,desc=f"batches loaded"):
        aggr.append(batch)

    df = pd.concat(aggr)
    if _cut is None: assert(len(df) == events.num_entries)
    df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df