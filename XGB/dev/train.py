"""Study the combinatorial rejection via an MVAusing WS DpMuNu data
"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import warnings

warnings.simplefilter(
    action="ignore", category=FutureWarning
)  # kill annoying pandas index type warnings

from header import *  # basic utilities and cosmetics
from load_utils import *
from sklearn.model_selection import train_test_split
from scipy.stats import pearsonr
from xgboost import XGBClassifier
import xgboost as xgb
import pandas as pd
from sklearn.metrics import accuracy_score
import scipy
import seaborn as sn
from sklearn.preprocessing import QuantileTransformer
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sweights import kendall_tau
from iminuit.pdg_format import pdg_format
import yaml
from termcolor2 import c
from uncertainties import ufloat, unumpy

parser = ArgumentParser(description="eval configuration")
parser.add_argument(
    "-p",
    "--plotsdir",
    default="plots/mva",
    help="where to dump the plots for DpMuNu studies",
)
parser.add_argument(
    "-d",
    "--datadir",
    default="scratch/mva",
    help="directory with the data used to train the MVA",
)
parser.add_argument(
    "-P",
    "--phy_scale",
    action="store_true",
    help="apply Physics-based scaling [default:False]",
)
parser.add_argument(
    "-y",
    "--year",
    choices=["2011", "2012", "2015", "2016", "2017", "2018"],
    required=True,
    help="year of data taking/MC",
)
parser.add_argument(
    "-f",
    "--features",
    default="/home/blaised/private/Bc2D0MuNuX/XGB/yml/features.yml",
    help="yml file with training variables",
)
parser.add_argument(
    "-x",
    "--fitsel",
    action="store_true",
    help="implement full offline selection [default:False]",
)
parser.add_argument(
    "-F",
    "--fast",
    action="store_true",
    help="fast mode: turn off all plotting [default:False]",
)
parser.add_argument(
    "-V1",
    "--optim_comb",
    action="store_true",
    help="optimse mode: run hyperpas CV on XGB (antiComb, antiVcb) [default:False]",
)
parser.add_argument(
    "-V2",
    "--optim_vcb",
    action="store_true",
    help="optimse mode: run hyperpas CV on XGB (antiComb, antiVcb) [default:False]",
)
opts = parser.parse_args()


candidates_features = [*yml_to_dict(opts.features)]
print(c("Features under examination:").cyan.underline)
print(c(candidates_features).cyan)


# read data, construct feature and target matrices
dataset = pd.read_pickle(f"{opts.datadir}/dataset.pkl")
print(c("Dataset loaded").green)
X = dataset[candidates_features]  # feats
y = dataset["target"].astype(int)  # one-hot enc
w = dataset["sw"] - dataset["sw"].min()  # sweights (+dummy 1s for MC)
(
    X_train,
    X_test,
    y_train,
    y_test,
    w_train,
    w_test,
    mcorr_train,
    mcorr_test,
    _,
    test_dataset,
) = train_test_split(
    X, y, w, dataset.B_plus_MCORR, dataset, test_size=0.33, random_state=2310
)
print(c("Train/Test split enacted").green)


# plot correlation
Path(f"{opts.plotsdir}/train/corr").mkdir(parents=True, exist_ok=True)
boi = candidates_features + ["B_plus_MCORR"]
_sig = dataset[dataset.target == 1][boi]
_bkg = dataset[dataset.target == 0][boi]
_sig.name = "signal"
_bkg.name = "background"

if not opts.fast:
    for f in candidates_features:
        plt.figure()
        for d in [_sig, _bkg]:
            plt.hist(d[f], label=d.name, density=True, alpha=0.5)
            plt.legend()
        plt.savefig(f"plots/test/{f}.png")
        plt.close()

    for d in [_sig, _bkg]:
        with sn.axes_style("white"):
            mask = np.zeros_like(d.corr())
            mask[np.triu_indices_from(mask)] = True
            f, ax = plt.subplots(figsize=(30, 23))
            ax = sn.heatmap(
                d.corr(),
                annot=True,
                cmap="Spectral",
                fmt=".2f",
                vmin=-1.0,
                vmax=1.0,
                mask=mask,
                square=True,
                cbar_kws={"label": "Correlation [Arbitrary Units]"},
            )
            # ax.set_xticklabels(rotation=75, fontsize=10)
            # ax.set_yticklabels(fontsize=10)
            ax.text(
                s=r"LHCb Unofficial", x=0.0, y=1.05, transform=ax.transAxes, fontsize=40
            )
            ax.figure.axes[-1].yaxis.label.set_size(15)
        plt.savefig(f"{opts.plotsdir}/train/corr/{d.name}.pdf")
        plt.savefig(f"{opts.plotsdir}/train/corr/{d.name}.png")
        plt.close()

# optim
if opts.optim_comb:

    dataset = pd.concat(
        [
            dataset[dataset.target == 0][:10_000],
            dataset[dataset.target == 1][:10_000],
        ]
    )

    X = dataset[candidates_features]  # feats
    y = dataset["target"].astype(int)  # one-hot enc
    w = dataset["sw"] - dataset["sw"].min()  # sweights (+dummy 1s for MC)
    (
        X_train,
        X_test,
        y_train,
        y_test,
        w_train,
        w_test,
        mcorr_train,
        mcorr_test,
        _,
        test_dataset,
    ) = train_test_split(
        X, y, w, dataset.B_plus_MCORR, dataset, test_size=0.33, random_state=2310
    )

    hpars = {
        "learning_rate": [0.01, 0.10, 0.20, 0.30],
        "n_estimators": [300, 400, 500, 1000, 1500, 2000],
        "max_depth": [3, 5, 10, 20],
        "min_child_weight": [1, 3, 5, 7],
        "gamma": [0.0, 0.1, 0.3, 0.5, 1.0],
        "colsample_bytree": [0.1, 0.3, 0.5],
    }
    model = XGBClassifier(
        use_label_encoder=False,
        objective="binary:logistic",
        booster="gbtree",
        eval_metric="logloss",
        tree_method="gpu_hist",
    )
    clf = RandomizedSearchCV(
        estimator=model,
        param_distributions=hpars,
        scoring="roc_auc",
        n_iter=100,
        verbose=1,
        cv=3,
    )
    clf.fit(X_train, y_train)
    print("Best parameters:", clf.best_params_)


# fit model no training data
config_dict = {
    "n_estimators": 400,
    "min_child_weight": 1,
    "max_depth": 3,
    "learning_rate": 0.3,
    "gamma": 0.5,
    "colsample_bytree": 0.5,
    "tree_method": "gpu_hist",
}  # enable gpu
model = XGBClassifier(
    use_label_encoder=False,
    objective="binary:logistic",
    booster="gbtree",
    eval_metric="logloss",
    **config_dict,
)

print(c("Commence training...").green)
model.fit(X_train, y_train, sample_weight=w_train)
print(c("Training complete").green.bold)

if not opts.fast:
    # feature importance
    Path(f"plots/mva/train").mkdir(parents=True, exist_ok=True)
    with plt.style.context("science", "notebook"):
        fig, ax = plt.subplots()
        xgb.plot_importance(model)
        plt.savefig(f"plots/mva/train/feat_importance.pdf")
        plt.savefig(f"plots/mva/train/feat_importance.png")


# make predictions for test data
y_pred = model.predict(X_test)
predicted_label_test = [round(value) for value in y_pred]
preds_test = model.predict_proba(X_test)[::, 1]
preds_train = model.predict_proba(X_train)[::, 1]

# evaluate predictions
accuracy = accuracy_score(y_test, predicted_label_test)
fpr, tpr, _ = metrics.roc_curve(y_test, preds_test)
auc = metrics.roc_auc_score(y_test, preds_test)
print("Accuracy: %.2f%%" % (accuracy * 100.0))
print(f"ROC AUC = {auc}")


# ====================================================
#           PLOT ANTICOMB XGB RESPONSE
# ====================================================
fig, (ax_p, ax) = plt.subplots(
    2,
    1,
    # figsize=(10, 10),
    gridspec_kw={"height_ratios": [1, 3]},
    sharex=True,
)

# === plot FoM evaluated on TEST ===
# signal
cut_vals = np.linspace(0, 0.99, 1000)
pc = []
ps = []
for _c in cut_vals:
    sigs = preds_test[(y_test > 0)]
    S_cval = len(sigs[sigs > _c])
    S = ufloat(S_cval, np.sqrt(S_cval))

    bkgs = preds_test[(y_test <= 0)]
    B_cval = len(bkgs[bkgs > _c])
    B = ufloat(B_cval, np.sqrt(B_cval))
    purity = S / ((S + B) ** 0.5)

    pc.append(purity.n)
    ps.append(purity.s)

purities = unumpy.uarray(pc, ps)
print(c("AntiComb purities successfully computed.").green)

cut_vals = np.array(cut_vals)
pc = np.array(pc)
ps = np.array(ps)

ax_p.plot(cut_vals, pc, color="lightgrey", marker=".", ms=1)
ax_p.set_ylabel(
    r"$\frac{S}{\sqrt{S+B}}$",
)
ax_p.fill_between(cut_vals, pc - ps, pc + ps, color="lightgrey", alpha=0.5)

OPTIMAL_CUT = cut_vals[np.argmax(pc)]
opt_sel_antiComb = OPTIMAL_CUT
for _ax in ax, ax_p:
    _ax.axvline(OPTIMAL_CUT, color="black", ls="-.")
ax_p.plot(OPTIMAL_CUT, np.max(pc), color="#fd8d3c", marker="*")
ax_p.set_ylim(top=np.max(pc) + np.max(pc) * 0.15)

# === response hists ====
plt_config = {
    "bins": 50,
    "range": [0, 1],
}
norm_hist(
    _branch=preds_train[y_train > 0],
    _ax=ax,
    histtype="stepfilled",
    alpha=0.33,
    color="#3288bd",
    label="Signal, Train",
    **plt_config,
    zorder=0,
)
norm_hist(
    _branch=preds_train[y_train <= 0],
    _ax=ax,
    color="#d53e4f",
    histtype="stepfilled",
    alpha=0.33,
    label="Background, Train",
    **plt_config,
    zorder=1,
)
h, bins = np.histogram(preds_test[y_test > 0], **plt_config)
hep.histplot(
    h,
    bins,
    yerr=np.sqrt(h),
    ax=ax,
    color="tab:blue",
    label="Signal, Test",
    density=True,
    zorder=2,
)

h, bins = np.histogram(preds_test[y_test <= 0], **plt_config)
hep.histplot(
    h,
    bins,
    yerr=np.sqrt(h),
    ax=ax,
    color="firebrick",
    label="Background, Test",
    density=True,
    zorder=3,
)

ax.plot(
    [],
    [],
    color="white",
    label=f"ROC AUC score = {auc:.3}",
    zorder=4,
)
ax.plot([], [], color="white", label=f"Accuracy = {accuracy*100.0:.0f}\%", zorder=5)
for t in [0, 1]:
    kts = kendall_tau(
        preds_test[test_dataset.target == t],
        test_dataset[test_dataset.target == t]["B_plus_MCORR"],
    )
    print(f"Kendall Tau coeff on test, class {str(t)}: ", pdg_format(kts[0], kts[1]))
    ax.plot(
        [],
        [],
        color="white",
        label=r"Kendall $\tau_{%s}=$ " % t + f"{pdg_format(kts[0], kts[1])}",
        zorder=6 + t,
    )

# order of legend handles
handles, labels = plt.gca().get_legend_handles_labels()

# specify order of items in legend
order = [0, 1, 6, 7, 2, 3, 4, 5]

# add legend to plot
ax.legend(
    [handles[idx] for idx in order],
    [labels[idx] for idx in order],
    bbox_to_anchor=(0.5, -0.6),
    loc="center",
    ncol=2,
)
ax_p.set_title(
    "LHCb Unofficial 2018 (13 TeV)",
)

ax.set_xlabel("Response [Arbitrary Units]")
ax.set_ylabel("Candidates")
# ax.set_yscale("log")
ax.set_ylim(bottom=0)
plt.savefig("plots/mva/train/response_antiComb.png")
plt.savefig("plots/mva/train/response_antiComb.pdf")
plt.close()

if not opts.fast:
    # save trained model
    model.save_model(
        "/home/blaised/private/Bc2D0MuNuX/XGB/scratch/mva/trained_model.json"
    )

# concat WS; sig;
dataset = pd.concat([dataset[dataset.target == 0], dataset[dataset.target == 1]])

# assign prediction
dataset["XGB_antiComb"] = model.predict_proba(dataset[candidates_features])[::, 1]
dataset.to_pickle("scratch/xgb_sw_dataset.pkl")
print(c("Output file written to scratch/xgb_sw_dataset.pkl").green.underline)


# inference
from termcolor2 import c

print(c("Moving onto inference. Reading in DCS data...").cyan)
if opts.fast:
    _stop = 10_000
    print(c(f"Cap of {_stop} candidates enacted").magenta)
if not opts.fast:
    _stop = None
    print(c(f"None cap on candidates").yellow)

# dcs_events = uproot.open(f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/{opts.year}/Bc2D0MuNuX_sig.root:DecayTree")
dcs_vars = list(dataset.keys())
dcs_vars.remove("target")
dcs_vars.remove("bw")
# dcs_vars.remove("B_plus_DIRA_OWNPV")
dcs_vars.remove(
    "XGB_antiComb"
)  # removed, as dcs does not seem to have this for some reasonc
dcs_vars.remove("D0_ID")  # removed, as dcs does not seem to have this for some reasonc


# =================================================
#                   Read DCS
# =================================================
dcs_data = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/2018/sw_D0MuNu.pkl"
).query("K_minus_ID*Mu_plus_ID>0")[dcs_vars]
print(c(f"SUCCESS: read in {len(dcs_data)} candidates").blue)
print(c(f"Full offline selectin implemented; {len(dcs_data)} survived.").bold)

if not opts.phy_scale:
    print(c("NOTE: no scaling activated").yellow)

if opts.phy_scale:
    print(c("===>Preprocess activated").green)
    feats_dict = yml_to_dict(opts.features)
    # scale, where needed
    for df in [
        dcs_data,
    ]:
        for f in candidates_features:
            if feats_dict[f] == "acos":
                df[f] = np.arccos(df[f])
            if feats_dict[f] == "log":
                df[f] = np.log(df[f])
            if feats_dict[f] == "gev":
                df[f] /= 1000.0
            if feats_dict[f] == "10x":
                df[f] *= 10.0
            if feats_dict[f] == None:
                pass

X_dcs = dcs_data[candidates_features]
dcs_data["XGB_antiComb"] = model.predict_proba(X_dcs)[::, 1]

# =================================================
#               AntiComb Sanity Plots
# =================================================
if not opts.fast:
    Path(f"plots/mva/train/antiComb/checks").mkdir(parents=True, exist_ok=True)
    with plt.style.context("vibrant"):

        ws = dataset[(dataset.target == 0) & (dataset.B_plus_M > 3500)]
        sig = dataset[(dataset.target == 1) & (dataset.B_plus_M > 3500)]

        mcorr_plt = {
            "range": [3_000, 12_000],
            "bins": 30,
        }
        d_plt = {
            "range": [1810, 1930],
            "bins": 30,
        }

        # >>> CORRECTED MASS
        # plot WS MCORR pre- and post-optimal cut
        fig, ax = plt.subplots()
        norm_hist(
            _branch=ws.B_plus_MCORR,
            _sw=ws.sw,
            _ax=ax,
            **mcorr_plt,
            label=r"$sW$ WS data",
            density=True,
            _isnorm=False,
            histtype="step",
        )

        norm_hist(
            _branch=ws.query(f"XGB_antiComb>0.2").B_plus_MCORR,
            _sw=ws.query(f"XGB_antiComb>0.2").sw,
            _ax=ax,
            label=r"$sW$ WS data, XGB$>0.2$",
            histtype="step",
            density=True,
            _isnorm=False,
            **mcorr_plt,
        )
        norm_hist(
            _branch=ws.query(f"XGB_antiComb>0.5").B_plus_MCORR,
            _sw=ws.query(f"XGB_antiComb>0.5").sw,
            _ax=ax,
            label=r"$sW$ WS data, XGB$>0.5$",
            histtype="step",
            density=True,
            _isnorm=False,
            **mcorr_plt,
        )
        norm_hist(
            _branch=ws.query(f"XGB_antiComb>{OPTIMAL_CUT}").B_plus_MCORR,
            _sw=ws.query(f"XGB_antiComb>{OPTIMAL_CUT}").sw,
            _ax=ax,
            label=r"$sW$ WS data, XGB$>\kappa$",
            density=True,
            _isnorm=False,
            **mcorr_plt,
        )
        ax.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]")
        ax.set_ylabel("Candidates, normalised")
        ax.set_title("LHCb Unofficial 2018 (13 TeV)")
        ax.legend(fontsize=7)

        plt.savefig("plots/mva/train/antiComb/checks/ws_mcorr.png")
        plt.savefig("plots/mva/train/antiComb/checks/ws_mcorr.pdf")

    # >>>> D mass
    with plt.style.context("high-contrast"):
        fig, ax = plt.subplots()
        norm_hist(
            _branch=ws.D0_M,
            _ax=ax,
            **d_plt,
            label=r"WS data",
            density=True,
            _isnorm=False,
            histtype="step",
        )
        norm_hist(
            _branch=ws.query(f"XGB_antiComb>{OPTIMAL_CUT}").D0_M,
            _ax=ax,
            label=r"WS data, XGB$>\kappa$",
            density=True,
            _isnorm=False,
            **d_plt,
        )
        ax.set_xlabel(r"Charm invariant mass [MeV/$c^2$]")
        ax.set_ylabel("Candidates, normalised")
        ax.set_title("LHCb Unofficial 2018 (13 TeV)")
        ax.legend(fontsize=7)

        plt.savefig("plots/mva/train/antiComb/checks/ws_charm_invmass.png")
        plt.savefig("plots/mva/train/antiComb/checks/ws_charm_invmass.pdf")


# =================================================
#               Anti-Vcb segment
# =================================================
from termcolor2 import c

print(c("Moving onto CF inference. Reading in CF data...").cyan)

if not opts.fast:
    _stop_cf = 5_000_000
if opts.fast:
    _stop_cf = 10_000

cf_data = batched_load_ROOT_file(
    _file=f"/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/{opts.year}/Bc2D0MuNuX_sig.root:DecayTree",
    _branches=dcs_vars.remove("sw"),
    _cut="(K_minus_ID*Mu_plus_ID<0)",
    _name="cf",
    _max_entries=_stop_cf,
)
cf_data["target"] = 2

print(c(f"SUCCESS: read in {len(cf_data)} candidates").blue)

if not opts.phy_scale:
    print(c("NOTE: no scaling activated").yellow)

if opts.phy_scale:
    print(c("===>Preprocess activated").green)
    feats_dict = yml_to_dict(opts.features)
    # scale, where needed
    for df in [
        cf_data,
    ]:
        for f in candidates_features:
            if feats_dict[f] == "acos":
                df[f] = np.arccos(df[f])
            if feats_dict[f] == "log":
                df[f] = np.log(df[f])
            if feats_dict[f] == "gev":
                df[f] /= 1000.0
            if feats_dict[f] == "10x":
                df[f] *= 10.0
            if feats_dict[f] == None:
                pass


X_cf = cf_data[candidates_features]
cf_data["XGB_antiComb"] = model.predict_proba(X_cf)[::, 1]
signal_size = len(dataset[dataset.target == 1])
if opts.fast:
    signal_size = 2_000


# include also the hadron-enriched sample, not extrapolated
from termcolor2 import c

print(c("Moving onto hadron enriched inference. Reading in CF data...").cyan)

if not opts.fast:
    _stop_cf = signal_size
if opts.fast:
    _stop_cf = 10_000


he_data = pd.read_pickle(
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined/D0MuNu/2018/sw_D0MuNu_misid_hadron_enriched.pkl"
).query("(K_minus_ID*Mu_plus_ID>0)")
print(c(f"SUCCESS: read in HADRON-ENRICHED with {len(cf_data)} candidates").blue)
if not opts.phy_scale:
    print(c("NOTE: no scaling activated").yellow)

if opts.phy_scale:
    print(c("===>Preprocess activated").green)
    feats_dict = yml_to_dict(opts.features)
    # scale, where needed
    for df in [
        cf_data,
    ]:
        for f in candidates_features:
            if feats_dict[f] == "acos":
                df[f] = np.arccos(df[f])
            if feats_dict[f] == "log":
                df[f] = np.log(df[f])
            if feats_dict[f] == "gev":
                df[f] /= 1000.0
            if feats_dict[f] == "10x":
                df[f] *= 10.0
            if feats_dict[f] == None:
                pass


X_he = he_data[candidates_features]
he_data["XGB_antiComb"] = model.predict_proba(X_he)[::, 1]
he_data["target"] = 4
signal_size = len(dataset[dataset.target == 1])
if opts.fast:
    signal_size = 2_000


dcs_data["target"] = int(3)

comps = pd.concat(
    [
        dataset[dataset.target == 0],  # resampled ws
        dataset[dataset.target == 1],
        cf_data[:signal_size],  # seems to effectively allow for the training
        dcs_data,
        he_data,
    ],
    ignore_index=True,
)

comps["rel_mcorrerr"] = comps.B_plus_MCORRERR / comps.B_plus_MCORR

XGB_CUT = OPTIMAL_CUT
# print(c(f"Proceeding with the antiVcb XGB. AntiComb cut at {XGB_CUT}").magenta.bold)
# comps = comps[comps.xgb>XGB_CUT]


# train xgboost anti-Vcb
feats_2d = ["B_plus_MCORRERR", "B_plus_FIT_LTIME"]
comps["target"] = comps["target"].map(
    {2: 0, 1: 1, 0: 2, 3: 3, 4: 4}
)  # default mapping of labels; 0 for bkg, 1 for bkg
_comps = comps[comps.target < 2]
_ws = comps[comps.target == 2]

X = _comps[feats_2d]
y = _comps["target"].astype("int")

(
    X_train,
    X_test,
    y_train,
    y_test,
    mcorr_train,
    mcorr_test,
    _,
    test_dataset,
) = train_test_split(
    X, y, _comps.B_plus_MCORR, _comps, test_size=0.33, random_state=2310
)

if opts.fast:
    print(c(f"Running in FAST mode: X dimension: {len(X)}"))

# optim
if opts.optim_vcb:
    hpars = {
        "learning_rate": [0.01, 0.10, 0.20, 0.30],
        "n_estimators": [300, 400, 500, 1000, 1500, 2000],
        "max_depth": [3, 5, 10, 20],
        "min_child_weight": [1, 3, 5, 7],
        "gamma": [0.0, 0.1, 0.3, 0.5, 1.0],
        "colsample_bytree": [0.1, 0.3, 0.5, 0.9],
    }
    model = XGBClassifier(
        use_label_encoder=False,
        objective="binary:logistic",
        booster="gbtree",
        eval_metric="logloss",
        tree_method="gpu_hist",
    )
    clf = RandomizedSearchCV(
        estimator=model,
        param_distributions=hpars,
        scoring="roc_auc",
        n_iter=100,
        verbose=1,
        cv=3,
    )
    clf.fit(X_train, y_train)
    print("Best parameters:", clf.best_params_)


# fit model no training data
config_dict = {
    "n_estimators": 1500,
    "min_child_weight": 7,
    "max_depth": 10,
    "learning_rate": 0.3,
    "gamma": 0.5,
    "colsample_bytree": 0.3,
    "tree_method": "gpu_hist",
    "monotone_constraints": "(0, 0)",
}
model = XGBClassifier(
    use_label_encoder=False,
    objective="binary:logistic",
    booster="gbtree",
    eval_metric="logloss",
    **config_dict,
)

model.fit(X_train, y_train)

# make predictions for test data
y_pred = model.predict(X_test)
predicted_label_test = [round(value) for value in y_pred]
preds_test = model.predict_proba(X_test)[::, 1]
preds_train = model.predict_proba(X_train)[::, 1]

# evaluate predictions
accuracy = accuracy_score(y_test, predicted_label_test)
fpr, tpr, _ = metrics.roc_curve(y_test, preds_test)
auc = metrics.roc_auc_score(y_test, preds_test)
print("Accuracy: %.2f%%" % (accuracy * 100.0))
print(f"ROC AUC = {auc}")

comps["XGB_antiVcb"] = model.predict_proba(comps[feats_2d])[::, 1]
comps.to_pickle("scratch/xgb2_sw_dataset.pkl")

# append to dcs_data df for plots
dcs_data["XGB_antiVcb"] = model.predict_proba(dcs_data[feats_2d])[::, 1]

# ====================================================
#           PLOT ANTIVCB XGB RESPONSE
# ====================================================
fig, (ax_p, ax) = plt.subplots(
    2,
    1,
    gridspec_kw={"height_ratios": [1, 3]},
    sharex=True,
)

# === plot FoM evaluated on TEST ===
# signal
cut_vals = np.linspace(0, 0.93, 1000)
pc = []
ps = []
for _c in cut_vals:
    sigs = preds_test[(y_test > 0)]
    S_cval = len(sigs[sigs > _c])
    S = ufloat(S_cval, np.sqrt(S_cval))

    bkgs = preds_test[(y_test <= 0)]
    B_cval = len(bkgs[bkgs > _c])
    B = ufloat(B_cval, np.sqrt(B_cval))

    if S.n == 0:
        purity = ufloat(1e-9, 1e-9)
    else:
        purity = S / ((S + B) ** 0.5)

    pc.append(purity.n)
    ps.append(purity.s)

purities = unumpy.uarray(pc, ps)
print(c("AntiVcb purities successfully computed.").green)

cut_vals = np.array(cut_vals)
pc = np.array(pc)
ps = np.array(ps)

ax_p.plot(cut_vals, pc, color="lightgrey", marker=".", ms=1)
ax_p.set_ylabel(
    r"$\frac{S}{\sqrt{S+B}}$",
)
ax_p.fill_between(cut_vals, pc - ps, pc + ps, color="lightgrey", alpha=0.5)

OPTIMAL_CUT = cut_vals[np.argmax(pc)]
opt_sel_antiVcb = OPTIMAL_CUT
for _ax in ax, ax_p:
    _ax.axvline(OPTIMAL_CUT, color="black", ls="-.")
ax_p.plot(OPTIMAL_CUT, np.max(pc), color="#fd8d3c", marker="*")
ax_p.set_ylim(top=np.max(pc) + np.max(pc) * 0.15)
# === response hists ====
plt_config = {
    "bins": 50,
    "range": [0, 1],
}
norm_hist(
    _branch=preds_train[y_train > 0],
    _ax=ax,
    histtype="stepfilled",
    alpha=0.33,
    color="#3288bd",
    label="Signal, Train",
    **plt_config,
    zorder=0,
)
norm_hist(
    _branch=preds_train[y_train <= 0],
    _ax=ax,
    color="#d53e4f",
    histtype="stepfilled",
    alpha=0.33,
    label="Background, Train",
    **plt_config,
    zorder=1,
)
h, bins = np.histogram(preds_test[y_test > 0], **plt_config)
hep.histplot(
    h,
    bins,
    yerr=np.sqrt(h),
    ax=ax,
    color="tab:blue",
    label="Signal, Test",
    density=True,
    zorder=2,
)

h, bins = np.histogram(preds_test[y_test <= 0], **plt_config)
hep.histplot(
    h,
    bins,
    yerr=np.sqrt(h),
    ax=ax,
    color="firebrick",
    label="Background, Test",
    density=True,
    zorder=3,
)

ax.plot(
    [],
    [],
    color="white",
    label=f"ROC AUC score = {auc:.3}",
    zorder=4,
)
ax.plot([], [], color="white", label=f"Accuracy = {accuracy*100.0:.0f}\%", zorder=5)
for t in [0, 1]:
    kts = kendall_tau(
        preds_test[test_dataset.target == t],
        test_dataset[test_dataset.target == t]["B_plus_MCORR"],
    )
    print(f"Kendall Tau coeff on test, class {str(t)}: ", pdg_format(kts[0], kts[1]))
    ax.plot(
        [],
        [],
        color="white",
        label=r"Kendall $\tau_{%s}=$ " % t + f"{pdg_format(kts[0], kts[1])}",
        zorder=6 + t,
    )

# order of legend handles
handles, labels = plt.gca().get_legend_handles_labels()

# specify order of items in legend
order = [0, 1, 6, 7, 2, 3, 4, 5]

# add legend to plot
ax.legend(
    [handles[idx] for idx in order],
    [labels[idx] for idx in order],
    bbox_to_anchor=(0.5, -0.6),
    loc="center",
    ncol=2,
)

ax_p.set_title(
    "LHCb Unofficial 2018 (13 TeV)",
)
ax.set_xlabel("Response [Arbitrary Units]")
ax.set_ylabel(
    "Candidates",
)
# ax.set_yscale("log")
ax.set_ylim(bottom=0)
plt.savefig("plots/mva/train/response_antiVcb.png")
plt.savefig("plots/mva/train/response_antiVcb.pdf")
plt.close()


from matplotlib import cm
import seaborn as sns

x_range = [0, 2_000]
y_range = [0, 0.01]

fig, ax = plt.subplots()
sns.kdeplot(
    x=comps[comps.target == 1][feats_2d[0]][:1000],
    y=comps[comps.target == 1][feats_2d[1]][:1000],
    cmap="Blues",
    shade=True,
    thresh=0.01,
    ax=ax,
    # levels = [0.10, 0.25, 0.50, 0.68, 0.9, 1.0],
    label=r"$B_c^+ \to D^{(*)0}\mu^+ \nu_{\mu}$",
    cbar=False,
)
sns.kdeplot(
    x=comps[comps.target == 0][feats_2d[0]][:1000],
    y=comps[comps.target == 0][feats_2d[1]][:1000],
    cmap="flare",
    shade=False,
    thresh=0.01,
    # levels = [0.10, 0.25, 0.50, 0.68, 0.9, 1.0],
    ax=ax,
    label="CF Data",
    legend=True,
    cbar=False,
)
x1grid = np.arange(0, comps[feats_2d[0]].max(), 100)
x2grid = np.arange(0, comps[feats_2d[1]].max(), 0.0001)
xx, yy = np.meshgrid(x1grid, x2grid)  # ensure feature order is correct
r1, r2 = xx.flatten(), yy.flatten()
r1, r2 = r1.reshape((len(r1), 1)), r2.reshape((len(r2), 1))
grid = np.hstack((r1, r2))
Z = model.predict_proba(grid)[::, 1].reshape(xx.shape)

CS = ax.contour(
    xx,
    yy,
    Z,
    levels=[
        OPTIMAL_CUT,
    ],
    colors="black",
)
ax.clabel(CS, inline=True)

ax.set(
    xlim=(0, 1500),
    ylim=(0.0, 0.0045),
    xlabel=r"$\delta m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]",
    ylabel=r"$\tau(X_b)$ [ns]",
)

ax.set_title(
    "LHCb Unofficial 2018 (13 TeV)",
)
trans = ax.get_yaxis_transform()
y = 0.002
ax.axhspan(0.0020, 0.0045, alpha=0.5, facecolor="lightgrey")

ax.text(
    0.675,
    0.0017,
    r"$\varepsilon_{\chi} =$"
    + r" ${:.0f}\%$".format(
        len(comps[comps.target == 1].query(f"XGB_antiVcb>{OPTIMAL_CUT}"))
        / len(comps[comps.target == 1])
        * 100.0
    ),
    color="black",
    transform=trans,
)
ax.text(
    0.675,
    0.0013,
    r"$\varepsilon_{\tau}= $"
    + r" ${:.0f}\%$".format(
        len(comps[comps.target == 1].query("B_plus_FIT_LTIME<0.002"))
        / len(comps[comps.target == 1])
        * 100.0
    ),
    color="black",
    transform=trans,
)

plt.savefig("plots/mva/train/contours_antiVcb.png")
plt.savefig("plots/mva/train/contours_antiVcb.pdf")
plt.close()

# ====================================================
#       Subsequent selection stage on DCS data
# ====================================================
with plt.style.context("science"):
    fig, ax = plt.subplots()

    base_sel = "B_plus_M>3500 and B_plus_M<6274.9"
    mcorr_plt = {
        "range": [4_200, 7_200],
        "bins": 30,
    }
    ax.hist(
        dcs_data.query(base_sel).B_plus_MCORR,
        weights=dcs_data.query(base_sel).sw,
        label=r"$m(D^0\mu^+)$",
        histtype="step",
        **mcorr_plt,
    )
    sel = f"{base_sel} and XGB_antiComb>{opt_sel_antiComb}"
    ax.hist(
        dcs_data.query(sel).B_plus_MCORR,
        weights=dcs_data.query(sel).sw,
        label=r"$m(D^0\mu^+)$, $\chi_{comb}$",
        histtype="step",
        **mcorr_plt,
    )
    sel = f"{base_sel} and XGB_antiComb>{opt_sel_antiComb}\
                and XGB_antiVcb>{opt_sel_antiVcb}"
    ax.hist(
        dcs_data.query(sel).B_plus_MCORR,
        weights=dcs_data.query(sel).sw,
        label=r"$m(D^0\mu^+)$, $\chi_{comb}$, $\chi_{V_{cb}}$",
        histtype="step",
        **mcorr_plt,
    )
    sel = f"{base_sel} and XGB_antiComb>{opt_sel_antiComb}\
                and XGB_antiVcb>{opt_sel_antiVcb}\
                and B_plus_FIT_LTIME<0.0020"
    ax.hist(
        dcs_data.query(sel).B_plus_MCORR,
        weights=dcs_data.query(sel).sw,
        label=r"$m(D^0\mu^+)$, $\chi_{comb}$, $\chi_{V_{cb}}$, $\tau(X_b)$",
        histtype="stepfilled",
        color="tab:blue",
        **mcorr_plt,
    )

    # ax.hist(
    #     dcs_data.query(
    #         f"{base_sel} and B_plus_FIT_LTIME<0.020"
    #     ).B_plus_MCORR,
    #     label=r"$m(D^0\mu^+)$, $\tau(X_b)$",
    #     histype="step",
    #     **mcorr_plt
    # )
    ax.legend(fontsize=7)
    ax.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]")
    ax.set_ylabel("Candidates, normalised")
    ax.set_title(f"$sW$ DCS LHCb Unofficial 2018 (13 TeV)", fontsize=10)

    plt.savefig("plots/dcs_sel_stages.png")
    plt.savefig("plots/dcs_sel_stages.pdf")

print(c("Optimal cuts:").red.underline)
print(c(f"Comb: {opt_sel_antiComb}\nAntiVcb: {opt_sel_antiVcb}").red)
