"""XGB class for training and inference"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"
__feats__  = "/home/blaised/private/Bc2D0MuNuX/XGB/yml/features.yml"

from builtins import breakpoint
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) # kill annoying pandas index type warnings
from header import * # basic imports and cosmetics
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score
import seaborn as sn
from sklearn.preprocessing import QuantileTransformer
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sweights import kendall_tau
from iminuit.pdg_format import pdg_format
from load_utils import *

class xgb:
    """XGB config for: 
        - preprocessing, if required
        - training
        - optimisation
        - inference
    """
    def __init__(self,
        dataset:"dataframe"=None, # dataframe 
        features:list=None, # feature list
        n_est=400, # estimators
        lr=0.1, # learning rate
        depth=3, # max depth,
        **params
    )->"xgb classifier":
        """initialise the classifier anatomy"""
        self.dataset  = dataset,
        self.n_est = n_est,
        self.lr    = lr,
        self.depth = depth,
        self.features = features,
        self.params= params,
    
    @property
    def dataset(self):
        return pd.DataFrame(self._dataset)
    
    @dataset.setter 
    def dataset(self, dataset):
        self._dataset = dataset
    
    @dataset.deleter 
    def dataset(self, dataset):
        del self._dataset

    def preprocess(self,)->"dataframe":
        """scale/transform variables as instructed in feats dict
        self.data update accordingly"""
        feats_dict = yml_to_dict(__feats__)
        _pre = self.dataset
        dataset = self.dataset
        for f in self.features:
            if feats_dict[f]=="acos":
                dataset[f] = np.arccos(dataset[f])
            if feats_dict[f]=="log":
                dataset[f] = np.log(dataset[f])
            if feats_dict[f]=="gev":
                dataset[f] /= 1000.
            if feats_dict[f]=="10x":
                dataset[f] *= 10.
            if feats_dict[f]==None:
                pass
            
        print("SUCCESS: preprocessing enacted. All training features to O(1).")


test = pd.read_pickle(f"scratch/mva/dataset.pkl")
test.name = test

mva = xgb()
mva.dataset = test
mva.features = [*yml_to_dict(__feats__)]
print(mva.dataset.B_plus_DIRA_OWNPV)
mva.preprocess()
print(mva.dataset.B_plus_DIRA_OWNPV)


# self.params.update({
#             "objective"         :   "binary:logistic",
#             "booster"           :   "gbtree",
#             "eval_metric"       :   "logloss",
#             "use_label_encoder" :   False,    
#             })
