from argparse import ArgumentParser
import uproot
import glob
import numpy as np
from tqdm import tqdm
import pandas as pd
from pathlib import Path
import yaml
import matplotlib.pyplot as plt
import mplhep as hep
import seaborn as sns

# cosmetics
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "axes.labelsize": 20,
    "axes.titlesize": 20,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 17,
    "ytick.labelsize": 20,
    "xtick.labelsize": 20,
})
sns.set_palette("viridis")

parser = ArgumentParser()
parser.add_argument('-y','--year', default='2016', help='Year')
parser.add_argument('-S','--sim' , default=False, action='store_true', help='Simulation?')
parser.add_argument('-n','--nfiles', default=-1, type=int, help='Number of files to process')
parser.add_argument('-E','--eospath',  
    default = "/eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4",
    help="main path on eos to the JpsiMuNu data ROOT files")    
parser.add_argument('-f','--feats',  
    default = "../features.yml",
    help="path yaml file with all anti-WS training features")    
args = parser.parse_args()

# pick up the names for the files on xrootd 
from snakemake.remote.XRootD import RemoteProvider as XRootDRemoteProvider
XRootD = XRootDRemoteProvider(stay_on_remote=True)
# data
MU_data_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+"/MU/{n}.root").n
MD_data_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+"/MD/{n}.root").n
# mc
MU_mc_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MU/MC_Bc2JpsiPi_"+args.year+"_{n}_pidgen.root").n
MD_mc_prefixs = XRootD.glob_wildcards("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MD/MC_Bc2JpsiPi_"+args.year+"_{n}_pidgen.root").n

import os
os.system('mkdir -p plots')
os.system(f'mkdir -p dfs/{args.year}')

def check_argpath(func):
    """decorator to check input path exists"""
    def wrapper(feats_path, _key, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, _key)
        return features 
    return wrapper

# read  feature list
@check_argpath
def read_feats(
    feats_path:str,
    _key:str,
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    try:
        _key in in_dict
        feature_list = in_dict[_key]
    except ValueError:
            print(f"'{_key}' key not in dict")
    
    return feature_list

# read
data = not args.sim
aliases = None

datatrees = []
if data:
    [datatrees.append("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+f"/MU/{u}.root:B2JpsiMuNuXTuple/DecayTree") for u in MU_data_prefixs]
    [datatrees.append("root://eoslhcb.cern.ch/"+args.eospath+"/DATA/JpsiMuNu/"+args.year+f"/MD/{d}.root:B2JpsiMuNuXTuple/DecayTree") for d in MD_data_prefixs]  
    tmp_mu = [i for i in datatrees if "MU" in i]; assert(len(tmp_mu)==len(MU_data_prefixs))
    tmp_md = [i for i in datatrees if "MD" in i]; assert(len(tmp_md)==len(MD_data_prefixs))
    assert(len(datatrees)==len(MU_data_prefixs)+len(MD_data_prefixs))
    aliases   = { 'Mu_1_PIDmu_corr' : 'Mu_1_PIDmu',
                  'Mu_2_PIDmu_corr' : 'Mu_2_PIDmu',
                  'Mu_plus_PIDmu_corr' : 'Mu_plus_PIDmu'}
else:
    [datatrees.append("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MU/MC_Bc2JpsiPi_"+args.year+f"_{u}_pidgen.root:DecayTree") for u in MU_mc_prefixs]
    [datatrees.append("root://eoslhcb.cern.ch/"+args.eospath+"/MC/JpsiMuNu/"+args.year+"/MD/MC_Bc2JpsiPi_"+args.year+f"_{d}_pidgen.root:DecayTree") for d in MD_mc_prefixs]
    tmp_mu = [i for i in datatrees if "MU" in i]; assert(len(tmp_mu)==len(MU_mc_prefixs))
    tmp_md = [i for i in datatrees if "MD" in i]; assert(len(tmp_md)==len(MD_mc_prefixs))
    assert(len(datatrees)==len(MU_mc_prefixs)+len(MD_mc_prefixs)) 

if args.nfiles>0:
    datatrees = datatrees[:args.nfiles]

branches = [ 'B_plus_M', 'B_plus_P', 'B_plus_PT', 'B_plus_DIRA_OWNPV',
             'B_plus_MCORR', 'B_plus_MCORRERR',
             'B_plus_LK_LTIME', 'B_plus_ENDVERTEX_CHI2', 'B_plus_DOCA_Jpsi_Mu_plus', 'nTracks',
             'Jpsi_M', 'Jpsi_P', 'Jpsi_PT', 
             'Mu_plus_P', 'Mu_plus_PT', 'Mu_plus_PIDmu', 'Mu_plus_IPCHI2_OWNPV', 
             'Mu_1_P', 'Mu_1_PT', 'Mu_1_PIDmu', 
             'Mu_2_P', 'Mu_2_PT', 'Mu_2_PIDmu',
             'Mu_1_PIDmu_corr', 'Mu_2_PIDmu_corr', 'Mu_plus_PIDmu_corr',
           ]

# add variables used in XGB, if not incluced already
# hack: study the norm-equivalent variables used in training
features = read_feats(args.feats, _key="features")
for f in features:
    if "D0" in f: 
        f=f.replace("D0","Jpsi")
    if "Pi_1" in f: 
        f=f.replace("Pi_1","Mu_1")
    if "K_minus" in f:
        f=f.replace("K_minus","Mu_2")
    if "_ETA" in f:
        f=f.replace("_ETA","_LK_ETA")
    if f not in branches: branches.append(f)

print('Found', len(datatrees), 'files. Will now select and frameify')

# mimic pipeline selection
cut_dict = {
'l0'   :
    '(Jpsi_L0MuonDecision_TOS | Jpsi_L0DiMuonDecision_TOS)',
'hlt1' :
    '(B_plus_Hlt1TwoTrackMVADecision_TOS | B_plus_Hlt1TrackMuonMVADecision_TOS | B_plus_Hlt1TrackMVADecision_TOS | B_plus_Hlt1TrackMuonDecision_TOS | B_plus_Hlt1TrackAllL0Decision_TOS | B_plus_Hlt1SingleMuonDecision_TOS)',
'hlt2' :
    '(B_plus_Hlt2Topo2BodyDecision_TOS | B_plus_Hlt2Topo3BodyDecision_TOS | B_plus_Hlt2Topo4BodyDecision_TOS | B_plus_Hlt2TopoMu2BodyDecision_TOS | B_plus_Hlt2TopoMu3BodyDecision_TOS | B_plus_Hlt2TopoMu4BodyDecision_TOS | B_plus_Hlt2SingleMuonDecision_TOS | B_plus_Hlt2Topo2BodyBBDTDecision_TOS | B_plus_Hlt2Topo3BodyBBDTDecision_TOS | B_plus_Hlt2Topo4BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS | B_plus_Hlt2TopoMu4BodyBBDTDecision_TOS)' ,

# invert muon selection
'detector_conditions' :
    '(Mu_plus_isMuon==False) & (Mu_plus_PIDmu<0) & (Mu_plus_PIDK<0)', # & (Mu_plus_hasMuon) & (Mu_plus_NShared==0) & (Mu_plus_InMuonAcc>0)',

#'MuonID':
  #'(Mu_plus_ProbNNpi*(1-Mu_plus_ProbNNk>0.8)',

'chi2_conditions':
    '(Mu_plus_IPCHI2_OWNPV>4.8)',# & ( (Mu_1_IPCHI2_OWNPV>9) | (Mu_2_IPCHI2_OWNPV>9) ) & (Mu_1_PIDmu>0) & (Mu_2_PIDmu>0)',

'kinematic_conditions' :
  '(Mu_plus_PT>750) & (Mu_plus_P>1e3) & (Jpsi_PT>2000) & (Jpsi_M>3040) & (Jpsi_M<3150) & ((Jpsi_ENDVERTEX_CHI2/Jpsi_ENDVERTEX_NDOF)<9) & (Mu_1_PT>900) & (Mu_2_PT>900)',

'Mother B hadron':
    '(B_plus_DOCA_Jpsi_Mu_plus<0.15) & (B_plus_ISOLATION_BDT<0.2) & ((B_plus_ENDVERTEX_Z-B_plus_PV_Z)>0) & (B_plus_ENDVERTEX_CHI2<25)',

'B_mass':
    '(B_plus_M>6000) & (B_plus_M<6600)',

}
cuts = ' & '.join( cut_dict.values() )
#print(cuts)
test = uproot.open( datatrees[0] )
#df = pd.DataFrame()

ntrees = len(datatrees)
for i, tree in tqdm(enumerate(datatrees),total=ntrees,desc='Progress'):
    with uproot.open(tree) as tr:
        df = tr.arrays( branches, cut=cuts, aliases=aliases, library='pd' )
        df.to_pickle(f'dfs/{args.year}/df_temp_{i}.pkl')

#for i, batch in enumerate(tqdm(uproot.iterate( datatrees, branches, cuts, aliases=aliases, library='pd' ), total=len(datatrees), desc='Progress')):
  ##df = df.append(batch)
  #batch.to_pickle(f'dfs/df_data_{args.year}_{i}.pkl')

df = pd.concat( [pd.read_pickle(f'dfs/{args.year}/df_temp_{i}.pkl') for i in range(ntrees)] ) 
print(df)
if data:
    df.to_pickle(f'dfs/df_data_{args.year}.pkl')
else:
    df.to_pickle(f'dfs/df_sim_{args.year}.pkl')
os.system(f'rm -rf dfs/{args.year}')

# sanity check plots
fig, ax = plt.subplots(1,2,figsize=(12,4))
hep.lhcb.label("Unofficial", data=True, year=args.year, loc=0, ax=ax[0])
ax[0].hist( df['B_plus_M'], bins=100, range=(6000,6600), color="tab:blue" )
ax[0].set_xlabel('B_plus_M')
hep.lhcb.label("Unofficial", data=True, year=args.year, loc=0, ax=ax[1])
ax[1].hist( df['Jpsi_M'], bins=100, range=(3000,3200), color="tab:blue" )
ax[1].set_xlabel('Jpsi_M')
fig.tight_layout()
fig.savefig(f'plots/df_check_{args.year}.pdf')
fig.savefig(f'plots/df_check_{args.year}.png')