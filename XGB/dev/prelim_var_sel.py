"""Study the combinatorial rejection via an MVAusing WS DpMuNu data

Filter ROOT files and perform variable selection
"""

__author__      = "Blaise Delaney"
__email__       = "blaise.delaney@cern.ch"
__storagepath__ = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal"

from builtins import breakpoint
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning) # kill annoying pandas index type warnings
from bdb import Breakpoint
from header import * # basic utilities and cosmetics
from sklearn.model_selection import train_test_split
from scipy.stats import pearsonr
from xgboost import XGBClassifier
import pandas as pd
from sklearn.metrics import accuracy_score
#from sweight import *
import scipy
import seaborn as sn
from sklearn.preprocessing import QuantileTransformer
import yaml
from termcolor2 import c
from pandas import MultiIndex, Int64Index
import time
from tqdm import tqdm

# ==============================
#       LOADING utilities 
# ==============================
def timing(func):
    """decorator to print function execution time [ms]"""
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = func(*args, **kwargs)
        time2 = time.time()
        print('{:s} function took {:.3f} ms'.format(func.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

def check_argpath(func):
    """decorator to check input path exists"""
    def wrapper(feats_path, _key, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, _key)
        return features 
    return wrapper

# read  feature list
@check_argpath
def read_feats(
    feats_path:str,
    _key:str,
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    try:
        _key in in_dict
        feature_list = in_dict[_key]
    except ValueError:
            print(f"'{_key}' key not in dict")
    
    return feature_list

# read in a BOI file
def yml_to_dict(
    _path:"yml file",
)->dict:
    with open(_path, 'r') as stream:
        try:
            parsed_yaml=yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    
    return parsed_yaml

@timing
def load_concat_ROOT_file(
    _files:list,
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    **kwargs
)->"pandas dataframe, with name for labels in plots":
    """wrapper for uproot.concatenate"""
    df = uproot.concatenate(
        files=_files,
        expressions=_branches,
        cut=_cut,
        library=_library,
        **kwargs
    ); df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df

@timing
def oneshot_load_ROOT_file(
    _file:"string",
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    _max_entries:"cap on number of events read in"=None,
    **kwargs
)->"pandas dataframe, with name for labels in plots, supports entry_stop":
    events = uproot.open(
        f"{_file}"
    )
    df = events.arrays(
        expressions=_branches,
        cut=_cut,
        library=_library,
        entry_stop=_max_entries,
        **kwargs
    ); df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df

@timing
def batched_load_ROOT_file(
    _file:"string",
    _branches:list,
    _library="pd",
    _cut:"string"=None,
    _name:"string, TeX format"=None,
    _max_entries:"cap on number of events read in"=None,
    _batch_size = "200 MB",
    **kwargs
)->"pandas dataframe, with name for labels in plots, supports entry_stop":
    """wrapper for uproot iterate"""
    events = uproot.open(
        f"{_file}"
    )
    bevs = events.num_entries_for(_batch_size, _branches, entry_stop=_max_entries)
    tevs = events.num_entries
    nits = round(tevs/bevs+0.5)
    aggr = []
    for batch in tqdm(events.iterate(
        expressions=_branches,
        cut=_cut,
        library=_library,
        entry_stop=_max_entries,
        step_size=_batch_size,
        **kwargs
        ), total=nits,ascii=True,desc=f"batches loaded"):
        aggr.append(batch)

    df = pandas.concat(aggr)
    if _cut is None: assert(len(df) == events.num_entries)
    df.name = _name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df

if __name__ == '__main__':
    parser = ArgumentParser(description="filter ROOT files [/combined] and perform training variable selection")
    parser.add_argument('-i','--inpath',  default=__storagepath__, 
        help="path to file for /combined/ ROOT files")    
    parser.add_argument('-b','--branches',  
        default = "yml/features.yml",
        help="path to yml file with branches to propagate")    
    parser.add_argument('-p','--plotsdir',  
        default="plots/xgb", 
        help="where to dump the XGB plots")       
    parser.add_argument('-t','--test',  action="store_true", 
        help="TEST mode, with 1e4 max entries per dataframe")    
    opts = parser.parse_args()

    if opts.test:
        _stop = 10_000
        print(c(f"WARNING: running in TEST mode with {_stop} entries per file").yellow)
    if not opts.test:
        _stop = None
        print(c(f"Reading in files with no cap on max entries").green)

    # bookkeeping
    FEATURES = read_feats(opts.branches, _key="features")
    breakpoint()
    samples = { # key:{path suffix, label},
        "DpMuNu": {
            "suffix" : f"{__storagepath__}/DATA/combined/DpMuNu/2018/Bc2DpMuNuX_sig.root:DecayTree",
            "label"  : r"WS $D^+\mu^+$ data",
            "cut"    : f"(D0_ID*Mu_plus_ID<0)", # WS data 
        },
        "CF": {
            "suffix" : f"{__storagepath__}/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_sig.root:DecayTree",
            "label"  : r"CF data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID<0)"
        },
        "DCS": {
            "suffix" : f"{__storagepath__}/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_sig.root:DecayTree",
            "label"  : r"DCS data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID>0)"
        },
        "misID": {
            "suffix" : f"{__storagepath__}/DATA/combined/D0MuNu/2018/Bc2D0MuNuX_misid.root:DecayTree",
            "label"  : r"DCS misID data",
            "cut"    : f"(K_minus_ID*Mu_plus_ID>0)"
        },           
        "D0MuNu": {
            "suffix" : f"{__storagepath__}/MC/combined/D0MuNu/2018/Bc2D0MuNu.root:DecayTree",
            "label"  : r"$B_c^+ \to D^0 \mu^+ \nu_{\mu}$",
            "cut"    : f""
        },           
        "DstMuNu": {
            "suffix" : f"{__storagepath__}/MC/combined/D0MuNu/2018/Bc2D0*MuNu.root:DecayTree", # for the moment, glob both D0g and Dzpi0
            "label"  : r"$B_c^+ \to D^{0*} \mu^+ \nu_{\mu}$",
            "cut"    : f""
        },           
    }

# read in the /combined/ files
ws = batched_load_ROOT_file(
    _file = samples["DpMuNu"]["suffix"],
    _branches = FEATURES,
    _cut = samples["DpMuNu"]["cut"],
    _name = "WS data",
    _max_entries = _stop 
)