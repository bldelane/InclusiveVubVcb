"""Compate the combinatorial templates in regions populated exclusively by this bkg;
Includes: validation CF data, D+mu+ data, GBR event-mixed pseudo-candidates (no pol correction)  
"""
__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from gettext import ngettext
import uproot, sweights
import matplotlib.pyplot as plt
import boost_histogram as bh
import pandas as pd
import numpy as np
import os
from argparse import ArgumentParser
from pathlib import Path
import mplhep as hep

# cosmetics
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "axes.labelsize": 20,
    "axes.titlesize": 20,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 17,
    "ytick.labelsize": 20,
    "xtick.labelsize": 20,
})

parser = ArgumentParser(description="loading & plotting config")
parser.add_argument('-i','--input',  
    default="/home/blaised/private/Bc2D0MuNuX/scratch/D0MuNu/2018/templates", 
    help="path to pkl dfs from which nominal templates are constructed")    
parser.add_argument('-d','--dppath',  
    default="/home/blaised/private/Bc2D0MuNuX/scratch/DpMuNu/2018/mass_ang/sw_DpMuNu.pkl", 
    help="path to file for dpmunu pkl'd df post-offline selection and sweighting")    
parser.add_argument('-p','--plotsdir',  
    default="/home/blaised/private/Bc2D0MuNuX/comb_study/plots/comparison_plots", 
    help="where to dump the comparison plots for DpMuNu studies")    
parser.add_argument('-n','--pidcut', 
    action="store_true", 
    help="implement cut on muon ProbNNmu response>0.99 [default:False]")   
opts = parser.parse_args()
if opts.pidcut: opts.dppath = opts.dppath.replace(".pkl","_ProbNNcut.pkl")

# plotting routine
def plot_data(data, ax, bins, range, wts=None, label=None, col=None, norm=True):
    nh, xe = np.histogram(data,bins=bins,range=range)
    cx = 0.5*(xe[1:]+xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
        whist.fill( data, weight = wts )
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance**0.5
        frac_err = err/nh
        if norm:
            integral = np.sum(nh)
            nh = nh/integral
            err = err/integral
            scaled_frac_err = err/nh
            assert(frac_err.all() == scaled_frac_err.all())

    ax.errorbar(cx, nh, 
        yerr=err, 
        xerr = (xe[1]-xe[0])/2.,
        fmt='.', 
        color="black",
        markersize=10,
        elinewidth=2,
        capsize=4, 
        markeredgewidth=2,
        label=label,
    )

# fill weighted hists
def fill_whist(data, ax, bins, range, wts=None, norm=True):
    """interface bh with mplhep"""
    nh, xe = np.histogram(data,bins=bins,range=range)
    cx = 0.5*(xe[1:]+xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
        whist.fill( data, weight = wts )
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance**0.5
        frac_err = err/nh
        if norm:
            integral = np.sum(nh)
            nh = nh/integral
            err = err/integral
            scaled_frac_err = err/nh
            assert(frac_err.all() == scaled_frac_err.all())
        
        return xe, cx, nh, err

# load the df from which the nominal templates are constrcuted.
cf = pd.read_pickle(
    f"{opts.input}/CF.pkl"
)
assert(np.max(cf.K_minus_ID * cf.Mu_plus_ID)<0)
mix = pd.read_pickle(
    f"{opts.input}/EvtMix.pkl"
)
dpmunu = pd.read_pickle(
    f"{opts.dppath}"
)
dpmunu = dpmunu[dpmunu.B_plus_M>3500]
assert(dpmunu.B_plus_M.min()>3500)

# identify two regions where to execute the comparison
mass_regions = {
    "validation" : {
        "cut" : "B_plus_M>5500 and B_plus_M<6280",
        "bins": 17,
        "range" : [5_500, 7_200]
    },
    "full" : {
        "cut" :"B_plus_M>3500 and B_plus_M<6280",
        "bins": 30,
        "range" : [4_200, 7_200]
    },
    "uppermass" : {
        "cut" :"B_plus_M>6280",
        "bins": 30,
        "range" : [6_000, 12_000]
    }
}

Path(f"{opts.plotsdir}").mkdir(parents=True, exist_ok=True)
for k in mass_regions.keys():
    
    fig, _ax = plt.subplots()
    plt.plot([],[], color="white", label=r"%s"%(mass_regions[k]["cut"].replace("B_plus_M", "$m(D^{0/+}\mu^+)$")\
                                        .replace("and", "$\&$"))\
                                        .replace("<", "$<$")\
                                        .replace(">", "$>$")
    )
    # dpmunu
    dpmunu_label = r"sWeighted $D^+\mu^+$ data"
    if opts.pidcut: 
        dpmunu_label+=r" ProbNN$\mu>0.99$"
    plot_data(
        dpmunu.query(mass_regions[k]["cut"]).B_plus_MCORR,
        ax=_ax,
        bins=mass_regions[k]["bins"],
        range=mass_regions[k]["range"],
        wts = dpmunu.query(mass_regions[k]["cut"]).sw,
        label=dpmunu_label,
    )
    # mixing
    xe, cx, nh, err =  fill_whist(
        mix.query(mass_regions[k]["cut"]).B_plus_MCORR,
        ax=_ax,
        bins=mass_regions[k]["bins"],
        range=mass_regions[k]["range"],
        wts = mix.query(mass_regions[k]["cut"]).sw * mix.query(mass_regions[k]["cut"]).GBR_w,
    )
    hep.histplot(nh, xe, histtype="fill", yerr=err, ax=_ax, 
        label=r"sWeighted GBR mixing",
        color="tab:blue", alpha=0.5
    )
    
    if k=="validation":
        print(f"Plotting validation region: {mass_regions[k]['cut']}; adding CF data.")
        # CF in (Bs, Bc) mass interval
        xe, cx, nh, err =  fill_whist(
            cf.query(mass_regions[k]["cut"]).B_plus_MCORR,
            ax=_ax,
            bins=mass_regions[k]["bins"],
            range=mass_regions[k]["range"],
            wts = cf.query(mass_regions[k]["cut"]).sw,
        )
        hep.histplot(nh, xe, histtype="step", yerr=err, ax=_ax, 
            label=r"sWeighted CF data", 
            color="tab:red", alpha=.75,
        )

    _ax.legend()
    _ax.set_ylim(bottom=0)
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=_ax)
    _ax.set_xlabel(r"$m_{\textrm{corr}}(D^{0/+}\mu^+)$   [MeV/$c^2$]")
    _ax.set_ylabel(r"Candidates /"+f"({int((mass_regions[k]['range'][-1] - mass_regions[k]['range'][0])/mass_regions[k]['bins'])} "+r"MeV$/c^2)$")
    if opts.pidcut:
        pid = "ProbNNcut"
    if not opts.pidcut:
        pid = ""
    plt.savefig(f"{opts.plotsdir}/{k}_region{pid}.pdf")
    plt.savefig(f"{opts.plotsdir}/{k}_region{pid}.png")