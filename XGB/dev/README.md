# Towards the deployment of XGB to suppress the combinatorial background, exspecially the real-charm componenet




## Comparison with the rectangular cuts
- [ ] For every variable used in the nominal rectangular selection, showcase the corresponding cut value, or decisions bdy, at the optimal cut value on the XGB response.

---
## Checks on simulation

- [ ] $1D$ distributions consistent with the control sample in daat
- [ ] Correlation between the training variables consistent with control data $\to$ perform the subtraction of the respective correlation matrices

---
## Open points

- There might a contribution of the likes of $B \to D^+ h$, where $h$ can be an $\eta$ or $J/\psi$ candidate, further decaying to a dimuon state; only ojne lepton is the reconstructed.
- The misID makeup in WS and DCS data **is different** 
    - _Potential shortcut_: implement a $ProbNN$ cut and we see that, post-XGb, the cut does not alter shape nor normalisation of the template. This would suggest that the misID pollution in the WS data is suppressed by the MVA. This, in many ways, can be seen as an anti-combinatorial + WS bakground. As long as it does not sculpt the corrected mass, the WS misID is probably different enough from the signal to act as a _proxy_ for the DCS misID.
    - _Rigorous solution_: extrapolate a WS misID template from WS hadron-enriched data. This can then be injected in the fit as a negative-yield component.
- The combinatorial in the CF, DCS, WS data is **likely to attain the same shape**. This is reasonable. Additionally, the poor resolution enforced in the selection is not expected to achieve a sensitivity to the $5 MeV$ different between the $D^0$ and $D^+$ invariant mass values.  

---
### Additional material for documentation
- Justify where and why the mixing breaks down at low mass.

--- 
# Instructions
1. `sw.py` executes an $sFit$ of the charm invariant mass. It produces the relevant plots (charm-mass fit, MCORR with sWeights, Kendall $\tau$ and a sW sanity plot).
2. `process.py` [temp].
3. `xgb.py`, a class that handles all data consistently in inference.
---
# Results
The results below are subjected to a loose offline selection:
```yaml
detector_conditions:
    "( Mu_plus_isMuon && Mu_plus_hasMuon && Mu_plus_NShared == 0 && Mu_plus_InMuonAcc && K_minus_hasRich && Pi_1_hasRich && !K_minus_isMuon && !Pi_1_isMuon )"

finalstateIPCHI2_conditions :
    "( Mu_plus_IPCHI2_OWNPV>16 && Pi_1_IPCHI2_OWNPV>16 && K_minus_IPCHI2_OWNPV>16 )" # trigger 

kinematic_conditions :
   "( Mu_plus_PT>1500 && Mu_plus_P>10e3 && Mu_plus_P<100e3 && D0_M>1790 && D0_M<1940 && K_minus_P<100e3 && Pi_1_P<100e3 )"

mother B hadron:
  "( (B_plus_ENDVERTEX_Z-B_plus_PV_Z)>0 )"

safety_chi2_conditions :
    "( D0_FDCHI2_OWNPV>0 && B_plus_IPCHI2_OWNPV>0 && B_plus_FDCHI2_OWNPV>0 )"

ghost removal:
    "(Mu_plus_ProbNNghost<.2)" # place this here to avoid interfering with the PIDmu split between signal and misid
```

## $sWeights$ from the $D^0$ mass in the WS sample
- [x] DCS data selected, no visible mass cut
- [x] Kendall $\tau$ computed wrt to $m_{corr}$

@import "plots/sw/mKpipi_fit.png"

```bash 
mi sanity checks
mi valid:  False
mi accurate:  True
Kendall tau: 0.0033 ± 0.0017
```
_Corollary_: it would be good to sWeight the DCS data, to target the fake-charm component. Let's see what the D0 mass looks like after XGB inference.

## Preliminary variable selection: _ie_ no preprocessing (log/GeV/arccos)

WS data:
@import "plots/mva/feat_sel/corr/dpmunu.png"

Signal MC:
@import "plots/mva/feat_sel/corr/sig_dz.png"
@import "plots/mva/feat_sel/corr/sig_dst.png"

WS correlation, post-transform
_A slight alteration of the correlation is due to the scaling_
@import "plots/mva/train/corr/background.png"

XGB response, with per-class Kendall $\tau$ values & feature importance
@import "plots/mva/train/response.png" 
@import "plots/mva/train/feat_importance.png"

Control plots:
@import "plots/mva/train/mcorr.png"
@import "plots/mva/train/dcs_XGBsel.png"
_Note: fake-charm subtraction not enacted in DCS data_
@import "plots/mva/train/dcs_XGBsel_D0_M.png"
_Seems not to preferentially target fake/real charm_
@import "plots/mva/train/dcs_inference.png"
_A mild sculpting in place; perhaps remove $p_T(B)$?_


## Discussion points:
- [ ] Use a (monotonic) XGB classifier to optimise the MCORR vs LTIMe cut?
@import "plots/mva/mcorrerr_ltime.png"
@import "plots/mva/train/dcs_inference_mcorrerr.png"
_1D projection of $\delta m_{corr}$_
@import "plots/mva/train/dcs_inference_ltime.png"
_1D projection of $\tau(X_b)$_
- [ ] MisID contamination to the WS data? How large post-xgb? Its makeup is likely quite different from the DCS data - see the notes above.
- [ ] The DCS data shown today is not sWeighted.


---

#to-do 

- [ ] after optimising XGB, what is the amount of MisID
- [ ] optimse the antiVcb XGB
- [ ] check correlation between anticomb and antivcb responses
- [ ] KDE plot
- [ ] Generate the hadron enriched data (and extend to the WS channel)
- [ ] Considerations on systematics
- [ ] Monotonic constraints experience
- [ ] Risk of throwing away the best-resolved MCORR candidates
