"""Study the combinatorial rejection via an MVAusing WS DpMuNu data
"""

__author__      = "Blaise Delaney"
__email__       = "blaise.delaney@cern.ch"
__storagepath__ = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch"

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) # kill annoying pandas index type warnings
from bdb import Breakpoint
from header import * # basic utilities and cosmetics and plotting
from load_utils import * # loading functions with uproot and yml
from sklearn.model_selection import train_test_split
from scipy.stats import pearsonr
from xgboost import XGBClassifier
import pandas as pd
from sklearn.metrics import accuracy_score
#from sweight import *
import scipy
import seaborn as sn
from sklearn.preprocessing import QuantileTransformer
import yaml
from termcolor2 import c
from pandas import MultiIndex, Int64Index


# cosmetics
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "axes.labelsize": 20,
    "axes.titlesize": 20,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 17,
    "ytick.labelsize": 20,
    "xtick.labelsize": 20,
})

parser = ArgumentParser(description="eval configuration")
parser.add_argument('-i','--inpath',  default=__storagepath__, 
    help="path to file for dpmunu post-offline selection")    
parser.add_argument('-o','--outpath',  
    default="/home/blaised/private/Bc2D0MuNuX/XGB/scratch/mva", 
    help="path to file for preprocessed dataset dir")    
parser.add_argument('-w','--sw_dir',  
    default="/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch", 
    help="path to dir for dpmunu post sW")    
parser.add_argument('-C','--channel',  
    required=True, choices=["D0MuNu", "DpMuNu", "JpsiMuNu"],
    help="decay channel [D0MuNu|DpMuNu|JpsiMuNu]")    
parser.add_argument('-p','--plotsdir',  
    default="plots/mva", 
    help="where to dump the plots for DpMuNu studies")       
parser.add_argument('-s','--skim',  action="store_true", 
    help="read and persist only B_M(corr), D mass (+probnnmu if no cut) [default:skim]")    
parser.add_argument('-x','--process',  action="store_true", 
    help="preprocess [default:True]")    
parser.add_argument('-b','--clamp',  action="store_true", 
    help="clamp [default:True]")    
parser.add_argument('-q','--qt',  action="store_true", 
    help="QT [default:False]")    
parser.add_argument('-y','--year', choices=["2011","2012","2015","2016","2017","2018"], 
    required=True,
    help="year of data taking/MC")    
parser.add_argument('-f','--features', 
    default="/home/blaised/private/Bc2D0MuNuX/XGB/yml/features.yml",
    help="yml file with training variables")    
opts = parser.parse_args()

def constrain(
    feat:"feature/column/branch",
    n_sigma=5, #"+/- n std dev of retention window",
)->"dataframe[features]":
    """retain if falling within a window"""

    mean = np.mean(feat)
    std  = np.std(feat)
    
    retained = feat[(feat>=mean-n_sigma*std) & (feat<=mean+n_sigma*std)]
    feat[(feat<mean-n_sigma*std)] = np.min(retained)
    feat[(feat>mean+n_sigma*std)] = np.max(retained)
    
    return feat

# book feature list
candidates_features = [*yml_to_dict(opts.features)]

# read WS data 
dpmunu = pd.read_pickle(f"{opts.sw_dir}/nominal/DATA/combined/{opts.channel}/{opts.year}/sw_{opts.channel}.pkl")
breakpoint()
dpmunu.name = "WS data"
assert((dpmunu.D0_ID * dpmunu.Mu_plus_ID).max()<0) # WS check

# common variables between signal and bkg
_vars = list(dpmunu.keys()) # use dpmunu to identify what vars need to be read in also in signal
_vars.remove("bw")
_vars.remove("sw")
_vars.remove("D0_ID")

breakpoint()

# load the simulated dfs
sig_dst = uproot.concatenate(
    files = [ 
        f"{opts.inpath}/nominal/MC/combined/D0MuNu/{opts.year}/Bc2D0gMuNu.root:DecayTree", 
        f"{opts.inpath}/nominal/MC/combined/D0MuNu/{opts.year}/Bc2D0pi0MuNu.root:DecayTree", 
        ], #should be weighted but raw concat will do for the moment
    expressions = _vars,
    library="pd"
)
sig_dst.name = r"$B_c^+ \to [D^{0}\pi^0/\gamma]_{D^{0*}}\mu^+ \nu_{\mu}$"

sig_dz = uproot.concatenate(
    files = [ 
        f"{opts.inpath}/nominal/MC/combined/D0MuNu/{opts.year}/Bc2D0MuNu.root:DecayTree", 
        ], #should be weighted but will do for the moment
    expressions = _vars,
    library="pd"
)
sig_dz.name = r"$B_c^+ \to D^{0}\mu^+ \nu_{\mu}$"

semiexcl_sig = pd.concat([sig_dst, sig_dz])
semiexcl_sig.name = r"$B_c^+ \to D^{0(*)}\mu^+ \nu_{\mu}$"
print("Loading complete")

breakpoint()

SAMPLES = (dpmunu, sig_dst, sig_dz, semiexcl_sig)
# relevant sanity plot w/o preprocessing
Path(f"{opts.plotsdir}/feat_sel/raw").mkdir(parents=True, exist_ok=True)
for _c in candidates_features:
    fig, ax = plt.subplots()
    for df in SAMPLES:
        if df is sig_dst: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "tab:blue",
                "ls"        : "-"
            }
        if df is sig_dz: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "blue",
                "ls"        : ":"
            }
        if df is semiexcl_sig: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "#c51b8a",
                "ls"        : "-."
            }
        if df is dpmunu: 
            plt_conf = {
                "color"     : "lightgrey",
                "histtype"  : "stepfilled",
                "alpha"     : 1.
            }

        norm_hist(
            _branch = df[_c],
            _ax = ax,
            label=df.name,
            bins=25,
            **plt_conf,
        )
        ax.plot(
            [], [], color="white",
            label = r"$\rho=$"+f"{scipy.stats.pearsonr(df[_c], df.B_plus_MCORR)[0]:.3f}"  
        )
    ax.legend(fontsize=20)
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax) 
    ax.set_ylabel("Candidates, normalised", fontsize=30)
    ax.set_xlabel(r"\texttt{"+f"{_c.replace('_','-')}"+r"}", fontsize=30)
    plt.savefig(f"{opts.plotsdir}/feat_sel/raw/{_c}.pdf")
    plt.savefig(f"{opts.plotsdir}/feat_sel/raw/{_c}.png")
    plt.close()


if not opts.process:
    print(c("NOTE: proceeding without scaling").yellow)
if opts.process:
    print(c("===>Preprocess activated").green)
    feats_dict = yml_to_dict(opts.features)
    # scale, where needed 
    for df in SAMPLES:
        for f in candidates_features:
            if feats_dict[f]=="acos":
                df[f] = np.arccos(df[f])
            if feats_dict[f]=="log":
                df[f] = np.log(df[f])
            if feats_dict[f]=="gev":
                df[f] /= 1000.
            if feats_dict[f]=="10x":
                df[f] *= 10.
            if feats_dict[f]==None:
                pass

    if opts.clamp:
        # constrain range of 5 stddev about µ
        print(c("===> * Clamping activated *").magenta)
        for df in (dpmunu, sig_dz, sig_dst):
            df[candidates_features] = df[candidates_features].apply(lambda x: constrain(x), axis=0)

if opts.qt: 
    for df in (sig_dz, sig_dst, dpmunu):
        qt = QuantileTransformer(output_distribution="normal")
        df[candidates_features] = qt.fit_transform(df[candidates_features])


# plot variables seen by the MVA 
Path(f"{opts.plotsdir}/feat_sel/scaled").mkdir(parents=True, exist_ok=True)
for _c in candidates_features:

    _min = np.min(
        [sig_dz[_c].min(), sig_dst[_c].min(), dpmunu[_c].min(), semiexcl_sig[_c].min()]
    ) 
    _max = np.max(
        [sig_dz[_c].max(), sig_dst[_c].max(), dpmunu[_c].max(), semiexcl_sig[_c].max()]
    ) 

    fig, ax = plt.subplots()
    for df in SAMPLES:
        if df is sig_dst: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "tab:blue",
                "ls"        : "-"
            }
        if df is sig_dz: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "blue",
                "ls"        : ":"
            }
        if df is semiexcl_sig: 
            plt_conf = {
                "histtype"  : "step",
                "color"     : "#c51b8a",
                "ls"        : "-."
            }
        if df is dpmunu: 
            plt_conf = {
                "color"     : "lightgrey",
                "histtype"  : "stepfilled",
                "alpha"     : 1.
            }

        norm_hist(
            _branch = df[_c],
            _ax = ax,
            label=df.name,
            bins=25,
            range=[_min, _max],
            **plt_conf,
        )
        ax.plot(
            [], [], color="white",
            label = r"$\rho$ = "+f"{scipy.stats.pearsonr(df[_c], df.B_plus_MCORR)[0]:.3f}"  
        )
    ax.plot(
            [], [], color="white",
            label = r""  
    )
    ax.plot(
            [], [], color="white",
            label = r"\textbf{KS test} = "+f"{scipy.stats.ks_2samp(semiexcl_sig[_c], dpmunu[_c])[0]:.3f}"  
    )
    ax.legend(fontsize=20)
    hep.lhcb.label("Unofficial", data=True, year=2018, loc=0, ax=ax) 
    ax.set_ylabel("Candidates, normalised", fontsize=30)
    ax.set_xlabel(r"\texttt{"+f"{_c.replace('_','-')}"+r"}", fontsize=30)
    plt.savefig(f"{opts.plotsdir}/feat_sel/scaled/{_c}.pdf")
    plt.savefig(f"{opts.plotsdir}/feat_sel/scaled/{_c}.png")
    plt.close()

dz_corr = sig_dz[candidates_features+["B_plus_MCORR"]]; dz_corr.name="sig_dz"
dst_corr = sig_dst[candidates_features+["B_plus_MCORR"]]; dst_corr.name="sig_dst"
dpmunu_corr = dpmunu[candidates_features+["B_plus_MCORR"]]; dpmunu_corr.name="dpmunu"

Path(f"{opts.plotsdir}/feat_sel/corr").mkdir(parents=True, exist_ok=True)
for d in [dz_corr, dst_corr, dpmunu_corr]:
    with sn.axes_style("white"):
        mask = np.zeros_like(d.corr())
        mask[np.triu_indices_from(mask)] = True
        f, ax = plt.subplots(figsize=(30, 23))
        ax = sn.heatmap(d.corr(), annot=True, cmap="Spectral",
            fmt=".2f", vmin=-1.0, vmax=1.0, mask=mask, square=True,
            cbar_kws={'label': 'Correlation [Arbitrary Units]'})
        #ax.set_xticklabels(rotation=75, fontsize=10)
        #ax.set_yticklabels(fontsize=10)
        ax.text(s=r"LHCb Unofficial", x=0.0, y=1.05, transform=ax.transAxes, fontsize=25)
        ax.figure.axes[-1].yaxis.label.set_size(15)
    plt.savefig(f"{opts.plotsdir}/feat_sel/corr/{d.name}.pdf")
    plt.savefig(f"{opts.plotsdir}/feat_sel/corr/{d.name}.png")


# one-hot encoding
sig_dst["target"] = 1.0; sig_dst["sw"] = 1 # dummy sweight
sig_dz["target"] = 1.0; sig_dz["sw"] = 1 # dummy sweight
dpmunu["target"] = 0.0


dataset = pd.concat([sig_dz, sig_dst, dpmunu])
breakpoint()
assert(len(dataset)==len(sig_dst)+len(sig_dz)+len(dpmunu))
print(c(f"SUCCESS: output file written to {outpath}/dataset.pkl").green.underline)
dataset.to_pickle(f"{opts.outpath}/dataset.pkl")
