"""Placeholder script to generate run XGB inference on all signal-fit components"""

import numpy as np
import pandas as pd
from XGB_base import ScalerBinaryClassifier, BinaryClassifier
from collections import namedtuple
import matplotlib.pyplot as plt
import sys
from iminuit.pdg_format import pdg_format
from sweights import kendall_tau
from argparse import ArgumentParser

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import read_feats

parser = ArgumentParser(description="debug plots")
parser.add_argument(
    "-b",
    "--debug",
    action="store_true",
    help="str: if True, plot the debug correlation and response plots",
)
opts = parser.parse_args()

# load the dataframe of interest; these must have sweghts applied
__storage_path = (
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
)

FileContainer = namedtuple("FileContainer", ["id", "df"])

# load the samples
container = {}  # slow, would be nice to run into something like a tuple or dataclass
paths = {
    # "dcs": "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/K3Pi_nominal/DATA/combined/D0MuNu/9.x_invfb/Bc2D0MuNu_dcs_sw.pkl",
    "dcs": f"{__storage_path}/D0MuNu/9.x_invfb/Bc2D0MuNu_dcs_sw.pkl",
    "cf": f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNu_cf_sw.pkl",
    "misid": f"{__storage_path}/D0MuNu/2018/Bc2D0MuNuX_misid_hadron_enriched_misid_sw.pkl",
    "comb": f"{__storage_path}/D0MuNu/9.x_invfb/Bc2D0MuNu_ws_sw.pkl",
    "sig": f"{__storage_path.replace('DATA', 'MC')}/D0MuNu/5.x_invfb/Bc2D0MuNuX.pkl",
}

# load D0mu data samples
for id in ("dcs", "cf", "misid", "comb", "sig"):
    container[id] = pd.read_pickle(paths[id])

print("Loading complete")

# book the antiComb trained XGB classifier
antiComb_clf = ScalerBinaryClassifier.from_state(
    features_conf=read_feats("yml/features.yml", "antiComb"),
    filepath="trained_models/antiComb.json",
    clf_name="antiComb",
)

# book the antiComb trained XGB classifier
antiVcb_clf = BinaryClassifier.from_state(
    features_conf=read_feats("yml/features.yml", "antiVcb"),
    filepath="trained_models/antiVcb.json",
    clf_name="antiVcb",
)

for id, df in container.items():
    # scale features
    antiComb_clf.scale_feats(
        df=df,
        verbose=True,
    )

    # perform inference
    # NOTE: verify that deriving the antiVcb without antiComb sel in not an issue
    df["XGB_antiComb"] = antiComb_clf.run_siglike_inference(X=df)
    df["XGB_antiVcb"] = antiVcb_clf.run_siglike_inference(X=df)

    # save to update file
    df.to_pickle(paths[id].replace(".pkl", "_xgb.pkl"))

if opts.debug:
    """Plot the response from the MVAs on the samples, and the WS MCORR
    distristribution subjected to variable antiComb cut working points
    """
    # visualise reponse
    with plt.style.context(["high-vis"]):
        fig, ax = plt.subplots()
        for id, df in container.items():
            ax.hist(
                df.XGB_antiComb,
                bins=50,
                range=[0, 1],
                histtype="step",
                label=id,
                density=True,
            )
        ax.legend()
        ax.set_xlabel(antiComb_clf.clf_name)
        plt.savefig("test_antiComb_resp_fitcomps.pdf")

        fig, ax = plt.subplots()
        for id, df in container.items():
            ax.hist(
                df.XGB_antiVcb,
                bins=50,
                range=[0, 1],
                histtype="step",
                label=id,
                density=True,
            )
        ax.legend()
        ax.set_xlabel(antiVcb_clf.clf_name)
        plt.savefig("test`_antiVcb_resp_fitcomps.pdf")

        # verify absence of sculpting
        fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, nrows=1, figsize=(11, 2))
        ws = container["comb"]
        # .query(
        #     "B_plus_M>3500 and B_plus_M<6285 and B_plus_FIT_LTIME<0.002"
        # )
        hist_conf = {"bins": 30, "histtype": "step", "density": True}
        for k in (0.0, 0.1, 0.3, 0.5, 0.8):
            ax1.hist(
                ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").D0_M,
                # weights=ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").sw,
                **hist_conf,
                label=str(k),
            )
            ax2.hist(
                ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").B_plus_MCORR,
                # weights=ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").sw,
                range=[4_200, 7_200],
                **hist_conf,
                label=str(k),
            )
            ax3.hist(
                ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").B_plus_MCORR,
                weights=ws.query(f"XGB_antiComb>={k} and (D0_ID*Mu_plus_ID)<0").sw,
                range=[4_200, 7_200],
                **hist_conf,
                label="sW " + str(k),
            )

        fit_region = ws.query("B_plus_MCORR>4_200 and B_plus_MCORR<7_200")
        kts = kendall_tau(fit_region.B_plus_MCORR, fit_region.sw)
        print("Kendall Tau:", pdg_format(kts[0], kts[1]))
        for _ax in (ax1, ax2, ax3):
            _ax.set_title(r"LHCb Unofficial 5.4 fb$^{-1}$ (13 TeV)")
            _ax.legend()
            _ax.set_xlabel("Monitor variable / MeV")
        ax3.set_ylim(bottom=0)
        [fig.savefig(f"test_antiComb_corr.{ext}") for ext in ("pdf", "png")]
