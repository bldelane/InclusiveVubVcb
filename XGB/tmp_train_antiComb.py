"""Placeholder script to run the two-class classifiers.
This script assumes that the antiComb XGB does not sculpt the MCORRERR and LTIME
distribs in the CF and sig samples.
"""

import numpy as np
import pandas as pd
from XGB_base import (
    XGBBuilder,
    XGBScalerBuilder,
    BinaryClassifier,
    antiComb_config_dict,
    plot_2d_dec_bdy,
)
from collections import namedtuple
import matplotlib.pyplot as plt
import sys

import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import read_feats

if __name__ == "__main__":
    __storage_path = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch"
    # train on full 16-18 samples aggregated
    # build a ScalerClassifier and train with WS data, passing sWeights
    clf = XGBScalerBuilder(
        features_conf=read_feats("yml/features.yml", "antiComb"),
        hpars_config=antiComb_config_dict,
        name="antiComb",
    ).build_new_clf(
        # semi-exclusive simulation
        sig_path=f"{__storage_path}/nominal/MC/combined/D0MuNu/5.x_invfb/Bc2D0MuNuX.pkl",
        # real-charm background
        bkg_path=f"{__storage_path}/nominal/DATA/combined/D0MuNu/5.x_invfb/Bc2D0MuNu_ws_sw.pkl",
    )

    # train, no scaling imposed here
    clf.prep_run_train(balanced=False)

    # save the model to default path
    clf.save_model()

    # perform inferece on the sig and bkg samples
    clf.bkg_sample["XGB_antiComb"] = clf.run_siglike_inference(X=clf.bkg_sample)
    clf.sig_sample["XGB_antiComb"] = clf.run_siglike_inference(X=clf.sig_sample)

    # plot bg
    fig, ax = plt.subplots()
    ax.hist(clf.bkg_sample.XGB_antiComb, bins=50, histtype="step", label="WS data")
    ax.set_xlabel(r"$\kappa_{comb}$ [A.U.]")
    ax.set_yscale("log")
    ax.set_title(r"LHCb Unofficial 5.4 fb$^{-1}$ (13 TeV)")
    ax.legend()
    fig.savefig("test_antiComb_resp_ws_5xinvfb.pdf")
