"""Attempt to compute the Punzi FoM via a simplified calculation given by dataset container"""

from builtins import breakpoint
import pandas as pd
import numpy as np
from uncertainties import ufloat
import sys
import matplotlib.pyplot as plt

plt.style.use("science")

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX/OOP_Fit")
from bmlfit import (
    source_sel,
    tarray,
    Template,
    BZObs,
    BZTemplate,
    calc_BZ_scalefactors,
)

# load the sWeighted data with inference
__storage_path = (
    "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch/nominal/DATA/combined"
)

dcs = pd.read_pickle(f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNu_dcs_sw_xgb.pkl")
ws = pd.read_pickle(f"{__storage_path}/D0MuNu/5.x_invfb/Bc2D0MuNu_ws_sw_xgb.pkl")
sig = pd.read_pickle(
    f"{__storage_path.replace('DATA', 'MC')}/D0MuNu/5.x_invfb/Bc2D0MuNuX_xgb.pkl"
)

breakpoint()
plt.figure()
plt.hist(ws.XGB_antiComb, bins=100, label="WS", density=True, histtype="step")
plt.hist(dcs.XGB_antiComb, bins=100, label="DCS", density=True, histtype="step")
plt.hist(sig.XGB_antiComb, bins=100, label="Sig", density=True, histtype="step")
plt.savefig("test_WS.png")

# apply sel
dcs = dcs.query(
    f"{source_sel('dcs')}"  # and B_plus_MCORR>5_500 and B_plus_MCORR<6_500"
)  # placeholder confidence interval
ws = ws.query(
    f"{source_sel('ws')}"  # and B_plus_MCORR>5_500 and B_plus_MCORR<6_500"
)  # placeholder confidence interval
sig = dcs.query(
    f"{source_sel('SemiExclSig')}"  # and B_plus_MCORR>5_500 and B_plus_MCORR<6_500"
)  # placeholder confidence interval
print("Samples loaded with visM, ltime and mcorr 95%-CI cuts loaded")

breakpoint()
# from baseline fit
# -----------------
sig_y = ufloat(3947, 627)
comb_y = ufloat(77554, 1189)

punzis = {}
for t in np.arange(0, 1, 0.001):
    sig_eff = len(sig[sig.XGB_antiComb > t]) / len(sig)
    b_y = len(ws[ws.XGB_antiComb > t]) / len(ws) * comb_y.n
    s_y = sig_y.n
    a = 5.0  # significance
    fom_t = np.nan_to_num(
        # handle degenerate infs or nans and map them to v small values
        # sig_eff / ((a / 2) + np.sqrt(b_y)),
        s_y / np.sqrt(s_y + b_y),
        nan=1e-5,
        posinf=1e-5,
        neginf=1e-5,
    )
    punzis[t] = fom_t

optim_FoM = list(punzis.keys())[np.argmax(list(punzis.values()))]
print(f">>> Best cut value for XGB ANTICOMB: {optim_FoM}")

# see how FoM evolves
fix, ax = plt.subplots()
ax.plot(list(punzis.keys()), list(punzis.values()))
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\frac{\varepsilon(t)}{5/2 + \sqrt{B(t)}}$")
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
[plt.savefig(f"punzi_antiComb.{ext}") for ext in ("png", "pdf")]

breakpoint()

# see what this looks like in DCS
hist_config = {"bins": 30, "range": [3_000, 10_000]}


dcs = master_df.query(f"{source_sel('dcs')} and target==3")
fix, ax = plt.subplots()
ax.hist(
    dcs.B_plus_MCORR, weights=dcs.sw, **hist_config, histtype="step", label="No MVA"
)
ax.hist(
    dcs.query(f"XGB_antiComb > {optim_FoM}").B_plus_MCORR,
    weights=dcs.query(f"XGB_antiComb > {optim_FoM}").sw,
    **hist_config,
    histtype="stepfilled",
    label=r"FoM $>%s$".format(optim_FoM),
)
ax.set_xlabel(r"$m_{corr}$")
ax.set_ylabel(r"Candidates")
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
[plt.savefig(f"antComb_sel_dcs.{ext}") for ext in ("png", "pdf")]


# see what this looks like in misid
hist_config = {"bins": 30, "range": [3_000, 10_000]}

dcs = master_df.query(f"{source_sel('misid')} and target==4")
fix, ax = plt.subplots()
ax.hist(
    dcs.B_plus_MCORR,
    weights=dcs.sw * dcs.misid_w,
    **hist_config,
    histtype="step",
    label="No MVA",
)
ax.hist(
    dcs.query(f"XGB_antiComb > {optim_FoM}").B_plus_MCORR,
    weights=dcs.query(f"XGB_antiComb > {optim_FoM}").sw
    * dcs.query(f"XGB_antiComb > {optim_FoM}").misid_w,
    **hist_config,
    histtype="stepfilled",
    label=r"FoM $>%s$".format(optim_FoM),
)
ax.set_xlabel(r"$m_{corr}$")
ax.set_ylabel(r"Candidates")
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
[plt.savefig(f"antComb_sel_misid.{ext}") for ext in ("png", "pdf")]

# effect on sig
fix, ax = plt.subplots()
ax.hist(sig.B_plus_MCORR, **hist_config, histtype="step", label="No MVA")
ax.hist(
    sig.query(f"XGB_antiComb > {optim_FoM}").B_plus_MCORR,
    **hist_config,
    histtype="stepfilled",
    label=r"FoM $>%s$".format(optim_FoM),
)
ax.set_xlabel(r"$m_{corr}$")
ax.set_ylabel(r"Candidates")
ax.set_title("LHCb Unofficial 2018 (13 TeV)")
[plt.savefig(f"antComb_sel_sig.{ext}") for ext in ("png", "pdf")]
