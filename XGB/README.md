# OOP implementation of the XGBoost classifiers 

The analysis exploits two XGBoost classifiers (XGBs):
- **_antiComb_**, devoted to the rejection of the real-charm combinatorial;
- **_antiVcb_**, devoted to the rejection of the $b \to c$ background, for which the CF dataset is a suitable proxy.

Both are part of the _high-level_ event selection, which comprises rectangular cuts in visible mass and beauty lifetime.

## Spec

1. Both classifiers adopt by default the sWeights extracted from the charm-mass fits. The sWeights are, in turn, rescaled to ensure non-negative values $\to$ both XGBs tackle real-charm background components;
3. The implementation relies on an OOP approach following the _Builder_ design pattern...

[insert inheritance diagram here]

## RFE
- [ ] The `balanced` bool keyword argument (default: `False`) currently forces the signal and background sample to have same size. This must be replaced with weight to counter the class imbalance and exploit the full sample stats;
- [x] Add a method or a standalone function capable of plotting the decision boundary for a two-feature classifier;
- [ ] Complete class/function annotations;
- [ ] Add `__repr__` class methods;
- [ ] Optimise the Punzi FoM:
    1. Algorithm to identify the best 95% interval on signal;
    2. Use $B(t)$ only in such an interval.

## ANA
- [ ] Verify that the two XGB training "factorise", _ie_ can train the two-class MVAs separately. Chiefly, this relies on the antiComb not sculpting the signal and CF (corrected mass,) corrected mass error and lifetime shapes.
- [ ] Comment on the fact that for the _antiVcb_ XGB the $\delta m_{corr} / m_{corr}$ feature was considered but it offered a reduced separation between signal and background.
- [ ] Check for absence of sculpting as a function of $t_{comb}$ within a nominal tolerance.
- [ ] Check that the XGBs target real-charm background components.
## Unit tests
- [ ] Scaling, where relevant
- [ ] Type checking with `isinstance(var, t | None)`, where `var` can be `None` in py3.10
### Example snippet
```python
"""
Test the implementation:
    1. Build the antiComb classifier using inheritance
    1b. Save the model
    2. Perform inference on a spectator dataset
    3. Re-open the spectator and dataset and scale the features
    3b. Load the trained model
    4. Standalone response evaluation
    5. Plot the response in the two regimes and compare. 
"""
if __name__ == "__main__":

    __storage_path = "/home/blaised/private/Bc2D0MuNuX/Pipeline/scratch"

    # load the hpars dict found in the dev/train.py script
    config_dict = {
        "n_estimators": 400,
        "min_child_weight": 1,
        "max_depth": 3,
        "learning_rate": 0.3,
        "gamma": 0.5,
        "colsample_bytree": 0.5,
        "tree_method": "gpu_hist",
    }  # enable gpu

    # build a ScalerClassifier and train with WS data, passing sWeights
    clf = XGBScalerBuilder(
        features_conf=read_feats("yml/features.yml", "antiComb"),
        hpars_config=config_dict,
        name="antiComb",
    ).build_new_clf(
        sig_path=f"{__storage_path}/nominal/MC/combined/D0MuNu/2018/Bc2D0MuNuX.pkl",
        bkg_path=f"{__storage_path}/nominal/DATA/combined/DpMuNu/2018/Bc2DpMuNuX_sig_ws_sw.pkl",
    )

    # scale to unity and train the model; response plot auto-generated with metrics
    clf.prep_run_train()

    # run the inference
    ws_df = pd.read_pickle(
        f"{__storage_path}/nominal/DATA/combined/DpMuNu/2018/Bc2DpMuNuX_sig_ws_sw.pkl"
    )
    ws_df["XGB_antiComb"] = clf.run_siglike_inference(X=clf.bkg_sample)

    # save the model to default path
    clf.save_model()

    # now build a new fresh classifier from the saved json file
    # Note that this requires the ScalerBinaryClassifier subclass inherinting from the BinaryClassifier and
    # implmìmenting the scaling method from ScalerMixin
    clf2 = ScalerBinaryClassifier.from_state(
        features_conf=read_feats("yml/features.yml", "antiComb"),
        filepath="trained_models/antiComb.json",
        clf_name="antiComb_check",
    )

    # load another spectator dataset
    ws_df2 = pd.read_pickle(
        f"{__storage_path}/nominal/DA`A/combined/DpMuNu/2018/Bc2DpMuNuX_sig_ws_sw.pkl"
    )

    # scale features
    clf2.scale_feats(
        df=ws_df2,
        verbose=False,
    )

    # perform inference
    ws_df2["XGB_antiComb"] = clf2.run_siglike_inference(X=ws_df2)

    with plt.style.context('high-vis'):
        # plot for consistency evaluation
        fig, ax = plt.subplots()

        hist_config = {"bins": 50, "range": [0, 1]}
        ax.hist(ws_df.XGB_antiComb, **hist_config, histtype="step")
        ax.hist(ws_df2.XGB_antiComb, **hist_config, histtype="stepfilled", alpha=0.75)
        ax.set_yscale("log")
        plt.savefig("test_XGB_impl_consistency.pdf")
    ```
