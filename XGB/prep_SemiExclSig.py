"""Prepare the semi-exclusive Bc->D0(*)MuNu sample"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
from collections import namedtuple
from argparse import ArgumentParser
from signal import signal
import sys
from typing import Callable
import pandas as pd
from uncertainties import ufloat
import pathlib
import matplotlib.pyplot as plt
from pathlib import Path

plt.style.use("science")

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import norm_hist, read_feats, oneshot_load_ROOT_file

parser = ArgumentParser(description="sig MC config")
parser.add_argument(
    "-y",
    "--year",
    choices=["2016", "2017", "2018", "5.x_invfb"],
    required=True,
    help="str: year of data taking; choices:['2016', '2017', '208', '5.x_invfb']",
)
opts = parser.parse_args()

__storage_path = "/home/blaised/private/Bc2D0MuNuX/"

# init namedtuple container for the per-year signal MC samples
SigModes = namedtuple("SigModes", ["name", "D0", "D0g", "D0pi0"])

# external inputs
# ---------------
# pdg
bf_dst_tod0pi0 = ufloat(0.647, 0.009)  # pdg
bf_dst_tod0g = ufloat(0.353, 0.009)  # pdg
# calculation by melic et al, arxiv:1909.01213
bf_bc2d0munu_3ptsr = ufloat(2.4e-5, 0.4e-5)
bf_bc2dstmunu_3ptsr = ufloat(7e-5, 3e-5)


def build_Dst_df(D0g: pd.DataFrame, D0pi0: pd.DataFrame) -> pd.DataFrame:
    """Assemble the D* template"""

    # HACK: assume efficiencies are the same between the two modes for the moment
    Dzg_frac2Dzpi0 = (bf_dst_tod0g / bf_dst_tod0pi0).n  # 1 Dzg every 2 Dzpi0
    assert len(D0pi0) * Dzg_frac2Dzpi0 <= len(D0g)

    D0g = D0g.sample(frac=1)[
        : int(len(D0pi0) * Dzg_frac2Dzpi0)
    ]  # shuffle and pick the number of events
    assert int(len(D0pi0) * Dzg_frac2Dzpi0) == len(D0g)

    # study how many events are loaded in the Dst samples
    print(
        f"\nSuccess: loaded in the D(st) {opts.year} MC samples: [WARNING: neglecting efficienciencies]"
    )
    print(f"-   Dst->D0pi0 has {len(D0pi0)} events")
    print(f"-   Dst->D0g has {len(D0g)} events")
    print(
        f"\nRatio of yields of D0g/D0pi0 = {len(D0g)/len(D0pi0)}, expect ~{35.3/64.7} [source: PDG]\n"
    )

    # stack & fill D* bh + assign name
    Dst = pd.concat([D0pi0, D0g])
    Dst.name = r"$B_c^{+}\to D^{*0} \mu^+ \nu_{\mu}$"

    return Dst


def build_SemiExcl_sig(Dst: pd.DataFrame, D0: pd.DataFrame, year: str) -> pd.DataFrame:
    """Assemble the D* template"""

    # HACK: assume efficiencies are the same between the two modes for the moment
    RBF_3ptSR = bf_bc2d0munu_3ptsr / bf_bc2dstmunu_3ptsr
    D0 = D0.sample(frac=1.0)[: int(len(Dst) * RBF_3ptSR.n)]
    # study how many events are loaded in the Dst samples
    print(
        f"\nSuccess: loaded in the D0 and composite Dst {year} MC samples: [WARNING: neglecting efficienciencies]"
    )
    print(f"-   Dst has {len(Dst)} events")
    print(f"-   D0  has {len(D0)}  events")
    print(
        f"\nRatio of yields of D0/Dst = {len(D0)/len(Dst)}, expect ~{RBF_3ptSR.n} [source: Leljak et al.]"
    )

    signal_df = pd.concat([D0, Dst])
    signal_df.name = r"$B_c^{+}\to D^{0(*)} \mu^+ \nu_{\mu}$"
    print(
        f"SemiExclusive {year} Bc2D0(*)MuNu sample generated with {len(signal_df)} events \n"
    )
    return signal_df


def constr_sig(persist_data: bool) -> callable:
    """Closure serving as a constructor to enable persistence of the semi-excl
    dataframe in the scope of the excutable
    """

    def run(year: str) -> None | pd.DataFrame:
        """Load the individual signal samples
        Assemble the semi-exclusive signal
        Write the resulting dataframe to file
        """

        # load and assign TeX name
        sigs = SigModes(
            f"sig_{year}",
            oneshot_load_ROOT_file(
                file=f"{__storage_path}/Pipeline/scratch/nominal/MC/combined/D0MuNu/{year}/Bc2D0MuNu.root:DecayTree",
                branches=BOI,
                name=r"$B_c^{+}\to D^0 \mu^+ \nu_{\mu}$",
            ),
            oneshot_load_ROOT_file(
                file=f"{__storage_path}/Pipeline/scratch/nominal/MC/combined/D0MuNu/{year}/Bc2D0gMuNu.root:DecayTree",
                branches=BOI,
                name=r"$B_c^{+}\to D^{*0} (\to D^0 \gamma) \mu^+ \nu_{\mu}$",
            ),
            oneshot_load_ROOT_file(
                file=f"{__storage_path}/Pipeline/scratch/nominal/MC/combined/D0MuNu/{year}/Bc2D0pi0MuNu.root:DecayTree",
                branches=BOI,
                name=r"$B_c^{+}\to D^{*0} (\to D^0 \pi^0) \mu^+ \nu_{\mu}$",
            ),
        )
        # BF-weighted semiexcl_sig
        dst_df = build_Dst_df(D0g=sigs.D0g, D0pi0=sigs.D0pi0)
        SemiExcl_df = build_SemiExcl_sig(Dst=dst_df, D0=sigs.D0, year=year)

        # try to get a feeling of how the templates compare
        fig, ax = plt.subplots()
        hist_config = {"bins": 30, "range": [4_200, 7_200], "density": True}

        _ = ax.hist(
            sigs.D0.B_plus_MCORR,
            **hist_config,
            histtype="step",
            color="tab:blue",
            label=sigs.D0.name,
        )
        _ = ax.hist(
            sigs.D0g.B_plus_MCORR,
            **hist_config,
            histtype="step",
            color="black",
            ls="--",
            label=sigs.D0g.name,
        )
        _ = ax.hist(
            sigs.D0pi0.B_plus_MCORR,
            **hist_config,
            histtype="step",
            color="black",
            ls=":",
            label=sigs.D0pi0.name,
        )
        _ = ax.hist(
            pd.concat([dst_df, sigs.D0]).B_plus_MCORR,
            **hist_config,
            histtype="step",
            color="red",
            ls="-.",
            label="Concat",
        )
        _ = ax.hist(
            SemiExcl_df.B_plus_MCORR,
            **hist_config,
            histtype="stepfilled",
            color="darkorange",
            alpha=0.33,
            label="Semi-exclusive",
        )

        ax.legend(fontsize=5, loc="upper left")
        ax.set_title(year)
        ax.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ / MeV")

        Path("plots/SemiExcl_sig").mkdir(parents=True, exist_ok=True)
        [plt.savefig(f"plots/SemiExcl_sig/val.{ext}") for ext in ("pdf", "png")]

        # write to file
        out_dir = pathlib.Path(
            f"{__storage_path}/Pipeline/scratch/nominal/MC/combined/D0MuNu/{year}"
        )
        out_dir.mkdir(parents=True, exist_ok=True)
        SemiExcl_df.to_pickle(f"{out_dir}/Bc2D0MuNuX.pkl")
        print(f"SUCCESS: pkl written to file @ {out_dir}/Bc2D0MuNuX.pkl")

        if persist_data:
            return SemiExcl_df

    return run


# construct the applications, preparing for aggregation for the total template
to_semiexcl = constr_sig(persist_data=False)
persist_semiexcl = constr_sig(persist_data=True)

if __name__ == "__main__":

    TOT_SIG_PATH = (
        f"{__storage_path}/Pipeline/scratch/nominal/MC/combined/D0MuNu/{opts.year}"
    )
    Path(TOT_SIG_PATH).mkdir(parents=True, exist_ok=True)

    # load the branches of interest
    BOI = (
        list(read_feats(f"{__storage_path}/XGB/yml/features.yml", "antiComb").keys())
        + list(read_feats(f"{__storage_path}/XGB/yml/features.yml", "antiVcb").keys())
        + ["B_plus_MCORR", "B_plus_M", "D0_M"]
    )

    if opts.year != "5.x_invfb":
        to_semiexcl(opts.year)

    # aggregate
    if opts.year == "5.x_invfb":
        # init a dict container over which perform aggregation
        y_container = {}
        for y in ("2016", "2017", "2018"):
            y_container[y] = persist_semiexcl(y)

        # perform aggregation
        tot_df = pd.concat(list(y_container.values()))
        assert len(tot_df) == len(y_container["2016"]) + len(y_container["2017"]) + len(
            y_container["2018"]
        ), ValueError("Aggregation failure. Event consistency check not passed")
        print("\nSUCCESS. Aggregation complete.")
        # write to file
        tot_df.to_pickle(f"{TOT_SIG_PATH}/Bc2D0MuNuX.pkl")
        print(f"SUCCESS. Written to file @ {TOT_SIG_PATH}/Bc2D0MuNuX.pkl")
