"""OOP implementation of the antiComb and antiVcb object
and standlone hpars_config dicts and decision bdy plotting function"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
from os import stat
from re import I
import numpy.typing as npt
import pandas as pd
from sklearn.model_selection import train_test_split
from scipy.stats import pearsonr
from xgboost import XGBClassifier
import xgboost as xgb
import pandas as pd
from sklearn.metrics import accuracy_score
import scipy
import seaborn as sn
from sklearn import metrics
import mplhep as hep
import matplotlib.pyplot as plt
import numpy as np
from sweights import kendall_tau
from iminuit.pdg_format import pdg_format
from collections import namedtuple
from pathlib import Path
import numpy as np
from typing import NamedTuple, TypeVar, Type
from pprint import pprint
from matplotlib import cm
import seaborn as sns

plt.style.use("science")

import sys

sys.path.insert(1, "/home/blaised/private/Bc2D0MuNuX")
from utils import norm_hist, read_feats, oneshot_load_ROOT_file, plot_data

# Create a generic variable that can be 'Parent', or any subclass.
T = TypeVar("T", bound="Parent")


class BinaryClassifier:
    """
    Following the Builder creational pattern, instiate a classifier clas
    with the self.sig_sample and self.bkg_samples that are set in the builder

    Attributes
    ----------
    name: str,
        name of the classifier

    model: XGBClassifier,
        two-class classifier initiated with the hyperpars dict in the builder class init

    features: List[str]
        set of input features

    weight_branch: str
        public attribute: name of the sWeights branch

    {sig, bkg}_sample: pd.DataFrame | None
        one-hot encoded and with sW sig and bkg dfs
        if running evaluation from loaded state, these are set to None
    """

    def __init__(
        self,
        clf_name: str,
        model: XGBClassifier,
        features: list,
        weight_branch: str | list[str],
        sig_sample: pd.DataFrame | None = None,
        bkg_sample: pd.DataFrame | None = None,
    ) -> None:

        self._clf_name = clf_name
        self._model = model
        self._features = features
        self._weight_branch = weight_branch
        self._sig_sample = sig_sample
        self._bkg_sample = bkg_sample

    @property
    def clf_name(self):
        return self._clf_name

    @property
    def model(self):
        return self._model

    @property
    def sig_sample(self):
        # check the correct assignment: signal should have placeholder 1.0 sWeights
        assert (
            self._sig_sample.sw.max() == 1.0
        ), "AttributeError: sW in signal should all be unity"
        assert (
            self._sig_sample.sw.min() == 1.0
        ), "AttributeError: sW in signal should all be unity"
        return self._sig_sample

    @sig_sample.setter
    def sig_sample(self, value):
        self._sig_sample = value

    @sig_sample.deleter
    def sig_sample(self):
        del self._sig_sample

    @property
    def bkg_sample(self):
        return self._bkg_sample

    @bkg_sample.setter
    def bkg_sample(self, value):
        self._bkg_sample = value

    @bkg_sample.deleter
    def bkg_sample(self):
        del self._bkg_sample

    @property
    def features(self):
        return self._features

    @property
    def weight_branch(self):
        return self._weight_branch

    def save_model(self, suffix: str = "", path: str = "") -> None:
        """Save model to file

        Parameters
        ----------
        path: str, optional, default=''
            path where to save the model in json format.
            If empty, save to local .trained_models directory.

        suffix: str, optional, default=''
            append to the name of model to denote eg year

        Returns
        -------
        None
            Save the trained model state to json file
        """
        if path == "":
            p = Path("trained_models")
            p.mkdir(parents=True, exist_ok=True)
            self.model.save_model(f"{p}/{self.clf_name}{suffix}.json")
        print(f"SUCESS: model state save to {p}/{self.clf_name}{suffix}.json")

    @classmethod
    def from_state(
        cls: Type[T],
        filepath,
        features_conf: dict,
        clf_name: str,
        weight_branch: str = "sw",
    ) -> T:
        """Create an instance with the model loaded from a file saveing post-training XGB weights"""
        # init method type
        model = XGBClassifier()
        model.load_model(filepath)
        print("SUCCESS: model loaded with params:")
        pprint(model.get_params())

        # HACK: handle the fact that the scaler requires an ad hoc features_conf parameter
        init_pars = {
            "clf_name": clf_name,
            "model": model,
            "weight_branch": weight_branch,
        }
        if cls.__name__ == "ScalerBinaryClassifier":
            # handle the fact that the scaler subclass needs the dict to apply the scaling
            return cls(features_conf=features_conf, **init_pars)
        else:
            # for the simpler BinaryClassifier one just needs the feature list
            return cls(features=list(features_conf.keys()), **init_pars)

    def run_siglike_inference(
        self, X: pd.DataFrame | npt.ArrayLike
    ) -> pd.DataFrame | npt.ArrayLike:
        """Run inference on spectator dataframe or array"""
        # the convention throughout is that 1 labels the signal-like class and corresponding probability
        return self.model.predict_proba(X[self.features])[::, 1]

    def fit_w_dataset(
        self, X: npt.ArrayLike, Y: npt.ArrayLike, w: npt.ArrayLike = None
    ) -> None:
        """Class instance wrapper for fit method"""
        self.model.fit(X, Y, sample_weight=w)
        print("SUCCESS: model-fitting complete with weights supplied")

    def eval_performance(
        self,
        X_train: npt.ArrayLike,
        X_test: npt.ArrayLike,
        y_test: npt.ArrayLike,
    ) -> tuple[npt.ArrayLike, npt.ArrayLike, dict]:
        """Evaluate performance on test"""
        # make predictions for test data
        y_pred = self.model.predict(X_test)
        predicted_label_test = [round(value) for value in y_pred]
        preds_test = self.model.predict_proba(X_test)[
            ::, 1
        ]  # convention: 1 for signal-like
        preds_train = self.model.predict_proba(X_train)[
            ::, 1
        ]  # convention: 1 for signal-like

        # evaluate predictions
        accuracy = accuracy_score(y_test, predicted_label_test)
        fpr, tpr, _ = metrics.roc_curve(y_test, preds_test)
        auc = metrics.roc_auc_score(y_test, preds_test)
        print("Accuracy: %.2f%%" % (accuracy * 100.0))
        print(f"ROC AUC = {auc}")

        # save metrics in container
        perf = {"auc": auc, "accuracy": accuracy}
        print("SUCCESS: performance metric evalued")

        return preds_train, preds_test, perf

    def plot_response(
        self,
        preds_train: npt.ArrayLike,
        preds_test: npt.ArrayLike,
        y_train: npt.ArrayLike,
        y_test: npt.ArrayLike,
        test_dataset: pd.DataFrame,
        auc: float,
        accuracy: float,
        OPTIMAL_CUT: None = None,  # HACK for type hinting
    ) -> None:
        """plot response
        if supplied, plot also the optimal cut value, obtained by maximising a FoM"""

        fig, ax = plt.subplots()

        if OPTIMAL_CUT != None:
            ax.axvline(OPTIMAL_CUT, color="black", ls="-.")

        # === response hists ====
        plt_config = {
            "bins": 30,
            "range": [0, 1],
        }
        norm_hist(
            _branch=preds_train[y_train > 0],
            _ax=ax,
            histtype="stepfilled",
            alpha=0.33,
            color="#3288bd",
            label="Signal, Train",
            **plt_config,
            zorder=0,
        )
        norm_hist(
            _branch=preds_train[y_train <= 0],
            _ax=ax,
            color="#d53e4f",
            histtype="stepfilled",
            alpha=0.33,
            label="Background, Train",
            **plt_config,
            zorder=1,
        )
        plot_data(
            data=preds_test[y_test > 0],
            ax=ax,
            col="tab:blue",
            label="Signal, Test",
            isnorm=True,
            **plt_config,
            zorder=2,
        )
        plot_data(
            data=preds_test[y_test <= 0],
            ax=ax,
            col="firebrick",
            label="Background, Test",
            isnorm=True,
            **plt_config,
            zorder=3,
        )
        ax.plot(
            [],
            [],
            color="white",
            label=f"ROC AUC score = {auc:.3}",
            zorder=4,
        )
        ax.plot(
            [],
            [],
            color="white",
            label=f"Accuracy = {accuracy*100.0:.0f}\%",
            zorder=5,
        )

        for t in [0, 1]:
            kts = kendall_tau(
                preds_test[test_dataset.target == t],
                test_dataset[test_dataset.target == t].B_plus_MCORR,
            )
            print(
                f"Kendall Tau coeff on test, class {str(t)}: ",
                pdg_format(kts[0], kts[1]),
            )
            ax.plot(
                [],
                [],
                color="white",
                label=r"Kendall $\tau_{%s}=$ " % t + f"{pdg_format(kts[0], kts[1])}",
                zorder=6 + t,
            )

        # order of legend handles
        handles, labels = plt.gca().get_legend_handles_labels()

        # specify order of items in legend
        order = [0, 1, 6, 7, 2, 3, 4, 5]

        # add legend to plot
        ax.legend(
            [handles[idx] for idx in order],
            [labels[idx] for idx in order],
            bbox_to_anchor=(0.5, -0.45),
            loc="center",
            ncol=2,
        )
        ax.set_title(
            "LHCb Unofficial (13 TeV)",
        )

        ax.set_xlabel("Response [Arbitrary Units]")
        ax.set_ylabel("Candidates")

        ax.set_yscale("log")
        Path("plots").mkdir(parents=True, exist_ok=True)
        [
            plt.savefig(f"plots/{self.clf_name}_response_logscale.{ext}")
            for ext in ["pdf", "png", "eps"]
        ]

        ax.set_ylim(
            bottom=0,
        )
        ax.set_yscale("linear")
        Path("plots").mkdir(parents=True, exist_ok=True)
        [
            plt.savefig(f"plots/{self.clf_name}_response.{ext}")
            for ext in ["pdf", "png", "eps"]
        ]

        print(
            f"SUCCESS: response plot saved to XGB/plots/oop/{self.clf_name}_response.*"
        )

    def prep_run_train(
        self,
        balanced: bool = False,
    ) -> tuple[npt.ArrayLike, ...]:
        """Execute the steps necessary for the train and test

        Parameters
        ----------
        balanced: bool, optional
            if True set the bkg sample size to match that of the signal [default: False]
        """
        # concate signal and background dataframes
        if balanced:
            # HACK: this should really be reciprocal weights
            dataset = pd.concat(
                [self.bkg_sample[: len(self.sig_sample)], self.sig_sample]
            )
            assert (
                len(dataset) == len(self.sig_sample) * 2
            ), "ERROR: balanced training called but bkg and sig have different size"
            print(
                "Running with balanced training. FutureWarning: implement reprical weights instead in the future."
            )
        else:
            dataset = pd.concat([self.bkg_sample, self.sig_sample])
            print(
                f"Running with inbalanced training: len(S) = {len(self.sig_sample)}; len(B) = {len(self.bkg_sample)}"
            )

        # rescale weights uniformly to have positive value
        dataset["sw"] = dataset["sw"] - dataset["sw"].min()
        assert (
            dataset["sw"].min() >= 0
        ), "ValueError: all sWeights should be positive before passing them to XGB"

        # book np arrays and partition into train and test
        X = dataset[self.features]  # feats
        y = dataset["target"].astype(int)  # one-hot enc
        # w = dataset["sw"]
        w = np.ones(len(dataset.sw))
        (
            X_train,
            X_test,
            y_train,
            y_test,
            w_train,
            _,
            _,
            test_dataset,
        ) = train_test_split(
            X,
            y,
            w,
            dataset,
            test_size=0.25,  # default in scikit-learn
            random_state=2310,
        )

        # train model
        self.fit_w_dataset(X_train, y_train, w_train)

        # evaluate performance on test
        preds_train, preds_test, perf_tuple = self.eval_performance(
            X_train=X_train,
            X_test=X_test,
            y_test=y_test,
        )

        # plot performance
        self.plot_response(
            preds_train,
            preds_test,
            y_train,
            y_test,
            test_dataset,
            **perf_tuple,
            OPTIMAL_CUT=None,  # HACK for type hinting
        )


# parent class for XGB classifiers
class XGBBuilder:
    """
    Following the Builder creational pattern, return a XGBBinaryClassifier class
    with the self.sig_sample and self.bkg_samples attributes set from the relavent
    one-hot encoded pkl files, with the sW provided

    Attributes
    ----------
    features: List[str]
        set of input features

    config: dict
        dict of model hyperpars

    weight_branch: str | list[str]
        public attribute: name of the sWeights branch

    name: str,
        name of the classifier
    """

    def __init__(
        self,
        features: list[str],
        hpars_config: dict,
        name: str,
        weight_branch: str | list[str] = "sw",
    ) -> None:

        self._hpars_config = hpars_config
        self._features = features
        self._weight_branch = weight_branch
        self._name = name

    @property
    def features(self):
        return self._features

    @property
    def hpars_config(self):
        return self._hpars_config

    @property
    def weight_branch(self):
        return self._weight_branch

    @weight_branch.setter
    def weight_branch(self, value):
        print(f"Setter of weights_branch called to set to {value}")
        self._weight_branch = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        print(f"Setter of clf name called to set to {value}")
        self._name = value

    @staticmethod
    def assign_lb_ssw(
        sig_sample: pd.DataFrame, bkg_sample: pd.DataFrame
    ) -> tuple[pd.DataFrame, pd.DataFrame]:
        """Implement one-hot encoding and assign placeholder sWeight to signal sample &
        assign a weight of 1 to the signal (as it's MC)"""
        bkg_sample["target"] = 0  # convention
        sig_sample["target"] = 1  # convention
        sig_sample["sw"] = 1.0  # placeholder weight

        return sig_sample, bkg_sample

    def build_sig_bkg(self, sig_path: str, bkg_path: str | list[str, ...]) -> dict:
        """build the train and test samples"""
        br = self.features + [
            "B_plus_MCORR",
        ]  # subset of the df keys to read in

        # NOTE: added flexibility to read multiple bkg samples
        if isinstance(bkg_path, str):
            S, B = self.assign_lb_ssw(
                pd.read_pickle(sig_path)[br],  # signal has no native sW
                pd.read_pickle(bkg_path)[br + [self.weight_branch]],
            )
        else:
            print(
                "WARNING: multiple bkg samples provided -> generating a single composite bkg sample"
            )
            to_concat = [pd.read_pickle(p)[br + [self.weight_branch]] for p in bkg_path]
            S, B = self.assign_lb_ssw(
                pd.read_pickle(sig_path)[br],  # signal has no native sW
                pd.concat(to_concat),
            )
            assert len(B) == sum(
                [len(df) for df in to_concat]
            ), "ERROR: bkg sample size mismatch"

        return {"sig_sample": S, "bkg_sample": B}

    def build_new_model(self) -> XGBClassifier:
        """build model pre-training with a given hypars config dict"""
        return XGBClassifier(
            use_label_encoder=False,
            objective="binary:logistic",
            booster="gbtree",
            eval_metric="logloss",
            **self.hpars_config,
        )

    def build_new_clf(self, sig_path: str, bkg_path: str) -> BinaryClassifier:
        return BinaryClassifier(
            clf_name=self.name,
            model=self.build_new_model(),
            features=self.features,
            weight_branch=self.weight_branch,
            **self.build_sig_bkg(sig_path, bkg_path),
        )


class ScalerMixin:
    """Mixin to add the scaling capabilities to a
    builder or classifier instance
    """

    def scale_feats(self, df: pd.DataFrame, verbose: bool = False) -> None:
        """Implement feature scaling"""
        for f in self.features_conf.keys():
            if self.features_conf[f] == "acos":
                df[f] = np.arccos(df[f])
            if self.features_conf[f] == "log":
                df[f] = np.log(df[f])
            if self.features_conf[f] == "gev":
                df[f] /= 1000.0
            if self.features_conf[f] == "10x":
                df[f] *= 10.0
            if self.features_conf[f] == None:
                pass
        if verbose == True:
            print(
                "SUCCESS: scaling on single dataframe complete, following the schema:"
            )
            pprint(self.features_conf)


class XGBScalerBuilder(ScalerMixin, XGBBuilder):
    """inherit from the XGBBuilder parent class
    and add the scale to O(1) in the preeprocessing stage
    """

    def __init__(
        self,
        features_conf: dict,  # read in the config dict to retain info on the scaling
        hpars_config: dict,
        name: str,
        weight_branch: str | list[str] = "sw",
    ) -> None:
        super().__init__(
            features=list(features_conf.keys()),
            hpars_config=hpars_config,
            name=name,
            weight_branch=weight_branch,
        )
        self._features_conf = features_conf
        print("------------------------------------------------")
        print("Booked XGBTwoClass object with scaling in place.")
        print("-> Scaling executed following:\n")
        pprint(self._features_conf)
        print("------------------------------------------------\n")

    @property
    def features_conf(self):
        return self._features_conf

    @staticmethod
    def plot_features(
        feature_list: list,
        sig_sample: pd.DataFrame,
        bkg_sample: pd.DataFrame,
        scaled: bool = False,
    ) -> None:
        with plt.style.context("high-contrast"):
            for _c in feature_list:
                _min = np.min([sig_sample[_c].min(), bkg_sample[_c].min()])
                _max = np.max([sig_sample[_c].max(), bkg_sample[_c].max()])

                fig, ax = plt.subplots()
                sig_sample.name = r"$B_c^+ \to D^{0)*)} \mu^+ \nu_{\mu}$ MC"
                bkg_sample.name = "Background data"
                for df in (sig_sample, bkg_sample):
                    norm_hist(
                        _branch=df[_c],
                        _ax=ax,
                        label=df.name,
                        bins=25,
                        range=[_min, _max],
                        histtype="step",
                    )
                    ax.plot(
                        [],
                        [],
                        color="white",
                        label=r"$\rho$ = "
                        + f"{scipy.stats.pearsonr(df[_c], df.B_plus_MCORR)[0]:.3f}",
                    )
                ax.plot([], [], color="white", label=r"")
                ax.plot(
                    [],
                    [],
                    color="white",
                    label=r"\textbf{KS test} = "
                    + f"{scipy.stats.ks_2samp(sig_sample[_c], bkg_sample[_c])[0]:.3f}",
                )
                ax.legend()
                ax.set_title(f"LHCb Unofficial (13 TeV)")
                ax.set_ylabel("Candidates, normalised")
                ax.set_xlabel(r"\texttt{" + f"{_c.replace('_','-')}" + r"}")
                out_path = Path("plots/feat_sel")
                out_path.mkdir(parents=True, exist_ok=True)
                suffix = "raw"
                if scaled:
                    suffix = "scaled"
                [
                    plt.savefig(f"{out_path}/{_c}_{suffix}.{ext}")
                    for ext in ("pdf", "png", "eps")
                ]
                plt.close()

    def build_sig_bkg(self, sig_path: str, bkg_path: str, save: bool = True) -> dict:
        """implement the preprocessing and return resulting two-sample dict"""
        # XXX: probably quite dangerous to have a closure with the same name
        smpl_dict = super().build_sig_bkg(sig_path, bkg_path)

        # plot pre-scaling
        print("Plotting features pre-scaling...")
        self.plot_features(list(self.features_conf.keys()), **smpl_dict)
        print("SUCCESS: plotting complete\n")

        # scale, where needed
        pr_out_dict = {}  # init processed two-sample out dict
        print(">>> Preprocess activated")
        for nm, df in smpl_dict.items():  # loop through bkg and semi-excl sig dfs
            self.scale_feats(df, self.features_conf)
            pr_out_dict[nm] = df

        # plot post-scaling
        print("Plotting features post-scaling...")
        self.plot_features(list(self.features_conf.keys()), **pr_out_dict, scaled=True)
        print("SUCESS: scaling and plotting complete\n")

        return pr_out_dict


class ScalerBinaryClassifier(ScalerMixin, BinaryClassifier):
    """inherit from the ScalerMixin and BinaryClassifier
    parent classes to be able to read in a dataframe, scale
    and perform inference from a trained model
    """

    def __init__(
        self,
        clf_name: str,
        features_conf: dict,  # read in the config dict to retain info on the scaling
        weight_branch: str | list[str],
        model: XGBClassifier = XGBClassifier(),  # book the kind of classifier
    ) -> None:
        super().__init__(
            clf_name=clf_name,
            model=model,
            features=list(features_conf.keys()),
            weight_branch=weight_branch,
        )
        self._features_conf = features_conf

    @property
    def features_conf(self):
        return self._features_conf


# standalone items
# ----------------

# load the hpars dicts found in the dev/train.py script via randomised sampling
antiComb_config_dict = {
    "n_estimators": 400,
    "min_child_weight": 1,
    "max_depth": 3,
    "learning_rate": 0.3,
    "gamma": 0.5,
    "colsample_bytree": 0.5,
    "tree_method": "gpu_hist",
}  # enable gpu

antiVcb_config_dict = {
    "n_estimators": 1500,
    "min_child_weight": 7,
    "max_depth": 10,
    "learning_rate": 0.3,
    "gamma": 0.5,
    "colsample_bytree": 0.3,
    "tree_method": "gpu_hist",
    "monotone_constraints": "(0, 0)",
}


# plot decision boundary for 2-feature antiVcb XGB
def plot_2d_dec_bdy(
    sig: pd.DataFrame,
    cf: pd.DataFrame,
    model: XGBClassifier,
    features: list[str, str] = ["B_plus_MCORRERR", "B_plus_FIT_LTIME"],
    xgb_cut: float = 0.4,  # placeholder
    plotname: str = "antiVcb_dec_bdy",
) -> None:
    """Plot sig and b->c countours in mcorrerr and ltime
    and the obtained decision boundary
    """
    fig, ax = plt.subplots()
    x_range = [0, 2_000]
    y_range = [0, 0.01]

    # plot the sig contours
    sns.kdeplot(
        x=sig[features[0]][:1000],
        y=sig[features[1]][:1000],
        cmap="Blues",
        shade=True,
        thresh=0.01,
        ax=ax,
        label=r"$B_c^+ \to D^{(*)0}\mu^+ \nu_{\mu}$",
        cbar=False,
    )

    # plot the cf contours
    sns.kdeplot(
        x=cf[features[0]][:1000],
        y=cf[features[1]][:1000],
        cmap="flare",
        shade=False,
        thresh=0.01,
        ax=ax,
        label="CF Data",
        legend=True,
        cbar=False,
    )
    x1grid = np.arange(
        0, np.maximum(sig[features[0]].max(), cf[features[0]].max()), 100
    )
    x2grid = np.arange(
        0, np.maximum(sig[features[1]].max(), cf[features[1]].max()), 0.0001
    )
    xx, yy = np.meshgrid(x1grid, x2grid)  # ensure feature order is correct
    r1, r2 = xx.flatten(), yy.flatten()
    r1, r2 = r1.reshape((len(r1), 1)), r2.reshape((len(r2), 1))
    grid = np.hstack((r1, r2))
    Z = model.predict_proba(grid)[::, 1].reshape(xx.shape)

    # plot the decision boundary
    CS = ax.contour(
        xx,
        yy,
        Z,
        levels=[
            xgb_cut,
        ],
        colors="black",
    )
    ax.clabel(CS, inline=True)

    ax.set(
        xlim=(0, 1500),
        ylim=(0.0, 0.0045),
        xlabel=r"$\delta m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]",
        ylabel=r"$\tau(X_b)$ [ns]",
    )

    ax.set_title(
        "LHCb Unofficial 2018 (13 TeV)",
    )
    trans = ax.get_yaxis_transform()
    y = 0.002
    ax.axhspan(0.0020, 0.0045, alpha=0.5, facecolor="lightgrey")

    ax.text(
        0.675,
        0.0017,
        r"$\varepsilon_{\chi} =$"
        + r" ${:.0f}\%$".format(
            len(sig.query(f"XGB_antiVcb>{xgb_cut}")) / len(sig) * 100.0
        ),
        color="black",
        transform=trans,
    )
    ax.text(
        0.675,
        0.0013,
        r"$\varepsilon_{\tau}= $"
        + r" ${:.0f}\%$".format(
            len(sig.query("B_plus_FIT_LTIME<0.002")) / len(sig) * 100.0
        ),
        color="black",
        transform=trans,
    )

    [plt.savefig(f"plots/{plotname}.{ext}") for ext in ("png", "pdf", "eps")]
    plt.close()


if __name__ == "__main__":
    pass
