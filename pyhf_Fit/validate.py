"""Stat tools to validate the fit performance

__author__ : Blaise Delaney
__email__  : blaise.delaney@cern.ch
"""
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX")
from pyhf_Fit.executables.common.header import *
import mplhep 
from hist import Hist
import boost_histogram as BH
import matplotlib.pyplot as plt
import pickle
from pathlib import Path
from termcolor2 import c
import sys, os
from tqdm import tqdm
from numba_stats import norm_pdf
from iminuit.cost import ExtendedUnbinnedNLL, UnbinnedNLL 
from iminuit import Minuit

# define model and cost function for toy pull fit
norm_model= lambda x, mu, sigma: norm_pdf(x, mu, sigma)

def build_pyhf_spec(alt, schemapath)->"pyhf schema":
    """fit each time with an alternative mixing template"""
    schema_18 = f"{schemapath}/model_spec_D0MuNu_{alt}_2018.json"
    schema_17 = f"{schemapath}/model_spec_D0MuNu_{alt}_2017.json"
    schema_16 = f"{schemapath}/model_spec_D0MuNu_{alt}_2016.json"

    # validate the syntax to interface with pyhf
    for SCHEMA in [schema_16, schema_17, schema_18]:
        workspace = json.load(open(SCHEMA))
        schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
        jsonschema.validate(instance=workspace, schema=schema)

    sch_18 = json.load(open(schema_18))
    sch_17 = json.load(open(schema_17))
    sch_16 = json.load(open(schema_16))

    # construct the spec
    spec = {
    "channels": [
        sch_18["channels"][0],
        sch_17["channels"][0],
        sch_16["channels"][0]
    ],
    "observations":[
        sch_18["observations"][0],
        sch_17["observations"][0],
        sch_16["observations"][0],
    ],
    "measurements": [
        { 
            "name": f"sig_y_extraction",
            "config": {
                "poi": "incl_sig_y",
                "parameters": sch_16["measurements"][0]["config"]["parameters"][1:]+sch_17["measurements"][0]["config"]["parameters"][1:]+sch_18["measurements"][0]["config"]["parameters"]
            }
        }
    ],
    "version": "1.0.0"
    }
    return spec

def build_validate_workspace(spec, alt, schemapath)->"pyhf workspace":
    """Build and validate the model and observation"""
    spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
    
    # write for preservation
    Path(f"{schemapath}/combined").mkdir(parents=True, exist_ok=True)
    SCHEMA = f"{schemapath}/combined/{alt}.json"
    if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
    with open(SCHEMA,'w') as outfile: outfile.write(spec)

    # validate workspace
    workspace = json.load(open(SCHEMA))
    schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
    # If no exception is raised by validate(), the instance is valid.
    jsonschema.validate(instance=workspace, schema=schema)
    workspace = pyhf.Workspace(workspace)

    return workspace

def plot_fitted_model(workspace, pdf, alt, bestfit_pars, ax0)->"fig":
    """ very raw plot comparing fit results"""
    proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)
    ax0.step(
        x = np.arange(len(proj_fit)),
        y = proj_fit,
        where="post",
        lw=1.5, ls="-",
        label=alt,
        alpha=.7
    )
    ax0.errorbar(
        x = np.arange(len(proj_fit))+0.5,
        y= workspace.data(pdf)[:len(proj_fit)],
        fmt=".",
        markersize=7,
        elinewidth=2,
        capsize=2,
        markeredgewidth=2,
        color="black",
        alpha=.7
    )

    ax0.set_title(r"LHCb Unofficial $5.4$ fb$^{-1}$", fontsize=18)
    ax0.legend(fontsize=15, loc = "center right", frameon=False)
    ax0.set_ylabel(rf"Bohm\&Zech-scaled events / 100 MeV", loc="center", fontsize=18)

    ax0.axvline(x=30, color="lightgrey", ls="--", lw=1.5)
    ax0.axvline(x=60, color="lightgrey", ls="--", lw=1.5)

    ax0.text(0.27, 0.90, '2016', transform=ax0.transAxes, color="grey", size=30)
    ax0.text(0.57, 0.90, '2017', transform=ax0.transAxes, color="grey", size=30)
    ax0.text(0.91, 0.90, '2018', transform=ax0.transAxes, color="grey", size=30)

    ax0.set_xticks([], minor=False)

    ax0.set_xticklabels([])
    ax0.set_xticks(np.arange(0, 95, 5), minor=True)
    ax0.set_xticklabels([4200, 4700, 5200, 5700, 6200, 6700, 4200, 4700, 5200, 5700, 6200, 6700, 4200, 4700, 5200, 5700, 6200, 6700, 7200], minor=True)
    ax0.tick_params(axis='x', which='both', labelsize=20)
    ax0.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]", loc="center", fontsize=18)


def fit(workspace, alt, resultspath, gen_fit_plot=False, **kwargs)->"par_name_dict, pdf, bestfit_pars, par_uncerts, PRESCALED_FIT_RESULTS[dict]":
    """run the fit"""
    pdf = workspace.model(measurement_name = "sig_y_extraction",
                        modifier_settings={
                            "normsys": {"interpcode": "code1"},
                            }
    )
    data = workspace.data(pdf)
    fit_result, result_obj = pyhf.infer.mle.fit(data, pdf, return_result_obj=True, return_uncertainties=True)

    bestfit_pars, par_uncerts = fit_result.T

    par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}
    PRESCALED_FIT_RESULTS = {}
    
    # write to dedicated file; repristinate outoput after
    fres = open(f"{resultspath}/{alt}.log", "w")
    for k, v in par_name_dict.items():
        PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
        print(f"Extracted value of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}", file=fres)
    fres.close()
    
    # print signal yield 
    print(c(f"Fit result: mixing template: {alt}; signal yield: {PRESCALED_FIT_RESULTS['incl_sig_y']:.0f}").blue)

    # book raw fit plot
    if gen_fit_plot: plot_fitted_model(workspace, pdf, alt, bestfit_pars, **kwargs)

    return par_name_dict, pdf, bestfit_pars, par_uncerts, PRESCALED_FIT_RESULTS

def fit_toys(toys:"set of pseudo-datasets", pdf:"model", 
    toy_path,
    channel)->"fit toys and write results to file; includes all fitted parameters":
    """fit the toy data and preserve the fit results and uncertianties, for all paramaters in the model; 
    returns dicts pars_cvals and pars_errs"""
    
    # book the toy results directory and dict containers
    Path(f"{toy_path}/{channel}").mkdir(parents=True, exist_ok=True)
    pars_cvals = {}
    pars_errs = {}
    
    # fit toys 
    print("Fitting toys...")
    for i in tqdm(range(len(toys))):
        toy = toys[i]
        
        # fit the toys generated with the bestfit pars with the original model
        toy_fit_result, toy_result_obj = pyhf.infer.mle.fit(toy, pdf, 
                                                    par_bounds = pdf.config.suggested_bounds(), 
                                                    return_result_obj=True, return_uncertainties=True)
        # retrieve results and uncertainties and save in dict containers
        toy_bestfit_pars, toy_par_uncerts = toy_fit_result.T
        pars_cvals[f"{i}"] = toy_bestfit_pars
        pars_errs[f"{i}"] = toy_par_uncerts
        
        # write to file for preservation
        if i % 50 == 0:

            c_file = open(f"{toy_path}/{channel}/par_cvals.pkl", "wb")
            pickle.dump(pars_cvals, c_file)
            c_file.close()

            s_file = open(f"{toy_path}/{channel}/par_stds.pkl", "wb")
            pickle.dump(pars_errs, s_file)
            s_file.close()

    # return dict containers
    return pars_cvals, pars_errs

def fit_toy_pull(
    toy_vals, 
    toy_errs,
    bestfit_pars,
    par_name_dict,
    plot_path, 
    _var=None, 
    _bins=100, 
    title=None, 
    )->"fit pull with gaussia and save plot":
    """verify that the fit results are unbiased ML estimators"""
    hep.style.use(["LHCbTex", "LHCb2"])
    f, ax = plt.subplots()

    toy_n = np.array(list(toy_vals.values()))[..., par_name_dict[_var]]
    toy_s = np.array(list(toy_errs.values()))[..., par_name_dict[_var]]
    px = (toy_n - bestfit_pars[par_name_dict[f"{_var}"]] ) / (toy_s),
    
    # bin it
    w, xe = np.histogram(px[0], bins=_bins, range=(-5, 5))

    # compute bin-wise density estimates
    werr = w ** 0.5
    cx = 0.5 * (xe[1:] + xe[:-1])
    dx = np.diff(xe)
    d = w / dx
    derr = werr / dx

    cost = UnbinnedNLL(px[0], norm_model)

    # fit the model
    m = Minuit(cost, mu=0, sigma=1)
    m.limits["mu"] = (-1, 1)
    m.limits["sigma"] = (0, None)
    m.migrad()
    m.hesse()

    # plot everything
    ax.errorbar(
        x = cx, 
        y = w, 
        fmt = "o", markersize=5, yerr=werr, elinewidth=1.5,# xerr = 0.5 * (xe[1] - xe[0]),
        color="black", alpha=1., label="Pseudodata")

    xm = np.linspace(xe[0], xe[-1], num=100)
    ax.plot(xm, norm_model(xm, *[p.value for p in m.init_params])* len(px[0]) * dx[0], 
            lw=3,
            label="Fit", 
            color="tab:blue")
    hep.lhcb.label("Unofficial", data=True, )

    ax.legend(frameon=False,
               title=
                     f"{len(px[0])} toys\n"
                     f"$\mu = {m.values[0]:.2f} \pm {m.errors[0]:.2f}$\n"
                     f"$\sigma = {m.values[1]:.2f} \pm {m.errors[1]:.2f}$", loc="upper right");

    plt.xlabel(str(title)+r" pull $\frac{y_{fit} - y'}{\sigma_{fit}}~[\sigma]$", loc="center")
    plt.ylabel("Pseudo-events / (0.1)", loc="center")
    
    plt.savefig(f"{plot_path}/{_var}.pdf")
    plt.savefig(f"{plot_path}/{_var}.png")


def run_toys(bestfit_pars, n_toys=1000, **kwargs)->"toys fit results":
    """generate pseudo-data with bestfit pars and fit; fit the pulls with gaussian pdf"""
    print(c("\nBooked toys routine").bold)
    # generate the toy data
    print(f"Generating {n_toys} toys...") 
    NLL_pdf = pdf.make_pdf(pyhf.tensorlib.astensor(bestfit_pars))
    toys = NLL_pdf.sample((n_toys,))

    return fit_toys(toys, **kwargs) # par cvals and errs


if __name__ == '__main__':
    parser = ArgumentParser(
            description="Plot configuration")
    parser.add_argument('-c','--channel',      required=True, choices=["D0MuNu", "JpsiMuNu"], help="Signal [D0MuNu] or normalisation [JpsiMuNu] channel")
    parser.add_argument('-p','--plotpath',     default=f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/validate", help="where to store the validation plots")
    parser.add_argument('-s','--schemapath',   default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/scratch", help="where to store the schema?")  
    parser.add_argument('-r','--results',      default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/results", help="where to store the fit results?")
    parser.add_argument('-T','--toypath',      default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/validate/toys", help="master path for toy-fit results")
    parser.add_argument('-a','--alt',          default="alt1", help="pol1 correction weights to event-mixed+GBR template")  
    opts = parser.parse_args()

    # assemble the model using the pol1 mixing correction weights as nominal
    combined_spec = build_pyhf_spec(alt=opts.alt, schemapath=opts.schemapath)
    workspace = build_validate_workspace(spec=combined_spec, alt=opts.alt, schemapath=opts.schemapath)
    print("SUCCESS: 2016+2017+2018 combined spec build and validation passed. \nIntiating fit...\nDone.")
    par_name_dict, pdf, bestfit_pars, par_uncerts, PRESCALED_FIT_RESULTS = fit(workspace, opts.alt, resultspath=opts.results)

    # run toys & fit
    pars_cvals, pars_errs = run_toys(bestfit_pars=bestfit_pars, pdf=pdf, toy_path=opts.toypath, channel=opts.channel)

    # generate the plot directory & plot toy pull fit
    print(f"Fitting the toy fit result pulls...")
    Path(f"{opts.plotpath}/{opts.channel}").mkdir(parents=True, exist_ok=True)
    fit_toy_pull(
        toy_vals=pars_cvals,
        toy_errs=pars_errs,
        par_name_dict = par_name_dict,
        bestfit_pars=bestfit_pars,
        plot_path = f"{opts.plotpath}/{opts.channel}",
        _var = "incl_sig_y",
        title="Signal yield",   
    )
    print(c(f"SUCCESS: the pull fit plot can be found here: {opts.plotpath}/{opts.channel}").green)
