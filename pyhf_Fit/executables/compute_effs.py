from common.header import *
from termcolor2 import c
import pprint 
import yaml

parser = ArgumentParser()
parser.add_argument('-y','--year',   required=True, help='Year of data-taking/MC', choices=["2011", "2012", "2015", "2016", "2017", "2018"])
parser.add_argument('-c','--config', default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json', help='Path to fit config')
parser.add_argument('-m','--mode', required=True, choices=["D0MuNu", "JpsiMuNu"])
parser.add_argument('-p','--path', default="/usera/delaney/private/Bc2D0MuNuX/Pipeline/CONFIG/Selection")
parser.add_argument('-C','--pidcorrected',    action='store_true') # default False

opts = parser.parse_args()

# enable multithreading
import ROOT
ROOT.ROOT.EnableImplicitMT() #---> maybe revisit this later?
ROOT.gInterpreter.Declare('#include "{}"'.format("/usera/delaney/private/Bc2D0MuNuX/Pipeline/common/EventMixing_D0Mu.h"))
RDF = ROOT.ROOT.RDataFrame

YEAR = opts.year
CONFIG = opts.config
MODE = opts.mode

# binning
with open( CONFIG ) as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").bold)
print(fit_config, "\n")

if MODE=="D0MuNu":
    decays = ["Bc2D0MuNu", "Bc2D0gMuNu", "Bc2D0pi0MuNu"]
    folder = "B2DMuNuX_D02KPiTuple"
if MODE=="JpsiMuNu":
    decays = ["Bc2JpsiMuNu", "Bc2JpsiTauNu"]
    folder = "B2JpsiMuNuXTuple"

if MODE=="JpsiMuNu":
    had_daughter_1 = "Mu_1"
    had_daughter_1_mass = 105.66
    had_daughter_2 = "Mu_2"
    had_daughter_2_mass = 105.66
    hadron = "Jpsi"
if MODE=="D0MuNu":
    had_daughter_1 = "Pi_1"
    had_daughter_1_mass = 139.57
    had_daughter_2 = "K_minus"
    had_daughter_2_mass = 493.68
    hadron = "D0"

truthmatching_file  = f"{opts.path}/{MODE}/Truthmatching.yml"
with open(truthmatching_file,'r') as f:
    sel = yaml.safe_load(f)
sel.pop("name")
TRUTHMATCHING = "( " + " && ".join(sel.values()) + " )"
print(c(f"TRUTHMATCHING : {TRUTHMATCHING}").yellow)
    
event_selection_files = {}
event_selection_files["trigger"]        = f"{opts.path}/{MODE}/TriggerSelection.yml"
event_selection_files["main_selection"] = f"{opts.path}/{MODE}/MainSelection.yml"
event_selection_files["realmuon"]       = f"{opts.path}/{MODE}/RealMuonMC.yml"
event_selection_files["masscuts"]       = f"{opts.path}/{MODE}/MassCuts.yml"

# efficiency def
def eff(npass, ntotal):
  if ntotal>0:
    eff = float(npass)/float(ntotal)
    eff_err = ( (eff*(1.-eff))/float(ntotal) )**0.5
  else:
    eff = 0.
    eff_err = 0.
  return (eff,eff_err)

event_selection = {}
for k, stage in event_selection_files.items():
    with open(stage,'r') as f:
        sel = yaml.safe_load(f)
    sel.pop("name")
    sel_str = "( " + " && ".join(sel.values()) + " )"
    event_selection[k] = sel_str 

event_selection["mcorr_cuts"] = f"( B_plus_MCORR >= {fit_config['B_plus_MCORR']['range'][0]} && B_plus_MCORR <= {fit_config['B_plus_MCORR']['range'][-1]} )"
event_selection["mcorr_cuts"] = f"( B_plus_FIT_LTIME >= 0.0005 && B_plus_FIT_LTIME <= 0.005 )"

full_selection = sel_str = "( " + " && ".join(event_selection.values()) + " )"
print(c(full_selection).blue)

remote_path = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bc2D0MuNuX/ntuples/v4/MC"

effs = {}
for decay in decays:
    
    mc_fileList = ROOT.vector('string')()
    for mc_fin in [
        f"{remote_path}/{MODE}/{YEAR}/MU/MC_{decay}_{YEAR}_MU.root",
        f"{remote_path}/{MODE}/{YEAR}/MD/MC_{decay}_{YEAR}_MD.root",
        ]:
        mc_fileList.push_back(mc_fin)
    
    mc_in_RDF = RDF(
    f"{folder}/DecayTree",
    mc_fileList
    )#.Filter(f"{TRUTHMATCHING}")

    print(c("TRUTHMATHING **NOT** APPLIED").red)

    mc_in_RDF = mc_in_RDF.Define(f"CosXY_Mu_plus_{hadron}",              f"GenerateCosXYD0Mu({hadron}_PX, {hadron}_PY, Mu_plus_PX, Mu_plus_PY)") \
                        .Define(f"CosXYZ_Mu_plus_{had_daughter_2}",     f"GenerateCosXYZ_hMu({had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                        .Define(f"CosXYZ_Mu_plus_{had_daughter_1}",     f"GenerateCosXYZ_hMu({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ)") \
                        .Define(f"B_plus_4Vec",                         f"Combine2Body({hadron}_PX, {hadron}_PY, {hadron}_PZ, {hadron}_M ,Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66)") \
                        .Define(f"B_plus_FIT_LTIME", f"((B_plus_M*B_plus_FD_OWNPV)/B_plus_P)/3e2") \
                        .Define(f"Mu_plus_{had_daughter_1}_4Vec", f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass})") \
                        .Define(f"Mu_plus_{had_daughter_2}_4Vec", f"Combine2Body(Mu_plus_PX, Mu_plus_PY, Mu_plus_PZ, 105.66, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                        .Define(f"{had_daughter_1}_{had_daughter_2}_4Vec", f"Combine2Body({had_daughter_1}_PX, {had_daughter_1}_PY, {had_daughter_1}_PZ, {had_daughter_1_mass}, {had_daughter_2}_PX, {had_daughter_2}_PY, {had_daughter_2}_PZ, {had_daughter_2_mass})") \
                        .Define(f"Mu_plus_{had_daughter_1}_M", f"Mu_plus_{had_daughter_1}_4Vec.M()") \
                        .Define(f"Mu_plus_{had_daughter_2}_M", f"Mu_plus_{had_daughter_2}_4Vec.M()") \
                        .Define(f"{had_daughter_1}_{had_daughter_2}_M", f"{had_daughter_1}_{had_daughter_2}_4Vec.M()") \
                    

    _ntotal = mc_in_RDF.Count().GetValue()
    _nsel   = mc_in_RDF.Filter(full_selection).Count().GetValue()
    
    eff_cval, eff_std = eff(_nsel, _ntotal)

    print(c(f"decay: {decay}, ntot : {_ntotal}, npass : {_nsel}").green)
    print(c(f"decay: {decay}, eff_cval : {eff_cval} , eff_std : {eff_std}").green.bold)
    print()
