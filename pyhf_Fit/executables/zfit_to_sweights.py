# Executable to perform a fit to the Dz invariant mass and extract sWeights
# NOTE: naively here will implement exp + gaussian first; later will follow Matt's approach to 
# include the k-pi swap shapes in the bkg fit model
# __author__: Blaise Delaney
# __email__ : blaise.delaney@cern.ch

# === imports ===
from termcolor import colored
import ROOT
ROOT.gInterpreter.Declare('#include "{}"'.format("/home/blaise/cernbox/VubVcb/InclusiveVubVcb/Pipeline/common/EventMixing_D0Mu.h"))
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend, TLine, gPad
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
RDF = ROOT.ROOT.RDataFrame

# analysis python modules
from argparse import ArgumentParser
import os, glob
from pathlib import Path
from scipy import stats
import numpy as np
import boost_histogram as bh
import pandas
import json
import pyhf
import pickle
pyhf.set_backend("numpy", "minuit") # get uncertainties and covariance
import json, requests, jsonschema
from root_pandas import to_root 
import zfit
from zfit.loss import ExtendedUnbinnedNLL
from zfit.minimize import Minuit
os.environ["ZFIT_DISABLE_TF_WARNINGS"] = "1"

#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
from common.utils import *
from hepstats.splot import compute_sweights
plt.style.use(hep.style.LHCb)
#from utils import pltdist, plotfitresult
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif"
    })


# === PREPARE FILES FOR FITTING ===
# engage the parses to take in the data and MC files out of the selection pipeline
parser = ArgumentParser()
parser.add_argument('-i',  '--input'   , default="inp"          , required=True , help='Input Data/MC file')
parser.add_argument('-o',  '--output'  , default="oup"          , required=True , help='Output ROOT files, split by fit component')
parser.add_argument('-s',  '--sample'  , default="DCS"          , required=True , help="Sample in fitter", choices=["DCS", "CF", "MISID", "COMB"])
parser.add_argument('-p',  '--plotsdir', default="plots/zfit_sw_plots", required=False, help='Plot folder')
parser.add_argument('-v',  '--verbose' , action ="store_true"   , required=False, help='Verbose zfit mode')
parser.add_argument('-w',  '--write'   , action ="store_true"  , required=False, help='Write ROOT files?')
opts = parser.parse_args()


# ------------------------------------------------------------------------------------------------------------------------------------------
# config
INPUT_FILE    = opts.input
INPUT_SAMPLE  = opts.sample
OUTPUT_FILE   = opts.output
PLOTS_DIR     = opts.plotsdir
IS_VERBOSE    = opts.verbose
WRITE_TUPLE   = opts.write   

# additional pars
ORIGIN_PATH="/home/blaise/scratch/ROOTFILES/fit/D0MuNu/2018"
DESTINATION_PATH = "/home/blaise/cernbox/VubVcb/TOYS/pyhf_fit/fit_pipeline/common/ntuples"
COLUMNS=["B_plus_M", "B_plus_MCORR", "B_plus_FIT_LTIME", "D0_M", "K_minus_ID", "Mu_plus_ID", "D0_IPCHI2_OWNPV", "D0_Pperp_BFV"] # read in only what is needed and propagate 

# sample-specific weights
if INPUT_SAMPLE=="COMB": 
    print(colored("\nNOTE: detected combinatorial sample -> added `GBR_w` branch to the input branches", "blue"))
    COLUMNS+=["GBR_w"] # need the evt mixing GBR weight to recover the correct shapes
if INPUT_SAMPLE=="MISID": 
    print(colored("\nNOTE: detected misid sample -> added `fakemuon_w` branch to the input branches", "blue"))
    COLUMNS+=["fakemuon_w"] # need the evt mixing GBR weight to recover the correct shapes

# map the sample to a selection:
#   - kaon-muon sign combination for the DCS/CF
#   - visible mass > 3500 [hoping for a 50x increase in misID stats w/o prescaling]
#   - allow for Dz sidebands to get a better handle on fakes  
#   - load in a json schema for the cuts applied to the fit templates
assert( os.path.exists( f"/home/blaise/cernbox/VubVcb/TOYS/pyhf_fit/fit_pipeline/config/config.json" ) )
with open("/home/blaise/cernbox/VubVcb/TOYS/pyhf_fit/fit_pipeline/config/config.json") as config_file:
    fit_config = json.load(config_file)

min_ltime = fit_config["B_plus_FIT_LTIME"]["range"][0]
max_ltime = fit_config["B_plus_FIT_LTIME"]["range"][-1]

mass_type_fitvar = "B_plus_MCORR"
min_mcorr = fit_config[f"{mass_type_fitvar}"]["range"][0]
max_mcorr = fit_config[f"{mass_type_fitvar}"]["range"][-1]
low_visM_cut  = 3500
FS_sel = {
    "DCS"   : f"(K_minus_ID*Mu_plus_ID>0) && B_plus_M>{low_visM_cut} && (D0_M>=1790 && D0_M<=1940) && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) && ({mass_type_fitvar}>={min_mcorr} && {mass_type_fitvar}<={max_mcorr})",
    "CF"    : f"(K_minus_ID*Mu_plus_ID<0) && B_plus_M>{low_visM_cut} && (D0_M>=1790 && D0_M<=1940) && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) && ({mass_type_fitvar}>={min_mcorr} && {mass_type_fitvar}<={max_mcorr})",
    "COMB"  : f"B_plus_M>{low_visM_cut} && (D0_M>=1790 && D0_M<=1940) && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) && ({mass_type_fitvar}>={min_mcorr} && {mass_type_fitvar}<={max_mcorr})",
    #"MISID" : f"(K_minus_ID*Mu_plus_ID>0) && B_plus_M>{low_visM_cut} && (D0_M>=1790 && D0_M<=1940) && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) && ({mass_type_fitvar}>={min_mcorr} && {mass_type_fitvar}<={max_mcorr})", #NOTE: must apply DCS to MISID as the CF and DCS misID sample are sensitive to different backgrounds
    "MISID" : f"B_plus_M>{low_visM_cut} && (D0_M>=1790 && D0_M<=1940) && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) && ({mass_type_fitvar}>={min_mcorr} && {mass_type_fitvar}<={max_mcorr})", #NOTE: must apply DCS to MISID as the CF and DCS misID sample are sensitive to different backgrounds
}
# ------------------------------------------------------------------------------------------------------------------------------------------

# check the input is sensible
assert( os.path.exists( f"{ORIGIN_PATH}/{INPUT_FILE}" ) )
os.system(f"mkdir -p {PLOTS_DIR}")

print (f"\n\nThis executable will enage a fit to the D0 invariant mass and derive sWeights")
print(colored("NOTE:", "red", attrs=["bold", ]), "this will perform a", colored("naive", attrs=["underline"]), "fit to the Dz mass with exp+gauss, i.e. the kaon-pion swap is neglected + need to add DCB pdfs [to be added later]\n")
print("="*100)
print(colored('CONFIG:', 'cyan', attrs=['bold', 'dark', 'underline']))
print(f"INPUT FILE = {ORIGIN_PATH}/{INPUT_FILE}\nMODE = {INPUT_SAMPLE} condition : {FS_sel[INPUT_SAMPLE]}\nOUTPUT FILE = {DESTINATION_PATH}/{OUTPUT_FILE}\nPLOTS DIRECTORY = {PLOTS_DIR}")
print("="*100)
print()

# generate D0 momentum orthogonal to B FV
in_RDF = RDF("DecayTree", f"{ORIGIN_PATH}/{INPUT_FILE}").Filter(FS_sel[INPUT_SAMPLE])
in_RDF = in_RDF.Define(f"B_plus_PV_3Vec",           f"gen3Vec(B_plus_OWNPV_X, B_plus_OWNPV_Y, B_plus_OWNPV_Z)") \
               .Define(f"B_plus_SV_3Vec",   f"gen3Vec(B_plus_SV_X, B_plus_SV_Y, B_plus_SV_Z)") \
               .Define(f"BflightVector",    f"subtract3Vec( B_plus_SV_3Vec, B_plus_PV_3Vec)") \
               .Define(f"D0_3momVector",    f"gen3Vec(D0_PX, D0_PY, D0_PZ)") \
               .Define(f"D0_Pperp_BFV",     f"D0_3momVector.Perp(BflightVector)")

# now actually read in files as pandas DFs
indf = pandas.DataFrame(in_RDF.AsNumpy(columns=COLUMNS))
# take the log of the D0 IPCHI2 to generate a third fit var
print(colored("Generated additional branch `D0_Pperp_BFV`", "blue"))
print(colored("Generated additional branch `log_D0_IPCHI2_OWNPV`\n", "blue"))
indf["log_D0_IPCHI2_OWNPV"] = np.log(indf["D0_IPCHI2_OWNPV"])

# === ZFIT ===
h_c = "D0"
h_c_nomM = 1865.

# fit the chamr hadron h_c mass
charm_M = indf[f"{h_c}_M"].to_numpy()

# initialise 
bounds=(1790, 1940)
obs = zfit.Space("x", limits=bounds)
data_ = zfit.data.Data.from_numpy(obs=obs, array=charm_M)
Nsig = zfit.Parameter("Nsig", 10000, 0., len(charm_M))
Nbkg = zfit.Parameter("Nbkg", 10000, 0., len(charm_M))

# pdf pars
mu = zfit.Parameter("mu", h_c_nomM, 1835., 1895.)
sigma = zfit.Parameter("sigma", 30, 0, 60)

if INPUT_SAMPLE!="MISID": 
    data_ = zfit.data.Data.from_numpy(  obs=obs, 
                                        array=charm_M,
                                        weights=None    )
if INPUT_SAMPLE=="MISID": 
    print( colored("NOTE: fitting a weighted misID dataset to account for anti-prescale 1/0.02 in stripping...\n", "red", attrs=["bold"]) )
    data_ = zfit.data.Data.from_numpy(  obs=obs, 
                                        array=charm_M,
                                        weights=indf["fakemuon_w"].to_numpy()    )

lambda_ = zfit.Parameter("lambda",-0.03, -4.0, 0.0)

# model
signal = zfit.pdf.Gauss(obs=obs, mu=mu, sigma=sigma).create_extended(Nsig)
background = zfit.pdf.Exponential(obs=obs, lambda_=lambda_).create_extended(Nbkg)
tot_model = zfit.pdf.SumPDF([signal, background])
data_ = zfit.data.Data.from_numpy(obs=obs, array=charm_M)

# minimise extended nll, all params floating
nll = ExtendedUnbinnedNLL(model=tot_model, data=data_)
minimizer = Minuit()
result = minimizer.minimize(loss=nll)
result.hesse()
print()
print(result)

s_weights = compute_sweights(tot_model, charm_M)
indf[f"real{h_c}_w"] = s_weights[Nsig]
indf[f"fake{h_c}_w"] = s_weights[Nbkg]


# === VIZ ===
plt_labels = {
    "DCS"   : "DCS",
    "CF"    : "CF",
    "COMB"  : "Combinatorial",
    "MISID" : "misID DCS"
}

NBINS = 100
binw_MeV = (bounds[1]- bounds[0])/NBINS

# fitted Dz M
fit_fig = plt.figure()
plt.minorticks_on()
plt.tick_params(axis='both', which='major', direction="in")
plt.tick_params(axis='both', which='minor', direction="in")
plt.plot([], [], ' ', label=f"{plt_labels[INPUT_SAMPLE]}") # hack: label sample
pltdist(charm_M, NBINS, bounds)
plotfitresult(background, bounds, NBINS, label=r"Fake $D^{0}$ Background", color="teal", marker=".", markersize=.01)
plotfitresult(signal, bounds, NBINS, label=r"Real $D^{0}$ Signal", color="royalblue", marker=".", markersize=.01)
plotfitresult(tot_model, bounds, NBINS, label=r"Total Fit Model", color="crimson", marker=".", markersize=.01)
plt.xlabel(r"$m(K\pi)$ [MeV/c$^2$]", fontsize=25)
plt.ylabel(f"Events / ({binw_MeV} MeV) ", fontsize=25)
plt.legend(loc="best", fontsize=20)
fit_fig.savefig(f"{PLOTS_DIR}/D0M_fit_{INPUT_SAMPLE}.pdf")
fit_fig.savefig(f"{PLOTS_DIR}/D0M_fit_{INPUT_SAMPLE}.png")

# check the sweights behave reasonably
assert((s_weights[Nsig] + s_weights[Nbkg]).all() == 1.)
print(colored("\nCHECK:", attrs=["bold", "underline"]), "computed sWeights add to unity?", colored("True", "white", "on_green"), "\n")


# === PREPARE OUTPUT ===
# write boost_histograms with weighted storage
n_hists = len(fit_config.keys()) # how many fit variables?
fitvars = list(fit_config.keys()) # which fit variables?

print(colored(f"Now moving onto writing the dedicated boost histogram(s) with weighted storage; detected {n_hists} vars: {fit_config.keys()} fitvars\n", "blue", attrs=["dark", "bold"]))

# figure out what weights go to storage
if INPUT_SAMPLE=="MISID":
    wts = indf["fakemuon_w"]*indf[f"real{h_c}_w"] # if misID, weights = sw x fakemuon anti-prescale weights
if INPUT_SAMPLE=="COMB":
    wts = indf["GBR_w"]*indf[f"real{h_c}_w"] # if COMB, weights = sw x GBR weight from evt mixing
else:
    wts = indf[f"real{h_c}_w"] # in any other case, we only care about the sw from fit to D0 mass

_nbins_1  = fit_config[fitvars[0]]["nbins"]
_minvar_1 = fit_config[fitvars[0]]["range"][0] 
_maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

# only one fitvar
if n_hists==1:
    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        wbh = bh.Histogram( 
            bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 ), storage=bh.storage.Weight() 
            )
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        wbh = bh.Histogram(
            bh.axis.Variable(fit_config[fitvars[0]]["range"]), storage=bh.storage.Weight() #custom binning 
        )
    wbh.fill( sample=indf[fitvars[0]], weight=wts)
    print(f"Histogram summary: {wbh}")

# two fitvars
if n_hists==2:
    _nbins_2  = fit_config[fitvars[1]]["nbins"]
    _minvar_2 = fit_config[fitvars[1]]["range"][0] 
    _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 
    
    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        ax_1 = bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        ax_1 = bh.axis.Variable(fit_config[fitvars[0]]["range"])

    if fit_config[fitvars[1]]["is_var_axis"]=="False":
        ax_2 = bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
    if fit_config[fitvars[1]]["is_var_axis"]=="True":
        ax_2 = bh.axis.Variable(fit_config[fitvars[1]]["range"])

    wbh = bh.Histogram( ax_1, ax_2, storage=bh.storage.Weight())
    samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
    wbh.fill( *samples, weight=wts)
    print(f"Histogram summary: {wbh}")

# three fitvars
if n_hists==3:
    _nbins_2  = fit_config[fitvars[1]]["nbins"]
    _minvar_2 = fit_config[fitvars[1]]["range"][0] 
    _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 
    _nbins_3  = fit_config[fitvars[2]]["nbins"]
    _minvar_3 = fit_config[fitvars[2]]["range"][0] 
    _maxvar_3 = fit_config[fitvars[2]]["range"][-1] 
    wbh = bh.Histogram( bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 ), bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2 ), bh.axis.Regular(_nbins_3, _minvar_3, _maxvar_3 ), storage=bh.storage.Weight())
    samples = [ np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]]), np.array(indf[fitvars[2]])]
    wbh.fill( *samples, weight=wts)
    print(f"Histogram summary: {wbh}")

# write boost object to file   
os.system(f"mkdir -p /home/blaise/cernbox/VubVcb/TOYS/pyhf_fit/fit_pipeline/common/pkl")
os.system(f"mkdir -p {DESTINATION_PATH}")

with open(f"/home/blaise/cernbox/VubVcb/TOYS/pyhf_fit/fit_pipeline/common/pkl/{INPUT_SAMPLE}.pkl", "wb") as bh_fout:
    pickle.dump(wbh, bh_fout)
print(colored(f"\nSUCCESS: fit executed successfully and boost objects written to file common/pkl/{INPUT_SAMPLE}.pkl\n", "green", attrs=["bold", "dark"]))

if WRITE_TUPLE is True:
    print("="*100)
    print(colored("You have requested to write out the ntuple to the following ROOT file:\n", "cyan"))
    OUT_COLUMNS = COLUMNS+["realD0_w"]
    print(colored("OUFILE CONFIG:", "cyan", attrs=["bold", "underline", "dark"]))
    print(f"OUTPUT FILE = {DESTINATION_PATH}/{OUTPUT_FILE}/DecayTree" )
    print(f"BRANCHES WRITTEN TO FILE = {OUT_COLUMNS}")

    # write ROOT file
    to_root(indf, f"{DESTINATION_PATH}/{OUTPUT_FILE}", key="DecayTree")
    print(colored("\nSUCCESS: output ntuple written to file", "green", attrs=["bold", "dark"]))
    print("="*100)
