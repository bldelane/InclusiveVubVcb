'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

=== Backend to the combinations of years of data taking ===

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from os import ttyname
from common.header import *
from termcolor2 import c
import pprint 
import boost_histogram as BH

parser = ArgumentParser()
parser.add_argument('-y','--year', required=True, help='Year of data taking [2011,2012,2015,2016,2017,2018]')
parser.add_argument('-s','--simfit', action="store_true", help="engage in generating a schema with lumi scaled to the fraction of the total 2016-2018 nominal integrated luminosity")
opts = parser.parse_args()


def load_bh(idx):
    """load data-driven templates from boost histograms"""
    indf = rfiles[idx]
    samples = [np.array(indf[FITVARS[0]])]
    
    if idx=="MisID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="EvtMix":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="Sig":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to jpsi mass

    _nbins_1  = fit_config[FITVARS[0]]["nbins"]
    _minvar_1 = fit_config[FITVARS[0]]["range"][0] 
    _maxvar_1 = fit_config[FITVARS[0]]["range"][-1] 

    # handle variable or fixed binning
    if fit_config[FITVARS[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[FITVARS[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[FITVARS[0]]["range"], underflow=False, overflow=False )
    
    wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
    wbh.fill( *samples, weight=wts )    
    
    return wbh

def compute_y_eff(eff_dict, mode, year):
    mag_up = ufloat(eff_dict[mode][year]["MU"]["Total"]["eff"], eff_dict[mode][year]["MU"]["Total"]["err"])
    mag_down = ufloat(eff_dict[mode][year]["MD"]["Total"]["eff"], eff_dict[mode][year]["MD"]["Total"]["err"])
    print(c(f"EFFS {mode} {year}: MagUp: {mag_up}; MagDown: {mag_down}").blue)
    print(c(f"Average: {(mag_up+mag_down)/2.}\n").magenta)
    return (mag_up+mag_down)/2.

# assign arg 
YEAR = opts.year

# binning
# -------
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json") as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").magenta)
print(c(fit_config).magenta)

# extract year efficiency of the inclusive sample
# -----------------------------------------------
# read in the json file with total efficiency
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Total.json") as f_in:
    tot_eff_dict = json.load(f_in)

mu_eff_dict = {}
tau_eff_dict = {}
# take the average of per-pol eff
for y in ["2016","2017", "2018"]:
    mu_eff_dict[y]  = compute_y_eff(tot_eff_dict, "Bc2JpsiMuNu",  y)
    tau_eff_dict[y] = compute_y_eff(tot_eff_dict, "Bc2JpsiTauNu", y)

# luminosity, also accounting for efficiencies
# --------------------------------
eff_lumi_raw = {
        "2018" : 2.1*mu_eff_dict["2018"], # 2.1 invfb lumi of data, but only an eff-valued portion of the data is retained
        "2017" : 1.7*mu_eff_dict["2017"],
        "2016" : 1.6*mu_eff_dict["2016"], # N.B. something is wrong with 2016 tau, so assume eff the same in 2016 and 2017
    }
print(c(f"Efficiency x lumiread in: \n {eff_lumi_raw}").yellow)
NORMALISATION = np.sum(list(eff_lumi_raw.values()))
print(c(f"=> NORMALISATION: {NORMALISATION}").red.bold)

if opts.simfit:
    lumi_factors = {
        "2018" : (2.1*mu_eff_dict["2018"]) / NORMALISATION, 
        "2017" : (1.7*mu_eff_dict["2017"]) / NORMALISATION, 
        "2016" : (1.6*mu_eff_dict["2016"]) / NORMALISATION, # N.B. something is wrong with 2016 tau, so assume eff the same in 2016 and 2017N.B. something is wrong with 2016 tau, so assume eff the same in 2016 and 2017
    }
    print(c(f"\n==========================================================").green)
    print(c(f"Engaged with fractional lumi x eff scaling").green)
    print(c(f"Taking into account effs and lumis: fractions = {lumi_factors}").green)
    print(c(f"Sanity check that lumis x eff are normalised correctly: sum = {np.sum(list(lumi_factors.values()))}").green)
else: 
    lumi_factors = {
        f"{YEAR}" : ufloat(1.0, 0.0)
    }
print(c(f"\nLumi sig scaling [efficiency-corrected]: {lumi_factors[YEAR]}").green.bold)
print(c(f"==========================================================\n").green)


# load in the pkl'd sW'd [all phase space] dataframes for the desired year
misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/JpsiMuNu/{YEAR}/MisID.pkl")
comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/JpsiMuNu/{YEAR}/EvtMix.pkl")
sig_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/JpsiMuNu/{YEAR}/Sig.pkl")

# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(misid_df["misid_w"]*misid_df["sw"]), np.sqrt(np.sum( (misid_df["misid_w"]*misid_df["sw"])**2  )) ) 

# a big ugly, from porting from dev.ipynb
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])

print(c(f"nBins = {TOT_BINS}").yellow)

# trackc omponents (might want to remove this in tidier version of code)
rfiles = {}
rfiles["Sig"] = sig_df
rfiles["MisID"] = misid_df
rfiles["EvtMix"] = comb_df

data_driven_samples = np.array(["MisID", "EvtMix"]) # data-driven templates 
MC_samples = np.array(["Bc2JpsiMuNu",  "Bc2JpsiTauNu", "Bc2ccX"]) # sim templates
fit_components = np.concatenate([data_driven_samples, MC_samples], axis=0)

# load seperately-generated MC hists
MC_dfs = {}
for m in MC_samples: 
    with open(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/JpsiMuNu/{YEAR}/{m}.pkl", "rb") as fsim: 
        MC_dfs[m] = pickle.load(fsim)
        print(c(f"Filling mode : {m}").cyan)
        print(c("Done").blue)

# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
print("\nLoading data-driven hists:")
for comp in data_driven_samples: 
    bh = load_bh(comp)
    unscaled_integral = np.sum(bh.view().value.flatten())
    template_vals[f"{comp}"] = bh.view().value.flatten() / unscaled_integral
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten() / unscaled_integral # weighted data-driven histograms; note how error and not variance is stored
    assert(abs( np.sum( template_vals[f"{comp}"] ) - 1.0 ) <= 0.00001)
    assert( ((template_vals[f"{comp}"] / template_errs[f"{comp}"]) / (bh.view().value.flatten() / (bh.view().variance**.5).flatten())).all() <=1.00001 )
    print(c(f"{comp}: done, normalised to unity\n").green)

# MC has a regular storage
for sim in MC_samples:
    unscaled_integral = np.sum( MC_dfs[sim].view().flatten() )
    template_vals[f"{sim}"] = MC_dfs[sim].view().flatten() / unscaled_integral
    template_errs[f"{sim}"] = np.sqrt( MC_dfs[sim].view().flatten() ) / unscaled_integral # poisson error; note how the error and not variance is stored
    assert(abs( np.sum( template_vals[f"{sim}"] ) - 1.0 ) <= 0.00001)
    assert( ((template_vals[f"{sim}"] / template_errs[f"{sim}"]) / (MC_dfs[sim].view().flatten() / (MC_dfs[sim].view().flatten()**.5) ) ).all() <=1.00001 )
    print(c(f"{sim}: done, normalised to unity\n").green) 

# load data but do not normalise!
template_vals["Sig"] = load_bh("Sig").view().value.flatten()
template_errs["Sig"] = (load_bh("Sig").view().variance**.5).flatten()
assert(abs( np.sum( template_vals["Sig"] ) - 1.0 ) >= 0.001)

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)

#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: Sig->sum(w), others->mu_i
assert(template_vals["Sig"].all()>0)

# prepare samples: derive scaling factor from observations
s_i = template_errs["Sig"]**2/template_vals["Sig"] # for weighted fits, sum(w**2) is err; correctness check: done
assert(s_i.all() == (load_bh("Sig").view().variance/load_bh("Sig").view().value).all()) # check that the error was stored in template_errs, and not variance

# scaled observed data
obs = template_vals["Sig"]/s_i

# scale the rest accordingly
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations) [note how normalise to the binned template area before B&Z-scaling]
for comp in fit_components:
    muprime_vals[f"{comp}"] = template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = template_errs[f"{comp}"] / s_i 

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["Sig"]) # n->n'

# read in the unscaled misid constraint; again, the constraint needs to be derived for a pdf not normalised to unity, owing to B&Z
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["MisID"].sum()

print(c(f"\n===================================").yellow)
print(c(f"misID constraint in physical space: {raw_misid_y_constr}").yellow)
print(c(f"===================================\n").yellow)

#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema...").green)

#====================================
# R(Jpsi), eff-corrected constraint #
#====================================
# fix tau to mu from R(jpsi), LHCB-PAPER-2017-035
r_jpsi_cval = 0.71
r_jpsi_stat = 0.17
r_jpsi_syst = 0.18
r_jpsi_err = np.sqrt( r_jpsi_stat**2 + r_jpsi_syst**2 )
r_jpsi_BF = ufloat(r_jpsi_cval, r_jpsi_err)

# need to take into account the BF(tau->mu nu nu)
BF_tau_to_mu = ufloat( 17.39e-2, 0.04e-2) # from PDG
RR_jpsi = r_jpsi_BF * BF_tau_to_mu 

print(f"Raw R(Jpsi) x BF(tau->mu) = {RR_jpsi}")
print(f"Efficiency ratio tau/mu = {tau_eff_dict[YEAR]/mu_eff_dict[YEAR]}")
r_jpsi  = RR_jpsi * (tau_eff_dict[YEAR]/mu_eff_dict[YEAR]) # r(jpsi) x BF(tau->mu nu nu) x eff ratio
print(c(f"efficiency-corrected RR_jpsi constraint = {r_jpsi.n} +/- {r_jpsi.s}").red)

#======================
# Simulated Templates
#======================
signal_Bc2JpsiMuNu_sample = {
                    "name": f"signal_Bc2JpsiMuNu_{YEAR}",
                    "data": muprime_vals["Bc2JpsiMuNu"].tolist(),
                    "modifiers": [
                        #{"name": f"jpsimunu_intgr_{YEAR}", "type": "normfactor", "data":None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2JpsiMuNu"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        {"name": f"jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}
# fix to jpsimunu component with eff-corrected R(jpsi)
Bc2JpsiTauNu_sample = {
                    "name": f"Bc2JpsiTauNu_{YEAR}",
                    "data": muprime_vals["Bc2JpsiTauNu"].tolist(),
                    "modifiers": [
                        {"name": f"jpsitaunu_intgr_{YEAR}", "type": "normfactor", "data":None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2JpsiTauNu"].tolist() }, # branch-§speficific per-bin variation
                        {"name": f"jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"r_jpsi_{YEAR}", "type": "normfactor", "data": None}, # fix this
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
#                        {"name": "r_jpsi", "type": "normsys", "data": {"lo" : r_jpsi.n - r_jpsi.s, "hi" : r_jpsi.n + r_jpsi.s} },
                        ]
}

# fixed fractions of Bc->DC/feeddown munu decays; floating template yield
Bc2ccX_sample = {
                    "name": f"Bc2ccX_{YEAR}",
                    "data": muprime_vals["Bc2ccX"].tolist(),
                    "modifiers": [
                        #{"name": f"bc2ccx_intgr_{YEAR}", "type": "normfactor", "data":None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2ccX"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"bc2ccx_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

#======================
# Data-Driven Templates
#======================
comb_sample = {
                    "name": f"combinatorial_{YEAR}",
                    "data": muprime_vals["EvtMix"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["EvtMix"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}
misid_sample = {  
                    "name": f"misid_{YEAR}",
                    "data": muprime_vals["MisID"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MisID"].tolist() }, # branch-speficific per-bin variation
                        #{"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None}, # floating
                        ]
}


# ===========================================================
#                     BUILD THE MODEL                       #
# ===========================================================
spec = {
    "channels": [
        {
            "name": f"singlechannel_{YEAR}",
            "samples": [
                signal_Bc2JpsiMuNu_sample,
                Bc2JpsiTauNu_sample,
                Bc2ccX_sample,
                comb_sample,
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": f"singlechannel_{YEAR}",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": "sig_y_extraction",
            "config": {
                "poi": "jpsimunu_sig_y",
                "parameters": [
                   
                    # freely floating signal yield 
                    # ---------------------------------
                    {"name": f"jpsimunu_sig_y", "bounds": [[ 0, 10*obs.sum() ]], "inits":[1e4]},
                    {"name": f"sig_lumi_factor_{YEAR}", "inits": [lumi_factors[YEAR].n], "fixed": True}, # fix template eff-corrected r-Jpsi

                    # integrals of pdfs -> all pdfs integrate to unity; this allows to imposefg constraints to relate the raw yields of components
                    # --------------------------------------------------------------------------------------------------------------------------
                    #{"name": f"jpsimunu_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["Bc2JpsiMuNu"])], "fixed": True}, # fix template normalisation to unity
                    {"name": f"jpsitaunu_intgr_{YEAR}", "inits": [np.sum(muprime_vals["Bc2JpsiMuNu"])/np.sum(muprime_vals["Bc2JpsiTauNu"])], "fixed": True}, # fix template normalisation to unity
                    #{"name": f"bc2ccx_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["Bc2ccX"])], "fixed": True}, # fix template normalisation to unity
                    
                    # freely floating bkg components
                    # ------------------------------
                    {"name": f"comb_y_{YEAR}", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name": f"misid_y_constr_{YEAR}", "inits":[16000], "bounds": [[1e-6, obs.sum()]], "fixed":False},
                    {"name": f"bc2ccx_y_{YEAR}", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name": f"r_jpsi_{YEAR}", "inits": [r_jpsi.n], "fixed": True}, # fix template eff-corrected r-Jpsi
                ]
            }
        }
    ],
    "version": "1.0.0"
}


# =================================================================================================
#               Write schema; this will be merged with other years of data taking                 #
# =================================================================================================
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_{YEAR}.json"
if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
with open(SCHEMA,'w') as outfile:
    outfile.write(spec)

print(c(f"SCHEMA written for {YEAR} successfully to /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_{YEAR}.json").green)

# ===========================================================================
#               Write specific templates for each component                 #
# ===========================================================================
interp_code = "code1"

signal_Bc2JpsiMuNu_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": f"signal_Bc2JpsiMuNu_{YEAR}",
                        "data": muprime_vals["Bc2JpsiMuNu"].tolist(),
                        "modifiers": [
                            {"name": f"jpsimunu_intgr_{YEAR}", "type": "normfactor", "data":None},
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None}, # fix template eff-corrected r-Jpsi
                            {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2JpsiMuNu"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]    
}

Bc2JpsiTauNu_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": f"Bc2JpsiTauNu_{YEAR}",
                        "data": muprime_vals["Bc2JpsiTauNu"].tolist(),
                        "modifiers": [
                            {"name": f"jpsitaunu_intgr_{YEAR}", "type": "normfactor", "data":None},
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": f"r_jpsi_{YEAR}", "type": "normfactor", "data": None },
                            #{"name": "r_jpsi", "type": "normsys", "data": {"lo" : r_jpsi.n - r_jpsi.s, "hi" : r_jpsi.n + r_jpsi.s} },
                            {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None}, # fix template eff-corrected r-Jpsi
                            {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2JpsiTauNu"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]    
}


misid_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": f"MisID_{YEAR}",
                        "data": muprime_vals["MisID"].tolist(),
                        "modifiers": [
                            #{"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                            {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None}, # floating
                            {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MisID"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]    
}

comb_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": f"combinatorial_{YEAR}",
                        "data": muprime_vals["EvtMix"].tolist(),
                        "modifiers": [
                            {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None}, # floating
                            {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["EvtMix"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]    
}

bc2ccx_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": f"Bc2ccX_{YEAR}",
                        "data": muprime_vals["Bc2ccX"].tolist(),
                        "modifiers": [
                            {"name": f"bc2ccx_intgr_{YEAR}", "type": "normfactor", "data":None},
                            {"name": f"bc2ccx_y_{YEAR}", "type": "normfactor", "data": None}, # floating
                            {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Bc2ccX"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]    
}


# write to file    
signal_Bc2JpsiMuNu_spec = str(json.dumps(signal_Bc2JpsiMuNu_template, indent=4)).replace("None", "null").replace("True", "true")
signal_Bc2JpsiMuNu_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_sig_{YEAR}.json"
with open(signal_Bc2JpsiMuNu_SCHEMA,'w') as outfile:
    outfile.write(signal_Bc2JpsiMuNu_spec)

misid_spec = str(json.dumps(misid_template, indent=4)).replace("None", "null").replace("True", "true")
misid_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_misid_{YEAR}.json"
with open(misid_SCHEMA,'w') as outfile:
    outfile.write(misid_spec)

comb_spec = str(json.dumps(comb_template, indent=4)).replace("None", "null").replace("True", "true")
comb_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_comb_{YEAR}.json"
with open(comb_SCHEMA,'w') as outfile:
    outfile.write(comb_spec)

Bc2JpsiTauNu_spec = str(json.dumps(Bc2JpsiTauNu_template, indent=4)).replace("None", "null").replace("True", "true")
Bc2JpsiTauNu_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_tau_{YEAR}.json"
with open(Bc2JpsiTauNu_SCHEMA,'w') as outfile:
    outfile.write(Bc2JpsiTauNu_spec)

bc2ccx_spec = str(json.dumps(bc2ccx_template, indent=4)).replace("None", "null").replace("True", "true")
bc2ccx_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_JpsiMuNu_bc2ccx_{YEAR}.json"
with open(bc2ccx_SCHEMA,'w') as outfile:
    outfile.write(bc2ccx_spec)
