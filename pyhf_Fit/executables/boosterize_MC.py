from re import I
import urllib
from common.header import *
from termcolor2 import c
import requests

parser = ArgumentParser()
parser.add_argument('-y','--year',     required=True, help='Year of data-taking/MC', choices=["2011", "2012", "2015", "2016", "2017", "2018"])
parser.add_argument('-m','--mode',     required=True, help='Decay mode, choose from [D0MuNu|JpsiMuNu]', choices=["D0MuNu", "JpsiMuNu"])
parser.add_argument('-p','--path',     default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl', help='Path for output')
parser.add_argument('-c','--config',   default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json', help='Path to fit config')
parser.add_argument('-H','--hardcode', action="store_true", help="Hardcode the D*/D0 ratio to produce a unique signal template? [default false]")
opts = parser.parse_args()

YEAR = opts.year
MODE = opts.mode
CONFIG = opts.config
OUT_MC_PATH = opts.path
MC_PATH = f"/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/MC/fit/{MODE}/{YEAR}"

# read in the json template cut config
assert( os.path.exists( CONFIG ) )
with open( CONFIG ) as config_file:
    fit_config = json.load(config_file)

min_ltime=0.000; max_ltime=0.005;
mass_var_range = "B_plus_MCORR"
min_mcorr = 4200; max_mcorr = 7200;

if MODE=="D0MuNu":

    low_visM_cut  = 3500.000
    D0_M_LOW = 1810; D0_M_HIGH=1920;
    mc_sel = f"B_plus_M>{low_visM_cut} && (D0_M>={D0_M_LOW} && D0_M<={D0_M_HIGH}) \
            && (B_plus_FIT_LTIME>{min_ltime} && B_plus_FIT_LTIME<={max_ltime}) \
            && ({mass_var_range}>={min_mcorr} && {mass_var_range}<={max_mcorr})",
    print(c(f"SELECTION: {mc_sel}").blue)
    
    D0pi0_MC16 = pandas.DataFrame(RDF("DecayTree", f"{MC_PATH}/Bc2D0pi0MuNu.root").Filter(mc_sel).AsNumpy())
    D0g_MC16 = pandas.DataFrame(RDF("DecayTree", f"{MC_PATH}/Bc2D0gMuNu.root").Filter(mc_sel).AsNumpy())

    # build the D* template based on ratio of BF from PDG 
    # ---------------------------------------------------
    BF_Dst_toD0pi0 = ufloat(0.647, 0.009) # PDG
    BF_Dst_toD0g   = ufloat(0.353, 0.009) # PDG
    
    Dzg_frac2Dzpi0 = (BF_Dst_toD0g/BF_Dst_toD0pi0).n # 1 Dzg every 2 Dzpi0
    assert(len(D0pi0_MC16) * Dzg_frac2Dzpi0 <= len(D0g_MC16))
    D0g_MC16 = D0g_MC16.sample(frac=1)[:int(len(D0pi0_MC16)*Dzg_frac2Dzpi0)] # shuffle and pick the number of events
    assert(int(len(D0pi0_MC16) * Dzg_frac2Dzpi0) == len(D0g_MC16))

    # study how many events are loaded in the Dst samples
    print(f"\nWe have loaded in the {MC_PATH}/D(st) samples passing the full selection")
    print(f"-   Dst->D0pi0 has {len(D0pi0_MC16)} events")
    print(f"-   Dst->D0g has {len(D0g_MC16)} events")
    print(c(f"\nRatio of yields of D0g/D0pi0 = {len(D0g_MC16)/len(D0pi0_MC16)}, expect ~{35.3/64.7} [source: PDG]", "red").magenta)

    # stack & fill D* bh + assign name 
    Dst_MC16 = pandas.concat([D0pi0_MC16, D0g_MC16])
    Dst_MC16.name="Dst_MC"
    # read in D0 + assign name
    D0_MC16 = pandas.DataFrame(RDF("DecayTree", f"{MC_PATH}/Bc2D0MuNu.root").Filter(mc_sel).AsNumpy())
    D0_MC16.name="D0_MC"
    print(c(f"D0 MC has {len(D0_MC16)} events").magenta)
    # # read in the inclusive misID and assign a name
    inclb2D0X_MC16 = pandas.DataFrame(RDF("DecayTree", f"{MC_PATH}/inclb2D0X.root").Filter(mc_sel).AsNumpy())
    inclb2D0X_MC16.name = "inclb2D0X"


    for template in [D0_MC16, Dst_MC16, inclb2D0X_MC16]:
        assert(min(template["B_plus_M"])>low_visM_cut)
        assert(min(template["B_plus_FIT_LTIME"])>min_ltime)
        assert(max(template["B_plus_FIT_LTIME"])<=max_ltime)
        assert(min(template["D0_M"])>= D0_M_LOW)
        assert(max(template["D0_M"])<= D0_M_HIGH)
    print(c("\n========================").green)
    print(c("Sanity checks passed :-)").green)
    print(c("========================\n").green)

    # === PREPARE OUTPUT ===
    # write boost_histograms with weighted storage
    n_hists = len(fit_config.keys()) # how many fit variables?
    fitvars = list(fit_config.keys()) # which fit variables?
    print(c(f"Fit vars : {fitvars}").red)

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    if opts.hardcode:
        '''In this case we generate one single template, with the D*/D0 fraction given by the 
        efficiency-corrected ratio of the BFs, using 3ptSR calculations 
        '''

        # read in the json file with total efficiency
        with open('/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Total.json') as f_in:
            tot_eff_dict = json.load(f_in)
        #print(tot_eff_dict["Bc2D0gMuNu"][YEAR]["MU"]["Total"])
        D0g_MU_eff = ufloat(tot_eff_dict["Bc2D0gMuNu"][YEAR]["MU"]["Total"]["eff"],tot_eff_dict["Bc2D0gMuNu"][YEAR]["MU"]["Total"]["err"])
        D0g_MD_eff = ufloat(tot_eff_dict["Bc2D0gMuNu"][YEAR]["MD"]["Total"]["eff"],tot_eff_dict["Bc2D0gMuNu"][YEAR]["MD"]["Total"]["err"])
        D0pi0_MU_eff = ufloat(tot_eff_dict["Bc2D0pi0MuNu"][YEAR]["MU"]["Total"]["eff"],tot_eff_dict["Bc2D0pi0MuNu"][YEAR]["MU"]["Total"]["err"])
        D0pi0_MD_eff = ufloat(tot_eff_dict["Bc2D0pi0MuNu"][YEAR]["MD"]["Total"]["eff"],tot_eff_dict["Bc2D0pi0MuNu"][YEAR]["MD"]["Total"]["err"])
        D0_MU_eff = ufloat(tot_eff_dict["Bc2D0MuNu"][YEAR]["MU"]["Total"]["eff"],tot_eff_dict["Bc2D0MuNu"][YEAR]["MU"]["Total"]["err"])
        D0_MD_eff = ufloat(tot_eff_dict["Bc2D0MuNu"][YEAR]["MD"]["Total"]["eff"],tot_eff_dict["Bc2D0MuNu"][YEAR]["MD"]["Total"]["err"])

        # take the average of per-pol eff
        eff_D0gMuNu = (D0g_MU_eff+D0g_MD_eff)/2
        eff_D0pi0MuNu = (D0pi0_MU_eff+D0pi0_MD_eff)/2
        eff_D0MuNu = (D0_MU_eff+D0_MD_eff)/2

        # === constrain D0/D* yields from theory and effs ===
        # CALCULATION BY MELIC ET AL, arXiv:1909.01213
        BF_Bc2D0MuNu_3ptSR = ufloat(2.4e-5, 0.4e-5)
        BF_Bc2DstMuNu_3ptSR = ufloat(7e-5, 3e-5)

        # take weighted averege for D* efficiencies
        Dst_toD0g_BF = ufloat(0.353, 0.009)
        Dst_toD0pi0_BF = ufloat(0.647, 0.009)

        # compute weighted average 
        eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)

        print(c(f"\n==================================================").cyan)
        print(c(f"Ratio of BF: D0/D* = {BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR}\n").cyan)
        print(c(f"Extracted weighted average for Dst efficiency = {eff_DstMuNu}").cyan)
        print(c(f"D0 efficiency = {eff_D0MuNu}").cyan)

        # N_D0/NDst = [BF(D0)*eff(D0)] / [BF(D*)*eff(D*)]; factorise into a kappa theory factor (common to all years) and year-specific efficiency ratio
        kappa = (BF_Bc2D0MuNu_3ptSR) / (BF_Bc2DstMuNu_3ptSR) # gaussian-constrained scaling: espress D0 as a function of Dst
        eff_ratio = eff_D0MuNu / eff_DstMuNu
        sig_rel_frac = kappa*eff_ratio # D0/D*
        print(c(f"Efficiency ratio : D0/D*= {eff_ratio}").cyan)

        print(c(f"\nKappa factor from 3ptSR, efficiency-corrected (2016) = {kappa*eff_ratio:.4f}").blue)
        print(c(f"==================================================\n").cyan) 

        sampled_D0 = D0_MC16.sample(frac=1.)[:int(len(Dst_MC16)*sig_rel_frac.n)]
        print(c(f"Fractional composition of the D0-D* mixture: {len(sampled_D0)/len(Dst_MC16)} [D0/D*]").red)
        signal_df = pandas.concat([sampled_D0, Dst_MC16])
        signal_df.name = "incl_sig"
        
        for indf in [signal_df]:
            # only one fitvar
            if n_hists==1:
                if fit_config[fitvars[0]]["is_var_axis"]=="False":
                    wbh = bh.Histogram( 
                    bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 )
                    )
                if fit_config[fitvars[0]]["is_var_axis"]=="True":
                    wbh = bh.Histogram(
                    bh.axis.Variable(fit_config[fitvars[0]]["range"]) #custom binning 
                )        
                wbh.fill( indf[fitvars[0]] )        
                print(c(f"Filling mode {indf.name} with {len(indf)} events").bold)
                print(c(f"Histogram summary: {wbh}"))
                
            # two fitvars
            if n_hists==2:
                _nbins_2  = fit_config[fitvars[1]]["nbins"]
                _minvar_2 = fit_config[fitvars[1]]["range"][0] 
                _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 

                if fit_config[fitvars[0]]["is_var_axis"]=="False":
                    ax_1 = bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
                if fit_config[fitvars[0]]["is_var_axis"]=="True":
                    ax_1 = bh.axis.Variable(fit_config[fitvars[0]]["range"])

                if fit_config[fitvars[1]]["is_var_axis"]=="False":
                    ax_2 = bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
                if fit_config[fitvars[1]]["is_var_axis"]=="True":
                    ax_2 = bh.axis.Variable(fit_config[fitvars[1]]["range"]) 
                
                wbh = bh.Histogram( ax_1, ax_2 )
                samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
                wbh.fill( *samples )
                print(c(f"Filling mode {indf.name}").cyan)
                print(c(f"Histogram summary: {wbh}").blue)
                
            os.system(f"mkdir -p {OUT_MC_PATH}/{MODE}/{YEAR}")
            with open(f"{OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl", "wb") as bh_fout:
                pickle.dump(wbh, bh_fout)
            print(c(f"SUCCESS: boost objects written to file {OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl\n").green)


    if not opts.hardcode:
        for indf in [D0_MC16, Dst_MC16]:
            # only one fitvar
            if n_hists==1:
                if fit_config[fitvars[0]]["is_var_axis"]=="False":
                    wbh = bh.Histogram( 
                    bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 )
                    )
                if fit_config[fitvars[0]]["is_var_axis"]=="True":
                    wbh = bh.Histogram(
                    bh.axis.Variable(fit_config[fitvars[0]]["range"]) #custom binning 
                )        
                wbh.fill( indf[fitvars[0]] )        
                print(c(f"Filling mode {indf.name}").blue.bold)
                print(c(f"Histogram summary: {wbh}").blue)
                
            # two fitvars
            if n_hists==2:
                _nbins_2  = fit_config[fitvars[1]]["nbins"]
                _minvar_2 = fit_config[fitvars[1]]["range"][0] 
                _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 

                if fit_config[fitvars[0]]["is_var_axis"]=="False":
                    ax_1 = bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
                if fit_config[fitvars[0]]["is_var_axis"]=="True":
                    ax_1 = bh.axis.Variable(fit_config[fitvars[0]]["range"])

                if fit_config[fitvars[1]]["is_var_axis"]=="False":
                    ax_2 = bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
                if fit_config[fitvars[1]]["is_var_axis"]=="True":
                    ax_2 = bh.axis.Variable(fit_config[fitvars[1]]["range"]) 
                
                wbh = bh.Histogram( ax_1, ax_2 )
                samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
                wbh.fill( *samples )
                print(c(f"Filling mode {indf.name}").bold.blue)
                print(c(f"Histogram summary: {wbh}").blue)
                
            os.system(f"mkdir -p {OUT_MC_PATH}/{MODE}/{YEAR}")
            with open(f"{OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl", "wb") as bh_fout:
                pickle.dump(wbh, bh_fout)
            print(c(f"SUCCESS: boost objects written to file {OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl\n").green)


    # ===== write out the inclusive b2D0X MC =====
    for indf in [inclb2D0X_MC16]:
        # only one fitvar
        if n_hists==1:
            if fit_config[fitvars[0]]["is_var_axis"]=="False":
                wbh = bh.Histogram( 
                bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 )
                )
            if fit_config[fitvars[0]]["is_var_axis"]=="True":
                wbh = bh.Histogram(
                bh.axis.Variable(fit_config[fitvars[0]]["range"]) #custom binning 
            )        
            wbh.fill( indf[fitvars[0]] )        
            print(c(f"Filling mode {indf.name}").bold)
            
        # two fitvars
        if n_hists==2:
            _nbins_2  = fit_config[fitvars[1]]["nbins"]
            _minvar_2 = fit_config[fitvars[1]]["range"][0] 
            _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 

            if fit_config[fitvars[0]]["is_var_axis"]=="False":
                ax_1 = bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
            if fit_config[fitvars[0]]["is_var_axis"]=="True":
                ax_1 = bh.axis.Variable(fit_config[fitvars[0]]["range"])

            if fit_config[fitvars[1]]["is_var_axis"]=="False":
                ax_2 = bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
            if fit_config[fitvars[1]]["is_var_axis"]=="True":
                ax_2 = bh.axis.Variable(fit_config[fitvars[1]]["range"]) 
            
            wbh = bh.Histogram( ax_1, ax_2 )
            samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
            wbh.fill( *samples )
            print(c(f"Filling mode {indf.name}").cyan)
            print(c(f"Histogram summary: {wbh}").blue)
            
        os.system(f"mkdir -p {OUT_MC_PATH}/{MODE}/{YEAR}")
        with open(f"{OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl", "wb") as bh_fout:
            pickle.dump(wbh, bh_fout)
        print(c(f"SUCCESS: boost objects written to file {OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl\n").green)


# =========================================
#               JpsiMuNu                  #
# =========================================

def generate_doublecharm_template(twobody, quasitwobody):
    # find out which one has the smallest size
    events = np.minimum(len(twobody), len(quasitwobody))
    doublecharm = pandas.concat([twobody[:events], quasitwobody[:events]])
    return doublecharm

# python executables/sweights/bin_split_templates.py --i sig -y 2016 --mode D0MuNu --label DCS --dcs
if MODE=="JpsiMuNu":

    CHARM_M_LOW = 3020; CHARM_M_HIGH=3190;
    charm_had = "Jpsi"
    mc_sel = f"({charm_had}_M>={CHARM_M_LOW} && {charm_had}_M<={CHARM_M_HIGH}) \
                && (B_plus_FIT_LTIME>={min_ltime} && B_plus_FIT_LTIME<={max_ltime}) \
                && ({mass_var_range}>={min_mcorr} && {mass_var_range}<={max_mcorr})",
    print(c(f"SELECTION: {mc_sel}").blue)
    
    modes = ["Bc2JpsiMuNu", "Bc2JpsiDx", "Bc2JpsiDxKx", "Bc2JpsiDsx", "Bc2JpsiTauNu", "Bc2Psi2SMuNu", "Bc2ChiCMuNu", "Bc2ChiC2MuNu"]

    mc_dfs = {}
    print(c("\nLoaded templates:").blue.underline)
    for m in modes:
        mc_dfs[m] = pandas.DataFrame(RDF("DecayTree", f"{MC_PATH}/{m}.root").Filter(mc_sel).AsNumpy())
        mc_dfs[m].name = m
        print(c(f"{mc_dfs[m].name} {YEAR}").cyan)
        print(c(f"entries: {len(mc_dfs[m])}\n").magenta)

    
    # ============================================
    #         build doublecharm template 
    # ============================================
    # adding doublecharm from equal contributions from two-body and quasi-two-body components, following R(jpsi)
    print(c("adding now double-charm from merging two+quasi-two body JpsiDX dataframes").yellow)
    mc_dfs["doublecharm"] = generate_doublecharm_template(twobody=mc_dfs["Bc2JpsiDx"], quasitwobody=mc_dfs["Bc2JpsiDsx"])    
    mc_dfs["doublecharm"].name = "doublecharm"
    print(c(f"Sanity check: double-charm template population: {len(mc_dfs['doublecharm'])}"))
    assert(len(mc_dfs["doublecharm"]) == np.minimum( len(mc_dfs["Bc2JpsiDx"]), len(mc_dfs["Bc2JpsiDsx"]))*2 )

    for template in mc_dfs.values():
        print(c("SANITY CHECK").magenta, f": expect no lower bound on visible mass :", np.min(template["B_plus_M"]))
        print(c("SANITY CHECK").magenta, f": expect a {charm_had}_M bounds to be [{CHARM_M_LOW}, {CHARM_M_HIGH}] :", np.min(template[f"{charm_had}_M"]), np.max(template[f"{charm_had}_M"]))
        print(c("SANITY CHECK").magenta, f": expect a LTIME bounds to be [{min_ltime}, {max_ltime}] :", np.min(template["B_plus_FIT_LTIME"]), np.max(template["B_plus_FIT_LTIME"]))
    

    # ========================================================
    #         build Bc->cc munu unresolved bkg template  
    # ========================================================
    # table of ratios from Rjpsi (table 20)
    rjpsi_raw_y = {}    
    rjpsi_raw_y["psi2S_tojpsimunu"] = ufloat(0.020, 0.040) # raw psi2S yield relative to jpsimunu
    rjpsi_raw_y["chic_tojpsimunu"]  = ufloat(0.111, 0.082) # raw chic  yield relative to jpsimunu [shared between the two components]
    rjpsi_raw_y["doublecharm_tojpsimunu"] = ufloat(0.002, 0.023) # raw doublecharm (two-body and quasi two-body) yield relative to jpsimunu

    #=====================================================================================
    #                      BUILD USING CHIC AS REFERENCE SAMPLE
    # pick as refrence sample the chic template: high stats and largest raw yield
    # => assemble one chic template 
    chicmunu_template = generate_doublecharm_template(twobody=mc_dfs["Bc2ChiCMuNu"], quasitwobody=mc_dfs["Bc2ChiC2MuNu"]) # recycle fn
    
    # compute the ratio relative to chic
    _psi2S_rchic = rjpsi_raw_y["psi2S_tojpsimunu"]/rjpsi_raw_y["chic_tojpsimunu"] 
    _doublecharm_rchic = rjpsi_raw_y["doublecharm_tojpsimunu"]/rjpsi_raw_y["chic_tojpsimunu"] 
    print(c(_psi2S_rchic*len(chicmunu_template)).green)
    print(c(_doublecharm_rchic*len(chicmunu_template)).green)
    
    # assemble Bc2ccX bkg
    mc_dfs["ChiC"] = chicmunu_template
    mc_dfs["ChiC"].name = "ChiC"

    mc_dfs["Bc2ccX"] = pandas.concat([
        chicmunu_template,
        mc_dfs["Bc2Psi2SMuNu"][:int( (_psi2S_rchic*len(chicmunu_template)).n )],
        mc_dfs["doublecharm"][:int( (_doublecharm_rchic*len(chicmunu_template)).n )],
    ])
    mc_dfs["Bc2ccX"].name = "Bc2ccX"
    #=====================================================================================

    # #=====================================================================================
    # #                       BUILD USING DOUBLECHARM + PSI2S ONLY
    # # compute the ratio relative to psi2D
    # _doublecharm_rpsi2s = rjpsi_raw_y["doublecharm_tojpsimunu"]/rjpsi_raw_y["psi2S_tojpsimunu"] 
    # mc_dfs["Bc2ccX"] = pandas.concat([
    #     mc_dfs["Bc2Psi2SMuNu"],
    #     mc_dfs["doublecharm"][:int( (_doublecharm_rpsi2s*len(mc_dfs["Bc2Psi2SMuNu"])).n )],
    # ])
    # mc_dfs["Bc2ccX"].name = "Bc2ccX"
    # #=====================================================================================
    
    for k, v in mc_dfs.items():
        print(k,len(v))
    
    # ============================================
    #                PREPARE OUTPUT 
    # ============================================
    # write boost_histograms with weighted storage
    n_hists = len(fit_config.keys()) # how many fit variables?
    fitvars = list(fit_config.keys()) # which fit variables?
    print(c(f"\nFit vars : {fitvars}").blue)

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    for indf in mc_dfs.values():
        # only one fitvar
        if n_hists==1:
            if fit_config[fitvars[0]]["is_var_axis"]=="False":
                wbh = bh.Histogram( 
                bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 )
                )
            if fit_config[fitvars[0]]["is_var_axis"]=="True":
                wbh = bh.Histogram(
                bh.axis.Variable(fit_config[fitvars[0]]["range"]) #custom binning 
            )        
            wbh.fill( indf[fitvars[0]] )        
            print(c(f"Filling mode {indf.name}").cyan.underline)
            print(c(f"Done.").blue.bold)
            #print(c(f"Histogram summary: \n{wbh}").blue)
            
        # two fitvars
        if n_hists==2:
            _nbins_2  = fit_config[fitvars[1]]["nbins"]
            _minvar_2 = fit_config[fitvars[1]]["range"][0] 
            _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 

            if fit_config[fitvars[0]]["is_var_axis"]=="False":
                ax_1 = bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
            if fit_config[fitvars[0]]["is_var_axis"]=="True":
                ax_1 = bh.axis.Variable(fit_config[fitvars[0]]["range"])

            if fit_config[fitvars[1]]["is_var_axis"]=="False":
                ax_2 = bh.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
            if fit_config[fitvars[1]]["is_var_axis"]=="True":
                ax_2 = bh.axis.Variable(fit_config[fitvars[1]]["range"]) 
            
            wbh = bh.Histogram( ax_1, ax_2 )
            samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
            wbh.fill( *samples )
            print(colored(f"Filling mode {indf.name}", "cyan", attrs=["underline"]))
            print(colored(f"Histogram summary: {wbh}", "blue"))

        os.system(f"mkdir -p {OUT_MC_PATH}/{MODE}/{YEAR}")
        with open(f"{OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl", "wb") as bh_fout:
            pickle.dump(wbh, bh_fout)
        print(c(f"SUCCESS: boost objects written to file {OUT_MC_PATH}/{MODE}/{YEAR}/{indf.name}.pkl\n").green)

