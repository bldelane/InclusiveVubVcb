#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble":r"\usepackage{amsmath}",
    })
from termcolor2 import c

from argparse import ArgumentParser
import uproot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
#mpl.use('agg')

import json
from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL, ExtendedUnbinnedNLL
from numba_stats import norm_pdf, norm_cdf, expon_pdf, expon_cdf
from scipy.stats import crystalball, expon
from SWeighter import SWeight

parser = ArgumentParser()
parser.add_argument('-i','--input', required=True, help='Input filename (.root) file', choices=["sig", "comb", "misid_hadron_enriched", "misid"]) # misid should be used only if hadron-enriched not ready yet as it comes from DLLmu cut reversal only
parser.add_argument('-o','--output',default=None , help='Output file name (default is input with "_sw" suffix)')
parser.add_argument('-y','--year',  required=True, help='Year of data-taking/MC', choices=["2011", "2012", "2015", "2016", "2017", "2018"])
parser.add_argument('-m','--mode',  required=True, help='Decay mode, choose from [D0MuNu|JpsiMuNu]', choices=["D0MuNu", "JpsiMuNu"])
parser.add_argument('-l','--label', default=None )
parser.add_argument('-d','--dcs',   action='store_true') # default False
parser.add_argument('-c','--cf',    action='store_true') # default False
parser.add_argument('-t','--test',  action='store_true', help="Test mode: will store the plots and *.pkl in Bc2D0MuNuX/Fitter/sweights") # default False
parser.add_argument('-p','--path',  default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/executables/sweights/', help="Path for fit pars")
parser.add_argument('-s','--save',  default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights', help="Path for pkl and plots")
parser.add_argument('-P','--pull',  default=False, action="store_true", help='Plot the pulls')
parser.add_argument('-C','--config', default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json', help='Path to fit config')

opts = parser.parse_args()

# =========================================================
# bookkeping
YEAR = opts.year
MODE = opts.mode
OUTPUT_NAMESPACE = opts.output
ROOT_PATH = f"/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/DATA/fit/{MODE}/{YEAR}/Bc2{MODE}X_"
INPUT_NAMESPACE = opts.input
CONFIG = opts.config

# bin in corrected mass
with open( CONFIG ) as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").bold)
print(fit_config)
MCORR_LOW = fit_config["B_plus_MCORR"]["range"][0]
MCORR_HIGH = fit_config["B_plus_MCORR"]["range"][-1]
MCORR_BINS = fit_config["B_plus_MCORR"]["nbins"]
assert(fit_config["B_plus_MCORR"]["is_var_axis"]=="False")

# bounds on charmed hadron
if MODE == "D0MuNu":
  CHARM_M_LOW = 1810; CHARM_M_HIGH=1920; # /v4/ data tuples have D0 mass - 60 MeV
  charm_hadron = "D0"
if MODE == "JpsiMuNu":
  CHARM_M_LOW = 3020; CHARM_M_HIGH=3190; # from previous binned fit implementation
  charm_hadron = "Jpsi"
print(c(f"\nFit /v4/ data: CHARM_M range restricted to [{CHARM_M_LOW} MeV, {CHARM_M_HIGH} MeV]").bold)
mrange = (CHARM_M_LOW, CHARM_M_HIGH)

if opts.test is not True :
  pkl_PATH=f"{opts.save}/{MODE}/{YEAR}/pkl"
  plots_PATH=f"{opts.save}/{MODE}/{YEAR}/plots"
  os.system(f"mkdir -p {pkl_PATH}")
  os.system(f"mkdir -p {plots_PATH}")
if opts.test is True :
  print(c("\nWARNING: running in TEST mode").yellow)
  pkl_PATH=f"/usera/delaney/private/Bc2D0MuNuX/Fitter/sweights/pkl"
  plots_PATH=f"/usera/delaney/private/Bc2D0MuNuX/Fitter/sweights/plots"
  os.system(f"mkdir -p {pkl_PATH}")
  os.system(f"mkdir -p {plots_PATH}")

if opts.output is None: opts.output = opts.input.replace('.root','_sw.pkl')

IN_BRANCHES = [f"{charm_hadron}_M", "B_plus_MCORR", "B_plus_FIT_LTIME"] # save only D0_M and fit vars
PLOT_BINS = 50 # how many datapoints in plots
# =========================================================


def read_pars(fil):
  res = {}
  f = open(fil)
  ls = f.readlines()[1:]
  for l in ls:
    res[ l.split()[0] ] = float(l.split()[1])
  f.close()
  return res

def cdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb):

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.cdf(x)/cb1n + (1-f)*(1-cb2.cdf(invx)/cb2n)

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.cdf(x)/expn

  ks = 0
  ps = 0
  if opts.dcs:
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.cdf(x)/kscbn

    # ps swap
    #pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-ps_pars['mu']+mrange[0],ps_pars['sg'])
    #pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],ps_pars['mu'],ps_pars['sg'])
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*(1-pscb1.cdf(invx)/pscb1n) + (1-ps_pars['f'])*pscb2.cdf(x)/pscb2n

  return ns * sig + nb * bkg + nks * ks + nps * ps

def pdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb, comps=None):

  if comps is None:
    comps = ['sig','bkg','ks','ps']

  tot = 0

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.pdf(x)/cb1n + (1-f)*(cb2.pdf(invx)/cb2n)
  if 'sig' in comps: tot += ns*sig

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.pdf(x)/expn
  if 'bkg' in comps: tot += nb*bkg

  ks = 0
  ps = 0
  if opts.dcs: # allow D0>KK/pipi shoulders only if dcs mode activated
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.pdf(x)/kscbn
    if 'ks' in comps: tot += nks*ks

    # ps swap
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*pscb1.pdf(invx)/pscb1n + (1-ps_pars['f'])*pscb2.pdf(x)/pscb2n
    ps = pscb2.pdf(x)/pscb2n
    if 'ps' in comps: tot += nps*ps

  return tot


# read in a ROOT file; by default will read all branches
print(c(f"Reading input file into pandas dataFrame {ROOT_PATH}{INPUT_NAMESPACE}.root"))
tree = uproot.open(f"{ROOT_PATH}{INPUT_NAMESPACE}.root:DecayTree")

# if signal, place a tighter cut on B_plus_visM
if MODE=="D0MuNu":
  mass_cuts = f"( ({charm_hadron}_M>={CHARM_M_LOW}) & ({charm_hadron}_M<={CHARM_M_HIGH}) ) & (B_plus_M>3500) & ( (B_plus_MCORR>={MCORR_LOW}) & (B_plus_MCORR<={MCORR_HIGH}) )"
  print(c(f"\nSign combination sanity check:").underline, f"\nDCS : {opts.dcs} \nCF : {opts.cf}")
if MODE=="JpsiMuNu":
  mass_cuts = f"( ({charm_hadron}_M>={CHARM_M_LOW}) & ({charm_hadron}_M<={CHARM_M_HIGH}) ) & ( (B_plus_MCORR>={MCORR_LOW}) & (B_plus_MCORR<={MCORR_HIGH}) )"

print(c(f"\nFitting charm hadron mass in {MODE} channel...").magenta)
print(c(f"Mass cuts applied: {mass_cuts}").magenta)


if opts.dcs is True: # only relevant for signal channel

  if INPUT_NAMESPACE=="misid_hadron_enriched":
    df = tree.arrays(IN_BRANCHES+["misid_w", "K_minus_ID", "Mu_plus_ID", "B_plus_M", "B_plus_MCORRERR"], cut=f"(K_minus_ID*Mu_plus_ID>0) & {mass_cuts}", library='pd') # added MCORRERR for debugging
    assert((df["K_minus_ID"]*df["Mu_plus_ID"]).all()>0)

  else:
    df = tree.arrays(IN_BRANCHES+["K_minus_ID", "Mu_plus_ID", "B_plus_M"], cut=f"(K_minus_ID*Mu_plus_ID>0) & {mass_cuts}", library='pd') # can afford to save extra branches for debugging purposes
    assert((df["K_minus_ID"]*df["Mu_plus_ID"]).all()>0)
  print(c(f"Ntuple read in with DCS sign requirement with mass cuts: {mass_cuts}").blue)

if opts.cf is True: # only relevant for signal channel
  df = tree.arrays(IN_BRANCHES+["K_minus_ID", "Mu_plus_ID", "B_plus_M"], cut=f"(K_minus_ID*Mu_plus_ID<0) & {mass_cuts}", library='pd')
  print(c(f"Ntuple read in with CF sign requirement with mass cuts: {mass_cuts}").magenta)
  print(c("max of kaon ID * muon ID = ").bold, np.max((df["K_minus_ID"]*df["Mu_plus_ID"])))
  print(c("min of kaon ID * muon ID = ").bold, np.min((df["K_minus_ID"]*df["Mu_plus_ID"])))
  assert(np.all(df["K_minus_ID"]*df["Mu_plus_ID"]<0.0))

if opts.dcs is not True and opts.cf is not True:

  if INPUT_NAMESPACE=="comb":
    print(c("sWeighting combinatorial sample").yellow.bold)
    df = tree.arrays(IN_BRANCHES+["GBR_w", "B_plus_M"], cut=f"{mass_cuts}", library='pd')
  if INPUT_NAMESPACE=="misid_hadron_enriched":
    print(c("sWeighting hadron-enriched sample").yellow.bold)
    df = tree.arrays(IN_BRANCHES+["misid_w", "B_plus_M"], cut=f"{mass_cuts}", library='pd')
  if INPUT_NAMESPACE=="sig" or INPUT_NAMESPACE=="misid":
    print(c("sWeighting selected sample").yellow.bold)
    df = tree.arrays(IN_BRANCHES+["B_plus_M"], cut=f"{mass_cuts}", library='pd')

  print(c(f"Ntuple read in without any kaon-muon sign requirement with mass cuts: {mass_cuts}").green)

mcorr_edges = np.linspace(start=MCORR_LOW, stop=MCORR_HIGH, endpoint=True,  num=MCORR_BINS+1) # nbins, not edges

# set placeholders before assignment  
df['sw'] = np.nan
df['bw'] = np.nan

for i in np.arange(len(mcorr_edges)-1):
  m_lo = mcorr_edges[i]
  m_hi = mcorr_edges[i+1]
  if m_hi != MCORR_HIGH:
    mcorr_bin_selection = f"B_plus_MCORR>={m_lo} and B_plus_MCORR<{m_hi}"
    mcorr_bin_label = f"[{m_lo}, {m_hi})"
  if m_hi == MCORR_HIGH:
    mcorr_bin_selection = f"B_plus_MCORR>={m_lo} and B_plus_MCORR<={m_hi}"
    mcorr_bin_label = f"[{m_lo}, {m_hi}]"
  
  data = df.query(mcorr_bin_selection)[f"{charm_hadron}_M"].to_numpy()
  print(c(f"\nCorrected mass bin: {mcorr_bin_selection}").yellow)
  print(c(f"Number of events read in: {len(data)}").yellow)
  print(c(f"Branches: {df.keys()}").yellow)

  ps_pars = read_pars(f'{opts.path}/pspars.log')
  ks_pars = read_pars(f'{opts.path}/kspars.log')
  mc_pars = read_pars(f"{opts.path}/mcsigpars_{MODE}.log")

  #data = data[ (data>=mrange[0]) & (data<=mrange[1]) ]
  mbins = 400

  nh, xe = np.histogram(data, bins=mbins, range=mrange)
  cx = 0.5*(xe[1:]+xe[:-1])


  mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf  ),
              ns = 0.2*len(data),
              nb = 0.8*len(data),
              nks = 0.01*len(data),
              nps = 0.01*len(data),
              mu = 1870 if opts.mode=='D0MuNu' else 3100 ,
              sg = 10,
              f  = 0.5,
              a1 = mc_pars['a1'],
              a2 = mc_pars['a2'],
              n1 = mc_pars['n1'],
              n2 = mc_pars['n2'],
              ksmu = ks_pars['mu'],
              kssg = ps_pars['sg'],
              psmu = ps_pars['mu'],
              pssg = ps_pars['sg'],
              lb = 400 )

  mi.limits['ns'] = (1e-10, len(data))
  mi.limits['nb'] = (1e-10, len(data))
  mi.limits['nks'] = (1e-10, 0.2*len(data))
  mi.fixed['lb'] = False
  mi.limits['nps'] = (1e-10, 0.2*len(data))
  #mi.values['nps'] = 0.05*len(data)
  #mi.fixed['nps'] = True
  mi.limits['mu'] = (1830,1890) if opts.mode=='D0MuNu' else (2900,3300)
  mi.limits['sg'] = (0,20)
  mi.limits['lb'] = (-100,1000)
  #mi.limits['n1'] = (2.5,3.5)
  #mi.limits['n2'] = (2.5,3.5)
  mi.fixed['n1'] = True
  mi.fixed['n2'] = True
  mi.limits['a1'] = (0,10)
  mi.limits['a2'] = (0,10)
  mi.fixed['a1'] = True
  mi.fixed['a2'] = True
  mi.limits['f']  = (0,1)
  mi.limits['ksmu'] = (1700,1840)
  mi.limits['psmu'] = (1900,1960)
  mi.limits['kssg'] = (0,20)
  mi.limits['pssg'] = (0,20)
  mi.fixed['psmu'] = True
  mi.fixed['pssg'] = True
  mi.fixed['ksmu'] = True
  mi.fixed['kssg'] = True
  if not opts.dcs:
    mi.values['nks'] = 0
    mi.values['nps'] = 0
    mi.fixed['nks'] = True
    mi.fixed['nps'] = True
    mi.fixed['lb'] = False

  print(c('Running fit').yellow)
  mi.migrad()
  mi.hesse()

  # sanity checks
  assert(mi.fmin.is_valid)
  assert(mi.fmin.has_covariance)
  assert(mi.fmin.has_posdef_covar)
  assert(mi.fmin.has_accurate_covar)
  print(c("Success: fit converged and sanity checks passed").green)
  print(mi)

  # plot the fit
  if opts.pull:
    fig, ax = plt.subplots(2,1,figsize=(9,8), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
    ax0 = ax[0]
  else:
    fig, ax0 = plt.subplots(1,1,figsize=(8,6))

  ax0.plot([], [], "", label=r"\textbf{LHCb Unofficial}", color="white")

  pnh, pxe = np.histogram(data, bins=PLOT_BINS, range=mrange)
  pcx = 0.5*(pxe[1:]+pxe[:-1])

  # plot data histogram with 50 bins
  if opts.label is not None: label = f"{opts.label} {YEAR} Data"
  if opts.label is None: label = f"LHCb {YEAR} Data"
  ax0.errorbar(
    x=pcx,
    y=pnh,
    xerr=0.5 * (pxe[1:]-pxe[:-1]),
    yerr=pnh**0.5,
    fmt='.',
    color="black",
    markersize=5,
    elinewidth=1.1,
    capsize=1,
    capthick=1,
    label=label )

  # plot fitted pdf every 400 points
  x = np.linspace(*mrange,400)
  N = (mrange[1]-mrange[0])/PLOT_BINS

  bkg = N*pdf(x,*mi.values,comps=['bkg'])
  ax0.fill_between(x, 0, bkg, alpha=0.7, facecolor='darkorange', label='Combinatorial')
  if opts.dcs:
    ks  = N*pdf(x,*mi.values,comps=['bkg','ks'])
    ax0.fill_between(x, bkg, ks, alpha=0.7, facecolor='forestgreen', label=r'$D^0 \rightarrow K^+ K^-$')
    ps  = N*pdf(x,*mi.values,comps=['bkg','ks','ps'])
    ax0.fill_between(x, ks, ps, alpha=0.7, facecolor='purple', label=r'$D^0 \rightarrow \pi^+ \pi^-$')
  ax0.plot(x, N*pdf(x,*mi.values, comps=["sig"]), color='firebrick', lw=2, ls="--", label=r"Signal" )
  ax0.plot(x, N*pdf(x,*mi.values), 'tab:blue', ls="-", lw=2, label='Total PDF' )
  ax0.set_ylim(bottom=0)

  # # plot pull
  if opts.pull:
    # make the pull
    pull = (pnh-N*pdf(pcx,*mi.values))/(pnh**0.5)

    ax[1].fill_between(x, -3, 3, alpha=0.3, facecolor='0.75')
    ax[1].plot(x, np.zeros_like(x), c='tab:blue', ls='--', lw=1)
    ax[1].errorbar(
      x=pcx,
      y=pull,
      xerr=0.5 * (pxe[1:]-pxe[:-1]),
      yerr=np.ones_like(pcx),
      fmt='.',
      color="black",
      markersize=5,
      elinewidth=1.1,
      capsize=1,
      capthick=1
    )
    ax[1].set_ylabel(r'Pulls [$\sigma$]', loc="center")
    ylim = ax[1].get_ylim()
    y = max(abs(ylim[0]),abs(ylim[1]))
    ax[1].set_ylim(-5,5)

  ax0.set_ylabel(rf"Events / {{{(mrange[1]-mrange[0])/PLOT_BINS}}} MeV$/c^2$")
  ax0.legend(fontsize=15)

  fig.tight_layout()

  if MODE=="D0MuNu":
    plt.xlabel(r"$m(K \pi)$ [MeV$/c^2$]", loc="center")
  if MODE=="JpsiMuNu":
    plt.xlabel(r"$m(\mu^+\mu^-)$ [MeV$/c^2$]", loc="center")

  fig.savefig(f"{plots_PATH}/{opts.output}_{mcorr_bin_label}.pdf")
  fig.savefig(f"{plots_PATH}/{opts.output}_{mcorr_bin_label}.png")

  # now compute the sweights
  spdf = lambda x: pdf(x,*mi.values,comps=['sig'])
  bpdf = lambda x: pdf(x,*mi.values,comps=['bkg','ps','ks'])

  # print computing sweights
  sweighter = SWeight(data, [spdf,bpdf], [mi.values['ns'],mi.values['nb']+mi.values['nks']+mi.values['nps']], (mrange,), compnames=('sig','bkg'), checks=True)

  sws = sweighter.getWeight(0,data)
  bws = sweighter.getWeight(1,data)

  print(len(sws), len(data))

  df.loc[df.eval(mcorr_bin_selection), "sw"] = sws
  df.loc[df.eval(mcorr_bin_selection), "bw"] = bws

  # plot sWeights check
  fig, ax = plt.subplots()
  swp = sweighter.getWeight(0,x)
  bwp = sweighter.getWeight(1,x)
  ax.plot(x, swp, 'b--', label='Signal')
  ax.plot(x, bwp, 'r:' , label='Background')
  ax.plot(x, swp+bwp, 'k-', label='Sum')
  if opts.label is not None: ax.set_title(opts.label)
  ax.set_xlabel('$m(D^0)$ [MeV]')
  ax.set_ylabel('Weight')
  ax.legend()
  fig.tight_layout()

  # save figs
  fig.savefig(f"{plots_PATH}/{opts.output}_{MODE}_sweights_{mcorr_bin_label}.pdf")
  fig.savefig(f"{plots_PATH}/{opts.output}_{MODE}_sweights_{mcorr_bin_label}.png")

# write to pkl file
df.to_pickle(f"{pkl_PATH}/{opts.output}.pkl")
print(c(f"Output pkl file written with stored sWeights: {pkl_PATH}/{opts.output}_{MODE}.pkl").green)

# ## now the output
# #print('Writing weights out to file')
# #import ROOT as r
# #import array as array
# #tf = r.TFile(opts.input)
# #tr = tf.Get('DecayTree')
# #outf = r.TFile(opts.output,'recreate')
# #outr = tr.CloneTree()
# #sw = array.array('f',[0])
# #sbr = outr.Branch( 'sw', sw, 'sw/F' )
# #for ev in range(tr.GetEntries()):
#   #if (ev%5000==0): print('\t', ev, '/', tr.GetEntries())
#   #tr.GetEntry(ev)
#   #sw[0] = sweighter.getWeight(0, outr.D0_M)
#   #sbr.Fill()

# #outr.Write()
# #tf.Close()
# #outf.Close()
