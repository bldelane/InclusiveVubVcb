# Instructions for running the SWeights

This is some custom code which first peforms a binned fit to either CF or DCS data in the D0 mass and then uses the fitted PDFs to extract sWeights for each sample.

The main script to run the sweighting is `minfit.py` which requires an
  -  `--input` file to run on (the root tuple of the relevant sample) 
You can optionally pass:
  - an `--output` name tag e.g "2018_MisID_DCS" which will append this name to output files and plots
  - a `--label` for the plots e.g. "2018 MisID DCS"
  - the `-D` option which will use the DCS data
When fitting the CF data then two components are fitted for (a double crystall ball signal and exponential background). When fitting the DCS data then two additional components are added for kaon and pion mis ID swaps (both crystal balls). The parameters for these are loaded from the `kspars.log` and `pspars.log` files. If you need to reproduce these files you can rerun `mcminfit.py` which accepts one argument, the MC file to fit.

An example for running the main script on the signal DCS would be:

`python -i Bc2D0MuNuX_sig.root -o Bc2D0MuNuX_sig_dcs_sws -l "2018 DCS" -D`

The output wil be saved as a `pandas` DataFrame in a `.pkl` file.

And it will produce some plots like these:

![](plots/sw_sig_dcs_2018_dmass_fit.png)
![](plots/sw_sig_dcs_2018_sweights.png)

