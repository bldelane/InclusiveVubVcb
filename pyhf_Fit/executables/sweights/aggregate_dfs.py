""" Aggregate bins with weights extracted from fits to 
charm mass distribution and plot against mcorr

__author__: blaise.delaney
__email__: blaise.delaney@cern.ch
"""
# =======================
#        IMPORTS
# =======================
#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('pdf')
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble":r"\usepackage{amsmath}",
    })
from termcolor2 import c

# manipulate tuples
from argparse import ArgumentParser
import uproot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import os
import json

parser = ArgumentParser()
parser.add_argument('-i','--input', nargs='+', help='<Required> Set flag', required=True)
parser.add_argument('-t','--templates', nargs='+', help='<Required> Set flag', required=True)
parser.add_argument('-x','--config',default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json', help='Path to fit config')
parser.add_argument('-s','--sweights',   default="False",  required=False , help='run sFit? [default: False]')
opts = parser.parse_args()


# ========================================
# 	     Read in fit config
# ========================================
# bin in corrected mass
with open( opts.config ) as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").bold.yellow)
print(c(fit_config).yellow)
fitvar = list(fit_config.keys())[0]
FITVAR_BINS = fit_config[fitvar]["nbins"]
assert(fit_config[fitvar]["is_var_axis"]=="False")


# ========================================
# 	Extract template namesplace
# ========================================
# read in token file and extract namespace
_path = os.path.split(os.path.splitext(opts.input[-1])[0])
_namespace = os.path.split(_path[0])[0]
_namespace, _ = os.path.split(_namespace)
_namespace, year = os.path.split(_namespace)
_namespace, mode = os.path.split(_namespace)
templates = opts.templates

# in case on normalisation: CF,DCS -> Sig
if mode=="JpsiMuNu": 
    templates = [x for x in templates if x not in ["CF", "DCS"]]
    templates.append("Sig")


for t in templates:
    
    print(c("\n======================================================================").blue)
    print(c(f"Will now proceed to aggregate templates with sFit mode: {opts.sweights}").cyan)
    print(c("======================================================================\n").blue)

    print(c(f"Detecting bins of template: {t}").blue)
    t_bins = [x for x in opts.input if t in x]
    # we should have FITVAR_BINS per template
    print(c(len(t_bins)).magenta)
    assert(len(t_bins)==FITVAR_BINS)

    # ========================================
    #    Aggregate input & sanity checks
    # ========================================
    df = pd.concat([pd.read_pickle(fp) for fp in t_bins], ignore_index=True)
    print(c(f"Post aggregation, read in {len(df)} events").blue)
    print(c("\nSanity check: each bin has one instance of sw:").magenta)
    print(df.groupby(pd.cut(df[fitvar], FITVAR_BINS))["sw"].value_counts())

    # expect only 1 value count in sw per bin; check for this
    if opts.sweights=="False":
        assert(len(df.groupby(pd.cut(df[fitvar], FITVAR_BINS))["sw"].value_counts())==FITVAR_BINS)
        print(c("Charm-fit-only-specific sanity check passed").green.bold)
    # ========================================
    # 		Write to pkl
    # ========================================
    os.system(f"mkdir -p /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/{mode}/{year}")
    df.to_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/{mode}/{year}/{t}.pkl")
    print(c(f"Template {t} written to file in /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/{mode}/{year}/{t}.pkl").green)