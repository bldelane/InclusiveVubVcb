""" Executable to assign to all events in a given bin a weight = Nsig/Ntot
N.B. assumes all selections (eg DCS and visM cuts) are in place; these
should have been checked at the previous stage of the pipeline in the generating 
the pkl'd dfs in bins of mcorr (& ltime, eventually)

__author__: blaise.delaney
__email__: blaise.delaney@cern.ch
"""

# =======================
#        IMPORTS
# =======================
#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('pdf')
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble":r"\usepackage{amsmath}",
    })
from termcolor2 import c

# manipulate tuples
from argparse import ArgumentParser
import uproot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import os
import json

# sWeights/fits 
from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL, ExtendedUnbinnedNLL
from numba_stats import norm_pdf, norm_cdf, expon_pdf, expon_cdf
from scipy.stats import crystalball, expon

# sweights
from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL, ExtendedUnbinnedNLL
from numba_stats import norm_pdf, norm_cdf, expon_pdf, expon_cdf
from scipy.stats import crystalball, expon
from SWeighter import SWeight

parser = ArgumentParser()
parser.add_argument('-i','--input', required=True, help='Input filename (.root) file') # any of the bins written to file 
parser.add_argument('-p','--path',  default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/executables/sweights/', help="Path for fit pars")
parser.add_argument('-s','--sweights',   default="False",  required=False , help='run sFit? [default: False]')
opts = parser.parse_args()

print(c(f"sWeights? {opts.sweights}").magenta)

def read_pars(fil):
  res = {}
  f = open(fil)
  ls = f.readlines()[1:]
  for l in ls:
    res[ l.split()[0] ] = float(l.split()[1])
  f.close()
  return res

def cdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb):

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.cdf(x)/cb1n + (1-f)*(1-cb2.cdf(invx)/cb2n)

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.cdf(x)/expn

  ks = 0
  ps = 0
  if isDCS==True:
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.cdf(x)/kscbn

    # ps swap
    #pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-ps_pars['mu']+mrange[0],ps_pars['sg'])
    #pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],ps_pars['mu'],ps_pars['sg'])
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*(1-pscb1.cdf(invx)/pscb1n) + (1-ps_pars['f'])*pscb2.cdf(x)/pscb2n

  return ns * sig + nb * bkg + nks * ks + nps * ps

def pdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb, comps=None):

  if comps is None:
    comps = ['sig','bkg','ks','ps']

  tot = 0

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.pdf(x)/cb1n + (1-f)*(cb2.pdf(invx)/cb2n)
  if 'sig' in comps: tot += ns*sig

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.pdf(x)/expn
  if 'bkg' in comps: tot += nb*bkg

  ks = 0
  ps = 0
  if isDCS==True: # allow D0>KK/pipi shoulders only if dcs mode activated
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.pdf(x)/kscbn
    if 'ks' in comps: tot += nks*ks

    # ps swap
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*pscb1.pdf(invx)/pscb1n + (1-ps_pars['f'])*pscb2.pdf(x)/pscb2n
    ps = pscb2.pdf(x)/pscb2n
    if 'ps' in comps: tot += nps*ps

  return tot

# ====================================================
#        Read in and retrieve info of interest
# ====================================================
input_pkl = opts.input
df = pd.read_pickle(input_pkl)
if "D0_M" in df.keys(): charm_hadron = "D0"
if "Jpsi_M" in df.keys(): charm_hadron = "Jpsi"
print(c(f"successfully read in a file from the {charm_hadron}MuNu channel").blue)


# ====================================================
#        manipulate the path 
# ====================================================
# generate new paths where to store updated dfs and fit plots
pkl_path_namespace, f_namespace = os.path.split((os.path.splitext(input_pkl)[0]).replace("charm_M_fitted_templates", "charm_M_w") )
plot_path_namespace = pkl_path_namespace.replace("pkl", "plots")
template = os.path.split(os.path.split(pkl_path_namespace)[0])[-1]
fixed_pars_path = (os.path.split(os.path.splitext(input_pkl)[0])[0]).replace("pkl", "fit_pars")
print(fixed_pars_path)
YEAR = os.path.split(os.path.split(os.path.split(pkl_path_namespace)[0])[0])[-1]
os.system(f"mkdir -p {pkl_path_namespace}")
os.system(f"mkdir -p {plot_path_namespace}")

print(c(f"Template: {template}").cyan)

# assign a boolean to book swap shoulders
isDCS = False # default
if charm_hadron=="D0":
  if template=="DCS" or template=="MisID":
    isDCS = True
print(c(f"Fit activated in DCS mode? {isDCS}").magenta)


# ====================================================
#         read in fixed CB pars and slope
# ====================================================
fixed_pars = read_pars(f"{fixed_pars_path}/fixed_pars.log")


# ====================================================
#        proceed to fit 
# ====================================================
MODE = f"{charm_hadron}MuNu"
data = df[f"{charm_hadron}_M"].to_numpy()
mrange = (df[f"{charm_hadron}_M"].min(), df[f"{charm_hadron}_M"].max())
print(c("\n========================================================================").green)
print(c(f"Will proceed to fit bin of MCORR: [{df.B_plus_MCORR.min()}, {df.B_plus_MCORR.max()}] MeV").green.bold)
print(c("========================================================================\n").green)
PLOT_BINS = 50 # how many datapoints in plot

Ntot = len(data)
print(c(f"Number of events read in: {Ntot}\n"))

ps_pars = read_pars(f'{opts.path}/pspars.log')
ks_pars = read_pars(f'{opts.path}/kspars.log')
mc_pars = read_pars(f"{opts.path}/mcsigpars_{MODE}.log")

mbins = 400

nh, xe = np.histogram(data, bins=mbins, range=mrange)
cx = 0.5*(xe[1:]+xe[:-1])

mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf  ),
            ns = 0.2*len(data),
            nb = 0.8*len(data),
            nks = 0.01*len(data),
            nps = 0.01*len(data),
            #mu = 1870 if MODE=='D0MuNu' else 3100 ,
            mu = fixed_pars["mu"],
            sg = fixed_pars["sg"],
            f  = 0.5,
            a1 = fixed_pars['a1'],
            a2 = fixed_pars['a2'],
            n1 = fixed_pars['n1'],
            n2 = fixed_pars['n2'],
            ksmu = ks_pars['mu'],
            kssg = ps_pars['sg'],
            psmu = ps_pars['mu'],
            pssg = ps_pars['sg'],
            lb = fixed_pars["lb"] )

mi.limits['ns'] = (1e-10, len(data))
mi.limits['nb'] = (0.0, len(data))
mi.limits['nks'] = (1e-10, 0.2*len(data))
mi.limits['nps'] = (1e-10, 0.2*len(data))
#mi.values['nps'] = 0.05*len(data)
#mi.fixed['nps'] = True
mi.limits['mu'] = (1830,1890) if charm_hadron=='D0' else (2900,3300)
mi.fixed['mu'] = False
mi.limits['sg'] = (0,20)
mi.fixed['sg'] = True
mi.limits['lb'] = (0,2000)
mi.fixed['lb'] = False
mi.limits['n1'] = (2.5,4.5)
mi.limits['n2'] = (2.5,4.5)
mi.fixed['n1'] = True # True for signal channel
mi.fixed['n2'] = True # True for signal channel
mi.limits['a1'] = (0,10)
mi.limits['a2'] = (0,10)
mi.fixed['a1'] = True
mi.fixed['a2'] = True
mi.limits['f']  = (0,1)
mi.limits['ksmu'] = (1700,1840)
mi.limits['psmu'] = (1900,1960)
mi.limits['kssg'] = (0,20)
mi.limits['pssg'] = (0,20)
mi.fixed['psmu'] = True
mi.fixed['pssg'] = True
mi.fixed['ksmu'] = True
mi.fixed['kssg'] = True
if isDCS==False:
  print(c("Nulled DCS fit conditions").red)
  mi.values['nks'] = 0
  mi.values['nps'] = 0
  mi.fixed['nks'] = True
  mi.fixed['nps'] = True

print(c('Running fit').yellow)
mi.migrad()
mi.hesse()


print(mi)
print(c("Success: fit converged and sanity checks passed").green)

print(c("\n=================================================================").blue)
print(c(f"Fitted signal charm yield: {mi.values['ns']} +/- {mi.errors['ns']}").blue.bold)
print(c("=================================================================\n").blue)

# plot the fit
fig, ax = plt.subplots(2,1,figsize=(9,8), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
ax0 = ax[0]

ax0.plot([], [], "", label=r"\textbf{LHCb Unofficial}", color="white")

pnh, pxe = np.histogram(data, bins=PLOT_BINS, range=mrange)
pcx = 0.5*(pxe[1:]+pxe[:-1])

# plot data histogram with 50 bins
label = f"{template} {YEAR} Data"
ax0.errorbar(
  x=pcx,
  y=pnh,
  xerr=0.5 * (pxe[1:]-pxe[:-1]),
  yerr=pnh**0.5,
  fmt='.',
  color="black",
  markersize=5,
  elinewidth=1.1,
  capsize=1,
  capthick=1,
  label=label )

# plot fitted pdf every 400 points
x = np.linspace(*mrange,400)
N = (mrange[1]-mrange[0])/PLOT_BINS

bkg = N*pdf(x,*mi.values,comps=['bkg'])
ax0.fill_between(x, 0, bkg, alpha=0.7, facecolor='darkorange', label='Combinatorial')
if isDCS==True:
  ks  = N*pdf(x,*mi.values,comps=['bkg','ks'])
  ax0.fill_between(x, bkg, ks, alpha=0.7, facecolor='forestgreen', label=r'$D^0 \rightarrow K^+ K^-$')
  ps  = N*pdf(x,*mi.values,comps=['bkg','ks','ps'])
  ax0.fill_between(x, ks, ps, alpha=0.7, facecolor='purple', label=r'$D^0 \rightarrow \pi^+ \pi^-$')
ax0.plot(x, N*pdf(x,*mi.values, comps=["sig"]), color='firebrick', lw=2, ls="--", label=r"Signal" )
ax0.plot(x, N*pdf(x,*mi.values), 'tab:blue', ls="-", lw=2, label='Total PDF' )
ax0.set_ylim(bottom=0)

pull = (pnh-N*pdf(pcx,*mi.values))/(pnh**0.5)

ax[1].fill_between(x, -3, 3, alpha=0.1, facecolor='forestgreen')
ax[1].plot(x, np.zeros_like(x), c='forestgreen', ls='--', lw=1)
ax[1].errorbar(
  x=pcx,
  y=pull,
  xerr=0.5 * (pxe[1:]-pxe[:-1]),
  yerr=np.ones_like(pcx),
  fmt='.',
  color="black",
  markersize=5,
  elinewidth=1.1,
  capsize=1,
  capthick=1
)
ax[1].set_ylabel(r'Pulls [$\sigma$]', loc="center")
ylim = ax[1].get_ylim()
y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-5,5)

ax0.set_ylabel(rf"Candidates / ({{{(mrange[1]-mrange[0])/PLOT_BINS:.1f}}} MeV$/c^2$)")
ax0.legend(fontsize=15)

fig.tight_layout()

if charm_hadron=="D0":
  plt.xlabel(r"$m(K^- \pi^+)$ [MeV$/c^2$]", loc="center")
if charm_hadron=="Jpsi":
  plt.xlabel(r"$m(\mu^+\mu^-)$ [MeV$/c^2$]", loc="center")

# save figure
fig.savefig(f"{plot_path_namespace}/{f_namespace}.pdf")
fig.savefig(f"{plot_path_namespace}/{f_namespace}.png")

# sanity checks
if (mi.fmin.is_valid is False) or (mi.fmin.has_covariance is False) or (mi.fmin.has_posdef_covar is False) or (mi.fmin.has_accurate_covar is False):
  fig.savefig(f"{plot_path_namespace}/{f_namespace}_FAILED.pdf")
  fig.savefig(f"{plot_path_namespace}/{f_namespace}_FAILED.png")


# ====================================================
#               Assign Nsig/Ntot wts 
# ====================================================
if opts.sweights=="True":
  print(c("\nEngage in sWeights mode\n").red.bold)
  # set placeholders before assignment  
  df['sw'] = np.nan
  df['bw'] = np.nan

  # now compute the sweights
  spdf = lambda x: pdf(x,*mi.values,comps=['sig'])
  bpdf = lambda x: pdf(x,*mi.values,comps=['bkg','ps','ks'])

  # print computing sweights
  sweighter = SWeight(data, [spdf,bpdf], [mi.values['ns'],mi.values['nb']+mi.values['nks']+mi.values['nps']], (mrange,), compnames=('sig','bkg'), checks=True)

  sws = sweighter.getWeight(0,data)
  bws = sweighter.getWeight(1,data)

  df["sw"] = sws
  df["bw"] = bws

  # check none are NaN
  assert(np.isnan(df.sw).any()==False)
  
  # plot sWeights check
  fig, ax = plt.subplots()
  swp = sweighter.getWeight(0,x)
  bwp = sweighter.getWeight(1,x)
  ax.plot(x, swp, 'b--', label='Signal')
  ax.plot(x, bwp, 'r:' , label='Background')
  ax.plot(x, swp+bwp, 'k-', label='Sum')
  ax.set_title(template)
  ax.set_xlabel(f"$m({charm_hadron})$ [MeV]")
  ax.set_ylabel('Weight')
  ax.legend()
  fig.tight_layout()

  # save figs
  fig.savefig(f"{plot_path_namespace}/{f_namespace}_sw_check.pdf")
  fig.savefig(f"{plot_path_namespace}/{f_namespace}_sw_check.png")


# ====================================================
#               Assign Nsig/Ntot wts 
# ====================================================
if opts.sweights=="False":
  print(c("\nEngage in charm-fit-only mode, so save Nsig/Ntot\n").red.bold)
  Nsig = mi.values["ns"]
  sw = Nsig/Ntot
  print(c(f"\nExtracted Nsig/Ntot w = {sw}").blue)
  df["sw"] = sw
  print(c("Added signal charm weight").cyan)

  # adding also error, divided by Ntot; in the sum, this returns
  # the error evaluated for the real charm yield
  df["charm_y_err"] = mi.errors["ns"]/Ntot
  
  # sanity check
  print(c(f"\nCheck value counts:").bold)
  print(df["sw"].value_counts())


# ====================================================
#               Assign Nsig/Ntot wts 
# ====================================================
df.to_pickle(f"{pkl_path_namespace}/{f_namespace}_sw.pkl")


# ====================================================
#              PLOT MCORR in BIN to check 
# ====================================================
mfig = plt.figure()
plt.hist(
  df.B_plus_MCORR,
  histtype="step",
  label=f"{template} {YEAR}"
)

plt.ylabel("Events", loc="center")
plt.xlabel(r"$m_{corr}$ [MeV]", loc="center")

# save figure
os.system(f"mkdir -p {plot_path_namespace}/mcorr_checks")
mfig.savefig(f"{plot_path_namespace}/mcorr_checks/{f_namespace}.pdf")
mfig.savefig(f"{plot_path_namespace}/mcorr_checks/{f_namespace}.png")
