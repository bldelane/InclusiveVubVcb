'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from common.header import *
from termcolor2 import c
import pprint 

# binning
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json") as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").bold)
print(fit_config)

# bounds
MCORR_LOW = fit_config["B_plus_MCORR"]["range"][0]
MCORR_HIGH = fit_config["B_plus_MCORR"]["range"][-1]
LTIME_LOW=0.0005
LTIME_HIGH=0.005;

#  plots data vs mc
compare_to_MC = False

# load in the pkl'd sW'd dataframes
#misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/misid.pkl").query(f"B_plus_FIT_LTIME>={LTIME_LOW} and B_plus_FIT_LTIME<={LTIME_HIGH} and B_plus_MCORR>={MCORR_LOW} and B_plus_MCORR<={MCORR_HIGH}")
#comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/comb.pkl").query(f"B_plus_FIT_LTIME>={LTIME_LOW} and B_plus_FIT_LTIME<={LTIME_HIGH} and B_plus_MCORR>={MCORR_LOW} and B_plus_MCORR<={MCORR_HIGH}")
#dcs_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/DCS.pkl").query(f"B_plus_FIT_LTIME>={LTIME_LOW} and B_plus_FIT_LTIME<={LTIME_HIGH} and B_plus_MCORR>={MCORR_LOW} and B_plus_MCORR<={MCORR_HIGH}")
#cf_df    = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/CF.pkl").query(f"B_plus_FIT_LTIME>={LTIME_LOW} and B_plus_FIT_LTIME<={LTIME_HIGH} and B_plus_MCORR>={MCORR_LOW} and B_plus_MCORR<={MCORR_HIGH}")
misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/2018/MisID.pkl")
comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/2018/EvtMix.pkl")
dcs_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/2018/DCS.pkl")
cf_df    = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/2018/CF.pkl")

for template in [dcs_df, cf_df, misid_df, comb_df]:
    print(c("SANITY CHECK").magenta, ": expect a lower bound onf visible mass at 3500 MeV :", np.min(template["B_plus_M"]))
    print(c("SANITY CHECK").magenta, ": expect a D0_M bounds to be [1810, 1920] :", np.min(template["D0_M"]), np.max(template["D0_M"]))
    print(c("SANITY CHECK").magenta, f": expect a LTIME bounds to be [{LTIME_LOW}, {LTIME_HIGH}] :", np.min(template["B_plus_FIT_LTIME"]), np.max(template["B_plus_FIT_LTIME"]))
    print(c("SANITY CHECK").magenta, f": expect a MCORR bounds to be [{MCORR_LOW}, {MCORR_HIGH}] :", np.min(template["B_plus_MCORR"]), np.max(template["B_plus_MCORR"]))

#print(c("SANITY CHECK on MISID").magenta, ": expect a MCORRERR upper bound to be 650 :", np.max(misid_df["B_plus_MCORRERR"]))

assert((dcs_df["K_minus_ID"]*dcs_df["Mu_plus_ID"]).all()>0.0)
assert((misid_df["K_minus_ID"]*misid_df["Mu_plus_ID"]).all()>0.0)
assert(np.all(cf_df["K_minus_ID"]*cf_df["Mu_plus_ID"]<0.0))

# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(misid_df["misid_w"]*misid_df["sw"]), np.sqrt(np.sum( (misid_df["misid_w"]*misid_df["sw"])**2  )) ) 
print(c(f"MisID constraint: {MISID_CONSTRAINT}").magenta)

# a big ugly, from porting from dev.ipynb
fitvars= list(fit_config.keys())
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])

print(c(f"MCORR nBins = {TOT_BINS}").yellow)

# track components (might want to remove this in tidier version of code)
rfiles = {}
rfiles["DCS"] = dcs_df
rfiles["CF"] = cf_df
rfiles["MISID"] = misid_df
rfiles["COMB"] = comb_df

import boost_histogram as BH
def load_bh(idx):
    print(c(f"\nFilling mode: {idx}").cyan)
    indf = rfiles[idx]
    samples = [np.array(indf[fitvars[0]])]
    
    if idx=="MISID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="COMB":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="CF" or idx=="DCS":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[fitvars[0]]["range"], underflow=False, overflow=False )
    if idx=="D0_MC16" or idx=="Dst_MC16":
        wbh = BH.Histogram( ax_1 )
        wbh.fill( *samples, )
    else:
        wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
        wbh.fill( *samples, weight=wts )    
    
    print(c(f"Histogram summary: {wbh}").blue)
    return wbh

# load the boost objects encoding CF, DCS, MISID, COMB and MC templates
data_driven_samples = ("DCS", "CF", "MISID", "COMB")
MC_samples = ("D0_MC16", "Dst_MC16")
fit_components = ("CF", "COMB", "MISID", "D0_MC16", "Dst_MC16")

# load seperately-generated MC hists
MC_hists = {}
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/2018/D0_MC16.pkl", "rb") as f_dz: 
    MC_hists["D0_MC16"] = pickle.load(f_dz)
    print(c(f"\nFilling mode : D0_MC16").cyan)
    print(c(f"{MC_hists['D0_MC16']}").blue)
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/2018/Dst_MC16.pkl", "rb") as f_dst: 
    MC_hists["Dst_MC16"] = pickle.load(f_dst)
    print(c(f"\nFilling mode : Dst_MC16").cyan)
    print(c(f"{MC_hists['Dst_MC16']}").blue)

# PLOT TEMPLATES
os.system("mkdir -p /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/1D/templates")
# make plots of hists
plt.figure()
for mc_key, mc in MC_hists.items():
    if mc_key=="D0_MC16":
        _label=r"$B_c^+\to D^0 \mu^+ \nu_\mu$"
        _color="darkorange"
    if mc_key=="Dst_MC16":
        _label=r"$B_c^+\to D^{0*} \mu^+ \nu_\mu$"
        _color="black"
    plt.errorbar(x=mc.axes[0].centers, y=mc.view(), yerr=np.sqrt(mc.view()), xerr=mc.axes[0].widths/2, 
    fmt=".", 
    color=_color, 
    label=_label, 
    markersize=5,
    elinewidth=1.5,
    capsize=1.5,
    markeredgewidth=1.5,
    )
plt.title("Simulated Signal Templates")
plt.legend()
plt.savefig("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/1D/templates/signal_templates.png")


for data_template in data_driven_samples:
    plt.figure()
    bh = load_bh(data_template)
    plt.errorbar(
        x = bh.axes[0].centers,
        y = bh.view().value,
        yerr = bh.view().variance**.5,
        xerr = bh.axes[0].widths/2.,
        fmt=".",
        color="tab:blue",
        label=f"{data_template} Template 2018",
        markersize=5,
        elinewidth=1.5,
        capsize=1.5,
        markeredgewidth=1.5,
    )
    plt.title("Data-driven Template")
    plt.ylabel(f"Events")
    plt.xlabel(r"$m_{corr}(D^0 \mu^+)$  MeV$/c^2$")
    plt.legend()
    plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/1D/templates/{data_template}_template.png")

# # check against the inclusive MC
# if compare_to_MC is True:
#     #MC_SEL = "Mu_plus_TRUEID!=13 && B_plus_M>3500 && (K_minus_ID*Mu_plus_ID>0) && Mu_plus_isMuon && Mu_plus_hasMuon && Mu_plus_NShared==0 && Mu_plus_P>1e4 && Mu_plus_P<1e5 && Mu_plus_PT>1500 && Mu_plus_PIDmu>3 && Mu_plus_L0MuonDecision_TOS"
#     MC_SEL = "Mu_plus_TRUEID!=13 && B_plus_M>3500 && (K_minus_ID*Mu_plus_ID>0) && !Mu_plus_isMuon && Mu_plus_PIDmu<0 && B_plus_L0Global_TIS && Mu_plus_P>1e4 && Mu_plus_P<1e5 && Mu_plus_PT>1500 && Mu_plus_hasMuon && Mu_plus_NShared==0"
#     inclMC = pandas.DataFrame(RDF("B2DMuNuX_D02KPiTuple/DecayTree", f"root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/Bc2D0munu/inclb2D0X/inclb2D0X_2016_MagUp_5M.root").Filter(MC_SEL).AsNumpy(columns=["B_plus_MCORR"]))

#     plt.figure()
#     data_template = "MISID"
#     plt.hist(inclMC["B_plus_MCORR"], bins=30, range=[4200, 7200], density=True, color="indianred", 
#             histtype="step", label=r"Inclusive $X_b\rightarrow D^0 X'$ 2016 MagUp, Unofficial")
#     plt.hist(misid_df["B_plus_MCORR"], #weights=misid_df["misid_w"]*misid_df["sw"],
#     bins=30, range=[4200, 7200], density=True, color="tab:blue", 
#             histtype="step", label="MisID 2018 template, unweighted")

#     plt.title(r"Baseline comparison: DCS FakeMuons \& L0GlobTIS, $m(D^0 \mu^+)>3500$ MeV$/c^2$")
#     plt.legend(fontsize=20)
#     plt.ylabel(f"Events, normalised to unity", loc="center")
#     plt.xlabel(r"$m_{corr}(D^0 \mu^+)$  MeV$/c^2$", loc="center")
#     plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/1D/templates/{data_template}_vs_MC.png")

# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
for comp in data_driven_samples: 
    bh = load_bh(comp)
    template_vals[f"{comp}"] = bh.view().value.flatten()
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten()

# ===================================================
#       PATCH: load the charm fit yield error
# ---------------------------------------------------
ax_1 = BH.axis.Regular(30, 4200, 7200, underflow=False, overflow=False )
ch_wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
ch_wbh.fill( dcs_df.B_plus_MCORR, weight=dcs_df.charm_y_err ) 
ch_y_err = ch_wbh.view().value # checked
# ===================================================


# MC has a regular storage
for sim in MC_samples:
    template_vals[f"{sim}"] = MC_hists[sim].view().flatten()
    template_errs[f"{sim}"] = np.sqrt( MC_hists[sim].view().flatten() ) # poisson error

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)

#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert(template_vals["DCS"].all()>0)

# prepare samples: derive scaling factor from observations
# s_i = template_errs["DCS"]**2/template_vals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done
# assert(s_i.all() == (load_bh("DCS").view().variance/load_bh("DCS").view().value).all())
s_i = ch_y_err**2/template_vals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done

# scaled observed data
obs = template_vals["DCS"]/s_i
print(c(f"Integral of scaled obs: {np.sum(obs)}").magenta)

# scale the rest accordingly
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations)
for comp in fit_components:
    muprime_vals[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_errs[f"{comp}"] / s_i 

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["DCS"]) # n->n'

# B+
# from PDG, get the BFs
CF = ufloat(3.950e-2, 0.031e-2)
DCS = ufloat(1.50e-4, 0.07e-4)

# scale factor for CF_y -> B+_y constraint in the fit 
R = DCS/CF
# compute the error as sqrt(sum(w**2))
CF_n = load_bh("CF").sum().value
print(c("Consistency check on B+ raw yield").green.underline)
print(c(f"{np.sum(load_bh('CF').view().value)}").green)
print(c(f"{CF_n}").green)
#assert(CF_n == np.sum(load_bh("CF").view().value))
CF_s = load_bh("CF").sum().variance**.5 
CF_y = ufloat( CF_n, CF_s )
bu_y_constraint = CF_y * R
s_i_scaled_bu_y_constraint = bu_y_constraint * norm_scale_factor 

print(c("B+ constraint checks").bold)
print(f"Ratio DCS/CF = {R}")
print(f"Un(s_i-)scaled B+ yield = {bu_y_constraint}")
print(f"s_i-scaled B+ yield = {s_i_scaled_bu_y_constraint}")

# need to take into account pdf not normed to 1; see notebooks for explanation
scaled_bu_y_constraint = s_i_scaled_bu_y_constraint / muprime_vals["CF"].sum()
print(f"s_i-scaled B+ yield accounting for normalisation = {scaled_bu_y_constraint}")

# read in the unscaled misid constraint
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["MISID"].sum()
#scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["old_MISID"].sum()

print(c("MisID constraint checks").bold)
print(f"Norm scale factor = {norm_scale_factor}")
print(f"s_i-scaled misID yield = {s_i_scaled_misid_y_constraint}")
print(f"s_i-scaled misID yield norm = {scaled_misid_y_constraint}")

print(c(f"\nContraints on the B&Z-scaled B+ and MisID templates successfully derived").yellow)

#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema").yellow)
# CALCULATION BY MELIC ET AL, arXiv:1909.01213
BF_Bc2D0MuNu_3ptSR = ufloat(2.4e-5, 0.4e-5)
BF_Bc2DstMuNu_3ptSR = ufloat(7e-5, 3e-5)

# ====== 2016 efficiencies ====== 
eff_D0MuNu= ufloat(0.269, 0.001)
eff_D0gMuNu= ufloat(0.255, 0.002)
eff_D0pi0MuNu= ufloat(0.256, 0.001)

# take weighted averege for D* efficiencies
Dst_toD0g_BF = ufloat(0.353, 0.009)
Dst_toD0pi0_BF = ufloat(0.647, 0.009)

# compute weighted average 
eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)
print(c(f"\n==================================================").cyan)
print(c(f"Ratio of BF: D0/D* = {BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR}\n").cyan)
print(c(f"Extracted weighted average for Dst efficiency = {eff_DstMuNu}").cyan)
print(c(f"D0 efficiency = {eff_D0MuNu}").cyan)
print(c(f"Efficiency ratio : D0/D*= {eff_D0MuNu / eff_DstMuNu}").cyan)


# final kappa: N_D0/NDst = [BF(D0)*eff(D0)] / [BF(D*)*eff(D*)]
kappa = (BF_Bc2D0MuNu_3ptSR*eff_D0MuNu) / (BF_Bc2DstMuNu_3ptSR*eff_DstMuNu) # gaussian-constrained scaling: espress D0 as a function of Dst
print(c(f"\nKappa factor from 3ptSR, efficiency-corrected (2016) = {kappa:.4f}").blue)
print(c(f"==================================================\n").cyan)
# moving onto the spec: first formulate the standalone samples 

Dst_sample = {
                    "name": "signal_DstMuNu",
                    "data": ( muprime_vals["Dst_MC16"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": "dst_intgr", "type": "normfactor", "data": None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Dst_MC16"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

D0_sample = {
                    "name": "signal_D0MuNu",
                    "data": ( muprime_vals["D0_MC16"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": "dz_intgr", "type": "normfactor", "data": None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["D0_MC16"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        # INTRODUCE CONSTRAINT BY MELIC ET AL, arXiv:1909.01213
                        {"name": "kappa", "type": "normsys", "data": {"lo" : kappa.n-kappa.s, "hi": kappa.n+kappa.s} }, # gaussian constraint of prediction of ratios of signal BFs
                        ]
}

comb_sample = {
                    "name": "combinatorial",
                    "data": muprime_vals["COMB"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                        {"name": "comb_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

misid_sample = {  
                    "name": "misid",
                    #"data": muprime_vals["MISID"].tolist(),
                    "data": muprime_vals["MISID"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                        #{"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        {"name": "misid_y_constr", "type": "normfactor", "data": None}, # gaussian constraint
                        ]
                    }

bu_sample = {  
                    "name": "bu",
                    "data": muprime_vals["CF"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                        {"name": "bu_y_constr", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                        #{"name": "bu_y_constr", "type": "normfactor", "data": None}, # gaussian constraint
                        ]
                    }

# compile standalone gaussain-constrained models
# B+
bu_template = pyhf.Model(
  {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": "bu",
                  "data": muprime_vals["CF"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      #{"name": "bu_y_constr", "type": "normfactor", "data": None}, # gaussian constraint
                      {"name": "bu_y_constr", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                  ]
              }          
          ]
      }
    ]
  }, poi_name=None
)

# misid
misid_template = pyhf.Model(
  {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": "misid",
                  "data": muprime_vals["MISID"].tolist(),
                  #"data": muprime_vals["old_MISID"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      #{"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                      {"name": "misid_y_constr", "type": "normfactor", "data": None }, # gaussian constraint
                  ]
              }         
          ]
      }
    ]
  }, poi_name=None
)

Dst_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples": [
                    {
                    "name": "signal_DstMuNu",
                    "data": ( muprime_vals["Dst_MC16"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": "dst_intgr", "type": "normfactor", "data": None},
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                    ]
                    }
                ]
            }
        ]
    },poi_name=None
)

D0_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                    "name": "signal_D0MuNu",
                    "data": ( muprime_vals["D0_MC16"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": "dz_intgr", "type": "normfactor", "data": None},
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        # INTRODUCE CONSTRAINT BY MELIC ET AL, arXiv:1909.01213
                        {"name": "kappa", "type": "normsys", "data": {"lo" : kappa.n-kappa.s, "hi": kappa.n+kappa.s} }, # gaussian constraint of prediction of ratios of signal BFs
                        ]
                    }
                ]
            }
        ]
    },poi_name=None
)



spec = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                D0_sample,
                Dst_sample,
                comb_sample,
                bu_sample, 
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": "singlechannel",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": "sig_y_extraction",
            "config": {
                "poi": "dst_sig_y",
                "parameters": [
                    # bounds on floatig normalisations 
                    {"name":"dst_sig_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    #{"name":"dz_sig_y", "bounds":[[ 100, obs.sum() ]], "inits":[1e3]},
                    {"name":"comb_y", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name":"dst_intgr", "inits": [1./np.sum(muprime_vals["Dst_MC16"])], "fixed":True}, # fix template normaliation
                    {"name":"dz_intgr", "inits": [1./np.sum(muprime_vals["D0_MC16"])], "fixed":True}, # fix template normalisation
                    {"name":"misid_y_constr", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e4]},
                    # {"name":"bu_y_constr", "bounds":   [[ 0, obs.sum() ]], "inits":[2e4]},
                ]
            }
        }
    ],
    "version": "1.0.0"
}

# some long-winded hackly way to produce a json file preserving the format and check the schema
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = "/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec.json"
if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
with open(SCHEMA,'w') as outfile:
    outfile.write(spec)

# validate schema
workspace = json.load(open(SCHEMA))
schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
# If no exception is raised by validate(), the instance is valid.
jsonschema.validate(instance=workspace, schema=schema)
print(c("SUCCESS: validation of workspace complete").green)
workspace = pyhf.Workspace(workspace)
print(c("SUCCESS: workspace read in").green)



#======================================================
#                     Run the fit                     #
#======================================================
pdf = workspace.model(measurement_name = "sig_y_extraction")
print(f"   samples: {pdf.config.samples}")
print(f" modifiers: {pdf.config.modifiers}")
print(f"parameters: {pdf.config.par_order}")
data = workspace.data(pdf)
fit_result, likelihood= pyhf.infer.mle.fit(data, pdf, return_fitted_val=True,return_uncertainties=True)

bestfit_pars, par_uncerts = fit_result.T

print(c("\nFit results:").bold)
par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}
PRESCALED_FIT_RESULTS = {}
for k, v in par_name_dict.items():
    PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
    print(f"Extracted value of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}")

# retrieve the parameter indices
bb = bestfit_pars[1:par_name_dict["dst_sig_y"]]

print(c("\nScaled constraints: B+").green.underline)
print(c("Central value of B+ constraint:").yellow, scaled_bu_y_constraint.n)
print(c("B+ constraint @ +1\u03C3:").yellow, scaled_bu_y_constraint.n + scaled_bu_y_constraint.s)
print(c("B+ constraint @ -1\u03C3:").yellow, scaled_bu_y_constraint.n - scaled_bu_y_constraint.s)
print(c("\nFit results: B+").green.underline)
print(c("Extracted fit parameter: ").red, PRESCALED_FIT_RESULTS["bu_y_constr"])
print(c("Fitted B+ yield w/o BB:").red.bold, np.sum(bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False ))/muprime_vals["CF"].sum())
print(c("Fitted B+ yield x BB:").red.bold, np.sum(bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False )*bb)/muprime_vals["CF"].sum())
print()
print(c("\nScaled constraints: MISID").green.underline)
print(c("Central value of misID constraint:").yellow, scaled_misid_y_constraint.n)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n + scaled_misid_y_constraint.s)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n - scaled_misid_y_constraint.s)
print(c("\nFit results: MISID").green.underline)
print(c("Extracted misID parameter of interest :").red, PRESCALED_FIT_RESULTS["misid_y_constr"].n)
print(c("Fitted MisID yield w/o BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False ))/muprime_vals["MISID"].sum())
print(c("Fitted MisID yield x BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb)/muprime_vals["MISID"].sum())


os.system(f"mkdir -p /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/ch/1D/")
DCS = load_bh("DCS")
    
"""generate 1D projections of the fit and data
pars:
    - bh_projection_axis = which axes to project form nD bh
    - plot_tot_fit_model = if plot tot fit model overlayed on the fit components
""" # sloppy to do it this way, but that's okay sometimes

# load in the DCS observed data and compile all scalings, vars and errors of interest
#obs_mcorr_s_i = DCS.view().variance / DCS.view().value
obs_mcorr_s_i = ch_y_err**2 / DCS.view().value
obs_mcorr_vals = DCS.view().value / obs_mcorr_s_i
#obs_mcorr_errs = (DCS.view().variance**.5) / obs_mcorr_s_i
obs_mcorr_errs = (ch_y_err) / obs_mcorr_s_i
obs_mcorr_bin_centers = DCS.axes[0].centers
obs_mcorr_bin_widths =  DCS.axes[0].widths
print(c(f"scaled data: {np.sum(obs_mcorr_vals)}").magenta)

# compile the overall model
proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)*obs_mcorr_s_i

# book plot
fig, ax = plt.subplots(2,1,figsize=(12,8),sharex=True,gridspec_kw={'height_ratios': [5, 1]})

# plot the data first
ax0 = ax[0]

ax0.plot([], [], "", label=r"\textbf{LHCb Unofficial}", color="white")

ax0.errorbar(x=obs_mcorr_bin_centers, y=obs_mcorr_vals*obs_mcorr_s_i, yerr=obs_mcorr_errs, xerr=obs_mcorr_bin_widths/2, 
fmt=".", 
color="black", 
label="2018 DCS Data", 
markersize=5,
elinewidth=1.5,
capsize=1.5,
markeredgewidth=1.5,
)

# plot total fit mode
ax0.errorbar(
    x=obs_mcorr_bin_centers, 
    y=proj_fit, 
    xerr=obs_mcorr_bin_widths/2, 
    fmt=".", 
    color="tab:blue", 
    label="Total fit model", 
    elinewidth=2, 
    markersize=.1,
    capsize=.1,
    markeredgewidth=.1,
)

ax0.set_ylabel(rf"Events / {{{obs_mcorr_bin_widths[0]:.0f}}} MeV$/c^2$", fontsize=25, loc="center")

# # combinatorial bkg
mcorr_comb_bkg = muprime_vals["COMB"] * PRESCALED_FIT_RESULTS["comb_y"].n * bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_comb_bkg, bottom=[0], width=obs_mcorr_bin_widths, alpha=.8, label=r"Combinatorial",color="#081d58")

# misid background
mcorr_misid_bkg = misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_misid_bkg, bottom=mcorr_comb_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"MisID", color="#225ea8")

# # B+ background
mcorr_bu_bkg = bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_bu_bkg, bottom=mcorr_comb_bkg+mcorr_misid_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B^{+}\rightarrow \bar{D^0} \mu^+ \nu_\mu$", color="#66c2a5")

# D0Mu signal
mcorr_D0_sig = D0_template.expected_data( [ PRESCALED_FIT_RESULTS["dz_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].n, PRESCALED_FIT_RESULTS["kappa"].n ], include_auxdata=False )*bb * obs_mcorr_s_i
mcorr_D0_err = D0_template.expected_data( [ PRESCALED_FIT_RESULTS["dz_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].s, PRESCALED_FIT_RESULTS["kappa"].s ], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_D0_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow D^{0}\mu^+\nu_\mu$", color="darkorange")

# Dst signal
mcorr_Dst_sig = Dst_template.expected_data( [ PRESCALED_FIT_RESULTS["dst_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].n ], include_auxdata=False )*bb*obs_mcorr_s_i
mcorr_Dst_err = Dst_template.expected_data( [ PRESCALED_FIT_RESULTS["dst_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].s ], include_auxdata=False )*bb*obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_Dst_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg + mcorr_D0_sig, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow D^{0*}\mu^+\nu_\mu$", color="firebrick")

# # D0Mu signal
# mcorr_D0_sig = muprime_vals["D0_MC16"] * PRESCALED_FIT_RESULTS["dz_sig_y"].n * bb
# plt.bar(x=obs_mcorr_bin_centers, height=mcorr_D0_sig, bottom=[0], width=obs_mcorr_bin_widths, alpha=.3, label=r"$B_c^+ \rightarrow D^{0}\mu^+\nu_\mu$", color="darkorange")

# # Dst signal
# mcorr_Dst_sig = muprime_vals["Dst_MC16"] * PRESCALED_FIT_RESULTS["dst_sig_y"].n * bb
# plt.bar(x=obs_mcorr_bin_centers, height=mcorr_Dst_sig, bottom=[0], width=obs_mcorr_bin_widths, alpha=.3, label=r"$B_c^+ \rightarrow D^{0*}\mu^+\nu_\mu$", color="firebrick")



#ax0.set_yscale("log")
ax0.legend(fontsize=17, bbox_to_anchor=(1.05, 1), loc='upper left')

#pull plot (missing errors on pulls)
ax1 = ax[1]
ax[1].set_ylim(bottom=-3.5, top=3.5)
ax[1].axhline(y=-3, color="tab:red", lw=1, ls="--")
ax[1].axhline(y=3, color="tab:red", lw=1, ls="--")
ax[1].set_yticks([-3, 0,  3], minor=False)
PULLS = (obs_mcorr_vals*obs_mcorr_s_i - proj_fit) / (obs_mcorr_errs * obs_mcorr_s_i)

print(c(f"sum of pulls sq= {np.sum(np.array(PULLS)**2)}").red.bold)


ax1.bar(x=obs_mcorr_bin_centers, height=PULLS, width=obs_mcorr_bin_widths, alpha=1.,  color="black")

# #plt.ylabel(r"$\frac{obs-fit}{\sigma_{obs}}$")
ax1.set_ylabel(rf"Pulls $[\sigma]$", loc="center", fontsize=25)

fig.tight_layout()
plt.xlabel(r"$m_{corr}(D^0 \mu^+)$   [MeV$/c^2$]", loc="center", fontsize=25)
plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/ch/1D/fit.png")
plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/ch/1D/fit.pdf")
#plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/1D/fit.eps")

# print fit results, removing the s_i precaling
dst_yield = np.sum(mcorr_Dst_sig) 
dst_yield_err = np.sum(mcorr_Dst_err) 
dz_yield = np.sum(mcorr_D0_sig) 
dz_yield_err = np.sum(mcorr_D0_err) 

headers= [
    "Signal channel", "Fit resuts (physical events)"
]

table = [
    [f"Bc->D0MuNu", f"{dz_yield:.0f} +/- {dz_yield_err:.0f}"],
    [f"Bc->D*MuNu", f"{dst_yield:.0f} +/- {dst_yield_err:.0f}"],
]
print(tabulate(table, headers, tablefmt="fancy_grid"))
print(c("to-do: ensure error propagation is performed correctly").magenta)


# =================================================
#               revert to si_scaling              #
# =================================================
print(c("Now revert back to s_i-scaled space to cross-check").red.bold)

# overwrite if want to work with scaled events
obs_mcorr_s_i=1.0

# compile the overall model
proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)*obs_mcorr_s_i

# book plot
fig, ax = plt.subplots(2,1,figsize=(12,8),sharex=True,gridspec_kw={'height_ratios': [5, 1]})

# plot the data first
ax0 = ax[0]

ax0.plot([], [], "", label=r"\textbf{LHCb Unofficial}", color="white")

ax0.errorbar(x=obs_mcorr_bin_centers, y=obs_mcorr_vals*obs_mcorr_s_i, yerr=obs_mcorr_errs, xerr=obs_mcorr_bin_widths/2, 
fmt=".", 
color="black", 
label="2018 DCS Data", 
markersize=5,
elinewidth=1.5,
capsize=1.5,
markeredgewidth=1.5,
)

# plot total fit mode
ax0.errorbar(
    x=obs_mcorr_bin_centers, 
    y=proj_fit, 
    xerr=obs_mcorr_bin_widths/2, 
    fmt=".", 
    color="tab:blue", 
    label="Total fit model", 
    elinewidth=2, 
    markersize=.1,
    capsize=.1,
    markeredgewidth=.1,
)

ax0.set_ylabel(rf"Scaled events / {{{obs_mcorr_bin_widths[0]:.0f}}} MeV$/c^2$", fontsize=25, loc="center")

# # combinatorial bkg
mcorr_comb_bkg = muprime_vals["COMB"] * PRESCALED_FIT_RESULTS["comb_y"].n * bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_comb_bkg, bottom=[0], width=obs_mcorr_bin_widths, alpha=.8, label=r"Combinatorial",color="#081d58")

# misid background
mcorr_misid_bkg = misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_misid_bkg, bottom=mcorr_comb_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"MisID", color="#225ea8")

# # B+ background
mcorr_bu_bkg = bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_bu_bkg, bottom=mcorr_comb_bkg+mcorr_misid_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B^{+}\rightarrow \bar{D^0} \mu^+ \nu_\mu$", color="#66c2a5")

# D0Mu signal
mcorr_D0_sig = D0_template.expected_data( [ PRESCALED_FIT_RESULTS["dz_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].n, PRESCALED_FIT_RESULTS["kappa"].n ], include_auxdata=False )*bb * obs_mcorr_s_i
mcorr_D0_err = D0_template.expected_data( [ PRESCALED_FIT_RESULTS["dz_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].s, PRESCALED_FIT_RESULTS["kappa"].s ], include_auxdata=False )*bb * obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_D0_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow D^{0}\mu^+\nu_\mu$", color="darkorange")

# Dst signal
mcorr_Dst_sig = Dst_template.expected_data( [ PRESCALED_FIT_RESULTS["dst_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].n ], include_auxdata=False )*bb*obs_mcorr_s_i
mcorr_Dst_err = Dst_template.expected_data( [ PRESCALED_FIT_RESULTS["dst_intgr"].n, PRESCALED_FIT_RESULTS["dst_sig_y"].s ], include_auxdata=False )*bb*obs_mcorr_s_i
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_Dst_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg + mcorr_D0_sig, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow D^{0*}\mu^+\nu_\mu$", color="firebrick")

#ax0.set_yscale("log")
ax0.legend(fontsize=17, bbox_to_anchor=(1.05, 1), loc='upper left')

#pull plot (missing errors on pulls)
ax1 = ax[1]
ax[1].set_ylim(bottom=-3.5, top=3.5)
ax[1].axhline(y=-3, color="tab:red", lw=1, ls="--")
ax[1].axhline(y=3, color="tab:red", lw=1, ls="--")
ax[1].set_yticks([-3, 0,  3], minor=False)
PULLS = (obs_mcorr_vals*obs_mcorr_s_i - proj_fit) / obs_mcorr_errs  

ax1.bar(x=obs_mcorr_bin_centers, height=PULLS, width=obs_mcorr_bin_widths, alpha=1.,  color="black")

# #plt.ylabel(r"$\frac{obs-fit}{\sigma_{obs}}$")
ax1.set_ylabel(rf"Pulls $[\sigma]$", loc="center", fontsize=25)

fig.tight_layout()
plt.xlabel(r"$m_{corr}(D^0 \mu^+)$   [MeV$/c^2$]", loc="center", fontsize=25)
plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/ch/1D/s_i_scaled_fit.png")
plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/ch/1D/s_i_scaled_fit.pdf")

# print fit results, removing the s_i precaling
dst_yield = np.sum(mcorr_Dst_sig) 
dst_yield_err = np.sum(mcorr_Dst_err) 
dz_yield = np.sum(mcorr_D0_sig) 
dz_yield_err = np.sum(mcorr_D0_err) 

headers= [
    "Signal channel", "Fit resuts (*B&Z events*)"
]

table = [
    [f"Bc->D0MuNu", f"{dz_yield:.0f} +/- {dz_yield_err:.0f}"],
    [f"Bc->D*MuNu", f"{dst_yield:.0f} +/- {dst_yield_err:.0f}"],
]
print(tabulate(table, headers, tablefmt="fancy_grid"))
print(c("to-do: ensure error propagation is performed correctly").magenta)
