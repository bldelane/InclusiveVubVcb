"""Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

=== Backend to the combinations of years of data taking ===

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
"""

from builtins import breakpoint
from json import load
from os import ttyname
from common.header import *
from termcolor2 import c
import pprint
from uncertainties import ufloat

parser = ArgumentParser()
parser.add_argument(
    "-y",
    "--year",
    required=True,
    help="Year of data taking [2011,2012,2015,2016,2017,2018]",
)
parser.add_argument(
    "-s",
    "--simfit",
    action="store_true",
    help="engage in generating a schema with lumi scaled to tthe fraction of the total 2016-2018 nominal integrated luminosity",
)
parser.add_argument("-notbz", "--notBZ", action="store_true")
opts = parser.parse_args()

import boost_histogram as BH


def load_bh(idx):
    indf = rfiles[idx]
    if idx != "COMB":
        samples = [np.array(indf[FITVARS[0]])]

    if idx == "COMB":
        samples = [rfiles[idx]]

    if idx == "MISID":
        wts = (
            indf["misid_w"] * indf[f"sw"]
        )  # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    # in any other case, we only care about the sw from fit to D0 mass
    if idx == "DCS":  # FIXME CF no sweights
        wts = indf[
            f"sw"
        ]  # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1 = fit_config[FITVARS[0]]["nbins"]
    _minvar_1 = fit_config[FITVARS[0]]["range"][0]
    _maxvar_1 = fit_config[FITVARS[0]]["range"][-1]

    print(f"FIT CONFIG: {_nbins_1}, [{_minvar_1}, {_maxvar_1}]")

    if fit_config[FITVARS[0]]["is_var_axis"] == "False":
        ax_1 = BH.axis.Regular(
            _nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False
        )
    if fit_config[FITVARS[0]]["is_var_axis"] == "True":
        ax_1 = BH.axis.Variable(
            fit_config[FITVARS[0]]["range"], underflow=False, overflow=False
        )
    if (
        idx == "D0_MC"
        or idx == "Dst_MC"
        or idx == "incl_sig"
        or idx == "COMB"
        or idx == "CF"
    ):
        wbh = BH.Histogram(ax_1)
        wbh.fill(
            *samples,
        )
    else:
        wbh = BH.Histogram(ax_1, storage=BH.storage.Weight())
        wbh.fill(*samples, weight=wts)

    return wbh


# assign arg
YEAR = opts.year

# binning
# -------
with open(
    "/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/config.json"
) as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").magenta)
print(c(fit_config).magenta)

base_sel = f"XGB_antiComb>0.7 and \
XGB_antiVcb>0.4 and \
B_plus_FIT_LTIME<0.002 and \
B_plus_M>3500 and \
B_plus_M<=6285"

# load in the pkl'd sW'd [all phase space] dataframes for the desired year
misid_df = pandas.read_pickle(
    f"/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(f"{base_sel} and target==4 and (K_minus_ID*Mu_plus_ID>0)")
dcs_df = pandas.read_pickle(
    f"/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(f"{base_sel} and target==3 and (K_minus_ID*Mu_plus_ID>0)")
cf_df = pandas.read_pickle(
    f"/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(f"{base_sel} and target==0 and (K_minus_ID*Mu_plus_ID<0)")
sig_df = pandas.read_pickle(
    f"/home/blaised/private/Bc2D0MuNuX/XGB/scratch/xgb2_sw_dataset.pkl"
).query(f"{base_sel} and target==1 and (K_minus_ID*Mu_plus_ID>0)")

# check the correct sign combination
assert (dcs_df["K_minus_ID"] * dcs_df["Mu_plus_ID"]).all() > 0.0
assert (misid_df["K_minus_ID"] * misid_df["Mu_plus_ID"]).all() > 0.0
assert np.all(cf_df["K_minus_ID"] * cf_df["Mu_plus_ID"] < 0.0)


# a big ugly, from porting from dev.ipynb
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS *= fit_config[FITVARS[i]]["nbins"]

print(c(f"nBins = {TOT_BINS}").yellow)

# track components (might want to remove this in tidier version of code)
rfiles = {}
rfiles["DCS"] = dcs_df
rfiles["CF"] = cf_df
rfiles["MISID"] = misid_df
rfiles["incl_sig"] = sig_df
rfiles["COMB"] = np.load(
    "/home/blaised/private/Bc2D0MuNuX/OOP_Fit/tmp/kde/nominal_ws_kde.npy"
)

# load the boost objects encoding CF, DCS, MISID, COMB and MC templates
data_driven_samples = ["CF", "MISID", "COMB"]
MC_samples = ["incl_sig"]
fit_components = ("CF", "COMB", "MISID", "incl_sig")


# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
print("\nLoading data-driven hists:")
for comp in data_driven_samples:
    print(f"loading {comp}...")
    bh = load_bh(comp)
    try:
        unscaled_integral = np.sum(bh.view().value.flatten())
        template_vals[f"{comp}"] = bh.view().value.flatten() / unscaled_integral
        template_errs[f"{comp}"] = (
            bh.view().variance ** 0.5
        ).flatten() / unscaled_integral  # weighted data-driven histograms; note how error and not variance is stored
        assert (
            (template_vals[f"{comp}"] / template_errs[f"{comp}"])
            / (bh.view().value.flatten() / (bh.view().variance ** 0.5).flatten())
        ).all() <= 1.00001
    except:
        unscaled_integral = np.sum(bh.view().flatten())
        template_vals[f"{comp}"] = bh.view().flatten() / unscaled_integral
        template_errs[f"{comp}"] = (
            bh.view() ** 0.5
        ).flatten() / unscaled_integral  # weighted data-driven histograms; note how error and not variance is stored
        assert (
            (template_vals[f"{comp}"] / template_errs[f"{comp}"])
            / (bh.view().flatten() / (bh.view() ** 0.5).flatten())
        ).all() <= 1.00001

    assert abs(np.sum(template_vals[f"{comp}"]) - 1.0) <= 0.00001
    print(c(f"{comp}: done, normalised to unity\n").green)


# MC has a regular storage
for sim in MC_samples:
    bh = load_bh(sim)
    unscaled_integral = np.sum(bh.view().flatten())
    template_vals[f"{sim}"] = bh.view().flatten() / unscaled_integral
    template_errs[f"{sim}"] = (
        np.sqrt(bh.view().flatten()) / unscaled_integral
    )  # poisson error; note how the error and not variance is stored
    assert abs(np.sum(template_vals[f"{sim}"]) - 1.0) <= 0.00001
    assert (
        (template_vals[f"{sim}"] / template_errs[f"{sim}"])
        / (bh.view().flatten() / (bh.view().flatten() ** 0.5))
    ).all() <= 1.00001
    print(c(f"{sim}: done, normalised to unity\n").green)

# load data but do not normalise!
template_vals["DCS"] = load_bh("DCS").view().value.flatten()
template_errs["DCS"] = (load_bh("DCS").view().variance ** 0.5).flatten()
assert abs(np.sum(template_vals["DCS"]) - 1.0) >= 0.001

# quick formatting checks
for s in template_vals.values():
    assert len(s) == TOT_BINS
for s in template_errs.values():
    assert len(s) == TOT_BINS

# ======================================================
#                   BOHM & ZECH                       #
# ======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert template_vals["DCS"].all() > 0

# prepare samples: derive scaling factor from observations
if opts.notBZ:
    print(c("\n========================================================").red.bold)
    print(c("   Warning: running in debug mode with BZ turned off!").red.bold)
    print(c("========================================================\n").red.bold)
    s_i = np.ones(len(template_vals["DCS"]))
else:
    print(c("\n========================================================").green.bold)
    print(c("   Warning: running in nominal mode with BZ turned ON!").green.bold)
    print(c("========================================================\n").green.bold)
    s_i = (
        template_errs["DCS"] ** 2 / template_vals["DCS"]
    )  # for weighted fits, sum(w**2) is err; correctness check: done
assert (
    s_i.all() == (load_bh("DCS").view().variance / load_bh("DCS").view().value).all()
)  # check that the error was stored in template_errs, and not variance

# scaled observed data
obs = template_vals["DCS"] / s_i

# scale the rest accordingly
muprime_vals = {}  # container for scaled fit components
muprime_errs = {}  # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations) [note how normalise to the binned template area before B&Z-scaling]
for comp in fit_components:
    assert abs(np.sum(template_vals[f"{comp}"]) - 1.0) <= 0.00001
    muprime_vals[f"{comp}"] = template_vals[f"{comp}"] / s_i
    muprime_errs[f"{comp}"] = template_errs[f"{comp}"] / s_i


# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs) / np.sum(template_vals["DCS"])  # n->n'

# B+
# from PDG, get the BFs
CF = ufloat(3.950e-2, 0.031e-2)
DCS = ufloat(1.50e-4, 0.07e-4)

# scale factor for CF_y -> B+_y constraint in the fit
R = DCS / CF
# compute the error as sqrt(sum(w**2))
CF_n = load_bh("CF").sum()  # .value #HACK: CF no sWeights
assert CF_n > 1000  # un-normalised template
CF_s = load_bh("CF").sum() ** 0.5
CF_y = ufloat(CF_n, CF_s)
bu_y_constraint = CF_y * R
print(c(f"\n===================================").yellow)
print(c(f"B+ constraint in physical space: {bu_y_constraint}").yellow)
print(c(f"===================================\n").yellow)
s_i_scaled_bu_y_constraint = bu_y_constraint * norm_scale_factor
# need to take into account pdf not normed to 1, owing to B&Z; in other words, norm * B&Z-scaled template = B&Z-scaled constraint
scaled_bu_y_constraint = s_i_scaled_bu_y_constraint / muprime_vals["CF"].sum()


print(
    c("\n=======================================================================").bold
)
print(
    c(f"Contraints on the B&Z-scaled B+ and MisID templates successfully derived:").bold
)
print(c("B+ constraint: {:10.4f}".format(bu_y_constraint)))
print(
    c("=======================================================================\n").bold
)

# ======================================================
#                     pyhf spec                       #
# ======================================================
print(c(f"\nPreparing the fit schema...").green)


# moving onto the spec: first formulate the standalone samples
signal_sample = {
    "name": f"signal_{YEAR}",
    "data": (muprime_vals["incl_sig"]).tolist(),
    "modifiers": [
        # need to normalise to unity the signal templates to share the Dst normfactor
        # {"name": f"sig_intgr_{YEAR}", "type": "normfactor", "data": None},
        {
            "name": f"beeston_barlow_{YEAR}",
            "type": "staterror",
            "data": muprime_errs["incl_sig"].tolist(),
        },  # branch-speficific per-bin variation
        {
            "name": f"incl_sig_y",
            "type": "normfactor",
            "data": None,
        },  # floating normalisation
    ],
}

comb_sample = {
    "name": f"combinatorial_{YEAR}",
    "data": muprime_vals["COMB"].tolist(),
    "modifiers": [
        # {"name": f"comb_intgr_{YEAR}", "type": "normfactor", "data": None},
        {
            "name": f"comb_y_{YEAR}",
            "type": "normfactor",
            "data": None,
        },  # floating normalisation
        {
            "name": f"beeston_barlow_{YEAR}",
            "type": "staterror",
            "data": muprime_errs["COMB"].tolist(),
        },  # branch-speficific per-bin variation
    ],
}

misid_sample = {
    "name": f"misid_{YEAR}",
    "data": muprime_vals["MISID"].tolist(),
    "modifiers": [
        # {"name": f"misid_intgr_{YEAR}", "type": "normfactor", "data": None},
        {
            "name": f"beeston_barlow_{YEAR}",
            "type": "staterror",
            "data": muprime_errs["MISID"].tolist(),
        },  # branch-speficific per-bin variation
        {
            "name": f"misid_y_constr_{YEAR}",
            "type": "normfactor",
            "data": None,
        },  # freely floating
        # {"name": f"misid_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
    ],
}

bu_sample = {
    "name": f"bu_{YEAR}",
    "data": muprime_vals["CF"].tolist(),
    "modifiers": [
        # {"name": f"bu_intgr_{YEAR}", "type": "normfactor", "data": None},
        {
            "name": f"beeston_barlow_{YEAR}",
            "type": "staterror",
            "data": muprime_errs["CF"].tolist(),
        },  # branch-speficific per-bin variation
        # only gaussian constraint in the fit model
        # {"name": f"bu_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
        {
            "name": f"bu_y_constr_{YEAR}",
            "type": "normsys",
            "data": {
                "lo": bu_y_constraint.n - bu_y_constraint.s,
                "hi": bu_y_constraint.n + bu_y_constraint.s,
            },
        },  # gaussian constraint
        # {"name": f"bu_y_constr_{YEAR}", "type": "normfactor", "data": None},
    ],
}


# ===========================================================
#                     BUILD THE MODEL                       #
# ===========================================================
spec = {
    "channels": [
        {
            "name": f"singlechannel_{YEAR}",
            "samples": [signal_sample, comb_sample, bu_sample, misid_sample],
        },
    ],
    "observations": [
        {
            "name": f"singlechannel_{YEAR}",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        {
            "name": f"sig_y_extraction",
            "config": {
                "poi": "incl_sig_y",
                "parameters": [
                    # bounds on floatig normalisations
                    {
                        "name": f"incl_sig_y",
                        "bounds": [[0.0, obs.sum()]],
                        "inits": [1e2],
                    },
                    {
                        "name": f"comb_y_{YEAR}",
                        "bounds": [[0.0, obs.sum()]],
                        "inits": [1e3],
                    },
                    # {"name":f"sig_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["incl_sig"])], "fixed":True}, # fix template normaliation
                    # {"name":f"misid_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["MISID"])], "fixed":True}, # fix template normaliation
                    # {"name":f"comb_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["COMB"])], "fixed":True}, # fix template normaliation
                    # {"name":f"bu_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["CF"])], "fixed":True}, # fix template normaliation
                    {
                        "name": f"misid_y_constr_{YEAR}",
                        "bounds": [[0.0, obs.sum()]],
                        "inits": [1e3],
                    },
                    # {"name":f"bu_y_constr_{YEAR}", "bounds":[[ 0.0, obs.sum() ]], "inits": [ 1e4 ]},
                ],
            },
        }
    ],
    "version": "1.0.0",
}


# =================================================================================================
#               Write schema; this will be merged with other years of data taking                 #
# =================================================================================================
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = (
    f"/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json"
)
if os.path.exists(SCHEMA):
    os.remove(SCHEMA)  # remove previous fit schema
with open(SCHEMA, "w") as outfile:
    outfile.write(spec)

print(
    c(
        f"SCHEMA written for {YEAR} successfully to /home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json"
    ).green
)


# ===========================================================================
#               Write specific templates for each component                 #
# ===========================================================================
# B+
bu_template = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                {
                    "name": f"bu_{YEAR}",
                    "data": muprime_vals["CF"].tolist(),
                    "modifiers": [
                        # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                        # {"name": f"bu_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {
                            "name": f"bu_y_constr_{YEAR}",
                            "type": "normsys",
                            "data": {
                                "lo": bu_y_constraint.n - bu_y_constraint.s,
                                "hi": bu_y_constraint.n + bu_y_constraint.s,
                            },
                        },  # gaussian constraint
                        # {"name": f"bu_y_constr_{YEAR}", "type": "normfactor", "data": None }, # freely floating
                        {
                            "name": f"beeston_barlow_{YEAR}",
                            "type": "staterror",
                            "data": muprime_errs["CF"].tolist(),
                        },  # branch-speficific per-bin variation
                    ],
                }
            ],
        }
    ]
}

# misid
misid_template = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                {
                    "name": f"misid_{YEAR}",
                    "data": muprime_vals["MISID"].tolist(),
                    "modifiers": [
                        # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                        # {"name": f"misid_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {
                            "name": f"misid_y_constr_{YEAR}",
                            "type": "normfactor",
                            "data": None,
                        },  # freely floating
                        # {"name": f"misid_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        {
                            "name": f"beeston_barlow_{YEAR}",
                            "type": "staterror",
                            "data": muprime_errs["MISID"].tolist(),
                        },  # branch-speficific per-bin variation
                    ],
                }
            ],
        }
    ]
}

signal_template = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                {
                    "name": f"signal_{YEAR}",
                    "data": (muprime_vals["incl_sig"]).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal template to extract s_i-scaled norm
                        # {"name": f"sig_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {
                            "name": "incl_sig_y",
                            "type": "normfactor",
                            "data": None,
                        },  # floating normalisation
                        {
                            "name": f"sig_lumi_factor_{YEAR}",
                            "type": "normfactor",
                            "data": None,
                        },  # scale signal to luminosity
                        {
                            "name": f"beeston_barlow_{YEAR}",
                            "type": "staterror",
                            "data": muprime_errs["incl_sig"].tolist(),
                        },  # branch-speficific per-bin variation
                    ],
                }
            ],
        }
    ]
}

# evt mix
comb_template = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                {
                    "name": f"combinatorial_{YEAR}",
                    "data": muprime_vals["COMB"].tolist(),
                    "modifiers": [
                        # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                        # {"name": f"comb_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {
                            "name": f"comb_y_{YEAR}",
                            "type": "normfactor",
                            "data": None,
                        },  # floating normalisation
                        {
                            "name": f"beeston_barlow_{YEAR}",
                            "type": "staterror",
                            "data": muprime_errs["COMB"].tolist(),
                        },  # branch-speficific per-bin variation
                    ],
                }
            ],
        }
    ]
}


# write to file
bu_spec = (
    str(json.dumps(bu_template, indent=4))
    .replace("None", "null")
    .replace("True", "true")
)
bu_SCHEMA = (
    f"/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_bu_{YEAR}.json"
)
with open(bu_SCHEMA, "w") as outfile:
    outfile.write(bu_spec)

misid_spec = (
    str(json.dumps(misid_template, indent=4))
    .replace("None", "null")
    .replace("True", "true")
)
misid_SCHEMA = f"/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_misid_{YEAR}.json"
with open(misid_SCHEMA, "w") as outfile:
    outfile.write(misid_spec)

signal_spec = (
    str(json.dumps(signal_template, indent=4))
    .replace("None", "null")
    .replace("True", "true")
)
signal_SCHEMA = f"/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_incl_signal_{YEAR}.json"
with open(signal_SCHEMA, "w") as outfile:
    outfile.write(signal_spec)

comb_spec = (
    str(json.dumps(comb_template, indent=4))
    .replace("None", "null")
    .replace("True", "true")
)
comb_SCHEMA = f"/home/blaised/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_comb_{YEAR}.json"
with open(comb_SCHEMA, "w") as outfile:
    outfile.write(comb_spec)
