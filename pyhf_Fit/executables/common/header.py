# === imports ===
import ROOT
from ROOT import TMath, TGraph, TCanvas, TColor, TFile, TLegend, TLine, gPad
from ROOT import kBlue, kGreen, kRed, kBlack, kAzure, kGray, kTeal, kOrange, kViolet, kCyan, kWhite
RDF = ROOT.ROOT.RDataFrame

# analysis python modules
from argparse import ArgumentParser
import os, glob
from pathlib import Path
#from scipy import stats
import numpy as np
import boost_histogram as bh
import pandas
import json
import pyhf
#pyhf.set_backend("numpy", "minuit") # get uncertainties and covariance
pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer(verbose=1, errordef=1.)) # get uncertainties and covariance
import pickle
from uncertainties import *
from matplotlib import gridspec
from tabulate import tabulate
import json, requests, jsonschema
#from root_pandas import to_root
import colored 

#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
#plt.style.use(hep.style.LHCb2)
# plt.rcParams.update({
#     "text.usetex": True,
    #"font.family": "serif"
#     })
