'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from common.header import *
from termcolor2 import c
import pprint 

# binning
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json") as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration").bold)
print(fit_config)

def integrate_pdf(tensor, to_project, fitvars_dict=fit_config):
    """for a given data/pdf integrate over one dimension to give the 1D projection along axis"""
    dims = np.array(range(len(FITVARS)))
    
    if len(FITVARS) == 2: 
        projection = np.sum(tensor.reshape(fitvars_dict[ FITVARS[0] ]["nbins"], -1), axis=tuple(np.delete(dims, to_project)) ) # checked formatting
    if len(FITVARS) == 3: 
        projection = np.sum(tensor.reshape(fitvars_dict[ FITVARS[0] ]["nbins"], fitvars_dict[ FITVARS[1] ]["nbins"], fitvars_dict[ FITVARS[-1] ]["nbins"]), axis=tuple(np.delete(dims, to_project)) ) # checked formatting
    return projection 

# load in the pkl'd sW'd dataframes
min_ltime=0.0; max_ltime=0.004;

misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/misid.pkl").query(f"B_plus_FIT_LTIME>={min_ltime} and B_plus_FIT_LTIME<={max_ltime}")
comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/comb.pkl").query(f"B_plus_FIT_LTIME>={min_ltime} and B_plus_FIT_LTIME<={max_ltime}")
dcs_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/DCS.pkl").query(f"B_plus_FIT_LTIME>={min_ltime} and B_plus_FIT_LTIME<={max_ltime}")
cf_df    = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/D0MuNu/2018/pkl/CF.pkl").query(f"B_plus_FIT_LTIME>={min_ltime} and B_plus_FIT_LTIME<={max_ltime}")

for template in [dcs_df, cf_df, misid_df, comb_df]:
    print(c("SANITY CHECK").magenta, ": expect a lower bound onf visible mass at 3500 MeV :", np.min(template["B_plus_M"]))
    print(c("SANITY CHECK").magenta, ": expect a D0_M bounds to be [1810, 1920] :", np.min(template["D0_M"]), np.max(template["D0_M"]))
    print(c("SANITY CHECK").magenta, ": expect a LTIME bounds to be [0., 0.004] :", np.min(template["B_plus_FIT_LTIME"]), np.max(template["B_plus_FIT_LTIME"]))
    
print(c("SANITY CHECK on MISID").magenta, ": expect a MCORRERR upper bounds to be 650 :", np.max(misid_df["B_plus_MCORRERR"]))

assert((dcs_df["K_minus_ID"]*dcs_df["Mu_plus_ID"]).all()>0)
assert((misid_df["K_minus_ID"]*misid_df["Mu_plus_ID"]).all()>0)

print("checking CF sign combination")
print(np.max(cf_df["Mu_plus_ID"]*cf_df["K_minus_ID"]))
print(np.min(cf_df["Mu_plus_ID"]*cf_df["K_minus_ID"]))

# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(misid_df["misid_w"]*misid_df["sw"]), np.sqrt(np.sum( (misid_df["misid_w"]*misid_df["sw"])**2  )) )
print(c(f"MisID constraint: {MISID_CONSTRAINT}").magenta)

# a big ugly, from porting from dev.ipynb
fitvars= list(fit_config.keys())
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])


# track components (might want to remove this in tidier version of code)
rfiles = {}
rfiles["DCS"] = dcs_df
rfiles["CF"] = cf_df
rfiles["MISID"] = misid_df
rfiles["COMB"] = comb_df

import boost_histogram as BH
def load_bh(idx):
    print(c(f"\nFilling mode: {idx}").cyan)
    indf = rfiles[idx]
    
    if len(FITVARS) == 2: 
        samples = [np.array(indf[fitvars[0]]), np.array(indf[fitvars[1]])]
    
    if idx=="MISID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="COMB":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    if idx=="CF" or idx=="DCS":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    _nbins_2  = fit_config[fitvars[1]]["nbins"]
    _minvar_2 = fit_config[fitvars[1]]["range"][0] 
    _maxvar_2 = fit_config[fitvars[1]]["range"][-1] 

    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1)
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[fitvars[0]]["range"])

    if fit_config[fitvars[1]]["is_var_axis"]=="False":
        ax_2 = BH.axis.Regular(_nbins_2, _minvar_2, _maxvar_2)
    if fit_config[fitvars[1]]["is_var_axis"]=="True":
        ax_2 = BH.axis.Variable(fit_config[fitvars[1]]["range"]) 
    
    if len(FITVARS)==2:
        if idx=="D0_MC16" or idx=="Dst_MC16":
            wbh = BH.Histogram( ax_1, ax_2 )
            wbh.fill( *samples )
        else:
            wbh = BH.Histogram( ax_1, ax_2, storage=BH.storage.Weight() )
            wbh.fill( *samples, weight=wts)
    
    print(c(f"Histogram summary: {wbh}").blue)
    return wbh

# load the boost objects encoding CF, DCS, MISID, COMB and MC templates
data_driven_samples = ("DCS", "CF", "MISID", "COMB")
MC_samples = ("D0_MC16", "Dst_MC16")

# load seperately-generated MC hists
MC_hists = {}
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0_MC16.pkl", "rb") as f_dz: 
    MC_hists["D0_MC16"] = pickle.load(f_dz)
    print(c(f"\nFilling mode : D0_MC16").cyan)
    print(c(f"{MC_hists['D0_MC16']}").blue)
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/Dst_MC16.pkl", "rb") as f_dst: 
    MC_hists["Dst_MC16"] = pickle.load(f_dst)
    print(c(f"\nFilling mode : Dst_MC16").cyan)
    print(c(f"{MC_hists['Dst_MC16']}").blue)


# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
for comp in data_driven_samples: 
    bh = load_bh(comp)
    # plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/tmp/{comp}.png")
    template_vals[f"{comp}"] = bh.view().value.flatten()
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten()

# MC has a regular storage
for sim in MC_samples:
    template_vals[f"{sim}"] = MC_hists[sim].view().flatten()
    template_errs[f"{sim}"] = np.sqrt( MC_hists[sim].view().flatten() ) # poisson error

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)

#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert(template_vals["DCS"].all()>0)

# prepare samples: derive scaling factor from observations
s_i = template_errs["DCS"]**2/template_vals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done

# scaled observed data
obs = template_vals["DCS"]/s_i
print(c(f"Integral of scaled obs: {np.sum(obs)}").blue)

# scale the rest accordingly
fit_components = ("CF", "COMB", "MISID", "D0_MC16", "Dst_MC16")
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations)
for comp in fit_components:
    muprime_vals[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_errs[f"{comp}"] / s_i 

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["DCS"]) # n->n'

# B+
# from PDG, get the BFs
CF = ufloat(3.950e-2, 0.031e-2)
DCS = ufloat(1.50e-4, 0.07e-4)

# scale factor for CF_y -> B+_y constraint in the fit 
R = DCS/CF
# compute the error as sqrt(sum(w**2))
CF_n = load_bh("CF").sum().value
CF_s = load_bh("CF").sum().variance**.5 
CF_y = ufloat( CF_n, CF_s )
bu_y_constraint = CF_y * R
s_i_scaled_bu_y_constraint = bu_y_constraint * norm_scale_factor 

# need to take into account pdf not normed to 1; see notebooks for explanation
scaled_bu_y_constraint = s_i_scaled_bu_y_constraint / muprime_vals["CF"].sum()

# read in the unscaled misid constraint
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["MISID"].sum()

print(c(f"\nContraints on the B&Z-scaled B+ and MisID templates successfully derived").yellow)

#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema").yellow)
# moving onto the spec: first formulate the standalone samples 
D0_sample = {
                    "name": "signal_D0MuNu",
                    "data": muprime_vals["D0_MC16"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["D0_MC16"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dz_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

Dst_sample = {
                    "name": "signal_DstMuNu",
                    "data": muprime_vals["Dst_MC16"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Dst_MC16"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

comb_sample = {
                    "name": "combinatorial",
                    "data": muprime_vals["COMB"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                        {"name": "comb_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

misid_sample = {  
                    "name": "misid",
                    "data": muprime_vals["MISID"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                        {"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        ]
                    }

bu_sample = {  
                    "name": "bu",
                    "data": muprime_vals["CF"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                        {"name": "bu_y_constr", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                        ]
                    }

# compile standalone gaussain-constrained models
# B+
bu_template = pyhf.Model(
  {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": "bu",
                  "data": muprime_vals["CF"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": "bu_y_constr", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                  ]
              }          
          ]
      }
    ]
  }, poi_name=None
)

# misid
misid_template = pyhf.Model(
  {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": "misid",
                  "data": muprime_vals["MISID"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                  ]
              }         
          ]
      }
    ]
  }, poi_name=None
)

spec = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                D0_sample,
                Dst_sample,
                comb_sample,
                bu_sample, 
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": "singlechannel",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": "sig_y_extraction",
            "config": {
                "poi": "dz_sig_y",
                "parameters": [
                    # bounds on floatig normalisations 
                    {"name":"dz_sig_y", "bounds": [[ 0, obs.sum() ]], "inits":[1e3]},
                    {"name":"dst_sig_y", "bounds":[[ 0, obs.sum() ]], "inits":[1e3]},
                    {"name":"comb_y", "bounds":   [[ 0, obs.sum() ]], "inits":[1e3]},
                ]
            }
        }
    ],
    "version": "1.0.0"
}

# some long-winded hackly way to produce a json file preserving the format and check the schema
spec = str(json.dumps(spec, indent=4)).replace("None", "null")
with open('/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec.json','w') as outfile:
    outfile.write(spec)

# validate schema
workspace = json.load(open('./config/model_spec.json'))
schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
# If no exception is raised by validate(), the instance is valid.
jsonschema.validate(instance=workspace, schema=schema)
print(c("SUCCESS: validation of workspace complete").green)
workspace = pyhf.Workspace(workspace)
print(c("SUCCESS: workspace read in").green)

#======================================================
#                     Run the fit                     #
#======================================================
pdf = workspace.model(measurement_name = "sig_y_extraction")
data = workspace.data(pdf)
fit_result, likelihood= pyhf.infer.mle.fit(data, pdf, return_fitted_val=True,return_uncertainties=True)
best_fit = pyhf.infer.mle.fit(data, pdf)
bestfit_pars, par_uncerts = fit_result.T

print(c("\nFit results:").bold)
par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}
PRESCALED_FIT_RESULTS = {}
for k, v in par_name_dict.items():
    PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
    print(f"Extracted normalisation of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}")

# print fit results, removing the s_i precaling
dst_yield = (PRESCALED_FIT_RESULTS["dst_sig_y"] * muprime_vals["Dst_MC16"].sum() )/ norm_scale_factor
dz_yield = (PRESCALED_FIT_RESULTS["dz_sig_y"] * muprime_vals["D0_MC16"].sum() )/ norm_scale_factor

headers= [
    "Signal channel", "Fit resuts (physical events)"
]

table = [
    [f"Bc->D0MuNu", "{:,.3u}".format(dz_yield)],
    [f"Bc->D*MuNu", "{:,.3u}".format(dst_yield)],
]

print()
print(tabulate(table, headers, tablefmt="fancy_grid"))

# retrieve the parameter indices
print(f"\n{par_name_dict}")
bb = bestfit_pars[:par_name_dict["dz_sig_y"]]

print(c("\nScaled constraints: B+").green.underline)
print(c("Central value of B+ constraint:").yellow, scaled_bu_y_constraint.n)
print(c("B+ constraint @ +1\u03C3:").yellow, scaled_bu_y_constraint.n + scaled_bu_y_constraint.s)
print(c("B+ constraint @ -1\u03C3:").yellow, scaled_bu_y_constraint.n - scaled_bu_y_constraint.s)
print(c("\nFit results: B+").green.underline)
print(c("Extracted fit parameter: ").red, PRESCALED_FIT_RESULTS["bu_y_constr"])
print(c("Fitted B+ yield w/o BB:").red.bold, np.sum(bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False ))/muprime_vals["CF"].sum())
print(c("Fitted B+ yield x BB:").red.bold, np.sum(bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False )*bb)/muprime_vals["CF"].sum())
print()
print(c("\nScaled constraints: MISID").green.underline)
print(c("Central value of misID constraint:").yellow, scaled_misid_y_constraint.n)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n + scaled_misid_y_constraint.s)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n - scaled_misid_y_constraint.s)
print(c("\nFit results: MISID").green.underline)
print(c("Extracted misID parameter of interest :").red, PRESCALED_FIT_RESULTS["misid_y_constr"].n)
print(c("Fitted MisID yield w/o BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False ))/muprime_vals["MISID"].sum())
print(c("Fitted MisID yield x BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb)/muprime_vals["MISID"].sum())


os.system(f"mkdir -p /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots")
DCS = load_bh("DCS")

def plot_fit_proj(proj_axis, plot_tot_fit_model=False, _units=None, _xlabel=None):
    """generate 1D projections of the fit and data
    pars:
        - bh_projection_axis = which axes to project form nD bh
        - plot_tot_fit_model = if plot tot fit model overlayed on the fit components
    """ # sloppy to do it this way, but that's okay sometimes
    
    # load in the DCS observed data and compile all scalings, vars and errors of interest
    
    obs_mcorr_s_i = DCS.project(proj_axis).view().variance / DCS.project(proj_axis).view().value
    obs_mcorr_vals = DCS.project(proj_axis).view().value / obs_mcorr_s_i
    print(c(f"scaled data: {np.sum(obs_mcorr_vals)}").red)
    obs_mcorr_errs = (DCS.project(proj_axis).view().variance**.5) / obs_mcorr_s_i
    obs_mcorr_bin_centers = DCS.axes[proj_axis].centers
    obs_mcorr_bin_widths =  DCS.axes[proj_axis].widths

    # compile the overall model
    proj_fit = integrate_pdf(tensor=pdf.expected_data(bestfit_pars, include_auxdata=False), to_project=proj_axis)
    print(c(f"{np.sum(proj_fit)}").cyan)
    fig = plt.figure(figsize=[10, 9])
    gs = gridspec.GridSpec(ncols=1, nrows=2, height_ratios=[5, 1])

    # plot the data first
    ax0 = plt.subplot(gs[0])
    plt.minorticks_on()
    plt.tick_params(axis='both', which='major', direction="in")
    plt.tick_params(axis='both', which='minor', direction="in")
    plt.errorbar(x=obs_mcorr_bin_centers, y=obs_mcorr_vals, yerr=obs_mcorr_errs, xerr=obs_mcorr_bin_widths/2, 
    fmt=".", 
    color="black", 
    label="LHCb 2018 DCS MU+MD ", 
    markersize=10,
    elinewidth=2,
    capsize=2,
    markeredgewidth=2,
    )
    
    
    if _units=="ns" : plt.ylabel(f"Scaled events / {obs_mcorr_bin_widths[0]:.4f} {_units}", fontsize=25, loc="center")
    else : plt.ylabel(f"Scaled events / {obs_mcorr_bin_widths[0]} {_units}", fontsize=25, loc="center")
    
    # total fit
    if plot_tot_fit_model==True:
        plt.errorbar(x=obs_mcorr_bin_centers, y=proj_fit, xerr=obs_mcorr_bin_widths/2, fmt=".", color="dodgerblue", label="Total fit model", elinewidth=1.5, markersize=.1)

    # # combinatorial bkg
    mcorr_comb_bkg = integrate_pdf(tensor=muprime_vals["COMB"] * PRESCALED_FIT_RESULTS["comb_y"].n * bb, to_project=proj_axis)
    plt.bar(x=obs_mcorr_bin_centers, height=mcorr_comb_bkg, bottom=[0], width=obs_mcorr_bin_widths, alpha=.8, label=r"Combinatorial $D^{0}\mu$",color="#081d58")

    # misid background
    mcorr_misid_bkg = integrate_pdf(tensor = misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb, to_project=proj_axis)
    plt.bar(x=obs_mcorr_bin_centers, height=mcorr_misid_bkg, bottom=mcorr_comb_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"MisID + $D^{0}h$ combinatorial", color="#225ea8")

    # # B+ background
    mcorr_bu_bkg = integrate_pdf(tensor = bu_template.expected_data( [ PRESCALED_FIT_RESULTS["bu_y_constr"].n], include_auxdata=False )*bb, to_project=proj_axis)
    plt.bar(x=obs_mcorr_bin_centers, height=mcorr_bu_bkg, bottom=mcorr_comb_bkg+mcorr_misid_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$B^{+}\rightarrow \bar{D^0} \mu \nu$", color="#7bccc4")

    # D0Mu signal
    mcorr_D0_sig = integrate_pdf(tensor=muprime_vals["D0_MC16"] * PRESCALED_FIT_RESULTS["dz_sig_y"].n * bb, to_project=proj_axis)
    plt.bar(x=obs_mcorr_bin_centers, height=mcorr_D0_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg, width=obs_mcorr_bin_widths, alpha=.7, label=r"$D^{0}\mu\nu$ signal", color="darkorange")

    # Dst signal
    mcorr_Dst_sig = integrate_pdf(tensor=muprime_vals["Dst_MC16"] * PRESCALED_FIT_RESULTS["dst_sig_y"].n * bb, to_project=proj_axis)
    plt.bar(x=obs_mcorr_bin_centers, height=mcorr_Dst_sig, bottom=mcorr_comb_bkg+mcorr_misid_bkg + mcorr_bu_bkg + mcorr_D0_sig, width=obs_mcorr_bin_widths, alpha=.7, label=r"$D^{*}\mu\nu$ signal", color="firebrick")

    # plt.legend(fontsize=15)
 
    # # pull plot (missing errors on pulls)
    # ax1 = plt.subplot(gs[1])

    # PULLS = (obs_mcorr_vals - proj_fit) / obs_mcorr_errs  
    # plt.bar(x=obs_mcorr_bin_centers, height=PULLS, width=obs_mcorr_bin_widths, alpha=1.,  color="black")

    # plt.ylim(-7, 7)
    # plt.minorticks_on()
    # plt.axhspan(5, 7, facecolor="firebrick", alpha=0.3)
    # plt.axhspan(-7, -5, facecolor="firebrick", alpha=0.3)
    # plt.axhspan(3, 5, facecolor="darkorange", alpha=0.3)
    # plt.axhspan(-5, -3, facecolor="darkorange", alpha=0.3)

    # #plt.ylabel(r"$\frac{obs-fit}{\sigma_{obs}}$")
    # plt.ylabel(rf"Pulls $[\sigma]$", loc="center")
    # plt.xlabel(_xlabel, loc="center")
    
    # save
    # plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/fit.pdf")
    plt.savefig(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots/fit_{proj_axis}.png")


plot_fit_proj(proj_axis=0, plot_tot_fit_model=True, _units="MeV", _xlabel=r"$m_{corr}(D^{0}\mu)$   [MeV]")
plot_fit_proj(proj_axis=1, plot_tot_fit_model=True, _units="MeV", _xlabel=r"$\tau(B)$   [ns]")