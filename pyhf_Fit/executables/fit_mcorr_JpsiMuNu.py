'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from common.header import *
from termcolor2 import c
import pprint 

parser = ArgumentParser()
parser.add_argument('-y','--year',   required=True, help='Year of data-taking/MC', choices=["2011", "2012", "2015", "2016", "2017", "2018"])
parser.add_argument('-Y','--mcyear', default="2016",help='Year of MC') # have all needed Run 2 MC in 2016
parser.add_argument('-m','--mode',   required=True, help='Decay mode, choose from [D0MuNu|JpsiMuNu]', choices=["D0MuNu", "JpsiMuNu"])
parser.add_argument('-p','--path',   default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/fit_plots', help='Path for output')
parser.add_argument('-c','--config', default='/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json', help='Path to fit config')
opts = parser.parse_args()

YEAR = opts.year
MCYEAR = opts.mcyear
MODE = opts.mode
CONFIG = opts.config
PLOTS_PATH = opts.path
DATA_PATH = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/{MODE}/{YEAR}/pkl"
MC_PATH = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/{MODE}/{MCYEAR}"
charm_had = "Jpsi"

# binning
with open( CONFIG ) as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").bold)
print(fit_config)

# bounds
MCORR_LOW = fit_config["B_plus_MCORR"]["range"][0]
MCORR_HIGH = fit_config["B_plus_MCORR"]["range"][-1]
LTIME_LOW=0.0005
LTIME_HIGH=0.005
CHARM_M_LOW = 3020 
CHARM_M_HIGH=3190

data_modes = ["sig", "comb", "misid"]

# read in double-charm instead of the n-body JpsiDX templates
mc_modes = ["Bc2JpsiMuNu", "doublecharm", "Bc2JpsiTauNu", "Bc2Psi2SMuNu", "Bc2ChiCMuNu", "Bc2ChiC2MuNu"]

data_dfs = {}
for d in data_modes:
    data_dfs[d] = pandas.read_pickle(f"{DATA_PATH}/{d}.pkl").query(f"B_plus_FIT_LTIME>={LTIME_LOW} and B_plus_FIT_LTIME<={LTIME_HIGH} and B_plus_MCORR>={MCORR_LOW} and B_plus_MCORR<={MCORR_HIGH}")
    print(c(f"Reading in{DATA_PATH}/{d}.pkl").yellow)
    print(c("SANITY CHECK").magenta, ": expect no lower bound on visible mass :", np.min(data_dfs[d]["B_plus_M"]))
    print(c("SANITY CHECK").magenta, f": expect a charm mass bounds to be [{CHARM_M_LOW}, {CHARM_M_HIGH}] :", np.min(data_dfs[d][f"{charm_had}_M"]), np.max(data_dfs[d][f"{charm_had}_M"]))
    print(c("SANITY CHECK").magenta, f": expect a LTIME bounds to be [{LTIME_LOW}, {LTIME_HIGH}] :", np.min(data_dfs[d]["B_plus_FIT_LTIME"]), np.max(data_dfs[d]["B_plus_FIT_LTIME"]))    
    print(c("SANITY CHECK").magenta, f": expect a MCORR bounds to be [{MCORR_LOW}, {MCORR_HIGH}] :", np.min(data_dfs[d]["B_plus_MCORR"]), np.max(data_dfs[d]["B_plus_MCORR"]))


# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(data_dfs["misid"]["misid_w"]*data_dfs["misid"]["sw"]), np.sqrt(np.sum( (data_dfs["misid"]["misid_w"]*data_dfs["misid"]["sw"])**2  )) ) 
print(c(f"MisID constraint: {MISID_CONSTRAINT}").magenta)

# a big ugly, from porting from dev.ipynb
fitvars= list(fit_config.keys())
n_fitvars = len(fitvars)
TOT_BINS = 1
for i in range(n_fitvars):
    TOT_BINS*=(fit_config[fitvars[i]]["nbins"])
print(c(f"MCORR nBins = {TOT_BINS}").yellow)


import boost_histogram as BH
def load_bh(idx):
    print(c(f"\nFilling mode: {idx}").cyan)
    indf = data_dfs[idx]
    samples = [np.array(indf[fitvars[0]])]
    if idx=="misid":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="comb":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="sig":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to charm hadron mass

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[fitvars[0]]["range"], underflow=False, overflow=False )
    # if idx=="D0_MC16" or idx=="Dst_MC16":
    #     wbh = BH.Histogram( ax_1 )
    #     wbh.fill( *samples, )
    else:
        wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
        wbh.fill( *samples, weight=wts )    
    
    print(c(f"Histogram summary: {wbh}").blue)
    return wbh


# load seperately-generated MC hists
MC_dfs = {}
for m in mc_modes: 
    with open(f"{MC_PATH}/{m}.pkl", "rb") as fsim: 
        MC_dfs[m] = pickle.load(fsim)
    print(c(f"\nFilling mode : {m}").cyan)
    print(c(MC_dfs[m]).blue)

# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# weighted data with dedicated storage
for comp in data_dfs.keys(): 
    bh = load_bh(comp) # load into boost hist
    template_vals[f"{comp}"] = bh.view().value.flatten() # for sim fit with n>=2 dimensions in fit
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten() # error for weighted hist

# MC has a regular storage
for sim in MC_dfs.keys():
    template_vals[f"{sim}"] = MC_dfs[sim].view().flatten()
    template_errs[f"{sim}"] = np.sqrt( MC_dfs[sim].view().flatten() ) # poisson error

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)


#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert(template_vals["sig"].all()>0)

# prepare samples: derive scaling factor from observations
s_i = template_errs["sig"]**2/template_vals["sig"] # for weighted fits, sum(w**2) is err; correctness check: done
assert(s_i.all() == (load_bh("sig").view().variance/load_bh("sig").view().value).all())

# scaled observed data
obs = template_vals["sig"]/s_i
print(c(f"Integral of scaled obs: {np.sum(obs)}").magenta)

# scale the rest accordingly
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# scale the normalised expectations
for comp in template_vals.keys():
    muprime_vals[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_errs[f"{comp}"] / s_i 

print(c("\nSummary of templates:").cyan)
print(c(muprime_vals.keys()).cyan)

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["sig"]) # n->n'

# read in the unscaled misid constraint
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["misid"].sum()
print(c("MisID constraint checks").bold)
print(f"Norm scale factor = {norm_scale_factor}")
print(f"s_i-scaled misID yield = {s_i_scaled_misid_y_constraint}")
print(f"s_i-scaled misID yield norm = {scaled_misid_y_constraint}")

print(c(f"\nContraints on the B&Z-scaled MisID template successfully derived").yellow)


#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema").yellow)
# moving onto the spec: first formulate the standalone samples

#====================
# Simulated Templates
#====================
# fix tau to mu from R(jpsi), LHCB-PAPER-2017-035
r_jpsi_cval = 0.71
r_jpsi_stat = 0.17
r_jpsi_syst = 0.18
r_jpsi_err = np.sqrt( r_jpsi_stat**2 + r_jpsi_syst**2 )
r_jpsi_BF = ufloat(r_jpsi_cval, r_jpsi_err)

# need to take into account the BF(tau->mu nu nu)
BF_tau_to_mu = ufloat( 17.39e-2, 0.04e-2) # from PDG
RR_jpsi = r_jpsi_BF * BF_tau_to_mu 

# quickly effs computed by hand
#https://gitlab.cern.ch/bldelane/InclusiveVubVcb/-/blob/new_efficiencies/Efficiencies/mds/Total.md
eff_muonic_MU = ufloat( 0.00425676 , 4.51177e-05 )
eff_muonic_MD = ufloat( 0.00423643 , 4.48132e-05 )
eff_muonic = .5 * (eff_muonic_MU + eff_muonic_MD)
print(c(f"muonic efficiency : {eff_muonic}").green)

eff_tauonic_MU = ufloat( 0.00157176, 1.13225e-05 )
eff_tauonic_MD = ufloat( 0.00157649 , 1.14298e-05 )
eff_tauonic = .5 * (eff_tauonic_MU + eff_tauonic_MD)
print(c(f"tauonic efficiency : {eff_tauonic}").green)


r_jpsi  = RR_jpsi * (eff_tauonic/eff_muonic) # r(jpsi) x BF(tau->mu nu nu) x eff ratio
print(c(f"efficiency-corrected RR_jpsi constraint = {r_jpsi.n} +/- {r_jpsi.s}").red)

signal_Bc2JpsiMuNu_sample = {
                    "name": "signal_Bc2JpsiMuNu",
                    "data": muprime_vals["Bc2JpsiMuNu"].tolist(),
                    "modifiers": [
                        {"name": "jpsimunu_intgr", "type": "normfactor", "data":None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2JpsiMuNu"].tolist() }, # branch-speficific per-bin variation
                        {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}
# gaussian-constrain to jpsimunu component with eff-corrected R(jpsi)
Bc2JpsiTauNu_sample = {
                    "name": "Bc2JpsiTauNu",
                    "data": muprime_vals["Bc2JpsiTauNu"].tolist(),
                    "modifiers": [
                        {"name": "jpsitaunu_intgr", "type": "normfactor", "data":None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2JpsiTauNu"].tolist() }, # branch-speficific per-bin variation
                        {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": "r_jpsi", "type": "normsys", "data": {"lo" : r_jpsi.n - r_jpsi.s, "hi" : r_jpsi.n + r_jpsi.s} },
                        ]
}
# taken from Table 20 of R(jpsi)
# gaussian constraint on psi(2S) yield relative to jpsimunu
Bc2Psi2SMuNu_sample = {
                    "name": "signal_Bc2Psi2SMuNu",
                    "data": muprime_vals["Bc2Psi2SMuNu"].tolist(),
                    "modifiers": [
                        {"name": "psi2Smunu_intgr", "type": "normfactor", "data":None}, # for normalisation to unity
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2Psi2SMuNu"].tolist() }, # MC stat per-bin variation
                        #{"name": "psi2Smunu_y", "type": "normfactor", "data": None }, # freely floating 
                        {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                        {"name": "psi2Smunu_constr", "type": "normsys", "data": {"lo" : 1e-6, "hi" : 0.020+0.040} }, # table 20
                        ]
}
Bc2ChiCMuNu_sample = {
                    "name": "Bc2ChiCMuNu",
                    "data": muprime_vals["Bc2ChiCMuNu"].tolist(),
                    "modifiers": [
                        {"name": "chic_intgr", "type": "normfactor", "data":None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2ChiCMuNu"].tolist() }, # branch-speficific per-bin variation
                        #{"name": "jpsichicmunu_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                        {"name": "chic_constr", "type": "normsys", "data": {"lo" : (0.111-0.082)/2., "hi" : (0.111+0.082)/2.} }, # table 20, shared between the two chic templates
                        ]
}
Bc2ChiC2MuNu_sample = {
                    "name": "Bc2ChiC2MuNu",
                    "data": muprime_vals["Bc2ChiC2MuNu"].tolist(),
                    "modifiers": [
                        {"name": "chic2_intgr", "type": "normfactor", "data":None},
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2ChiC2MuNu"].tolist() }, # branch-speficific per-bin variation
                        # fix yield to be the same as Bc2ChiCMuNu
                        #{"name": "jpsichicmunu_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                        {"name": "chic_constr", "type": "normsys", "data": {"lo" : (0.111-0.082)/2., "hi" : (0.111+0.082)/2.} }, # table 20, shared between the two chic templates
                        ]
}
doublecharm_sample = {
                    "name": "doublecharm",
                    "data": muprime_vals["doublecharm"].tolist(),
                    "modifiers": [
                        {"name": "doublecharm_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["doublecharm"].tolist() }, # branch-speficific per-bin variation
                        {"name": "doublecharm_y", "type": "normfactor", "data": None }, # freely floating 
                        #{"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                        #{"name": "doublecharm_constr", "type": "normsys", "data": {"lo" : 1e-6, "hi" : 0.002+0.023} }, # table 20
                        ]
}
# Bc2JpsiDx_sample = {
#                     "name": "Bc2JpsiDx",
#                     "data": muprime_vals["Bc2JpsiDx"].tolist(),
#                     "modifiers": [
#                         {"name": "dc_2body_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
#                         {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2JpsiDx"].tolist() }, # branch-speficific per-bin variation
#                         {"name": "jpsidx_y", "type": "normfactor", "data": None }, # floating normalisation
#                         ]
# }
# Bc2JpsiDsx_sample = {
#                     "name": "Bc2JpsiDsx",
#                     "data": muprime_vals["Bc2JpsiDsx"].tolist(),
#                     "modifiers": [
#                         {"name": "dc_quasi2body_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
#                         {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2JpsiDsx"].tolist() }, # branch-speficific per-bin variation
#                         {"name": "jpsidx_y", "type": "normfactor", "data": None }, # same yield as 2-body double charm
#                         ]
# }
# # # omitted from R(jpsi) nominl model: double-charm made of 2-body and quasi-2-body 
# # Bc2JpsiDxKx_sample = {
# #                     "name": "Bc2JpsiDxKx",
# #                     "data": muprime_vals["Bc2JpsiDxKx"].tolist(),
# #                     "modifiers": [
# #                         {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["Bc2JpsiDxKx"].tolist() }, # branch-speficific per-bin variation
# #                         {"name": "jpsidxkx_y", "type": "normfactor", "data": None }, # floating normalisation
# #                         ]
# # }


#======================
# Data-Driven Templates
#======================
comb_sample = {
                    "name": "combinatorial",
                    "data": muprime_vals["comb"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["comb"].tolist() }, # branch-speficific per-bin variation
                        {"name": "comb_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}
misid_sample = {  
                    "name": "misid",
                    "data": muprime_vals["misid"].tolist(),
                    "modifiers": [
                        {"name": "beeston_barlow", "type": "staterror", "data": muprime_errs["misid"].tolist() }, # branch-speficific per-bin variation
                        {"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        #{"name": "misid_y_constr", "type": "normfactor", "data": None}, # floating
                        ]
}


spec = {
    "channels": [
        {
            "name": "singlechannel",
            "samples": [
                signal_Bc2JpsiMuNu_sample,
                Bc2Psi2SMuNu_sample,
                Bc2JpsiTauNu_sample,
                Bc2ChiCMuNu_sample, 
                Bc2ChiC2MuNu_sample,
                # Bc2JpsiDx_sample,
                # Bc2JpsiDsx_sample,
                #Bc2JpsiDxKx_sample,
                doublecharm_sample,
                comb_sample,
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": "singlechannel",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": "sig_y_extraction",
            "config": {
                "poi": "jpsimunu_sig_y",
                "parameters": [
                    # bounds on floatig normalisations 
                    {"name": "jpsimunu_sig_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name": "jpsimunu_intgr", "inits": [1./np.sum(muprime_vals["Bc2JpsiMuNu"])], "fixed": True}, # fix template normalisation to unity
                    
                    {"name": "jpsitaunu_intgr", "inits": [1./np.sum(muprime_vals["Bc2JpsiTauNu"])], "fixed": True}, # fix template normalisation to unity
                    
                    {"name": "psi2Smunu_intgr", "inits": [1./np.sum(muprime_vals["Bc2Psi2SMuNu"])], "fixed": True}, # fix template normalisation to unity
                    #{"name": "psi2Smunu_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]}, # free normalisation
                    
                    #{"name":"jpsichicmunu_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name": "chic_intgr", "inits":  [1./np.sum(muprime_vals["Bc2ChiCMuNu"])], "fixed": True}, # fix template normalisation to unity
                    {"name": "chic2_intgr", "inits": [1./np.sum(muprime_vals["Bc2ChiC2MuNu"])], "fixed": True}, # fix template normalisation to unity
                    
                    # {"name":"jpsidx_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    # {"name": "dc_2body_intgr", "inits": [1./np.sum(muprime_vals["Bc2JpsiDx"])], "fixed": True}, # fix template normalisation to unity
                    # #{"name":"jpsidxkx_y", "bounds": [[ 0, obs.sum() ]], "inits":[1e3]},
                    # {"name":"jpsidsx_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    # {"name": "dc_quasi2body_intgr", "inits": [1./np.sum(muprime_vals["Bc2JpsiDsx"])], "fixed": True}, # fix template normalisation to unity

                    {"name": "doublecharm_intgr", "inits": [1./np.sum(muprime_vals["doublecharm"])], "fixed": True}, # fix template normalisation to unity
                    {"name": "doublecharm_y", "bounds": [[ 1e-6, obs.sum() ]], "inits":[1e3]}, # free normalisation

                    {"name":"comb_y", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    #{"name":"misid_y_constr", "inits":[16000], "bounds": [[1e-6, obs.sum()]], "fixed":False},
                ]
            }
        }
    ],
    "version": "1.0.0"
}


# =========================
# Component-specific models
# =========================
Bc2ChiCMuNu_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "Bc2ChiCMuNu",
                        "data": muprime_vals["Bc2ChiCMuNu"].tolist(),
                        "modifiers": [
                            {"name": "chic_intgr", "type": "normfactor", "data":None},
                            #{"name": "jpsichicmunu_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                            {"name": "chic_constr", "type": "normsys", "data": {"lo" : (0.111-0.082)/2., "hi" : (0.111+0.082)/2.} }, # table 20, shared between the two chic templates
                        ]
                    }
                ]
            }
        ]
    },poi_name=None
)
Bc2ChiC2MuNu_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "Bc2ChiC2MuNu",
                        "data": muprime_vals["Bc2ChiC2MuNu"].tolist(),
                        "modifiers": [
                            {"name": "chic_intgr", "type": "normfactor", "data":None},
                            #{"name": "jpsichicmunu_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                            {"name": "chic_constr", "type": "normsys", "data": {"lo" : (0.111-0.082)/2., "hi" : (0.111+0.082)/2.} }, # table 20, shared between the two chic templates
                        ]
                    }
                ]
            }
        ]
    },poi_name=None
)
# Bc2JpsiDx_template = pyhf.Model(
#     {
#         "channels": [
#             {
#                 "name": "singlechannel",
#                 "samples":[
#                     {
#                         "name": "Bc2JpsiDx",
#                         "data": muprime_vals["Bc2JpsiDx"].tolist(),
#                         "modifiers": [
#                             {"name": "dc_2body_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
#                             {"name": "jpsidx_y", "type": "normfactor", "data": None }, # floating normalisation
#                         ]
#                     }
#                 ]
#             }
#         ]
#     },poi_name=None
# )
# Bc2JpsiDsx_template = pyhf.Model(
#     {
#         "channels": [
#             {
#                 "name": "singlechannel",
#                 "samples":[
#                     {
#                         "name": "Bc2JpsiDsx",
#                         "data": muprime_vals["Bc2JpsiDsx"].tolist(),
#                         "modifiers": [
#                             {"name": "dc_quasi2body_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
#                             {"name": "jpsidx_y", "type": "normfactor", "data": None }, # floating normalisation
#                         ]
#                     }
#                 ]
#             }
#         ]
#     },poi_name=None
# )

doublecharm_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "doublecharm",
                        "data": muprime_vals["doublecharm"].tolist(),
                        "modifiers": [
                            {"name": "doublecharm_intgr", "type": "normfactor", "data": None }, # fix template normalisation to unity
                            {"name": "doublecharm_y", "type": "normfactor", "data": None}, # free normalisation    
                            #{"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                            #{"name": "doublecharm_constr", "type": "normsys", "data": {"lo" : 1e-6, "hi" : 0.002+0.023} }, # table 20
                        ]
                    }
                ]
            }
        ]
    },poi_name=None
)

signal_Bc2JpsiMuNu_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "signal_Bc2JpsiMuNu",
                        "data": muprime_vals["Bc2JpsiMuNu"].tolist(),
                        "modifiers": [
                            {"name": "jpsimunu_intgr", "type": "normfactor", "data":None},
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        ]
                    }
                ]
            }
        ]    
    }, poi_name=None
)

Bc2JpsiTauNu_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "Bc2JpsiTauNu",
                        "data": muprime_vals["Bc2JpsiTauNu"].tolist(),
                        "modifiers": [
                            {"name": "jpsitaunu_intgr", "type": "normfactor", "data":None},
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": "r_jpsi", "type": "normsys", "data": {"lo" : r_jpsi.n - r_jpsi.s, "hi" : r_jpsi.n + r_jpsi.s} },
                        ]
                    }
                ]
            }
        ]    
    }, poi_name=None
)

Bc2Psi2SMuNu_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "Bc2Psi2SMuNu",
                        "data": muprime_vals["Bc2Psi2SMuNu"].tolist(),
                        "modifiers": [
                            {"name": "psi2Smunu_intgr", "type": "normfactor", "data":None}, # for normalisation to unity
                            #{"name": "psi2Smunu_y", "type": "normfactor", "data": None}, # free normalisation
                            {"name": "jpsimunu_sig_y", "type": "normfactor", "data": None }, # share with JpsiMuNu 
                            {"name": "psi2Smunu_constr", "type": "normsys", "data": {"lo" : 1e-6, "hi" : 0.020+0.040} }, # table 20
                        ]
                    }
                ]
            }
        ]    
    }, poi_name=None
)

misid_template = pyhf.Model(
    {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                        "name": "misid",
                        "data": muprime_vals["misid"].tolist(),
                        "modifiers": [
                            {"name": "misid_y_constr", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                            #{"name": "misid_y_constr", "type": "normfactor", "data": None}, # floating
                        ]
                    }
                ]
            }
        ]    
    }, poi_name=None
)


# some long-winded hackly way to produce a json file preserving the format and check the schema
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = "/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec.json"
if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
with open(SCHEMA,'w') as outfile:
    outfile.write(spec)

# validate schema
workspace = json.load(open(SCHEMA))
schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
# If no exception is raised by validate(), the instance is valid.
jsonschema.validate(instance=workspace, schema=schema)
print(c("SUCCESS: validation of workspace complete").green)
workspace = pyhf.Workspace(workspace)
print(c("SUCCESS: workspace read in").green)


#======================================================
#                     Run the fit                     #
#======================================================
pdf = workspace.model(measurement_name = "sig_y_extraction")
data = workspace.data(pdf)
print(f"   samples: {pdf.config.samples}")
print(f" modifiers: {pdf.config.modifiers}")
print(f"parameters: {pdf.config.par_order}")
fit_result, likelihood= pyhf.infer.mle.fit(data, pdf, return_fitted_val=True,return_uncertainties=True)
best_fit = pyhf.infer.mle.fit(data, pdf)
bestfit_pars, par_uncerts = fit_result.T

print(c("\nFit results:").bold)
par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}
PRESCALED_FIT_RESULTS = {}
for k, v in par_name_dict.items():
    PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
    print(f"Extracted normalisation of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}")




# retrieve the parameter indices
#print(f"\n{par_name_dict}")
bb = bestfit_pars[1:par_name_dict["jpsimunu_sig_y"]]
print(bb)
print()
print(c("\nScaled constraints: MISID").green.underline)
print(c("Central value of misID constraint:").yellow, scaled_misid_y_constraint.n)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n + scaled_misid_y_constraint.s)
print(c("misID constraint @ +1\u03C3:").yellow, scaled_misid_y_constraint.n - scaled_misid_y_constraint.s)
print(c("\nFit results: MISID").green.underline)
print(c("Extracted misID parameter of interest :").red, PRESCALED_FIT_RESULTS["misid_y_constr"].n)
print(c("Fitted MisID yield w/o BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False ))/muprime_vals["misid"].sum())
print(c("Fitted MisID yield x BB:").red.bold, np.sum(misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb)/muprime_vals["misid"].sum())


os.system(f"mkdir -p {PLOTS_PATH}/{MODE}/{YEAR}/1D")
OBS = load_bh("sig")
    
"""generate 1D projections of the fit and data
pars:
    - bh_projection_axis = which axes to project form nD bh
    - plot_tot_fit_model = if plot tot fit model overlayed on the fit components
""" # sloppy to do it this way, but that's okay sometimes

# load in the OBS observed data and compile all scalings, vars and errors of interest
obs_mcorr_s_i = OBS.view().variance / OBS.view().value
obs_mcorr_vals = OBS.view().value / obs_mcorr_s_i
obs_mcorr_errs = (OBS.view().variance**.5) / obs_mcorr_s_i
obs_mcorr_bin_centers = OBS.axes[0].centers
obs_mcorr_bin_widths =  OBS.axes[0].widths
print(c(f"scaled data integral: {np.sum(obs_mcorr_vals)}").magenta)

# compile the overall model
proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)

# book plot
fig, ax = plt.subplots(2,1,figsize=(13,8),sharex=True,gridspec_kw={'height_ratios': [5, 1]})
#fig, ax0 = plt.subplots(1,1,figsize=(9,7))

# plot the data first
ax0 = ax[0]

ax0.plot([], [], "", label=r"\textbf{LHCb Unofficial}", color="white")
ax0.errorbar(x=obs_mcorr_bin_centers, y=obs_mcorr_vals, yerr=obs_mcorr_errs, xerr=obs_mcorr_bin_widths/2, 
fmt=".", 
color="black", 
label=f"LHCb {YEAR} Data", 
markersize=5,
elinewidth=1.5,
capsize=1.5,
markeredgewidth=1.5,
)


ax0.set_ylabel(rf"Scaled events / {{{obs_mcorr_bin_widths[0]:.0f}}} MeV$/c^2$", fontsize=25, loc="center")

# # combinatorial bkg
mcorr_comb_bkg = muprime_vals["comb"] * PRESCALED_FIT_RESULTS["comb_y"].n * bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_comb_bkg, bottom=[0], width=obs_mcorr_bin_widths, alpha=.8, label=r"Combinatorial",color="#081d58")

# signal
mcorr_sig = signal_Bc2JpsiMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["jpsimunu_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n ], include_auxdata=False )*bb
mcorr_sig_err = signal_Bc2JpsiMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["jpsimunu_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].s ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_sig, bottom=(mcorr_comb_bkg), width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow J/\psi \mu^+\nu_\mu$", color="darkorange")

# misid background
mcorr_misid_bkg = misid_template.expected_data( [ PRESCALED_FIT_RESULTS["misid_y_constr"].n], include_auxdata=False )*bb
#mcorr_misid_bkg = muprime_vals["misid"] * PRESCALED_FIT_RESULTS["misid_y_constr"].n * bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_misid_bkg, bottom=mcorr_comb_bkg+mcorr_sig, width=obs_mcorr_bin_widths, alpha=.7, label=r"MisID", color="#225ea8")

# chic
#mcorr_chic = Bc2ChiCMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["chic_intgr"].n, PRESCALED_FIT_RESULTS["jpsichicmunu_y"].n ], include_auxdata=False )*bb
mcorr_chic = Bc2ChiCMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["chic_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n, PRESCALED_FIT_RESULTS["chic_constr"].n ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_chic, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig), width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow \chi_{c} \mu^+ \nu_\mu$", color="mediumaquamarine")

# chic2
#mcorr_chic2 = Bc2ChiC2MuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["chic2_intgr"].n, PRESCALED_FIT_RESULTS["jpsichicmunu_y"].n ], include_auxdata=False )*bb
mcorr_chic2 = Bc2ChiC2MuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["chic2_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n, PRESCALED_FIT_RESULTS["chic_constr"].n ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_chic2, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic), width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow \chi_{c2} \mu^+ \nu_\mu$", color="firebrick")

# tau mode
mcorr_tau = Bc2JpsiTauNu_template.expected_data( [ PRESCALED_FIT_RESULTS["jpsitaunu_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n, PRESCALED_FIT_RESULTS["r_jpsi"].n ], include_auxdata=False )*bb
mcorr_tau_err = Bc2JpsiTauNu_template.expected_data( [ PRESCALED_FIT_RESULTS["jpsitaunu_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].s, PRESCALED_FIT_RESULTS["r_jpsi"].s ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_tau, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic+mcorr_chic2), width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow J/\psi \tau^+\nu_\mu$", color="deepskyblue")

# psi2S mode
mcorr_psi2S = Bc2Psi2SMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["psi2Smunu_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n, PRESCALED_FIT_RESULTS["psi2Smunu_constr"].n ], include_auxdata=False )*bb
#mcorr_psi2S = Bc2Psi2SMuNu_template.expected_data( [ PRESCALED_FIT_RESULTS["psi2Smunu_intgr"].n, PRESCALED_FIT_RESULTS["psi2Smunu_y"].n ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_psi2S, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic+mcorr_chic2+mcorr_tau), width=obs_mcorr_bin_widths, alpha=.7, label=r"$B_c^+ \rightarrow \psi(2S) \mu^+\nu_\mu$", color="purple")

# # double-charm 2 body
# mcorr_2body = Bc2JpsiDx_template.expected_data( [ PRESCALED_FIT_RESULTS["dc_2body_intgr"].n, PRESCALED_FIT_RESULTS["jpsidx_y"].n ], include_auxdata=False )*bb
# ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_2body, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic+mcorr_chic2+mcorr_tau+mcorr_psi2S), width=obs_mcorr_bin_widths, alpha=.7, label=r"Double charm, two body", color="purple")

# # double-charm quasi-2 body
# mcorr_quasi2body = Bc2JpsiDsx_template.expected_data( [ PRESCALED_FIT_RESULTS["dc_quasi2body_intgr"].n, PRESCALED_FIT_RESULTS["jpsidx_y"].n ], include_auxdata=False )*bb
# ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_quasi2body, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic+mcorr_chic2+mcorr_tau+mcorr_psi2S+mcorr_2body), width=obs_mcorr_bin_widths, alpha=.7, label=r"Double charm, quasi-two body", color="pink")

# double-charm quasi-2 body
#mcorr_doublecharm = doublecharm_template.expected_data( [ PRESCALED_FIT_RESULTS["doublecharm_intgr"].n, PRESCALED_FIT_RESULTS["jpsimunu_sig_y"].n, PRESCALED_FIT_RESULTS["doublecharm_constr"].n ], include_auxdata=False )*bb
mcorr_doublecharm = doublecharm_template.expected_data( [ PRESCALED_FIT_RESULTS["doublecharm_intgr"].n, PRESCALED_FIT_RESULTS["doublecharm_y"].n ], include_auxdata=False )*bb
ax0.bar(x=obs_mcorr_bin_centers, height=mcorr_doublecharm, bottom=(mcorr_comb_bkg+mcorr_misid_bkg+mcorr_sig+mcorr_chic+mcorr_chic2+mcorr_tau+mcorr_psi2S), width=obs_mcorr_bin_widths, alpha=.7, label=r"Double charm $B_c^+ \rightarrow J/\psi D X$", color="pink")

# plot total fit mode
ax0.errorbar(
    x=obs_mcorr_bin_centers, 
    y=proj_fit, 
    xerr=obs_mcorr_bin_widths/2, 
    fmt=".", 
    color="tab:blue", 
    label="Total fit model", 
    elinewidth=2., 
    markersize=.1,
    capsize=.1,
    markeredgewidth=.1,
)


ax0.legend(fontsize=17, bbox_to_anchor=(1.05, 1), loc='upper left')

#pull plot (missing errors on pulls)
ax1 = ax[1]
ax[1].set_ylim(bottom=-3.5, top=3.5)
ax[1].axhline(y=-3, color="tab:red", lw=1, ls="--")
ax[1].axhline(y=3, color="tab:red", lw=1, ls="--")
ax[1].set_yticks([-3, 0,  3], minor=False)
PULLS = (obs_mcorr_vals - proj_fit) / obs_mcorr_errs  

ax1.bar(x=obs_mcorr_bin_centers, height=PULLS, width=obs_mcorr_bin_widths, alpha=1.,  color="black")

ax1.set_ylabel(rf"Pulls $[\sigma]$", loc="center", fontsize=25)

fig.tight_layout()
plt.xlabel(r"$m_{corr}(J/\psi \mu^+)$   [MeV$/c^2$]", loc="center", fontsize=25)
plt.savefig(f"{PLOTS_PATH}/{MODE}/{YEAR}/1D/fit.pdf")
plt.savefig(f"{PLOTS_PATH}/{MODE}/{YEAR}/1D/fit.png")
#plt.savefig(f"{PLOTS_PATH}/{MODE}/{YEAR}/1D/fit.eps")

# print fit results, removing the s_i precaling
signal_yield = np.sum( mcorr_sig )/ norm_scale_factor
signal_err = np.sum( mcorr_sig_err )/ norm_scale_factor
tau_yield = np.sum( mcorr_tau )/ norm_scale_factor
tau_err = np.sum( mcorr_tau_err )/ norm_scale_factor
headers= [
    "Channel", "Fit resuts (physical events)"
]
table = [
    [f"Bc->JpsiMuNu", f"{signal_yield:.0f} +/- {signal_err:.0f}"],
    [f"Bc->JpsiTauNu", f"{tau_yield:.0f} +/- {tau_err:.0f}"],
]
print()
print(tabulate(table, headers, tablefmt="fancy_grid"))

print(c(f"\nCompatibility check of R(jpsi)").bold)
print(c(f"N.B. this is with a naive estimate of the error on the yields"))
fitted_muonic_y = ufloat(signal_yield, signal_err)
fitted_tauonic_y = ufloat(tau_yield, tau_err)

print(c(f"Ratio of tau/mu yields : {fitted_tauonic_y/fitted_muonic_y}").yellow)
print(c(f"efficiency-corrected RR_jpsi constraint = {r_jpsi.n} +/- {r_jpsi.s}").red)


print(c(f"\nOther numbers of interest (physical events)").bold)

psi2S_y = np.sum(mcorr_psi2S)/norm_scale_factor
chic_y = np.sum(mcorr_chic)/norm_scale_factor
chic2_y = np.sum(mcorr_chic2)/norm_scale_factor
doublecharm_y = np.sum(mcorr_doublecharm)/norm_scale_factor

print(c(f"psi2S yield : {psi2S_y}, ratio to signal: {psi2S_y/signal_yield}"))
print(c(f"chic yield : {chic_y}, ratio to signal: {chic_y/signal_yield}"))
print(c(f"chic2 yield : {chic2_y}, ratio to signal: {chic2_y/signal_yield}"))
print(c(f"doublecharm yield : {doublecharm_y}, , ratio to signal: {doublecharm_y/signal_yield}"))