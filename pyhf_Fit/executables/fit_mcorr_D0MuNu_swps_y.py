'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

=== Backend to the combinations of years of data taking ===

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from os import ttyname
from common.header import *
from termcolor2 import c
import pprint 

parser = ArgumentParser()
parser.add_argument('-y','--year', required=True, help='Year of data taking [2011,2012,2015,2016,2017,2018]')
parser.add_argument('-s','--simfit', action="store_true", help="engage in generating a schema with lumi scaled to tthe fraction of the total 2016-2018 nominal integrated luminosity")
opts = parser.parse_args()

print(opts.simfit)

import boost_histogram as BH
def load_bh(idx):
    indf = rfiles[idx]
    samples = [np.array(indf[FITVARS[0]])]
    
    if idx=="MISID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="COMB":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="CF" or idx=="DCS":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1  = fit_config[FITVARS[0]]["nbins"]
    _minvar_1 = fit_config[FITVARS[0]]["range"][0] 
    _maxvar_1 = fit_config[FITVARS[0]]["range"][-1] 

    if fit_config[FITVARS[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[FITVARS[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[FITVARS[0]]["range"], underflow=False, overflow=False )
    if idx=="D0_MC" or idx=="Dst_MC":
        wbh = BH.Histogram( ax_1 )
        wbh.fill( *samples, )
    else:
        wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
        wbh.fill( *samples, weight=wts )    
    
    return wbh

def compute_y_eff(eff_dict, mode, year):
    mag_up = ufloat(eff_dict[mode][year]["MU"]["eff"], eff_dict[mode][year]["MU"]["err"])
    mag_down = ufloat(eff_dict[mode][year]["MD"]["eff"], eff_dict[mode][year]["MD"]["err"])
    return (mag_up+mag_down)/2.

# assign arg 
YEAR = opts.year

# binning
# -------
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json") as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").magenta)
print(c(fit_config).magenta)


# luminosity
# ----------
# used to float the shared signal normalisation across all years; scaling provided across 2016-2018 simply by ratio of integrated lumis
if opts.simfit:
    lumi_factors = {
        "2018" : 2.1/(2.1+1.7+1.6), # baseline, 2.1 invfb
        "2017" : 1.7/(2.1+1.7+1.6),
        "2016" : 1.6/(2.1+1.7+1.6),
    }
    assert(np.sum(list(lumi_factors.values()))==1.0)
else: 
    lumi_factors = {
        f"{YEAR}" : 1.0
    }
print(c(f"\nLumi sig scaling: {lumi_factors}\n").red)


# total efficiencies
# ------------------
# load from json
eff_file = "/usera/delaney/private/effs/InclusiveVubVcb/Efficiencies/jsons/Total.json"
eff_data = open(eff_file)
_effs = json.load(eff_data)
# compute polarity-averaged total efficiency, per mode 
eff_D0MuNu = compute_y_eff(_effs, mode="Bc2D0MuNu", year=YEAR)
eff_D0gMuNu = compute_y_eff(_effs, mode="Bc2D0gMuNu", year=YEAR)
eff_D0pi0MuNu = compute_y_eff(_effs, mode="Bc2D0pi0MuNu", year=YEAR)


# load in the pkl'd sW'd [all phase space] dataframes for the desired year
misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/MisID.pkl")
comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/EvtMix.pkl")
dcs_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/DCS.pkl")
cf_df    = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/CF.pkl")

# check the correct sign combination
assert((dcs_df["K_minus_ID"]*dcs_df["Mu_plus_ID"]).all()>0.0)
assert((misid_df["K_minus_ID"]*misid_df["Mu_plus_ID"]).all()>0.0)
assert(np.all(cf_df["K_minus_ID"]*cf_df["Mu_plus_ID"]<0.0))

# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(misid_df["misid_w"]*misid_df["sw"]), np.sqrt(np.sum( (misid_df["misid_w"]*misid_df["sw"])**2  )) ) 

# a big ugly, from porting from dev.ipynb
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])

print(c(f"nBins = {TOT_BINS}").yellow)

# track components (might want to remove this in tidier version of code)
rfiles = {}
rfiles["DCS"] = dcs_df
rfiles["CF"] = cf_df
rfiles["MISID"] = misid_df
rfiles["COMB"] = comb_df

# load the boost objects encoding CF, DCS, MISID, COMB and MC templates
data_driven_samples = ["DCS", "CF", "MISID", "COMB"]
MC_samples = ("D0_MC", "Dst_MC")
fit_components = ("CF", "COMB", "MISID", "D0_MC", "Dst_MC")

# load seperately-generated MC hists
print("\nLoading simulates hists")
MC_hists = {}
with open(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/{YEAR}/D0_MC.pkl", "rb") as f_dz: 
    MC_hists["D0_MC"] = pickle.load(f_dz)
    print("D0 MC: done")
with open(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/{YEAR}/Dst_MC.pkl", "rb") as f_dst: 
    MC_hists["Dst_MC"] = pickle.load(f_dst)
    print("Dst MC: done")


# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
print("\nLoading data-driven hists:")
for comp in data_driven_samples: 
    bh = load_bh(comp)
    template_vals[f"{comp}"] = bh.view().value.flatten()
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten() # weighted data-driven histograms; note how error and not variance is stored
    print(f"{comp}: done")

# MC has a regular storage
for sim in MC_samples:
    template_vals[f"{sim}"] = MC_hists[sim].view().flatten()
    template_errs[f"{sim}"] = np.sqrt( MC_hists[sim].view().flatten() ) # poisson error; note how the error and not variance is stored

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)

#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert(template_vals["DCS"].all()>0)

# prepare samples: derive scaling factor from observations
s_i = template_errs["DCS"]**2/template_vals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done
assert(s_i.all() == (load_bh("DCS").view().variance/load_bh("DCS").view().value).all()) # check that the error was stored in template_errs, and not variance

# scaled observed data
obs = template_vals["DCS"]/s_i

# scale the rest accordingly
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations) [note how normalise to the binned template area before B&Z-scaling]
for comp in fit_components:
    muprime_vals[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_errs[f"{comp}"] / s_i 

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["DCS"]) # n->n'

# B+
# from PDG, get the BFs
CF = ufloat(3.950e-2, 0.031e-2)
DCS = ufloat(1.50e-4, 0.07e-4)

# scale factor for CF_y -> B+_y constraint in the fit 
R = DCS/CF
# compute the error as sqrt(sum(w**2))
CF_n = load_bh("CF").sum().value
CF_s = load_bh("CF").sum().variance**.5 
CF_y = ufloat( CF_n, CF_s )
bu_y_constraint = CF_y * R
s_i_scaled_bu_y_constraint = bu_y_constraint * norm_scale_factor 
# need to take into account pdf not normed to 1, owing to B&Z; in other words, norm * B&Z-scaled template = B&Z-scaled constraint
scaled_bu_y_constraint = s_i_scaled_bu_y_constraint / muprime_vals["CF"].sum()

# read in the unscaled misid constraint; again, the constraint needs to be derived for a pdf not normalised to unity, owing to B&Z
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["MISID"].sum()

print(c(f"\nContraints on the B&Z-scaled B+ and MisID templates successfully derived").yellow)

#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema...").green)

# === constrain D0/D* yields from theory and effs ===
# CALCULATION BY MELIC ET AL, arXiv:1909.01213
BF_Bc2D0MuNu_3ptSR = ufloat(2.4e-5, 0.4e-5)
BF_Bc2DstMuNu_3ptSR = ufloat(7e-5, 3e-5)

# take weighted averege for D* efficiencies
Dst_toD0g_BF = ufloat(0.353, 0.009)
Dst_toD0pi0_BF = ufloat(0.647, 0.009)

# compute weighted average 
eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)

print(c(f"\n==================================================").cyan)
print(c(f"Ratio of BF: D0/D* = {BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR}\n").cyan)
print(c(f"Extracted weighted average for Dst efficiency = {eff_DstMuNu}").cyan)
print(c(f"D0 efficiency = {eff_D0MuNu}").cyan)

# N_D0/NDst = [BF(D0)*eff(D0)] / [BF(D*)*eff(D*)]; factorise into a kappa theory factor (common to all years) and year-specific efficiency ratio
kappa = (BF_Bc2D0MuNu_3ptSR) / (BF_Bc2DstMuNu_3ptSR) # gaussian-constrained scaling: espress D0 as a function of Dst
eff_ratio = eff_D0MuNu / eff_DstMuNu
print(c(f"Efficiency ratio : D0/D*= {eff_ratio}").cyan)


print(c(f"\nKappa factor from 3ptSR, efficiency-corrected (2016) = {kappa*eff_ratio:.4f}").blue)
print(c(f"==================================================\n").cyan)

# moving onto the spec: first formulate the standalone samples 
Dst_sample = {
                    "name": f"signal_DstMuNu_{YEAR}",
                    "data": ( muprime_vals["Dst_MC"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": f"dst_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Dst_MC"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        ]
}

D0_sample = {
                    "name": f"signal_D0MuNu_{YEAR}",
                    "data": ( muprime_vals["D0_MC"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": f"dz_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["D0_MC"].tolist() }, # branch-speficific per-bin variation
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        # INTRODUCE CONSTRAINT BY MELIC ET AL, arXiv:1909.01213
                        {"name": f"kappa", "type": "normsys", "data": {"lo" : kappa.n-kappa.s, "hi": kappa.n+kappa.s} }, # gaussian constraint of prediction of ratios of signal BFs; shared across all years
                        # factor in eff ratio of D0/D*, specific to year of data taking
                        {"name": f"sig_eff_ratio_{YEAR}", "type": "normsys", "data": {"lo" : eff_ratio.n-eff_ratio.s, "hi": eff_ratio.n+eff_ratio.s} }, # gaussian-constrained ratio of D0/D* efficiencies
                        ]
}

comb_sample = {
                    "name": f"combinatorial_{YEAR}",
                    "data": muprime_vals["COMB"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                        ]
}

misid_sample = {  
                    "name": f"misid_{YEAR}",
                    "data": muprime_vals["MISID"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None}, # freely floating
                        ]
                    }

bu_sample = {  
                    "name": f"bu_{YEAR}",
                    "data": muprime_vals["CF"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                        # only gaussian constraint in the fit model
                        {"name": f"bu_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                        ]
                    }


# ===========================================================
#                     BUILD THE MODEL                       #
# ===========================================================
spec = {
    "channels": [
        {
            "name": f"singlechannel_{YEAR}",
            "samples": [
                D0_sample,
                Dst_sample,
                comb_sample,
                bu_sample, 
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": f"singlechannel_{YEAR}",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": f"sig_y_extraction",
            "config": {
                "poi": "dst_sig_y",
                "parameters": [
                    # bounds on floatig normalisations 
                    {"name":f"dst_sig_y", "bounds": [[ 1e-6, 1e5 ]], "inits":[1e3]},
                    {"name":f"sig_lumi_factor_{YEAR}", "inits": [lumi_factors[YEAR]], "fixed":True},
                    {"name":f"comb_y_{YEAR}", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e3]},
                    {"name":f"dst_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["Dst_MC"])], "fixed":True}, # fix template normaliation
                    {"name":f"dz_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["D0_MC"])], "fixed":True}, # fix template normalisation
                    {"name":f"misid_y_constr_{YEAR}", "bounds":   [[ 1e-6, obs.sum() ]], "inits":[1e4]},
                ]
            }
        }
    ],
    "version": "1.0.0"
}


# =================================================================================================
#               Write schema; this will be merged with other years of data taking                 #
# =================================================================================================
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json"
if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
with open(SCHEMA,'w') as outfile:
    outfile.write(spec)

print(c(f"SCHEMA written for {YEAR} successfully to /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json").green)


# ===========================================================================
#               Write specific templates for each component                 #
# ===========================================================================
# B+
bu_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"bu_{YEAR}",
                  "data": muprime_vals["CF"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": f"bu_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                      {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                  ]
              }          
          ]
      }
    ]
  }

# misid
misid_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"misid_{YEAR}",
                  "data": muprime_vals["MISID"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None }, # freely floating
                      {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                  ]
              }         
          ]
      }
    ]
  }

Dst_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples": [
                    {
                    "name": f"signal_DstMuNu_{YEAR}",
                    "data": ( muprime_vals["Dst_MC"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": f"dst_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["Dst_MC"].tolist() }, # branch-speficific per-bin variation
                    ]
                    }
                ]
            }
        ]
    }

D0_template = {
        "channels": [
            {
                "name": "singlechannel",
                "samples":[
                    {
                    "name": f"signal_D0MuNu_{YEAR}",
                    "data": ( muprime_vals["D0_MC"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": f"dz_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {"name": "dst_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        # INTRODUCE CONSTRAINT BY MELIC ET AL, arXiv:1909.01213
                        {"name": f"kappa", "type": "normsys", "data": {"lo" : kappa.n-kappa.s, "hi": kappa.n+kappa.s} }, # gaussian constraint of prediction of ratios of signal BFs; shared across all years
                        # factor in eff ratio of D0/D*, specific to year of data taking
                        {"name": f"sig_eff_ratio_{YEAR}", "type": "normsys", "data": {"lo" : eff_ratio.n-eff_ratio.s, "hi": eff_ratio.n+eff_ratio.s} }, # gaussian-constrained ratio of D0/D* efficiencies
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["D0_MC"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }
                ]
            }
        ]
    }

# evt mix
comb_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"combinatorial_{YEAR}",
                  "data": muprime_vals["COMB"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                      {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                  ]
              }          
          ]
      }
    ]
  }


# write to file    
bu_spec = str(json.dumps(bu_template, indent=4)).replace("None", "null").replace("True", "true")
bu_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_bu_{YEAR}.json"
with open(bu_SCHEMA,'w') as outfile:
    outfile.write(bu_spec)

misid_spec = str(json.dumps(misid_template, indent=4)).replace("None", "null").replace("True", "true")
misid_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_misid_{YEAR}.json"
with open(misid_SCHEMA,'w') as outfile:
    outfile.write(misid_spec)

dst_spec = str(json.dumps(Dst_template, indent=4)).replace("None", "null").replace("True", "true")
dst_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_dst_{YEAR}.json"
with open(dst_SCHEMA,'w') as outfile:
    outfile.write(dst_spec)

dz_spec = str(json.dumps(D0_template, indent=4)).replace("None", "null").replace("True", "true")
dz_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_dz_{YEAR}.json"
with open(dz_SCHEMA,'w') as outfile:
    outfile.write(dz_spec)

comb_spec = str(json.dumps(comb_template, indent=4)).replace("None", "null").replace("True", "true")
comb_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_comb_{YEAR}.json"
with open(comb_SCHEMA,'w') as outfile:
    outfile.write(comb_spec)