'''Fitting code: EML binned fit implementing B&Z (arxiv:1309.1287)

=== Backend to the combinations of years of data taking ===

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

from os import ttyname
from common.header import *
from termcolor2 import c
import pprint 

parser = ArgumentParser()
parser.add_argument('-y','--year', required=True, help='Year of data taking [2011,2012,2015,2016,2017,2018]')
parser.add_argument('-s','--simfit', action="store_true", help="engage in generating a schema with lumi scaled to tthe fraction of the total 2016-2018 nominal integrated luminosity")
opts = parser.parse_args()

import boost_histogram as BH
def load_bh(idx):
    indf = rfiles[idx]
    samples = [np.array(indf[FITVARS[0]])]
    
    if idx=="MISID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="COMB":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="CF" or idx=="DCS":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1  = fit_config[FITVARS[0]]["nbins"]
    _minvar_1 = fit_config[FITVARS[0]]["range"][0] 
    _maxvar_1 = fit_config[FITVARS[0]]["range"][-1] 
    
    print(f"FIT CONFIG: {_nbins_1}, [{_minvar_1}, {_maxvar_1}]")

    if fit_config[FITVARS[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[FITVARS[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[FITVARS[0]]["range"], underflow=False, overflow=False )
    if idx=="D0_MC" or idx=="Dst_MC" or idx=="incl_sig":
        wbh = BH.Histogram( ax_1 )
        wbh.fill( *samples, )
    else:
        wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
        wbh.fill( *samples, weight=wts )    
    
    return wbh

def compute_y_eff(eff_dict, mode, year):
    mag_up = ufloat(eff_dict[mode][year]["MU"]["eff"], eff_dict[mode][year]["MU"]["err"])
    mag_down = ufloat(eff_dict[mode][year]["MD"]["eff"], eff_dict[mode][year]["MD"]["err"])
    return (mag_up+mag_down)/2.

# assign arg 
YEAR = opts.year

# binning
# -------
with open("/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json") as config_file:
    fit_config = json.load(config_file)
print(c(f"\nLoaded fit configuration:").magenta)
print(c(fit_config).magenta)

# extract year efficiency of the inclusive sample
# -----------------------------------------------
# read in the json file with total efficiency
with open('/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Total.json') as f_in:
    tot_eff_dict = json.load(f_in)

sig_eff_dict = {}
for  y in ["2016", "2017", "2018"]:
    D0g_MU_eff = ufloat(tot_eff_dict["Bc2D0gMuNu"][y]["MU"]["eff"],tot_eff_dict["Bc2D0gMuNu"][y]["MU"]["err"])
    D0g_MD_eff = ufloat(tot_eff_dict["Bc2D0gMuNu"][y]["MD"]["eff"],tot_eff_dict["Bc2D0gMuNu"][y]["MD"]["err"])
    D0pi0_MU_eff = ufloat(tot_eff_dict["Bc2D0pi0MuNu"][y]["MU"]["eff"],tot_eff_dict["Bc2D0pi0MuNu"][y]["MU"]["err"])
    D0pi0_MD_eff = ufloat(tot_eff_dict["Bc2D0pi0MuNu"][y]["MD"]["eff"],tot_eff_dict["Bc2D0pi0MuNu"][y]["MD"]["err"])
    D0_MU_eff = ufloat(tot_eff_dict["Bc2D0MuNu"][y]["MU"]["eff"],tot_eff_dict["Bc2D0MuNu"][y]["MU"]["err"])
    D0_MD_eff = ufloat(tot_eff_dict["Bc2D0MuNu"][y]["MD"]["eff"],tot_eff_dict["Bc2D0MuNu"][y]["MD"]["err"])

    # take the average of per-pol eff
    eff_D0gMuNu = (D0g_MU_eff+D0g_MD_eff)/2
    eff_D0pi0MuNu = (D0pi0_MU_eff+D0pi0_MD_eff)/2
    eff_D0MuNu = (D0_MU_eff+D0_MD_eff)/2

    # === constrain D0/D* yields from theory and effs ===
    # CALCULATION BY MELIC ET AL, arXiv:1909.01213
    BF_Bc2D0MuNu_3ptSR = ufloat(2.4e-5, 0.4e-5)
    BF_Bc2DstMuNu_3ptSR = ufloat(7e-5, 3e-5)

    # take weighted averege for D* efficiencies
    Dst_toD0g_BF = ufloat(0.353, 0.009)
    Dst_toD0pi0_BF = ufloat(0.647, 0.009)

    # compute weighted average 
    eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)

    print(c(f"\n==================================================").cyan)
    print(c(f"Ratio of BF: D0/D* = {BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR}\n").cyan)
    print(c(f"Extracted weighted average for Dst efficiency = {eff_DstMuNu}").cyan)
    print(c(f"D0 efficiency = {eff_D0MuNu}").cyan)

    # N_D0/NDst = [BF(D0)*eff(D0)] / [BF(D*)*eff(D*)]; factorise into a kappa theory factor (common to all years) and year-specific efficiency ratio
    kappa = (BF_Bc2D0MuNu_3ptSR) / (BF_Bc2DstMuNu_3ptSR) # gaussian-constrained scaling: espress D0 as a function of Dst
    eff_ratio = eff_D0MuNu / eff_DstMuNu
    sig_rel_frac = kappa*eff_ratio # D0/D*
    print(c(f"Efficiency ratio : D0/D*= {eff_ratio}").cyan)

    print(c(f"\nKappa factor from 3ptSR, efficiency-corrected (2016) = {kappa*eff_ratio:.4f}").blue)

    # now put everything together
    # eff = (sig_rel_frac) x D0 + (1-sig_rel_frac) x D*
    incl_sig_eff = (sig_rel_frac * eff_D0MuNu) + (1-sig_rel_frac)*eff_DstMuNu
    print(c(f"\n=> Put everything together: kappa_eff x D0 + (1 - kappa_eff) x D*: {incl_sig_eff:.4f}").blue.bold)
    print(c(f"==================================================\n").cyan) 
    sig_eff_dict[y] = incl_sig_eff

# luminosity, efficiency-corrected
# --------------------------------
# used to float the shared signal normalisation across all years; scaling provided across 2016-2018 simply by ratio of integrated lumis
eff_lumi_raw = {
        "2018" : 2.1,#*sig_eff_dict["2018"], # baseline, 2.1 invfb
        "2017" : 1.7,#*sig_eff_dict["2017"],
        "2016" : 1.6,#*sig_eff_dict["2016"],
    }
NORMALISATION = np.sum(list(eff_lumi_raw.values()))

if opts.simfit:
    lumi_factors = {
        #"2018" : (2.1*sig_eff_dict["2018"]) / NORMALISATION, 
        #"2017" : (1.7*sig_eff_dict["2017"]) / NORMALISATION, 
        #"2016" : (1.6*sig_eff_dict["2016"]) / NORMALISATION, 
        "2018" : (2.1) / NORMALISATION, 
        "2017" : (1.7) / NORMALISATION, 
        "2016" : (1.6) / NORMALISATION, 
    }
    print(np.sum(list(lumi_factors.values())))
else: 
    lumi_factors = {
        f"{YEAR}" : 1.0
    }
print(c(f"\nLumi sig scaling [efficiency-corrected]: {lumi_factors[YEAR]}\n").red)


# total efficiencies
# ------------------
# load from json
eff_file = "/usera/delaney/private/effs/InclusiveVubVcb/Efficiencies/jsons/Total.json"
eff_data = open(eff_file)
_effs = json.load(eff_data)
# compute polarity-averaged total efficiency, per mode 
eff_D0MuNu = compute_y_eff(_effs, mode="Bc2D0MuNu", year=YEAR)
eff_D0gMuNu = compute_y_eff(_effs, mode="Bc2D0gMuNu", year=YEAR)
eff_D0pi0MuNu = compute_y_eff(_effs, mode="Bc2D0pi0MuNu", year=YEAR)


# load in the pkl'd sW'd [all phase space] dataframes for the desired year
misid_df = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/MisID.pkl")
comb_df  = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/EvtMix.pkl")
dcs_df   = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/DCS.pkl")
cf_df    = pandas.read_pickle(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu/{YEAR}/CF.pkl")

# check the correct sign combination
assert((dcs_df["K_minus_ID"]*dcs_df["Mu_plus_ID"]).all()>0.0)
assert((misid_df["K_minus_ID"]*misid_df["Mu_plus_ID"]).all()>0.0)
assert(np.all(cf_df["K_minus_ID"]*cf_df["Mu_plus_ID"]<0.0))

# derive a yeild constraint on misID
MISID_CONSTRAINT = ufloat( np.sum(misid_df["misid_w"]*misid_df["sw"]), np.sqrt(np.sum( (misid_df["misid_w"]*misid_df["sw"])**2  )) ) 

# a big ugly, from porting from dev.ipynb
FITVARS = list(fit_config.keys())
N_FITVARS = len(FITVARS)
TOT_BINS = 1
for i in range(N_FITVARS):
    TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])

print(c(f"nBins = {TOT_BINS}").yellow)

# track components (might want to remove this in tidier version of code)
rfiles = {}
rfiles["DCS"] = dcs_df
rfiles["CF"] = cf_df
rfiles["MISID"] = misid_df
rfiles["COMB"] = comb_df

# load the boost objects encoding CF, DCS, MISID, COMB and MC templates
data_driven_samples = ["DCS", "CF", "COMB"]
MC_samples = ["incl_sig", "inclb2D0X"]
fit_components = ("CF", "COMB", "MISID", "incl_sig")

# load seperately-generated MC hists
print("\nLoading simulates hists")
MC_hists = {}
with open(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/{YEAR}/incl_sig.pkl", "rb") as f_dz: 
    MC_hists["incl_sig"] = pickle.load(f_dz)
    print("Inclusive signal MC: done")

# load the inclusive b->D0X
with open(f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu/{YEAR}/inclb2D0X.pkl", "rb") as f_dz: 
    MC_hists["inclb2D0X"] = pickle.load(f_dz)
    print("Inclusive b->D0X MISID MC: done")


# store cvals and err of templates and data
template_vals = {}
template_errs = {}

# # weighted data with dedicated storage
print("\nLoading data-driven hists:")
for comp in data_driven_samples: 
    bh = load_bh(comp)
    template_vals[f"{comp}"] = bh.view().value.flatten()
    template_errs[f"{comp}"] = (bh.view().variance**.5).flatten() # weighted data-driven histograms; note how error and not variance is stored
    print(f"{comp}: done")

# MC has a regular storage
for sim in MC_samples:
    print(sim)
    if sim=="inclb2D0X":
        template_vals[f"MISID"] = MC_hists[sim].view().flatten()
        template_errs[f"MISID"] = np.sqrt( MC_hists[sim].view().flatten() ) # poisson error; note how the error and not variance is stored
    else:
        template_vals[f"{sim}"] = MC_hists[sim].view().flatten()
        template_errs[f"{sim}"] = np.sqrt( MC_hists[sim].view().flatten() ) # poisson error; note how the error and not variance is stored

# quick formatting checks 
for s in template_vals.values(): assert(len(s) == TOT_BINS)
for s in template_errs.values(): assert(len(s) == TOT_BINS)

#======================================================
#                   BOHM & ZECH                       #
#======================================================
# check that the requirement for B&Z is met: DCS->sum(w), others->mu_i
assert(template_vals["DCS"].all()>0)

# prepare samples: derive scaling factor from observations
s_i = template_errs["DCS"]**2/template_vals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done
assert(s_i.all() == (load_bh("DCS").view().variance/load_bh("DCS").view().value).all()) # check that the error was stored in template_errs, and not variance

# scaled observed data
obs = template_vals["DCS"]/s_i

# scale the rest accordingly
muprime_vals = {} # container for scaled fit components
muprime_errs = {} # contained for errors on scaled fit components

# normalise the scaled muprimes (=the scaled expectations) [note how normalise to the binned template area before B&Z-scaling]
for comp in fit_components:
    muprime_vals[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_vals[f"{comp}"] / s_i  
    muprime_errs[f"{comp}"] = 1./np.sum( template_vals[f"{comp}"] ) * template_errs[f"{comp}"] / s_i 

# now proceed to extract scale factor and B+ constraint
norm_scale_factor = np.sum(obs)/np.sum(template_vals["DCS"]) # n->n'

# B+
# from PDG, get the BFs
CF = ufloat(3.950e-2, 0.031e-2)
DCS = ufloat(1.50e-4, 0.07e-4)

# scale factor for CF_y -> B+_y constraint in the fit 
R = DCS/CF
# compute the error as sqrt(sum(w**2))
CF_n = load_bh("CF").sum().value
CF_s = load_bh("CF").sum().variance**.5 
CF_y = ufloat( CF_n, CF_s )
bu_y_constraint = CF_y * R
s_i_scaled_bu_y_constraint = bu_y_constraint * norm_scale_factor 
# need to take into account pdf not normed to 1, owing to B&Z; in other words, norm * B&Z-scaled template = B&Z-scaled constraint
scaled_bu_y_constraint = s_i_scaled_bu_y_constraint / muprime_vals["CF"].sum()

# read in the unscaled misid constraint; again, the constraint needs to be derived for a pdf not normalised to unity, owing to B&Z
raw_misid_y_constr = MISID_CONSTRAINT
s_i_scaled_misid_y_constraint = raw_misid_y_constr*norm_scale_factor 
scaled_misid_y_constraint = s_i_scaled_misid_y_constraint / muprime_vals["MISID"].sum()

print(c("\n=======================================================================").bold)
print(c(f"Contraints on the B&Z-scaled B+ and MisID templates successfully derived:").bold)
print(c("S-i B+ constraint: {:10.4f}".format(s_i_scaled_bu_y_constraint)))
print(c("S-i MisID constraint: {:10.4f}".format(s_i_scaled_misid_y_constraint)))
print(c("=======================================================================\n").bold)

#======================================================
#                     pyhf spec                       #
#======================================================
print(c(f"\nPreparing the fit schema...").green)


# moving onto the spec: first formulate the standalone samples 
signal_sample = {
                    "name": f"signal_{YEAR}",
                    "data": ( muprime_vals["incl_sig"] ).tolist(),
                    "modifiers": [
                        # need to normalise to unity the signal templates to share the Dst normfactor
                        {"name": f"sig_intgr_{YEAR}", "type": "normfactor", "data": None},
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["incl_sig"].tolist() }, # branch-speficific per-bin variation
                        {"name": "incl_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                        ]
}

comb_sample = {
                    "name": f"combinatorial_{YEAR}",
                    "data": muprime_vals["COMB"].tolist(),
                    "modifiers": [
                        {"name": f"uncorr_shapesys_{YEAR}", "type": "shapesys", "data": (muprime_vals["COMB"]/10.).tolist() }, # assign a 10% uncertainty to the template from event mixing 
                        {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                        ]
}

misid_sample = {  
                    "name": f"misid_{YEAR}",
                    "data": muprime_vals["MISID"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                        {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None}, # freely floating
                        #{"name": f"misid_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                        ]
                    }

bu_sample = {  
                    "name": f"bu_{YEAR}",
                    "data": muprime_vals["CF"].tolist(),
                    "modifiers": [
                        {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                        # only gaussian constraint in the fit model
                        # {"name": f"bu_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                        {"name": f"bu_y_constr_{YEAR}", "type": "normfactor", "data": None},
                        ]
                    }


# ===========================================================
#                     BUILD THE MODEL                       #
# ===========================================================
spec = {
    "channels": [
        {
            "name": f"singlechannel_{YEAR}",
            "samples": [
                signal_sample,
                comb_sample,
                bu_sample, 
                misid_sample
            ]
        },
    ],
    "observations": [
        {
            "name": f"singlechannel_{YEAR}",
            "data": obs.tolist(),
        },
    ],
    "measurements": [
        { 
            "name": f"sig_y_extraction",
            "config": {
                "poi": "incl_sig_y",
                "parameters": [
                    # bounds on floatig normalisations 
                    {"name":f"incl_sig_y", "bounds": [[ 0.0, obs.sum() ]], "inits":[1e2]},
                    {"name":f"sig_lumi_factor_{YEAR}", "inits": [lumi_factors[YEAR]], "fixed":True},
                    {"name":f"comb_y_{YEAR}", "bounds":   [[ 0.0, obs.sum() ]], "inits":[1e3]},
                    {"name":f"sig_intgr_{YEAR}", "inits": [1./np.sum(muprime_vals["incl_sig"])], "fixed":True}, # fix template normaliation
                    {"name":f"misid_y_constr_{YEAR}", "bounds":   [[ 0.0, obs.sum() ]], "inits":[1e3]},
                    {"name":f"bu_y_constr_{YEAR}", "inits": [ scaled_bu_y_constraint.n ], "fixed": True, "bounds":[[ 0.0, obs.sum() ]]},
                ]
            }
        }
    ],
    "version": "1.0.0"
}


# =================================================================================================
#               Write schema; this will be merged with other years of data taking                 #
# =================================================================================================
spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json"
if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
with open(SCHEMA,'w') as outfile:
    outfile.write(spec)

print(c(f"SCHEMA written for {YEAR} successfully to /usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_{YEAR}.json").green)


# ===========================================================================
#               Write specific templates for each component                 #
# ===========================================================================
# B+
bu_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"bu_{YEAR}",
                  "data": muprime_vals["CF"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      # {"name": f"bu_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_bu_y_constraint.n - scaled_bu_y_constraint.s, "hi": scaled_bu_y_constraint.n + scaled_bu_y_constraint.s} }, # gaussian constraint
                      {"name": f"bu_y_constr_{YEAR}", "type": "normfactor", "data": None }, # gaussian constraint
                      {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                  ]
              }          
          ]
      }
    ]
  }

# misid
misid_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"misid_{YEAR}",
                  "data": muprime_vals["MISID"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": f"misid_y_constr_{YEAR}", "type": "normfactor", "data": None }, # freely floating
                      #{"name": f"misid_y_constr_{YEAR}", "type": "normsys", "data": {"lo" : scaled_misid_y_constraint.n - scaled_misid_y_constraint.s, "hi": scaled_misid_y_constraint.n + scaled_misid_y_constraint.s} }, # gaussian constraint
                      # {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                  ]
              }         
          ]
      }
    ]
  }

signal_template = {
        "channels": [
            {
            "name": "singlechannel",
            "samples": [
                {
                "name": f"signal_{YEAR}",
                "data": ( muprime_vals["incl_sig"] ).tolist(),
                "modifiers": [
                    # need to normalise to unity the signal template to extract s_i-scaled norm
                    {"name": f"sig_intgr_{YEAR}", "type": "normfactor", "data": None},
                    {"name": "incl_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                    {"name": f"sig_lumi_factor_{YEAR}", "type": "normfactor", "data": None }, # scale signal to luminosity
                    {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["incl_sig"].tolist() }, # branch-speficific per-bin variation
                    ]
                }
            ]
            }
        ]
    }

# evt mix
comb_template = {
    "channels": [
      {
        "name": "singlechannel",
        "samples": [
              {  
                  "name": f"combinatorial_{YEAR}",
                  "data": muprime_vals["COMB"].tolist(),
                  "modifiers": [
                      # remove BB from this standalone template, as not shared with anything else; fold in bin-wise variations later
                      {"name": f"uncorr_shapesys_{YEAR}", "type": "shapesys", "data": (muprime_vals["COMB"]/10.).tolist() }, # assign a 10% uncertainty to the template from event mixing 
                      {"name": f"comb_y_{YEAR}", "type": "normfactor", "data": None }, # floating normalisation
                      {"name": f"beeston_barlow_{YEAR}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                  ]
              }          
          ]
      }
    ]
  }


# write to file    
bu_spec = str(json.dumps(bu_template, indent=4)).replace("None", "null").replace("True", "true")
bu_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_bu_{YEAR}.json"
with open(bu_SCHEMA,'w') as outfile:
    outfile.write(bu_spec)

misid_spec = str(json.dumps(misid_template, indent=4)).replace("None", "null").replace("True", "true")
misid_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_misid_{YEAR}.json"
with open(misid_SCHEMA,'w') as outfile:
    outfile.write(misid_spec)

signal_spec = str(json.dumps(signal_template, indent=4)).replace("None", "null").replace("True", "true")
signal_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_incl_signal_{YEAR}.json"
with open(signal_SCHEMA,'w') as outfile:
    outfile.write(signal_spec)

comb_spec = str(json.dumps(comb_template, indent=4)).replace("None", "null").replace("True", "true")
comb_SCHEMA = f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/model_spec_D0MuNu_comb_{YEAR}.json"
with open(comb_SCHEMA,'w') as outfile:
    outfile.write(comb_spec)