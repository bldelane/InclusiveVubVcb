import python.utils as utils
import boost_histogram as bh

class mode:

  def __init__(self, name, data, components, yields={}, wider_plot_range=False, binning_file=None, submodes=['']):

    self.name = name
    if wider_plot_range and not binning_file:
      print('mode',name,'__init__() : For a wider plot range you must pass a binning configuration')
      assert(0)
    if len(submodes)==0:
      print('mode',name,'__init__() : You cannot pass a submode of length zero')
      assert(0)

    # initialise members
    self.data = None
    self.components = {}
    self.yields = {}
    self.compnames = []
    self.plot_range_binning = None
    self.fit_range_binning  = None
    self.data_pr = None
    self.components_pr = {}
    self.submodes = submodes

    # two choices of initialisation
    if wider_plot_range:
      self.read_init_wide(data, components, binning_file )
    else:
      self.read_init(data, components )

    # store configuration for component yields, default type is 'frac'
    for name, obj in yields.items():
      if name not in self.components.keys():
        print('mode',name,'__init__() : WARNING : You have a specified a yield with the name', name, 'but it has not been listed as a component. It will be ignored')
    for name, obj in self.components.items():
      if name in yields.keys():
        yldarg = yields[name]
        if type(yldarg)==type(str()):
          yldarg = [yldarg]
        elif type(yldarg)==type(dict()):
          assert( sorted(yldarg.keys())==sorted(self.submodes) )
          yldarg = [ yldarg[submode] for submode in self.submodes ]
        elif type(yldarg)==type(list()):
          yldarg = yldarg
        elif type(yldarg)==type(tuple()):
          yldarg = list( yldarg )
        else:
          print('mode',name,'__init__() : ERROR : Invalid type for yield',name)
        assert( type(yldarg) == type(list()) )
        self.yields[name] = yldarg
      else:
        self.yields[name] = [ 'frac' for i in range(len(submodes)) ]

  # call this guy when histograms are passed normally
  def read_init(self, data, components):
    self.data = self.read_histogram_obj(data)
    for name, obj in components.items():
      self.components[name] = self.read_histogram_obj(obj)
      self.components_pr[name] = None
      for i in range(len(self.submodes)):
        # check same shape as data
        assert( self.components[name][i].shape == self.data[i].shape )
        # normalize to 1
        self.components[name][i] /= self.components[name][i].sum().value
        # check it is now normalized
        assert(abs(self.components[name][i].sum().value-1.)<1.e-8)
    # ordering is important so we need a list defined once
    self.compnames = sorted( self.components.keys() )

  # call this guy when requesting the wider plot range init
  def read_init_wide(self, data, components, binning_file):

    # get the binning defs
    self.plot_range_binning = utils.load_binning_hist( binning_file )
    self.fit_range_binning  = utils.load_binning_hist( binning_file, fit_bins=True )

    # get the plot range data
    self.data_pr = self.read_histogram_obj(data)
    for subdata in self.data_pr:
      assert( subdata.shape == self.plot_range_binning.shape )

    # get the fit range data
    self.data = [utils.get_fit_hist_from_plot_hist( subdata, binning_file ) for subdata in self.data_pr]
    for subdata in self.data:
      assert( subdata.shape == self.fit_range_binning.shape )

    for name, obj in components.items():
      # get the plot range components
      self.components_pr[name] = self.read_histogram_obj(obj)
      for i, subcomp in enumerate(self.components_pr[name]):
        assert( subcomp.shape == self.plot_range_binning.shape )
        assert( subcomp.shape == self.data_pr[i].shape )

      # get the fit range components
      self.components[name] = [utils.get_fit_hist_from_plot_hist( self.components_pr[name][i], binning_file ) for i in range(len(self.submodes))]
      for i, subcomp in enumerate(self.components[name]):
        assert( subcomp.shape == self.fit_range_binning.shape )
        assert( subcomp.shape == self.data[i].shape )

      # normalise both shapes to 1
      for i in range(len(self.submodes)):
        self.components[name][i] /= self.components[name][i].sum().value
        self.components_pr[name][i] /= self.components_pr[name][i].sum().value

      # check they are now normalized
      for i in range(len(self.submodes)):
        assert(abs(self.components[name][i].sum().value-1.)<1.e-8)
        assert(abs(self.components_pr[name][i].sum().value-1.)<1.e-8)

    # ordering is important so we need a list defined once
    self.compnames = sorted( self.components.keys() )

  # read histogram object
  def read_histogram_obj(self, obj):

    # object can be passed either directly as a boost_histogram
    # or as a tuple, list or dict of boost_histograms
    # or as a str to a file path
    # or as a tuple, list or dict of strs to file paths

    # we want to return a list of boost_histograms

    # if bh.Histogram return it as a list
    if type(obj)==type(bh.Histogram()):
      return [obj]

    # if str return it as list of bh.Histogram
    elif type(obj)==type(str()):
      return [ utils.readHistogram(obj) ]

    # if dict
    elif type(obj)==type(dict()):
      # check keys match submodes
      assert( sorted(obj.keys())==sorted(self.submodes) )
      # check all values are of the same type
      for item in obj.values():
        assert( type(item) == type(obj.values()[0]) )
      # if bh.Histogram return as list
      if type(obj.values()[0])==type(bh.Histogram()):
        return [ obj[submode] for submode in self.submodes ]
      # if str return as list of bh.Histogram
      elif type(obj.values()[0])==type(str()):
        return [ utils.readHistogram(obj[submode]) for submode in self.submodes ]
      else:
        print('mode',self.name,'read_histogram_obj() invalid type')
        assert(0)

    # if tuple
    elif type(obj)==type(tuple()):
      # check all values are of the same type
      for item in obj:
        assert( type(item)==type(obj[0]) )
      # if bh.Histogram return as list
      if type(obj[0])==type(bh.Histogram()):
        return list(obj)
      # if str return as list of bh.Histogram
      if type(obj[0])==type(str()):
        return [ utils.readHistogram(obj[i]) for i in range(len(self.submodes)) ]
      else:
        print('mode',self.name,'read_histogram_obj() invalid type')
        assert(0)

    # if list
    elif type(obj)==type(list()):
      # check all values are of the same type
      for item in obj:
        assert( type(item)==type(obj[0]) )
      # if bh.Histogram return as list
      if type(obj[0])==type(bh.Histogram()):
        return obj
      # if str return as list of bh.Histogram
      if type(obj[0])==type(str()):
        return [ utils.readHistogram(obj[i]) for i in range(len(self.submodes)) ]
      else:
        print('mode',self.name,'read_histogram_obj() invalid type')
        assert(0)

    else:
      print('mode',self.name,'read_histogram_obj() invalid type')
      assert(0)

  def __str__(self):
    ret_str = 'Mode('+self.name+',components=['+ ','.join(self.compnames) + ']'
    if len(self.submodes)>1: ret_str += ',submodes=['+ ','.join(self.submodes) + ']'
    ret_str += ')'
    return ret_str

  def print(self):
    print( 'Mode( name =', self.name, ',' )
    print( '      components=[' + ','.join(self.compnames) +'],')
    if len(self.submodes)>1:
      print( '      submodes=[' + ','.join(self.submodes) +'],')
      for i in range(len(self.submodes)):
        print( '      submode : ', self.submodes[i],':' )
        print( '         data   : shape='+str(self.data[i].shape)+', events='+str(self.data[0].sum().value) )
        for comp in self.compnames:
          print('        ', '{:6s}'.format(comp), ':', self.yields[comp][i],',')
    else:
      print( '      data   : shape='+str(self.data[0].shape)+', events='+str(self.data[0].sum().value) )
      for comp in self.compnames:
        print('     ', '{:6s}'.format(comp), ':', self.yields[comp][0],',')
    print('    )')


