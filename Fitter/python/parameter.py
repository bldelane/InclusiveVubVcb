import numpy as np
from uncertainties import ufloat

class parameter:
  def __init__(self, name='', value=0, error=None, limit=[None,None], fix=False, scale=1):
    self.name  = name
    if error is None: error = value/10.
    self.uval  = ufloat(value,error)
    self.limit = limit
    self.fix   = fix
    self.scale = scale

  def get_value(self, scaled=False):
    if scaled:
      return ( self.uval * self.scale ).nominal_value
    else:
      return self.uval.nominal_value

  def get_error(self, scaled=False):
    if scaled:
      return ( self.uval * self.scale ).std_dev
    else:
      return self.uval.std_dev

  def get_limit(self, scaled=False):
    if scaled:
      return (self.limit[0] * self.scale, self.limit[1] * self.scale )
    else:
      return self.limit

  def set_value(self, val, scaled=False):
    sval = val
    serr = self.get_error()
    self.set_uval( sval, serr, scaled )

  def set_error(self, err, scaled=False):
    serr = err
    sval = self.get_value()
    self.set_uval( sval, serr, scaled )

  def set_limit(self, lim, scaled=False):
    if scaled:
      self.limit[0] = lim[0]/self.scale
      self.limit[1] = lim[1]/self.scale
    else:
      self.limit[0] = lim[0]
      self.limit[1] = lim[1]

  def set_uval(self, val, err=None, scaled=False):
    if err is not None:
      if scaled:
        self.uval = ufloat(val,err)/self.scale
      else:
        self.uval = ufloat(val,err)
    else:
      if scaled:
        self.uval = ufloat(*val)/self.scale
      else:
        self.uval = ufloat(*val)

  def copy(self):
    return parameter(self.name,self.get_value(),self.get_error(), self.limit, self.fix, self.scale)

  def __repr__(self):
    return str(self)

  def __str__(self):
    ret_str = 'Parameter('+self.name
    ret_str += ',val='+str(self.get_value())
    ret_str += ',err='+str(self.get_error())
    lim0 = str(self.limit[0]) if self.limit[0] else '-inf'
    lim1 = str(self.limit[1]) if self.limit[1] else '+inf'
    ret_str += ',lim=('+lim0+','+lim1+')'
    if self.fix:   ret_str += ', fixed'
    if self.scale!=1: ret_str += ', scale='+str(self.scale)
    ret_str += ')'
    return ret_str

  def __add__(self, other):
    ret_par = self.copy()
    if hasattr(other,'uval'):
      ret_par.uval = self.uval + other.uval
    else:
      ret_par.uval = self.uval + other
    ret_par.limit = (None,None)
    return ret_par

  def __sub__(self, other):
    ret_par = self.copy()
    if hasattr(other,'uval'):
      ret_par.uval = self.uval - other.uval
    else:
      ret_par.uval = self.uval - other
    ret_par.limit = (None,None)
    return ret_par

  def __mul__(self, other):
    ret_par = self.copy()
    if hasattr(other,'uval'):
      ret_par.uval = self.uval * other.uval
    else:
      ret_par.uval = self.uval * other
    ret_par.limit = (None,None)
    return ret_par

  def __truediv__(self, other):
    ret_par = self.copy()
    if hasattr(other,'uval'):
      ret_par.uval = self.uval / other.uval
    else:
      ret_par.uval = self.uval / other
    ret_par.limit = (None,None)
    return ret_par


