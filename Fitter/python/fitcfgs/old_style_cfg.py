# config dictionaries for fitter

def simplecfg(year):

  if year not in ['2011','2012','2015','2016','2017','2018','run1','run2']:
    print('Not a valid data sample',year)
    assert(0)

  if year in ['run1','run2']: run = year
  elif year in ['2011','2012']: run = 'run1'
  elif year in ['2015','2016','2017','2018']: run = 'run2'
  else:
    print('Not a valid data sample',year)
    assert(0)

  cfg = { 'D0MuNu' :
            {
              'title'      : f'$D^{0}$ Mode',
              'binning'    : f'config/binnings/D0MuNu/nominal.json',
              'data'       : f'fit_hists/D0MuNu/nominal/{year}/sig_dcs.pkl',
              'components' : { 'bu'    : f'fit_hists/D0MuNu/nominal/{year}/sig_cf.pkl'  ,
                               'misid' : f'fit_hists/D0MuNu/nominal/{year}/misid_dcs.pkl',
                               'comb'  : f'fit_hists/D0MuNu/nominal/{year}/comb_cf.pkl',
                               'sigd0' : f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2D0MuNu.pkl',
                               'sigdst': f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2Dst0MuNu.pkl',
                             },
              'yields'     : { 'bu'    : 'abs',
                               'misid' : 'abs',
                               'comb'  : 'abs',
                               'sigd0' : 'abs',
                               'sigdst': 'abs',
                             },
              'plotcfg'    : { 'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': 'B+ Background' },
                               'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sigd0' : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'D0 Signal' },
                               'sigdst': { 'order' : 4, 'color': (0.2,0.6,1.0), 'title': 'D* Signal' },
                             },
            },
          'JpsiMuNu' :
            {
              'title'      : f'$J/\psi$ Mode',
              'binning'    : f'config/binnings/JpsiMuNu/nominal.json',
              'data'       : f'fit_hists/JpsiMuNu/nominal/{year}/sig.pkl',
              'components' : { 'misid' : f'fit_hists/JpsiMuNu/nominal/{year}/misid.pkl',
                               'comb'  : f'fit_hists/JpsiMuNu/nominal/{year}/comb.pkl',
                               'sig'   : f'fit_hists/JpsiMuNu/nominal/{run}/mc_Bc2JpsiMuNu.pkl',
                               'bu'    : f'fit_hists/JpsiMuNu/nominal/{run}/mc_Bu2JpsiX.pkl',
                             },
              'yields'     : { 'misid' : 'abs',
                               'comb'  : 'abs',
                               'sig'   : 'abs',
                               'bu'    : 'abs',
                             },
              'plotcfg'    : { 'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sig'   : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'Jpsi Signal' },
                               'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': '$B \\rightarrow J/\psi X$' },
                             },
            }
        }

  return cfg

def getcfg(year):

  if year not in ['2011','2012','2015','2016','2017','2018','run1','run2']:
    print('Not a valid data sample',year)
    assert(0)

  if year in ['run1','run2']: run = year
  elif year in ['2011','2012']: run = 'run1'
  elif year in ['2015','2016','2017','2018']: run = 'run2'
  else:
    print('Not a valid data sample',year)
    assert(0)

  cfg = { 'D0MuNu' :
            {
              'title'      : f'$D^{0}$ Mode',
              'binning'    : f'config/binnings/D0MuNu/nominal.json',
              'data'       : f'fit_hists/D0MuNu/nominal/{year}/sig_dcs.pkl',
              'components' : { 'bu'    : f'fit_hists/D0MuNu/nominal/{year}/sig_cf.pkl'  ,
                               'misid' : f'fit_hists/D0MuNu/nominal/{year}/misid_dcs.pkl',
                               'comb'  : f'fit_hists/D0MuNu/nominal/{year}/comb_cf.pkl',
                               'sigd0' : f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2D0MuNu.pkl',
                               'sigdst': f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2Dst0MuNu.pkl',
                             },
              'yields'     : { 'bu'    : 'func( ( eff_rat_bu_to_jpsi * br_rat_dcs_to_mm * ckm * JpsiMuNu:sig ) / f_c )',
                               'misid' : 'abs',
                               'comb'  : 'abs',
                               'sigd0' : 'func(eff_rat_d0_to_jpsi * br_rat_cf_to_mm * ckm * JpsiMuNu:sig)',
                               'sigdst': 'func(eff_rat_dst_to_jpsi * dst_sc * br_rat_cf_to_mm * ckm * JpsiMuNu:sig)',
                             },
              'plotcfg'    : { 'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': 'B+ Background' },
                               'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sigd0' : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'D0 Signal' },
                               'sigdst': { 'order' : 4, 'color': (0.2,0.6,1.0), 'title': 'D* Signal' },
                             },
            },
          'JpsiMuNu' :
            {
              'title'      : f'$J/\psi$ Mode',
              'binning'    : f'config/binnings/JpsiMuNu/nominal.json',
              'data'       : f'fit_hists/JpsiMuNu/nominal/{year}/sig.pkl',
              'components' : { 'misid' : f'fit_hists/JpsiMuNu/nominal/{year}/misid.pkl',
                               'comb'  : f'fit_hists/JpsiMuNu/nominal/{year}/comb.pkl',
                               'sig'   : f'fit_hists/JpsiMuNu/nominal/{run}/mc_Bc2JpsiMuNu.pkl',
                               'bu'    : f'fit_hists/JpsiMuNu/nominal/{run}/mc_Bu2JpsiX.pkl',
                             },
              'yields'     : { 'misid' : 'abs',
                               'comb'  : 'abs',
                               'sig'   : 'abs',
                               'bu'    : 'abs',
                             },
              'plotcfg'    : { 'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sig'   : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'Jpsi Signal' },
                               'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': '$B \\rightarrow J/\psi X$' },
                             },
            }
        }

  return cfg

def getsimcfg():

  cfg = { 'D0MuNu' :
            {
              'title'      : '$D^{0}$ Mode',
              'binning'    : 'config/binnings/D0MuNu/nominal.json',
              'submodes'   : ['run1','run2'],
              'subtitles'  : ['Run 1','Run 2'],
              'data'       : ['fit_hists/D0MuNu/nominal/run1/sig_dcs.pkl',
                              'fit_hists/D0MuNu/nominal/run2/sig_dcs.pkl'],
              'components' : { 'bu'    : ['fit_hists/D0MuNu/nominal/run1/sig_cf.pkl',
                                          'fit_hists/D0MuNu/nominal/run2/sig_cf.pkl'],
                               'misid' : ['fit_hists/D0MuNu/nominal/run1/misid_dcs.pkl',
                                          'fit_hists/D0MuNu/nominal/run2/misid_dcs.pkl'],
                               'comb'  : ['fit_hists/D0MuNu/nominal/run1/comb_cf.pkl',
                                          'fit_hists/D0MuNu/nominal/run2/comb_cf.pkl'],
                               'sigd0' : ['fit_hists/D0MuNu/nominal/run1/mc_Bc2D0MuNu.pkl',
                                          'fit_hists/D0MuNu/nominal/run2/mc_Bc2D0MuNu.pkl'],
                               'sigdst': ['fit_hists/D0MuNu/nominal/run1/mc_Bc2Dst0MuNu.pkl',
                                          'fit_hists/D0MuNu/nominal/run2/mc_Bc2Dst0MuNu.pkl'],
                             },
              'yields'     : { 'bu'    : ['func( (f_bcd0_to_bcjpsi * JpsiMuNu:run1:sig) / f_bcd0_to_bud0 )',
                                          'func( (f_bcd0_to_bcjpsi * JpsiMuNu:run2:sig) / f_bcd0_to_bud0 )'],
                               'misid' : ['abs','abs'],
                               'comb'  : ['abs','abs'],
                               'sigd0' : ['func(f_bcd0_to_bcjpsi * JpsiMuNu:run1:sig)',
                                         'func(f_bcd0_to_bcjpsi * JpsiMuNu:run2:sig)'],
                               'sigdst': ['func(f_bcdst_to_bcjpsi * JpsiMuNu:run1:sig)',
                                          'func(f_bcdst_to_bcjpsi * JpsiMuNu:run2:sig)'],
                             },
              #'yields'     : { 'bu'    : ['abs','abs'],
                               #'misid' : ['abs','abs'],
                               #'comb'  : ['abs','abs'],
                               #'sigd0' : ['abs','abs'],
                               #'sigdst': ['abs','abs'],
                             #},
              'plotcfg'    : { 'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': 'B+ Background' },
                               'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sigd0' : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'D0 Signal' },
                               'sigdst': { 'order' : 4, 'color': (0.2,0.6,1.0), 'title': 'D* Signal' },
                             },
            },
          'JpsiMuNu' :
            {
              'title'      : '$J/\psi$ Mode',
              'binning'    : 'config/binnings/JpsiMuNu/nominal.json',
              'submodes'   : ['run1','run2'],
              'subtitles'  : ['Run 1','Run 2'],
              'data'       : ['fit_hists/JpsiMuNu/nominal/run1/sig.pkl',
                              'fit_hists/JpsiMuNu/nominal/run2/sig.pkl'],
              'components' : { 'misid' : ['fit_hists/JpsiMuNu/nominal/run1/misid.pkl',
                                          'fit_hists/JpsiMuNu/nominal/run2/misid.pkl'],
                               'comb'  : ['fit_hists/JpsiMuNu/nominal/run1/comb.pkl',
                                          'fit_hists/JpsiMuNu/nominal/run2/comb.pkl'],
                               'sig'   : ['fit_hists/JpsiMuNu/nominal/run1/mc_Bc2JpsiMuNu.pkl',
                                          'fit_hists/JpsiMuNu/nominal/run2/mc_Bc2JpsiMuNu.pkl'],
                               'bu'    : ['fit_hists/JpsiMuNu/nominal/run1/mc_Bu2JpsiX.pkl',
                                          'fit_hists/JpsiMuNu/nominal/run2/mc_Bu2JpsiX.pkl'],
                             },
              'yields'     : { 'misid' : ['abs','abs'],
                               'comb'  : ['abs','abs'],
                               'sig'   : ['abs','abs'],
                               'bu'    : ['abs','abs'],
                             },
              'plotcfg'    : { 'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sig'   : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'Jpsi Signal' },
                               'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': '$B \\rightarrow J/\psi X$' },
                             },
            }
        }

  return cfg

def parcfg(opt='sim'):
  if opt=='run1':
    cfg = {
            'D0MuNu_comb_y'     : { 'value': 986      ,  'error':  188      ,  'limit': (500, 1e4  ) } ,
            'D0MuNu_misid_y'    : { 'value': 5.15e+04 ,  'error':  2.38e+03 ,  'limit': (1e4, 6e4  ) } ,
            'JpsiMuNu_bu_y'     : { 'value': 3.43e+04 ,  'error':  1.22e+04 ,  'limit': (3e4, 1e5  ) } ,
            'JpsiMuNu_comb_y'   : { 'value': 6.9e+04  ,  'error':  1.23e+04 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_misid_y'  : { 'value': 6.93e+04 ,  'error':  1.19e+03 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_sig_y'    : { 'value': 3.89e+03 ,  'error':  6.87e+03 ,  'limit': (1e3, 4e4  ) } ,
            'f_bcd0_to_bcjpsi'  : { 'value': 0.146    ,  'error':  0.416    ,  'limit': (0, 0.5    ) } ,
            'f_bcd0_to_bud0'    : { 'value': 0.0822   ,  'error':  0.585    ,  'limit': (0, 0.5    ) } ,
            'f_bcdst_to_bcjpsi' : { 'value': 0.502    ,  'error':  0.429    ,  'limit': (0, 0.5    ) } ,
          }
    return cfg

  elif opt=='run2':
    cfg = {
            'D0MuNu_comb_y'      : { 'value':  8.07e+03 ,  'error':  800      ,  'limit': (500 , 1e4  ) } ,
            'D0MuNu_misid_y'     : { 'value':  2.13e+05 ,  'error':  4.68e+03 ,  'limit': (1e4 , 3e5   ) } ,
            'JpsiMuNu_bu_y'      : { 'value':  2.43e+04 ,  'error':  2.35e+03 ,  'limit': (1e4 , 1e5   ) } ,
            'JpsiMuNu_comb_y'    : { 'value':  3.57e+03 ,  'error':  406      ,  'limit': (1e3 , 1e5   ) } ,
            'JpsiMuNu_misid_y'   : { 'value':  7.22e+04 ,  'error':  1.23e+03 ,  'limit': (1e4 , 1e5   ) } ,
            'JpsiMuNu_sig_y'     : { 'value':  5.87e+04 ,  'error':  728      ,  'limit': (1e3 , 1e5   ) } ,
            'eff_rat_d0_to_jpsi' : { 'value':  1.2      ,  'error':  0.01     ,  'limit': (0.8 , 1.6   ) } ,
            'eff_rat_dst_to_jpsi': { 'value':  1.2      ,  'error':  0.01     ,  'limit': (0.8 , 1.6   ) } ,
            'eff_rat_bu_to_jpsi' : { 'value':  0.6      ,  'error':  0.01     ,  'limit': (0.1 , 1.1   ) } ,
            'br_rat_cf_to_mm'    : { 'value':  0.6      ,  'error':  0.01     ,  'limit': (0.1 , 1.1   ) } ,
            'br_rat_dcs_to_mm'   : { 'value':  0.002    ,  'error':  5e-5     ,  'limit': (1e-8, 0.01  ) } ,
            'dst_sc'             : { 'value':  0.5      ,  'error':  0.5      ,  'limit': (1e-8, 2.5   ) } ,
            'f_c'                : { 'value':  0.01     ,  'error':  0.01     ,  'limit': (1e-8, 0.2   ) } ,
            'ckm'                : { 'value':  0.01     ,  'error':  1.01     ,  'limit': (1e-8, 0.2   ) } ,
          }
    return cfg
  elif opt=='sim':
    cfg = {
            'D0MuNu_run1_comb_y'    : { 'value': 986       ,  'error':  188      ,  'limit': (500, 1e4 ) } ,
            'D0MuNu_run2_comb_y'    : { 'value':  8.07e+03 ,  'error':  800      ,  'limit': (500, 1e4 ) } ,
            'D0MuNu_run1_misid_y'   : { 'value': 5.15e+04  ,  'error':  2.38e+03 ,  'limit': (1e4, 6e4  ) } ,
            'D0MuNu_run2_misid_y'   : { 'value':  2.13e+05 ,  'error':  4.68e+03 ,  'limit': (1e4, 3e5  ) } ,
            'JpsiMuNu_run1_bu_y'    : { 'value': 3.43e+04  ,  'error':  1.22e+04 ,  'limit': (3e4, 1e5  ) } ,
            'JpsiMuNu_run2_bu_y'    : { 'value':  2.43e+04 ,  'error':  2.35e+03 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_run1_comb_y'  : { 'value': 6.9e+04   ,  'error':  1.23e+04 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_run2_comb_y'  : { 'value':  3.57e+03 ,  'error':  406      ,  'limit': (1e3, 1e5  ) } ,
            'JpsiMuNu_run1_misid_y' : { 'value': 6.93e+04  ,  'error':  1.19e+03 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_run2_misid_y' : { 'value':  7.22e+04 ,  'error':  1.23e+03 ,  'limit': (1e4, 1e5  ) } ,
            'JpsiMuNu_run1_sig_y'   : { 'value': 3.89e+03  ,  'error':  6.87e+03 ,  'limit': (1e3, 4e4  ) } ,
            'JpsiMuNu_run2_sig_y'   : { 'value':  5.87e+04 ,  'error':  728      ,  'limit': (1e3, 1e5  ) } ,
            'f_bcd0_to_bcjpsi'      : { 'value': 0.146     ,  'error':  0.416    ,  'limit': (0, 0.5    ) } ,
            'f_bcd0_to_bud0'        : { 'value': 0.0822    ,  'error':  0.585    ,  'limit': (0, 0.5    ) } ,
            'f_bcdst_to_bcjpsi'     : { 'value': 0.502     ,  'error':  0.429    ,  'limit': (0, 0.5    ) } ,
          }
    return cfg
  else:
    print('Option not recognised')
    return None

def constrcfg(opt='sim'):
  if opt=='run1':
    cfg = {
            #'var' : (val,err),
          }
    return None

  elif opt=='run2':
    cfg = {
            'eff_rat_d0_to_jpsi'  : ( 1.171, 0.009 ),
            'eff_rat_dst_to_jpsi' : ( 1.124, 0.011 ),
            'eff_rat_bu_to_jpsi'  : ( 0.624, 0.010 ),
            'br_rat_cf_to_mm'     : ( 0.663, 0.006 ),
            'br_rat_dcs_to_mm'    : ( 0.00229, 0.00005 ),
          }
    return cfg
  elif opt=='sim':
    cfg = {
          }
    return None
  else:
    print('Option not recognised')
    return None


