## utility functions ##

import os
import sys
import json
import pickle
from tqdm import tqdm
import itertools
import numpy as np
import boost_histogram as bh
import boost_histogram.cpp.algorithm as bh_alg
import ROOT as r
from ROOT import RooFit as rf
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from matplotlib.transforms import Bbox
from scipy.stats import norm as ssnorm
from scipy.optimize import curve_fit
import inspect
import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
from parameter import parameter

# ROOT Style
r.gROOT.SetBatch()
r.gROOT.ProcessLine(".L config/lhcbStyle.C")
r.gStyle.SetTitleXSize(0.07);
r.gStyle.SetTitleYSize(0.07);
r.gStyle.SetTitleXOffset(0.95);
r.gStyle.SetTitleYOffset(1.1);
r.gStyle.SetPadLeftMargin(0.15);

def msg(*args):
  prev_frame = inspect.currentframe().f_back
  if "self" in prev_frame.f_locals.keys():
    the_class = prev_frame.f_locals["self"]
    classname = the_class.__class__.__name__
    if hasattr(the_class,'name'): classname = the_class.name
  else:
    classname = "utils"
  methodname = prev_frame.f_code.co_name
  print( "{:6.6} :".format(classname), *args)

def info(*args):
  prev_frame = inspect.currentframe().f_back
  if "self" in prev_frame.f_locals.keys():
    the_class = prev_frame.f_locals["self"]
    classname = the_class.__class__.__name__
    if hasattr(the_class,'name'): classname = the_class.name
  else:
    classname = "utils"
  methodname = prev_frame.f_code.co_name
  print( "{0:6.6} : INFO    : in function {1} :".format(classname,methodname+'()'), *args)

def warning(*args):
  prev_frame = inspect.currentframe().f_back
  if "self" in prev_frame.f_locals.keys():
    the_class = prev_frame.f_locals["self"]
    classname = the_class.__class__.__name__
    if hasattr(the_class,'name'): classname = the_class.name
  else:
    classname = "utils"
  methodname = prev_frame.f_code.co_name
  print( "{0:6.6} : WARNING : in function {1} :".format(classname,methodname+'()'), *args)

def error(*args):
  prev_frame = inspect.currentframe().f_back
  if "self" in prev_frame.f_locals.keys():
    the_class = prev_frame.f_locals["self"]
    classname = the_class.__class__.__name__
    if hasattr(the_class,'name'): classname = the_class.name
  else:
    classname = "utils"
  methodname = prev_frame.f_code.co_name
  print( "{0:6.6} : ERROR   : in function {1} :".format(classname,methodname+'()'), *args)
  exit(1)

def fatal(*args):
  prev_frame = inspect.currentframe().f_back
  if "self" in prev_frame.f_locals.keys():
    the_class = prev_frame.f_locals["self"]
    classname = the_class.__class__.__name__
    if hasattr(the_class,'name'): classname = the_class.name
  else:
    classname = "utils"
  methodname = prev_frame.f_code.co_name
  print( "{0:6.6} : FATAL   : in function {1} :".format(classname,methodname+'()'), *args)
  exit(1)

def check(value, *args):
  if not value:
    fatal(*args)

def load_binning(fname, fit_bins=False):

  if not os.path.exists(fname):
    sys.exit('Cannot load binning as no such file',fname)

  with open(fname) as f:
    cfg = json.load(f)

  if "axes" not in cfg.keys():
    sys.exit("No axes definition found in",fname)

  fit_binning  = []
  plot_binning = []
  for i, ax in enumerate(cfg["axes"]):

    if not ("observable" in ax.keys() and "bins" in ax.keys() and "range" in ax.keys()):
      sys.exit("Cannot find an observable, bins and range for axis",i,"in defintion", fname)

    fit_binning.append( bh.axis.Regular( ax["bins"], ax["range"][0], ax["range"][1], metadata=ax["observable"] ) )

    if "extra_bins" not in ax.keys():
      plot_binning.append( fit_binning[-1].copy() )

    else:
      n_plot_bins = ax["bins"] + abs( ax["extra_bins"][0] ) + abs( ax["extra_bins"][1] )
      plot_low    = fit_binning[-1].edges[0]  - abs( ax["extra_bins"][0]*fit_binning[-1].widths[0]  )
      plot_high   = fit_binning[-1].edges[-1] + abs( ax["extra_bins"][1]*fit_binning[-1].widths[-1] )
      plot_binning.append( bh.axis.Regular( n_plot_bins, plot_low, plot_high, metadata=ax["observable"] ) )

  return fit_binning if fit_bins else plot_binning

def load_binning_hist(fname, fit_bins=False):
  binning = load_binning(fname, fit_bins)
  return bh.Histogram( *binning )

def get_binning_name(fname):

  if not os.path.exists(fname):
    sys.exit('Cannot load binning as no such file',fname)

  with open(fname) as f:
    cfg = json.load(f)

  if "name" not in cfg.keys():
    sys.exit("Binning file",fname,"does not provide a name")

  return cfg["name"]

def get_binning_titles(fname):

  if not os.path.exists(fname):
    sys.exit('Cannot load binning as no such file '+fname)

  with open(fname) as f:
    cfg = json.load(f)

  if "axes" not in cfg.keys():
    sys.exit("No axes definition found in",fname)

  titles = []
  for i, ax in enumerate(cfg["axes"]):
    if "title" in ax.keys():
      titles.append( ax["title"] )
    else:
      titles.append('')

  return titles


def get_fit_hist_from_plot_hist( hist, binning ):
  plot_binning = load_binning_hist( binning )
  fit_binning  = load_binning_hist( binning, True )

  if hist.shape != plot_binning.shape:
    sys.exit('Histogram does not have the right shape')

  lower_edges = [ ax.edges[0] for ax in fit_binning.axes ]
  upper_edges = [ ax.edges[-1] for ax in fit_binning.axes ]
  lower_coord = plot_binning.axes.index( *lower_edges )
  upper_coord = plot_binning.axes.index( *upper_edges )

  slices = [ slice(lower_coord[i],upper_coord[i]) for i in range(hist.rank) ]
  return hist[ tuple(slices) ]

def get_fit_bin_coord_from_plot_bin_coord( bin_coord, plot_hist, fit_hist, include_flow=False ):
  plot_bin_center = tuple( plot_hist.axes[i].centers[ind] for i, ind in enumerate(bin_coord) )
  fit_coord = fit_hist.axes.index( *plot_bin_center )
  if not include_flow:
    for i, coor in enumerate(fit_coord):
      if coor<0 or coor>=fit_hist.axes[i].size: return None
  return fit_coord

def get_fit_range_scale_factor( plot_hist, fit_hist ):
  ph_sum = plot_hist.sum().value

  lower_edges = [ ax.edges[0] for ax in fit_hist.axes ]
  upper_edges = [ ax.edges[-1] for ax in fit_hist.axes ]
  lower_coord = plot_hist.axes.index( *lower_edges )
  upper_coord = plot_hist.axes.index( *upper_edges )

  slices = [ slice(lower_coord[i],upper_coord[i]) for i in range( plot_hist.rank ) ]

  fh_sum = plot_hist[ tuple(slices) ].sum().value

  scale_factor = ph_sum / fh_sum
  assert( scale_factor>=1. )

  return scale_factor

def progress_bar(description, nevents):

  pbar = tqdm( total=nevents, ncols=150, unit=' events', ascii=True, desc=description, mininterval=0.1, miniters=nevents/5000 )
  return pbar

def loop_bin_coords( hist ):
  return itertools.product( *[ range(s) for s in hist.shape ] )

def plotHadronMassFit( name, dset, pdf ):

  # get component pdfs (not signal)
  comps = {}
  it = pdf.pdfList().createIterator()
  pd = it.Next()
  while pd:
    col = r.kWhite
    if 'bkg_' in pd.GetName(): col = r.kRed-7
    if 'ks_' in pd.GetName(): col = r.kGreen-3
    if 'ps_' in pd.GetName(): col = r.kMagenta-3
    if 'sig_' in pd.GetName():
      pd = it.Next()
      continue
    comps[ pd.GetName() ] = col
    pd = it.Next()

  # get observable
  var = dset.get().find("D0_M")
  if var: var.setBins(75)
  if not var:
    var = dset.get().find("Jpsi_M")
    if var: var.setBins(85)
  if not var:
    sys.exit('Cannot find appropriate variable (D0_M,Jpsi_M) in dataset')

  pl = var.frame()

  # plot the data
  dset.plotOn(pl, rf.Invisible())

  # plot full pdf in kBlue-7
  pdf.plotOn(pl, rf.LineColor(r.kBlue-7), rf.FillColor(r.kBlue-7), rf.DrawOption("F"), rf.VLines() )

  # then plot the other components (alphabetical order)
  comps_to_plot = sorted(comps.keys())
  for i in range(len(comps.keys())):
    comp_plt_str = ",".join( comps_to_plot )
    comp = comps_to_plot.pop()
    pdf.plotOn(pl, rf.LineColor(comps[comp]), rf.FillColor(comps[comp]), rf.DrawOption("F"), rf.VLines(), rf.Components(comp_plt_str) )

  # then plot the data again
  dset.plotOn(pl)

  # add name to help
  lat = r.TLatex(0.55,0.95,"Bin_%s"%os.path.basename(name))
  lat.SetNDC()
  lat.SetTextAlign(22)
  pl.addObject(lat)

  # draw
  c = r.TCanvas()
  pl.Draw()
  c.Update()
  c.Modified()
  os.system('mkdir -p %s'%os.path.dirname(name))
  c.Print(name+".pdf")
  c.Print(name+".png")
  c.Print(name.replace( os.path.basename(name), 'bin_yields.gif+25' ) )

def plotHistogram( hist, path ):

  os.system('mkdir -p %s'%path)

  # plot 1D projections
  for i, ax in enumerate(hist.axes):
    proj = hist.project(i)
    plt.clf()
    plt.bar( ax.centers, proj.view().value, width=ax.widths, yerr=np.sqrt(proj.view().variance) )
    plt.savefig( os.path.join(path,'histogram_'+ax.metadata+'.pdf') )
    plt.savefig( os.path.join(path,'histogram_'+ax.metadata+'.png') )

  # plot 2D if a 2D
  if hist.rank==2:
    X, Y = hist.axes.edges
    plt.clf()
    fig, ax = plt.subplots()
    mesh = ax.pcolormesh( X.T, Y.T, hist.view().value.T)
    fig.colorbar(mesh)
    plt.savefig( os.path.join(path,'histogram.pdf') )
    plt.savefig( os.path.join(path,'histogram.png') )

def writeHistogram( hist, fname ):
  os.system('mkdir -p %s'%os.path.dirname(fname))
  with open(fname,'wb') as f:
    pickle.dump(hist,f)

def readHistogram( fname ):
  if not os.path.exists(fname):
    sys.exit('Cannot read histogram. No such file '+fname)

  with open(fname,'rb') as f:
    h = pickle.load(f)

  return h

def saveSubPlot(fig, fname, points=None, subplt=None):
  if points:
    extent = Bbox(points)
  else:
    extent = (subplt.get_tightbbox(fig.canvas.get_renderer()).transformed(fig.dpi_scale_trans.inverted())).expanded(1.1,1.05)
  if not os.path.exists( os.path.dirname(fname) ): os.system("mkdir -p "+os.path.dirname(fname) )
  fig.savefig(fname+".pdf", bbox_inches=extent)
  fig.savefig(fname+".png", bbox_inches=extent)
  print('Saved figure to:',fname+".pdf")

def savePlot(fig, fname):
  if not os.path.exists( os.path.dirname(fname) ): os.system("mkdir -p "+os.path.dirname(fname) )
  fig.savefig(fname+".pdf")
  fig.savefig(fname+".png")
  print('Saved figure to:',fname+".pdf")


def gauss(x, N, mu, sigma):
  return ( N * np.exp(-1.0 * (x-mu)**2 / ( 2 * sigma**2)))

def makeBetaSPlot(subplt, betas, resplt=None):
  xmin = 0.5
  xmax = 1.5
  bhist = bh.Histogram( bh.axis.Regular( int(betas.size/10), xmin, xmax ) )
  bhist.fill( betas.flatten() )
  subplt.errorbar( bhist.axes[0].centers, bhist.view(), xerr=0.5*bhist.axes[0].widths, yerr=0.5*np.sqrt(bhist.view()), fmt='ok', ecolor='k' )
  # fit Gaussian
  #mu, std = ssnorm.fit( betas.flatten() )
  #x = np.linspace(xmin, xmax, 100)
  #p = ssnorm.pdf(x, mu, std)
  #subplt.plot(x, p, 'k', linewidth=2)
  popt, pcov = curve_fit( gauss, xdata=bhist.axes[0].centers, ydata=bhist.view(), p0=[len(betas.flatten()),1.0,0.5] )
  x = np.linspace(xmin,xmax,100)
  p = gauss(x,*popt)
  subplt.plot(x, p, 'k', linewidth=2)
  subplt.title.set_text( "$\mu=%.2f$, $\sigma=%.2f$"%(popt[1],popt[2]) )
  subplt.set_xlabel( '$\\beta$' )
  subplt.set_ylabel( 'Bins' )
  if resplt:
    resplt.plot( [ bhist.axes[0].centers[0]-0.5*bhist.axes[0].widths[0], bhist.axes[0].centers[-1]+0.5*bhist.axes[0].widths[-1] ], [0,0], color='k' )
    resplt.errorbar( bhist.axes[0].centers, bhist.view()-gauss(bhist.view(),*popt), xerr=0.5*bhist.axes[0].widths, yerr=0.5*np.sqrt(bhist.view()), fmt='ok', ecolor='k' )
    resplt.set_xlabel( '$\\beta$' )
    resplt.set_ylabel( 'Bins' )

def printFitRes(pars):
  parlist = pars.keys()
  plen = max( [len(str(p)) for p in range(len(parlist))] )
  nlen = max( [len(parname) for parname in parlist ] )
  tit_str = '| {:{width}s} '.format('',width=plen) + '| {:{width}s} '.format('Name',width=nlen) +'| {:^10s} | {:^10s} | {:^10s} | {:^10s} | {:^5s} |'.format('Value','Error','Limit-','Limit+','Fixed')
  print( '-'.join( ['' for i in range(len(tit_str)) ] ) )
  print(tit_str)
  print( '-'.join( ['' for i in range(len(tit_str)) ] ) )
  for p, parname in enumerate(parlist):
    val = pars[parname].get_value(scaled=True)
    err = pars[parname].get_error(scaled=True)
    lim = pars[parname].get_limit(scaled=True)
    fix = pars[parname].fix
    print( '| {:<{width}d} '.format(p,width=plen) + '| {:{width}s} '.format(parname,width=nlen) + '| {:^ 10.3g} | {:^ 10.3g} | {:^ 10.3g} | {:^ 10.3g} | {:^5s} |'.format(val,err,lim[0],lim[1],str(fix)) )
  print( '-'.join( ['' for i in range(len(tit_str)) ] ) )

def loadFitResult(fname):

  if not os.path.exists(fname):
    error('No such file',fname)

  with open(fname,'rb') as f:
    pars = pickle.load(f)

  printFitRes(pars)
  return pars

