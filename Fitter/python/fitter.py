import python.utils as utils
from python.parameter import parameter
from python.mode import mode
import boost_histogram as bh
from iminuit import Minuit
import numpy as np
from sympy.physics.units import Dimension
from sympy.physics.units.systems.si import dimsys_SI
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl
mpl.rcParams['axes.titlesize'] = 14
mpl.rcParams['axes.labelsize'] = 14
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
import os
import pickle

class fitter:

  def __init__(self, name, hesse=False, minos=False, extended=True, strict=True, test=False, verbose=False):

    self.name = name
    self.hesse = hesse
    self.minos = minos
    self.extended = extended
    self.strict = strict
    self.test = test
    self.verbose = verbose

    self.modes = []
    self.betas = []

    self.yieldscfg = {}
    self.parameters = {}
    self.parlist = []
    self.constraints = {}

    self.nll_calls = 0
    self.plt_calls = 0

    self.user_has_set_parameters = False

    utils.msg('Initialised fitter')

  def getname(self):
    return self.name

  def AddMode(self, name, data, components, yields, submodes=[''], binning=None, **kwargs):
    self.modes.append( mode(name, data, components, yields, submodes=submodes, binning_file=binning, wider_plot_range=True) )
    self.betas.append( [ np.ones( self.modes[-1].data[i].shape ) for i in range(len(self.modes[-1].submodes)) ] )

    utils.msg('Added fit mode',self.modes[-1])
    if self.verbose:
      self.modes[-1].print()

  def GetMode(self, name):
    for mode in self.modes:
      if name==mode.name: return mode
    return None

  def iterator(self):
    itlist = []
    for mode in self.modes:
      for comp in mode.compnames:
        itlist.append( (mode.name, comp) )
    return itlist

  def get_yield_name(self, modename, compname, submodename='' ):
    if submodename=='':
      return '_'.join([modename,compname,'y'])
    else:
      return '_'.join([modename,submodename,compname,'y'])

  def get_yield_dimension(self, parname):
    dim_pref = parname
    for mode in self.modes:
      if parname.startswith(mode.name) and parname.endswith('_y'):
        dim_pref = mode.name
        for submode in mode.submodes:
          if submode in parname:
            if parname.startswith(mode.name+'_'+submode):
              dim_pref += '_'+submode
        dim_pref += '_y'
    return dim_pref

  def get_mode_submode_from_yield_dim(self, ylddim):
    modename = None
    submodename = ''
    for mode in self.modes:
      if ylddim.startswith(mode.name) and ylddim.endswith('_y'):
        modename = mode.name
        for submode in mode.submodes:
          if submode in ylddim:
            if ylddim.startswith(mode.name+'_'+submode):
              submodename = submode
    return modename, submodename

  def print_parameters(self):
    plen = max( [len(str(p)) for p in range(len(self.parlist))] )
    nlen = max( [len(parname) for parname in self.parlist ] )
    tit_str = '| {:{width}s} '.format('',width=plen) + '| {:{width}s} '.format('Name',width=nlen) +'| {:^10s} | {:^10s} | {:^10s} | {:^10s} | {:^5s} |'.format('Value','Error','Limit-','Limit+','Fixed')
    print( '-'.join( ['' for i in range(len(tit_str)) ] ) )
    print(tit_str)
    print( '-'.join( ['' for i in range(len(tit_str)) ] ) )
    for p, parname in enumerate(self.parlist):
      val = self.parameters[parname].get_value(scaled=True)
      err = self.parameters[parname].get_error(scaled=True)
      lim = self.parameters[parname].get_limit(scaled=True)
      fix = self.parameters[parname].fix
      print( '| {:<{width}d} '.format(p,width=plen) + '| {:{width}s} '.format(parname,width=nlen) + '| {:^ 10.3g} | {:^ 10.3g} | {:^ 10.3g} | {:^ 10.3g} | {:^5s} |'.format(val,err,lim[0],lim[1],str(fix)) )
    print( '-'.join( ['' for i in range(len(tit_str)) ] ) )

  def set_parameters(self, parameter_cfg):

    self.configure_parameters()

    for par in self.parlist:
      if par in parameter_cfg.keys():
        setpar = parameter_cfg[par]
        if type(setpar)==type(parameter()):
          self.parameters[par] = setpar
        elif type(setpar)==type(dict()):
          if 'value' in setpar.keys():
            self.parameters[par].set_value( setpar['value']) #, scaled=True )
          if 'error' in setpar.keys():
            self.parameters[par].set_error( setpar['error'], scaled=True )
          if 'limit' in setpar.keys():
            self.parameters[par].set_limit( setpar['limit'], scaled=True )
          if 'fix' in setpar.keys():
            self.parameters[par].fix = setpar['fix']
          if 'scale' in setpar.keys():
            self.parameters[par].scale = setpar['scale']

    self.user_has_set_parameters = True
    utils.msg('Set parameter values to:')
    self.print_parameters()

  def set_constraints(self, constr_cfg):

    if not self.user_has_set_parameters:
      self.configure_parameters()

    # reset constraints
    self.constraints = {}

    for par, constr in constr_cfg.items():
      if par not in self.parlist:
        utils.error('You are trying to set a constraint on parameter', par, 'but it does not exist in the list of parameters')

      self.constraints[par] = constr
      utils.msg('Added constraint for parameter',par,' = ',constr[0], '+/-', constr[1])

  def load_parameters(self, fname):

    self.configure_parameters()

    if not os.path.exists(fname):
      utils.fatal('No such file '+fname+' to load parameters from')

    utils.msg('Loading parameters from', fname )
    with open(fname,'rb') as f:
      loaded_pars = pickle.load(f)

    for par in self.parlist:
      if par not in loaded_pars.keys():
        utils.fatal('Parameter', par, 'not found in loaded parameter file',fname)
      self.parameters[par] = loaded_pars[par]

    for par in loaded_pars.keys():
      if par not in self.parlist:
        utils.warning('Parameter', par,'loaded from file',fname,'is not in the parlist. Ignoring it')

    # also need to load the beta values
    betaname = os.path.splitext(fname)[0] + "_betas" + os.path.splitext(fname)[1]

    if not os.path.exists(betaname):
      utils.fatal('No such file',betaname,'to load beta values from')

    if self.verbose:
      utils.msg('Loading beta values from', betaname)
    with open(betaname,'rb') as bf:
      loaded_betas = pickle.load(bf)

    if len(loaded_betas) != len(self.betas):
      utils.fatal('beta values loaded from',betaname,'are not the right shape')
    for m, smbetas in enumerate(self.betas):
      if len(smbetas) != len(self.betas[m]):
        utils.fatal('beta values loaded from',betaname,'are not the right shape')
      for sm, betas in enumerate(smbetas):
        if betas.shape != loaded_betas[m][sm].shape:
          utils.fatal('beta values laoded from',betaname,'have the wrong shape')

    self.betas = loaded_betas

    utils.msg('Loaded parameters with values:')
    self.print_parameters()

  def save_parameters(self, fname):
    utils.msg('Saving parameters to', fname)

    if not os.path.exists( os.path.dirname(fname) ):
      os.system('mkdir -p '+os.path.dirname(fname))

    with open(fname,'wb') as f:
      pickle.dump(self.parameters,f)

    # also need to save the beta values
    betaname = os.path.splitext(fname)[0] + "_betas" + os.path.splitext(fname)[1]
    if self.verbose:
      utils.msg('Saving beta values to', betaname)

    with open(betaname,'wb') as bf:
      pickle.dump(self.betas,bf)

  def get_par_values(self):
    return [ self.parameters[par].get_value() for par in self.parlist ]

  def get_pars_from_func_str(self, func_str, keep_spaces=False):
    known_operators = ['+','-','/','*','(',')']
    tmp = ''.join(func_str.replace(' ',''))
    ops = [ char for char in func_str if char in known_operators ]
    for op in known_operators:
      tmp = tmp.replace(op,'*')
    pars = tmp.split('*')
    if not keep_spaces: pars = list(filter(lambda x: x!='', pars))
    return pars, ops

  # this helps us out with the internal scaling
  def get_func_dimension(self, func_str):
    pars, ops = self.get_pars_from_func_str( func_str )
    dim_expr = "".join(func_str)
    for par in pars:
      dim_par = self.get_yield_dimension(par)
      #for mode in self.modes:
        #if par.startswith(mode.name) and par.endswith('_y'):
          #dim_expr = dim_expr.replace(par, mode.name+'_y')
          #par = mode.name+'_y'
      dim_expr = dim_expr.replace(par,'Dimension(\''+dim_par+'\')')
    func_dim = eval(dim_expr)
    return func_dim

  # this gets the internal scaling
  def get_scale_from_func_dim(self, func_dim, parname, modename, submodename=''):
    func_dim_wo_par = func_dim / Dimension(parname)
    sm = self.GetMode(modename).submodes.index(submodename)
    scale = self.GetMode(modename).data[sm].sum().value
    dim_dep = dimsys_SI.get_dimensional_dependencies(func_dim_wo_par)
    for dim, val in dim_dep.items():
      if dim.endswith('_y'):
        mdname, smname = self.get_mode_submode_from_yield_dim(dim)
        md   = self.GetMode(mdname)
        mdsm = md.submodes.index( smname )
        norm = md.data[mdsm].sum().value
        sign = np.sign(val)
        for i in range(abs(val)):
          if sign>0: scale /= norm
          else:      scale *= norm
      elif dim in self.parlist:
        sign = np.sign(val)
        for i in range(abs(val)):
          if sign>0: scale /= self.parameters[dim].scale
          else:      scale *= self.parameters[dim].scale
    return(scale)

  # do some manipulation so that shortcuts are replaced with
  # the full variable names
  # i.e it is allowed to pass a parameter just by its component name
  # where the same mode will be used
  # or by passing the mode and then a colon
  # or by passing submode and then a colon
  # or by passing mode : submode : expr
  # e.g. yields : { 'bkg'  : 'abs',
  #                 'sig1' : 'func(f1 * bkg)',
  #                 'sig2' : 'func(f2 * Mode2:bkg)',
  #                 'sig3' : 'func(f3 * submode2:sig1)',
  #                 'sig4' : 'func(f4 * Mode2:submode3:bkg)'
  #               }
  def manipulate_func_str(self, func_str, mode, submode=''):
    pars, ops = self.get_pars_from_func_str( func_str, keep_spaces=True )
    for p in range(len(pars)):
      par = pars[p]
      # if it's a component name
      if par in mode.compnames:
        newpar = self.get_yield_name(mode.name, par, submode)
        pars[p] = newpar
      if ':' in par:
        els = par.split(':')
        if len(els)==2:
          mdnm = els[0]
          cpnm = els[1]
          # if it's a different mode name
          if mdnm in [ md.name for md in self.modes ]:
            relmd = self.GetMode( mdnm )
            assert(cpnm in relmd.compnames)
            newpar = self.get_yield_name( mdnm, cpnm, submode )
            pars[p] = newpar
          # if it's a submode name
          elif mdnm in mode.submodes:
            assert(cpnm in mode.compnames)
            newpar = self.get_yield_name( mode.name, cpnm, mdnm )
            pars[p] = newpar
          else:
            print(self.name, ': ERROR : function', func_str, 'asks for mode or submode', mdnm, 'which doesnt exist')
            assert(0)
        elif len(els)==3:
          mdnm = els[0]
          smnm = els[1]
          cpnm = els[2]
          assert( mdnm in [ md.name for md in self.modes ] )
          relmd = self.GetMode( mdnm )
          assert(smnm in relmd.submodes)
          assert(cpnm in relmd.compnames)
          newpar = self.get_yield_name( mdnm, cpnm, smnm )
          pars[p] = newpar
        else:
          print(self.name,': ERROR : Too many colons (:) in func string',func_str)
          assert(0)

    # now we want to rebuild the func str
    ret_str = ''
    for i, par in enumerate(pars):
      ret_str += par
      if i<(len(pars)-1): ret_str += ops[i]

    return ret_str

  ### this is an important part of the code where we configure the
  ### different paramters. we store them here because when we call the
  ### likelihood we have a very specific ordering
  ### we use this alongside get_yields_from_parameters
  ### minuit (and fitters in general) like their fit parameters to be of O(1)
  ### so we will always scale the fit down when fitting yields to actually fit
  ### a fractional yield which we can then internally convert into the total yield

  def configure_parameters(self):

    # reset configuration
    self.parameters = {}
    self.parlist = []
    self.betas = [ [ np.ones( mode.data[i].shape) for i in range(len(mode.submodes)) ] for mode in self.modes ]

    # store funcs in a list so we can do these last
    funcs = []

    allowed_options = ['frac','abs'] # can also start with func()

    # loop over modes, submodes and components
    for mode in self.modes:
      for comp in mode.compnames:
        for sm, submode in enumerate(mode.submodes):

          # get the yield expression and store it
          yld_expr = mode.yields[comp][sm]
          yld_name = self.get_yield_name(mode.name, comp, submode)
          self.yieldscfg[yld_name] = yld_expr

          # if its a function store in a list and deal with it a bit later
          if yld_expr.startswith('func('):
            assert( yld_expr.endswith(')') )
            func_str = yld_expr[5:-1]
            funcs.append( (self.manipulate_func_str( func_str, mode ), mode.name, submode) )

          # otherwise make the parameter
          else:
            if yld_expr not in allowed_options:
              print("Don't recongnise this protocol",yld_expr,"for a yield yet")
              assert( yld in allowed_options )

            parname  = yld_name
            parval   = 0.9/len(mode.compnames)
            parerr   = parval/10.
            parlimit = [0,1]
            parscale = 1. if yld_expr=='frac' else mode.data[sm].sum().value
            self.parlist.append( parname )
            self.parameters[parname] = parameter( name=parname, value=parval, error=parerr, limit=parlimit, fix=False, scale=parscale )

    # now add parameters for funcs
    for func, modename, submodename in funcs:
      # get the parameters in the func str
      pars, ops = self.get_pars_from_func_str( func )
      # get the dimension of the func str
      func_dim = self.get_func_dimension( func )
      for parname in pars:
        if parname not in self.parlist:
          parval   = 0.1
          parerr   = parval/10.
          parlimit = [0,1]
          # we need to figure out our internal scale for this parameter
          parscale = self.get_scale_from_func_dim(func_dim, parname, modename, submodename)
          self.parlist.append( parname )
          self.parameters[parname] = parameter( name=parname, value=parval, error=parerr, limit=parlimit, fix=False, scale=parscale )

    utils.msg('Initiliased parameters with values:')
    self.print_parameters()

  def calc_yield(self, mode, submode, compname, parvals):

    # find out if direct yield exists
    yieldname = self.get_yield_name(mode.name, compname, submode)
    assert(yieldname in self.yieldscfg.keys())

    yieldcfg = self.yieldscfg[yieldname]

    # if a direct floating yield
    if yieldname in self.parlist:
      ind = self.parlist.index(yieldname)
      return parvals[ind]

    # otherwise a function which we need to compute
    elif yieldcfg.startswith('func('):
      assert( yieldcfg.endswith(')') )
      func_str = self.manipulate_func_str( yieldcfg[5:-1], mode )
      pars, ops = self.get_pars_from_func_str( func_str )
      for par in pars:
        if par not in self.parlist:
          print("Coudln't find parameter",par,"in parlist")
          assert(par in self.parlist)
        ind = self.parlist.index(par)
        func_str = func_str.replace(par,'parvals[%d]'%ind)
      val = eval(func_str)
      return val

    else:
      print("Dont't recongnise this protocol for a yield yet")
      assert(0)

  # this takes in a set of parameter values (as array)
  # and will return a dictionary of the relevant yield values
  def get_yields_from_parameters(self, parvals):
    yields = {}
    for mode in self.modes:
      yields[mode.name] = {}
      for submode in mode.submodes:
        yields[mode.name][submode] = {}
        for comp in mode.compnames:
          yields[mode.name][submode][comp] = self.calc_yield(mode, submode, comp, parvals)
    return yields

  def N2LL(self, parvals):
    ## Beeston-Barow-lite description
    ## Mostly stolen from O. Lupton
    ## Useful guide in Sec 5 of https://arxiv.org/pdf/1103.0354.pdf
    ## Eq 10 says contribution to -lnL in each bin is
    ##
    ## -lnL = -n*log(beta*mu) + beta*mu + (beta-1)^2/(2*sigma_beta^2)
    ##
    ## where beta (~1) is the solution of
    ##
    ## beta^2 + (mu*sigma_beta^2 - 1)*beta - n*sigma_beta^2 = 0
    ##
    ## mu is expected yield from templates
    ## n is the data yield in the bin (mu ~ n)
    ## sigma_beta is the relative uncertainty sigma_mu / mu i.e. sqrt( sum w^2 ) / sum w
    ##
    ## this is simply
    ##
    ## L = Poisson( n; beta*mu ) * Gaussian ( beta*mu; mu, sigma_mu )
    ##
    ## where beta is a nuisance parameter (in each bin) which accounts for limited template statistics
    ## minimising -lnL with respect to beta provides the solution
    ##
    ## beta = 0.5 * ( 1 - mu*sigma_beta^2 ) + sqrt( (1 - mu*sigma_beta^2)^2 + 4*n*sigma_beta^2 )
    ##
    ## note to then add the usual data contribution we want an additional term per bin:
    ##
    ## n*ln(n) - n
    ##
    ## giving a total contribution to the -lnL per bin:
    ##
    ## -lnL = -n*log(beta*mu) + beta*mu + (beta-1)^2/(2*sigma_beta^2) + n*ln(n) - n
    ##
    ## note it is comuptationally more efficient to only compute a logarithm once so combining log terms:
    ##
    ## -lnL = beta*mu + n*( log( n / (beta*mu) ) - 1.0 ) + (beta-1)^2 / (2*sigma_beta^2)
    ##
    ## the above is for -lnL, but for equivalence to a chi2 fit then we minimise -2lnL
    ##

    self.nll_calls += 1

    n2ll = 0.
    # reset beta values
    self.betas = [ [ np.ones( mode.data[i].shape) for i in range(len(mode.submodes)) ] for mode in self.modes ]

    yields = self.get_yields_from_parameters( parvals )

    # loop over modes e.g ['D0','Jpsi']
    for m, mode in enumerate(self.modes):
      modename = mode.name
      # loop over submodes e.g. ['2011','2012']
      for sm, submode in enumerate(mode.submodes):
        dh = mode.data[sm]
        norm = dh.sum().value
        # loop bins in the data
        for bin_coord in utils.loop_bin_coords( dh ):

          n           = dh[ bin_coord ].value
          mu          = 0.
          mu_variance = 0.

          # loop components (which are normed to unity)
          for c, compname in enumerate(mode.compnames):
            ch = mode.components[compname][sm]
            mu          += norm    * yields[modename][submode][compname]    * ch[ bin_coord ].value
            mu_variance += norm**2 * yields[modename][submode][compname]**2 * ch[ bin_coord ].variance

          beta = 1.
          if mu>0.:
            if mu_variance>0.:
              beta_variance = mu_variance / ( mu**2 )
              beta = 0.5 * ( 1.0 - (mu*beta_variance ) + ( (1.0 - (mu*beta_variance))**2 + 4.0*n*beta_variance )**0.5 )
              n2ll += 2.0 * beta * mu
              n2ll += ( beta - 1.0 )**2 / beta_variance
              if ( n > 0. ):
                n2ll += 2.0 * n * ( np.log( n / ( beta*mu) ) - 1.0 )
            else:
              # No MC err: beta=1
              n2ll += 2.0 * mu + 2.0 * n * ( np.log( n/mu ) - 1.0 )

          self.betas[m][sm][ bin_coord ] = beta

        # add extended term so that fractions are constrained to sum to 1 given the amount of data
        if self.extended:
          sum_fracs = sum( [ yields[modename][submode][compname] for compname in mode.compnames ] )
          norm_frac_variance  = 1./norm
          n2ll += ( sum_fracs - 1.0 )**2 / norm_frac_variance
          #if (self.nll_calls-1)%100==0: print( sum_fracs, norm_frac_variance )

    # add constraint terms
    for par, constr in self.constraints.items():
      parindex = self.parlist.index(par)
      n2ll += ( parvals[parindex] - constr[0] )**2 / constr[1]**2

    if self.verbose and (self.nll_calls-1)%100==0:
      if self.nll_calls==1: print( "".join( [ "=" for i in range(42) ] ) )
      print( self.name,': nll_call {:4d}'.format(self.nll_calls),'-2LL = {:.14g}'.format(n2ll) )
      print( self.name,': evaluation at:')
      for i, par in enumerate(self.parlist):
        print(self.name,': \t', '{:20s}'.format(par), '=', '{:10.3g}'.format(parvals[i]))
      print( self.name,': propagated to fractional yields:' )
      for m, mode in enumerate(self.modes):
        modename = mode.name
        for sm, submodename in enumerate(mode.submodes):
          for c, compname in enumerate(mode.compnames):
            print( self.name,': \t', '{:20s}'.format(self.get_yield_name(modename,compname,submodename)),'=', '{:10.3f}'.format(yields[modename][submodename][compname]) )
      print( self.name,': \t-2LL = ', n2ll )
      print( "".join( [ "=" for i in range(42) ] ) )

    return n2ll

  def CheckFitResult(self):
    fr = self.m.get_fmin()
    utils.check( fr.is_valid                   , 'Invalid fit result:\n', fr)
    passed = True
    # check minuit status
    if self.strict:
      utils.check( fr.has_accurate_covar         , 'Inaccurate coviarnace:\n', fr)
      utils.check( fr.has_posdef_covar           , 'Covariance not pos def:\n', fr)
      utils.check( not fr.has_made_posdef_covar  , 'Covariance forced pos def:\n', fr)
      utils.check( not fr.hesse_failed           , 'Hesse failed:\n', fr)
      utils.check( fr.has_covariance             , 'No covariance:\n', fr)
    else:
      if not fr.has_accurate_covar:
        utils.warning('Inaccurate coviarnace:\n', fr)
        passed = False
      if not fr.has_posdef_covar  :
        utils.warning('Covariance not pos def:\n', fr)
        False
      if fr.has_made_posdef_covar :
        utils.warning('Covariance forced pos def:\n', fr)
        False
      if fr.hesse_failed          :
        utils.warning('Hesse failed:\n', fr)
        False
      if not fr.has_covariance    :
        utils.warning('No covariance:\n', fr)
        False

    # check the errors
    if ( self.m.covariance is None ):
      # something went wrong with hesse so rerun migrad to get the errors back
      if self.strict: utils.fatal( 'No valid covariance matrix' )
      else: utils.warning('It looks like Hesse failed so going to rerun Migrad')
      passed = False
      self.m.migrad()

    if passed:
      utils.msg('Fit result passed checks OK')
    else:
      utils.msg('Fit result passed with WARNINGS')

  def Fit(self, savefile="fitres/fit.pkl"):

    # configure params
    if not self.user_has_set_parameters:
      self.configure_parameters()

    # set up minimizer arguments
    minuit_args = dict()

    # note that although we are doing a likelihood fit the errordef is still 1 because we
    # have defined the -2NLL (i.e. with factor of two already)
    minuit_args['errordef'] = 1.

    # pass parameter name, values, errors and limits
    start_vals           = tuple( self.parameters[parname].get_value() for parname in self.parlist )
    minuit_args['name']  = tuple( self.parameters[parname].name        for parname in self.parlist )
    minuit_args['error'] = tuple( self.parameters[parname].get_error() for parname in self.parlist )
    minuit_args['limit'] = tuple( self.parameters[parname].limit       for parname in self.parlist )
    minuit_args['fix']   = tuple( self.parameters[parname].fix         for parname in self.parlist )

    self.m = Minuit.from_array_func( self.N2LL, start_vals , **minuit_args )

    utils.msg('Running fit')
    if not self.test:
      self.m.migrad()
      self.m.hesse()
      self.CheckFitResult()
    if self.verbose:
      utils.msg('iminuit fit result:')
      print( self.m.get_fmin() )
      print( self.m.get_param_states() )

    # set the fitted values and errors back into our own parameters
    for parname in self.parlist:
      val = self.m.values[parname]
      err = self.m.errors[parname]
      self.parameters[parname].set_uval(val,err)

    utils.msg('Fitted values:')
    self.print_parameters()

    self.save_parameters(savefile)

  def Plot(self, cfg=[], titles=[], xlabels=[], ylabels=[], beta_scaling=True, resid=True, beta_hist=False, outdir='plots', wide=False, interactive=False):

    utils.msg('Making plot')
    self.plt_calls += 1

    # figure settings
    fw = 6 # subfigure width in inches
    fh = 4 # subfigure height in inches
    rr = 2 # height ratio from upper plot to residual
    sc = 1 # overall scale

    # configure how many subplots there will be
    # we want each subplot to be (6,4) and if we have a residual then (6,6)
    # plot each mode on a different row with each dimension in a different column
    nrows = sum( [ len(m.submodes) for m in self.modes ] )
    ncols = max( [ mode.data[0].rank for mode in self.modes] ) + beta_hist
    figure = plt.figure( self.plt_calls, figsize=(sc*fw*ncols,sc*(fh+resid*rr)*nrows) )
    outer = gridspec.GridSpec(nrows,ncols)
    for i in range( ncols*nrows ):
      if resid: inner = gridspec.GridSpecFromSubplotSpec(2,1,subplot_spec=outer[i], height_ratios=(rr,1), hspace=0.0)
      else:     inner = gridspec.GridSpecFromSubplotSpec(1,1,subplot_spec=outer[i])
      for j in range(1+resid):
        subplt = plt.Subplot(figure, inner[j])
        figure.add_subplot(subplt)

    subplots = figure.get_axes()

    # define some default colours
    defcols = [ 'b','r','m','g','c' ]
    for _ in range(100): defcols.append('k')

    # loop over fit modes
    for m, mode in enumerate(self.modes):

      complist = mode.compnames
      for sm, submode in enumerate(mode.submodes):
        dh = mode.data[sm] if not wide else mode.data_pr[sm]

        # normalisation is fiddly in the case of plotting in a wider range
        # this gets close
        norm = dh.sum().value

        # but the amount of total template outside fit range is not necessarily the same
        # as the amount of data outside the fit range
        # for a wider plot range we want it such that inside the fit range the total amount of template
        # is the same as the total amount of data so we'll have to compute an appropriate scale factor
        data_scale_factor = 1
        if wide:
          data_scale_factor = utils.get_fit_range_scale_factor( dh, mode.data[sm] )

        # setup some plot configuration from the cfg dictionary passed
        cols = { complist[i] : defcols[i] for i in range(len(complist)) }
        labels = { complist[i] : '' for i in range(len(complist)) }
        if len(cfg)>0 and m < len(cfg):
          for comp, comp_plt_cfg in cfg[m].items():
            if 'order' in comp_plt_cfg.keys():
              order = comp_plt_cfg['order']
              complist[order] = comp
            if 'color' in comp_plt_cfg.keys():
              cols[comp] = comp_plt_cfg['color']
            if 'title' in comp_plt_cfg.keys():
              labels[comp] = comp_plt_cfg['title']

        if m>=len(titles):  titles.append( '' )
        if m>=len(xlabels): xlabels.append( ['' for i in range(dh.rank)] )
        if m>=len(ylabels): ylabels.append( ['' for i in range(dh.rank)] )
        # need to make some copies of the components so we can scale them for plotting
        plot_comps = {}
        # make a stack object (which we use for stacking a bit later)
        stack_comps = {}
        stack = bh.Histogram( *dh.axes, storage=bh.storage.Weight() )
        # keep a total object (which we use for plotting errors later)
        total = bh.Histogram( *dh.axes, storage=bh.storage.Weight() )

        # now loop the components
        for compname in complist:
          ch = mode.components[compname][sm] if not wide else mode.components_pr[compname][sm]
          ph = ch.copy()
          assert(abs(ph.sum().value-1)<1e-6)

          compy = self.calc_yield(mode,submode,compname,self.get_par_values())

          # figure out the appropriate scaling
          if wide:
            comp_scale_factor = utils.get_fit_range_scale_factor( ch, mode.components[compname][sm] )
            ph *= norm * (comp_scale_factor/data_scale_factor) * compy
          else:
            ph *= norm * compy

          # implement beta scaling here
          if beta_scaling:
            if not wide:
              assert( self.betas[m][sm].shape == ph.shape )
              for bin_coords in utils.loop_bin_coords(ph):
                val = ph[bin_coords].value * self.betas[m][sm][bin_coords]
                var = ph[bin_coords].variance
                ph[bin_coords] = bh.accumulators.WeightedSum( value=val, variance=var )
            else:
              assert( self.betas[m][sm].shape == mode.components[compname][sm].shape )
              # now we have to loop the bin coords of the plot range hist and
              # find the corresponding bin coord of the fit range hist to
              # extract the relevant betas value
              for plot_bin_coord in utils.loop_bin_coords(ph):
                fit_coord = utils.get_fit_bin_coord_from_plot_bin_coord( plot_bin_coord, ph, mode.components[compname][sm] )
                betas = 1.
                if fit_coord: betas = self.betas[m][sm][fit_coord]
                val = ph[plot_bin_coord].value * betas
                var = ph[plot_bin_coord].variance
                ph[plot_bin_coord] = bh.accumulators.WeightedSum( value=val, variance=var )

          # add to plot components
          plot_comps[compname] = ph

          # add to stacks (which is one behind)
          stack_comps[compname] = stack.copy()
          stack += ph

          # add to total
          total += ph

        # now loop over each dimension and make the plot
        for i, ax in enumerate(dh.axes):

          # figure out the subplot
          spind = ncols*(m*len(mode.submodes)+sm)*(resid+1) + i*(resid+1)
          subplt = subplots[spind]

          # get the data and total template projections
          proj = dh.project(i)
          tot = total.project(i)
          # loop the components and get each projection
          for c, compname in enumerate(complist):
            ph = plot_comps[compname]
            sh = stack_comps[compname]

            comp = ph.project(i)
            stck = sh.project(i)

            # plot the component
            subplt.bar( ax.centers, comp.view().value, width=ax.widths, color=cols[compname], bottom=stck.view().value, label=labels[compname] )

          # plot the data
          subplt.errorbar( ax.centers, proj.view().value, xerr=0.5*ax.widths, yerr=0.5*np.sqrt(proj.view().variance), fmt='ok', ecolor='k', label='Data' )
          # plot the template error
          for j, x, y, xe, ye in zip(range(len(ax.centers)), ax.centers, tot.view().value, 0.5*ax.widths, 0.5*np.sqrt(tot.view().variance)):
            lb = 'Total Template Error' if j==0 else ''
            subplt.fill( [ x-xe, x+xe, x+xe, x-xe ], [ y-ye, y-ye, y+ye, y+ye ], fill=False, hatch="xx",label=lb )

          # dress plot

          # legend
          subplt.legend(loc='upper right', framealpha=1, facecolor='white')

          # title
          if type(titles[m])==type(str()):
            subplt.title.set_text( titles[m] )
          elif type(titles[m])==type(list()):
            if sm>=len(titles[m]): titles[m].append('')
            subplt.title.set_text( titles[m][sm] )

          # xy labels
          subplt.set_xlabel( xlabels[m][i] )
          subplt.set_ylabel( ylabels[m][i] )

          # if a residual is coming remove the x ticks
          if resid:
            subplt.tick_params(axis='x', labelsize=0)

          # if wide plot fit range lines
          if wide:
            ylim = subplt.get_ylim()
            subplt.plot( [mode.data[sm].axes[i].edges[0], mode.data[sm].axes[i].edges[0]], ylim, color='k', linestyle='--' )
            subplt.plot( [mode.data[sm].axes[i].edges[-1], mode.data[sm].axes[i].edges[-1]], ylim, color='k', linestyle='--' )
            subplt.set_ylim( ylim )

          # plot residual
          resplt = None
          if resid:
            resplt = subplots[spind + 1]
            # horizontal line
            resplt.plot( [ ax.centers[0]-0.5*ax.widths[0], ax.centers[-1]+0.5*ax.widths[-1] ], [0,0], color='k' )
            # data residual
            resplt.errorbar( ax.centers, proj.view().value-tot.view().value, xerr=0.5*ax.widths, yerr=0.5*np.sqrt(proj.view().variance), fmt='ok', ecolor='k', label='Data' )
            # template errors
            for j, x, y, xe, ye in zip(range(len(ax.centers)), ax.centers, tot.view().value-tot.view().value, 0.5*ax.widths, 0.5*np.sqrt(tot.view().variance)):
              lb = 'Total Template Error' if j==0 else ''
              resplt.fill( [ x-xe, x+xe, x+xe, x-xe ], [ -ye, -ye, ye, ye ], fill=False, hatch="xx",label=lb )
            # dress plot
            resplt.set_xlabel( xlabels[m][i] )
            resplt.set_ylabel( 'Residuals' )
            ylim = resplt.get_ylim()
            ymax = max(abs(ylim[0]),abs(ylim[1]))
            resplt.set_ylim( -1*ymax, ymax )
          # if wide plot fit range lines
            if wide:
              resplt.plot( [mode.data[sm].axes[i].edges[0], mode.data[sm].axes[i].edges[0]], resplt.get_ylim(), color='k', linestyle='--' )
              resplt.plot( [mode.data[sm].axes[i].edges[-1], mode.data[sm].axes[i].edges[-1]], resplt.get_ylim(), color='k', linestyle='--' )


        # now make the betas histogram if requested
        if beta_hist:
          spind = ncols*(m*len(mode.submodes)+sm)*(resid+1) + (ncols-1)*(resid+1)
          subplt = subplots[spind]
          resplt = subplots[spind + 1] if resid else None
          utils.makeBetaSPlot( subplt, self.betas[m][sm], resplt )

    figure.tight_layout()
    # align y labels
    for col in range(ncols):
      figure.align_ylabels( [ subplots[i+col*(resid+1)] for i in range(len(subplots)) if int(i/(resid+1))%ncols==0 ] )

    plot_name_prefix = 'fit'
    if wide: plot_name_prefix += '_wide'

    # now we have "tightened the layout we can save the subfigures
    for m, mode in enumerate(self.modes):
      for sm, submode in enumerate(mode.submodes):
        dh = mode.data[sm]
        for i, ax in enumerate(dh.axes):
          x0 = i*sc*fw
          x1 = (i+1)*sc*fw
          y0 = nrows*(fh+resid*rr) - (m*len(mode.submodes)+sm+1)*sc*(fh+resid*rr)
          y1 = nrows*(fh+resid*rr) - (m*len(mode.submodes)+sm)*sc*(fh+resid*rr)
          plotname = outdir + '/' + plot_name_prefix + '_' + mode.name
          if len(mode.submodes)>1: plotname += '_' + submode
          plotname += '_' + ax.metadata
          utils.saveSubPlot( figure, plotname, points=[[x0,y0],[x1,y1]] )

    utils.savePlot( figure, outdir+'/'+plot_name_prefix )

  def ShowPlots(self):
    plt.show()

