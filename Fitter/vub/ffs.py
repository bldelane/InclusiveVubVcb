import numpy as np
from uncertainties import ufloat
from uncertainties import unumpy
import matplotlib.pyplot as plt
plt.rcParams.update({'text.usetex': True})
from scipy.optimize import curve_fit

# from https://arxiv.org/pdf/1909.01213.pdf

jpsi_q2_bins = np.linspace(0,10,11)
dz_q2_bins   = np.linspace(0,20,11)

# array shape (10,10,2)
# cols are jpsi q2 bins
# rows are d0 q2 bins
# hold val and err
Rvals = np.array([
      [(0.26, 0.06), (0.21, 0.05), (0.19, 0.04), (0.17, 0.04), (0.15, 0.03), (0.15, 0.03), (0.14, 0.03), (0.15, 0.03), (0.17, 0.04), (0.24, 0.05)],
      [(0.28, 0.07), (0.23, 0.05), (0.20, 0.04), (0.18, 0.04), (0.16, 0.03), (0.16, 0.03), (0.15, 0.03), (0.16, 0.03), (0.18, 0.04), (0.26, 0.05)],
      [(0.29, 0.07), (0.24, 0.05), (0.21, 0.05), (0.19, 0.04), (0.17, 0.04), (0.16, 0.03), (0.16, 0.03), (0.16, 0.03), (0.19, 0.04), (0.27, 0.06)],
      [(0.30, 0.08), (0.24, 0.06), (0.21, 0.05), (0.19, 0.04), (0.18, 0.04), (0.17, 0.04), (0.16, 0.04), (0.17, 0.04), (0.19, 0.04), (0.28, 0.06)],
      [(0.30, 0.08), (0.24, 0.06), (0.21, 0.05), (0.19, 0.05), (0.18, 0.04), (0.17, 0.04), (0.17, 0.04), (0.17, 0.04), (0.19, 0.05), (0.28, 0.07)],
      [(0.30, 0.09), (0.24, 0.07), (0.21, 0.06), (0.19, 0.05), (0.17, 0.05), (0.16, 0.04), (0.16, 0.04), (0.17, 0.04), (0.19, 0.05), (0.27, 0.07)],
      [(0.27, 0.08), (0.22, 0.07), (0.19, 0.06), (0.17, 0.05), (0.16, 0.05), (0.15, 0.04), (0.15, 0.04), (0.15, 0.04), (0.17, 0.05), (0.25, 0.07)],
      [(0.22, 0.07), (0.18, 0.06), (0.16, 0.05), (0.14, 0.04), (0.13, 0.04), (0.12, 0.04), (0.12, 0.04), (0.12, 0.04), (0.14, 0.04), (0.20, 0.06)],
      [(0.13, 0.05), (0.11, 0.04), (0.10, 0.03), (0.09, 0.03), (0.08, 0.03), (0.08, 0.02), (0.07, 0.02), (0.08, 0.02), (0.09, 0.03), (0.12, 0.04)],
      [(0.03, 0.01), (0.02, 0.01), (0.018, 0.006), (0.016, 0.006), (0.015, 0.005), (0.014, 0.005), (0.014, 0.005), (0.014, 0.005), (0.016, 0.006), (0.023, 0.008)] ])
xi_dz_vals = np.array( [ (2.2,0.2), (2.4,0.2), (2.5,0.3), (2.5,0.3), (2.6,0.3), (2.5,0.5), (2.3,0.5), (1.9,0.5), (1.1,0.3), (0.21,0.06) ] )

# unumpy array
uRvals      = unumpy.uarray( Rvals[:,:,0], Rvals[:,:,1] )
uXiDzvals   = unumpy.uarray( xi_dz_vals[:,0], xi_dz_vals[:,1] )
uXiJpsivals = np.dot( uXiDzvals, 1/uRvals ) / 10

jpsi_avs = np.mean( Rvals[:,:,0], axis=1 )
jpsi_errs = 1/np.sqrt( np.sum( 1/Rvals[:,:,1]**2, axis=1 ) )
dz_avs = np.mean( Rvals[:,:,0], axis=0 )
dz_errs = 1/np.sqrt( np.sum( 1/Rvals[:,:,1]**2, axis=0 ) )

def plot_box():
  fig = plt.figure()
  ax = plt.gca()

  cx = 0.5*(dz_q2_bins[:-1]+dz_q2_bins[1:])
  for i in range(10):
    ax.errorbar( cx, Rvals[i,:,0], xerr=1, yerr=Rvals[i,:,1], fmt='none' )
  ax.fill_between( cx, dz_avs-dz_errs, dz_avs+dz_errs, alpha=0.5, color='0.5' )
  ax.plot(cx, dz_avs, 'k-', lw=3)

  cx = 0.5*(jpsi_q2_bins[:-1]+jpsi_q2_bins[1:])
  fig = plt.figure()
  ax = plt.gca()
  for i in range(10):
    ax.errorbar( cx, Rvals[:,i,0], xerr=0.5, yerr=Rvals[:,i,1], fmt='none')
  ax.fill_between( cx, jpsi_avs-jpsi_errs, jpsi_avs+jpsi_errs, alpha=0.5, color='0.5' )
  ax.plot(cx, jpsi_avs, 'k-' , lw=3)

def plot_ff():

  xi_dz  = (2.0,0.3)
  xi_dst = (5,2)

  pdg = (3.82, 0.24)

  gammaDz = np.linspace(15,50,100)
  VubDz     = np.sqrt( gammaDz / xi_dz[0] )
  VubDzerr  = (xi_dz[1]/xi_dz[0])*VubDz

  gammaDst = np.linspace(20,85,100)
  VubDst    = np.sqrt( gammaDst / xi_dst[0] )
  VubDsterr = (xi_dst[1]/xi_dst[0])*VubDst


  fig,ax = plt.subplots(1,2, figsize=(8,4))
  ax[0].fill_between(gammaDz, VubDz-VubDzerr, VubDz+VubDzerr, color='cornflowerblue', alpha=1)
  ax[0].plot( gammaDz, VubDz, 'w--')
  ax[0].fill_between(gammaDz, pdg[0]-pdg[1], pdg[0]+pdg[1], color='orange', alpha=0.5)
  ax[0].plot( gammaDz, np.full_like(gammaDz, pdg[0]), 'k--')
  ax[0].set_xlabel(r'$\Gamma(B_c^+\to D^0 \mu^+\nu)$ [$10^{-18}$ GeV]')
  ax[0].set_ylabel('$|V_{ub}|$ [$10^{-3}$]')
  ax[0].set_title(r'$\xi_{D^0} = (2.0 \pm 0.3) \times 10^{-3}$')

  ax[1].fill_between(gammaDst, VubDst-VubDsterr, VubDst+VubDsterr, color='cornflowerblue', alpha=1)
  ax[1].plot( gammaDst, VubDst, 'w--')
  ax[1].fill_between(gammaDst, pdg[0]-pdg[1], pdg[0]+pdg[1], color='orange', alpha=0.5)
  ax[1].plot( gammaDst, np.full_like(gammaDst, pdg[0]), 'k--')
  ax[1].set_xlabel(r'$\Gamma(B_c^+\to D^{*0} \mu^+\nu)$ [$10^{-18}$ GeV]')
  ax[1].set_ylabel('$|V_{ub}|$ [$10^{-3}$]')
  ax[1].set_title(r'$\xi_{D^{*0}} = (5 \pm 2) \times 10^{-3}$')

  fig.tight_layout()

def bf_rat_to_vubvcb(opt='quadoff', range=(8e-4,28e-4), plot=False,ret_funcs=False):

  # image capture points
  vals = np.loadtxt('ffs/vub_o_vcb_vs_bfr.log', delimiter=',')
  low  = np.loadtxt('ffs/vub_o_vcb_vs_bfr_errdn.log', delimiter=',')
  high = np.loadtxt('ffs/vub_o_vcb_vs_bfr_errup.log', delimiter=',')

  if opt=='linear':
    # straight line
    f = lambda x,m,c: m*x + c
    vals = np.hstack( vals[:,0], vals[:,1]**2 )
    low  = np.hstack( low[:,0], low[:,1]**2 )
    high = np.hstack( high[:,0], high[:,1]**2 )
    invf = lambda y,m,c: (y-c)/m

  elif opt=='quad':
    # square thing
    f = lambda x,r: (r*x)**0.5
    invf = lambda y,r: y**2/r

  elif opt=='quadoff':
    # square allowing offset
    f = lambda x,r,eps: (r*x)**0.5 + eps
    invf = lambda y,r,eps: (f-eps)**2 / r

  # fit the curves
  vopt , _ = curve_fit(f, vals[:,0], vals[:,1])
  elopt, _ = curve_fit(f, low[:,0] , low[:,1])
  ehopt, _ = curve_fit(f, high[:,0], high[:,1])

  # R estimate
  RFF = ufloat( vopt[0], abs(ehopt[0]-elopt[0])/2 )

  if plot:
    # draw the result
    x = np.linspace(*range,200)
    fig,ax = plt.subplots(figsize=(6,5))

    # plot extracted central
    ax.plot( vals[:,0], vals[:,1], 'ko', ms=3, zorder=5 )
    ax.plot( x, f(x,*vopt), 'k-',zorder=6)

    # plot extracted up and dn
    ax.plot( low[:,0], low[:,1], 'ro', ms=3 )
    ax.plot( high[:,0], high[:,1], 'ro', ms=3)
    ax.plot( x, f(x,*elopt), 'r-')
    ax.plot( x, f(x,*ehopt), 'r-')

    # plot prediction
    ax.fill_between(x, f(x,*elopt), f(x,*ehopt), alpha=0.5, color='cornflowerblue', zorder=0)
    ax.plot(x, f(x,*vopt), 'w--', zorder=1)

    ax.set_xlim(*range)
    ax.set_xticks( np.linspace(0.001,0.0025,4) )
    ax.tick_params(labelsize=14)
    ax.set_ylabel(r'$\frac{|V_{ub}|}{|V_{cb}|}$', fontsize=16)
    ax.set_xlabel(r'$\frac{\mathcal{B}(B_c^+\to D^0 \mu^+ \nu)}{\mathcal{B}(B_c^+\to J/\psi \mu^+ \nu)}$', fontsize=16)

    fig.tight_layout()
    fig.savefig('plots/rff_extraction.pdf')

    fig,ax = plt.subplots(figsize=(6,5))
    # plot prediction
    ax.fill_between(x, f(x,*elopt), f(x,*ehopt), alpha=0.5, color='cornflowerblue', zorder=0)
    ax.plot(x, f(x,*vopt), 'w--', zorder=1)
    ax.set_xlim(*range)
    ax.set_xticks( np.linspace(0.001,0.0025,4) )
    ax.tick_params(labelsize=14)
    ax.set_ylabel(r'$\frac{|V_{ub}|}{|V_{cb}|}$', fontsize=16)
    ax.set_xlabel(r'$\frac{\mathcal{B}(B_c^+\to D^0 \mu^+ \nu)}{\mathcal{B}(B_c^+\to J/\psi \mu^+ \nu)}$', fontsize=16)

    fig.tight_layout()
    fig.savefig('plots/rff.pdf')

  # define the functions to return which given a value of the BR returns the Vub/Vcb val and it's error
  v    = lambda x: f(x,*vopt)
  el   = lambda x: f(x,*elopt)
  eh   = lambda x: f(x,*ehopt)
  inv  = lambda x: invf(x,*vopt)
  inel = lambda x: invf(x,*elopt)
  ineh = lambda x: invf(x,*ehopt)

  if ret_funcs:
    return RFF, v, el, eh, inv, inel, ineh
  else:
    return RFF

def get_bfs():
  # from pdg
  Bc_tau = ufloat(0.510e-12,0.009e-12)
  hbar   = 6.58211951e-16 # in eV s
  # Bc total width
  Bc_width = hbar / Bc_tau
  print(Bc_width)

  # from Blazenka paper
  Bc_to_D0_width = ufloat(31e-9,6e-9)
  Bc_to_Dst_width = ufloat(85e-9,33e-9)

  BF_Bc_to_D0  = Bc_to_D0_width  / Bc_width
  BF_Bc_to_Dst = Bc_to_Dst_width / Bc_width

  BF_Bc_to_Jpsi = ufloat(2.24e-2,0.53e-2)

  print(BF_Bc_to_Jpsi)
  print(BF_Bc_to_Dst)
  print(BF_Bc_to_D0)

if __name__ == "__main__":
  #plot_box()
  plot_ff()
  RFF, v, el, eh, inv, inel, ineh = bf_rat_to_vubvcb(plot=True, ret_funcs=True)

  print( v(0.017) )
  print(RFF)
  print(1/RFF)

  plt.show()
