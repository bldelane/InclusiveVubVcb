import numpy as np
np.random.seed(210187)
import json
from uncertainties import ufloat, unumpy
import matplotlib.pyplot as plt

# import theory
from ffs import bf_rat_to_vubvcb
RFFth, bfr2vub, b2vel, b2veh, vub2bfr, v2bel, v2beh = bf_rat_to_vubvcb(ret_funcs=True, plot=True)

print('1/RFF from plot', RFFth)
print('RFF            ', 1/RFFth)

year = '2018'

N_dz   = ufloat(   445, 287 )
N_dst  = ufloat(  1397, 250 )
N_jpsi = ufloat( 18402, 400 )
N_bp   = ufloat( 35653, 190 )

# vub / vcb expected
pdg_vub_vcb = ufloat( 0.091, 0.006 )

# load efficiencies
with open('../../Efficiencies/jsons/Total.json') as f:
  effs = json.load(f)

# load lumis
with open('../../Efficiencies/jsons/Lumi.json') as f:
  lumis = json.load(f)

# average polarity effs
def average_effs( effs, channel, year, weights=(1,1) ):
  # check weights
  if len(weights)!=2:
    raise AttributeError('weights has wrong shape')
  # make sure weights sum to one
  weights = np.asarray(weights)
  weights = weights / np.sum(weights)
  try:
    mu = effs[channel][year]['MU']
    md = effs[channel][year]['MD']
  except:
    raise KeyError(channel, year, 'not found in effs dict')

  mu = weights[0]*ufloat( mu['eff'], mu['err'] )
  md = weights[1]*ufloat( md['eff'], md['err'] )

  return (mu+md)/2

# get eff corrected yield
def eff_corr_yield( effs, ylds ):
  return sum( effs * ylds )

# get lumi sum
def get_lumi_sum( lumis, channel, year ):
  try:
    mu = lumis[channel][year]['MU']
    md = lumis[channel][year]['MD']
  except:
    raise KeyError(channel, year, 'not found in lumis dict')

  mu = ufloat( mu['lumi'], mu['err'] )
  md = ufloat( md['lumi'], md['err'] )

  return mu+md

def plot_vub_vs_rff_from_bfr( bfr, mode='dz' ):

  xlim = (0.1,1)
  RFF    = np.linspace(*xlim,100)
  VubVcb = (bfr/RFF)**0.5

  vals = unumpy.nominal_values(VubVcb)
  errs = unumpy.std_devs(VubVcb)

  fig, ax = plt.subplots()

  # plot our thing
  ax.fill_between( RFF, vals-errs, vals+errs, alpha=0.5, color='skyblue')
  exp_band, = ax.fill( np.nan,np.nan, alpha=0.5, color='skyblue')
  exp_val,  = ax.plot( RFF, vals, color='dodgerblue', ls=':' )

  # plot pdg average
  ax.fill_between( RFF, pdg_vub_vcb.n-pdg_vub_vcb.s, pdg_vub_vcb.n+pdg_vub_vcb.s, alpha=0.5, color='indianred')
  pdg_band, = ax.fill( np.nan,np.nan, alpha=0.5, color='indianred')
  pdg_val,  = ax.plot( RFF, np.full_like(RFF,pdg_vub_vcb.n), color='darkred', ls='-' )

  # plot theoy pred
  ylim = ax.get_ylim()
  invRFFth = 1/RFFth
  RFF_th_low = invRFFth.n - invRFFth.s
  RFF_th_upp = invRFFth.n + invRFFth.s
  xpreds = RFF[ (RFF>RFF_th_low) & (RFF<RFF_th_upp) ]

  ax.fill_between( xpreds, ylim[0], ylim[1], alpha=0.5, color='lightgreen')
  th_band, = ax.fill( np.nan,np.nan, alpha=0.5, color='lightgreen')
  th_val,  = ax.plot( (invRFFth.n,invRFFth.n), ylim, color='forestgreen', ls='--' )

  ax.legend( [(exp_band,exp_val),(th_band,th_val),(pdg_band,pdg_val)], ["Experiment","Theory","PDG Average"], fontsize=14 )

  ax.set_ylim(*ylim)
  ax.tick_params(labelsize=14)
  ax.set_xlabel('$R_{FF}$', fontsize=14)
  ax.set_ylabel('$|V_{ub}|/|V_{cb}|$', fontsize=16)

  fig.tight_layout()
  fig.savefig('plots/vub_rff.pdf')
  fig.savefig('plots/vub_rff.png')

# efficiencies
eff_dz   = average_effs(effs, 'Bc2D0MuNu'   , year)
eff_dzg  = average_effs(effs, 'Bc2D0gMuNu'  , year)
eff_dzp  = average_effs(effs, 'Bc2D0pi0MuNu', year)
eff_dst  = 0.647*eff_dzp + 0.353*eff_dzg
eff_jpsi = average_effs(effs, 'Bc2JpsiMuNu' , year)
eff_bp   = average_effs(effs, 'Bu2D0MuNu'   , year)

# lumis
lum_dz   = get_lumi_sum(lumis, 'D0MuNu'  , year )
lum_jpsi = get_lumi_sum(lumis, 'JpsiMuNu', year )

# brs
br_dz_cf  = ufloat( 3.950e-2, 0.031e-2)
br_dz_dcs = ufloat( 1.364e-4, 0.026e-4)
br_jpsi   = ufloat( 5.961e-2, 0.033e-2)

# recall the relationship between what we measure and what we want to extract:
# N = xs * BR * eff * L
# relationship between BRs and Vub is |Vub|^2 / |Vcb|^2 = BRF * RFF
# lump these all as eff_rat
eff_rat_dz  = (eff_dz  / eff_jpsi ) * (br_dz_cf / br_jpsi ) * ( lum_dz / lum_jpsi )
eff_rat_dst = (eff_dst / eff_jpsi ) * (br_dz_cf / br_jpsi ) * ( lum_dz / lum_jpsi )

# our measurement for bfr is:
bfr_dz  = N_dz / N_jpsi / eff_rat_dz * np.random.normal(1,0.2)
bfr_dst = N_dst / N_jpsi / eff_rat_dst
bfr_D   = bfr_dz + bfr_dst

plot_vub_vs_rff_from_bfr( bfr_dz, mode='dz' )

# compute how many we expect given vub if ratio of form factors is 1
print('Effs: ', 'D0:', eff_dz, 'Jpsi', eff_jpsi, 'D0/Jpsi', eff_dz/eff_jpsi)
print('Lumis:', 'D0:', lum_dz, 'Jpsi', lum_jpsi, 'D0/Jpsi', lum_dz/lum_jpsi)
print('Eff*BF*L Ratio D0:',eff_rat_dz)
print('Eff*BF*L Ratio D*:',eff_rat_dst)
exp_rat_dz = eff_rat_dz * pdg_vub_vcb**2
exp_rat_dst = eff_rat_dst * pdg_vub_vcb**2

# measured
meas_rat_dz  = N_dz/N_jpsi
meas_rat_dst = N_dst/N_jpsi
meas_rat_D   = (N_dz+N_dst)/N_jpsi

RFF = np.linspace(0,1,100)

# figure showing our ratio of efficiencies*L*BF_sec multiplied by the PDG value
fig, ax = plt.subplots()
# Dz
vals = exp_rat_dz.n*RFF
upp  = (exp_rat_dz.n+exp_rat_dz.s)*RFF
low  = (exp_rat_dz.n-exp_rat_dz.s)*RFF
ax.fill_between( RFF, low, upp, alpha=0.5, color='cornflowerblue', label='Analysis efficiencies (D0/Jpsi) and PDG Vub/Vcb' )
ax.plot( RFF, vals, 'k--', lw=1)
# Dst
vals = exp_rat_dst.n*RFF
upp  = (exp_rat_dst.n+exp_rat_dst.s)*RFF
low  = (exp_rat_dst.n-exp_rat_dst.s)*RFF
ax.fill_between( RFF, low, upp, alpha=0.5, color='coral', label='Analysis efficiencies (D*/Jpsi) and PDG Vub/Vcb' )
ax.plot( RFF, vals, 'k--', lw=1)
# Theory
thpred = 1/RFFth
ax.fill_between( RFF[(RFF>thpred.n-thpred.s) & (RFF<thpred.n+thpred.s)], ax.get_ylim()[0], ax.get_ylim()[1], alpha=0.5, color='orange', label='Theory Pred')

ax.set_xlabel('$R_{FF}$')
ax.set_ylabel('Ratio of $D^0$ and $J/\psi$ Yields')
ax.legend()
fig.tight_layout()

#plt.fill_between( RFF, vals-exp_rat.n, vals+exp_rat.n, alpha=0.5, color='skyblue' )
#plt.plot( (RFF[0],RFF[-1]), (meas_rat_dz.n, meas_rat_dz.n),'r-')
#plt.plot( (RFF[0],RFF[-1]), (meas_rat_dst.n, meas_rat_dst.n),'r-')
#plt.plot( (RFF[0],RFF[-1]), (meas_rat_D.n, meas_rat_D.n),'r-')
#plt.plot( (RFFth.n,RFFth.n), (0,max(vals)), 'k--', lw=3)

#plt.fill_between( RFF, meas_rat_dz.n-meas_rat_dz.s, meas_rat_dz.n+meas_rat_dz.s, alpha=0.5, color='0.7' )
#plt.plot( RFF, np.full_like(RFF,meas_rat_dz.n), 'k-', lw=3 )

plt.show()
