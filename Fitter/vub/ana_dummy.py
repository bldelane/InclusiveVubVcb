import numpy as np
from uncertainties import ufloat, unumpy
import json
import matplotlib.pyplot as plt
from scipy.stats import chi2

# Blaise's extracted BF
r_BF_incl_Bl = ufloat(0.064,0.008)

# Blaise's fitted yields
#incl_sig_y = ufloat(3615,446)
incl_sig_y = ufloat(2300,(462**2 + 340**2)**0.5)
Jpsi_sig_y = ufloat(47486,873)

# PDG average
VubVcb_PDG = ufloat(0.091, 0.006)
VubVcb_PDG_incl = ufloat(0.101, 0.007)
VubVcb_PDG_excl = ufloat(0.094, 0.005)

# BF Dz -> Kpi
BF_Dz_CF  = ufloat(3.946e-2,0.030e-2)
BF_Dz_DCS = ufloat(1.50e-4,0.07e-4)

# BD Jpsi -> mumu
BF_Jpsi_mm = ufloat(5.961e-2,0.033e-2)

# BF ratio D*/D0 according to 3ptSR
r_BF_3ptSR = ufloat(2.92,1.34)

# FF integral D0, 3ptSR
FF_Dz_3ptSR = ufloat(0.002,0.0003)

# FF integral D*, 3ptSR
FF_Dst_3ptSR = ufloat(0.005,0.002)

# FF ratio D0 / Jpsi, 3ptSR (extraced from plot in Blazenka)
RFF_Dz_Jpsi_3ptSR = ufloat(0.36,0.12)

# FF integral Jpsi, 3ptSR (using extraction above)
FF_Jpsi_3ptSR = FF_Dz_3ptSR / RFF_Dz_Jpsi_3ptSR

# FF ratio Dst / Jpsi, 3ptSR (using extraction above)
RFF_Dst_Jpsi_3ptSR = FF_Dst_3ptSR / FF_Jpsi_3ptSR

# FF integral Jpsi, LCQD
FF_Jpsi_LQCD = ufloat(0.0113, 0.0008)

# FF ratio D0 / Jpsi, LQCD (from Cooper)
RFF_Dz_Jpsi_LQCD = ufloat(0.257, (0.036**2 + 0.018**2)**0.5)

# FF ratio Dst / Jpsi, LQCD (Use 3ptSR to predict LQCD Dst)
RFF_Dst_Jpsi_LQCD = r_BF_3ptSR * RFF_Dz_Jpsi_LQCD


# load effs
with open('../../Efficiencies/jsons/Total.json') as f:
  effsDict = json.load(f)

# load lumis
with open('../../Efficiencies/jsons/Lumi.json') as f:
  lumiDict = json.load(f)

def getEff(channel,year):
  if channel not in effsDict.keys():
    raise RuntimeError(channel, 'not in eff dict')
  if year not in effsDict[channel].keys():
    raise RuntimeError(year, 'not in eff dict')

  MU = effsDict[channel][year]['MU']['Total']
  MD = effsDict[channel][year]['MD']['Total']

  uMU = ufloat( MU['eff'], MU['err'] )
  uMD = ufloat( MD['eff'], MD['err'] )

  return (uMU+uMD)/2

def getLumi(channel,year):
  if channel not in lumiDict.keys():
    raise RuntimeError(channel, 'not in lumi dict')
  if year not in lumiDict[channel].keys():
    raise RuntimeError(year, 'not in lumi dict')

  MU = lumiDict[channel][year]['MU']
  MD = lumiDict[channel][year]['MD']

  uMU = ufloat( MU['lumi'], MU['err'] )
  uMD = ufloat( MD['lumi'], MD['err'] )

  return (uMU+uMD)

def getTotLumi(mode,years):
  lumis = [ getLumi(mode,year) for year in years ]
  return sum(lumis)

def getLumiWeightedEff(channel, years):
  mode = 'JpsiMuNu' if 'Jpsi' in channel else 'D0MuNu'

  lumis = [ getLumi(mode,year) for year in years ]
  effs  = [ getEff(channel,year) for year in years ]

  totL = sum(lumis)

  avEff = sum( [ (lumis[i]*effs[i])/totL for i in range(len(years)) ] )

  return avEff

def VubVcbFromRFFAndBFR(RFF,BFR):
  return (BFR/RFF)**0.5

def plot_vub_vs_rff_from_bfr(BFR, pdg_lines=[], rff_lines=[], use2dExp=True, ylim=None):

  # rff lines can be list of tuples with [ (val,name,fillcolor,linecolor) ]

  xlim = (0.1,4)
  RFF = np.linspace(*xlim,200)
  VubVcb = VubVcbFromRFFAndBFR(RFF,BFR)

  vals = unumpy.nominal_values(VubVcb)
  errs = unumpy.std_devs(VubVcb)

  fig,ax = plt.subplots()

  # plot the experimental result
  # this may take some digesting but because this is now a 2D plot which we want to project into 1D
  # the band will not contain the right fraction of the population unless we increase the uncertainty by the
  # appropriate fraction which takes us from a 1D result to a 2D band
  expfact = chi2.ppf(chi2.cdf(1,1),2)**0.5 if use2dExp else 1
  # draw
  ax.fill_between( RFF, vals-expfact*errs, vals+expfact*errs, alpha=0.5, color='skyblue')
  # for legend
  exp_band, = ax.fill( np.nan,np.nan, alpha=0.5, color='skyblue')
  exp_val,  = ax.plot( RFF, vals, color='dodgerblue', ls=':' )

  # plot pdg lines
  for line in pdg_lines:
    low = line.n - line.s
    high = line.n + line.s
    # draw
    ax.fill_between( RFF, low, high, alpha=0.5, color='indianred')
    # for legend
    pdg_band, = ax.fill( np.nan,np.nan, alpha=0.5, color='indianred')
    pdg_val,  = ax.plot( RFF, np.full_like(RFF,line.n), color='darkred', ls='-' )

  # plot rff lines
  ylim = ylim or ax.get_ylim()
  th_names = []
  th_legs  = []
  for line in rff_lines:
    fcol = 'lightgreen'
    lcol = 'forestgreen'
    name = 'Theory'
    if hasattr(line,'__iter__'):
      low = line[0].n - line[0].s
      high = line[0].n + line[0].s
      if len(line)>1: name = line[1]
      if len(line)>2: fcol = line[2]
      if len(line)>3: lcol = line[3]
      line = line[0]
    else:
      low = line.n - line.s
      high = line.n + line.s

    xpreds = RFF[ (RFF>low) & (RFF<high) ]
    # draw
    ax.fill_between( xpreds, ylim[0], ylim[1], alpha=0.5, color=fcol)
    # for legend
    th_band, = ax.fill( np.nan,np.nan, alpha=0.5, color=fcol)
    th_val,  = ax.plot( (line.n,line.n), ylim, color=lcol, ls='--' )
    th_legs.append( (th_band, th_val) )
    th_names.append(name)

  ax.legend( [(exp_band,exp_val)]+th_legs+[(pdg_band,pdg_val)], ["Experiment"]+th_names+["PDG Average"], fontsize=14 )
  ax.set_ylim(*ylim)
  ax.tick_params(labelsize=14)
  ax.set_xlabel('$R_{FF}$', fontsize=14)
  ax.set_ylabel('$|V_{ub}|/|V_{cb}|$', fontsize=16)

  fig.tight_layout()
  fig.savefig('plots/vub_rff.pdf')
  fig.savefig('plots/vub_rff.png')
  plt.show()

if __name__ == '__main__':

  years = ['2016','2017','2018']

  # average efficiencies
  eff_Dz = getLumiWeightedEff('Bc2D0MuNu',years)
  eff_Dst = 0.647 * getLumiWeightedEff('Bc2D0pi0MuNu',years) + 0.353 * getLumiWeightedEff('Bc2D0gMuNu',years)

  eff_Dincl = (r_BF_3ptSR/(1+r_BF_3ptSR))*eff_Dst + (1/(1+r_BF_3ptSR))*eff_Dz

  eff_Jpsi  = getLumiWeightedEff('Bc2JpsiMuNu',years)

  # total lumis
  lumi_Dincl = getTotLumi('D0MuNu', years)
  lumi_Jpsi  = getTotLumi('JpsiMuNu',years)

  print('RFF Dz/Jpsi 3ptSR:   ', RFF_Dz_Jpsi_3ptSR)
  print('RFF Dst/Jpsi 3ptSR:  ', RFF_Dst_Jpsi_3ptSR)
  print('RFF D(*)/Jpsi 3ptSR: ', RFF_Dz_Jpsi_3ptSR + RFF_Dst_Jpsi_3ptSR)

  print('RFF Dz/Jpsi LQCD:    ', RFF_Dz_Jpsi_LQCD)
  print('RFF Dst/Jpsi LQCD:   ', RFF_Dst_Jpsi_LQCD)
  print('RFF D(*)/Jpsi LQCD:  ', RFF_Dz_Jpsi_LQCD + RFF_Dst_Jpsi_LQCD)

  print('-'*20)

  print('Inclusive D0 or D* eff', eff_Dincl)
  print('Jpsi eff', eff_Jpsi)
  print('Lumi D', lumi_Dincl)
  print('Lumi J', lumi_Jpsi)

  print('-'*20)

  r_BF_incl = ( incl_sig_y / Jpsi_sig_y ) * ( eff_Jpsi / eff_Dincl ) * ( BF_Jpsi_mm / BF_Dz_CF ) * ( lumi_Jpsi / lumi_Dincl )

  print('Inclusive BF:', r_BF_incl)

  print('-'*20)

  print('VubVcb_PDG', VubVcb_PDG )

  # VubVcb using 3ptSR
  VubVcb_3ptSR = VubVcbFromRFFAndBFR( RFF_Dz_Jpsi_3ptSR + RFF_Dst_Jpsi_3ptSR, r_BF_incl )

  # VubVcb using LQCD
  VubVcb_LQCD  = VubVcbFromRFFAndBFR( RFF_Dz_Jpsi_LQCD + RFF_Dst_Jpsi_LQCD, r_BF_incl )

  print('VubVcb_3ptSR', VubVcb_3ptSR)
  diff = VubVcb_3ptSR - VubVcb_PDG
  nsig = diff.n / diff.s
  print('Compatibility', nsig)

  print('VubVcb_LQCD', VubVcb_LQCD)
  diff = VubVcb_LQCD - VubVcb_PDG
  nsig = diff.n / diff.s
  print('Compatibility', nsig)

  # our observed plot
  plot_vub_vs_rff_from_bfr(r_BF_incl, pdg_lines=[VubVcb_PDG], rff_lines=[(RFF_Dz_Jpsi_LQCD+RFF_Dst_Jpsi_LQCD,'LQCD','mediumpurple','indigo'),(RFF_Dz_Jpsi_3ptSR+RFF_Dst_Jpsi_3ptSR,'3ptSR','lightgreen','forestgreen') ], ylim=(0,0.4))

  # dummy plot based on a random shift from the PDG
  r_BF_dummy = ( VubVcb_PDG**2 ) * ( RFF_Dz_Jpsi_3ptSR + RFF_Dst_Jpsi_3ptSR )
  r_BF_dummy = ufloat(r_BF_dummy.n, r_BF_incl.s)
  np.random.seed(210187)
  r_BF_dummy  = ufloat( np.random.normal(r_BF_dummy.n, r_BF_dummy.s), r_BF_dummy.s )

  #plot_vub_vs_rff_from_bfr(r_BF_dummy, pdg_lines=[VubVcb_PDG], rff_lines=[RFF_Dz_Jpsi_3ptSR+RFF_Dst_Jpsi_3ptSR])

