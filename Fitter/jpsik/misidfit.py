import os
import numpy as np
import uproot
import matplotlib.pyplot as plt
import pickle

os.system('mkdir -p plots')

def read(files,cut=None,vars=["B_M"], flat=False):

  df = uproot.concatenate( files, vars, cut, library="pd" )
  print(len(df))
  if flat: return df[vars[0]].to_numpy()

  return df

read_root = False

if read_root or not os.path.exists('misid.npy'):
  mc = read( ["root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagDown/B2Jpsimumupi.root:B2JpsimmKTuple/DecayTree",
              "root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagUp/B2Jpsimumupi.root:B2JpsimmKTuple/DecayTree"],
              "Bplus_BKGCAT==30",
              vars=["Bplus_DTF_M"],
              flat = True)
  np.save('misid.npy', mc)

mc = np.load('misid.npy')

from iminuit import Minuit
from iminuit.cost import BinnedNLL, UnbinnedNLL
from models import cbr, cbr_cdf, dcb, dcb_cdf, dcbwg, dcbwg_cdf

mrange = (5200,5600)
mbins = 400 # nbins for fitting
pbins = 100 # nbins for plotting

# cb
#pdf = lambda x, mur, sgr, ar, nr: cbr(x,mur,sgr,ar,nr,mrange)
#cdf = lambda x, mur, sgr, ar, nr: cbr_cdf(x,mur,sgr,ar,nr,mrange)
# dcb
pdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)
cdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb_cdf(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)
# dcb wg (independent means)
#pdf = lambda x, f1, f2, mug, mul, mur, sgg, sgl, sgr, al, ar, nl, nr: dcbwg(x,f1,f2,mug,mul,mur,sgg,sgl,sgr,al,ar,nl,nr,mrange)
#cdf = lambda x, f1, f2, mug, mul, mur, sgg, sgl, sgr, al, ar, nl, nr: dcbwg_cdf(x,f1,f2,mug,mul,mur,sgg,sgl,sgr,al,ar,nl,nr,mrange)
# dcb wg (same means)
#pdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg(x,f1,f2,mug,mug,mug,sgg,sgl,sgr,al,ar,nl,nr,mrange)
#cdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg_cdf(x,f1,f2,mug,mug,mug,sgg,sgl,sgr,al,ar,nl,nr,mrange)

nh, xe = np.histogram(mc, range=mrange, bins=mbins)

#mi = Minuit( UnbinnedNLL( mc, pdf ),
mi = Minuit( BinnedNLL( nh, xe, cdf ),
             f  = 0.5,
             #f1  = 0.1,
             #f2  = 0.5,
             #mug = 5330,
             mul = 5330,
             mur = 5330,
             #sgg = 80,
             sgl = 15,
             sgr = 15,
             al  = 1.7,
             ar  = 0.5,
             nl  = 3,
             nr  = 3
           )
mi.limits['f'] = (0,1)
#mi.limits['f1'] = (0,1)
#mi.limits['f2'] = (0,1)
#mi.limits['mug'] = (5300,5500)
mi.limits['mul'] = (5300,5500)
mi.limits['mur'] = (5300,5500)
#mi.limits['sgg'] = (0,200)
mi.limits['sgl'] = (0,30)
mi.limits['sgr'] = (0,30)
mi.limits['al'] = (0,3)
mi.limits['ar'] = (0,3)
mi.limits['nl'] = (1,5)
mi.limits['nr'] = (1,5)
#mi.fixed['nl'] = True
#mi.fixed['nr'] = True

do_mc_fit=True
if do_mc_fit:
  mi.migrad()
  mi.hesse()
  print(mi)
  with open('misidpars.log','w') as f:
    for par in mi.parameters:
      f.write('{:10s} {:f}\n'.format(par, mi.values[par]))
else:
  with open('misidpars.log') as f:
    for line in f.readlines():
      mi.values[line.split()[0]] = float(line.split()[1])

fig, ax = plt.subplots(2,1,figsize=(8,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})

nh, xe = np.histogram(mc, range=mrange, bins=pbins)
cx = 0.5 * (xe[1:]+xe[:-1])

ax[0].errorbar( cx, nh, nh**0.5, fmt='ko', capsize=2, label='MC' )

x = np.linspace(*mrange,400)
N = len(mc)*(mrange[1]-mrange[0])/pbins

ax[0].plot(x, N*pdf(x,*mi.values), label='MC Fit')

pull = (nh-N*pdf(cx,*mi.values))/(nh**0.5)

# plot box and line at zero
ax[1].fill_between(x, -1, 1, alpha=0.5, color='0.5')
ax[1].plot(x, np.zeros_like(x), 'b-')

# plot pull
ax[1].errorbar( cx, pull, np.ones_like(cx), capsize=2, fmt='ko' )
ylim = ax[1].get_ylim()

# symmetrise pull y-axis
y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-y,y)

# labels
ax[1].set_xlabel('$m(B^+)$ [MeV]')
ax[0].set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/100) ) )
ax[1].set_ylabel('Pull')
ax[0].legend()

fig.tight_layout()
fig.savefig('plots/misidfit.pdf')
fig.savefig('plots/misidfit.png')

ax[0].set_yscale('log')
fig.tight_layout()
fig.savefig('plots/misidfit_log.pdf')
fig.savefig('plots/misidfit_log.png')

plt.show()
