import os
import numpy as np
import uproot
import matplotlib.pyplot as plt
import pickle
import pandas as pd

os.system('mkdir -p plots')

def read(files,cut=None,vars=["B_M"], flat=False):

  df = uproot.concatenate( files, vars, cut, library="pd" )
  print(len(df))
  if flat: return df[vars[0]].to_numpy()

  return df

read_root = False

if read_root or not os.path.exists('data.pkl'):
  data = read( ["root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/data/2018/MagDown/all.root:B2JpsimmKTuple/DecayTree",
                "root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/data/2018/MagUp/all.root:B2JpsimmKTuple/DecayTree"],
                "(Kplus_P>10e3) & (Kplus_P<100e3) & (Kplus_PT>1.5e3) & (Kplus_PT<20e3) & (Kplus_PIDK>0) & (muplus_L0MuonDecision_Dec | muminus_L0MuonDecision_Dec)",
                vars=["Bplus_DTF_M","Kplus_P","Kplus_PT","Bplus_L0Global_TIS"],
                flat = False)
  data.to_pickle('data.pkl')

# read data
data = pd.read_pickle('data.pkl')
tos = data['Bplus_DTF_M'].to_numpy()
tis = data[ data['Bplus_L0Global_TIS'] ]['Bplus_DTF_M'].to_numpy()

# read mc fit pars
mcpars = {}
with open('mcpars.log') as f:
  for line in f.readlines():
    mcpars[line.split()[0]] = float(line.split()[1])
midpars = {}
with open('misidpars.log') as f:
  for line in f.readlines():
    midpars[line.split()[0]] = float(line.split()[1])

from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL
from models import dcb, dcb_cdf, dcbwg, dcbwg_cdf
from scipy.stats import expon

mrange = (5200,5400)
mbins = 400 # nbins for fitting
pbins = 100 # nbins for plotting

def pdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb, comps=None):
  if comps is None:
    comps = ['sig','bkg','mid']

  tot = 0

  sig = dcbwg(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  if 'sig' in comps:
    tot += ns * sig

  mid = dcb(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)
  if 'mid' in comps:
    tot += nm * mid

  exp = expon(mrange[0],lb)
  bkg  = exp.pdf(x) / np.diff( exp.cdf(mrange) )
  if 'bkg' in comps:
    tot += nb * bkg

  return tot

def cdf(x, ns, nb, nm, mug, sgg, sgl, sgr, mum, lb):

  sig = dcbwg_cdf(x,mcpars['f1'],mcpars['f2'],mug,mug,mug,sgg,sgl,sgr,mcpars['al'],mcpars['ar'],mcpars['nl'],mcpars['nr'],mrange)
  mid = dcb_cdf(x, midpars['f'], mum, mum, midpars['sgl'], midpars['sgr'], midpars['al'], midpars['ar'], midpars['nl'], midpars['nr'],mrange)

  exp = expon(mrange[0],lb)
  bkg = exp.cdf(x) / np.diff( exp.cdf(mrange) )

  return ns*sig + nb*bkg + nm*mid

def fit(data, cdf):

  nh, xe = np.histogram(data, range=mrange, bins=mbins)

  mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf ),
               ns = 0.98*len(data),
               nb = 0.02*len(data),
               nm = 0.01*len(data),
               mug = 5280,
               sgg = 7,
               sgl = 10,
               sgr = 10,
               mum = 5330,
               lb  = 200
             )

  mi.limits['ns'] = (0, len(data))
  mi.limits['nb'] = (0, len(data))
  mi.limits['nm'] = (0, len(data))
  mi.limits['mug'] = (5240,5320)
  mi.limits['sgg'] = (0,20)
  mi.limits['sgl'] = (0,20)
  mi.limits['sgr'] = (0,20)
  mi.limits['mum'] = (5300,5350)
  mi.limits['lb']  = (10,500)

  mi.migrad()
  mi.hesse()
  print(mi)

  return mi

def plot(axt, axb, data, pdf):
  nh, xe = np.histogram(data, range=mrange, bins=pbins)
  cx = 0.5 * (xe[1:]+xe[:-1])

  axt.errorbar( cx, nh, nh**0.5, fmt='ko', capsize=2, label='Data' )

  x = np.linspace(*mrange,400)
  N = (mrange[1]-mrange[0])/pbins

  axt.plot(x, N*pdf(x, comps=['bkg']), 'r--', label='Background')
  axt.plot(x, N*pdf(x, comps=['sig']), 'g:' , label='Signal')
  axt.plot(x, N*pdf(x, comps=['mid']), 'm:' , label='Mis ID')
  axt.plot(x, N*pdf(x), 'b-', label='Total PDF')

  pull = (nh-N*pdf(cx))/(nh**0.5)

  # plot box and line at zero
  axb.fill_between(x, -1, 1, alpha=0.5, color='0.5')
  axb.plot(x, np.zeros_like(x), 'b-')

  # plot pull
  axb.errorbar( cx, pull, np.ones_like(cx), capsize=2, fmt='ko' )
  ylim = axb.get_ylim()

  # symmetrise pull y-axis
  y = max(abs(ylim[0]),abs(ylim[1]))
  axb.set_ylim(-y,y)

  # labels
  axb.set_xlabel('$m(B^+)$ [MeV]')
  axt.set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/100) ) )
  axb.set_ylabel('Pull')
  axt.legend()

# fit tos sample
tos_mi = fit( tos, cdf )
tos_pdf = lambda x, comps=None: pdf(x,*tos_mi.values,comps)

# fit tis sample
tis_mi = fit( tis, cdf )
tis_pdf = lambda x, comps=None: pdf(x,*tis_mi.values,comps)

print('nTOS = {:f} +/- {:f}'.format(tos_mi.values['ns'], tos_mi.errors['ns']))
print('nTIS = {:f} +/- {:f}'.format(tis_mi.values['ns'], tis_mi.errors['ns']))
import uncertainties as u
ntos = u.ufloat( tos_mi.values['ns'], tos_mi.errors['ns'] )
ntis = u.ufloat( tis_mi.values['ns'], tis_mi.errors['ns'] )
eff = ntis / ntos
print('Efficiency = {:f} +/- {:f}'.format(eff.n,eff.s))

# make plots
fig, ax = plt.subplots(2,2,figsize=(16,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})

plot(ax[0,0], ax[1,0], tos, tos_pdf )
plot(ax[0,1], ax[1,1], tis, tis_pdf )

ax[0,0].set_title('TOS')
ax[0,1].set_title('TIS')

fig.tight_layout()
fig.savefig('plots/datafit.pdf')
fig.savefig('plots/datafit.png')

ax[0,0].set_yscale('log')
ax[0,1].set_yscale('log')
fig.tight_layout()
fig.savefig('plots/datafit_log.pdf')
fig.savefig('plots/datafit_log.png')

plt.show()
