import os
import numpy as np
import uproot
import matplotlib.pyplot as plt
import pickle

os.system('mkdir -p plots')

def read(files,cut=None,vars=["B_M"], flat=False):

  df = uproot.concatenate( files, vars, cut, library="pd" )
  print(len(df))
  if flat: return df[vars[0]].to_numpy()

  return df

read_root = False

if read_root or not os.path.exists('mc.npy'):
  mc = read( ["root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagDown/B2JpsimumuK.root:B2JpsimmKTuple/DecayTree",
              "root://eoslhcb.cern.ch//eos/lhcb/user/a/atully/B2emu/MC/2018/MagUp/B2JpsimumuK.root:B2JpsimmKTuple/DecayTree"],
              "Bplus_BKGCAT==0",
              vars=["Bplus_DTF_M"],
              flat = True)
  np.save('mc.npy', mc)

mc = np.load('mc.npy')

from iminuit import Minuit
from iminuit.cost import BinnedNLL, UnbinnedNLL
from models import dcb, dcb_cdf, dcbwg, dcbwg_cdf

mrange = (5200,5400)
mbins = 400 # nbins for fitting
pbins = 100 # nbins for plotting

# dcb
#pdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)
#cdf = lambda x, f, mul, mur, sgl, sgr, al, ar, nl, nr: dcb_cdf(x,f,mul,mur,sgl,sgr,al,ar,nl,nr,mrange)
# dcb wg (independent means)
#pdf = lambda x, f1, f2, mug, mul, mur, sgg, sgl, sgr, al, ar, nl, nr: dcbwg(x,f1,f2,mug,mul,mur,sgg,sgl,sgr,al,ar,nl,nr,mrange)
#cdf = lambda x, f1, f2, mug, mul, mur, sgg, sgl, sgr, al, ar, nl, nr: dcbwg_cdf(x,f1,f2,mug,mul,mur,sgg,sgl,sgr,al,ar,nl,nr,mrange)
# dcb wg (same means)
pdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg(x,f1,f2,mug,mug,mug,sgg,sgl,sgr,al,ar,nl,nr,mrange)
cdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg_cdf(x,f1,f2,mug,mug,mug,sgg,sgl,sgr,al,ar,nl,nr,mrange)

nh, xe = np.histogram(mc, range=mrange, bins=mbins)

#mi = Minuit( UnbinnedNLL( mc, pdf ),
mi = Minuit( BinnedNLL( nh, xe, cdf ),
             f1  = 0.3,
             f2  = 0.5,
             mug = 5280,
             #mul = 5280,
             #mur = 5280,
             sgg = 30,
             sgl = 30,
             sgr = 30,
             al  = 1,
             ar  = 1,
             nl  = 2,
             nr  = 2
           )
mi.limits['f1'] = (0,1)
mi.limits['f2'] = (0,1)
mi.limits['mug'] = (5250,5300)
#mi.limits['mul'] = (5250,5300)
#mi.limits['mur'] = (5250,5300)
mi.limits['sgg'] = (0,50)
mi.limits['sgl'] = (0,50)
mi.limits['sgr'] = (0,50)
mi.limits['al'] = (0,10)
mi.limits['ar'] = (0,10)
mi.limits['nl'] = (0,10)
mi.limits['nr'] = (0,10)

do_mc_fit=True
if do_mc_fit:
  mi.migrad()
  mi.hesse()
  print(mi)
  with open('mcpars.log','w') as f:
    for par in mi.parameters:
      f.write('{:10s} {:f}\n'.format(par, mi.values[par]))
else:
  with open('mcpars.log') as f:
    for line in f.readlines():
      mi.values[line.split()[0]] = float(line.split()[1])

fig, ax = plt.subplots(2,1,figsize=(8,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})

nh, xe = np.histogram(mc, range=mrange, bins=pbins)
cx = 0.5 * (xe[1:]+xe[:-1])

ax[0].errorbar( cx, nh, nh**0.5, fmt='ko', capsize=2, label='MC' )

x = np.linspace(*mrange,400)
N = len(mc)*(mrange[1]-mrange[0])/pbins

ax[0].plot(x, N*pdf(x,*mi.values), label='MC Fit')

pull = (nh-N*pdf(cx,*mi.values))/(nh**0.5)

# plot box and line at zero
ax[1].fill_between(x, -1, 1, alpha=0.5, color='0.5')
ax[1].plot(x, np.zeros_like(x), 'b-')

# plot pull
ax[1].errorbar( cx, pull, np.ones_like(cx), capsize=2, fmt='ko' )
ylim = ax[1].get_ylim()

# symmetrise pull y-axis
y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-y,y)

# labels
ax[1].set_xlabel('$m(B^+)$ [MeV]')
ax[0].set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/100) ) )
ax[1].set_ylabel('Pull')
ax[0].legend()

fig.tight_layout()
fig.savefig('plots/mcfit.pdf')
fig.savefig('plots/mcfit.png')

ax[0].set_yscale('log')
fig.tight_layout()
fig.savefig('plots/mcfit_log.pdf')
fig.savefig('plots/mcfit_log.png')

plt.show()
