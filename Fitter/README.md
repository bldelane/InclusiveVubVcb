# Fitting Setup and Framework

After some simlar inspiration from the pipeline the fitting setup and fit is now run by Snakemake. This will perform a few steps

- Fit the D0 and Jpsi MC files to get an idea of the signal (and misID) shapes in D0 and Jpsi mass
- Then fit the D0 and Jpsi Data to actually obtain these shapes in data (along with that for the background) in D0 and Jpsi mass
- Then it will read in some binning configuration file defined in `config/binnings` and prepare a tree so that fits to the number of D0 and Jpsi
  candidates can be performed in each template bin
- Then perform the bin by bin yield fits which will give us all the relevant templates we need from data
- In parallel it will get the signal template shapes from MC
- It will then make some plots for you and put them on the web if you  like

**These steps do all the preparation for the fit and will save .pkl files of boost-histograms containing each template**
Run it by simply executing
``` bash
snakemake -j8 <options>
```

**It is advisable especially the first time to do this in steps and check the progress as you go:**

- `snakemake --until get_mc_shapes -j 4`
- `snakemake --until get_data_shapes -j 12`
- `snakemake --until perform_bin_yield_fits -j 8`
- `snakemake -j 8`

**You can look at the output of one of the most recent runs here:**
[https://www.hep.phy.cam.ac.uk/~mkenzie//Bc2D0MuNuX/Fit3/datashapes/D0MuNu/]

The actual template fit is then run using these templates using the `Exectuable/PerformTemplateFit.py` script.

**A few notes**:
- At the moment there is only MC in 2012 and 2016 so the MC templates (and efficiencies) are just computed as `run1` and `run2`
- The data is processed independently in each year (2011-2018)
- After our next MC request this will become standalone for each year


## Compute the efficiencies

After the snakemake workflow has run (see section below)

- There is a script which amalgamates all the different efficiencies in the `../Efficiencies` folder.
- This write a file called `../Efficiencies/CalculatedEfficiencies.log`
- There's also scripts in there which provide the BF ratios we need and the luminosity
- This will provide us with Run 1 and Run 2 efficiency ratios:
  1. eff(Bc -> D0MuNu) / eff(Bc -> JpsiMuNu)
  2. eff(Bc -> D\*0MuNu) / eff(Bc -> JpsiMuNu)
  3. eff(Bc -> D0MuNu) / eff(Bu -> D0MuNu)
- We can then parameterise our yields with:
  - f1 = N(Bc -> D0MuNu)/N(Bc -> JpsiMuNu) =               ( |Vub|^2 / |Vcb|^2 ) * ( eff(Bc -> D0MuNu) / eff(Bc -> JpsiMuNu) ) * ( L(Bc -> D0MuNu) / L(Bc -> JpsiMuNU) ) * ( BR(D0 -> K- pi+) / BR(Jpsi -> mu mu) )
  - f2 = N(Bc -> D0MuNu)/N(Bu -> D0MuNu)   = ( fc / fu ) * ( |Vub|^2 / |Vcv|^2 ) * ( eff(Bc -> D0MuNu) / eff(Bu -> D0MuNu  ) ) * ( BR(D0 -> K- pi+) / BR(D0 -> K+ pi-) )

## Running the fit setup with snakemake

The setup is configured with the `config/config.yml` file. It should be pretty obvious what is done in there but the things worth checking
and potentially changing are `topdir` and `webloc`.

Because our analysis pipeline is run on the cambridge cluster and thus the files are produced there it's most straightforward to run this setup part at cambridge as well.
However the only thing the fit setup needs access to is the pipeline's `fit` directories.

By default this fit setup stuff will write into directories (in the pipeline folder) called `fit_cache` and `fit_hists`. So to re-run everything you need to either delete
these or move them to somewhere else.

It's always worth a quick check by running:
```bash
snakemake --dryrun
```
first.

The entire process will take quiet a while if you try it locally so depending on the status of the batch queues it's usually worth a condor submission to run it through:
```bash
snakemake --cluster config/condor_wrapper.py --jobs 80 --latency-wait 120
```

In the subsections below there is a description of what each step in the workflow does (i.e. what scripts Snakemake ends up calling).

### Define the binning scheme for the template fit

This is a crucial part and must happen before we start the fitting workflow. We have a certain number of modes to fit (at the moment this is just D0MuNu and JpsiMuNu but later we will probably add the DK3Pi mode as well). These can each have a different binning definition.

The binning definitions live in a folder called `config/binnings` and are `.json` files holding a dictionary which configures the binning. You need to give the binning and a name and then some axes (which can be any number of dimensions). The axes define the binning requiring:
 - an `observable` which must match the branchname in the tuples
 - a number of `bins`
 - a fit `range`
You can optionally also give
 - a `title`
 - some `extra_bins` in the format [-x,y] which will compute the yields from some additional bins (-x bins on the low side and y bins on the high side). This is useful when we come to plotting our fit result because we can inspect the behaviour of the templates outside of the given fit range.

A `nominal` binning definition has been provided which uses the same binning and same dimensions for the D0 and Jpsi modes.

### Run some setup stuff ##

Step required:

1. Get D0 and Jpsi mass shapes from MC
2. Get D0 and Jpsi mass shapes from all data fit
3. Now prepare some trees given a template binning
4. Now get the bin by bin yields and save them in histograms
5. Extract the equivalent histograms for MC
6. Combine the D0g and D0pi0 templates into one D\*0 template

#### Step 1. Get the D0 and Jpsi mass shapes from fit to MC ###

Here we want to get an idea of what the shapes are like from truth matched signal MC. We will fix the tail paramters from the MC fit but then get the data shape allowing the mean and width parameters to float or scale appropriately in the next step.

*For the D0*
- We want a shape for the signal MC and we also want to perform a PID swap on K or pi to get the misID shapes
```bash
python Executables/PerformD0MCFit.py -i FitInput/MC_Bc2D0MuNu_2012.root -o FitCache/d0_mc_shapes_run1.root -p plots/MCShapes/D0MuNu/run1
python Executables/PerformD0MCFit.py -i FitInput/MC_Bc2D0MuNu_2016.root -o FitCache/d0_mc_shapes_run2.root -p plots/MCShapes/D0MuNu/run2
```

*For the Jpsi*
- In this case we just need a shape for the signal
```bash
python Executables/PerformJpsiMCFit.py -i FitInput/MC_Bc2JpsiMuNu_2012.root -o FitCache/jpsi_mc_shapes_run1.root -p plots/MCShapes/JpsiMuNu/run1
python Executables/PerformJpsiMCFit.py -i FitInput/MC_Bc2JpsiMuNu_2016.root -o FitCache/jpsi_mc_shapes_run2.root -p plots/MCShapes/JpsiMuNu/run2
```

#### Step 2. Get the D0 and Jpsi mass shapes from fit to DATA ###

Using our MC fits from Step 1 as a starting point we now want to fit the total dataset for the shapes (including the background shapes) in the D0 and Jpsi mass.
We will now have the shapes in Data for signal and background which in a later step (Step 4) we will freeze as we then go through each bin of our histogram fitting just the yields.
This will then tell us in each bin of our template fit how many real D0 / Jpsi events there are.

*For the D0*
- In this case we do a simultaneous fit across four categories (CF,DCS)x(Sig,misID). We include components for the PID swaps where relevant and also get a shape for the combinatorial background in each category
```bash
for year in 2011 2012 2015 2016 2017 2018
do
  if [ ${year} == "2011" ] || [ ${year} == "2012" ]
  then
    run="run1"
  else
    run="run2"
  fi
  python Executables/PerformD0ShapesFit.py -s FitInput/DATA_D0MuNu_sig_${year}.root -m FitInput/DATA_D0MuNu_misid_${year}.root -M FitCache/d0_mc_shapes_${run}.root -o FitCache/d0_data_shapes_${year}.root -p plots/DataShapes/D0MuNu/${year}
done
```

*For the Jpsi*
- In this case we just get shapes for the signal and combinatorial background
```bash
for year in 2011 2012 2015 2016 2017 2018
do
  if [ ${year} == "2011" ] || [ ${year} == "2012" ]
  then
    run="run1"
  else
    run="run2"
  fi
  python Executables/PerformJpsiShapesFit.py -s FitInput/DATA_JpsiMuNu_sig_${year}.root -m FitInput/DATA_JpsiMuNu_misid_${year}.root -M FitCache/jpsi_mc_shapes_${run}.root -o FitCache/jpsi_data_shapes_${year}.root -p plots/DataShapes/JpsiMuNu/${year}
done
```

#### Step 3. Prepare trees given a binning

Now we want to actually read in a binning configuration (i.e. what we will use for the template fit). This step simply gets some trees prepped for this by reading the binning configuration and then adding a branch which tells us which bin a given event is in.

```bash
binning="nominal"
for year in 2011 2012 2015 2016 2017 2018
do
  for comp in sig misid comb
  do
    python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_D0MuNu_${comp}_${year}.root   -o FitCache/DATA_D0MuNu_${comp}_${year}_w${binning}bins.root   -b config/binnings/${binning}_d0.json   -m D0
    python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_JpsiMuNu_${comp}_${year}.root -o FitCache/DATA_JpsiMuNu_${comp}_${year}_w${binning}bins.root -b config/binnings/${binning}_jpsi.json -m Jpsi
  done
done
```

#### Step 4. Perform the fits for the yields in each bin

Now we take the shapes we have already found and using the knowledge of which bin each event is in we create a dataset in D0 or Jpsi mass for each bin and extract the yield from a fit and save it in a histogram.

```bash
binning="nominal"
for year in 2011 2012 2015 2016 2017 2018
do
  for comp in sig misid comb
  do
    if [ ${comp} == "misid" ]
    then
      par="Data_MisID"
    else
      par="Data"
    fi
    python Executables/PerformBinYieldFits.py -i FitCache/DATA_D0MuNu_${comp}_${year}_w${binning}bins_CF.root -o Histograms/D0MuNu/${binning}/${year}/${comp}_cf.pkl -b config/binnings/${binning}_d0.json -s FitCache/d0_data_shapes_${year}.root -n pdf_${par}_CF -y sy_${par}_CF -m D0 -p plots/BinYields/D0MuNu/${binning}/${year}/${comp}_cf
    python Executables/PerformBinYieldFits.py -i FitCache/DATA_D0MuNu_${comp}_${year}_w${binning}bins_DCS.root -o Histograms/D0MuNu/${binning}/${year}/${comp}_dcs.pkl -b config/binnings/${binning}_d0.json -s FitCache/d0_data_shapes_${year}.root -n pdf_${par}_DCS -y sy_${par}_DCS -m D0 -p plots/BinYields/D0MuNu/${binning}/${year}/${comp}_dcs
    python Executables/PerformBinYieldFits.py -i FitCache/DATA_JpsiMuNu_${comp}_${year}_w${binning}bins.root -o Histograms/JpsiMuNu/${binning}/${year}/${comp}.pkl -b config/binnings/${binning}_jpsi.json -s FitCache/jpsi_data_shapes_${year}.root -n pdf_${par} -y sy_${par} -m Jpsi -p plots/BinYields/JpsiMuNu/${binning}/${year}/${comp}
  done
done
```

#### Step 5. Extract the MC yields in each bin

Here we just want to take our MC truthmatched trees and put them into the histogram template format.

```bash
binning="nominal"
for year in 2012 2016
do
  if [ ${year} == "2011" ] || [ ${year} == "2012" ]
  then
    run="run1"
  else
    run="run2"
  fi
  # D0 mode
  for mcdec in Bc2D0MuNu Bc2D0gMuNu Bc2D0pi0MuNu Bu2D0MuNu
  do
    python Executables/ExtractMCBinYields.py -i FitInput/MC_${mcdec}_${year}.root -o Histograms/D0MuNu/${binning}/${run}/mc_${mcdec}.pkl -b config/binnings/${nominal}_d0.json -m D0 -p plots/BinYields/D0MuNu/${binning}/${run}/mc_${mcdec}
  done
  # Jpsi mode
  for mcdec in Bc2JpsiMuNu Bu2JpsiX Bd2JpsiX Bs2JpsiX
  do
    python Executables/ExtractMCBinYields.py -i FitInput/MC_${mcdec}_${year}.root -o Histograms/JpsiMuNu/${binning}/${run}/mc_${mcdec}.pkl -b config/binnings/${nominal}_jpsi.json -m Jpsi -p plots/BinYields/JpsiMuNu/${binning}/${run}/mc_${mcdec}
  done
done
```

#### Step 6. Make the Dst template

Our samples are split into D0g and D0pi0 so we just want to run a simple little script to combine them

```bash
binning="nominal"
for run in run1 run2
do
  python Executables/PrepareDstHists.py -g Histograms/D0MuNu/${binning}/${run}/mc_bc2d0gmunu.pkl -p Histograms/D0MuNu/${binning}/${run}/mc_bc2d0pi0munu.pkl -o Histograms/D0MuNu/${binning}/${run}/mc_bc2dst0munu.pkl
done
```

### Run the actual template fit

Now we are in a position to actually run the template fit.

I run this locally so the first thing to do is copy over the `fit_hists` directory which is where the output of the `snakemake` stuff performed above goes.

Take a look at the options available for the template fit by running:

```bash
python Executables/PerformTemplateFit.py -h
```

This is just a wrapper which does some pretty basic stuff. The heavy lifting is done by various classes in the `python` directory. These are:

- `parameter.py` : defines a class called `parameter` which describes a fit parameter. It has variables like:
   - start value, error, limits etc.
- `mode.py` : defines a class called `mode` which describes a fit mode (in our case we have a `D0MuNu` mode and a `JpsiMuNu` mode - at some later stage we will want to add a `D0K3piMuNu` mode). It contains:
   - components (the number of different template components in mode)
   - yields (something to store the yields for each component)
   - data (the data we are fitting)
- `fitter.py` : defines a class called `fitter` which actually runs the fit. It has methods like:
   - Fit (runs the fit)
   - Plot (makes a plot)
   - Generate (throws a toy)
- `utils.py`: is a module which defines some useful helper functions
 
The fit itself is configured by a dictionary listing the modes, their binning scheme and what components and yields go into each. Several useful defaults are
collected in a file called `python/fitcfg.py`.

The strucutre of this config dictionary is like this (this just lists a single mode but you can add as many as you like):

```python
cfg = { 'ModeName' :
        {
              'title'      : f'$D^{0}$ Mode',
              'binning'    : f'config/binnings/D0MuNu/nominal.json',
              'data'       : f'fit_hists/D0MuNu/nominal/{year}/sig_dcs.pkl',
              'components' : { 'bu'    : f'fit_hists/D0MuNu/nominal/{year}/sig_cf.pkl'  ,
                               'misid' : f'fit_hists/D0MuNu/nominal/{year}/misid_dcs.pkl',
                               'comb'  : f'fit_hists/D0MuNu/nominal/{year}/comb_cf.pkl',
                               'sigd0' : f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2D0MuNu.pkl',
                               'sigdst': f'fit_hists/D0MuNu/nominal/{run}/mc_Bc2Dst0MuNu.pkl',
                             },
              'yields'     : { 'bu'    : 'func( ( eff_rat_bu_to_jpsi * br_rat_dcs_to_mm * ckm * JpsiMuNu:sig ) / f_c )',
                               'misid' : 'abs',
                               'comb'  : 'abs',
                               'sigd0' : 'func(eff_rat_d0_to_jpsi * br_rat_cf_to_mm * ckm * JpsiMuNu:sig)',
                               'sigdst': 'func(eff_rat_dst_to_jpsi * dst_sc * br_rat_cf_to_mm * ckm * JpsiMuNu:sig)',
                             },
              'plotcfg'    : { 'bu'    : { 'order' : 2, 'color': (1.0,0.4,0.4), 'title': 'B+ Background' },
                               'misid' : { 'order' : 1, 'color': (0.8,0.8,0.8), 'title': 'MisID Background'},
                               'comb'  : { 'order' : 0, 'color': (0.4,0.4,1.0), 'title': 'Comb Background' },
                               'sigd0' : { 'order' : 3, 'color': (0.0,0.8,0.0), 'title': 'D0 Signal' },
                               'sigdst': { 'order' : 4, 'color': (0.2,0.6,1.0), 'title': 'D* Signal' },
                             },
            },
}
```

You pass the following items:
 - title
 - binning : points to a file which defines the binning scheme
 - data : points to a `.pkl` file which stores the data to be fitted
 - components : a dictionary with each component name and then the `.pkl` file which contains that components template
 - yields : a dictionary with each component name and then a string which defines the yield. this can be:
    - 'abs' : just floats the absolute yield
    - 'frac' : just float the fractional yield
    - 'func()' : is a function which depends on some other parameters
- plotcfg : a dictionary with each component name and then a dictionary of options for the plotting

You can make such a dictionary in your own `.py` file and then pass it directly to the fitter using the `-c` option. See the examples in `python/fitcfgs/`.
When passing with an option file like this you need to define a dictionary called `cfg` like the example above. You can then also additional define a dictionary called `par` which sets the starting values and limits of parameters (and allows you to fix them). You can then define another one called `constr` which allows you to set Gaussian constraints on any of the parameters.

It's a decent idea to start by running in test mode so you can see what the shapes will look like 

```bash
python Executables/PerformTemplateFit.py -c python/fitcfgs/run2.py -t
```

Then you can run the fit itself by removing the `-t` option
