from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-s','--signal'  , default='FitInput/DATA_JpsiMuNu_sig_2016.root'  , help='Input signal data file')
parser.add_argument('-m','--misid'   , default='FitInput/DATA_JpsiMuNu_misid_2016.root', help='Input misid data file')
parser.add_argument('-M','--mcshapes', default='FitCache/jpsi_mc_shapes.root'          , help='MC shapes ws file')
parser.add_argument('-o','--output'  , default='FitCache/jpsi_data_shapes.root'        , help='Output file')
parser.add_argument('-p','--plots'   , default='plots'                                 , help='Plot folder')
parser.add_argument('-r','--noresid' , default=False, action="store_true"              , help='Don\'t plot residuals')
parser.add_argument('-v','--verbose' , default=False, action="store_true"              , help='Verbose output')
opts = parser.parse_args()
plot_resid = not opts.noresid

## SOME ADDITIONAL CUTS ##
jpsi_ran = [3020,3190]
bm_low = 0
jpsi_bins = 850
jpsi_simfit = True

print ('Commencing fits for Jpsi mass shapes')

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import utils as utils

assert( os.path.exists( opts.signal ) )
assert( os.path.exists( opts.misid ) )

os.system('mkdir -p %s'%opts.plots)

from ROOT import gROOT, gStyle, TFile, TCanvas, TLorentzVector, TLegend, TPad, TLine
from ROOT import RooFit as rf
from ROOT import RooRealVar, RooDataSet, RooArgSet, RooWorkspace, RooDataHist, RooArgList, RooSimultaneous
from ROOT import kBlue, kRed, kBlack, kGreen, kMagenta
gROOT.SetBatch()
gROOT.ProcessLine(".L common/lhcbStyle.C")
gStyle.SetLabelSize(0.05,"X");
gStyle.SetLabelSize(0.05,"Y");
gStyle.SetTitleXSize(0.06);
gStyle.SetTitleYSize(0.06);
gStyle.SetTitleXOffset(0.95);
gStyle.SetTitleYOffset(1.3);
gStyle.SetPadLeftMargin(0.15);

wsf = TFile(opts.mcshapes)
w = wsf.Get('w')

## First thing to do is use the MC shape for tail paramters
## then fit all of the data together to simultaneously get the
## other shape parameters
## Then we save these shapes so that later we can
## just fit for the yields in each bin with shapes fixed

### TOTAL FIT ###
print ('Filling datasets for total sample shape fit')

# set up mass variable
w.var('Jpsi_M').setRange(jpsi_ran[0],jpsi_ran[1])
if jpsi_bins>0: w.var('Jpsi_M').setBins(jpsi_bins)
w.var('Jpsi_M').SetTitle('m(#it{#mu}^{#plus}#it{#mu}^{#minus}) [MeV]')

# define the categories to fit in
cats = ['Data','Data_MisID' ]
w.factory( 'dtype[' + ','.join(cats) + ']')

# construct the dsets
all_Jpsi_mass_dsets = {}

for smpl in cats:
  if jpsi_bins>0: all_Jpsi_mass_dsets[smpl] = RooDataHist('tot_jpsim_'+smpl,'',RooArgSet(w.var('Jpsi_M')))
  else:           all_Jpsi_mass_dsets[smpl] = RooDataSet('tot_jpsim_'+smpl,'',RooArgSet(w.var('Jpsi_M')))

# fill the dsets
# loop files
for fname in [opts.signal, opts.misid]:
  print('Opening file', fname)
  f  = TFile(fname)
  ch = f.Get('DecayTree')

  # loop events
  nevs = ch.GetEntries()
  pbar = utils.progress_bar( "Jpsi Shape Fit", nevs)
  for ev in range(nevs):
    ch.GetEntry(ev)
    pbar.update()

    # apply cuts
    if ch.Jpsi_M < jpsi_ran[0]: continue
    if ch.Jpsi_M > jpsi_ran[1]: continue
    if ch.B_plus_M < bm_low: continue

    # work out appropriate category
    cat = 'Data'
    if fname==opts.misid: cat += '_MisID'

    # fill appropriate dataset
    w.var('Jpsi_M').setVal( ch.Jpsi_M )
    all_Jpsi_mass_dsets[cat].add( RooArgSet( w.var('Jpsi_M') ) )

  pbar.close()

# now make the pdfs
# contruct bkg pdf
w.factory( 'p0[ -0.02, 0.]' )
w.factory( 'Exponential::bkg_pdf(Jpsi_M,p0)' )
sim_pdf = RooSimultaneous( 'tot_jpsim_sim_pdf', '', w.cat('dtype') )
for smpl in cats:
  nentries = all_Jpsi_mass_dsets[smpl].sumEntries()
  w.factory( 'sy_'+smpl+'[%6.1f,0,%6.1f]'%(0.9*nentries,2*nentries) )
  w.factory( 'by_'+smpl+'[%6.1f,0,%6.1f]'%(0.1*nentries,2*nentries) )
  w.factory( 'SUM::pdf_'+smpl+'( sy_'+smpl+'*sig_pdf, by_'+smpl+'*bkg_pdf)' )
  sim_pdf.addPdf( w.pdf('pdf_'+smpl), smpl )

# can't seem to automate this in python so has to be hard-coded
if jpsi_bins>0:
  sim_data = RooDataHist( 'tot_jpsim_sim_data', '', RooArgList(w.var('Jpsi_M')), rf.Index(w.cat('dtype')),
                                                  rf.Import('Data',all_Jpsi_mass_dsets['Data']),
                                                  rf.Import('Data_MisID',all_Jpsi_mass_dsets['Data_MisID']) )
else:
  sim_data = RooDataSet( 'tot_jpsim_sim_data', '', RooArgSet(w.var('Jpsi_M')), rf.Index(w.cat('dtype')),
                                                 rf.Import('Data',all_Jpsi_mass_dsets['Data']),
                                                 rf.Import('Data_MisID',all_Jpsi_mass_dsets['Data_MisID']) )
getattr(w,'import')(sim_data)
getattr(w,'import')(sim_pdf)

# fix relevant params
w.loadSnapshot( 'fit_params_mc' )

# fix width scales
w.var('n1').setConstant()
w.var('n2').setConstant()

# perform fit (very slow if unbinned)
if jpsi_simfit:
  print('Performing fits to total sample')
  if jpsi_bins>0: w.pdf('tot_jpsim_sim_pdf').fitTo(w.data('tot_jpsim_sim_data'), rf.Extended() )
  else:         w.pdf('tot_jpsim_sim_pdf').fitTo(w.data('tot_jpsim_sim_data'), rf.Extended(), rf.NumCPU(8,3) )
  params = w.pdf('tot_jpsim_sim_pdf').getParameters( RooArgSet(w.var('Jpsi_M')) )
  w.saveSnapshot( 'tot_jpsim_sim_pdf_fitres', params )
  w.defineSet( 'tot_jpsim_sim_pdf_fitpars', params )
else:
  for smpl in cats:
    if jpsi_bins>0: w.pdf('pdf_'+smpl).fitTo( all_Jpsi_mass_dsets[smpl], rf.Extended() )
    else:         w.pdf('pdf_'+smpl).fitTo( all_Jpsi_mass_dsets[smpl], rf.Extended(), rf.NumCPU(8,3) )
    fpars = w.pdf('pdf_'+smpl).getParameters( RooArgSet(w.var('Jpsi_M')) )
    w.saveSnapshot( 'pdf_'+smpl+'_fitres', fpars )
    w.defineSet( 'pdf_'+smpl+'_fitpars', fpars )

# now plot these fits
c = TCanvas('jpsim_all','c',1200+plot_resid*200,600+plot_resid*400)
c.Divide(2,1)
for i, smpl in enumerate(cats):
  if not jpsi_simfit: w.loadSnapshot( 'pdf_'+smpl+'_fitres' )
  c.cd(i+1)
  ymin = 0.55 if 'DCS' in smpl else 0.68
  leg = TLegend(0.65,ymin,0.95,0.9)
  pl = w.var('Jpsi_M').frame()
  pl.SetTitle(smpl)
  all_Jpsi_mass_dsets[smpl].plotOn( pl, rf.Binning(85) )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), smpl, 'LEP' )
  w.pdf('pdf_'+smpl).plotOn( pl )
  resid = pl.pullHist()
  #w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.FillColor( kBlue-7 ),  rf.FillStyle(1001), rf.DrawOption("F") )
  w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.LineStyle(2), rf.Components('sig_pdf') )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Real Jpsi', 'L' )
  #w.pdf( 'pdf_'+smpl ).plotOn( pl, rf.LineColor(kRed-7), rf.FillColor(kRed-7 ),   rf.FillStyle(1001), rf.DrawOption("F"), rf.Components('bkg_pdf') )
  w.pdf( 'pdf_'+smpl ).plotOn( pl, rf.LineColor(kRed-7), rf.LineStyle(2), rf.Components('bkg_pdf') )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Fake Jpsi', 'L' )
  w.pdf('pdf_'+smpl).plotOn( pl )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Total PDF', 'L' )
  all_Jpsi_mass_dsets[smpl].plotOn( pl, rf.Binning(85) )
  pl.addObject(leg,'same')

  if plot_resid:
    padU = TPad( 'up%d'%i, '', 0., 0.33, 1., 1.)
    padD = TPad( 'dp%d'%i, '', 0., 0.,1.,0.33)
    padU.Draw()
    padD.Draw()
    resid.GetYaxis().SetTitle("Pull")
    resid.GetYaxis().SetLabelSize(0.1)
    resid.GetXaxis().SetLabelSize(0.1)
    resid.GetXaxis().SetTitleSize(0.14)
    resid.GetYaxis().SetTitleSize(0.14)
    resid.GetXaxis().SetTitleOffset(0.7)
    resid.GetYaxis().SetTitleOffset(0.5)
    resid.GetXaxis().SetTitle( pl.GetXaxis().GetTitle() )
    pl.GetXaxis().SetTitle('')
    padU.SetBottomMargin(0.1)
    padU.cd()
    pl.Draw()
    padD.SetTopMargin(0.05)
    padD.SetBottomMargin(0.35)
    padD.cd()
    resid.GetXaxis().SetRangeUser( pl.GetXaxis().GetXmin(), pl.GetXaxis().GetXmax() )
    resid.Draw("AP")
    l = TLine()
    l.SetLineWidth(3)
    l.SetLineColor(kBlue)
    l.DrawLine( pl.GetXaxis().GetXmin(),0.,pl.GetXaxis().GetXmax(),0. )
    resid.Draw("Psame")
  else:
    pl.Draw()
  c.Update()
  c.Modified()

c.Print(opts.plots+'/Jpsi_M_ShapeFits.pdf')
c.Print(opts.plots+'/Jpsi_M_ShapeFits.png')

print ('Jpsi Total Shape Fit Result:')
if jpsi_simfit:
  w.loadSnapshot( 'tot_jpsim_sim_pdf_fitres' )
  w.set( 'tot_jpsim_sim_pdf_fitpars' ).Print('v')
else:
  for smpl in cats:
    print( smpl, 'Sample' )
    w.loadSnapshot( 'pdf_'+smpl+'_fitres' )
    w.set( 'pdf_'+smpl+'_fitpars' ).Print('v')

w.writeToFile(opts.output)
wsf.Close()


