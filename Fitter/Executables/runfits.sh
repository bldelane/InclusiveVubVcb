#/bin/bash
for year in 2011 2012 2015 2016 2017 2018
  do python Executables/PerformTemplateFit.py -p plots/fit/${year} -s fitres/${year}/fit.pkl -y ${year} &
done
wait
for run in 1 2
  do python Executables/PerformTemplateFit.py -p plots/fit/run${run} -s fitres/run${run}/fit.pkl -r ${run} &
done
wait
