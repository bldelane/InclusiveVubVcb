python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_D0MuNu_sig_2016.root   -o FitCache/DATA_D0MuNu_sig_2016_wnominalbins.root   -b config/binnings/nominal_d0.json -m D0 &
python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_D0MuNu_misid_2016.root -o FitCache/DATA_D0MuNu_misid_2016_wnominalbins.root -b config/binnings/nominal_d0.json -m D0 &
python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_D0MuNu_comb_2016.root  -o FitCache/DATA_D0MuNu_comb_2016_wnominalbins.root  -b config/binnings/nominal_d0.json -m D0 &
python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_JpsiMuNu_sig_2016.root   -o FitCache/DATA_JpsiMuNu_sig_2016_wnominalbins.root   -b config/binnings/nominal_jpsi.json -m Jpsi &
python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_JpsiMuNu_misid_2016.root -o FitCache/DATA_JpsiMuNu_misid_2016_wnominalbins.root -b config/binnings/nominal_jpsi.json -m Jpsi &
python Executables/PrepareBinYieldTrees.py -i FitInput/DATA_JpsiMuNu_comb_2016.root  -o FitCache/DATA_JpsiMuNu_comb_2016_wnominalbins.root  -b config/binnings/nominal_jpsi.json -m Jpsi &
wait
