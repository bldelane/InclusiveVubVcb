from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-s','--signal'  , default='FitInput/DATA_D0MuNu_sig_2016.root'  , help='Input signal data file')
parser.add_argument('-m','--misid'   , default='FitInput/DATA_D0MuNu_misid_2016.root', help='Input misid data file')
parser.add_argument('-M','--mcshapes', default='FitCache/d0_mc_shapes.root'          , help='MC shapes ws file')
parser.add_argument('-o','--output'  , default='FitCache/d0_data_shapes.root'        , help='Output file')
parser.add_argument('-p','--plots'   , default='plots'                               , help='Plot folder')
parser.add_argument('-r','--noresid' , default=False, action="store_true"            , help='Don\'t plot residuals')
parser.add_argument('-e','--maxevs'  , default=-1, type=int                          , help='Max events')
parser.add_argument('-i','--interact', default=False, action="store_true"            , help='Run in interactive mode')
parser.add_argument('-v','--verbose' , default=False, action="store_true"            , help='Verbose output')
opts = parser.parse_args()
plot_resid = not opts.noresid

## SOME ADDITIONAL CUTS ##
d0_ran = [1790,1940]
bm_low = 0
d0_bins = 1200
d0_simfit = True

print ('Commencing fits for D0 mass shapes')

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import utils as utils

assert( os.path.exists( opts.signal ) )
assert( os.path.exists( opts.misid ) )

os.system('mkdir -p %s'%opts.plots)

from ROOT import gROOT, gStyle, TFile, TCanvas, TLorentzVector, TLegend, TPad, TLine
from ROOT import RooFit as rf
from ROOT import RooRealVar, RooDataSet, RooArgSet, RooWorkspace, RooDataHist, RooArgList, RooSimultaneous
from ROOT import kBlue, kRed, kBlack, kGreen, kMagenta
if not opts.interact: gROOT.SetBatch()
gROOT.ProcessLine(".L common/lhcbStyle.C")
gStyle.SetTitleXSize(0.07);
gStyle.SetTitleYSize(0.07);
gStyle.SetTitleXOffset(0.95);
gStyle.SetTitleYOffset(1.1);
gStyle.SetPadLeftMargin(0.15);

wsf = TFile(opts.mcshapes)
w = wsf.Get('w')

## First thing to do is use the MC shape for tail paramters
## then fit all of the data together to simultaneously get the
## other shape parameters
## For this part we'll use just the CF and DCS Sig and MisID data
## as if we also include the Combinatorial we could in prinicple be
## using an event twice
## Then we save these shapes so that later we can
## just fit for the yields in each bin with shapes fixed

### TOTAL FIT ###
print ('Filling datasets for total sample shape fit')

# set up mass variable
w.var('D0_M').setRange(d0_ran[0],d0_ran[1])
if d0_bins>0: w.var('D0_M').setBins(d0_bins)

# define the categories to fit in
cats = ['Data_CF','Data_DCS','Data_MisID_CF','Data_MisID_DCS']
w.factory( 'dtype[' + ','.join(cats) + ']')

# construct the dsets
all_D0_mass_dsets = {}

for smpl in cats:
  if d0_bins>0: all_D0_mass_dsets[smpl] = RooDataHist('tot_d0m_'+smpl,'',RooArgSet(w.var('D0_M')))
  else:         all_D0_mass_dsets[smpl] = RooDataSet('tot_d0m_'+smpl,'',RooArgSet(w.var('D0_M')))

# fill the dsets
# loop files
for fname in [opts.signal, opts.misid]:
  print('Opening file', fname)
  f  = TFile(fname)
  ch = f.Get('DecayTree')

  # loop events
  nevs = ch.GetEntries()
  pbar = utils.progress_bar( "D0 Shape Fit", nevs)
  for ev in range(nevs):
    if opts.maxevs>0 and ev>opts.maxevs: continue
    ch.GetEntry(ev)
    pbar.update()

    # apply cuts
    if ch.D0_M < d0_ran[0]: continue
    if ch.D0_M > d0_ran[1]: continue
    if ch.B_plus_M < bm_low: continue

    # work out appropriate category
    cf_str = 'CF' if ( ch.K_minus_ID * ch.Mu_plus_ID ) < 0 else 'DCS'
    cat = 'Data_'
    if fname==opts.misid: cat += 'MisID_'
    cat += cf_str

    # fill appropriate dataset
    w.var('D0_M').setVal( ch.D0_M )
    all_D0_mass_dsets[cat].add( RooArgSet( w.var('D0_M') ) )

  pbar.close()

# now make the pdfs
# contruct bkg pdf
w.factory( 'p0_cf[ -0.02, 0.]' )
w.factory( 'Exponential::bkg_cf_pdf(D0_M,p0_cf)' )
w.factory( 'p0_dcs[ -0.02, 0.]' )
w.factory( 'Exponential::bkg_dcs_pdf(D0_M,p0_dcs)' )
sim_pdf = RooSimultaneous( 'tot_d0m_sim_pdf', '', w.cat('dtype') )
for smpl in cats:
  nentries = all_D0_mass_dsets[smpl].sumEntries()
  if 'DCS' in smpl:
    w.factory( 'sy_'+smpl+'[%6.1f,0,%6.1f]'%(0.4*nentries,2*nentries) )
    w.factory( 'by_'+smpl+'[%6.1f,0,%6.1f]'%(0.4*nentries,2*nentries) )
    w.factory( 'ksy_'+smpl+'[%6.1f,0,%6.1f]'%(0.1*nentries,2*nentries) )
    w.factory( 'psy_'+smpl+'[%6.1f,0,%6.1f]'%(0.1*nentries,2*nentries) )
    w.factory( 'SUM::pdf_'+smpl+'( sy_'+smpl+'*sig_pdf, by_'+smpl+'*bkg_dcs_pdf, ksy_'+smpl+'*ks_pdf, psy_'+smpl+'*ps_pdf)' )
  else:
    w.factory( 'sy_'+smpl+'[%6.1f,0,%6.1f]'%(0.9*nentries,2*nentries) )
    w.factory( 'by_'+smpl+'[%6.1f,0,%6.1f]'%(0.1*nentries,2*nentries) )
    w.factory( 'SUM::pdf_'+smpl+'( sy_'+smpl+'*sig_pdf, by_'+smpl+'*bkg_cf_pdf)' )
  sim_pdf.addPdf( w.pdf('pdf_'+smpl), smpl )

# can't seem to automate this in python so has to be hard-coded
if d0_bins>0:
  sim_data = RooDataHist( 'tot_d0m_sim_data', '', RooArgList(w.var('D0_M')), rf.Index(w.cat('dtype')),
                                                  rf.Import('Data_CF',all_D0_mass_dsets['Data_CF']),
                                                  rf.Import('Data_DCS',all_D0_mass_dsets['Data_DCS']),
                                                  rf.Import('Data_MisID_CF',all_D0_mass_dsets['Data_MisID_CF']),
                                                  rf.Import('Data_MisID_DCS',all_D0_mass_dsets['Data_MisID_DCS']) )
else:
  sim_data = RooDataSet( 'tot_d0m_sim_data', '', RooArgSet(w.var('D0_M')), rf.Index(w.cat('dtype')),
                                                 rf.Import('Data_CF',all_D0_mass_dsets['Data_CF']),
                                                 rf.Import('Data_DCS',all_D0_mass_dsets['Data_DCS']),
                                                 rf.Import('Data_MisID_CF',all_D0_mass_dsets['Data_MisID_CF']),
                                                 rf.Import('Data_MisID_DCS',all_D0_mass_dsets['Data_MisID_DCS']) )
getattr(w,'import')(sim_data)
getattr(w,'import')(sim_pdf)

# fix relevant params
w.loadSnapshot( 'fit_params_mc' )

# fix width scales
w.var('s1sc').setConstant()
w.var('s2sc').setConstant()
# fix fractions
w.var('f1').setConstant()
w.var('f2').setConstant()
# fix tail parameters
w.var('alpha1').setConstant()
w.var('alpha2').setConstant()
w.var('n1').setConstant()
w.var('n2').setConstant()
w.var('ks_alpha').setConstant()
w.var('ks_n').setConstant()
w.var('ps_alpha').setConstant()
w.var('ps_n').setConstant()
# set relative parameters constant
w.var('ks_ss').setConstant()
w.var('ps_ss').setConstant()
w.var('ks_dm').setConstant()
w.var('ps_dm').setConstant()
# help out some of the data with better ranges
w.var('mu').setRange(1860,1870)
w.var('sigma').setRange(6,8)
w.var('p0_cf').setRange(-0.02,-1e-4)
w.var('p0_dcs').setRange(-0.02,-1e-4)


# perform fit (very slow if unbinned)
if d0_simfit:
  print('Performing fits to total sample')
  if d0_bins>0: w.pdf('tot_d0m_sim_pdf').fitTo(w.data('tot_d0m_sim_data'), rf.Extended() )
  else:         w.pdf('tot_d0m_sim_pdf').fitTo(w.data('tot_d0m_sim_data'), rf.Extended(), rf.NumCPU(8,3) )
  params = w.pdf('tot_d0m_sim_pdf').getParameters( RooArgSet(w.var('D0_M')) )
  w.saveSnapshot( 'tot_d0m_sim_pdf_fitres', params )
  w.defineSet( 'tot_d0m_sim_pdf_fitpars', params )
else:
  for smpl in cats:
    if d0_bins>0: w.pdf('pdf_'+smpl).fitTo( all_D0_mass_dsets[smpl], rf.Extended() )
    else:         w.pdf('pdf_'+smpl).fitTo( all_D0_mass_dsets[smpl], rf.Extended(), rf.NumCPU(8,3) )
    fpars = w.pdf('pdf_'+smpl).getParameters( RooArgSet(w.var('D0_M')) )
    w.saveSnapshot( 'pdf_'+smpl+'_fitres', fpars )
    w.defineSet( 'pdf_'+smpl+'_fitpars', fpars )

# now plot these fits
c = TCanvas('d0m_all','c',1200+plot_resid*200,1200+plot_resid*400)
c.Divide(2,2)
for i, smpl in enumerate(cats):
  if not d0_simfit: w.loadSnapshot( 'pdf_'+smpl+'_fitres' )
  c.cd(i+1)
  ymin = 0.55 if 'DCS' in smpl else 0.68
  leg = TLegend(0.65,ymin,0.95,0.9)
  pl = w.var('D0_M').frame()
  pl.SetTitle(smpl)
  all_D0_mass_dsets[smpl].plotOn( pl, rf.Binning(75) )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), smpl, 'LEP' )
  w.pdf('pdf_'+smpl).plotOn( pl )
  resid = pl.pullHist()
  if 'DCS' in smpl:
    #w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.FillColor( kBlue-7 ), rf.DrawOption("F") , rf.VLines() )
    w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.LineStyle(2), rf.Components('sig_dcs_pdf') )
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Real D0', 'L' )
    #w.pdf('pdf_'+smpl).plotOn(pl,rf.FillColor(kGreen-3),rf.LineColor(kGreen-3),rf.DrawOption("F")    , rf.VLines(),rf.Components('bkg_dcs_pdf,ks_pdf'))
    w.pdf('pdf_'+smpl).plotOn(pl,rf.LineColor(kGreen-3), rf.LineStyle(2), rf.Components('ks_pdf'))
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'D->KK MisID', 'L' )
    #w.pdf('pdf_'+smpl).plotOn(pl,rf.FillColor(kMagenta-3),rf.LineColor(kMagenta-3),rf.DrawOption("F"), rf.VLines(),rf.Components('bkg_dcs_pdf,ps_pdf'))
    w.pdf('pdf_'+smpl).plotOn(pl,rf.LineColor(kMagenta-3), rf.LineStyle(2), rf.Components('ps_pdf'))
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'D->#pi#pi MisID', 'L' )
    #w.pdf( 'pdf_'+smpl ).plotOn( pl, rf.LineColor(kRed-7), rf.FillColor(kRed-7 ), rf.DrawOption("F") , rf.VLines(), rf.Components('bkg_dcs_pdf')      )
    w.pdf( 'pdf_'+smpl ).plotOn( pl, rf.LineColor(kRed-7), rf.LineStyle(2), rf.Components('bkg_dcs_pdf')      )
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Fake D0', 'L' )
  else:
    #w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.FillColor( kBlue-7 ), rf.DrawOption("F") , rf.VLines() )
    w.pdf('pdf_'+smpl).plotOn( pl, rf.LineColor(kBlue-7), rf.LineStyle(2), rf.Components('sig_cf_pdf') )
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Real D0', 'L' )
    w.pdf( 'pdf_'+smpl ).plotOn( pl, rf.LineColor(kRed-7), rf.FillColor(kRed-7 ), rf.DrawOption("F") , rf.VLines(), rf.Components('bkg_cf_pdf')       )
    leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Fake D0', 'L' )
  w.pdf('pdf_'+smpl).plotOn( pl )
  leg.AddEntry( pl.getObject( int(pl.numItems())-1 ), 'Total PDF', 'L' )
  all_D0_mass_dsets[smpl].plotOn( pl, rf.Binning(75) )
  pl.addObject(leg,'same')

  if plot_resid:
    padU = TPad( 'up%d'%i, '', 0., 0.33, 1., 1.)
    padD = TPad( 'dp%d'%i, '', 0., 0.,1.,0.33)
    padU.Draw()
    padD.Draw()
    resid.GetYaxis().SetTitle("Pull")
    resid.GetYaxis().SetLabelSize(0.12)
    resid.GetXaxis().SetLabelSize(0.12)
    resid.GetXaxis().SetTitleSize(0.2)
    resid.GetYaxis().SetTitleSize(0.14)
    resid.GetXaxis().SetTitleOffset(0.7)
    resid.GetYaxis().SetTitleOffset(0.38)
    resid.GetXaxis().SetTitle( pl.GetXaxis().GetTitle() )
    pl.GetXaxis().SetTitle('')
    padU.SetBottomMargin(0.1)
    padU.cd()
    pl.Draw()
    padD.SetTopMargin(0.05)
    padD.SetBottomMargin(0.35)
    padD.cd()
    resid.GetXaxis().SetRangeUser( pl.GetXaxis().GetXmin(), pl.GetXaxis().GetXmax() )
    resid.Draw("AP")
    l = TLine()
    l.SetLineWidth(3)
    l.SetLineColor(kBlue)
    l.DrawLine( pl.GetXaxis().GetXmin(),0.,pl.GetXaxis().GetXmax(),0. )
    resid.Draw("Psame")
  else:
    pl.Draw()
  c.Update()
  c.Modified()

c.Print(opts.plots+'/D0_M_ShapeFits.pdf')
c.Print(opts.plots+'/D0_M_ShapeFits.png')

print ('D0 Total Shape Fit Result:')
if d0_simfit:
  w.loadSnapshot( 'tot_d0m_sim_pdf_fitres' )
  w.set( 'tot_d0m_sim_pdf_fitpars' ).Print('v')
else:
  for smpl in cats:
    print( smpl, 'Sample' )
    w.loadSnapshot( 'pdf_'+smpl+'_fitres' )
    w.set( 'pdf_'+smpl+'_fitpars' ).Print('v')

w.writeToFile(opts.output)
wsf.Close()

if opts.interact: input('Ctrl-C to exit')
