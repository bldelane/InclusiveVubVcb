#!/bin/bash

python Executables/ExtractMCBinYields.py -i FitInput/MC_Bc2D0MuNu_2016.root    -o Histograms/D0MuNu/nominal/mc_bc2d0munu_2016.pkl    -b config/binnings/nominal_d0.json   -m D0   -p plots/D0MuNu/nominal/binyields/mc_bc2d0munu_2016     &
python Executables/ExtractMCBinYields.py -i FitInput/MC_Bc2D0gMuNu_2016.root   -o Histograms/D0MuNu/nominal/mc_bc2d0gmunu_2016.pkl   -b config/binnings/nominal_d0.json   -m D0   -p plots/D0MuNu/nominal/binyields/mc_bc2d0gmunu_2016    &
python Executables/ExtractMCBinYields.py -i FitInput/MC_Bc2D0pi0MuNu_2016.root -o Histograms/D0MuNu/nominal/mc_bc2d0pi0munu_2016.pkl -b config/binnings/nominal_d0.json   -m D0   -p plots/D0MuNu/nominal/binyields/mc_bc2d0pi0munu_2016  &
#python Executables/ExtractMCBinYields.py -i FitInput/MC_Bu2D0MuNu_2016.root    -o Histograms/D0MuNu/nominal/mc_bu2d0munu_2016.pkl    -b config/binnings/nominal_d0.json   -m D0   -p plots/D0MuNu/nominal/binyields/mc_bu2d0munu_2016     &
python Executables/ExtractMCBinYields.py -i FitInput/MC_Bc2JpsiMuNu_2016.root  -o Histograms/JpsiMuNu/nominal/mc_bc2jpsimunu_2016.pkl  -b config/binnings/nominal_jpsi.json -m Jpsi -p plots/JpsiMuNu/nominal/binyields/mc_bc2jpsimunu_2016 &
wait
