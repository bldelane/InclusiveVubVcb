from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'   , default='FitCache/DATA_D0MuNu_sig_2016_wnominalbins_DCS.root', help='Input file')
parser.add_argument('-o','--output'  , default='Histograms/DATA_D0MuNu_sig_2016_wnominalbins_DCS.pkl', help='Output file')
parser.add_argument('-b','--binning' , default='config/binnings/nominal_d0.json'                , help='Binning file')
parser.add_argument('-s','--shapes'  , default='FitCache/d0_data_shapes.root'                   , help='Shapes file')
parser.add_argument('-n','--shapename', default='pdf_Data_DCS'                                  , help='Shape name in file')
parser.add_argument('-y','--yieldname', default='sy_Data_DCS'                                   , help='Signal yield name in file')
parser.add_argument('-m','--hadron'  , default='D0', choices=['D0','Jpsi']                      , help='Which hadron species')
parser.add_argument('-w','--weight'  , default='GBR_w'                                          , help='Weight variable')
parser.add_argument('-p','--plots'   , default='plots'                                          , help='Plot folder')
parser.add_argument('-c','--config'  , default=None                                             , help='Provide options via config file')
parser.add_argument('-v','--verbose' , default=False, action="store_true"                       , help='Verbose output')
opts = parser.parse_args()

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))

import pandas as pd
import numpy as np
import ROOT as r
from ROOT import RooFit as rf

from root_numpy import array2tree
from root_pandas import read_root

import utils as utils
import boost_histogram as bh

# cuts
had_m_range = [1790,1940] if opts.hadron=='D0' else [3020,3190]
bm_low = 0 if opts.hadron=='D0' else 0

# clear the .gif
os.system('rm -f %s/bin_yields.gif'%opts.plots)

# get the binning and initialise the output hist
binning = utils.load_binning( opts.binning )
hist = bh.Histogram( *binning, storage=bh.storage.Weight() )

# get the shapes and fix them
wsf = r.TFile(opts.shapes)
w = wsf.Get("w")
if not w.pdf(opts.shapename):
  sys.exit('Shape',opts.shapename,'not found in shapes file', opts.shape )
if not w.var(opts.yieldname):
  sys.exit('Yield',opts.yieldname,'not found in shapes file', opts.shape )

# fix the parameters
snapshot_prefix = 'tot_d0m_sim_pdf' if opts.hadron=='D0' else 'tot_jpsim_sim_pdf'
w.loadSnapshot( snapshot_prefix+'_fitres' )
w.set( snapshot_prefix+'_fitpars').setAttribAll('Constant')
# get the yield names and release them
yields = []
it = w.pdf(opts.shapename).coefList().createIterator()
var = it.Next()
while var:
  var.setConstant(False)
  yields.append( var.GetName() )
  var = it.Next()
if opts.yieldname not in yields:
  sys.exit('Did not find yield',opts.yieldname,'in list of pdf',opts.shapename,'coefficients')

## now we have a choice to make ##
## - either we loop the input tree once and fill the relevant dataset
##   however this would require us to keep every dataset (one for each bin)
##   in a container in memory (so actually for >=2D fits this becomes really
##   expensive
## - alternative is to loop over each bin and each time get an appropriate
##   slice of the tree, make the dataset, fit it and then fill the histogram
##   with the appropriate number (with quick routines from root_pandas this
##   is actually reasonably fast)

# load the input tree
df = read_root(opts.input)

# set observables to read i.e. D0_M / Jpsi_M and weight
obs = [ opts.hadron+'_M' ]
use_weight = opts.weight in df.columns
if use_weight:
  w.factory( opts.weight+"[-100,100]" )
  obs.append( opts.weight )

# turn off roofit output
if not opts.verbose:
  r.gErrorIgnoreLevel = r.kWarning
  r.RooMsgService.instance().setGlobalKillBelow(rf.ERROR)

# loop over each histogram bin combination
pbar = utils.progress_bar( "Filling hist", np.product(hist.shape) )

for bin_coords in utils.loop_bin_coords(hist):

  pbar.update()

  # get the sub dset
  query_str = " and ".join( [ hist.axes[i].metadata+'_BIN=='+str(bin_coords[i]) for i in range(hist.rank) ] )
  df_subset = df.query( query_str )[ obs ]

  syval = 0
  syerr = 0

  # fit the D0 / Jpsi mass in that bin (if there are any events there)
  if len(df_subset.index)>0:

    # make the dataset
    df_subset_tr = array2tree( df_subset.to_records() )
    bin_name = "_".join( [str(b) for b in bin_coords] )
    if use_weight:
      dset = r.RooDataSet( bin_name, '', r.RooArgSet( w.var(obs[0]), w.var(opts.weight) ), rf.Import(df_subset_tr), rf.WeightVar( opts.weight) )
    else:
      dset = r.RooDataSet( bin_name, '', r.RooArgSet( w.var(obs[0]) ), rf.Import(df_subset_tr) )

    # set some sensible starting values and fit the dataset
    for yld in yields:
      w.var(yld).setConstant(False)
      w.var(yld).setRange(0, 2.*dset.sumEntries())
      if yld.startswith('ksy_') or yld.startswith('psy_'):
        w.var(yld).setVal(0.1*dset.sumEntries())
      if yld.startswith('sy_'):
        if 'DCS' in yld: w.var(yld).setVal(0.4*dset.sumEntries())
        if 'CF'  in yld: w.var(yld).setVal(0.9*dset.sumEntries())
      if yld.startswith('by_'):
        if 'DCS' in yld: w.var(yld).setVal(0.4*dset.sumEntries())
        if 'CF'  in yld: w.var(yld).setVal(0.1*dset.sumEntries())

    w.pdf(opts.shapename).fitTo( dset, rf.Extended(True), rf.PrintLevel(-1) )

    utils.plotHadronMassFit( os.path.join(opts.plots, bin_name), dset, w.pdf(opts.shapename) )

    # save the vals
    syval = w.var( opts.yieldname ).getVal()
    syerr = w.var( opts.yieldname ).getError()

  hist[bin_coords] = bh.accumulators.WeightedSum( value = syval, variance = syerr**2 )

pbar.close()

# plot the histogram
utils.plotHistogram( hist, opts.plots )

# write the histogram
utils.writeHistogram( hist, opts.output )
