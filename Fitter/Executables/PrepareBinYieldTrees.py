from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'   , default='FitInput/DATA_D0MuNu_sig_2016.root'             , help='Input file')
parser.add_argument('-o','--output'  , default='FitCache/DATA_D0MuNu_sig_2016_wnominalbins.root', help='Output file (note for D0 mode will make a CF and DCS version)')
parser.add_argument('-b','--binning' , default='config/binnings/nominal_d0.json'                , help='Binning file')
parser.add_argument('-m','--hadron'  , default='D0', choices=['D0','Jpsi']                      , help='Which hadron species')
parser.add_argument('-p','--plots'   , default='plots'                                          , help='Plot folder')
parser.add_argument('-B','--batch'   , default=False, action="store_true"                       , help='Run in batch mode')
parser.add_argument('-v','--verbose' , default=False, action="store_true"                       , help='Verbose output')
opts = parser.parse_args()

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import utils as utils

from ROOT import TFile
from array import array

import boost_histogram as bh

had_m_range = [1790,1940] if opts.hadron=='D0' else [3020,3190]
bm_low = 0 if opts.hadron=='D0' else 0

binning = utils.load_binning( opts.binning )

binning_hist = bh.Histogram( *binning )

itf = TFile(opts.input)
itr = itf.Get('DecayTree')

otf = []
otr = []
if opts.hadron == 'D0':
  otf.append( TFile( opts.output.replace('.root','_CF.root'),'recreate') )
  otr.append( itr.CloneTree(0) )
  otf.append( TFile( opts.output.replace('.root','_DCS.root'),'recreate') )
  otr.append( itr.CloneTree(0) )
else:
  otf.append( TFile( opts.output, 'recreate') )
  otr.append( itr.CloneTree(0) )

vals = {}
for axis in binning_hist.axes:
  if axis.metadata not in itr.GetListOfBranches():
    sys.exit('No matching branch name for axis',axis.metadata)
  vals[axis.metadata] = array('i',[-99])
  for t in otr: t.Branch(axis.metadata+'_BIN', vals[axis.metadata], axis.metadata+'_BIN/I')

nevents = itr.GetEntries()
if not opts.batch: pbar    = utils.progress_bar( "Reading tree", nevents )

for ev in range(nevents):
  itr.GetEntry(ev)
  if (ev%100==0) and not opts.batch: pbar.update(100)
  if opts.batch and (ev%5000==0): print('Event',ev,'/',nevents)

  # apply cuts
  had_m = getattr(itr,opts.hadron+"_M")
  if had_m < had_m_range[0]: continue
  if had_m > had_m_range[1]: continue
  if itr.B_plus_M < bm_low: continue

  for axis in binning_hist.axes:
    ax_val = getattr(itr,axis.metadata)
    if ax_val < axis.edges[0]: continue
    if ax_val > axis.edges[-1]: continue

    ind = axis.index(ax_val)
    vals[axis.metadata][0] = ind

  # Fill
  if opts.hadron == 'D0':
    if ( itr.K_minus_ID * itr.Mu_plus_ID ) < 0:
      otf[0].cd()
      otr[0].Fill()
    else:
      otf[1].cd()
      otr[1].Fill()
  else:
    otf[0].cd()
    otr[0].Fill()

if not opts.batch: pbar.close()

for i, f in enumerate(otf):
  otf[i].cd()
  otr[i].Write()
  otf[i].Close()
itf.Close()
