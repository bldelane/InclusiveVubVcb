from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input' , default='FitInput/MC_Bc2JpsiMuNu_2016.root', help='Input MC file')
parser.add_argument('-o','--output', default='FitCache/jpsi_mc_shapes.root'     , help='Output workspace')
parser.add_argument('-p','--plots' , default='plots'                          , help='Plot folder')
parser.add_argument('-v','--verbose', default=False, action="store_true"      , help='Verbose output')
opts = parser.parse_args()

print ('Commencing fits to MC for Jpsi mass shapes using {input}'.format(input=opts.input))

import os
assert( os.path.exists( opts.input ) )

os.system('mkdir -p %s'%opts.plots)

from ROOT import TFile, TCanvas, gROOT, gStyle
from ROOT import RooFit as rf
from ROOT import RooRealVar, RooDataSet, RooArgSet, RooWorkspace
from ROOT import kBlue, kRed, kBlack
gROOT.SetBatch()
gROOT.ProcessLine(".L common/lhcbStyle.C")
gStyle.SetTitleXSize(0.07);
gStyle.SetTitleYSize(0.07);
gStyle.SetTitleXOffset(0.95);
gStyle.SetTitleYOffset(1.1);
gStyle.SetPadLeftMargin(0.15);

tf = TFile(opts.input)
ch = tf.Get('DecayTree')

nevents = ch.GetEntries()

# make dataset
Jpsi_M = RooRealVar("Jpsi_M","m(#mu#mu)",3020,3190)
rd_jpsi_m = RooDataSet("rd_jpsi_m","",RooArgSet(Jpsi_M))

# fill them from the tree
for ev in range(nevents):
  ch.GetEntry(ev)

  if ( ch.Jpsi_M >= Jpsi_M.getMin() and ch.Jpsi_M <= Jpsi_M.getMax() ):
    Jpsi_M.setVal( ch.Jpsi_M )
    rd_jpsi_m.add( RooArgSet(Jpsi_M) )

# now make the fit shape
w = RooWorkspace('w','w')
getattr(w,'import')(Jpsi_M)
getattr(w,'import')(rd_jpsi_m)

# construct PDF
w.factory( "CBShape::cb1( Jpsi_M, mu[3000,3200], sigma[0.5,20], alpha1[0,10], n1[3.] )" )
w.factory( "CBShape::cb2( Jpsi_M, mu, sigma, alpha2[-10,0], n2[3.] )" )
w.factory( "SUM::sig_pdf( f1[0.1,0.9]*cb1, cb2 )" )

# fit it
c = TCanvas()
w.pdf("sig_pdf").fitTo( rd_jpsi_m )
pl = Jpsi_M.frame()
rd_jpsi_m.plotOn(pl)
w.pdf("sig_pdf").plotOn(pl)
c.cd()
pl.Draw()
c.Update()
c.Modified()
c.Print(opts.plots+"/Jpsi_M_MCShapesFree.pdf")
c.Print(opts.plots+"/Jpsi_M_MCShapesFree.png")

params = w.pdf("sig_pdf").getParameters( RooArgSet(Jpsi_M) )
w.saveSnapshot( 'fit_params_mc', params )
w.defineSet( 'fit_pars', params )

w.writeToFile(opts.output)

print('Fitted:')
params.Print('v')
