from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter

parser = ArgumentParser(description='Vub/Vcb Fitter', formatter_class=ArgumentDefaultsHelpFormatter)

parser.add_argument('-c','--config'      , default=None,                                                       help="Read the fit configuration from a specific file")
parser.add_argument('-p','--plots'       , default='plots/fit',                                                help="Output folder for plots")
parser.add_argument('-s','--save'        , default='fitres/fit.pkl',                                           help="Output file for fit result")
parser.add_argument('-l','--load'        , default=None,                                                       help="Load the fit result from a file (this will not run the fit)")
parser.add_argument('-i','--interactive' , default=False, action="store_true",                                 help="Run in interactive mode (i.e. show plots as created)")
parser.add_argument('-x','--strict'      , default=False, action="store_true",                                 help="Run the fitter in strict mode. Fit result has to pass a series of checks")
parser.add_argument('-e','--extended'    , default=False, action="store_true",                                 help="Add extended term to fit likelihood")
parser.add_argument('-R','--resid'       , default=True,  action="store_false",                                help="Turn off residual plots")
parser.add_argument('-S','--scale'       , default=True,  action="store_false",                                help="Turn off application of beta scales in plots")
parser.add_argument('-B','--betahist'    , default=False, action="store_true",                                 help="Also make plots of the beta values")
parser.add_argument('-T','--toys'        , default=0, type=int,                                                help="Throw this number of toys (note -1 will give you an Asimov toy)")
parser.add_argument('-d','--toydir'      , default='toys',                                                     help="Directory to throw toys in")
parser.add_argument(     '--seed'        , default=0, type=int,                                                help="Random seed for toys")
parser.add_argument('-f','--fittoys'     , default=False, action="store_true",                                 help="Fit the toys back")
parser.add_argument('-t','--test'        , default=False, action="store_true",                                 help="Run in a test mode in which no minimisation is run")
parser.add_argument('-v','--verbose'     , default=False, action="store_true",                                 help='Verbose output')
opts = parser.parse_args()

import os
import sys
sys.path.append( os.getcwd() )
import python.utils as utils
from python.fitter import fitter
import importlib

def load_config(fname):
  if not os.path.exists(fname):
    raise RuntimeError('No such file', fname)
  print('Loading configuration from file', fname)
  spec = importlib.util.spec_from_file_location('module',fname)
  module = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(module)
  cfg = None
  par = None
  constr = None
  if hasattr(module,'cfg'):
    cfg = module.cfg
  if hasattr(module,'par'):
    par = module.par
  if hasattr(module,'constr'):
    constr = module.constr
  if constr is None:
    raise ValueError('Cannot load configuration')

  return cfg, par, constr

# config loaded here -> go and look at this file
cfg, par, constr = load_config( opts.config )

# make our fitter
f = fitter( 'Fit', hesse=True, test=opts.test, verbose=opts.verbose, extended=opts.extended, strict=opts.strict )

# add the modes
for mode, config in cfg.items():
  f.AddMode( mode, **config )

# set starting parameters
if par is not None:
  f.set_parameters(par)

# add the constraints
if constr is not None:
  f.set_constraints(constr)

# load parameters if asked
if opts.load:
  f.load_parameters(opts.load)
# otherwise run the fit
else:
  f.Fit(opts.save)

# throw toys if asked
if opts.toys:
  print('Have not yet implemented the toy throwing')
  assert(0)

# configure plot options
pltcfg  = [ cfg[mode]['plotcfg'] for mode in cfg.keys() ]
xlabels = [ utils.get_binning_titles( cfg[mode]['binning'] ) for mode in cfg.keys() ]
ylabels = [ [ 'Events' for x in xlabels[-1] ] for mode in cfg.keys() ]
titles  = [ cfg[mode]['title'] for mode in cfg.keys() ]
if 'subtitles' in cfg[mode].keys():
  titles = [ [ cfg[mode]['title'] + ' ('+ subtitle +')' for subtitle in cfg[mode]['subtitles'] ] for mode in cfg.keys() ]

# make the plots
f.Plot( pltcfg, titles, xlabels, ylabels, outdir=opts.plots, resid=opts.resid, beta_scaling=opts.scale, beta_hist=opts.betahist, wide=True , interactive=opts.interactive)
f.Plot( pltcfg, titles, xlabels, ylabels, outdir=opts.plots, resid=opts.resid, beta_scaling=opts.scale, beta_hist=opts.betahist, wide=False, interactive=opts.interactive)

# show the plots if asked
if opts.interactive:
  f.ShowPlots()
