from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'   , default='FitInput/MC_Bc2D0MuNu_2016.root'                , help='Input file')
parser.add_argument('-o','--output'  , default='Histograms/D0MuNu/nominal/mc_bc2d0munu_2016.pkl', help='Output file')
parser.add_argument('-b','--binning' , default='config/binnings/nominal_d0.json'                , help='Binning file')
parser.add_argument('-m','--hadron'  , default='D0', choices=['D0','Jpsi']                      , help='Which hadron species')
parser.add_argument('-w','--weight'  , default=None                                             , help='Weight variable')
parser.add_argument('-p','--plots'   , default='plots'                                          , help='Plot folder')
parser.add_argument('-v','--verbose' , default=False, action="store_true"                       , help='Verbose output')
opts = parser.parse_args()

had_m_range = [1790,1940] if opts.hadron=='D0' else [3020,3190]
bm_low = 0 if opts.hadron=='D0' else 0

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import ROOT as r
import boost_histogram as bh
import utils as utils

binning = utils.load_binning( opts.binning )
hist = bh.Histogram( *binning, storage=bh.storage.Weight() )

inf = r.TFile(opts.input)
tree = inf.Get('DecayTree')

# check vars available
for ax in hist.axes:
  if ax.metadata not in tree.GetListOfBranches():
    sys.exit('No such branch named '+ax.metadata+' in tree')

# check weight available
if opts.weight and opts.weight not in tree.GetListOfBranches():
  sys.exit('No such branch named '+opts.weight+' in tree')

nevents = tree.GetEntries()
pbar = utils.progress_bar( "Reading tree", nevents )

for ev in range(nevents):
  tree.GetEntry(ev)
  pbar.update()

  # apply cuts
  had_m = getattr(tree,opts.hadron+"_M")
  if had_m < had_m_range[0]: continue
  if had_m > had_m_range[1]: continue
  if tree.B_plus_M < bm_low: continue

  vals = []
  in_range = True
  for axis in hist.axes:
    ax_val = getattr(tree,axis.metadata)
    if ax_val < axis.edges[0]: in_range = False
    if ax_val > axis.edges[-1]: in_range = False
    if in_range: vals.append( ax_val )

  if not in_range: continue

  wt = 1.
  if opts.weight:
    wt = getattr(tree,opts.weight)

  # Fill
  hist.fill( *vals, weight=wt )

pbar.close()
inf.Close()

# plot the histogram
utils.plotHistogram( hist, opts.plots )

# write histogram
utils.writeHistogram( hist, opts.output )
