from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input'     , default=[], action="append"       , help='Input files')
parser.add_argument('-o','--output'    , required=True                     , help='Output file')
parser.add_argument('-y','--yieldmatch', default=[], action="append"       , help='Match yields to these files')
parser.add_argument('-p','--plots'     , default='plots'                   , help='Plot folder')
parser.add_argument('-v','--verbose'   , default=False, action="store_true", help='Verbose output')
opts = parser.parse_args()

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import utils as utils
import numpy as np

def makesum( hists ):
  tot = hists[0].copy()
  for i, h in enumerate(hists):
    if i==0: continue
    tot += h
  return tot

inhist = [ utils.readHistogram(infile) for infile in opts.input ]

if len(opts.yieldmatch)>0:
  yldhist = [ utils.readHistogram(yldfile) for yldfile in opts.yieldmatch ]
  assert( len(inhist) == len(yldhist) )
  yldmatch = [ ( inhist[i].sum().value / yldhist[i].sum().value ) * inhist[i] for i in range(len(yldhist)) ]
  outhist = makesum( yldmatch )
else:
  outhist = makesum( inhist )

# check
for h in inhist:
  assert( outhist.shape == h.shape )

# plot the histogram
utils.plotHistogram( outhist, opts.plots )

# write histogram
utils.writeHistogram( outhist, opts.output )
