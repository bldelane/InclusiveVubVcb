from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i','--input' , default='FitInput/MC_Bc2D0MuNu_2016.root', help='Input MC file')
parser.add_argument('-o','--output', default='FitCache/d0_mc_shapes.root'     , help='Output workspace')
parser.add_argument('-p','--plots' , default='plots'                          , help='Plot folder')
parser.add_argument('-v','--verbose', default=False, action="store_true"      , help='Verbose output')
opts = parser.parse_args()

print ('Commencing fits to MC for D0 mass shapes using {input}'.format(input=opts.input))

import os
assert( os.path.exists( opts.input ) )

os.system('mkdir -p %s'%opts.plots)

from ROOT import gROOT, gStyle, TFile, TCanvas, TLorentzVector, TLegend
from ROOT import RooFit as rf
from ROOT import RooRealVar, RooDataSet, RooArgSet, RooWorkspace
from ROOT import kBlue, kRed, kBlack
gROOT.SetBatch()
gROOT.ProcessLine(".L common/lhcbStyle.C")
gStyle.SetTitleXSize(0.07);
gStyle.SetTitleYSize(0.07);
gStyle.SetTitleXOffset(0.95);
gStyle.SetTitleYOffset(1.1);
gStyle.SetPadLeftMargin(0.15);

tf = TFile(opts.input)
ch = tf.Get('DecayTree')

nevents = ch.GetEntries()
m_k  = 493.677
m_pi = 139.57018

# make datasets for misID shapes
D0_M = RooRealVar("D0_M","m(K#pi)",1600,2100)
rd_d0_m = RooDataSet("rd_d0_m","",RooArgSet(D0_M))
rd_d0_kswap_m = RooDataSet("rd_d0_kswap_m","",RooArgSet(D0_M))
rd_d0_piswap_m = RooDataSet("rd_d0_piswap_m","",RooArgSet(D0_M))

# fill them from the tree
for ev in range(nevents):
  ch.GetEntry(ev)
  k_px = ch.K_minus_PX
  k_py = ch.K_minus_PY
  k_pz = ch.K_minus_PZ
  pi_px = ch.Pi_1_PX
  pi_py = ch.Pi_1_PY
  pi_pz = ch.Pi_1_PZ

  k_p4 = TLorentzVector()
  pi_p4 = TLorentzVector()

  k_p4.SetXYZM( k_px, k_py, k_pz, m_k )
  pi_p4.SetXYZM( pi_px, pi_py, pi_pz, m_pi )
  d0_p4 = k_p4 + pi_p4

  k_swap_p4 = TLorentzVector()
  pi_swap_p4 = TLorentzVector()
  k_swap_p4.SetXYZM( k_px, k_py, k_pz, m_pi )
  pi_swap_p4.SetXYZM( pi_px, pi_py, pi_pz, m_k )

  d0_kswap_p4 = k_swap_p4 + pi_p4
  d0_piswap_p4 = k_p4 + pi_swap_p4
  d0_dblswap_p4 = k_swap_p4 + pi_swap_p4

  #print (ch.D0_M, d0_p4.M(), d0_kswap_p4.M(), d0_piswap_p4.M(), d0_dblswap_p4.M() )

  if ( d0_p4.M() >= D0_M.getMin() and d0_p4.M() <= D0_M.getMax() ):
    D0_M.setVal( d0_p4.M() )
    rd_d0_m.add( RooArgSet(D0_M) )
  if ( d0_kswap_p4.M() >= D0_M.getMin() and d0_kswap_p4.M() <= D0_M.getMax() ):
    D0_M.setVal( d0_kswap_p4.M() )
    rd_d0_kswap_m.add( RooArgSet(D0_M) )
  if ( d0_piswap_p4.M() >= D0_M.getMin() and d0_piswap_p4.M() <= D0_M.getMax() ):
    D0_M.setVal( d0_piswap_p4.M() )
    rd_d0_piswap_m.add( RooArgSet(D0_M) )

# now make the fit shapes
w = RooWorkspace('w','w')
getattr(w,'import')(D0_M)
getattr(w,'import')(rd_d0_m)
getattr(w,'import')(rd_d0_kswap_m)
getattr(w,'import')(rd_d0_piswap_m)

# construct PDFs
w.factory( "Gaussian::gaus( D0_M, mu[1840,1880], sigma[0.5,10] )" )
w.factory( "prod::sigma1( s1sc[0.5,10], sigma )" )
w.factory( "prod::sigma2( s2sc[0.5,10], sigma )" )
w.factory( "CBShape::cb1( D0_M, mu, sigma1, alpha1[0,20], n1[3.,1,5] )" )
w.factory( "CBShape::cb2( D0_M, mu, sigma2, alpha2[-20,0], n2[3.,1,5] )" )
w.factory( "SUM::cbs( f1[0.001,0.999]*cb1, cb2 )" )
w.factory( "SUM::sig_pdf( f2[0.001,0.999]*gaus, cbs)" )

w.factory( "ks_dm[83,50,100]" )
w.factory( "ps_dm[80,50,100]" )
w.factory( "expr::ks_mu('mu-ks_dm',mu,ks_dm)" )
w.factory( "expr::ps_mu('mu+ps_dm',mu,ps_dm)" )

w.factory( "ks_ss[2.4,1,5]" )
w.factory( "ps_ss[2.1,1,5]" )
w.factory( "prod::ks_sigma(ks_ss,sigma)" )
w.factory( "prod::ps_sigma(ps_ss,sigma)" )

w.factory( "CBShape::ks_pdf( D0_M, ks_mu, ks_sigma, ks_alpha[0.2,0,1], ks_n[3.,1,5] )" )
w.factory( "CBShape::ps_pdf( D0_M, ps_mu, ps_sigma, ps_alpha[-0.2,-1.,0.], ps_n[3.,1,5] )" )

# fit each contrib individually to get good start params
c = TCanvas()
w.pdf('sig_pdf').fitTo( rd_d0_m )
pl = D0_M.frame()
rd_d0_m.plotOn(pl)
w.pdf('sig_pdf').plotOn(pl, rf.LineColor(kBlack))
mset = w.pdf('sig_pdf').getParameters( RooArgSet(D0_M) )
mset.setAttribAll("Constant")
w.pdf('ks_pdf').fitTo( rd_d0_kswap_m )
rd_d0_kswap_m.plotOn(pl,rf.LineColor(kBlue),rf.MarkerColor(kBlue))
w.pdf('ks_pdf').plotOn(pl, rf.LineColor(kBlue) )
rd_d0_piswap_m.plotOn(pl,rf.LineColor(kRed),rf.MarkerColor(kRed))
w.pdf('ps_pdf').plotOn(pl, rf.LineColor(kRed))
w.pdf('ps_pdf').fitTo( rd_d0_piswap_m )
c.cd()
pl.Draw()
c.Update()
c.Modified()
c.Print(opts.plots+"/D0_M_MCShapesFree.pdf")
c.Print(opts.plots+"/D0_M_MCShapesFree.png")

# now fit them simultaneously
w.factory( 'ID[Corr=0,KS=-1,PS=1]' )
w.factory( 'SIMUL::sim_pdf(ID,Corr=sig_pdf,KS=ks_pdf,PS=ps_pdf)' )

sim_data = RooDataSet('sim_data','',RooArgSet(D0_M), rf.Index(w.cat('ID')), rf.Import("Corr",rd_d0_m), rf.Import("KS",rd_d0_kswap_m), rf.Import("PS",rd_d0_piswap_m) )
getattr(w,'import')(sim_data)

# free pars
w.var('mu').setConstant(False)
w.var('sigma').setConstant(False)
#w.var('s1sc').setConstant(False)
#w.var('s2sc').setConstant(False)
#w.var('alpha1').setConstant(False)
#w.var('alpha2').setConstant(False)
#w.var('n1').setConstant(False)
#w.var('n2').setConstant(False)
#w.var('f1').setConstant(False)
#w.var('f2').setConstant(False)
w.var('ks_alpha').setConstant(True)
w.var('ks_n').setConstant(True)
w.var('ps_alpha').setConstant(True)
w.var('ps_n').setConstant(True)

#w.pdf('sim_pdf').fitTo(sim_data)
params = w.pdf('sim_pdf').getParameters( RooArgSet(D0_M) )
w.saveSnapshot( 'fit_params_mc', params )
w.defineSet( 'fit_pars', params )

leg = TLegend(0.15,0.6,0.4,0.89)
leg.SetLineColor(0)

c2 = TCanvas()
pl = D0_M.frame()
pl.GetXaxis().SetTitle( 'm(K#pi) [MeV]' )
rd_d0_m.plotOn(pl)
leg.AddEntry( pl.getObject(int(pl.numItems())-1), "Correct ID", "LEP" )
w.pdf('sig_pdf').plotOn(pl,rf.LineColor(kBlack))
rd_d0_kswap_m.plotOn(pl,rf.MarkerColor(kBlue), rf.LineColor(kBlue))
leg.AddEntry( pl.getObject(int(pl.numItems())-1), "D#rightarrowKK MisID", "LEP" )
w.pdf('ks_pdf').plotOn(pl,rf.LineColor(kBlue))
rd_d0_piswap_m.plotOn(pl,rf.MarkerColor(kRed), rf.LineColor(kRed))
leg.AddEntry( pl.getObject(int(pl.numItems())-1), "D#rightarrow#pi#pi MisID", "LEP" )
w.pdf('ps_pdf').plotOn(pl,rf.LineColor(kRed))
rd_d0_m.plotOn(pl)
w.pdf('sig_pdf').plotOn(pl,rf.LineColor(kBlack))
pl.Draw()
leg.Draw()
c2.Update()
c2.Modified()
c2.Print(opts.plots+'/D0_M_MCShapes.pdf')
c2.Print(opts.plots+'/D0_M_MCShapes.png')

c3 = TCanvas()
pl2 = D0_M.frame(rf.Range(1790,1940))
pl2.GetXaxis().SetTitle( 'm(K#pi) [MeV]' )
rd_d0_m.plotOn(pl2)
w.pdf('sig_pdf').plotOn(pl2,rf.LineColor(kBlack))
rd_d0_kswap_m.plotOn(pl2,rf.MarkerColor(kBlue), rf.LineColor(kBlue))
w.pdf('ks_pdf').plotOn(pl2,rf.LineColor(kBlue))
rd_d0_piswap_m.plotOn(pl2,rf.MarkerColor(kRed), rf.LineColor(kRed))
w.pdf('ps_pdf').plotOn(pl2,rf.LineColor(kRed))
rd_d0_m.plotOn(pl2)
w.pdf('sig_pdf').plotOn(pl2,rf.LineColor(kBlack))
pl2.Draw()
leg.Draw()
c3.Update()
c3.Modified()
c3.Print(opts.plots+'/D0_M_MCShapesZoom.pdf')
c3.Print(opts.plots+'/D0_M_MCShapesZoom.png')

w.writeToFile(opts.output)

print ('Fitted:')
params.Print('v')


