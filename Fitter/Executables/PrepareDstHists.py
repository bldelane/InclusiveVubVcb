from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-g','--inputdg'  , default='Histograms/D0MuNu/nominal/mc_bc2d0gmunu_2016.pkl'  , help='Input D0g file')
parser.add_argument('-p','--inputdpi' , default='Histograms/D0MuNu/nominal/mc_bc2d0pi0munu_2016.pkl', help='Input D0pi0 file')
parser.add_argument('-o','--outputdst', default='Histograms/D0MuNu/nominal/mc_bc2dst0munu_2016.pkl' , help='Output Dst0 file')
opts = parser.parse_args()

import os
import sys
sys.path.append(os.path.join(os.getcwd(),'python'))
import utils as utils

dg  = utils.readHistogram( opts.inputdg )
dpi = utils.readHistogram( opts.inputdpi )

# this is arbitary but keeps things in perspective of how much MC we have
totevs = dg.sum().value + dpi.sum().value

dst = ( 0.353 * (dg / dg.sum().value) + 0.647 * (dpi / dpi.sum().value ) ) * totevs

utils.writeHistogram( dst, opts.outputdst )
