import os
import sys
import fnmatch
outbase = "/usera/mkenzie/public_html/Bc2D0MuNuX/Fit2" if len(sys.argv)<2 else sys.argv[1]

ftypes = ['png','pdf']

def collate_folder( path, outloc, matchspec="*", atback=False ):

  if not os.path.exists(outloc):
    os.system('mkdir -p %s'%outloc)

  for root, dirs, files in os.walk(path):
    fs = { ftyp : fnmatch.filter(files,"%s.%s"%(matchspec,ftyp)) for ftyp in ftypes }
    if sum( [ len(fs[ftyp]) for ftyp in ftypes] )==0: continue
    basepath = root.split(os.path.join(path,""))[-1]
    newbase  = basepath.replace("/","_")
    for ext, fils in fs.items():
      for f in fils:
        #print('cp %s %s'%(os.path.join(root,f),os.path.join(outloc,newbase+"_"+f)))
        oldf = os.path.join(root,f)
        newf = os.path.join(outloc,os.path.splitext(f)[0]+"_"+newbase+os.path.splitext(f)[1]) if atback else os.path.join(outloc,newbase+"_"+f)
        os.system('cp %s %s'%(oldf,newf))

os.system("rm -rf %s"%outbase)

for folder in ['mcshapes','datashapes','binyields']:
  for channel in ['D0MuNu','JpsiMuNu']:
    outpath = os.path.join(outbase,folder,channel)
    if folder=='binyields':
      collate_folder( os.path.join("plots",folder,channel), outpath, "histogram*", atback=True )
    else:
      collate_folder( os.path.join("plots",folder,channel), outpath )

#os.system("Executables/make_html /usera/mkenzie/public_html/Bc2D0MuNuX/Fit2 -c -l -o -P 3 -s --newLine -w 600 --title \"Bc2D0MuNu Fit Results\"")
