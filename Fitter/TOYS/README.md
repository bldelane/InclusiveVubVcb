
# TOYS 
Toys section of the fitter:
- Evaluate bias in binning scheme
- Bias in the fit 
- Closure tests

## KDE Smoothing
Ensure full support on templates. Derive analytic pdfs to study the effects of spurious bins and then generate pseudo-datasets.

## Tasks
- Implement rule for boostrap
- Profile pulls: study average pull in bins of mcorr and ltime.