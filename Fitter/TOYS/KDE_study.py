import ROOT
from ROOT import RooFit as rf
from ROOT import gROOT, gStyle
gROOT.SetBatch()
gROOT.ProcessLine(".L /home/blaise/cernbox/VubVcb/InclusiveVubVcb/Pipeline/common/plotting/lhcbStyle.C")
gStyle.SetTitleXSize(0.07);
gStyle.SetTitleYSize(0.07);
gStyle.SetTitleXOffset(0.95);
gStyle.SetTitleYOffset(1.1);
gStyle.SetPadLeftMargin(0.15);

from pathlib import Path
import argparse, os
parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mode", help="decay mode: select either `D0MuNu` or `JpsiMuNu`", required="True")
parser.add_argument("-i", "--input", help="input ~/fit/.py file to sWeight", required=True)
parser.add_argument("-p", "--path", help="source path of fit files", required=True)
parser.add_argument("-o", "--output", help="path+filename of output files", required=True)
parser.add_argument("-M", "--massbins", help="number of bins in corrected B mass", required=True, type=int)
parser.add_argument("-t", "--taubins", help="number of bins in B lifetime", required=True, type=int)
parser.add_argument("-y", "--year", help="year of data-taking", required=True)
args = parser.parse_args()


def KDE():
    infile = ROOT.TFile(args.input)
    intree = infile.Get("DecayTree")
    if args.mode=="D0MuNu":
        h_c = "D0"
    if args.mode=="JpsiMuNu":
        h_c = "Jpsi"
    
    # create dataset 
    Bmass = ROOT.RooRealVar("B_plus_MCORR", "B_plus_MCORR", 3500, 8500)
    Btau = ROOT.RooRealVar("B_plus_FIT_LTIME", "B_plus_FIT_LTIME", 0., 0.004)
    h_c_sw = ROOT.RooRealVar(f"{h_c}_sw", f"{h_c}_sw", -2., 2)
    ds = ROOT.RooDataSet("ds", "ds", intree, ROOT.RooArgSet(Bmass, Btau, h_c_sw), "", f"{h_c}_sw")
    print(">>> DATASET CHECKS:")
    ds.Print()

    # KDE on sweighted dataset
    kde = ROOT.RooNDKeysPdf( "kde", "kde", ROOT.RooArgList( Bmass, Btau), ds, "a", 1.5, 2. )
        
    # ===== PLOTS =====
    c_ = ROOT.TCanvas("kde_2d_canvas", "kde_2d_canvas", 1200, 600)
    c_.Divide(2)
    
    c_.cd(1)
    pad_1 = ROOT.TPad("pad_mass_1", "pad_mass_1", 0, 0.3, 1, 1)
    pad_2 = ROOT.TPad("pad_mass_2", "pad_mass_2", 0, 0, 1, 0.3)
    pad_1.SetBottomMargin(0.)
    pad_2.SetTopMargin(0.)
    pad_2.SetBottomMargin (0.5)
    pad_1.Draw()
    pad_2.Draw()

    pad_1.cd()
    mcorr_frame = Bmass.frame(ROOT.RooFit.Title("2D KDE projection: B_MCORR"), 
                              ROOT.RooFit.Bins(args.massbins))

    ds.plotOn(mcorr_frame, ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2))
    kde.plotOn(mcorr_frame, rf.LineColor(ROOT.kAzure+2), rf.Name("KDE_2D"))
    mcorr_frame.Draw()

    legend=ROOT.TLegend(0.5,0.7,0.8,0.8)
    legend.SetTextFont(132)
    legend.SetTextSize(0.075)
    legend.AddEntry("", f"LHCb Data {args.year}","")
    legend.Draw("SAME")

    #pulls
    pad_2.cd()
    hpull = mcorr_frame.pullHist()
    frame_pulls = Bmass.frame(ROOT.RooFit.Title("Pull Distribution"))
    frame_pulls.addPlotable(hpull, "P")
    if args.mode=="D0MuNu":
            frame_pulls.GetXaxis().SetTitle("#it{m_{corr}(D^{0}#mu^{+})}")
    if args.mode=="JpsiMuNu":
            frame_pulls.GetXaxis().SetTitle("#it{m_{corr}(J/#psi #mu^{+})}")
    frame_pulls.GetYaxis().SetTitle("Pulls")
    frame_pulls.GetXaxis().SetTitleSize(0.2)
    frame_pulls.GetXaxis().SetLabelSize(0.14)
    frame_pulls.Draw("AP")
    frame_pulls.GetYaxis().SetRangeUser(-5, 5.)
    l = ROOT.TLine()
    l.SetLineWidth(3)
    l.SetLineColor(ROOT.kGray)
    l.DrawLine( frame_pulls.GetXaxis().GetXmin(),0., frame_pulls.GetXaxis().GetXmax(),0. )
    frame_pulls.Draw("Psame")

    # LIFETIME
    c_.cd(2)
    pad_2_1 = ROOT.TPad("pad_ltime_1", "pad_ltime_1", 0, 0.3, 1, 1)
    pad_2_2 = ROOT.TPad("pad_ltime_2", "pad_ltime_2", 0, 0, 1, 0.3)
    pad_2_1.SetBottomMargin(0.)
    pad_2_2.SetTopMargin(0.)
    pad_2_2.SetBottomMargin (0.5)
    pad_2_1.Draw()
    pad_2_2.Draw()

    pad_2_1.cd()
    ltime_frame = Btau.frame(ROOT.RooFit.Title("2D KDE projection: B_LTIME"), 
                             ROOT.RooFit.Bins(args.taubins))

    ds.plotOn(ltime_frame, ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2))
    kde.plotOn(ltime_frame, rf.LineColor(ROOT.kAzure+2))
    ltime_frame.Draw()

    legend_2=ROOT.TLegend(0.5,0.7,0.8,0.8)
    legend_2.SetTextFont(132)
    legend_2.SetTextSize(0.075)
    legend_2.AddEntry("", f"LHCb Data {args.year}","")
    legend_2.Draw("SAME")
        
    # pulls    
    pad_2_2.cd()
    hpull_2 = ltime_frame.pullHist()
    frame_pulls_2 = Btau.frame(ROOT.RooFit.Title("Pull Distribution LTIME"))
    frame_pulls_2.addPlotable(hpull_2, "P")
    frame_pulls_2.GetXaxis().SetTitle("#tau(#it{B_{c}^{+}})")
    frame_pulls_2.GetYaxis().SetTitle("Pulls")
    frame_pulls_2.GetXaxis().SetTitleSize(0.2)
    frame_pulls_2.GetXaxis().SetLabelSize(0.14)
    frame_pulls_2.Draw("AP")
    frame_pulls_2.GetYaxis().SetRangeUser(-5, 5.)
    l2 = ROOT.TLine()
    l2.SetLineWidth(3)
    l2.SetLineColor(ROOT.kGray)
    l2.DrawLine( frame_pulls_2.GetXaxis().GetXmin(),0., frame_pulls_2.GetXaxis().GetXmax(),0. )
    frame_pulls_2.Draw("Psame")

    c_.Update()
    c_.Modified()
    
    destination_file = args.input.replace(args.path, "")
    destination_ = os.path.dirname(destination_file)
    Path(f"./rf_kde_plots/{destination_}").mkdir(parents=True, exist_ok=True)
    c_.SaveAs(f"./rf_kde_plots/{destination_file[:-5]}_kde_mcorr_ltime.pdf")
        
    # 3D Canvas
    c_3D = ROOT.TCanvas("KDE_3D", "KDE_3D", 800, 600)        
    c_3D.cd()
        
    hh_data = ROOT.RooAbsData.createHistogram(ds, "hh_data", Bmass, ROOT.RooFit.Binning(
        30), ROOT.RooFit.YVar(Btau, ROOT.RooFit.Binning(8)))        
    hh_data.SetLineColor(ROOT.kBlack)
    hh_data.SetLineWidth(2)
        
    hh_pdf = kde.createHistogram("hh_pdf", Bmass, ROOT.RooFit.Binning(
        30), ROOT.RooFit.YVar(Btau, ROOT.RooFit.Binning(8)))                
    gStyle.SetPalette(ROOT.kBird)
    hh_pdf.Draw("SURF2")
    hh_data.Draw("LEGO SAME")
    
    c_3D.SaveAs(f"./rf_kde_plots/{destination_file[:-5]}_3D_kde_mcorr_ltime.pdf")

    # write workspace 
    w = ROOT.RooWorkspace("w", "workspace")
    getattr(w, 'import')(kde)
    
    storage_destination = os.path.dirname(args.output)
    Path(f"{storage_destination}").mkdir(parents=True, exist_ok=True)
    w.writeToFile(args.output)

if __name__ == "__main__":
    KDE()