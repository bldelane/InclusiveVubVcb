import ROOT
import pandas
import zfit
from zfit.loss import ExtendedUnbinnedNLL
from zfit.minimize import Minuit
import os, glob
from pathlib import Path
from scipy import stats
import numpy as np
from root_pandas import to_root
os.environ["ZFIT_DISABLE_TF_WARNINGS"] = "1"

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-m", "--mode", help="Decay mode: select either `D0MuNu` or `JpsiMuNu`", required="True")
parser.add_argument("-i", "--input", help="Input ~/fit/.py file to sWeight", required=True)
parser.add_argument("-p", "--path", help="source path of fit files", required=True)
parser.add_argument("-o", "--output", help="path+filename of output files", required=True)
args = parser.parse_args()

#cosmetics
import mplhep
import matplotlib.pyplot as plt
import matplotlib
from hepstats.splot import compute_sweights
#import mplhep as hep
#plt.style.use(hep.style.LHCb)
from utils import pltdist, plotfitresult


plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif"
    })


RDF = ROOT.ROOT.RDataFrame

def sw_fit():
    # no mass cuts on h_c if B visible mass to keep high stats in sWeights: impose DCS and h_c mass range
    if args.mode=="D0MuNu":
        in_df = pandas.DataFrame(RDF("DecayTree", args.input).Filter("(K_minus_ID*Mu_plus_ID)>0 && (D0_M>1790 && D0_M<1940)").AsNumpy()) 
        h_c = "D0"
        h_c_nomM = 1865.
    if args.mode=="JpsiMuNu":
        in_df = pandas.DataFrame(RDF("DecayTree", args.input).Filter("(Jpsi_M>3020 && Jpsi_M<3190").AsNumpy()) 
        h_c = "Jpsi"
        h_c_nomM = 3097.

    
    # fit the chamr hadron h_c mass
    charm_M = in_df[f"{h_c}_M"].to_numpy()

    # initialise 
    bounds=(min(charm_M), max(charm_M))
    obs = zfit.Space("x", limits=bounds)
    data_ = zfit.data.Data.from_numpy(obs=obs, array=charm_M)
    Nsig = zfit.Parameter("Nsig", 100, 0., len(charm_M))
    Nbkg = zfit.Parameter("Nbkg", 100, 0., len(charm_M))

    # pdf pars
    mu = zfit.Parameter("mu", h_c_nomM, 1835., 1895.)
    sigma = zfit.Parameter("sigma", 30, 0, 60)
    data_ = zfit.data.Data.from_numpy(obs=obs, array=charm_M)
    lambda_ = zfit.Parameter("lambda",-0.03, -4.0, 0.0)

    # model
    signal = zfit.pdf.Gauss(obs=obs, mu=mu, sigma=sigma).create_extended(Nsig)
    background = zfit.pdf.Exponential(obs=obs, lambda_=lambda_).create_extended(Nbkg)
    tot_model = zfit.pdf.SumPDF([signal, background])
    data_ = zfit.data.Data.from_numpy(obs=obs, array=charm_M)

    # minimise extended nll, all params floating
    nll = ExtendedUnbinnedNLL(model=tot_model, data=data_)
    minimizer = Minuit()
    result = minimizer.minimize(loss=nll)
    result.hesse()
    
    # pipe plots and logs in directories
    destination_file = args.input.replace(args.path, "")
    destination_ = os.path.dirname(destination_file)
    Path(f"./sw_plots/{destination_}").mkdir(parents=True, exist_ok=True)
    Path(f"./sw_logs/{destination_}").mkdir(parents=True, exist_ok=True)
    
    with open(f"./sw_logs/{destination_file[:-5]}_sw.log", "w") as f:
        print(result, file=f)

    # compute the sweights
    s_weights = compute_sweights(tot_model, charm_M)

    # plot the sweights to check 
    plt.figure()
    plt.minorticks_on()
    plt.tick_params(axis='both', which='major', direction="in")
    plt.tick_params(axis='both', which='minor', direction="in")
    plt.scatter(charm_M[:-1], s_weights[Nsig][:-1], label="Signal sWeights", color="crimson", marker=".", linewidths=1)
    plt.scatter(charm_M[:-1], s_weights[Nbkg][:-1], label="Background sWeights", color="steelblue", marker=".", linewidths=1)
    plt.plot(charm_M, s_weights[Nsig] + s_weights[Nbkg], "-k", label=r"$\sum_i s\mathcal{W}_i $")
    if args.mode=="D0MuNu":
        plt.xlabel(r"$m(K^{\pm}\pi^{\mp})$ [MeV/c$^2$]", fontsize=20)
    if args.mode=="JpsiMuNu":
        plt.xlabel(r"$m(\mu^{+}\mu^{-})$ [MeV/c$^2$]", fontsize=20)
    plt.ylabel(f"Computed sWeights", fontsize=20)
    plt.legend(loc="best")
    plt.savefig(f"./sw_plots/{destination_file[:-5]}_sw.pdf")


    # plot the fit
    fig = plt.figure()
    plt.minorticks_on()
    plt.tick_params(axis='both', which='major', direction="in")
    plt.tick_params(axis='both', which='minor', direction="in")
    nbins = 80
    pltdist(charm_M, nbins, bounds)
    plotfitresult(background, bounds, nbins, label=r"Fake $D^{0}$ Background", color="teal")
    plotfitresult(signal, bounds, nbins, label=r"Real $D^{0}$ Signal", color="royalblue", linestyle="-")
    plotfitresult(tot_model, bounds, nbins, label=r"Total Fit Model", color="crimson")
    if args.mode=="D0MuNu":
        plt.xlabel(r"$m(K^{\pm}\pi^{\mp})$ [MeV/c$^2$]", fontsize=20)
    if args.mode=="JpsiMuNu":
        plt.xlabel(r"$m(\mu^{+}\mu^{-})$ [MeV/c$^2$]", fontsize=20)
    plt.ylabel(f"Events [Arbitrary Units]", fontsize=20)
    plt.legend(loc="best")
    plt.savefig(f"./sw_plots/{destination_file[:-5]}__fit.pdf")

    # write to file for Meerkat
    in_df[f"{h_c}_sw"] = s_weights[Nsig]
    
    # apply final mass cut here at 3500 MeV if D0MuNu -> derive 2D KDE for the fit templates
    if args.mode=="D0MuNu":
        out_df = in_df.query("B_plus_M>3500")[["B_plus_MCORR", "B_plus_FIT_LTIME", f"{h_c}_M", f"{h_c}_sw"]]
    if args.mode=="JpsiMuNu":
        out_df = in_df[["B_plus_MCORR", "B_plus_FIT_LTIME", f"{h_c}_M", f"{h_c}_sw"]]
    
    storage_destination = os.path.dirname(args.output)
    Path(f"{storage_destination}").mkdir(parents=True, exist_ok=True)
    out_df.to_root(args.output, key="DecayTree") 

if __name__ == "__main__":
    sw_fit()