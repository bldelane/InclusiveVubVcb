import ROOT
import pandas
import os
from pathlib import Path
import numpy as np
from root_pandas import to_root


# read in the tuples produced by the ANA pipeline
# misid DCS 2016 - branches of interest
RDF = ROOT.ROOT.RDataFrame
misid_dcs_16_df = pandas.DataFrame(RDF("DecayTree", "~/scratch/ROOTFILES/fit/D0MuNu/2016/Bc2D0MuNuX_misid.root").Filter("(K_minus_ID*Mu_plus_ID)>0").AsNumpy())
misid_dcs_16_df = misid_dcs_16_df[["B_plus_MCORR", "B_plus_FIT_LTIME", "D0_M"]]

# --- bootstrap: same-sized sampled, randomly sampling w/ replacement
n_pseudo_dsets = 2
pseudo_dsets = {}

for i in range(n_pseudo_dsets):
    pseudo_dsets[f"bootstrap_{i}"] = misid_dcs_16_df.sample(frac=1,
                                                        replace=True,
                                                        random_state=i)

scratch_path = "/home/blaise/scratch/ROOTFILES/fit/kde/bootstrap"
[ val.to_root(f"{scratch_path}/misid_dcs16_{key}.root", key="DecayTree") for key, val in pseudo_dsets.items() ]