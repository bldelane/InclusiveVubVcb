import sys
import uproot
import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import crystalball, expon

from iminuit import Minuit
from iminuit.cost import UnbinnedNLL

# read the MC file
mctree = uproot.open(sys.argv[1]+':DecayTree')
df = mctree.arrays( ['K_minus_PX','K_minus_PY','K_minus_PZ','Pi_1_PX','Pi_1_PY','Pi_1_PZ','D0_M'], library='pd' )

# make the mass swap variables
m_k  = 493.677
m_pi = 139.57018

K_p3  = df[['K_minus_PX','K_minus_PY','K_minus_PZ']]
Pi_p3 = df[['Pi_1_PX','Pi_1_PY','Pi_1_PZ']]

df['K_minus_PE'] = np.sqrt( df.K_minus_PX**2 + df.K_minus_PY**2 + df.K_minus_PZ**2 + m_k**2 )
df['Pi_1_PE']    = np.sqrt( df.Pi_1_PX**2 + df.Pi_1_PY**2 + df.Pi_1_PZ**2 + m_pi**2 )

df['K_minus_SwapPE'] = np.sqrt( df.K_minus_PX**2 + df.K_minus_PY**2 + df.K_minus_PZ**2 + m_pi**2 )
df['Pi_1_SwapPE']    = np.sqrt( df.Pi_1_PX**2 + df.Pi_1_PY**2 + df.Pi_1_PZ**2 + m_k**2 )

df['CheckM']  = np.sqrt( (df.K_minus_PE+df.Pi_1_PE)**2     - (df.K_minus_PX+df.Pi_1_PX)**2 - (df.K_minus_PY+df.Pi_1_PY)**2 - (df.K_minus_PZ+df.Pi_1_PZ)**2 )
df['SwapPiM'] = np.sqrt( (df.K_minus_PE+df.Pi_1_SwapPE)**2 - (df.K_minus_PX+df.Pi_1_PX)**2 - (df.K_minus_PY+df.Pi_1_PY)**2 - (df.K_minus_PZ+df.Pi_1_PZ)**2 )
df['SwapKM']  = np.sqrt( (df.K_minus_SwapPE+df.Pi_1_PE)**2 - (df.K_minus_PX+df.Pi_1_PX)**2 - (df.K_minus_PY+df.Pi_1_PY)**2 - (df.K_minus_PZ+df.Pi_1_PZ)**2 )

# plot
mrange = (1600,2100)
frange = (1790,1940)

fig, ax = plt.subplots()
ax.hist( df['D0_M'].to_numpy(), range=mrange, bins=100, label='Nominal' )
ax.hist( df['SwapPiM'].to_numpy(), range=mrange, bins=100, label='$\pi$ swap' )
ax.hist( df['SwapKM'].to_numpy(), range=mrange, bins=100, label='$K$ swap' )
ax.legend()
fig.tight_layout()
fig.savefig('plots/mc_mass_swaps.pdf')

# fit the K swap
mrange = (1760,1940)

ks_data = df['SwapKM'].to_numpy()
ks_data = ks_data[ (ks_data>mrange[0]) & (ks_data<mrange[1]) ]

def ks_pdf(x, mu, sg, a1, n1):
  cb = crystalball(a1,n1,mu,sg)
  cbn = np.diff(cb.cdf(mrange))
  return cb.pdf(x) / cbn

ksmi = Minuit( UnbinnedNLL( ks_data, ks_pdf ),
               mu = 1790,
               sg = 20,
               a1 = 0.1,
               n1 = 3 )

ksmi.limits['mu'] = (1750,1850)
ksmi.limits['sg'] = (0,50)
ksmi.limits['a1'] = (0.1,10)
ksmi.limits['n1'] = (1,5)
ksmi.fixed['n1'] = True

ksmi.migrad()
ksmi.hesse()

print(ksmi)

# write pars to a file
f = open('kspars.log','w')
f.write('CB\n')
for par in ksmi.parameters:
  f.write('{:4s} {:f} {:f}\n'.format(par,ksmi.values[par],ksmi.errors[par]))
f.close()

# plot the K swap fit
fig, ax = plt.subplots(2,1,figsize=(8,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})
pnh, pxe = np.histogram(ks_data, bins=50, range=mrange)
pcx = 0.5*(pxe[1:]+pxe[:-1])
ax[0].errorbar( pcx, pnh, pnh**0.5, fmt='ko', capsize=2, label='$K$ swap MC')
x = np.linspace(*mrange,400)
N = (mrange[1]-mrange[0])/50 * len(ks_data)
#ax[0].plot(x, N * ks_pdf_pl(x, *ksmi.values, cb1_only=True), 'r--', label='CB1' )
#ax[0].plot(x, N * ks_pdf_pl(x, *ksmi.values, cb2_only=True), 'g:' , label='CB2' )
ax[0].plot(x, N * ks_pdf(x, *ksmi.values), 'b-' , label='Total PDF')
ax[0].plot( (frange[0],frange[0]), (0,ax[0].get_ylim()[1]), 'k--' )

pull = np.nan_to_num( (pnh - N * ks_pdf(pcx, *ksmi.values) ) / pnh**0.5, nan=0, posinf=0, neginf=0 )
ax[1].fill_between(x, -1, 1, alpha=0.5, color='0.5')
ax[1].plot(x, np.zeros_like(x), 'b-')
ax[1].errorbar( pcx, pull, np.ones_like(pcx), capsize=2, fmt='ko' )
ylim = ax[1].get_ylim()

y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-y,y)

ax[1].set_xlabel('$m(D^0)$ [MeV]')
ax[0].set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/50) ) )
ax[1].set_ylabel('Pull')
ax[0].legend()

fig.tight_layout()
fig.savefig('plots/mc_kswap_fit.pdf')

# fit the pi swap
mrange = (1790,2100)

ps_data = df['SwapPiM'].to_numpy()
ps_data = ps_data[ (ps_data>mrange[0]) & (ps_data<mrange[1]) ]

# fit pdf function
def ps_pdf(x, mu, sg, f, a1, n1, a2, n2):
  cb1 = crystalball(a1,n1,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2 = crystalball(a2,n2,mu,sg)
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  return f*cb1.pdf(invx) / cb1n + (1-f)*cb2.pdf(x) / cb2n

# plot pdf function (option for different components)
def ps_pdf_pl(x, mu, sg, f, a1, n1, a2, n2, cb1_only=False, cb2_only=False):
  cb1 = crystalball(a1,n1,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2 = crystalball(a2,n2,mu,sg)
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  if cb1_only: return f*cb1.pdf(invx) / cb1n
  if cb2_only: return (1-f)*cb2.pdf(x) / cb2n
  return f*cb1.pdf(invx) / cb1n + (1-f)*cb2.pdf(x) / cb2n

psmi = Minuit( UnbinnedNLL( ps_data, ps_pdf ),
               mu = 1950,
               sg = 20,
               f  = 0.5,
               a1 = 0.4,
               n1 = 3.6,
               a2 = 1.,
               n2 = 3)

psmi.limits['mu'] = (1850,2050)
psmi.limits['sg'] = (0,50)
psmi.limits['f'] = (0,1)
psmi.limits['a1'] = (0.1,10)
psmi.limits['n1'] = (1,5)
psmi.limits['a2'] = (0.1,10)
psmi.limits['n2'] = (1,5)
psmi.fixed['n1'] = True
psmi.fixed['n2'] = True

psmi.migrad()
psmi.hesse()

print(psmi)

# write pars to a file
f = open('pspars.log','w')
f.write('DCB\n')
for par in psmi.parameters:
  f.write('{:4s} {:f} {:f}\n'.format(par,psmi.values[par],psmi.errors[par]))
f.close()

# plot the pi swap fit
fig, ax = plt.subplots(2,1,figsize=(8,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})
pnh, pxe = np.histogram(ps_data, bins=50, range=mrange)
pcx = 0.5*(pxe[1:]+pxe[:-1])
ax[0].errorbar( pcx, pnh, pnh**0.5, fmt='ko', capsize=2, label='$\pi$ swap MC')
x = np.linspace(*mrange,400)
N = (mrange[1]-mrange[0])/50 * len(ps_data)
ax[0].plot(x, N * ps_pdf_pl(x, *psmi.values, cb1_only=True), 'r--', label='CB1' )
ax[0].plot(x, N * ps_pdf_pl(x, *psmi.values, cb2_only=True), 'g:' , label='CB2' )
ax[0].plot(x, N * ps_pdf(x, *psmi.values), 'b-' , label='Total PDF')
ax[0].plot( (frange[1],frange[1]), (0,ax[0].get_ylim()[1]), 'k--' )

pull = np.nan_to_num( (pnh - N * ps_pdf(pcx, *psmi.values) ) / pnh**0.5, nan=0, posinf=0, neginf=0 )
ax[1].fill_between(x, -1, 1, alpha=0.5, color='0.5')
ax[1].plot(x, np.zeros_like(x), 'b-')
ax[1].errorbar( pcx, pull, np.ones_like(pcx), capsize=2, fmt='ko' )
ylim = ax[1].get_ylim()

y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-y,y)

ax[1].set_xlabel('$m(D^0)$ [MeV]')
ax[0].set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/50) ) )
ax[1].set_ylabel('Pull')
ax[0].legend()

fig.tight_layout()
fig.savefig('plots/mc_pswap_fit.pdf')

## fit the signal
mrange = (1810,1920)

mcsig_data = df['D0_M'].to_numpy()
mcsig_data = mcsig_data[ (mcsig_data>mrange[0]) & (mcsig_data<mrange[1]) ]

def mcsig_pdf(x, mu, sg, f, a1, n1, a2, n2):
  cb1 = crystalball(a1,n1,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2 = crystalball(a2,n2,mu,sg)
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  #if cb1_only: return f*cb1.pdf(invx) / cb1n
  #if cb2_only: return (1-f)*cb2.pdf(x) / cb2n
  return f*cb1.pdf(invx) / cb1n + (1-f)*cb2.pdf(x) / cb2n

mcsigmi = Minuit( UnbinnedNLL( mcsig_data, mcsig_pdf ),
               mu = 1860,
               sg = 5,
               f = 0.1,
               a1 = 0.1,
               n1 = 3,
               a2 = 0.1,
               n2 = 3)

mcsigmi.limits['mu'] = (1840,1900)
mcsigmi.limits['sg'] = (2,10)
mcsigmi.limits['f'] = (0,1)
mcsigmi.limits['a1'] = (0.1,20)
mcsigmi.limits['a2'] = (0.1,20)
mcsigmi.limits['n1'] = (1,10)
#mcsigmi.fixed['n1'] = True
mcsigmi.limits['n2'] = (1,10)
#mcsigmi.fixed['n2'] = True

mcsigmi.migrad()
mcsigmi.hesse()

print(mcsigmi)

# write pars to a file
f = open('mcsigpars.log','w')
f.write('CB\n')
for par in mcsigmi.parameters:
  f.write('{:4s} {:f} {:f}\n'.format(par,mcsigmi.values[par],mcsigmi.errors[par]))
f.close()

# plot the K swap fit
fig, ax = plt.subplots(2,1,figsize=(8,8),sharex=True,gridspec_kw={'height_ratios': [3, 1]})
pnh, pxe = np.histogram(mcsig_data, bins=50, range=mrange)
pcx = 0.5*(pxe[1:]+pxe[:-1])
ax[0].errorbar( pcx, pnh, pnh**0.5, fmt='ko', capsize=2, label='$K$ swap MC')
x = np.linspace(*mrange,400)
N = (mrange[1]-mrange[0])/50 * len(mcsig_data)
#ax[0].plot(x, N * mcsig_pdf_pl(x, *mcsigmi.values, cb1_only=True), 'r--', label='CB1' )
#ax[0].plot(x, N * mcsig_pdf_pl(x, *mcsigmi.values, cb2_only=True), 'g:' , label='CB2' )
ax[0].plot(x, N * mcsig_pdf(x, *mcsigmi.values), 'b-' , label='Total PDF')

pull = np.nan_to_num( (pnh - N * mcsig_pdf(pcx, *mcsigmi.values) ) / pnh**0.5, nan=0, posinf=0, neginf=0 )
ax[1].fill_between(x, -1, 1, alpha=0.5, color='0.5')
ax[1].plot(x, np.zeros_like(x), 'b-')
ax[1].errorbar( pcx, pull, np.ones_like(pcx), capsize=2, fmt='ko' )
ylim = ax[1].get_ylim()

y = max(abs(ylim[0]),abs(ylim[1]))
ax[1].set_ylim(-y,y)

ax[1].set_xlabel('$m(D^0)$ [MeV]')
ax[0].set_ylabel('Events / {0} MeV'.format( (mrange[1]-mrange[0]/50) ) )
ax[1].set_ylabel('Pull')
ax[0].legend()

fig.tight_layout()
fig.savefig('plots/mc_mcsig_fit.pdf')
plt.show()

