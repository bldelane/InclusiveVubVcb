#cosmetics
#import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib
#plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble":r"\usepackage{amsmath}",
    })
from termcolor2 import c

from argparse import ArgumentParser
import uproot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
#mpl.use('agg')

from iminuit import Minuit
from iminuit.cost import ExtendedBinnedNLL, ExtendedUnbinnedNLL
from numba_stats import norm_pdf, norm_cdf, expon_pdf, expon_cdf
from scipy.stats import crystalball, expon
from SWeighter import SWeight

parser = ArgumentParser()
parser.add_argument('-i','--input',  required=True, help='Input filename (.root) file', choices=["sig", "comb", "misid_hadron_enriched"])
parser.add_argument('-o','--output', required=False, help='Output file name (default is input with "_sw" suffix)')
parser.add_argument('-y','--year',  required=True, help='Year of data-taking/MC', choices=["2011", "2012", "2015", "2016", "2017", "2018"])
parser.add_argument('-m','--mode', default="D0MuNu", help='Decay mode, choose from [DoMuNu|JpsiMuNu]', choices=["D0MuNu", "JpsiMuNu"])
parser.add_argument('-l','--label', default=None )
parser.add_argument('-d','--dcs',   action='store_true') # default False
parser.add_argument('-c','--cf',    action='store_true') # default False
parser.add_argument('-t','--test',  action='store_true', help="Test mode: will store the plots and *.pkl in Bc2D0MuNuX/Fitter/sweights") # default False

opts = parser.parse_args()
if opts.output is None: opts.output = opts.input.replace('.root','_sw.pkl')

def read_pars(fil):
  res = {}
  f = open(fil)
  ls = f.readlines()[1:]
  for l in ls:
    res[ l.split()[0] ] = float(l.split()[1])
  f.close()
  return res

def cdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb):

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.cdf(x)/cb1n + (1-f)*(1-cb2.cdf(invx)/cb2n)

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.cdf(x)/expn

  ks = 0
  ps = 0
  if opts.dcs:
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.cdf(x)/kscbn

    # ps swap
    #pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-ps_pars['mu']+mrange[0],ps_pars['sg'])
    #pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],ps_pars['mu'],ps_pars['sg'])
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*(1-pscb1.cdf(invx)/pscb1n) + (1-ps_pars['f'])*pscb2.cdf(x)/pscb2n

  return ns * sig + nb * bkg + nks * ks + nps * ps

def pdf(x, ns, nb, nks, nps, f, mu, sg, a1, a2, n1, n2, ksmu, kssg, psmu, pssg, lb, comps=None):

  if comps is None:
    comps = ['sig','bkg','ks','ps']

  tot = 0

  # signal
  cb1 = crystalball(a1,n1,mu,sg)
  cb2 = crystalball(a2,n2,mrange[1]-mu+mrange[0],sg)
  cb1n = np.diff(cb1.cdf(mrange))
  cb2n = np.diff(cb2.cdf(mrange))
  invx = mrange[1]-x+mrange[0]
  sig = f * cb1.pdf(x)/cb1n + (1-f)*(cb2.pdf(invx)/cb2n)
  if 'sig' in comps: tot += ns*sig

  # background
  exp = expon(mrange[0], lb)
  expn = np.diff(exp.cdf(mrange))
  bkg = exp.pdf(x)/expn
  if 'bkg' in comps: tot += nb*bkg

  ks = 0
  ps = 0
  if opts.dcs:
    # ks swap
    kscb = crystalball(ks_pars['a1'],ks_pars['n1'],ksmu,kssg)
    kscbn = np.diff( kscb.cdf(mrange) )
    ks = kscb.pdf(x)/kscbn
    if 'ks' in comps: tot += nks*ks

    # ps swap
    pscb1 = crystalball(ps_pars['a1'],ps_pars['n1'],mrange[1]-psmu+mrange[0],pssg)
    pscb2 = crystalball(ps_pars['a2'],ps_pars['n2'],psmu,pssg)
    pscb1n = np.diff(pscb1.cdf(mrange))
    pscb2n = np.diff(pscb2.cdf(mrange))
    ps = ps_pars['f']*pscb1.pdf(invx)/pscb1n + (1-ps_pars['f'])*pscb2.pdf(x)/pscb2n
    ps = pscb2.pdf(x)/pscb2n
    if 'ps' in comps: tot += nps*ps

  return tot

# =========================================================
# bookkeping
YEAR = opts.year
MODE = opts.mode
OUTPUT_NAMESPACE = opts.output
#ROOT_PATH = f"/r01/lhcb/Bc2{MODE}X/pipeline/nominal/DATA/fit/{MODE}/{YEAR}/Bc2D0MuNuX_"
ROOT_PATH = "files/Bc2D0MuNuX_"
INPUT_NAMESPACE = opts.input
# bounds on D0_M
D0_M_LOW = 1810; D0_M_HIGH=1920; # /v4/ data tuples have D0 mass - 60 MeV
print(c(f"\nFit /v4/ data: D0_M range restricted to [{D0_M_LOW} MeV, {D0_M_HIGH} MeV]").bold)
mrange = (D0_M_LOW, D0_M_HIGH)

if opts.test is not True :
  #pkl_PATH=f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/{MODE}/{YEAR}/pkl"
  #plots_PATH=f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/sWeights/{MODE}/{YEAR}/plots"
  pkl_PATH = 'pkl'
  plots_PATH = 'plots'
  os.system(f"mkdir -p {pkl_PATH}")
  os.system(f"mkdir -p {plots_PATH}")
if opts.test is True :
  print(c("\nWARNING: running in TEST mode").yellow)
  pkl_PATH=f"/usera/delaney/private/Bc2D0MuNuX/Fitter/sweights/pkl"
  plots_PATH=f"/usera/delaney/private/Bc2D0MuNuX/Fitter/sweights/plots"
  os.system(f"mkdir -p {pkl_PATH}")
  os.system(f"mkdir -p {plots_PATH}")

IN_BRANCHES = ["D0_M", "B_plus_MCORR", "B_plus_FIT_LTIME"] # save only D0_M and fit vars

PLOT_BINS = 50 # how many datapoints in plots
# =========================================================

# read in a ROOT file; by default will read all branches
print(c(f"Reading input file into pandas dataFrame {ROOT_PATH}{INPUT_NAMESPACE}.root"))
tree = uproot.open(f"{ROOT_PATH}{INPUT_NAMESPACE}.root:DecayTree")

# if signal, place a tighter cut on B_plus_visM
if MODE=="D0MuNu":
  mass_cuts = f"((D0_M>={D0_M_LOW}) & (D0_M<={D0_M_HIGH})) & (B_plus_M>3500)"
if MODE!="D0MuNu":
  mass_cuts = f"((D0_M>={D0_M_LOW}) & (D0_M<={D0_M_HIGH}))"

print(c(f"\nSign combination sanity check:").underline, f"\nDCS : {opts.dcs} \nCF : {opts.cf}")
if opts.dcs is True:
  if INPUT_NAMESPACE=="misid_hadron_enriched":
    df = tree.arrays(IN_BRANCHES+["misid_w", "K_minus_ID", "Mu_plus_ID"], cut=f"(K_minus_ID*Mu_plus_ID>0) & {mass_cuts}", library='pd')
    assert((df["K_minus_ID"]*df["Mu_plus_ID"]).all()>0)
  else:
    df = tree.arrays(IN_BRANCHES+["K_minus_ID", "Mu_plus_ID", "B_plus_M"], cut=f"(K_minus_ID*Mu_plus_ID>0) & {mass_cuts}", library='pd') # can afford to save extra branches for debugging purposes
    assert((df["K_minus_ID"]*df["Mu_plus_ID"]).all()>0)
  print(c(f"Ntuple read in with DCS sign requirement with mass cuts: {mass_cuts}").blue)
if opts.cf is True:
  df = tree.arrays(IN_BRANCHES, cut=f"(K_minus_ID*Mu_plus_ID<0) & {mass_cuts}", library='pd')
  print(c(f"Ntuple read in with CF sign requirement with mass cuts: {mass_cuts}").magenta)
  #assert((df["K_minus_ID"]*df["Mu_plus_ID"]).all()<0)
if opts.dcs is not True and opts.cf is not True:
  df = tree.arrays(IN_BRANCHES+["GBR_w"], cut=f"{mass_cuts}", library='pd')
  print(c(f"Ntuple read in without any kaon-muon sign requirement with mass cuts: {mass_cuts}").green)

data = df['D0_M'].to_numpy()
print(c(f"Number of events read in: {len(data)}\n").yellow)
print(c(f"Branches: {df.keys()}").yellow)

ps_pars = read_pars('pspars.log')
ks_pars = read_pars('kspars.log')
mc_pars = read_pars('mcsigpars.log')

#data = data[ (data>=mrange[0]) & (data<=mrange[1]) ]
mbins = 400

nh, xe = np.histogram(data, bins=mbins, range=mrange)
cx = 0.5*(xe[1:]+xe[:-1])


mi = Minuit( ExtendedBinnedNLL(nh, xe, cdf  ),
             ns = 0.2*len(data),
             nb = 0.8*len(data),
             nks = 0.01*len(data),
             nps = 0.01*len(data),
             mu = 1870,
             sg = 10,
             f  = 0.5,
             a1 = mc_pars['a1'],
             a2 = mc_pars['a2'],
             n1 = mc_pars['n1'],
             n2 = mc_pars['n2'],
             ksmu = ks_pars['mu'],
             kssg = ps_pars['sg'],
             psmu = ps_pars['mu'],
             pssg = ps_pars['sg'],
             lb = 400 )

mi.limits['ns'] = (1e-10, len(data))
mi.limits['nb'] = (1e-10, len(data))
mi.limits['nks'] = (1e-10, 0.2*len(data))
mi.limits['nps'] = (1e-10, 0.2*len(data))
#mi.values['nps'] = 0.05*len(data)
#mi.fixed['nps'] = True
mi.limits['mu'] = (1830,1890)
mi.limits['sg'] = (0,20)
mi.limits['lb'] = (0,1000)
#mi.limits['n1'] = (2.5,3.5)
#mi.limits['n2'] = (2.5,3.5)
mi.fixed['n1'] = True
mi.fixed['n2'] = True
mi.limits['a1'] = (0,10)
mi.limits['a2'] = (0,10)
mi.fixed['a1'] = True
mi.fixed['a2'] = True
mi.limits['f']  = (0,1)
mi.limits['ksmu'] = (1700,1840)
mi.limits['psmu'] = (1900,1960)
mi.limits['kssg'] = (0,20)
mi.limits['pssg'] = (0,20)
mi.fixed['psmu'] = True
mi.fixed['pssg'] = True
mi.fixed['ksmu'] = True
mi.fixed['kssg'] = True
if not opts.dcs:
  mi.values['nks'] = 0
  mi.values['nps'] = 0
  mi.fixed['nks'] = True
  mi.fixed['nps'] = True

print(c('Running fit').yellow)
mi.migrad()
mi.hesse()

# sanity checks
assert(mi.fmin.is_valid)
assert(mi.fmin.has_covariance)
assert(mi.fmin.has_posdef_covar)
assert(mi.fmin.has_accurate_covar)
print(c("Success: fit converged and sanity checks passed").green)
print(mi)

# plot the fit
fig, ax = plt.subplots(2,1,figsize=(9,8), sharex=True, gridspec_kw={'height_ratios': [5, 1]})
pnh, pxe = np.histogram(data, bins=PLOT_BINS, range=mrange)
pcx = 0.5*(pxe[1:]+pxe[:-1])

# plot data histogram with 50 bins
if opts.label is not None: label = f"{opts.label} {YEAR} Data"
if opts.label is None: label = f"LHCb {YEAR} Data"
ax[0].errorbar(
  x=pcx,
  y=pnh,
  xerr=0.5 * (pxe[1:]-pxe[:-1]),
  yerr=pnh**0.5,
  fmt='.',
  color="black",
  markersize=5,
  elinewidth=1.1,
  capsize=1,
  capthick=1,
  label=label )

# plot fitted pdf every 400 points
x = np.linspace(*mrange,400)
N = (mrange[1]-mrange[0])/PLOT_BINS

bkg = N*pdf(x,*mi.values,comps=['bkg'])
ax[0].fill_between(x, 0, bkg, alpha=0.7, facecolor='darkorange', label='Combinatorial')
if opts.dcs:
  ks  = N*pdf(x,*mi.values,comps=['bkg','ks'])
  ax[0].fill_between(x, bkg, ks, alpha=0.7, facecolor='forestgreen', label='$K$ swap')
  ps  = N*pdf(x,*mi.values,comps=['bkg','ks','ps'])
  ax[0].fill_between(x, ks, ps, alpha=0.7, facecolor='purple', label='$\pi$ swap')
ax[0].plot(x, N*pdf(x,*mi.values, comps=["sig"]), color='firebrick', lw=2, ls="--", label=r"Signal $D^0\rightarrow K^+ \pi^-$" )
ax[0].plot(x, N*pdf(x,*mi.values), 'tab:blue', ls="-", lw=2, label='Total PDF' )
ax[0].set_ylim(bottom=0)
# make the pull
pull = (pnh-N*pdf(pcx,*mi.values))/(pnh**0.5)

# pull box
ax[1].set_ylim(bottom=-5.5, top=5.5)
ax[1].axhline(y=5, color="red", ls="--", lw=1)
ax[1].axhline(y=-5, color="red", ls="--", lw=1)
ax[1].axhspan(-3, 3, alpha=0.1, facecolor="green")
ax[1].axhline(y=0, color="green", ls="--", lw=1, alpha=1.)

# plot pull
ax[1].errorbar(
  x=pcx,
  y=pull,
  xerr=0.5 * (pxe[1:]-pxe[:-1]),
  yerr=np.ones_like(pcx),
   fmt='.',
  color="black",
  markersize=5,
  elinewidth=1.1,
  capsize=1,
  capthick=1
)

# labels
ax[1].set_xlabel(r"$m(K \pi)$ [MeV$/c^2$]", loc="center")
ax[0].set_ylabel(rf"Events / {{{(mrange[1]-mrange[0])/PLOT_BINS}}} MeV$/c^2$")
ax[1].set_ylabel(r'Pulls [$\sigma$]', loc="center")
ax[0].legend(fontsize=15)

fig.tight_layout()

fig.savefig(f"{plots_PATH}/{opts.output}.pdf")
fig.savefig(f"{plots_PATH}/{opts.output}.png")

# now compute the sweights
spdf = lambda x: pdf(x,*mi.values,comps=['sig'])
bpdf = lambda x: pdf(x,*mi.values,comps=['bkg','ps','ks'])

# print computing sweights
sweighter = SWeight(data, [spdf,bpdf], [mi.values['ns'],mi.values['nb']+mi.values['nks']+mi.values['nps']], (mrange,), compnames=('sig','bkg'), checks=True)

sws = sweighter.getWeight(0,data)
bws = sweighter.getWeight(1,data)

print(len(sws), len(data))

df['sw'] = sws
df['bw'] = bws

# write to pkl file
df.to_pickle(f"{pkl_PATH}/{opts.output}.pkl")
print(c(f"Output pkl file written with stored sWeights: {pkl_PATH}/{opts.output}.pkl").green)

# plot sWeights check
fig, ax = plt.subplots()
swp = sweighter.getWeight(0,x)
bwp = sweighter.getWeight(1,x)
ax.plot(x, swp, 'b--', label='Signal')
ax.plot(x, bwp, 'r:' , label='Background')
ax.plot(x, swp+bwp, 'k-', label='Sum')
if opts.label is not None: ax.set_title(opts.label)
ax.set_xlabel('$m(D^0)$ [MeV]')
ax.set_ylabel('Weight')
ax.legend()
fig.tight_layout()

# save figs
fig.savefig(f"{plots_PATH}/{opts.output}_sweights.pdf")
fig.savefig(f"{plots_PATH}/{opts.output}_sweights.png")

## now the output
#print('Writing weights out to file')
#import ROOT as r
#import array as array
#tf = r.TFile(opts.input)
#tr = tf.Get('DecayTree')
#outf = r.TFile(opts.output,'recreate')
#outr = tr.CloneTree()
#sw = array.array('f',[0])
#sbr = outr.Branch( 'sw', sw, 'sw/F' )
#for ev in range(tr.GetEntries()):
  #if (ev%5000==0): print('\t', ev, '/', tr.GetEntries())
  #tr.GetEntry(ev)
  #sw[0] = sweighter.getWeight(0, outr.D0_M)
  #sbr.Fill()

#outr.Write()
#tf.Close()
#outf.Close()
