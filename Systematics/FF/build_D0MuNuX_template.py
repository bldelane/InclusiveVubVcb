"""Build the inclusive Bc->D(*)MuNu template based on FF model
and ratio BF(Bc->D0MuNu)/BF(Bc->D*MuNu), RBF3ptSR

__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
"""

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX")
from pyhf_Fit.executables.common.header import *
from Systematics.build_schema_D0MuNuX import build_write_schema, calc_eff, load_binning, calc_RBF_3ptSR, load_effs, load_lumis, \
    load_effs, load_lumis, calc_eff_y
from Systematics.build_schema_D0MuNuX import calc_eff as calc_incl_eff
import mplhep 
from hist import Hist
import boost_histogram as BH
import matplotlib.pyplot as plt
import pickle
from pathlib import Path
from termcolor2 import c
import sys, os
import math

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif": ["Palatino"],
})

# in several instances, we want to make sure that the mass cuts have been applied
mass_cuts = {
    "min_ltime" : 0.000, 
    "max_ltime" : 0.005, 
    "min_mcorr" : 4200,
    "max_mcorr" : 7200, 
    "low_visM_cut" : 3500, 
    "D0_M_LOW" : 1810,
    "D0_M_HIGH" : 1920,
    "mcorrerr_ltime_cut" : 0.00001
}

def check_mass_cuts(template:"dataframe for fit component",
        min_ltime, 
        max_ltime, 
        min_mcorr,
        max_mcorr, 
        low_visM_cut, 
        D0_M_LOW,
        D0_M_HIGH,
        mcorrerr_ltime_cut,
    )->"sanity check":
    assert(min(template["B_plus_M"])>low_visM_cut)
    assert(min(template["B_plus_FIT_LTIME"])>min_ltime)
    assert(max(template["B_plus_FIT_LTIME"])<=max_ltime)
    assert(min(template["D0_M"])>= D0_M_LOW)
    assert(max(template["D0_M"])<= D0_M_HIGH)
    assert(min(template["B_plus_MCORR"])>=min_mcorr)
    assert(max(template["B_plus_MCORR"])<= max_mcorr)
    assert(max(template["B_plus_FIT_LTIME"]/template["B_plus_MCORRERR"])<mcorrerr_ltime_cut)

def mass_filter_mc(channel:"sim dataframe", _name:"name df, channel", 
    min_ltime, 
    max_ltime, 
    min_mcorr,
    max_mcorr, 
    low_visM_cut, 
    D0_M_LOW,
    D0_M_HIGH,
    mcorrerr_ltime_cut,
    MCPATH, year, ch, FF, 
    )->"return MC df with mass cuts applied (sanity check)":
    """filter the MC with mass cuts"""
    if channel not in ("D0MuNu", "D0pi0MuNu", "D0gMuNu"):
        raise ValueError("Channel not recongnised; choose from [D0MuNu, D0gMuNu, D0pi0MuNu]")
    
    # sanity check: make sure all mass cuts in place"""
    mc_sel = f"B_plus_M>{low_visM_cut} && (D0_M>={D0_M_LOW} && D0_M<={D0_M_HIGH}) \
            && (B_plus_FIT_LTIME>{min_ltime} && B_plus_FIT_LTIME<={max_ltime}) \
            && (B_plus_FIT_LTIME/B_plus_MCORRERR)<{mcorrerr_ltime_cut} \
            && (B_plus_MCORR>={min_mcorr} && B_plus_MCORR<={max_mcorr})",
    
    # apply cuts to the RDF
    _df = pandas.DataFrame(RDF("DecayTree", f"{MCPATH}/{ch}/{year}/Bc2{channel}{FF}.root").Filter(mc_sel).AsNumpy())
    _df.name = _name
    
    return _df

def build_Dst_template(D0pi0_MC:"D0pi0 dataframe", D0g_MC:"D0g dataframe")->"D* df from BF-weighted sum of events in MC":
    """build the D* template based on ratio of BF from PDG;
    this assumes that the D0gMuNu and D0pi0MuNu modes have ~same reco efficiency""" 
    BF_Dst_toD0pi0 = ufloat(0.647, 0.009) # PDG
    BF_Dst_toD0g   = ufloat(0.353, 0.009) # PDG
    # what fraction of the D0pi0 does the D0g mode amount to?
    Dzg_frac2Dzpi0 = (BF_Dst_toD0g/BF_Dst_toD0pi0).n # 1 Dzg every 2 Dzpi0
    
    # sanity check to verify that the D0g has enough event in template
    try:
        assert(len(D0pi0_MC) * Dzg_frac2Dzpi0 <= len(D0g_MC)) 
    except: 
        D0pi0_MC = D0pi0_MC.sample(frac=1)[:math.floor(len(D0g_MC)/Dzg_frac2Dzpi0)]
    assert(len(D0pi0_MC) * Dzg_frac2Dzpi0 <= len(D0g_MC))
    
    sampled_D0g_MC = D0g_MC.sample(frac=1)[:int(len(D0pi0_MC)*Dzg_frac2Dzpi0)] # shuffle and pick the number of events, expressed as a fraction of the larget component
    assert(int(len(D0pi0_MC) * Dzg_frac2Dzpi0) == len(sampled_D0g_MC))

    # => Now the D* is just the number of events in pi0 mode + a fraction of these for the D0g fixed from the ratio of BFs
    Dst_MC = pandas.concat([sampled_D0g_MC, D0pi0_MC])
    Dst_MC.name="Dst_MC"
    assert(len(Dst_MC) == len(D0pi0_MC) + len(sampled_D0g_MC))

    # study how many events are loaded in the Dst samples
    print(f"\nWe have loaded in the D(st) samples passing the full selection")
    print(f"-   Dst->D0pi0 has {len(D0pi0_MC)} events")
    print(f"-   Dst->D0g has {len(sampled_D0g_MC)} events")
    print(c(f"Ratio of n_events of D0g/D0pi0 = {len(sampled_D0g_MC)/len(D0pi0_MC)}, expect ~{35.3/64.7} [source: PDG]\n").bold)
    assert(np.abs( (len(sampled_D0g_MC)/len(D0pi0_MC)) - Dzg_frac2Dzpi0) < 0.001) # the ratio of events should be consistent with BF ratio within a tolerance
    
    return Dst_MC

def build_incl_df(tot_eff_dict, y, D0_MC, Dst_MC, FF="", **kwargs)->"inclusive D(*) df":
    """construct the inclusive template"""
    # magpol-averaged per-year effcy
    eff_D0gMuNu   = calc_eff_y(eff_dict=tot_eff_dict, year=y, channel=f"Bc2D0gMuNu{FF}")
    eff_D0pi0MuNu = calc_eff_y(eff_dict=tot_eff_dict, year=y, channel=f"Bc2D0pi0MuNu{FF}")
    eff_D0MuNu    = calc_eff_y(eff_dict=tot_eff_dict, year=y, channel=f"Bc2D0MuNu{FF}")

    # D* BF from PDG
    Dst_toD0g_BF = ufloat(0.353, 0.009)
    Dst_toD0pi0_BF = ufloat(0.647, 0.009)

    # compute weighted average of the D* effienciency
    eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)
    # compute the D0/D* BF ratio from theory
    RBF_3PTSR = calc_RBF_3ptSR(**kwargs)        

    # N_D0/NDst = [BF(D0)*eff(D0)] / [BF(D*)*eff(D*)]
    eff_ratio = eff_D0MuNu / eff_DstMuNu
    sig_rel_frac = RBF_3PTSR*eff_ratio # D0/D*; fraction of D0 relative to Dst
    sampled_D0 = D0_MC.sample(frac=1.)[:int(len(Dst_MC)*sig_rel_frac.n)]
    signal_df = pandas.concat([sampled_D0, Dst_MC])
    signal_df.name = "incl_sig"
    assert(np.abs( (len(sampled_D0)/len(Dst_MC))-sig_rel_frac)<0.001) # consistency check within a tolerance threshold
    
    print(c("Success: inclusive dataframe built:").underline)
    print(f"D0 efficiency: {eff_D0MuNu}")
    print(f"D* efficiency: {eff_DstMuNu}")
    print(f"=> eff((D0)/eff(D*): {eff_ratio}")
    print(f"D0/D* RBF: {RBF_3PTSR}")

    return signal_df

def gen_mc_template(indf:"datadrame from which to extract template", fit_config:"binning")->"boost histogram":
    """generate template in the form of a boost histogram"""
    if indf.name!="incl_sig":
        raise ValueError("Realistically, we want to write to file the inclusive Bc->D(*)MuNu template. \
            Error in df supplied.")
    n_hists = len(fit_config.keys()) # how many fit variables?
    fitvars = list(fit_config.keys()) # which fit variables?

    _nbins_1  = fit_config[fitvars[0]]["nbins"]
    _minvar_1 = fit_config[fitvars[0]]["range"][0] 
    _maxvar_1 = fit_config[fitvars[0]]["range"][-1] 

    # for MC require regular storage
    if fit_config[fitvars[0]]["is_var_axis"]=="False":
        wbh = bh.Histogram( 
        bh.axis.Regular(_nbins_1, _minvar_1, _maxvar_1 )
        )
    if fit_config[fitvars[0]]["is_var_axis"]=="True":
        wbh = bh.Histogram(
        bh.axis.Variable(fit_config[fitvars[0]]["range"]) #custom binning 
    )        
    wbh.fill( indf[fitvars[0]] )        
    print(c(f"Filling mode {indf.name} with {len(indf)} events").bold)
    print(c(f"Histogram summary:\n {wbh}"))

    return wbh


def build_inclusive_sig_template(raw_args=None):
    parser = ArgumentParser(
        description="Parameters to generate inclusive sig template subject to FF and RBF3ptSR prediction"
    )
    parser.add_argument('-M','--MCpath',       default=f"/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/MC/fit", help="Location of simulations passing full event selection")
    parser.add_argument('-p','--plotpath',     default=f"/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/plots")
    parser.add_argument('-S','--schemapath',   default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/scratch", help="where to store the schema?")
    parser.add_argument('-r','--results',      default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/results", help="where to store the fit results?")
    parser.add_argument('-c','--confpath',     default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json")
    parser.add_argument('-x','--debug',        action='store_true', help="stdout to terminal")
    parser.add_argument('-q','--SR',           default="cval", help="central value or 1σ bounds", choices=["cval", "sup", "inf"]) 
    parser.add_argument('-F','--FF',           default="", help="FF model", choices=["", "_Kis", "_ISGW2"]) 
    parser.add_argument('-C','--channel',      default="D0MuNu", help="Signal or normalisation channel", choices=["D0MuNu", "JpsiMuNu"]) 
    parser.add_argument('-y','--year',         required=True, help="Year of data taking", choices=["2011", "2012", "2015", "2016", "2017", "2018"]) 
    parser.add_argument('-Y','--yeartuple',    default=("2016","2017","2018"), help="cycle through years to extract effs", nargs='+')
    parser.add_argument('-e','--effpath',      default="/usera/delaney/private/Bc2D0MuNuX/Efficiencies/jsons/Total.json")
    parser.add_argument('-L','--lumipath',     default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Lumi.json")
    parser.add_argument('-P','--pkl',          default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/pkl")
    parser.add_argument('-E','--effout',       default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/effs")
    opts = parser.parse_args(raw_args)

    # load binning scheme
    fit_config = load_binning(opts.confpath)

    # signal dfs after filtering with mass cuts
    y_ff_ch_pars = {"MCPATH" : opts.MCpath, "year" : opts.year, "ch": opts.channel, "FF" : opts.FF} 
    D0MuNu    = mass_filter_mc(channel="D0MuNu",    _name="D0_MC",    **y_ff_ch_pars, **mass_cuts)
    D0pi0MuNu = mass_filter_mc(channel="D0pi0MuNu", _name="D0pi0_MC", **y_ff_ch_pars, **mass_cuts)
    D0gMuNu   = mass_filter_mc(channel="D0gMuNu",   _name="D0g_MC",   **y_ff_ch_pars, **mass_cuts)

    # build the D* template based on ratio of BF from PDG 
    DstMuNu = build_Dst_template(D0pi0_MC=D0pi0MuNu, D0g_MC=D0gMuNu)
    
    # sanity checks for df from which construct the templates
    for _template in (DstMuNu, D0MuNu): check_mass_cuts(template=_template, **mass_cuts)

    # read in the json file with total efficiency
    tot_eff_dict = load_effs(opts.effpath)

    # construct inclusive D(*) df 
    DDstMuNu = build_incl_df(tot_eff_dict=tot_eff_dict, y=opts.year, D0_MC=D0MuNu, Dst_MC=DstMuNu, FF=opts.FF, _val=opts.SR)

    # write to pkl, matching the file structure required by the main schema builder code
    Path(f"{opts.pkl}/3pstSR_{opts.SR}_FF{opts.FF}/{opts.year}").mkdir(parents=True, exist_ok=True)
    with open(f"{opts.pkl}/3pstSR_{opts.SR}_FF{opts.FF}/{opts.year}/{DDstMuNu.name}.pkl", "wb") as bh_fout:
        pickle.dump(gen_mc_template(DDstMuNu, fit_config), bh_fout)
    print(c(f"SUCCESS: boost objects written to file: {opts.pkl}/3pstSR_{opts.SR}_FF{opts.FF}/{opts.year}/{DDstMuNu.name}.pkl").green)  

    # write to file the inclusive D(*) efficiency 
    DDst_eff=calc_incl_eff(tot_eff_dict=tot_eff_dict, FF=opts.FF, year_tuple=opts.yeartuple, _val=opts.SR)[opts.year]
    Path(f"{opts.effout}").mkdir(parents=True, exist_ok=True)
    with open(f"{opts.effout}/DDst_{opts.year}_3ptSR_{opts.SR}_FF{opts.FF}.log", "w") as f_eff_out:
        print(DDst_eff, file=f_eff_out)

if __name__ == '__main__':
    build_inclusive_sig_template()
