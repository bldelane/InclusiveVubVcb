"""Evaluate the impact on signal yield and efficiency from 3ptSR BF ratio (D0/D*) prediction

__author__: Blaise Delaney
__email__ : blaise.delaney 
"""
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX")
from pyhf_Fit.executables.common.header import *
from build_D0MuNuX_template import build_inclusive_sig_template
from Systematics.build_schema_D0MuNuX import build_write_schema, load_effs
from Systematics.EventMixing.calc_yield_syst import fit, build_pyhf_spec, build_validate_workspace
from Systematics.build_schema_D0MuNuX import calc_eff as calc_incl_eff
import mplhep 
from hist import Hist
import boost_histogram as BH
import matplotlib.pyplot as plt
import pickle
from pathlib import Path
from termcolor2 import c
import sys, os

parser = ArgumentParser(
    description="Parameters to generate inclusive sig template subject to FF and RBF3ptSR prediction"
)
parser.add_argument('-p','--plotpath',     default=f"/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/plots")
parser.add_argument('-z','--year',         default="2018", nargs='+', help="year for which alternative FF templates are available")
parser.add_argument('-l','--logs',         default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/logs",    help="where to store stdout?")
parser.add_argument('-S','--schemapath',   default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/scratch", help="where to store the schema?")
parser.add_argument('-r','--results',      default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/results", help="where to store the fit results?")
parser.add_argument('-c','--confpath',     default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json")
parser.add_argument('-q','--SR',           default="cval", help="central value or 1σ bounds", choices=["cval", "sup", "inf"]) 
parser.add_argument('-C','--channel',      default="D0MuNu", help="Signal or normalisation channel", choices=["D0MuNu", "JpsiMuNu"]) 
parser.add_argument('-e','--effpath',      default="/usera/delaney/private/Bc2D0MuNuX/Efficiencies/jsons/Total.json")
parser.add_argument('-L','--lumipath',     default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Lumi.json")
parser.add_argument('-P','--pkl',          default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/pkl")
parser.add_argument('-E','--effout',       default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/effs")
parser.add_argument('-w','--prop',         default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/prop")
parser.add_argument('-x','--debug',        action='store_true', help="stdout to terminal")
opts = parser.parse_args()

# as I build the schemas, I redirect the output to /logs/; after that is done, it's probably best to restore the original stdout stream
original_stdout = sys.stdout

def build_validate_workspace_y(alt="alt1", year=opts.year, schemapath=opts.schemapath)->"pyhf schema":
    """validation of spec (model and observation) for fits to one year only (FF syst)"""
    SCHEMA = f"{schemapath}/model_spec_D0MuNu_{alt}_{year}.json"

    # validate the syntax to interface with pyhf
    workspace = json.load(open(SCHEMA))
    schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
    jsonschema.validate(instance=workspace, schema=schema)

    workspace = pyhf.Workspace(workspace)

    return workspace


if __name__ == '__main__':
    # generate the inclusive signal templates and extract the efficiencies
    Path(f"{opts.logs}").mkdir(parents=True, exist_ok=True)
    for y in (opts.year,):
        for ff in ("","_Kis","_ISGW2"):
            print(c(f"Running with FF: {ff}").white.on_blue)
            with open(f"{opts.logs}/DDst_{y}_3ptSR_{opts.SR}_FF{ff}.log", 'w') as _fout:
                if not opts.debug: sys.stdout = _fout
                build_inclusive_sig_template(["-y", y, "--FF", ff, "-Y", opts.year])
                _fout.close() 
    # restore the stdout to terminal
    sys.stdout = original_stdout
    print(c("Success: inclusive signal templates and efficiencies written to file.").green)

    # write per-year schemas
    for y in (opts.year,):
        for ff in ("",):#,"_Kis","_ISGW2"):
            print(c(f"Running with FF: {ff}").white.on_blue)
            build_write_schema(["-y", y, "--FF", ff, 
            "--yeartuple", opts.year,
            "--simpath", f"/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/pkl/3pstSR_{opts.SR}_FF{ff}", 
            "--output", f"{opts.schemapath}/3ptSR_{opts.SR}_FF{ff}", "--debug"])

    # book dict that want to propagate
    eff_yield_results = {
            "nominal":{
                f"{opts.year}" : {},
            },
            "_Kis": {
                f"{opts.year}" : {},
            },
            "_ISGW2": {
                f"{opts.year}" : {},
            }
        }
    
    # assemble workspace for each RBF3ptSR value and fit
    Path(f"{opts.results}").mkdir(parents=True, exist_ok=True)
    fig, ax0 = plt.subplots(figsize=(20, 6))
    for ff in ("","_Kis", "_ISGW2"):
        print(c(f"Fit result with nominal mixing template and RBF3ptSR {opts.SR} FF {ff}:").underline)
        # alt1: nominal polynomial correction to event mixing
        workspace = build_validate_workspace_y(alt="alt1", schemapath=f"{opts.schemapath}/3ptSR_{opts.SR}_FF{ff}")
        incl_sig_y = fit(workspace=workspace, alt="alt1", resultspath=opts.results, ax0=ax0)
        incl_eff = calc_incl_eff(tot_eff_dict=load_effs(opts.effpath), FF=ff, year_tuple=(opts.year,), _val=opts.SR)
        if ff=="":
            eff_yield_results[f"nominal"][f"{opts.year}"]["yield"] = incl_sig_y
            eff_yield_results[f"nominal"][f"{opts.year}"]["eff"]   = incl_eff
        else:
            eff_yield_results[f"{ff}"][f"{opts.year}"]["yield"] = incl_sig_y
            eff_yield_results[f"{ff}"][f"{opts.year}"]["eff"]   = incl_eff
    # savefig

    # propagate info for syst evaluation
    Path(f"{opts.prop}").mkdir(parents=True, exist_ok=True)
    with open(f"{opts.prop}/to_syst_FF.pkl", "wb") as fsyst: pickle.dump(eff_yield_results, fsyst)
