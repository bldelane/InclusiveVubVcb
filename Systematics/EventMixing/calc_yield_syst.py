"""Run through the alternative event mixing templates and compute systematic"""
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX")
from pyhf_Fit.executables.common.header import *
from Systematics.build_schema_D0MuNuX import build_write_schema
import mplhep 
from hist import Hist
import boost_histogram as BH
import matplotlib.pyplot as plt
import pickle
from pathlib import Path
from termcolor2 import c
import sys, os

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif": ["Palatino"],
})

parser = ArgumentParser()
parser.add_argument('-s','--systpath',     default="/r01/lhcb/Bc2D0MuNuX/systematics/event-mixing")
parser.add_argument('-n','--nompath',      default=f"/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu")
parser.add_argument('-p','--plotpath',     default=f"/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/plots")
parser.add_argument('-S','--schemapath',   default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/scratch", help="where to store the schema?")
parser.add_argument('-r','--results',      default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/results", help="where to store the fit results?")
parser.add_argument('-x','--debug',        action='store_true', help="stdout to terminal")
opts = parser.parse_args()

# as I build the schemas, I redirect the output to /logs/; after that is done, it's probably best to restore the original stdout stream
original_stdout = sys.stdout

# cosmetic for plots comparing templates
stylemap = {}
stylemap["alt1"] = {"color":"lightgrey", "label":"Nominal", "alpha":0.75, "histtype":"fill"}
stylemap["alt2"] = {"color":"tab:blue", "label":r"Polynomial $2^{nd}$ order", "lw":1}
stylemap["alt3"] = {"color":"tab:red","label":r"Polynomial $3^{rd}$ order", "lw":1}

def load_bh(indf)->"boost_histogram":
    """"""
    wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing        
    samples = [np.array(indf["B_plus_MCORR"])]
    ax_1 = BH.axis.Regular(30, 4_200, 7_200, underflow=False, overflow=False )
    wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
    wbh.fill( *samples, weight=wts )    
    
    return wbh

def plot_templs(y)->"template comparison plots":
    """plot the different templates"""
    fig, ax = plt.subplots(figsize=(6, 4))
    for t in ["alt1", "alt2", "alt3"]:
        with open(f"{opts.systpath}/{t}_{y}.pkl", "rb") as f:    
            h = Hist(pickle.load(f)) # prefer to work with hist as opposed to boost_histogram
            mplhep.histplot(h.project(0), ax=ax, **stylemap[t])
    plt.ylabel(r"Candidates / 100 MeV/$c^2$", loc="center")
    plt.xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]", loc="center")
    plt.title(f"LHCb Unofficial {y}")
    plt.legend(frameon=False)
    plt.savefig(f"{opts.plotpath}/alts_{y}.png")
    plt.savefig(f"{opts.plotpath}/alts_{y}.pdf")
    
def build_pyhf_spec(alt, schemapath)->"pyhf schema":
    """fit each time with an alternative mixing template"""
    schema_18 = f"{schemapath}/model_spec_D0MuNu_{alt}_2018.json"
    schema_17 = f"{schemapath}/model_spec_D0MuNu_{alt}_2017.json"
    schema_16 = f"{schemapath}/model_spec_D0MuNu_{alt}_2016.json"

    # validate the syntax to interface with pyhf
    for SCHEMA in [schema_16, schema_17, schema_18]:
        workspace = json.load(open(SCHEMA))
        schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
        jsonschema.validate(instance=workspace, schema=schema)

    sch_18 = json.load(open(schema_18))
    sch_17 = json.load(open(schema_17))
    sch_16 = json.load(open(schema_16))

    # construct the spec
    spec = {
    "channels": [
        sch_18["channels"][0],
        sch_17["channels"][0],
        sch_16["channels"][0]
    ],
    "observations":[
        sch_18["observations"][0],
        sch_17["observations"][0],
        sch_16["observations"][0],
    ],
    "measurements": [
        { 
            "name": f"sig_y_extraction",
            "config": {
                "poi": "incl_sig_y",
                "parameters": sch_16["measurements"][0]["config"]["parameters"][1:]+sch_17["measurements"][0]["config"]["parameters"][1:]+sch_18["measurements"][0]["config"]["parameters"]
            }
        }
    ],
    "version": "1.0.0"
    }
    return spec

def build_validate_workspace(spec, alt, schemapath)->"pyhf workspace":
    """Build and validate the model and observation"""
    spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
    
    # write for preservation
    Path(f"{schemapath}/combined").mkdir(parents=True, exist_ok=True)
    SCHEMA = f"{schemapath}/combined/{alt}.json"
    if os.path.exists(SCHEMA) : os.remove(SCHEMA) # remove previous fit schema
    with open(SCHEMA,'w') as outfile: outfile.write(spec)

    # validate workspace
    workspace = json.load(open(SCHEMA))
    schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
    # If no exception is raised by validate(), the instance is valid.
    jsonschema.validate(instance=workspace, schema=schema)
    workspace = pyhf.Workspace(workspace)

    return workspace

def plot_fitted_model(workspace, pdf, alt, bestfit_pars, ax0)->"fig":
    """ very raw plot comparing fit results"""
    proj_fit = pdf.expected_data(bestfit_pars, include_auxdata=False)
    ax0.step(
        x = np.arange(len(proj_fit)),
        y = proj_fit,
        where="post",
        lw=1.5, ls="-",
        label=alt,
        alpha=.7
    )
    ax0.errorbar(
        x = np.arange(len(proj_fit))+0.5,
        y= workspace.data(pdf)[:len(proj_fit)],
        fmt=".",
        markersize=7,
        elinewidth=2,
        capsize=2,
        markeredgewidth=2,
        color="black",
        alpha=.7
    )

    ax0.set_title(r"LHCb Unofficial $5.4$ fb$^{-1}$", fontsize=18)
    ax0.legend(fontsize=15, loc = "center right", frameon=False)
    ax0.set_ylabel(rf"Bohm\&Zech-scaled events / 100 MeV", loc="center", fontsize=18)

    ax0.axvline(x=30, color="lightgrey", ls="--", lw=1.5)
    ax0.axvline(x=60, color="lightgrey", ls="--", lw=1.5)

    ax0.text(0.27, 0.90, '2016', transform=ax0.transAxes, color="grey", size=30)
    ax0.text(0.57, 0.90, '2017', transform=ax0.transAxes, color="grey", size=30)
    ax0.text(0.91, 0.90, '2018', transform=ax0.transAxes, color="grey", size=30)

    ax0.set_xticks([], minor=False)

    ax0.set_xticklabels([])
    ax0.set_xticks(np.arange(0, 95, 5), minor=True)
    ax0.set_xticklabels([4200, 4700, 5200, 5700, 6200, 6700, 4200, 4700, 5200, 5700, 6200, 6700, 4200, 4700, 5200, 5700, 6200, 6700, 7200], minor=True)
    ax0.tick_params(axis='x', which='both', labelsize=20)
    ax0.set_xlabel(r"$m_{corr}(D^0 \mu^+)$ [MeV/$c^2$]", loc="center", fontsize=18)

def fit(workspace, alt, resultspath, **kwargs)->float:
    """run the fit"""
    pdf = workspace.model(measurement_name = "sig_y_extraction",
                        modifier_settings={
                            "normsys": {"interpcode": "code1"},
                            }
    )
    data = workspace.data(pdf)
    fit_result, result_obj = pyhf.infer.mle.fit(data, pdf, return_result_obj=True, return_uncertainties=True)

    bestfit_pars, par_uncerts = fit_result.T

    par_name_dict = {k: v["slice"].start for k, v in pdf.config.par_map.items()}
    PRESCALED_FIT_RESULTS = {}
    
    # write to dedicated file; repristinate outoput after
    fres = open(f"{resultspath}/{alt}.log", "w")
    for k, v in par_name_dict.items():
        PRESCALED_FIT_RESULTS[k] = ufloat(bestfit_pars[v], par_uncerts[v])
        print(f"Extracted value of {k} = {bestfit_pars[v]} +/- {par_uncerts[v]}", file=fres)
    fres.close()
    
    # print signal yield for each template
    if alt=="alt1":
        print(c(f"Template: {alt}; signal yield: {PRESCALED_FIT_RESULTS['incl_sig_y']:.0f}").yellow)
    if alt=="alt2":
        print(c(f"Template: {alt}; signal yield: {PRESCALED_FIT_RESULTS['incl_sig_y']:.0f}").green)
    if alt=="alt3":
        print(c(f"Template: {alt}; signal yield: {PRESCALED_FIT_RESULTS['incl_sig_y']:.0f}").blue)

    plot_fitted_model(workspace, pdf, alt, bestfit_pars, **kwargs)

    return PRESCALED_FIT_RESULTS['incl_sig_y']

def calc_syst(y_dict)->"mixing systematic taking alt1 as nominal":
    resid = {}
    for alt in ["alt2", "alt3"]:
        resid[alt] = np.abs(y_dict["alt1"].n - y_dict[alt].n) # difference in central values
    
    syst = np.max(list(resid.values()))
    return syst

if __name__ == '__main__':
    Path(f"{opts.plotpath}").mkdir(parents=True, exist_ok=True)
    alt_tmpls = {}
    for y in ["2016", "2017", "2018"]:
        plot_templs(y) # plot the nominal and alternative templates
        for t in ["alt1", "alt2", "alt3"]:
            if opts.debug: 
                build_write_schema(["-y", y, "-m", t, "--debug"])
            else:
                build_write_schema(["-y", y, "-m", t])
    # restore the stdout stream
    sys.stdout = original_stdout
    print(c("\nSuccess: all schemas written to /scratch/ and /combined/ directories, with logs in /logs/; results in /results/;").bold)

    # proceed with fits
    Path(f"{opts.results}").mkdir(parents=True, exist_ok=True)
    print(c("\nFit results:").underline)
    fig, ax0 = plt.subplots(figsize=(20, 6))

    signal_ys = {}
    for t in ["alt1", "alt2", "alt3"]:
        spec = build_pyhf_spec(t, opts.schemapath)
        workspace = build_validate_workspace(spec, t, schemapath=opts.schemapath)
        sig_y = fit(workspace, t, resultspath = opts.results, ax0=ax0)
        signal_ys[t] = sig_y

    plt.savefig(f"{opts.plotpath}/compare_fitres.png")
    plt.savefig(f"{opts.plotpath}/compare_fitres.pdf")

    # === compute systematics ===
    print(c("\n============================================================================================================"))
    print(c(f"Systematic uncertainty (maximal difference in cvals): {calc_syst(signal_ys):.0f}"))
    print(c(f"Relative to nominal result stat uncty: {calc_syst(signal_ys) / signal_ys['alt1'].s * 100.:.0f}% of stat uncty"))
    print(c(f"Assuming uncorrelated errors, result with stat+mixing syst: {signal_ys['alt1'].n:.0f} +/- {(signal_ys['alt1'].s**2 + calc_syst(signal_ys)**2)**.5:.0f}").bold)
    print(c("============================================================================================================"))
