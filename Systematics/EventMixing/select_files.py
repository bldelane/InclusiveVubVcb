import uproot
import pandas as pd
from tqdm import tqdm
import sys

year = '2016' if len(sys.argv)<2 else sys.argv[1]
path = '/r01/lhcb/Bc2D0MuNuX/pipeline/nominal/DATA/combined/D0MuNu/'
evts = None

cuts = [ #'(B_plus_FD_OWNPV<25)',
         #'(B_plus_SV_RXY<2)',
         '(CosXY_Mu_plus_D0>-0.8)',
         #'(CosXYZ_Mu_plus_Pi_1 < 0.9997)',
         #'(CosXYZ_Mu_plus_K_minus < 0.9997)',
         #'(log(D0_FDCHI2_OWNPV)<9)',
         #'(log(D0_IPCHI2_OWNPV)<7.5)',
         #'(FitVar_El<3000)',
         #'(FitVar_Mmiss2>-0.5e7)',
         #'(FitVar_Mmiss2<0.7e7)',
         #'(log(K_minus_IPCHI2_OWNPV)<9)',
         #'(log(Mu_plus_IPCHI2_OWNPV)<9)',
         #'(Mu_plus_K_minus_M<5800)',
         #'(Mu_plus_Pi_1_M<5500)',
         #'(Mu_plus_PIDp<0)',
         #'(Mu_plus_PIDK<0)',
         #'(Mu_plus_ProbNNghost<0.1)',
         '((B_plus_FIT_LTIME/B_plus_MCORRERR)<0.00001)',
         '(B_plus_MCORRERR<650)',
         #'(B_plus_FIT_LTIME>0.000)',
         #'(B_plus_FIT_LTIME<=0.005)',
         #'(D0_M>=1810)',
         #'(D0_M<=1920)'
        ]
#cuts = []

aliases = { 'B_plus_SV_RXY': 'sqrt(B_plus_SV_X**2 + B_plus_SV_Y**2)' }
aliases = None


for suff in ['sig','comb']:
    f = uproot.open(f'{path}/{year}/Bc2D0MuNuX_{suff}.root:DecayTree')
    #f.show()
    cut_string = ' & '.join(cuts)
    pars = ['B_plus_M','B_plus_MCORR','B_plus_P']
    if suff=='comb':
        pars += ['GBR_w']
        cut_string = None
    else:
        #cut_string += ' & ((K_minus_ID*Mu_plus_ID)<0)'
        cut_string = '(K_minus_ID*Mu_plus_ID)<0'
    batch_size = '50 MB'
    bevs = f.num_entries_for(batch_size, pars, cut=cut_string, aliases=aliases)
    tevs = f.num_entries
    nits = round(tevs/bevs+0.5)
    #print(bevs, tevs, nits)
    df = pd.DataFrame( columns=pars )
    for batch in tqdm(f.iterate( pars , cut=cut_string, aliases=aliases, entry_stop=evts, library='pd', step_size=batch_size ), total=nits,ascii=True,desc=f'{suff} file'):
        df = df.append(batch)
        #print(df)
    df.to_pickle(f'df_{suff}_{year}.pkl')

