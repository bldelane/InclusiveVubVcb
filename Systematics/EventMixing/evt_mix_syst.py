import pandas as pd
import matplotlib.pyplot as plt
import boost_histogram as bh
from uncertainties import unumpy
from iminuit import Minuit
from iminuit.cost import LeastSquares
import numpy as np
import os
import pickle
from argparse import ArgumentParser
from scipy.stats import chi2

parser = ArgumentParser()
parser.add_argument('-y','--year', default='2016', help='Year')
parser.add_argument('-r','--norenorm', default=False, action='store_true', help='Do not rescale output alternate templates to match nominal')
args = parser.parse_args()

def bhme(vals,weights,bins,range):
  h = bh.Histogram( bh.axis.Regular(bins,*range), storage=bh.storage.Weight() )
  h.fill( vals, weight=weights )
  sumw2 = h.sum().variance
  h /= h.sum().value
  return h

bins = 100
range = (6000,12000)

year = args.year

# these are made by select_files.py
cf = pd.read_pickle(f'files/df_sig_{year}.pkl')
em = pd.read_pickle(f'files/df_comb_{year}.pkl')

cut_str = 'B_plus_M>5500 & B_plus_M<6280'

cf = cf[ cf.eval(cut_str) ]
em = em[ em.eval(cut_str) ]


emH = bhme( em['B_plus_MCORR'], 1, bins, range )
emR = bhme( em['B_plus_MCORR'], em['GBR_w'], bins, range )
cfH = bhme( cf['B_plus_MCORR'], 1, bins, range )

fig, ax = plt.subplots()

ax.stairs(emH.values(), emH.axes[0].edges, fill=True, alpha=0.5, label='Event Mixing')
ax.stairs(cfH.values(), cfH.axes[0].edges, fill=True, alpha=0.5, label='Validation Region')
ax.stairs(emR.values(), emR.axes[0].edges, color='darkblue', label='Mixing + GBR')
ax.legend()
ax.set_xlabel(r'$m_{\rm{corr}}(D^0\mu^+)$ [MeV/$c^2$]')
fig.tight_layout()
fig.savefig(f'plots/dist_{year}.png')

cfs = unumpy.uarray( cfH.values(), cfH.variances()**0.5 )
ems = unumpy.uarray( emR.values(), emR.variances()**0.5 )

rat = cfs / ems

pol1 = lambda x, p0, p1: p0 + p1*x
pol2 = lambda x, p0, p1, p2: p0 + p1*x + p2*x**2
pol3 = lambda x, p0, p1, p2, p3: p0 + p1*x + p2*x**2 + p3*x**3

x = cfH.axes[0].centers
y = unumpy.nominal_values(rat)
yerr = unumpy.std_devs(rat)

cost1 = LeastSquares(x,y,yerr,pol1)
cost2 = LeastSquares(x,y,yerr,pol2)
cost3 = LeastSquares(x,y,yerr,pol3)

mi1 = Minuit( cost1, p0=0, p1=-1 )
mi2 = Minuit( cost2, p0=0, p1=-1, p2=0.01 )
mi3 = Minuit( cost3, p0=0, p1=-1, p2=0.01, p3=0.01 )

mi1.migrad()
mi1.hesse()
print(mi1)

mi2.migrad()
mi2.hesse()
print(mi2)

mi3.migrad()
mi3.hesse()
print(mi3)

fig, ax = plt.subplots()
ax.errorbar( x, y, yerr, fmt='ko' )

testx = np.linspace(4200,12000,400)
ax.plot( (4200,12000), (1,1), 'k--' )
ax.plot( testx, pol1(testx,*mi1.values), label='pol1' )
ax.plot( testx, pol2(testx,*mi2.values), label='pol2' )
ax.plot( testx, pol3(testx,*mi3.values), label='pol3' )
ax.set_xlabel(r'$m_{\rm{corr}}(D^0\mu^+)$ [MeV/$c^2$]')
ax.legend()
fig.tight_layout()
fig.savefig(f'plots/extrap_{year}.png')

# compute p-values
print_rows = []
for p, mi in enumerate([mi1,mi2,mi3]):
  chisq = mi.fval
  pval = 1-chi2.cdf(chisq,len(x)-p-2)
  print_rows.append( [ f'pol{p+1}', chisq, len(x), p+2, pval ] )
from tabulate import tabulate
print(tabulate(print_rows,headers=['Function','chi2','nObs','nPar','p-value','prob']))

# now apply extrapolation to templates
fit = pd.read_pickle(f'files/EvtMix_{year}.pkl')

fith = bh.Histogram( bh.axis.Regular(30,4200,7200), storage=bh.storage.Weight() )

fith.fill( fit['B_plus_MCORR'], weight=fit['sw']*fit['GBR_w'] )

vnce = fith.variances()
alt1_vs = pol1(fith.axes[0].centers, *mi1.values) * fith.values()
alt2_vs = pol2(fith.axes[0].centers, *mi2.values) * fith.values()
alt3_vs = pol3(fith.axes[0].centers, *mi3.values) * fith.values()

alt1 = fith.copy()
alt2 = fith.copy()
alt3 = fith.copy()

alt1[:] = np.stack( (alt1_vs,vnce), axis=1)
alt2[:] = np.stack( (alt2_vs,vnce), axis=1)
alt3[:] = np.stack( (alt3_vs,vnce), axis=1)

renorm = not args.norenorm

if renorm:
  alt1 *= ( fith.sum().value / alt1.sum().value )
  alt2 *= ( fith.sum().value / alt2.sum().value )
  alt3 *= ( fith.sum().value / alt3.sum().value )

fig, ax = plt.subplots()
ax.stairs( fith.values(), fith.axes[0].edges, label='Nominal Template')
ax.stairs( alt1.values(), fith.axes[0].edges, label='Systematic Alternate pol1')
ax.stairs( alt2.values(), fith.axes[0].edges, label='Systematic Alternate pol2')
ax.stairs( alt3.values(), fith.axes[0].edges, label='Systematic Alternate pol3')
ax.set_xlabel(r'$m_{\rm{corr}}(D^0\mu^+)$ [MeV/$c^2$]')
ax.legend(loc='upper left')
fig.tight_layout()
fig.savefig(f'plots/alt_{year}.png')

os.system('mkdir -p alts')
with open(f'alts/nom_{year}.pkl','wb') as f:
  pickle.dump(fith,f)
with open(f'alts/alt1_{year}.pkl','wb') as f:
  pickle.dump(alt1,f)
with open(f'alts/alt2_{year}.pkl','wb') as f:
  pickle.dump(alt2,f)
with open(f'alts/alt3_{year}.pkl','wb') as f:
  pickle.dump(alt3,f)

plt.show()

