""""Executable to generate the Vub/Vcb vs RFF band plot

__author__ : Blaise Delaney
__email__  : blaise.delaney@cern.ch"""

import sys, os
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/executables/common")
from build_schema_D0MuNuX import load_lumis, load_effs, calc_eff, calc_lumi_y, calc_eff_y, calc_RBF_3ptSR
from header import *
from termcolor2 import c
import boost_histogram as BH
import pickle
from pathlib import Path
from tabulate import tabulate
import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{amsmath}')


retrieve_sig_y = lambda blindfac, sig_y : sig_y*blindfac

def calc_lumi_weighted_eff(lumi_dict, eff_dict)->"Efficiency number the mode, lumi-weighted":
    """return the lumi-weighted sum of the efficiencies"""
    tot_lumi = np.sum(list(lumi_dict.values()))
    final_eff = 0
    for y in ("2016", "2017", "2018"):
        final_eff += lumi_dict[y]*eff_dict[y]
    final_eff /= tot_lumi # normalise

    return final_eff

def calc_FF_syst_y(_dict, _key:"eff or yield", result_cval:"central value of nominal yeild of effcy", _year="2018")->"return FF syst":
    """ compute syst following the LHCb stat guidelines---for these we have 2018 only"""
    # retrive the parameters of interest
    assert(_key in ("yield","eff"))
    if _key=="yield":
        var_nominal     = _dict["nominal"][_year][_key].n
        var_kis         = _dict["_Kis"][_year][_key].n
        var_isgw2       = _dict["_ISGW2"][_year][_key].n
    if _key=="eff":
        var_nominal     = _dict["nominal"][_year][_key][_year].n
        var_kis         = _dict["_Kis"][_year][_key][_year].n
        var_isgw2       = _dict["_ISGW2"][_year][_key][_year].n
    
    av_diff_over_sqrt2 = (np.abs(var_nominal-var_kis)+np.abs(var_nominal-var_isgw2))/(2 * (2**.5))
    frac_err = av_diff_over_sqrt2 / var_nominal
    syst = result_cval * frac_err

    return syst

def calc_RBF_syst_y(_dict, _key:"yield", result_cval:"central value of nominal yeild of effcy", _year="2018")->"return RBF syst":
    """ compute syst following the LHCb stat guidelines"""
    # retrive the parameters of interest
    assert(_key in ("yield",))
    var_nominal     = _dict["cval"][_key].n
    var_sup         = _dict["sup"][_key].n
    var_inf       = _dict["inf"][_key].n
    
    av_diff = (np.abs(var_nominal-var_sup)+np.abs(var_nominal-var_inf))/2.
    frac_err = av_diff / var_nominal
    syst = result_cval * frac_err

    return syst

def calc_RBF_syst_eff(_dict, _key:"effs", result_cval:"central value of nominal yeild of effcy", _year="2018", **kwargs)->"return RBF syst":
    """ compute syst following the LHCb stat guidelines"""
    # retrive the parameters of interest
    assert(_key in ("effs"))
    if _key=="effs":
        var_nominal     = calc_lumi_weighted_eff(eff_dict=_dict["cval"][_key], **kwargs).n
        var_sup         = calc_lumi_weighted_eff(eff_dict=_dict["sup"][_key],  **kwargs).n
        var_inf         = calc_lumi_weighted_eff(eff_dict=_dict["inf"][_key],  **kwargs).n
    
    av_diff = (np.abs(var_nominal-var_sup)+np.abs(var_nominal-var_inf))/2.
    frac_err = av_diff / var_nominal
    syst = result_cval * frac_err

    return syst

def extract_RBF(sig_y, norm_y, sig_eff, norm_eff, bfjpsi2mumu, bfdz2kpi, sig_tot_lumi, norm_tot_lumi)->float:
    """compute BF(D(*))/BF(Jpsi)"""
    RBF = (sig_y/norm_y) * (norm_eff/sig_eff) * (bfjpsi2mumu/bfdz2kpi) * (norm_tot_lumi/sig_tot_lumi)
    
    return RBF

def extract_VubVcb(RBF, RFF_Dz, RFF_Dstz)->float:
    """perform the semi-exclusive extraction of Vub/Vcb"""
    print("check inclusive width ",RFF_Dz + RFF_Dstz)
    VubVcb = (
        RBF / (RFF_Dz + RFF_Dstz) 
    )**.5

    return VubVcb

def plot_results(RBF, RFF_Dz:"Dst width", RFF_Dstz:"Dst width", _path, plot_data_points=True, _xmin=.01, _xmax=1.5)->"2d plot of VubVcb vs RFF":
    """produce the exp and theory combination for the extraction of VubVcb plot"""
    plt.figure(figsize=(13,8))
    _rff = np.arange(_xmin, _xmax, 0.001)

    # plot FF inputs
    RFF = RFF_Dz+RFF_Dstz
    plt.axvspan(RFF.n-RFF.s, RFF.n+RFF.s, alpha=.55, facecolor='darkorange', zorder=0, edgecolor="firebrick", lw=1.)
    plt.axvline(x = RFF.n, color="firebrick",  lw=1.5, zorder=.9, alpha=1., ls=":")

    # experimental input
    plt.fill_between(
        _rff, 
        np.sqrt( (RBF.n-RBF.s) / _rff),
        np.sqrt( (RBF.n+RBF.s) / _rff),
        alpha=.9,
        facecolor="lightblue",
        edgecolor="tab:blue", lw=1., zorder=2,
    ) # stat uncertainty band
    RBF_cval = plt.plot(
        _rff,
        np.sqrt(RBF.n / _rff),
        color="tab:blue", ls="--",
        lw=1.5,
        zorder=3,
    )# central value

    # VubVcb exclusive 
    plt.axhspan( VubVcb_excl_PDG.n - VubVcb_excl_PDG.s, VubVcb_excl_PDG.n+VubVcb_excl_PDG.s, 
        facecolor="forestgreen", alpha=.4, 
        zorder=4, 
        edgecolor="darkgreen", lw=1.
    )
    plt.axhline(y = VubVcb_excl_PDG.n, color="darkgreen", ls="-.", lw=1, zorder=5)

    # data points extracted in this work
    if plot_data_points:
        res = plt.errorbar(
            x = RFF.n,
            y = ((RBF/RFF)**.5).n,
            yerr = ((RBF/RFF)**.5).s,
            fmt="^", color="black",
            elinewidth=1.5, capsize=3.0, capthick=2.0,
            lw=3,
            markersize=5,
            zorder=6,
            label="This work"
        )

    # patch for legend, filles
    RFF_std  = plt.fill(np.NaN, np.NaN, "darkorange", alpha=0.55, edgecolor="firebrick", lw=1.)
    RFF_cval = plt.plot(np.NaN, np.NaN, "firebrick", lw=1, ls=":")
    RBF_std  = plt.fill(np.NaN, np.NaN, "lightblue", alpha=0.9, edgecolor="tab:blue", lw=1.)
    PDG_std  = plt.fill(np.NaN, np.NaN, "forestgreen", alpha=0.4, edgecolor="darkgreen", lw=1)
    PDG_cval = plt.plot(np.NaN, np.NaN, "darkgreen", ls="-.", lw=1)
    plt.legend(
        [
            (RFF_std[0], RFF_cval[0]), 
            (RBF_std[0], RBF_cval[0]), 
            (PDG_std[0], PDG_cval[0]), 
            (res),
        ],
        [
            r"$R_{FF}^{D^0} + R_{FF}^{D^{0(*)}}$",
            r"$\frac{\mathcal{B}(B_c^+ \to D^{0(*)}  \mu^+ \nu_{\mu})}{\mathcal{B}(B_c^+ \to J/\psi  \mu^+ \nu_{\mu})}$",
            r"PDG $|V_{ub}|/|V_{cb}|$ exclusive", 
            r"$|V_{ub}|/|V_{cb}|$, this work",
        ],
        loc='upper left', bbox_to_anchor=(1, 0.95), 
        fontsize=20)
    plt.ylim(0.001, 0.8)
    plt.xlim(_xmin, _xmax)
    plt.title("LHCb Unofficial 5.4 fb$^{-1}$", loc="left")
    plt.ylabel(r"$|V_{ub}|/|V_{cb}|$", loc="center")
    plt.xlabel(r"$R_{FF}$", loc="center")
    plt.savefig(f"{_path}/VubVcb.png")

# global vars
# ======================================
# experimental input
# yeilds and efficiencies
# ======================================
# Bc->JpsiMuNu 5.4 invfb yield, OK to keep unblind
JpsiMuNu_y = ufloat(47486,873)
# Inclusive yield Bc->D(*)MuNu 5.4 invfb yield, keep blind
SIGNAL_YIELD_INPUT = ufloat(2300, (446**2 + 340**2)**.5) # stat and mixingsyst, respectively, added in quadrature (assumed uncorrelated)
SIGNAL_YIELD_INPUT_STATONLY = ufloat(2300, 446)


# ======================================
# theoretical input
# RFF = 1/|Vxb|^2 width(Bc->Xu,c(*)MuNu)
# ======================================
# lattice QCD prediction of the width ratio D0/Jpsi
RFF_D0_LCQD = ufloat(0.257, (0.036**2 + 0.018**2)**.5)

# 3ptSR width in eV
w_D0_3ptSR  = ufloat(2.0e-3, 0.3e-3) # eV
w_Dst_3ptSR = ufloat(5e-3, 2e-3) # eV

# BF predictions, according to 3ptSR
BF_Dz_3ptSR     = ufloat(2.4e-5, 0.4e-5)
BF_Dstarz_3ptSR = ufloat(7e-5, 3e-5)

# the jpsimunu lattice width (consistent normalisation)
eta_EW = ufloat(1.0062, 0.0016) # eV
w_Jpsi_LQCD = eta_EW * ufloat(11.36e-3, 0.81e-3) # eV

# ======================================
# external inputs (PDG)
# BF for charmed hadrons->final state
# ======================================
BF_Jpsi2mumu = ufloat(5.961e-2, 0.033e-2)
BF_D02KmPip = ufloat(3.946e-2, 0.030e-2)

Bc_ltime_PDG = ufloat(0.510e-12, 0,009e-12)

# PDG exclusive CKM
Vcb_excl_PDG = ufloat(39.5e-3,0.9e-3) 
Vub_excl_PDG = ufloat(3.70e-3, (0.10e-3**2 + 0.12e-3**2)**.5 )
VubVcb_excl_PDG = ufloat(0.094, 0.005)

# work out the Bc->JpsiMuNu/Vcb**2 rate from predicted BF in arXiv:1901.08368v2
w_Jpsi_LQCD_s = eta_EW * ufloat(1.73e13, 0.12e13) # s-1


if __name__ == '__main__':
    parser = ArgumentParser(
            description="Plot configuration")
    parser.add_argument('-U','--unblind',   action="store_true", help='Unblind the inclusive D(*) yield')
    parser.add_argument('-D','--datapoints',action="store_true", help='Plot the datapoints of this work')
    parser.add_argument('-e','--effpath',   default="/usera/delaney/private/Bc2D0MuNuX/Efficiencies/jsons/Total.json")
    parser.add_argument('-L','--lumipath',  default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Lumi.json")
    parser.add_argument('-p','--plotpath',  default="/usera/delaney/private/Bc2D0MuNuX/Systematics")
    parser.add_argument('-q','--SR',        default="cval", help="central value or 1σ bounds", choices=["cval", "sup", "inf"]) 
    parser.add_argument('-x','--debug',     action='store_true', help="stdout to terminal for all fcns")
    parser.add_argument('-F','--FFsystpath',default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/prop/to_syst_FF.pkl", help="location of the FF model syst dic")
    parser.add_argument('-Q','--BFsystpath',default="/usera/delaney/private/Bc2D0MuNuX/Systematics/FF/prop/to_syst_3ptSR.pkl", help="location of the FF model syst dic")
    opts = parser.parse_args()

    # load the FF-related systdicts related to yeilds and effs
    with open(f"{opts.FFsystpath}", "rb") as f_FF: 
        FF_systdict = pickle.load(f_FF)
    with open(f"{opts.BFsystpath}", "rb") as f_RBF: 
        RBF_systdict = pickle.load(f_RBF)
    assert(np.abs(FF_systdict["nominal"]["2018"]["eff"]["2018"].n - RBF_systdict["cval"]["effs"]["2018"].n)<1e-6)

    # suppress the output from imported functions
    original_stdout = sys.stdout
    # with open(f"/usera/delaney/private/Bc2D0MuNuX/Systematics/trash", 'w') as _fout:
    #     #if not opts.debug: sys.stdout = _fout 

    # =================================
    #             YIELDS
    # =================================
    # signal yield blinding implemented via a scale factor
    if opts.unblind: 
        blind_factor = 1.0
    else:
        blind_factor = np.random.normal(loc=1.0, scale=0.2, size=None)
    DDstMuNu_y = retrieve_sig_y(blind_factor, SIGNAL_YIELD_INPUT)

    DDstMuNu_y_FF_syst = calc_FF_syst_y(
        _dict=FF_systdict, 
        _key="yield",
        result_cval=DDstMuNu_y.n
    )
    DDstMuNu_y_RBF_syst = calc_RBF_syst_y(
        _dict=RBF_systdict, 
        _key="yield",
        result_cval=DDstMuNu_y.n
    )
    SIGNAL_YIELD = ufloat(DDstMuNu_y.n, (DDstMuNu_y.s**2 + DDstMuNu_y_FF_syst**2 + DDstMuNu_y_RBF_syst**2)**.5 )
    

    #     # need to go from eV to s-q
    #     s_to_eV_conv = w_Jpsi_LQCD / w_Jpsi_LQCD_s
    #     BF_JpsiMuNu_3ptSR = ufloat(2.24e-2, 0.57e-2) # take the larger error
    #     w_Jpsi_3ptSR = BF_JpsiMuNu_3ptSR / Bc_ltime_PDG / (Vcb_excl_PDG**2) * s_to_eV_conv
    #     # # matt's value
    #     # w_Jpsi_3ptSR = ufloat(0.005555555555555556,0.002030714092542714)
    #     #print(c(f"compatibility of lattice and 3ptSR for JpsiMuNu width/|Vcb|^2:\n{w_Jpsi_LQCD} (LQCD)\n{w_Jpsi_3ptSR} (from Bc2JpsiMuu BF prediction)").red)


    # =================================
    #           EFFICIENCIES
    # =================================
    # lumis
    JpsiMuNu_lumis= {}
    DDstMuNu_lumis= {}
    for y in ("2016", "2017", "2018"):
        JpsiMuNu_lumis[y] = calc_lumi_y(y, load_lumis(opts.lumipath), channel="JpsiMuNu")
        DDstMuNu_lumis[y] = calc_lumi_y(y, load_lumis(opts.lumipath), channel="D0MuNu")

    # D(*)MuNu effs
    DDstMuNu_effs = calc_eff(load_effs(opts.effpath), _val=opts.SR)
    JpsiMuNu_effs = {}
    for y in ("2016", "2017", "2018"):
        JpsiMuNu_effs[y] = calc_eff_y(y, load_effs(opts.effpath), channel="Bc2JpsiMuNu")

    # effcy across 5.4 invfb
    sig_effcy  = calc_lumi_weighted_eff(lumi_dict=DDstMuNu_lumis, eff_dict=DDstMuNu_effs)
    DDstMuNu_eff_FF_syst = calc_FF_syst_y(
        _dict=FF_systdict, 
        _key="eff",
        result_cval=sig_effcy.n # lumi-weighted effcy
    )
    DDstMuNu_eff_RBF_syst = calc_RBF_syst_eff(
        _dict = RBF_systdict,
        _key = "effs",
        result_cval=sig_effcy.n,
        lumi_dict = DDstMuNu_lumis # need to do a lumi-weightig before inspecting the variations between the RBF bounsds 
    )
    SIGNAL_EFF = ufloat(
        sig_effcy.n, (sig_effcy.s**2 + DDstMuNu_eff_FF_syst**2 + DDstMuNu_eff_RBF_syst**2)**.5
    )
    NORM_EFF = calc_lumi_weighted_eff(lumi_dict=JpsiMuNu_lumis, eff_dict=JpsiMuNu_effs)


    # =================================
    # experimental result, ratio of BFs
    # =================================
    RBF_STATONLY = extract_RBF(
        sig_y=SIGNAL_YIELD_INPUT_STATONLY, 
        norm_y=JpsiMuNu_y,
        sig_eff=SIGNAL_EFF,
        norm_eff=NORM_EFF,
        bfjpsi2mumu=BF_Jpsi2mumu,
        bfdz2kpi= BF_D02KmPip,
        sig_tot_lumi=np.sum(list(DDstMuNu_lumis.values())),
        norm_tot_lumi=np.sum(list(JpsiMuNu_lumis.values()))
    )
    RBF = extract_RBF(
        sig_y=SIGNAL_YIELD, 
        norm_y=JpsiMuNu_y,
        sig_eff=SIGNAL_EFF,
        norm_eff=NORM_EFF,
        bfjpsi2mumu=BF_Jpsi2mumu,
        bfdz2kpi= BF_D02KmPip,
        sig_tot_lumi=np.sum(list(DDstMuNu_lumis.values())),
        norm_tot_lumi=np.sum(list(JpsiMuNu_lumis.values()))
    )

    # semi-exclusive extractions of VubVcb
    VubVcb = extract_VubVcb(
        RBF=RBF,
        RFF_Dz=RFF_D0_LCQD,
        RFF_Dstz=RFF_D0_LCQD*(BF_Dstarz_3ptSR/BF_Dz_3ptSR),
    )
    
    # plot
    hep.style.use(["LHCbTex", "LHCb2"])
    plot_results(
        RBF=RBF,
        RFF_Dz=RFF_D0_LCQD,
        RFF_Dstz=RFF_D0_LCQD*(BF_Dstarz_3ptSR/BF_Dz_3ptSR),
        plot_data_points=opts.datapoints,
        _path=opts.plotpath
        )
    #_fout.close()
    sys.stdout = original_stdout # restore output


    # onto the stuff we want printed
    # inspect the compatibility of results
    BF_table = [[
        "BF(Bc->D(*)MuNu = ", f"{RBF:.3f}"
    ]]
    print(tabulate(BF_table, tablefmt="fancy_grid"))

    VubVcb_header = ["FF calculation", "RFF D0", "RFF D*", "RFF D(*)", "|Vub|/|Vcb| this work", "Compatibility with PDG |Vub|/|Vcb|excl"]
    VubVcb_table  = [[
        "Lattice+BF(D*/D0) scaling", RFF_D0_LCQD, RFF_D0_LCQD*(BF_Dstarz_3ptSR/BF_Dz_3ptSR),  RFF_D0_LCQD+RFF_D0_LCQD*(BF_Dstarz_3ptSR/BF_Dz_3ptSR), VubVcb, np.abs((VubVcb-VubVcb_excl_PDG).n)/(VubVcb-VubVcb_excl_PDG).s
    ]]
    print(tabulate(VubVcb_table, VubVcb_header, tablefmt="fancy_grid", numalign="center"))