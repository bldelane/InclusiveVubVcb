'''Fitting code: ML binned fit implementing B&Z (arxiv:1309.1287)
__author__: Blaise Delaney
__email__ : blaise.delaney@cern.ch
'''

import sys, os
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, "/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/executables/common")
from header import *
from termcolor2 import c
import boost_histogram as BH
import pickle
from pathlib import Path
from collections import namedtuple

def load_bh(idx, rfiles, FITVARS, fit_config)->"boost histogram":
    """Load the nominal data-driven fit templates"""

    indf = rfiles[idx]
    samples = [np.array(indf[FITVARS[0]])]
    
    if idx=="MISID":
        wts = indf["misid_w"]*indf[f"sw"] # if misID, weights = sw x fully-extracted misID weight (includes anti-prescale)
    if idx=="COMB":
        wts = indf["GBR_w"]*indf[f"sw"] # if COMB, weights = sw x GBR weight from evt mixing       
    # in any other case, we only care about the sw from fit to D0 mass
    if idx=="CF" or idx=="DCS":
        wts = indf[f"sw"] # in any other case, we only care about the sw from fit to D0 mass

    _nbins_1  = fit_config[FITVARS[0]]["nbins"]
    _minvar_1 = fit_config[FITVARS[0]]["range"][0] 
    _maxvar_1 = fit_config[FITVARS[0]]["range"][-1] 

    if fit_config[FITVARS[0]]["is_var_axis"]=="False":
        ax_1 = BH.axis.Regular(_nbins_1, _minvar_1, _maxvar_1, underflow=False, overflow=False )
    if fit_config[FITVARS[0]]["is_var_axis"]=="True":
        ax_1 = BH.axis.Variable(fit_config[FITVARS[0]]["range"], underflow=False, overflow=False )
    if idx=="D0_MC" or idx=="Dst_MC" or idx=="incl_sig":
        wbh = BH.Histogram( ax_1 )
        wbh.fill( *samples, )
    else:
        wbh = BH.Histogram( ax_1, storage=BH.storage.Weight() )
        wbh.fill( *samples, weight=wts )    
    return wbh

def calc_RBF_3ptSR(_val:"central value, supremum or infimum")->"3ptSR [central value only]":
    """=== constrain D0/D* yields from theory and effs
    Leljak et al., arXiv:1909.01213
    CONVENTION: D0/D*"""
    if _val not in ("cval, sup, inf"):
        raise ValueError("Value not allowed; choose from [cval, sup (+1sigma), inf (-1sigma)]")
    BF_Bc2D0MuNu_3ptSR = ufloat(2.4e-5, 0.4e-5)
    BF_Bc2DstMuNu_3ptSR = ufloat(7e-5, 3e-5)
    
    print(c(f"Ratio of D0/D* BF predicted by 3ptSR: {BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR}"))
    if _val=="cval":
        RBF_3ptSR = (BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR).n
    if _val=="sup": #+1σ on ratio of BFs
        RBF_3ptSR = (BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR).n + (BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR).s
    if _val=="inf": #-1σ on ratio of BFs
        RBF_3ptSR = (BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR).n - (BF_Bc2D0MuNu_3ptSR/BF_Bc2DstMuNu_3ptSR).s
    print(c(f"Running with RBF_3ptSR: {RBF_3ptSR}").white.on_cyan)
    
    return RBF_3ptSR

def calc_eff(tot_eff_dict, FF="", year_tuple=("2016","2017","2018"), **kwargs)->dict:
    """work out the polarity-average D0 and D* effs"""
    sig_eff_dict = {}

    for  y in year_tuple: 
        """Assemble the inclusive reco efficiency"""

        # load the per-sim eff
        D0g_MU_eff = ufloat(tot_eff_dict[f"Bc2D0gMuNu{FF}"][y]["MU"]["Total"]["eff"],tot_eff_dict[f"Bc2D0gMuNu{FF}"][y]["MU"]["Total"]["err"])
        D0g_MD_eff = ufloat(tot_eff_dict[f"Bc2D0gMuNu{FF}"][y]["MD"]["Total"]["eff"],tot_eff_dict[f"Bc2D0gMuNu{FF}"][y]["MD"]["Total"]["err"])
        D0pi0_MU_eff = ufloat(tot_eff_dict[f"Bc2D0pi0MuNu{FF}"][y]["MU"]["Total"]["eff"],tot_eff_dict[f"Bc2D0pi0MuNu{FF}"][y]["MU"]["Total"]["err"])
        D0pi0_MD_eff = ufloat(tot_eff_dict[f"Bc2D0pi0MuNu{FF}"][y]["MD"]["Total"]["eff"],tot_eff_dict[f"Bc2D0pi0MuNu{FF}"][y]["MD"]["Total"]["err"])
        D0_MU_eff = ufloat(tot_eff_dict[f"Bc2D0MuNu{FF}"][y]["MU"]["Total"]["eff"],tot_eff_dict[f"Bc2D0MuNu{FF}"][y]["MU"]["Total"]["err"])
        D0_MD_eff = ufloat(tot_eff_dict[f"Bc2D0MuNu{FF}"][y]["MD"]["Total"]["eff"],tot_eff_dict[f"Bc2D0MuNu{FF}"][y]["MD"]["Total"]["err"])

        # take the average of per-pol eff
        eff_D0gMuNu = (D0g_MU_eff+D0g_MD_eff)/2
        eff_D0pi0MuNu = (D0pi0_MU_eff+D0pi0_MD_eff)/2
        eff_D0MuNu = (D0_MU_eff+D0_MD_eff)/2

        # D* BF from PDG
        Dst_toD0g_BF = ufloat(0.353, 0.009)
        Dst_toD0pi0_BF = ufloat(0.647, 0.009)

        # compute weighted average of the D* effienciency
        eff_DstMuNu = (Dst_toD0g_BF*eff_D0gMuNu + Dst_toD0pi0_BF*eff_D0pi0MuNu)/(Dst_toD0g_BF+Dst_toD0pi0_BF)
        # compute the D0/D* BF ratio from theory
        RBF_3PTSR = calc_RBF_3ptSR(**kwargs)        

        print(c(f"=================================================="))
        print(c(f"Year tuple: {year_tuple}").yellow)
        print(c(f"Retrieving the reco efficiency in {y}").yellow)
        print(c(f"Ratio of BF: D0/D* = {RBF_3PTSR}\n").cyan)
        print(c(f"Extracted weighted average for Dst efficiency in year= {eff_DstMuNu}").cyan)
        print(c(f"D0 efficiency = {eff_D0MuNu}").cyan)

        # put together the per-year inclusive efficiency
        incl_sig_eff = (RBF_3PTSR)/(RBF_3PTSR+1)*eff_D0MuNu + eff_DstMuNu/(RBF_3PTSR+1)
        print(c(f"=> Put everything together: inlclusive mode efficiency: {incl_sig_eff:.6f}").blue)
        print(c(f"==================================================\n")) 
        sig_eff_dict[y] = incl_sig_eff

    return sig_eff_dict

def load_binning(conf_path)->dict:
    """Utility function to load fit variable(s) and binning"""
    assert( os.path.exists( conf_path ) )
    with open(conf_path) as config_file: 
        fit_config = json.load(config_file)
    print(c(f"\nLoaded fit configuration:").magenta.underline)
    print(c(f"{fit_config}\n").magenta)
    
    return fit_config

def load_effs(eff_path)->dict:
    """Utility function to load efficiency dict"""
    with open(eff_path) as f_in:
        tot_eff_dict = json.load(f_in)
    
    return tot_eff_dict

def load_lumis(lumi_path)->dict:
    """Utility function to load lumi dict"""
    with open(lumi_path) as f_in:
        tot_lumi_dict = json.load(f_in)
    
    return tot_lumi_dict

def calc_lumi_y(year, lumi_dict, channel="D0MuNu")->"magpol-averaged per-year luminosity ":
    """compute mag-pol average of lumis"""
    if channel not in lumi_dict.keys():
        raise KeyError(channel, 'not in lumi dict')
    if year not in lumi_dict[channel].keys():
        raise KeyError(year, 'not in lumi dict years')

    MU = lumi_dict[channel][year]['MU']
    MD = lumi_dict[channel][year]['MD']

    uMU = ufloat( MU['lumi'], MU['err'] )
    uMD = ufloat( MD['lumi'], MD['err'] )

    return (uMU+uMD)

def calc_eff_y(year, eff_dict, channel:"Bc2XcMuNu")->"magpol-averaged per-year efficiency":
    """compute mag-pol average of effs"""
    if channel not in eff_dict.keys():
        raise KeyError(channel, 'not in effcy dict')
    if year not in eff_dict[channel].keys():
        raise KeyError(year, 'not in effcy dict years')

    MU = eff_dict[channel][year]['MU']["Total"]
    MD = eff_dict[channel][year]['MD']["Total"]

    uMU = ufloat( MU['eff'], MU['err'] )
    uMD = ufloat( MD['eff'], MD['err'] )

    print(c("============================================================================="))
    print(c(f"TOTAL MAGUP EFF: year: {year};channel: {channel}; eff:{uMU:.6f}").blue)
    print(c(f"TOTAL MAGDN EFF: year: {year};channel: {channel}; eff:{uMD:.6f}").blue)
    print(c("=============================================================================\n"))
    return (uMU+uMD)/2 

def calc_efflumi_factor(sig_eff_dict, year_tuple, **kwargs)->dict:
    """Each year has a prefactor:= lumi x eff
    use this utility function to compute this, assuming nominal lumi values"""
    assert(len(year_tuple)==len(list(sig_eff_dict.keys())))
    eff_lumi_raw = {}
    for y in year_tuple:
        eff_lumi_raw[str(y)] = calc_lumi_y(year=str(y), **kwargs)*sig_eff_dict[str(y)]
    
    NORMALISATION = np.sum(list(eff_lumi_raw.values()))
    
    lumi_factors = {}
    for y in year_tuple:
        lumi_factors[str(y)] = eff_lumi_raw[str(y)] / NORMALISATION
    # check prefactors sum to unity, within a tolerance threshold
    assert(np.abs(np.sum(list(lumi_factors.values()))) - 1.0 < 0.00001)
    
    return lumi_factors

def load_sim(simpath, year, FF)->dict:
    """Load boosterised simulated templates"""
    print(c("\nLoading simulates hists:").underline)
    MC_hists = {}
    with open(f"{simpath}/{year}/{FF}.pkl", "rb") as f_dz: 
        MC_hists["incl_sig"] = pickle.load(f_dz)
        print("Inclusive signal MC: done")

    return MC_hists

def derive_CF_constr(**kwargs)->float:
    """Extract the CF yield constraint"""
    # from PDG, get the BFs D0->Kpi
    CF = ufloat(3.950e-2, 0.031e-2)
    DCS = ufloat(1.50e-4, 0.07e-4)
    # scale factor for CF_y -> B+_y constraint in the fit 
    R = DCS/CF
    # compute the error as sqrt(sum(w**2))
    CF_n = load_bh("CF", **kwargs).sum().value
    assert(CF_n>1000) #un-normalised template
    CF_s = load_bh("CF", **kwargs).sum().variance**.5 
    CF_y = ufloat( CF_n, CF_s )
    bu_y_constraint = CF_y * R
    print(c(f"\n===============================================================").cyan)
    print(c(f"      B+ constraint in physical space: {bu_y_constraint}").cyan)
    print(c(f"===============================================================").cyan)
    
    return bu_y_constraint

# =================================================
# XXX: deprecated, keep here in view of refactoring
def calc_BZ_scalefactors(
    unscaled_templ_dict_cvals:"dictionary holding unscaled boost histograms, cvals",
    unscaled_templ_dict_errs:"dictionary holding unscaled boost histograms, errors")->"Bohm-Zech s_i factors":
    """compute the Bohm-Zech scale factors from weighted observation
    Nucl.Instrum.Meth.A 748 (2014) 1-6"""
    if "DCS" not in unscaled_templ_dict_cvals.keys() or "DCS" not in unscaled_templ_dict_errs.keys():
        raise KeyError("DCS key not in input dict")
    
    assert(unscaled_templ_dict_cvals["DCS"].all()>0) # requirement: positive expectation 
    print(c("=========================================================").bold)
    print(c("   Warning: running in nominal mode with BZ turned ON!   ").bold.on_magenta)
    print(c("=========================================================").bold)
    
    s_i = unscaled_templ_dict_errs["DCS"]**2/unscaled_templ_dict_cvals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done

    return s_i
# =================================================

def calc_BZ() -> 
    """compute the Bohm-Zech scale factors from weighted observation
    Nucl.Instrum.Meth.A 748 (2014) 1-6"""
    if "DCS" not in unscaled_templ_dict_cvals.keys() or "DCS" not in unscaled_templ_dict_errs.keys():
        raise KeyError("DCS key not in input dict")
    
    assert(unscaled_templ_dict_cvals["DCS"].all()>0) # requirement: positive expectation 
    print(c("=========================================================").bold)
    print(c("   Warning: running in nominal mode with BZ turned ON!   ").bold.on_magenta)
    print(c("=========================================================").bold)
    
    s_i = unscaled_templ_dict_errs["DCS"]**2/unscaled_templ_dict_cvals["DCS"] # for weighted fits, sum(w**2) is err; correctness check: done

    return s_i


def load_validate_data_df(nompath, year, component)->"dataframe for data-driven template":
    """load in the pkl'd sW'd per-year dataframes
    components allowed: [MisID, CF, DCS]"""
    if component not in ["MisID", "CF", "DCS"]:
        raise ValueError(f"Data-driven template name not allowed. Choose from [MisID, CF, DCS]")
    _df = pandas.read_pickle(f"{nompath}/{year}/{component}.pkl")
    # sanity checks
    if component=="DCS" or component=="MisID":
        assert((_df["K_minus_ID"]*_df["Mu_plus_ID"]).all()>0.0) # data and misid selected in the DCS final-state signature
    if component=="CF":
        assert(np.all(_df["K_minus_ID"]*_df["Mu_plus_ID"]<0.0))
    
    return _df

def build_write_schema(raw_args=None):
    parser = ArgumentParser(
        description="Schema parameters for syst mixing study")
    parser.add_argument('-y','--year',     required=True, help='Year of data taking [2011,2012,2015,2016,2017,2018]')
    parser.add_argument('-Y','--yeartuple',default=("2016","2017","2018"), help="cycle through years to extract effs", nargs='+')
    parser.add_argument('-s','--systpath', default="/r01/lhcb/Bc2D0MuNuX/systematics/event-mixing")
    parser.add_argument('-n','--nompath',  default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/DATA_TEMPLATES/D0MuNu")
    parser.add_argument('-c','--confpath', default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/config/config.json")
    parser.add_argument('-e','--effpath',  default="/usera/delaney/private/Bc2D0MuNuX/Efficiencies/jsons/Total.json")
    parser.add_argument('-L','--lumipath', default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/Lumi.json")
    parser.add_argument('-m','--mix_templ',default="alt1", help="choose the alternative event mixing model", choices=["alt1", "alt2", "alt3"]) # alt1: pol 1st order as nominal
    parser.add_argument('-q','--SR',       default="cval", help="central value or 1σ bounds", choices=["cval", "sup", "inf"]) 
    parser.add_argument('-F','--FF',       default="", help="FF model", choices=["", "_Kis", "_ISGW2"]) 
    parser.add_argument('-l','--logs',     default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/logs", help="Redirect print statements")
    parser.add_argument('-S','--simpath',  default="/usera/delaney/private/Bc2D0MuNuX/pyhf_Fit/MC/pkl/D0MuNu", help="Where the MC templates live")
    parser.add_argument('-o','--output',   default="/usera/delaney/private/Bc2D0MuNuX/Systematics/EventMixing/scratch", help="Where the schemas live")
    parser.add_argument('-d','--sigsim',   default="incl_sig", choices=["incl_sig", "incl_sig_Kis", "incl_sig_ISGW2"], help="Form factor model for D0(*)MuNu")
    parser.add_argument('-x','--debug',    action='store_true', help="stdout to terminal")
    opts = parser.parse_args(raw_args)

    # redirect print statements to log file
    Path(f"{opts.logs}").mkdir(parents=True, exist_ok=True)
    with open(f"{opts.logs}/schema_D0MuNu_{opts.year}_{opts.mix_templ}.log", 'w') as _fout:
        if not opts.debug: sys.stdout = _fout 

        #======================================================
        #                   EFF LUMI FACTORS                  #
        #======================================================
        # load fit configuration (fit var and binnnig)
        fit_config =load_binning(opts.confpath)

        # read in the json file with total efficiency
        tot_eff_dict = load_effs(opts.effpath)

        # compute all efficiencies, based on chosen value of ratio of signal branching fractions
        sig_eff_dict = calc_eff(tot_eff_dict, FF=opts.FF, year_tuple=opts.yeartuple, _val=opts.SR)

        # assuming nominal lumi vals, each year has as scale factor:= lumi x efficiency
        lumi_factors = calc_efflumi_factor(sig_eff_dict=sig_eff_dict, year_tuple=opts.yeartuple, channel="D0MuNu", lumi_dict=load_lumis(opts.lumipath), )
        print(c(f"Lumi sig scaling in {opts.year} [efficiency-corrected]: {lumi_factors[opts.year]}\n").bold)

        # data-driven dataframes for which systematic is not computed; final-state DCS/CF signature checks included
        rfiles = {}
        for t in ("DCS", "CF", "MISID"):
            if t!="MISID":
                rfiles[t] = load_validate_data_df(
                    nompath=opts.nompath,
                    year=opts.year,
                    component=t
                )
            if t=="MISID": # mismatch in notation
                rfiles[t] = load_validate_data_df(
                    nompath=opts.nompath,
                    year=opts.year,
                    component="MisID"
                )

        # a big ugly, from porting from dev.ipynb
        FITVARS = list(fit_config.keys())
        N_FITVARS = len(FITVARS)
        TOT_BINS = 1
        for i in range(N_FITVARS):
            TOT_BINS*=(fit_config[FITVARS[i]]["nbins"])

        #======================================================
        #           LOAD UNSCALED HISTOGRAMS                  #
        #======================================================
        # data-driven templates for which not consider alternative templates
        data_driven_samples = ["CF", "MISID"]
        MC_samples = ["incl_sig"]
        # fit components
        fit_components = ("CF", "COMB", "MISID", "incl_sig")

        # load seperately-generated MC hists
        MC_hists = load_sim(opts.simpath, year=opts.year, FF=opts.sigsim)

        # store cvals and err of templates and data
        template_vals = {}
        template_errs = {}

        # weighted data with dedicated storage
        print(c("\nLoading data-driven hists:").underline)
        for comp in data_driven_samples: 
            bh = load_bh(comp, FITVARS=FITVARS, fit_config=fit_config, rfiles=rfiles)
            unscaled_integral = np.sum(bh.view().value.flatten())
            template_vals[f"{comp}"] = bh.view().value.flatten() / unscaled_integral
            template_errs[f"{comp}"] = (bh.view().variance**.5).flatten() / unscaled_integral # weighted data-driven histograms; note how error and not variance is stored
            assert(abs( np.sum( template_vals[f"{comp}"] ) - 1.0 ) <= 0.00001) # sanity check: normalised to unity
            assert( ((template_vals[f"{comp}"] / template_errs[f"{comp}"]) / (bh.view().value.flatten() / (bh.view().variance**.5).flatten())).all() <=1.00001 ) # sanity check: relative error preserved
            print(c(f"{comp}: done, normalised to unity").green)

        # block for the loading of the mixing templates
        print(c("Towards the evaluation of the mixing syst:").underline)
        print(c(f"Running with alternative event mixing template {opts.mix_templ}").bold.on_cyan)
        with open(f"{opts.systpath}/{opts.mix_templ}_{opts.year}.pkl", "rb") as _fin:    
            bh = pickle.load(_fin)
        unscaled_integral = np.sum(bh.view().value.flatten())
        template_vals[f"COMB"] = bh.view().value.flatten() / unscaled_integral
        template_errs[f"COMB"] = (bh.view().variance**.5).flatten() / unscaled_integral # weighted data-driven histograms; note how error and not variance is stored
        assert(abs( np.sum( template_vals[f"{comp}"] ) - 1.0 ) <= 0.00001) # sanity check: normalised to unity
        assert( ((template_vals[f"{comp}"] / template_errs[f"{comp}"]) / (bh.view().value.flatten() / (bh.view().variance**.5).flatten())).all() <=1.00001 ) # sanity check: relative error preserved
        print(c(f"{comp}: done, normalised to unity\n").green)

        # MC has a regular storage
        for sim in MC_samples:
            unscaled_integral = np.sum( MC_hists[sim].view().flatten() )
            template_vals[f"{sim}"] = MC_hists[sim].view().flatten() / unscaled_integral
            template_errs[f"{sim}"] = np.sqrt( MC_hists[sim].view().flatten() ) / unscaled_integral # poisson error; note how the error and not variance is stored
            assert(abs( np.sum( template_vals[f"{sim}"] ) - 1.0 ) <= 0.00001) # sanity check: normalised to unity
            assert( ((template_vals[f"{sim}"] / template_errs[f"{sim}"]) / (MC_hists[sim].view().flatten() / (MC_hists[sim].view().flatten()**.5) ) ).all() <=1.00001 ) # sanity check: relative error preserved
            print(c(f"{sim}: done, normalised to unity\n").green) 

        # load data but do not normalise (->needed for BZ scaling later)
        template_vals["DCS"] = load_bh("DCS", rfiles, FITVARS, fit_config).view().value.flatten()
        template_errs["DCS"] = (load_bh("DCS", rfiles, FITVARS, fit_config).view().variance**.5).flatten()
        assert(abs( np.sum( template_vals["DCS"] ) - 1.0 ) >= 0.001) # check that it is not nomarlised

        # quick formatting checks 
        for s in template_vals.values(): assert(len(s) == TOT_BINS)
        for s in template_errs.values(): assert(len(s) == TOT_BINS)

        #======================================================
        #                   BOHM & ZECH                       #
        #======================================================
        s_i = calc_BZ_scalefactors(
            unscaled_templ_dict_cvals=template_vals,
            unscaled_templ_dict_errs=template_errs
        )
        # quick calculation by hand to check BZ
        assert(s_i.all() == (load_bh("DCS", rfiles, FITVARS, fit_config).view().variance/load_bh("DCS", rfiles, FITVARS, fit_config).view().value).all()) 

        # scaled observed data
        obs = template_vals["DCS"]/s_i

        # scale the rest accordingly
        muprime_vals = {} # container for scaled fit components
        muprime_errs = {} # contained for errors on scaled fit components

        # normalise the scaled muprimes (=the scaled expectations) [note how normalise to the binned template area before B&Z-scaling]
        for comp in fit_components:
            assert(abs(np.sum(template_vals[f"{comp}"]) - 1.0) <= 0.00001) # sanity check: all should be normalised to unity *before* BZ
            muprime_vals[f"{comp}"] = template_vals[f"{comp}"] / s_i  
            muprime_errs[f"{comp}"] = template_errs[f"{comp}"] / s_i 

        # extract the CF yield constraint
        bu_y_constraint = derive_CF_constr(rfiles=rfiles, FITVARS=FITVARS, fit_config=fit_config)

        #======================================================
        #                     pyhf spec                       #
        #======================================================
        print(c(f"\nPreparing the fit schema..."))

        # moving onto the spec: first formulate the standalone samples 
        signal_sample = {
                            "name": f"signal_{opts.year}",
                            "data": ( muprime_vals["incl_sig"] ).tolist(),
                            "modifiers": [
                                {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["incl_sig"].tolist() }, # branch-speficific per-bin variation
                                {"name": f"incl_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                                {"name": f"sig_lumi_factor_{opts.year}", "type": "normfactor", "data": None }, # scale signal to luminosity
                                ]
        }

        comb_sample = {
                            "name": f"combinatorial_{opts.year}",
                            "data": muprime_vals["COMB"].tolist(),
                            "modifiers": [
                                {"name": f"comb_y_{opts.year}", "type": "normfactor", "data": None }, # floating normalisation
                                {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                                ]
        }

        misid_sample = {  
                            "name": f"misid_{opts.year}",
                            "data": muprime_vals["MISID"].tolist(),
                            "modifiers": [
                                {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                                {"name": f"misid_y_{opts.year}", "type": "normfactor", "data": None}, # freely floating
                                ]
                            }

        bu_sample = {  
                            "name": f"bu_{opts.year}",
                            "data": muprime_vals["CF"].tolist(),
                            "modifiers": [
                                {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                                {"name": f"bu_y_constr_{opts.year}", "type": "normsys", "data": {"lo" : bu_y_constraint.n - bu_y_constraint.s, "hi": bu_y_constraint.n + bu_y_constraint.s} }, # gaussian constraint
                                ]
                            }


        # ===========================================================
        #                     BUILD THE MODEL                       #
        # ===========================================================
        spec = {
            "channels": [
                {
                    "name": f"singlechannel_{opts.year}",
                    "samples": [
                        signal_sample,
                        comb_sample,
                        bu_sample, 
                        misid_sample
                    ]
                },
            ],
            "observations": [
                {
                    "name": f"singlechannel_{opts.year}",
                    "data": obs.tolist(),
                },
            ],
            "measurements": [
                { 
                    "name": f"sig_y_extraction",
                    "config": {
                        "poi": "incl_sig_y",
                        "parameters": [
                            # bounds on floatig normalisations 
                            {"name":f"incl_sig_y", "bounds": [[ 0.0, obs.sum() ]], "inits":[1e2]},
                            {"name":f"sig_lumi_factor_{opts.year}", "inits": [lumi_factors[opts.year].n], "fixed":True}, # fix to the central value of the eff-lumi scale factor
                            {"name":f"comb_y_{opts.year}", "bounds":   [[ 0.0, obs.sum() ]], "inits":[1e3]},
                            {"name":f"misid_y_{opts.year}", "bounds":   [[ 0.0, obs.sum() ]], "inits":[1e3]},
                        ]
                    }
                }
            ],
            "version": "1.0.0"
        }

        # =================================================================================================
        #               Write schema; this will be merged with other years of data taking                 #
        # =================================================================================================
        Path(f"{opts.output}").mkdir(parents=True, exist_ok=True)
        spec = str(json.dumps(spec, indent=4)).replace("None", "null").replace("True", "true")
        SCHEMA = f"{opts.output}/model_spec_D0MuNu_{opts.mix_templ}_{opts.year}.json"
        with open(SCHEMA,'w') as outfile:
            outfile.write(spec)
        print(c(f"SCHEMA written for {opts.year} successfully to {opts.output}/model_spec_D0MuNu_{opts.mix_templ}_{opts.year}.json").green)
    

        # ===========================================================================
        #               Write specific templates for each component                 #
        # ===========================================================================
        # B+
        bu_template = {
            "channels": [
            {
                "name": "singlechannel",
                "samples": [
                    {  
                        "name": f"bu_{opts.year}",
                        "data": muprime_vals["CF"].tolist(),
                        "modifiers": [
                            {"name": f"bu_y_constr_{opts.year}", "type": "normsys", "data": {"lo" : bu_y_constraint.n - bu_y_constraint.s, "hi": bu_y_constraint.n + bu_y_constraint.s} }, # gaussian constraint
                            {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["CF"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }          
                ]
            }
            ]
        }

        # misid
        misid_template = {
            "channels": [
            {
                "name": "singlechannel",
                "samples": [
                    {  
                        "name": f"misid_{opts.year}",
                        "data": muprime_vals["MISID"].tolist(),
                        "modifiers": [
                            {"name": f"misid_y_constr_{opts.year}", "type": "normfactor", "data": None }, # freely floating
                            {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["MISID"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }         
                ]
            }
            ]
        }

        # D(*)MuNu
        signal_template = {
                "channels": [
                    {
                    "name": "singlechannel",
                    "samples": [
                        {
                        "name": f"signal_{opts.year}",
                        "data": ( muprime_vals["incl_sig"] ).tolist(),
                        "modifiers": [
                            {"name": "incl_sig_y", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": f"sig_lumi_factor_{opts.year}", "type": "normfactor", "data": None }, # scale signal to luminosity
                            {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["incl_sig"].tolist() }, # branch-speficific per-bin variation
                            ]
                        }
                    ]
                    }
                ]
            }

        # evt mix
        comb_template = {
            "channels": [
            {
                "name": "singlechannel",
                "samples": [
                    {  
                        "name": f"combinatorial_{opts.year}",
                        "data": muprime_vals["COMB"].tolist(),
                        "modifiers": [
                            {"name": f"comb_y_{opts.year}", "type": "normfactor", "data": None }, # floating normalisation
                            {"name": f"beeston_barlow_{opts.year}", "type": "staterror", "data": muprime_errs["COMB"].tolist() }, # branch-speficific per-bin variation
                        ]
                    }          
                ]
            }
            ]
        }

        # write to file    
        bu_spec = str(json.dumps(bu_template, indent=4)).replace("None", "null").replace("True", "true")
        bu_SCHEMA = f"{opts.output}/model_spec_D0MuNu_bu_{opts.mix_templ}_{opts.year}.json"
        with open(bu_SCHEMA,'w') as outfile:
            outfile.write(bu_spec)
        
        misid_spec = str(json.dumps(misid_template, indent=4)).replace("None", "null").replace("True", "true")
        misid_SCHEMA = f"{opts.output}/model_spec_D0MuNu_misid_{opts.mix_templ}_{opts.year}.json"
        with open(misid_SCHEMA,'w') as outfile:
            outfile.write(misid_spec)


        signal_spec = str(json.dumps(signal_template, indent=4)).replace("None", "null").replace("True", "true")
        signal_SCHEMA = f"{opts.output}/model_spec_D0MuNu_incl_signal_{opts.mix_templ}_{opts.year}.json"
        with open(signal_SCHEMA,'w') as outfile:
            outfile.write(signal_spec)

        comb_spec = str(json.dumps(comb_template, indent=4)).replace("None", "null").replace("True", "true")
        comb_SCHEMA = f"{opts.output}/model_spec_D0MuNu_comb_{opts.mix_templ}_{opts.year}.json"
        with open(comb_SCHEMA,'w') as outfile:
            outfile.write(comb_spec)

        print(c(f"Component-specific schemas written for {opts.year} successfully to:\n{bu_SCHEMA},\n{misid_SCHEMA},\n{signal_SCHEMA},\n{comb_SCHEMA}").green)
    
    _fout.close()


if __name__ == '__main__':
    build_write_schema()
